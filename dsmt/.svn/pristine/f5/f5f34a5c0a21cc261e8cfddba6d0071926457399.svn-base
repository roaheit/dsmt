package com.citi.ebx.dsmt.jdbc.connector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;

public class GOCUploadUtil {


	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	
	private static final String header = "SetID^GOC Primary Key^Eff Date^Status^MgrID^SID^Managed Geography ID^Managed Segment ID^FRS BU^FRS OU^Function ID^LVID^Headcount GOC^Short Description^Long Description^GOC Usage^Local Cost Code^Comment";

	private static String reportType = "SQL";
	private static String startDate;
	private static String endtDate;
	
	final String env =  propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);

    protected String sqlID ;
    protected String exportFileDirectoryPath;
    protected String exportFileName;
    protected static Connection dbConnection = null;
    protected static ArrayList<String> reportNames ;
    protected static ArrayList<String> sqlNames  ;
    protected static Map<String, Integer> readmeMap;
	
	
    protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
    
    public Map<String, Integer> generateReport (final Connection conection, String outputFilePath) {    	
    	GOCUploadUtil reportUtil = new GOCUploadUtil();
    	try{
    		 dbConnection=conection;
    		 getSQlReportMappingRelative();
			   		 
    		 /** get default output file path if value is not passed in as a parameter */
    		 if(null == outputFilePath){
    			 
    			 outputFilePath = SQLReportConnectionUtils.bundle.getString(env + DSMTConstants.DOT +"ExportFileDirectory");
    		 }
    		 
    		 readmeMap = reportUtil.generateGOCUploadFile(SQLReportConnectionUtils.bundle.getString(env + DSMTConstants.DOT + "SqlFileDirectory"), outputFilePath );
			 
    	}
    	catch(SQLException e)
    	{
    		LOG.error ("Error inside method  generateReport  ->" + e + " , message " + e.getMessage()); 
    	}
    	LOG.info("Readme map file in SQlConnect Util file "+ readmeMap);
    	return readmeMap;
	}
    
    public Map<String, Integer>  generateGOCUploadFile (final String sqlFileDirectory, final String exportFileDirectory) throws SQLException
	{
    	
		int reportcount=0;
		try {
			readmeMap= new java.util.HashMap<String, Integer>();
			
			for (String sqlfilename : sqlNames) {
				LOG.info("sqlfilename-->"+sqlfilename);
				sqlID = getInputQuery(sqlFileDirectory, sqlfilename);
				exportFileDirectoryPath= exportFileDirectory;
				exportFileName = reportNames.get(reportcount);
				//This method will generate execute the query and write each row in given file
				readmeMap = selectRecordsFromTable(); 
				reportcount++;	
			}
			
		} catch (SQLException e) {
			LOG.error ("Error inside method  generateCurrencyRpt  ->" + e + " , message " + e.getMessage()); 
		}
		finally{
			if (dbConnection != null) {
				dbConnection.close();
			}
			
		}
		  
		return readmeMap;
	}
    
    /**
   	 * Helper method to get the O/P from the file table
   	 * @param  
   	 * @throws SQLException
   	 */   
    
    
    
    
	public Map<String, Integer> selectRecordsFromTable() throws SQLException {

		PreparedStatement preparedStatement = null;

		File file = null;
		FileOutputStream fos = null;

		try {
			LOG.info(" SQl Connect report started for report name  = "
					+ exportFileName + " for report type " + getReportType());
			// Creating prepare statement.
			
			preparedStatement = dbConnection.prepareStatement(sqlID);
	
			/**
			for(int size=1;size<=16 ;  size++){
				if(size%2==0){
					preparedStatement.setString(size, getEndtDate());
				}else{
					
					preparedStatement.setString(size, getStartDate());
				}
			}
			
			
			LOG.info(" SQl Connect start date   = "
					+ getStartDate() + " and end date  " + getEndtDate());
				
				*/
			
			ResultSet rs = preparedStatement.executeQuery();
			ResultSetMetaData rsMetaData = rs.getMetaData();
			int columncount = rsMetaData.getColumnCount();
			// to get record counts in report
			int recordcount = 0;

			if (!exportFileDirectoryPath.endsWith("/")) {
				exportFileDirectoryPath = exportFileDirectoryPath + "/";
			}

			file = new File(exportFileDirectoryPath + exportFileName);

			file.setReadable(true, false);
			file.setWritable(true, true);

			fos = new FileOutputStream(file);
			while (rs.next()) {

				StringBuffer reportfile = new StringBuffer();
				if(recordcount==0){
					reportfile.append(header);
					reportfile.append(DSMTConstants.NEXT_LINE);
				}
				
				recordcount++;
				for (int i = 0; i < columncount; i++) {
					
					
					String data = rs.getString(i + 1);
					if (DSMTUtils.isStringNotEmptyOrNull(data)) {
						reportfile.append(data);
					} else {
						reportfile.append("");
					}

				}

				reportfile.append(DSMTConstants.NEXT_LINE);
				byte[] contentInBytes = reportfile.toString().getBytes();
				fos.write(contentInBytes);

			}
			readmeMap.put(exportFileName, recordcount);
			LOG.info("Total record count in " + exportFileName + " is "
					+ recordcount);
		} catch (SQLException e) {
			LOG.error(" Error  while excuting the query for report  >>>>"
					+ exportFileName + ". Exception ->" + e + " , message "
					+ e.getMessage());

		} catch (FileNotFoundException e) {

			LOG.error(" Error  while creating  >>>>" + exportFileName
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} catch (IOException e) {

			LOG.error("  Error  while writing  >>>>" + exportFileName
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException e) {
				LOG.error("  Error  while closing  >>>>" + exportFileName
						+ ". Exception ->" + e + " , message " + e.getMessage());
			}
			sqlID = DSMTConstants.BLANK_STRING;
		}
		
		return readmeMap;
	}  
    
    
    /**
   	 * Helper method to read Sql from file
   	 * @param  sqlFileDirectory , sqlFile
   	 * @throws 
   	 */       
    
	public String getInputQuery(String sqlFileDirectory,
			String sqlFile) {
		LOG.info("getInputQuery  " + sqlFileDirectory + "    " + sqlFile);
		StringBuffer inputquery = new StringBuffer();
		BufferedReader br = null;
		String line = null;
		try {
			br = new BufferedReader(new FileReader(sqlFileDirectory + sqlFile));
			while ((line = br.readLine()) != null) {

				inputquery.append(line);
				inputquery.append(DSMTConstants.NEXT_LINE);

			}

		} catch (FileNotFoundException e) {

			LOG.error("Unable to find SQl file  >>>>" + sqlFile
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} catch (IOException e) {

			LOG.error("Unable to read SQl file " + sqlFile + ". Exception ->"
					+ e + " , message " + e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return inputquery.toString();
	}
    
    
   /**
   	 * Helper method to get SQl file name and corresponding Report name
   	 * @param  
   	 * @throws 
   	 */        
    

	public void getSQlReportMappingRelative() {
		BufferedReader br = null;
		reportNames = new ArrayList<String>();
		sqlNames = new ArrayList<String>();
		int count_of_sql = 0;
		try {

			String line = DSMTConstants.BLANK_STRING;
			String splitBy = DSMTConstants.EQUALS;

			if (null != getReportType()) {
				br = new BufferedReader(new FileReader(
						SQLReportConnectionUtils.bundle.getString(env
								+ DSMTConstants.DOT + "SqlMAppingFile"
								+ DSMTConstants.DOT + getReportType())));
			} else {
				throw new FileNotFoundException("Missing file");
			}
			while ((line = br.readLine()) != null) {

				String[] values = line.split(splitBy);
				sqlNames.add(count_of_sql, values[0]);
				reportNames.add(count_of_sql, values[1]);
				count_of_sql++;

			}

		} catch (FileNotFoundException e) {

			LOG.error("Unable to find SQl property file  >>>> for report Type "
					+ getReportType() + " : " + e.getMessage());

		} catch (IOException e) {

			LOG.error("Unable to read SQl property file " + e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		LOG.info("NO of report ->> " + count_of_sql);
	}



			
public String getEffectiveDateForReport(){
		
		if(null!=getReportType()){
			 if(getReportType().equals(DSMTConstants.BD3REPORT)){
				return DSMTUtils.formatDate(DSMTUtils.previousMonthEffectiveDate(), DSMTConstants.DEFAULT_DATE_FORMAT);	
			}
			return DSMTUtils.treeEffectiveDate();
		}else{
			return DSMTUtils.treeEffectiveDate();
		}
	}
	
	 public String getReportType() {
			return reportType;
		}



		public void setReportType(String reportType) {
			this.reportType = reportType;
		}

		/**
		 * @return the startDate
		 */
		public String getStartDate() {
			return startDate;
		}

		/**
		 * @param startDate the startDate to set
		 */
		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}

		/**
		 * @return the endtDate
		 */
		public String getEndtDate() {
			return endtDate;
		}

		/**
		 * @param endtDate the endtDate to set
		 */
		public void setEndtDate(String endtDate) {
			this.endtDate = endtDate;
		}
 
		
		
}
