package com.citi.ebx.dsmt.exporter.task;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.citi.ebx.dsmt.jdbc.connector.SQLConnectReportUtil;
import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTExportBean;
import com.citi.ebx.util.SaveUtil;
import com.citi.ebx.util.Utils;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class GOCBulkSQLReportScheduledTask extends ScheduledTask {
	 protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	 protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	   
		
		protected String sqlID;
		protected String exportFileDirectoryPath;
		protected String exportFileName;
		final String env =  propertyHelper.getProp("dsmt.environment");
		
		@Override
		public void execute(ScheduledExecutionContext arg0)
				throws OperationException, ScheduledTaskInterruption {
			
			generateSQLReport(arg0);
		
		}
		
		public Map<String, Integer> generateSQLReport (ScheduledExecutionContext arg0){
			
			LOG.info("Report generation started !! "); 
			
			Map<String, Integer> sqlReportMap = null;
			Connection conn=null;
			try{
				
				SQLConnectReportUtil connectReportUtil = new SQLConnectReportUtil();
				connectReportUtil.setReportType(DSMTConstants.BULKREPORTS_HEADERTRAILER);
				conn = SQLReportConnectionUtils.getConnection();
				connectReportUtil.setCustomHeaderTrailer(true);
				LOG.info("GOCBulkSQLReportScheduledTask");
				sqlReportMap = connectReportUtil.generateReport(conn, null);
				List<String> sqlreportList = new ArrayList<String>(sqlReportMap.keySet());
				DSMTExportBean dsmtExportBean= new DSMTExportBean();
				dsmtExportBean.setFileNames(sqlreportList);
				dsmtExportBean.getFileNamesAndCount().putAll(sqlReportMap);
				final String userID = arg0.getSession().getUserReference().getUserId();
				for(String fileName:dsmtExportBean.getFileNamesAndCount().keySet()){
					Map<Path, Object> inputMap= new HashMap<Path, Object>();
					inputMap.put(DSMTConstants.REPORT_NAME, fileName);
					inputMap.put(DSMTConstants.REPORT_LOCATION, dsmtExportBean.getOutputFilePath());
					inputMap.put(DSMTConstants.REPORT_ROW_COUNT, dsmtExportBean.getFileNamesAndCount().get(fileName));
					inputMap.put(DSMTConstants.REPORT_STATUS, "Success");
					inputMap.put(DSMTConstants.REPORT_LASTUPDOPRID, userID);
					inputMap.put(DSMTConstants.REPORT_TIMESTAMP,Calendar.getInstance().getTime());		
					
					SaveUtil saveUtil = new SaveUtil(arg0.getRepository(),DSMTConstants.DATASPACE_DSMT2,DSMTConstants.DATASET_NEW_DSMT2,DSMTConstants.REPORT_AUDIT_PATH,inputMap,false );
					Utils.executeProcedure(saveUtil, arg0.getSession(), arg0.getRepository().lookupHome(
							HomeKey.forBranchName(DSMTConstants.DATASPACE_DSMT2)));
					
					Map<Path, Object> updateMap= new HashMap<Path, Object>();
					updateMap.put(DSMTConstants.CATALOG_TABLE_FILENAME, fileName);
					updateMap.put(DSMTConstants.CATALOG_TABLE_COUNT, dsmtExportBean.getFileNamesAndCount().get(fileName));
					
					
					
					
					SaveUtil asaveUtil = new SaveUtil(arg0.getRepository(),DSMTConstants.DATASPACE_DSMT2,DSMTConstants.DATASET_NEW_DSMT2,DSMTConstants.CATALOG_TABLE_PATH,updateMap,true );
					Utils.executeProcedure(asaveUtil, arg0.getSession(), arg0.getRepository().lookupHome(
							HomeKey.forBranchName(DSMTConstants.DATASPACE_DSMT2)));
					
				}
								
				}
			catch (Exception e){
				
				LOG.error("Report generation terminated with Exception -> " + e + ", message = " + e.getMessage());
			}
			finally{
				
				if(null != conn){
					try{
					conn.close();
					conn = null;
					}
					catch (SQLException SQ){
						LOG.error("Report generation terminated with Exception -> " + SQ + ", message = " + SQ.getMessage());
					}
				}
			}
			
			return sqlReportMap;
		}
		
		
		
		public String getSqlID() {
			return sqlID;
		}



		public void setSqlID(String sqlID) {
			this.sqlID = sqlID;
		}



		public String getExportFileDirectoryPath() {
			return exportFileDirectoryPath;
		}

		public void setExportFileDirectoryPath(String exportFileDirectoryPath) {
			this.exportFileDirectoryPath = exportFileDirectoryPath;
		}

		public String getExportFileName() {
			return exportFileName;
		}

		public void setExportFileName(String exportFileName) {
			this.exportFileName = exportFileName;
		}
		

}
