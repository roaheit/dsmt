package com.citi.ebx.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.citi.ebx.dsmt.util.ApprovalMonitoringTableBean;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.DSMT2WFPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class InsertApprovalWFMonitoringdata implements Procedure{
	ApprovalMonitoringTableBean mntBean;
	public Repository repo;
	public String tablePath ;
	public String tableupdated;
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	public static final Path APPROVER_1 = Path.parse("./REVIEW_REQUEST_1");
	
	public static final Path APPROVER_LEVEL_COMMENT_1 = Path.parse("./REVIEW_REQUEST_Comment_1");
	public static final Path APPROVER_LEVEL_ACTION_DATE_1 = Path.parse("./REVIEW_REQUEST_Action_Date_1");
	
	public static final Path APPROVER_2 = Path.parse("./REVIEW_REQUEST_2");
	public static final Path APPROVER_LEVEL_COMMENT_2 = Path.parse("./REVIEW_REQUEST_Comment_2");
	public static final Path APPROVER_LEVEL_ACTION_DATE_2 = Path.parse("./REVIEW_REQUEST_Action_Date_2");
	
	public static final Path APPROVER_3 = Path.parse("./REVIEW_REQUEST_3");
	public static final Path APPROVER_LEVEL_COMMENT_3 = Path.parse("./REVIEW_REQUEST_Comment_3");
	public static final Path APPROVER_LEVEL_ACTION_DATE_3 = Path.parse("./REVIEW_REQUEST_Action_Date_3");
	
	String REVIEW_REQUEST_ = "./REVIEW_REQUEST_";
	String REVIEW_REQUEST_Comment_ = "./REVIEW_REQUEST_Comment_";
	String REVIEW_REQUEST_Action_Date_ = "./REVIEW_REQUEST_Action_Date_";
	HashMap<String, String> map = new HashMap<String, String>();

	
	public InsertApprovalWFMonitoringdata(ApprovalMonitoringTableBean mntBean,
			Repository repo, String tablePath, String tableUpdated) {
		super();
		this.mntBean = mntBean;
		this.repo = repo;
		this.tablePath = tablePath;
		this.tableupdated = tableUpdated;
	}
	


	@Override
	public void execute(ProcedureContext pContext) throws Exception {
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,"DSMT_WF", "DSMT_WF");
		final AdaptationTable targetTable = metadataDataSet.getTable(getPath(tablePath));
			
		LOG.info("InsertDSMTWFMonitoringdata :  execute Started ");
			insertRecord(mntBean, pContext, targetTable);
			LOG.debug("InsertDSMTWFMonitoringdata :  operation " +mntBean.getOperation());
			LOG.info("InsertDSMTWFMonitoringdata :  execute End ");
	}

	
	private void insertRecord(ApprovalMonitoringTableBean tableBean, final ProcedureContext pContext,
			final AdaptationTable targetTable) throws OperationException {
		
		LOG.info("InsertMonitoringdata :  insertRecord Started ");
		Adaptation targetRecord =  getHistoryRecord(tableBean.getRequestID(),targetTable); 
		int level = tableBean.getApproverLevel();
		
		LOG.info("checking level for approvor in monitoring data   " +level);
		final ValueContextForUpdate valueContext;
		Date now = Calendar.getInstance().getTime();
		if (targetRecord == null) {
			valueContext = pContext.getContextForNewOccurrence(targetTable);
			valueContext.setValue(tableBean.getRequestID(),GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_ID);
			valueContext.setValue(tableBean.getRequestType(),GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_TYPE);
			valueContext.setValue(tableBean.getChildDataSpaceID(),GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID );
			valueContext.setValue(tableBean.getRequestBy(),GOCWFPaths._C_GOC_WF_REPORTING._REQ_BY);
			valueContext.setValue(now,GOCWFPaths._C_GOC_WF_REPORTING._REQ_DT);
			valueContext.setValue(tableBean.getRequestStatus(), GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
			valueContext.setValue(tableBean.getDataSet(), DSMTConstants.DATA_SET);
			valueContext.setValue(tableBean.getTableUpdated(), DSMTConstants.tableId);
			valueContext.setValue(tableBean.getTablePath(),DSMT2WFPaths._C_APP_WF_REPORTING._Table_Path);
		} else {
			
			LOG.info("InsertMonitoringdata :  insertRecord inside else ");
			valueContext = pContext.getContext(targetRecord.getAdaptationName());
			
			String reqStatus = tableBean.getRequestStatus();
			
			LOG.info("checking for request status " + reqStatus);
			valueContext.setValue(reqStatus , GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
			
			
			if (reqStatus.equalsIgnoreCase(GOCConstants.CREATED)){
				
				valueContext.setValue(tableBean.getRequestType(),GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_TYPE);
				valueContext.setValue(tableBean.getChildDataSpaceID(),GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID );
				valueContext.setValue(tableBean.getRequestBy(),GOCWFPaths._C_GOC_WF_REPORTING._REQ_BY);
				valueContext.setValue(now,GOCWFPaths._C_GOC_WF_REPORTING._REQ_DT);
				valueContext.setValue(tableBean.getDataSet(), DSMTConstants.DATA_SET);
				valueContext.setValue(tableBean.getTableUpdated(), DSMTConstants.tableId);
				valueContext.setValue(tableBean.getTablePath(),DSMT2WFPaths._C_APP_WF_REPORTING._Table_Path);
			
			}
			
			if (reqStatus.equalsIgnoreCase(GOCConstants.SUBMITTED)){
				valueContext.setValue(now,GOCWFPaths._C_GOC_WF_REPORTING._SUBIMITTED_DT);
				valueContext.setValue(tableBean.getRequestorComment(),GOCWFPaths._C_GOC_WF_REPORTING._Requester_Comment);
				valueContext.setValue(tableBean.getRecordCount(), DSMTConstants.RECORD_COUNT);
				valueContext.setValue(tableBean.getAddition(), DSMTConstants.ADDITION);
				valueContext.setValue(tableBean.getUpdation(), DSMTConstants.UPDATION);
				valueContext.setValue(tableBean.getReactivation(), DSMTConstants.REACTIVATION);
				valueContext.setValue(tableBean.getDeactivation(), DSMTConstants.DEACTIVATION);

			}
						
			String count = String.valueOf(level);
			
			if (reqStatus.equalsIgnoreCase(DSMTConstants.ASSIGN)){
				
				String path1 = REVIEW_REQUEST_.concat(count);
				LOG.info("checking path of table " +path1);
				String path2 = REVIEW_REQUEST_Comment_.concat(String.valueOf(level-1));
				String path3 = REVIEW_REQUEST_Action_Date_.concat(String.valueOf(level-1));
			
				valueContext.setValue(tableBean.getApprover(),Path.parse(path1));
			
				if (tableBean.getApprover()==null || tableBean.getApprover().equals("")){
						valueContext.setValue("DEFAULT APPROVER",Path.parse(path1));
				}
				
					valueContext.setValue(DSMTConstants.REQUEST_STATUS + count, GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
				
				if (level >1 && !reqStatus.equalsIgnoreCase(GOCConstants.REJECTED)
					&& !reqStatus.equalsIgnoreCase(DSMTConstants.PENDING_STATUS) ){
						valueContext.setValue(tableBean.getAllocatedApprover()+ " : " +tableBean.getRequestorComment() , Path.parse(path2));
						valueContext.setValue(now, Path.parse(path3));
					}
			}	
			
			else if(reqStatus.equalsIgnoreCase(DSMTConstants.COMPLETED) || 
					reqStatus.equalsIgnoreCase(DSMTConstants.PENDING_STATUS)){
				LOG.info("checking request status in comp or pending " +tableBean.getRequestStatus());
			// if request is completed. update status as completed and send mail to requestor	
				if (reqStatus.equalsIgnoreCase(DSMTConstants.COMPLETED)){
					
						WFMailNotification.sendMail('C',targetRecord,tableBean);
						valueContext.setValue(DSMTConstants.COMPLETED, GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
				}
				
				//if request is approved and pending to Merge updated status as Pending for Merge and send mail to requestor
				else if (reqStatus.equalsIgnoreCase(DSMTConstants.PENDING_STATUS)){
					
					WFMailNotification.sendMail('P',targetRecord,tableBean);

					valueContext.setValue(DSMTConstants.PENDING_STATUS, GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
				}
				if(level >=1){
					valueContext.setValue(tableBean.getAllocatedApprover()+ " : " +tableBean.getRequestorComment() , Path.parse(REVIEW_REQUEST_Comment_.concat(count)));
					valueContext.setValue(now, Path.parse(REVIEW_REQUEST_Action_Date_.concat(count)));
				}
				}
			// if request is rejected by approver. update status as Rejected and send mail to requestor
			else if(reqStatus.equalsIgnoreCase(GOCConstants.REJECTED)){
					valueContext.setValue(GOCConstants.REJECTED, GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
				if(level >=1 ){
					
					WFMailNotification.sendMail('R',targetRecord,tableBean);
					
					valueContext.setValue(tableBean.getAllocatedApprover()+ " : " +tableBean.getRequestorComment() , Path.parse(REVIEW_REQUEST_Comment_.concat(count)));
					valueContext.setValue(now, Path.parse(REVIEW_REQUEST_Action_Date_.concat(count)));
			}
			}
					
			
		}
		
		if (targetRecord == null) {
			 targetRecord = pContext.doCreateOccurrence(valueContext,  targetTable);
		} else {
			LOG.info("InsertMonitoringdata :  insertRecord inside else else");
			targetRecord = pContext.doModifyContent(targetRecord,  valueContext);
		}
		
		pContext.setTriggerActivation(true);
		pContext.setAllPrivileges(false);
		       
		LOG.info("InsertMonitoringdata :  insertRecord Ends ");
		
	}
	
	protected Adaptation getHistoryRecord(final Object requestID, final AdaptationTable targetTable){
		
		Adaptation targetRecord = null;
		final String predicate = 	DSMT2WFPaths._C_APP_WF_REPORTING._REQUEST_ID.format()  + " = \"" + requestID + "\"";
		LOG.info("printing predicate :- " +predicate );
		
		RequestResult reqRes = targetTable.createRequestResult(predicate);
		if (null != reqRes) {
			
			targetRecord = reqRes.nextAdaptation();
			reqRes.close();
			
		}
	
		return targetRecord;
	}
	
	private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}

}
