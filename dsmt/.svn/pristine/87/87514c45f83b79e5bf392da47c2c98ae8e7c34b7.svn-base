package com.citi.ebx.dsmt.trigger;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.HARPTableDuplication;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class NewHARPHirerarchyTrigger extends HierarchyTrigger {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	public String tablePath;
	public String columnId;
	public String dataSpace;
	public String dataSet;
	public String parentFk;
	public String deleteIdPost;
	private static final String MAX_END_DATE = "9999-12-31";
	private String endDateField = "./ENDDT";
	
	//private Path DESCR = Path.parse("./DESCR");
	//private Path DESCRSHORT = Path.parse("./DESCRSHORT");

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		LOG.info("inside New HARP Hirerarchy Trigger handelAfterCreate");
		final Adaptation newRec = context.getAdaptationOccurrence();
		final ProcedureContext pContext = context.getProcedureContext();
		String currentDataSet = newRec.getContainerTable().toString();
		if (currentDataSet.contains(dataSet)) {
			Repository repo = context.getAdaptationHome().getRepository();
			Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
					dataSpace, dataSet);
			final AdaptationTable targetTable = metadataDataSet
					.getTable(getPath(tablePath));
			String keyValue1 = newRec.getString(getPath(columnId));
			final PrimaryKey pk = targetTable
					.computePrimaryKey(new String[] { keyValue1 });

			Adaptation targetRecord = targetTable
					.lookupAdaptationByPrimaryKey(pk);

			final ValueContextForUpdate vc;
			String fkValue=null;
			if (targetRecord == null) {
				vc = pContext.getContextForNewOccurrence(newRec,targetTable);
				vc.setValue(keyValue1, getPath(columnId));
			} else {
				vc = pContext.getContextForNewOccurrence(newRec,targetTable);
			}
			if(null!=parentFk){
				if(parentFk.contains("FK")||(parentFk.contains("LVID_PARENT")))
				{	
					String oldFkValue =newRec.getString(getPath(parentFk));
					
					if(oldFkValue!=null){
						
						fkValue =oldFkValue.substring(oldFkValue.indexOf("|")+1,oldFkValue.lastIndexOf("|"));
						}
				}
				vc.setValue(fkValue,getPath(parentFk));
			}
			if (targetRecord == null) {
				targetRecord = pContext.doCreateOccurrence(vc, targetTable);
			} else {
				targetRecord = pContext.doModifyContent(targetRecord, vc);
			}
			} else {
			try {
				HARPTableDuplication HARPtableDuplication = new HARPTableDuplication(
						newRec, context.getAdaptationHome().getRepository(),
						tablePath, columnId, dataSpace, dataSet,parentFk);
				Utils.executeProcedure(
						HARPtableDuplication,
						context.getSession(),
						context.getAdaptationHome().getRepository()
								.lookupHome(HomeKey.forBranchName(dataSpace)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		pContext.setAllPrivileges(false);
		super.handleAfterCreate(context);
	}
	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context)
			throws OperationException {
		LOG.info("inside New HARP Hirerarchy Trigger handleAfterModify");
		final Adaptation newRec = context.getAdaptationOccurrence();

		final ProcedureContext pContext = context.getProcedureContext();
		String currentDataSet = newRec.getContainerTable().toString();
		final SimpleDateFormat dateFormat = new SimpleDateFormat(DSMTConstants.DEFAULT_DATE_FORMAT);
		final Date endDate = newRec.getDate(Path.parse(endDateField));
		String dateStr = dateFormat.format(endDate);
		if (endDate == null || MAX_END_DATE.equals(dateStr)) {
		if (currentDataSet.contains(dataSet)) {
			Repository repo = context.getAdaptationHome().getRepository();
			Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
					dataSpace, dataSet);
			final AdaptationTable targetTable = metadataDataSet
					.getTable(getPath(tablePath));
			String keyValue1 = newRec.getString(getPath(columnId));
			final PrimaryKey pk = targetTable
					.computePrimaryKey(new String[] { keyValue1 });

			Adaptation targetRecord = targetTable
					.lookupAdaptationByPrimaryKey(pk);

			final ValueContextForUpdate vc;
			String fkValue=null;
			
			vc = pContext.getContextForNewOccurrence(newRec,targetTable);
			vc.setValue(keyValue1, getPath(columnId));

			if(null!=parentFk){
				if(parentFk.contains("FK")||(parentFk.contains("LVID_PARENT")))
				{	
					String oldFkValue =newRec.getString(getPath(parentFk));		
					if(oldFkValue!=null){
						
						fkValue =oldFkValue.substring(oldFkValue.indexOf("|")+1,oldFkValue.lastIndexOf("|"));
						}
				}
				vc.setValue(fkValue,getPath(parentFk));
			}
			if (targetRecord == null) {
				targetRecord = pContext.doCreateOccurrence(vc, targetTable);
			} else {
				targetRecord = pContext.doModifyContent(targetRecord, vc);
			}
		} else {
			try {
				HARPTableDuplication HARPtableDuplication = new HARPTableDuplication(
						newRec, context.getAdaptationHome().getRepository(),
						tablePath, columnId, dataSpace, dataSet,parentFk);
				Utils.executeProcedure(
						HARPtableDuplication,
						context.getSession(),
						context.getAdaptationHome().getRepository()
								.lookupHome(HomeKey.forBranchName(dataSpace)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		pContext.setAllPrivileges(false);
		super.handleAfterModify(context);
	}
	}
	
	@Override
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
	//	 deletedPK = context.getAdaptationOccurrence().getOccurrencePrimaryKey();
	}

	public void handleAfterDelete(AfterDeleteOccurrenceContext context)
			throws OperationException {
		LOG.info("NewHARPHirerarchyTrigger : inside handleAfter delete");
		Repository repo = context.getAdaptationHome().getRepository();
		String currentDataSet = context.getTable().getContainerAdaptation().toString();

		final AdaptationHome gocDataSpace = repo.lookupHome(
				HomeKey.forBranchName(dataSpace));
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
				dataSpace, dataSet);
		AdaptationTable targetTable = metadataDataSet
				.getTable(getPath(tablePath));
		
		final String rec = context.getDeletedRecordXPathExpression();
		String[] sToken = rec.split("\\'");
		String mapId = null;
		LOG.info("rec :: "+rec);
		/*
		if(rec.contains("MLE")||rec.contains("CLE"))
		{
			mapId=sToken[1];
		}
		else{
			if (sToken != null && sToken.length > 4) {
				mapId = sToken[3];
			}	
		}*/
		if(deleteIdPost!=null){
			mapId=sToken[Integer.parseInt(deleteIdPost)];
		}
		else{
			LOG.info("NewHARPHirerarchyTrigger -->handleAfterDelete -->deleteIdPost  is not set in trigger parameter");
		}
		LOG.info("mapId"+mapId);
		
		ProcedureContext procCtx = context.getProcedureContext();
		procCtx.setAllPrivileges(true);
	
		String[] pkey = new String[] {mapId};		
		PrimaryKey pk = targetTable.computePrimaryKey(pkey);
		Adaptation record = targetTable.lookupAdaptationByPrimaryKey(pk);
	
			if(null!=record){
				// call procedure to udpate record
				if (currentDataSet.contains(dataSet)) {
					procCtx.doDelete(record.getAdaptationName(), false);
				} else{
					final DeleteRecordFromChildTablesProcedure copyProc = new DeleteRecordFromChildTablesProcedure(targetTable, record);
					Utils.executeProcedure(copyProc, context.getSession(), gocDataSpace);
				}
				
				
				
		}
		
	}
	
	private class DeleteRecordFromChildTablesProcedure implements Procedure {
		private AdaptationTable table;
		private Adaptation record;
		
		public DeleteRecordFromChildTablesProcedure(AdaptationTable table,Adaptation record) {
			this.table = table;
			this.record= record;
		}
		//GOCConstants.DSMT2_RELATIONAL_DATASET
		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			pContext.doDelete(record.getAdaptationName(), false);
			
		}
	}

	public String getTablePath() {
		return tablePath;
	}

	public void setTablePath(String tablePath) {
		this.tablePath = tablePath;
	}

	public String getColumnId() {
		return columnId;
	}

	public void setColumnId(String columnId) {
		this.columnId = columnId;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	private Path getPath(final String fieldName) {

		return Path.parse(fieldName);
	}
	public String getParentFk() {
		return parentFk;
	}

	public void setParentFk(String parentFk) {
		this.parentFk = parentFk;
	}
	public String getDeleteIdPost() {
		return deleteIdPost;
	}
	public void setDeleteIdPost(String deleteIdPost) {
		this.deleteIdPost = deleteIdPost;
	}


}
