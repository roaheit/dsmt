package com.citi.ebx.goc.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.management.OperationsException;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

import org.apache.commons.lang.StringUtils;

public class IGWGOCWorkflowUtils {
	
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	
	
	
	public static String getCurrentSID(final Adaptation dataSet) {
		return dataSet.getString(GOCPaths._CurrentSID);
	}
	
	/*
	public static String getFunctionalID(final Adaptation dataSet) {
		return dataSet.getString(GOCPaths._FunctionalID);
	}
	*/
	
	public static String getMaintenanceType(final Adaptation dataSet) {
		return dataSet.getString(GOCPaths._MaintenanceType);
	}
	
	public static boolean isValidateGOC(final Adaptation dataSet) {
		return dataSet.get_boolean(GOCPaths._ValidateGOC);
	}
	public static String getManagementOnly(final Adaptation dataSet) {
		return dataSet.getString(GOCPaths._ManagementOnly);
	}
	public static String getWorkflowStatus(final Adaptation dataSet) {
		return dataSet.getString(GOCPaths._WorkflowStatus);
	}
	/**
	 * 
	 * @param workflowParameterMap
	 * @param dataSet
	 * @param pContext
	 * @throws OperationException
	 */
	public static void setWorkflowParameters(final Map<Path, String> workflowParameterMap, final Adaptation dataSet,
			final ProcedureContext pContext) throws OperationException {
		final ValueContextForUpdate vc = pContext.getContext(
				dataSet.getAdaptationName());
		
		for(Path key:workflowParameterMap.keySet()){
			vc.setValue(workflowParameterMap.get(key),key);	
		}
		
		
		
		if(null != workflowParameterMap){
			LOG.info("Set workflow Parameters" +  workflowParameterMap  + " to " + dataSet.getAdaptationName().getStringName());
		}
		
		pContext.setAllPrivileges(true);
		pContext.doModifyContent(dataSet, vc);
		pContext.setAllPrivileges(false);
	}
	
	public static boolean isSidMatching(
			ValueContext defineGoc, Adaptation dataSet) {
		String currentSid = getCurrentSID(dataSet);

		if (currentSid == null || "".equals(currentSid)) {
			// Not in WF, do not check
			return true;
		}

		Adaptation sidRecord = Utils.getLinkedRecord(defineGoc,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);

		if (sidRecord == null) {
			return false;
		}
		if (currentSid
				.equals(sidRecord
						.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID))) {
			return true;
		}
		return false;
	}
	
	public static Adaptation getMetadataDataSet(final Repository repo)
			throws OperationException {
		final AdaptationHome metadataDataSpace = repo.lookupHome(
				HomeKey.forBranchName(GOCConstants.METADATA_DATA_SPACE));
		if (metadataDataSpace == null) {
			throw OperationException.createError("Metadata data space \"" + GOCConstants.METADATA_DATA_SPACE + "\" not found.");
		}
		final Adaptation metadataDataSet = metadataDataSpace.findAdaptationOrNull(
				AdaptationName.forName(GOCConstants.METADATA_DATA_SET));
		if (metadataDataSet == null) {
			throw OperationException.createError("Metadata data set \"" + GOCConstants.METADATA_DATA_SET + "\" not found.");
		}
		return metadataDataSet;
	}
	public static Adaptation getMetadataDataSetGOCPartnerRefData(final Repository repo)
			throws OperationException {
		final AdaptationHome metadataDataSpace = repo.lookupHome(
				HomeKey.forBranchName(GOCConstants.GOC_REF_DATA_SPACE));
		if (metadataDataSpace == null) {
			throw OperationException.createError("Metadata data space \"" + GOCConstants.METADATA_DATA_SPACE + "\" not found.");
		}
		final Adaptation metadataDataSet = metadataDataSpace.findAdaptationOrNull(
				AdaptationName.forName(GOCConstants.GOC_REF_METADATA_DATA_SET));
		if (metadataDataSet == null) {
			throw OperationException.createError("Metadata data set \"" + GOCConstants.METADATA_DATA_SET + "\" not found.");
		}
		return metadataDataSet;
	}
	
	public static AdaptationTable getTableInInitialSnapshot(final AdaptationTable table) {
		final Adaptation dataSet = table.getContainerAdaptation();
		final AdaptationHome dataSpace = dataSet.getHome();
		
		final AdaptationHome initialSnapshot = dataSpace.getParent();
		final Adaptation initialDataSet = initialSnapshot.findAdaptationOrNull(
				dataSet.getAdaptationName());
		return initialDataSet.getTable(table.getTablePath());
	}
	
	// This just returns first of month of the current month,
	// but in future may need to do a lookup in a calendar
	public static final Date calculateGOCEffectiveDateOld(final Adaptation gocRecord) {
		final Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}
	
	public static final Date calculateGOCEffectiveDate(final Adaptation dataSet, final ProcedureContext pContext) {
		final Calendar cal = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		final String  currentSid= getCurrentSID(dataSet);
		final String  maintance_Type= getMaintenanceType(dataSet);
		final Adaptation ccRecord=getCalenderDatefromDaterange(pContext.getAdaptationHome().getRepository(), currentSid, maintance_Type);
		Date calendarDate =null;
		
		
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		
		if(null == ccRecord){
			return cal.getTime();
		}
		else{
			String calendertype= ccRecord.getString(GOCWFPaths._C_DSMT_CAL._CAL_TYPE_ID);
			if("1".equalsIgnoreCase(calendertype)){
				if("functional".equals(maintance_Type))
				{
					calendarDate = ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._FNL_END_DT);
				}else
				{
					calendarDate = ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._REG_END_DT);	
				}
			}
			else if("2".equalsIgnoreCase(calendertype)){
				if("functional".equals(maintance_Type))
				{
					calendarDate = ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._FNL_END_DT);
				}
				else{
					String calanderPeriod = String.valueOf(ccRecord.get_int(GOCWFPaths._C_DSMT_CAL._PERIOD));
					cal.set(Calendar.MONTH, Integer.parseInt(calanderPeriod)-1);
					return cal.getTime();
				}
				
			}
			
		}
		
			
			
			
			if(  null != calendarDate && cal2.getTime().compareTo(calendarDate)>0){
				LOG.debug("cal is after calendarDate");
				cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH)+1));
				
	    	}
    	
		
		LOG.debug("currentSid : "+currentSid +" maintance_Type : "+ maintance_Type + " new eff date " +cal.getTime() );
		
		return cal.getTime();
	}
	
	
	
	public static String checkFunctionEndDate(final Adaptation dataSet, Repository repo) {
		final Calendar cal = Calendar.getInstance();
		final String  currentSid= getCurrentSID(dataSet);
		final String  maintance_Type= getMaintenanceType(dataSet);
		String isvalidRequest ="true";
		//to check for current Data
		cal.add(Calendar.DATE, -1);
		if(maintance_Type.equals(DSMTConstants.FUNCTIONAL))	{
				final Date functionalEndDate=(Date)getCalenderDate(repo, currentSid, maintance_Type).get(0);		
				if(null == functionalEndDate){
					return "true";
				}
				
				// if (!(cal.getTime().before(calendarDate)
					//		&& cal.getTime().after(calendarDate)))
					 if( cal.getTime().compareTo(functionalEndDate)>=0){
					LOG.info("checkFunctionEndDate : sysdate date is not in functional Date Range.");
					SimpleDateFormat sfd= new SimpleDateFormat("yyyy-MM-dd");
					 String date = sfd.format(functionalEndDate);
					isvalidRequest=date;
					
		    	}
				
				LOG.info("checkFunctionEndDate >>> currentSid : "+currentSid +" maintance_Type : "+ maintance_Type + " new eff date " +functionalEndDate );
				}
				
		return isvalidRequest;
	}
	
	
	
/*	public static String getLocalCostCode(final ValueContext vc) {
		final String glLCC = (String) vc.getValue(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GLLocalCostCode);
		if (glLCC == null || "".equals(glLCC)) {
			final String reLCC = (String) vc.getValue(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._RELocalCostCode);
			if (reLCC == null || "".equals(reLCC)) {
				final String p2pLCC = (String) vc.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._P2PLocalCostCode);
				if (p2pLCC == null || "".equals(p2pLCC)) {
					return null;
				}
				return p2pLCC;
			}
			return reLCC;
		}
		return glLCC;
	}*/

	private IGWGOCWorkflowUtils() {
	}
	
	
	
	public static List<String> isValidSID(Repository repo, String sid)
	{
		
		final Adaptation metadataDataSet ;
		List<String> outputList= new ArrayList<String>();
		 
		String isValidSID ="false";
		
		try {
			
			metadataDataSet = IGWGOCWorkflowUtils.getMetadataDataSet(repo);
			outputList= getCalenderTypeId (metadataDataSet,sid);
			if(null!=outputList){
				String calenderType_ID=outputList.get(0);
				if(calenderType_ID!=null)
				{
					
					isValidSID= getCalenderDataFull(metadataDataSet, calenderType_ID);
					//isValidSID= "true";
				}
				outputList.set(0, isValidSID);
			}
			
			
			
			
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
		
		}
		
		return outputList;
		
		
}
	
	
	
	public static List<String> isValidFunctionID(Repository repo, String functionid)
	{
		
		final Adaptation metadataDataSet ;
		List<String> outputList= new ArrayList<String>();
		 
		try {
			
			metadataDataSet = IGWGOCWorkflowUtils.getMetadataDataSet(repo);
			outputList= getFunctionalId (metadataDataSet,functionid);
				
			
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
		
		}
		
		return outputList;
		
		
}

	
	private static List<String> getFunctionalId(final Adaptation metadataDataSet, 
			final String functionid) {
			
		/*final AdaptationTable ccTable =	metadataDataSet.getTable(
				GOCWFPaths._FUNCTIONAL_ID_VALIDATION.getPathInSchema());
		final String predicate = GOCWFPaths._FUNCTIONAL_ID_VALIDATION._FUNCTIONAL_ID.format()
				+ "=\"" + functionid + "\" ";
		
		//System.out.println("inside getFunctionalId Type id 1 "+predicate);
		LOG.info("predicate to check if an Active Function ID exists in the Function ID table - " + predicate);
		final RequestResult reqRes = ccTable.createRequestResult(predicate);
		List<String> outputList= new ArrayList<String>();
		final Adaptation ccRecord;
		try {
			ccRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (null == ccRecord ) {
			outputList.add("false");
		}else{
			
			//getCalenderDataFull(metadataDataSet,"1");
			outputList.add(getCalenderDataFull(metadataDataSet,"1"));
		}
		
		*/
		List<String> outputList= new ArrayList<String>();
		outputList.add(getCalenderDataFull(metadataDataSet,"1"));
		
		return outputList;
	}
	
	
	
	

	public static List<Object> getCalenderDate(Repository repo, String sid, String maintance_Type)
	{
		
		final Adaptation metadataDataSet ;
		 Calendar cal = new GregorianCalendar();
		
		 int period=(cal.get(Calendar.MONTH) + 1);
		 int year =cal.get(Calendar.YEAR); 
		 Date endDate =null ;
		 List<Object> outputList= new ArrayList<Object>();

		 
	//	 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 

		
		try {
			
			metadataDataSet = IGWGOCWorkflowUtils.getMetadataDataSet(repo);
			List<String>outLst= getCalenderTypeId (metadataDataSet,sid);
			//String calenderType_ID= outLst.get(0);
			if(null != outLst && "2".equals(outLst.get(0)) && "regular".equals(maintance_Type)){
				 endDate= getCalenderEndDateregular(outLst.get(0),Integer.toString(year),metadataDataSet ,maintance_Type);	
			}
			else{
				if(null==outLst){
					 //endDate=getCalendarDate(null,Integer.toString(year),Integer.toString(period),metadataDataSet ,maintance_Type);
					 return null;
					
				}
				
				 else{
					 endDate=getCalendarDate(outLst.get(0),Integer.toString(year),Integer.toString(period),metadataDataSet ,maintance_Type);
				 }
			}
			 
				/*if(endDate!=null)
				{
					
					calenderDate = formatter.format(endDate);
					//long dateDiff= endDate.getTime() -cal.getTimeInMillis();
					 // dateDiffDays=dateDiff/(24 * 60 * 60 * 1000);
				
				}*/
			
			outputList.add(endDate);
			outputList.add(outLst.get(1));
			
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
		
		}
		
		return outputList;
		
	}
	public static String getCalenderDataFull(Adaptation metadataDataSet,  String calenderType_ID)
	{
		
		
		 Calendar cal = new GregorianCalendar();
		
		 int period=(cal.get(Calendar.MONTH) + 1);
		 int year =cal.get(Calendar.YEAR); 
		 List<Date> endDate ;
		 String isValidDate="true";
		 
	//	 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 

		
		try {
			
			
			
			
			 endDate=getCalendarDataWildCard(calenderType_ID,Integer.toString(year),Integer.toString(period),metadataDataSet );
			 
			 if(endDate!=null)
				{
				 // datevalue = formatter.format(endDate);
				 Calendar calender = Calendar.getInstance();
				 if (calender.getTime().before(endDate.get(0))
							&& calender.getTime().after(endDate.get(1))) {
						//ok everything is fine, date in range
						LOG.info("date is in the range");
						
					} else {
							LOG.info("date is out of range");
							isValidDate="error";
						
					}
				 
				}
			 else{
				 isValidDate="error";
			 }
				
		} catch (Exception ex) {
			LOG.error("Error looking up metadata data set", ex);
		
		}
		
		return isValidDate;
		
	}

	private static List<String> getCalenderTypeId(final Adaptation metadataDataSet, 
			final String sid) {
			
		int calenderType =1;
		String managementOnly= "false";
		List<String> outputList= new ArrayList<String>();

		/*final AdaptationTable ccTable =	metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
		final String predicate = GOCWFPaths._C_DSMT_SID_ENH._SID.format()
				+ "=\"" + sid + "\" and " + GOCWFPaths._C_DSMT_SID_ENH._EFF_STATUS.format() + "=\"A\"";
		
		LOG.debug("predicate to check if an Active SID exists in the SID enhancement table - " + predicate);
		final RequestResult reqRes = ccTable.createRequestResult(predicate);
		final Adaptation ccRecord;
		try {
			ccRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (null == ccRecord ) {
			
			if(null != sid){
			LOG.error("No record found for predicate " + predicate
					+ " in table " + ccTable.getTablePath().format());
			final AdaptationTable sidBUReferenceTable = metadataDataSet.getTable(
					GOCWFPaths._C_DSMT_SID_BU_REF.getPathInSchema());
			final String prd = GOCWFPaths._C_DSMT_SID_BU_REF._SID.format()+ "=\"" + sid + "\"" +
					" and " +endDatePredicate(GOCWFPaths._C_DSMT_SID_BU_REF._ENDDT.format()) + 
					" and " + GOCWFPaths._C_DSMT_SID_BU_REF._EFF_STATUS.format() +  "=\"A\"";
			final RequestResult result = sidBUReferenceTable.createRequestResult(prd);
			final Adaptation record;
			try {
				record = result.nextAdaptation();
			} finally {
				result.close();
			}
			if(null!=record){
				managementOnly="true";
				outputList.add("1");
				outputList.add(managementOnly);
				// For IGW: incase of request being of management only SID the calendar type should be defaulted to 1. 
				return outputList;
				
			}
			else{
				LOG.error("No record found for predicate " + prd
						+ " in table " + sidBUReferenceTable.getTablePath().format());
				return null;
			}
			
			}else{
				System.out.println("inside getCalender Type id 5");
				outputList.add("1");
				outputList.add(managementOnly);
				return outputList;

			
			}
			
			
		}
		
		calenderType = ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._CalendarType);
		LOG.debug("CalendarType for SID: "+ sid + "   is : "+ calenderType); 
		
		 set default value to 1 
		if (calenderType != 1 && calenderType !=2 ) {
			LOG.debug("CalendarType for SID is defaulted to 1"); 
			calenderType = 1;
		}*/
		outputList.add(Integer.toString(calenderType));
		outputList.add(managementOnly);
		
		return outputList;
	}
	private static Date getCalendarDate(
			final String calendertType, final String year, final String period,
			final Adaptation metadataDataSet, final String maintance_Type ) {
		final Date endDate;
		String predicate ;
//		final Date currDate = new Date();
		
		final AdaptationTable ccTable =	metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_CAL.getPathInSchema());
		
			predicate = GOCWFPaths._C_DSMT_CAL._CAL_TYPE_ID.format()
				+ "=\"" + calendertType + "\" and "
				+ GOCWFPaths._C_DSMT_CAL._YEAR.format()
				+ "=\"" + year + "\" and "
				+ GOCWFPaths._C_DSMT_CAL._PERIOD.format()
				+ "=\"" + period + "\"";
		
		//enable below code for managment only end date check for regular request
				/*else
				{
					endDatePredicate= regularDatePredicate(cal.getTime());	
				}*/
				
				
				
				LOG.info("::::::: fetch calender data pridicate ::::::::::::: "+ predicate);
		final RequestResult reqRes = ccTable.createRequestResult(predicate);
		final Adaptation ccRecord;
		try {
			ccRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (ccRecord == null ) {

			if(null !=  calendertType){
				LOG.error("No record found for predicate " + predicate
					+ " in table " + ccTable.getTablePath().format());
			}
			return null;
		}
		if(maintance_Type.equals(DSMTConstants.FUNCTIONAL))
		{
			endDate = ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._FNL_END_DT);
		}else
		{
			 endDate = ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._REG_END_DT);	
		}
		
		//ccRecord.getString(GOCWFPaths._C_DSMT_CAL._REG_END_DT);
		
		LOG.debug("End Date for maintance_Type : "+ maintance_Type +"   is : "+ endDate ); 
		
		return endDate;
	}
	
	private static List<Date> getCalendarDataWildCard(
			final String calendertType, final String year, final String period,
			final Adaptation metadataDataSet) {
		
//		final Date currDate = new Date();
		List<Date> calenderdates= new ArrayList<Date>();
		final AdaptationTable ccTable =	metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_CAL.getPathInSchema());
		final String predicate = GOCWFPaths._C_DSMT_CAL._CAL_TYPE_ID.format()
				+ "=\"" + calendertType + "\" and "
				+ GOCWFPaths._C_DSMT_CAL._YEAR.format()
				+ "=\"" + year + "\" and "
				+ GOCWFPaths._C_DSMT_CAL._PERIOD.format()
				+ "=\"" + period + "\"";
		final RequestResult reqRes = ccTable.createRequestResult(predicate);
		final Adaptation ccRecord;
		try {
			ccRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (ccRecord == null) {
			if(null != calendertType){
			LOG.error("No record found for predicate " + predicate
					+ " in table " + ccTable.getTablePath().format());
			}
			return null;
		}else{
			
			calenderdates.add(ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._FNL_END_DT));
			calenderdates.add(ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._FNL_START_DT));
			calenderdates.add(ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._REG_END_DT));
			calenderdates.add(ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._REG_START_DT));
			
		}
		
		//ccRecord.getString(GOCWFPaths._C_DSMT_CAL._REG_END_DT);
		
		LOG.info("End Date for maintance_Type   is : "+ calenderdates ); 
		
		return calenderdates;
	}
	
	public static String getMaintenanceOrder(Repository repo, String sid)
	{
		
		final Adaptation metadataDataSet ;
		String maintenanceOrder =DSMTConstants.MAINTENANCEORDER;		
		int seq=0;
		
		Map<String , Integer> orginalMap = new LinkedHashMap<String, Integer>();
		
		try {
			metadataDataSet = IGWGOCWorkflowUtils.getMetadataDataSet(repo);
			final AdaptationTable ccTable =	metadataDataSet.getTable(
					GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
			final String predicate = GOCWFPaths._C_DSMT_SID_ENH._SID.format()
					+ "=\"" + sid + "\"";
			final RequestResult reqRes = ccTable.createRequestResult(predicate);
			final Adaptation ccRecord;
			try {
				ccRecord = reqRes.nextAdaptation();
			} finally {
				reqRes.close();
			}
			if (ccRecord == null) {
				if(null != sid){
				LOG.error("No record found for predicate " + predicate
						+ " in table " + ccTable.getTablePath().format());
				}
			
			}else{
					if(ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._GLMaintenanceOrder)==0){
					orginalMap.put(DSMTConstants.GLSYSTEM, 3);
					seq++;
						}else{
							orginalMap.put(DSMTConstants.GLSYSTEM, ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._GLMaintenanceOrder));
						}
					if(ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._RptEngineMaintenanceOrder)==0){
						orginalMap.put(DSMTConstants.REPSYSTEM, 3);
						}else{
							orginalMap.put(DSMTConstants.REPSYSTEM, ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._RptEngineMaintenanceOrder));
						}
					if(ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._P2PMaintenanceOrder)==0){
						orginalMap.put(DSMTConstants.P2PSYSTEM, 3);
						}else{
							orginalMap.put(DSMTConstants.P2PSYSTEM, ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._P2PMaintenanceOrder));
						}
					
			}
			if(seq!=3){
				maintenanceOrder= generateSeq(orginalMap);
			}
			
			
			
			
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
		
		}
		LOG.debug(" maintenanceOrder >> "+ maintenanceOrder);
		return maintenanceOrder;
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static  String generateSeq(Map<String , Integer> orginalMap ){
		
		String maintenanceOrder = DSMTConstants.MAINTENANCEORDER;
		
		List<LinkedList<Entry<String, Integer>>> list = new LinkedList(orginalMap.entrySet());
		Collections.sort(list, new Comparator() {
			
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue())
                                       .compareTo(((Map.Entry) (o2)).getValue());
			}
		});
		
		
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey().toString(), (Integer)entry.getValue());
		}
		StringBuffer maintenaceseq =new StringBuffer() ;
		for (Map.Entry entry : sortedMap.entrySet()) {
			LOG.debug("Key : " + entry.getKey() 
                                   + " Value : " + entry.getValue());
			maintenaceseq.append(entry.getKey());
			maintenaceseq.append("_");
		}
		
		try {
			maintenanceOrder = maintenaceseq.substring(0, 9);
		} catch (Exception e) {
			LOG.error(e +  " caught while trying to create maintenance order string. Default value will be set GL_P2P_RE.");
		}
		return maintenanceOrder;
	}


	public static Adaptation getCalenderDatefromDaterange(Repository repo, String sid, String maintance_Type)
	{
		
		final Adaptation metadataDataSet ;
		 Calendar cal = new GregorianCalendar();
		
		 int period=(cal.get(Calendar.MONTH) + 1);
		 int year =cal.get(Calendar.YEAR); 
		//  Date endDate =null ;
		  Adaptation resultSet =null ;
		
		try {
			
			metadataDataSet = IGWGOCWorkflowUtils.getMetadataDataSet(repo);
			String calenderType_ID= getCalenderTypeId (metadataDataSet,sid).get(0);
			
			resultSet = getCalendarDatefromDateRange(calenderType_ID,Integer.toString(year),Integer.toString(period),metadataDataSet ,maintance_Type);
			
			
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
		
		}
		catch (Exception ex) {
			LOG.error("Exception caught", ex);
		
		}
		return resultSet;
		
	}
	
	private static Adaptation getCalendarDatefromDateRange(
			final String calendertType, final String year, final String period,
			final Adaptation metadataDataSet, final String maintance_Type) {
	//	final Date endDate;
//		final Date currDate = new Date();
		 Calendar cal = Calendar.getInstance();
		final AdaptationTable ccTable =	metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_CAL.getPathInSchema());
		String endDatePredicate = null;
		final String predicate ;
		
		if("functional".equals(maintance_Type))
		{
			endDatePredicate= functionalDatePredicate(cal.getTime());
		}
		else
		{
			endDatePredicate= regularDatePredicate(cal.getTime());	
		}
		
		if("1".equals(calendertType)){
					predicate = GOCWFPaths._C_DSMT_CAL._CAL_TYPE_ID.format()
						+ "=\"" + calendertType + "\" and "
						+ GOCWFPaths._C_DSMT_CAL._YEAR.format()
						+ "=\"" + year + "\" and "
						+ GOCWFPaths._C_DSMT_CAL._PERIOD.format()
						+ "=\"" + period + "\"";
		}
		else {
				predicate = GOCWFPaths._C_DSMT_CAL._CAL_TYPE_ID.format()
					+ "=\"" + calendertType + "\" and "
					+ GOCWFPaths._C_DSMT_CAL._YEAR.format()
					+ "=\"" + year + "\" and "
					+ endDatePredicate ;
		
		
			
		}
		
		LOG.debug("predicate to search calendar data - " + predicate);
		final RequestResult reqRes = ccTable.createRequestResult(predicate);
		final Adaptation ccRecord;
		try {
			ccRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (ccRecord == null) {
			
			if(null != calendertType){
			LOG.error("No record found for predicate " + predicate
					+ " in table " + ccTable.getTablePath().format());
			}
			return null;
		}
		LOG.debug("Calender period for maintance_Type : "+ maintance_Type  +"   is : "+ ccRecord.get(GOCWFPaths._C_DSMT_CAL._PERIOD) );
	
		
		return ccRecord;
	}
	
private static String functionalDatePredicate( Date sysdate){
	
		final String date = DSMTUtils.formatDate(sysdate, DSMTConstants.DEFAULT_DATETIME_FORMAT);
		String predicate = " ( date-less-than("+  GOCWFPaths._C_DSMT_CAL._FNL_START_DT.format() + ",'" + date+ "') and date-greater-than("+  GOCWFPaths._C_DSMT_CAL._FNL_END_DT.format() + ",'" + date+ "')) ";
		
		return predicate; 
	}


public static void main(String[] args) {
	
//	System.out.println(functionalDatePredicate(Calendar.getInstance().getTime()));
}

private static String regularDatePredicate( Date sysdate ){
	
	final String date = DSMTUtils.formatDate(sysdate, DSMTConstants.DEFAULT_DATETIME_FORMAT);
	final String predicate = " ( date-less-than("+  GOCWFPaths._C_DSMT_CAL._REG_START_DT.format() + ",'" + date+ "') and date-greater-than("+  GOCWFPaths._C_DSMT_CAL._REG_END_DT.format() + ",'" + date+ "')) ";
	
	return predicate ;
}


public static Date getCalenderEndDateregular(
		final String calendertType, final String year,
		final Adaptation metadataDataSet, final String maintance_Type)
{
		final Date endDate;
//	final Date currDate = new Date();
	 Calendar cal = Calendar.getInstance();
	final AdaptationTable ccTable =	metadataDataSet.getTable(
			GOCWFPaths._C_DSMT_CAL.getPathInSchema());
	String endDatePredicate = regularDatePredicate(cal.getTime());	
	final String predicate ;

	predicate = GOCWFPaths._C_DSMT_CAL._CAL_TYPE_ID.format()
			+ "=\"" + calendertType + "\" and "
			+ GOCWFPaths._C_DSMT_CAL._YEAR.format()
			+ "=\"" + year + "\" and "
			+ endDatePredicate ;
	
	
	
	
	LOG.debug("getCalenderEndDateregular >> predicate to search calendar data - " + predicate);
	final RequestResult reqRes = ccTable.createRequestResult(predicate);
	final Adaptation ccRecord;
	try {
		ccRecord = reqRes.nextAdaptation();
	} finally {
		reqRes.close();
	}
	if (ccRecord == null) {
		
		if(null != calendertType){
		LOG.error("No record found for predicate " + predicate
				+ " in table " + ccTable.getTablePath().format());
		}
		return null;
	}
	LOG.debug("Calender period for maintance_Type : "+ maintance_Type  +"   is : "+ ccRecord.get(GOCWFPaths._C_DSMT_CAL._PERIOD) );

	endDate = ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._REG_END_DT);
	 return  endDate;	
}


	private static String endDatePredicate(String endDateField){
		
		
		return " ( osd:is-null(" + endDateField + ") or date-equal(" + endDateField + ", '9999-12-31') )";
	}

	
	public static String isManagmentOnlySID( Repository repo, 
			final String sid) {
			
		String managementOnly= "false";
		Path tablePath = GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema();
		Path [] tableColumns = new Path [10];
		tableColumns[0] = GOCWFPaths._C_DSMT_SID_ENH._SID;
		tableColumns[1] = GOCWFPaths._C_DSMT_SID_ENH._EFF_STATUS;
		String [] params = new String [10];
		params[0] = sid; params[1] = "A";
		Adaptation sidEHNRecord = null;
		try {
			sidEHNRecord = GOCWorkflowUtils.getQueryRecord(repo, tablePath, tableColumns, params);
		} catch (OperationException e) {
			LOG.error("error while retriving record from sid enhancement table");
		}
		
		if (null == sidEHNRecord ) {
			
			AdaptationTable sidBUReferenceTable = null;
			try {
				sidBUReferenceTable = GOCWorkflowUtils.getMetadataDataSet(repo).getTable(
						GOCWFPaths._C_DSMT_SID_BU_REF.getPathInSchema());
			} catch (PathAccessException e) {
				LOG.error("error while getting reference for sidBUReferenceTable from isManagmentOnlySID(");
				 e.printStackTrace();
			} catch (OperationException e) {
				LOG.error("error while getting reference for sidBUReferenceTable from isManagmentOnlySID(");
				e.printStackTrace();
				
			}
			final String prd = GOCWFPaths._C_DSMT_SID_BU_REF._SID.format()+ "=\"" + sid + "\"" +
					" and " +endDatePredicate(GOCWFPaths._C_DSMT_SID_BU_REF._ENDDT.format()) + 
					" and " + GOCWFPaths._C_DSMT_SID_BU_REF._EFF_STATUS.format() +  "=\"A\"";
			final RequestResult result = sidBUReferenceTable.createRequestResult(prd);
			final Adaptation sidBURecord;
			try {
				sidBURecord = result.nextAdaptation();
			} finally {
				result.close();
			}
			
			if(null!=sidBURecord){
				managementOnly = "true";
				LOG.info("managementOnly == "+managementOnly);
				return managementOnly;
			}else{
				LOG.error("No record found for predicate " + prd+ " in table " + sidBUReferenceTable.getTablePath().format());
				LOG.info("managementOnly == "+managementOnly);
				return managementOnly;
			}
			
		}else{
			LOG.info("managementOnly == "+managementOnly);
			return managementOnly;
		}
	}
	
	
	
	public static Adaptation getSIDEhnechmentData(
			final String sid, String bu, final Adaptation metadataDataSet) {
		
		final AdaptationTable sidEnhancementTable = metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
		final String predicate = GOCWFPaths._C_DSMT_SID_ENH._SID.format()
				+ "=\"" + sid + "\"";
		final RequestResult reqRes = sidEnhancementTable.createRequestResult(predicate);
		 Adaptation sidEnhancementRecord;
		try {
			sidEnhancementRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (sidEnhancementRecord == null) {
			LOG.error("No record found for predicate " + predicate
					+ " in table " + sidEnhancementTable.getTablePath().format());
			
			final AdaptationTable sidBUReferenceTable = metadataDataSet.getTable(
					GOCWFPaths._C_DSMT_SID_BU_REF.getPathInSchema());
			final String prd = GOCWFPaths._C_DSMT_SID_BU_REF._SID.format()+ "=\"" + sid + "\"" +
					" and "+ GOCWFPaths._C_DSMT_SID_BU_REF._C_DSMT_FRS_BU.format() + "=\"" + bu + "\"" + 
					" and " +endDatePredicate(GOCWFPaths._C_DSMT_SID_BU_REF._ENDDT.format()) + 
					" and " + GOCWFPaths._C_DSMT_SID_BU_REF._EFF_STATUS.format() +  "=\"A\"";
			final RequestResult result = sidBUReferenceTable.createRequestResult(prd);
			try {
				sidEnhancementRecord = result.nextAdaptation();
			} finally {
				result.close();
				
			}
			
		}
		return sidEnhancementRecord;
		
	}
}
