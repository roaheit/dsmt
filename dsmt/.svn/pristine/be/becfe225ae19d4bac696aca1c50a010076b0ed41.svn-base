package com.citi.ebx.dsmt.trigger;

import java.util.HashMap;
import java.util.StringTokenizer;

import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.Request;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.adaptation.RequestSortCriteria;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class ControllerIDPopulationTrigger extends TableTrigger {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private  String controllerTable;
	public String getControllerTable() {
		return controllerTable;
	}
	public void setControllerTable(String controllerTable) {
		this.controllerTable = controllerTable;
	}
	AdaptationTable targetTable = null;
	AdaptationTable initialTable = null;
	public static final Path population_flag = Path.parse("/Populate_Child");
	public static final Path acct_matrix_flag = Path.parse("/Acc_Matrix_flag");
	public static final Path lvidPath = Path.parse("/C_DSMT_LVID");
	public static final Path levelPath = Path.parse("/LEVEL");
	String oldControllerID = null;
	ValueContextForUpdate vc;
	public static final Path LVID_LEMTable = Path
			.parse("/root/ACC_MAT/LVID_LEM_Mapping");
	Adaptation newRec;
	Repository rp;
	Adaptation metadataDataSet;
	AdaptationTable lvidLEMTargetTable;
	
	@Override
	public void setup(TriggerSetupContext arg0) {
		// TODO Auto-generated method stub

	}
	
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		Adaptation dataSet=context.getTable().getContainerAdaptation();
		lvidLEMTargetTable = dataSet.getTable(LVID_LEMTable);
		newRec = context.getAdaptationOccurrence();
		ValueContext oldvc=context.getOccurrenceContextForUpdate();
		String oldController="";
			if(oldvc.getValue(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID)!=null)
			{
				oldController=oldvc.getValue(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID).toString();
			}
		String newController=newRec.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID);
		if(oldController!=newController || !newController.equals(oldController))
		{
			LOG.debug("Controller have changes : oldController " + oldController+ " newController "+ newController);
		}
		else{
			LOG.debug("Controller are same : oldController " + oldController+ " newController "+ newController);
		}
		targetTable = context.getTable();
		String lvidRecord = newRec.getString(lvidPath);
		oldControllerID=newRec.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID);
		String userID = context.getSession().getUserReference().getUserId()
				.toString();
		final String controllerPredicate ="osd:contains-case-insensitive("
				+ AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID
				.format()
				+ ",'"
					+ userID.toUpperCase()
				+ "')"
				+ " and "
				+ AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_LVID
						.format() + "= '" + lvidRecord + "'";

		final String lemPredicate = AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM
				.format()
				+ "= '"
				+ userID.toUpperCase()
				+ "' and "
				+ AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID
						.format() + "= '" + lvidRecord + "'";
		RequestResult resultSet1 = lvidLEMTargetTable
				.createRequestResult(lemPredicate);
		if (resultSet1.getSize() <= 0) {
			Request request = targetTable.createRequest();
			request.setXPathFilter(controllerPredicate);
			request.setSortCriteria(new RequestSortCriteria().add(levelPath));
			RequestResult resultSet = request.execute();
			int controllerlevel = 0;
			if (resultSet.getSize() > 0) {

				controllerlevel = resultSet.nextAdaptation().get_int(levelPath);
				int currentLevel = newRec.get_int(levelPath);
				if (controllerlevel == 0) {
					throw OperationException
							.createError("Controller has not a defined level");

				}
				if (currentLevel < controllerlevel) {
					throw OperationException
							.createError("Controller is not allowed to update above level controller");
				}
			}
			resultSet.close();
		}
		resultSet1.close();
		super.handleBeforeModify(context);

	}

	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context)
			throws OperationException {
		
		HashMap<String,String> originalControlMap=new HashMap<String, String>();
		String originalValue="";
		if(null!= context.getChanges().getChange(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID)){
		originalValue =(null==context.getChanges().getChange(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID).getValueBefore()?"":(String) context.getChanges().getChange(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID).getValueBefore().toString());
		
		//String originalValue =(String) context.getChanges().getChange(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID).getValueBefore();
		if(originalValue!="" || !originalValue.equals(""))
		{
		
			StringTokenizer mapToken=new StringTokenizer(originalValue,"/");
			while(mapToken.hasMoreTokens())
					{
						originalControlMap.put(mapToken.nextToken(), "");
					}
			}
		}
		
		newRec = context.getAdaptationOccurrence();
		HashMap<String, Adaptation> controlMap=new HashMap<String, Adaptation>();
		String controllerID = newRec
				.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID);
		String msID = newRec.getOccurrencePrimaryKey().format();
		String lvid = newRec
				.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_LVID);
		Adaptation dataSet=context.getTable().getContainerAdaptation();
		
		final Path controllerTablePath=Path.parse(controllerTable);
		AdaptationTable controllerTable=dataSet.getTable(controllerTablePath);
		boolean autoPopulate=true;
		autoPopulate = newRec.get_boolean(population_flag);
		if(null==controllerID && !originalControlMap.isEmpty())
		{
			controllerID=originalValue;
			LOG.info("Controller Value in Modify: "+controllerID);
		}
		if (autoPopulate && null != controllerID) {
			
			final String predicate2 = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID
						.format() + "='" + msID + "'";
			RequestResult reqRes=controllerTable.createRequestResult(predicate2);
			LOG.info(" handleAfterModify reqRes.getSize() "+ reqRes.getSize());
			if(reqRes.getSize()>0){
					
				for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
						
						String value =  record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID);
						LOG.info(" handleAfterModify controller name  "+ value );
						controlMap.put(value, record);
				}
			}
			
			getChildRecord(msID, controllerID, targetTable, context, controlMap,originalControlMap);
		}
		
		final String lvidlemPredicate = AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID
				.format() + "='" + lvid + "'";
		RequestResult rs = lvidLEMTargetTable
				.createRequestResult(lvidlemPredicate);
		if (rs.getSize() > 0) {
			for (Adaptation record; (record = rs.nextAdaptation()) != null;) {
				final ProcedureContext pContext = context.getProcedureContext();
				final ValueContextForUpdate vc = pContext.getContext(record
						.getAdaptationName());
				if (record.getString(acct_matrix_flag) == null) {
					pContext.setAllPrivileges(true);
					vc.setValue("Y", acct_matrix_flag);
					pContext.doModifyContent(record, vc);
				} else if (!record.getString(acct_matrix_flag).equals("Y")) {
					pContext.setAllPrivileges(true);
					vc.setValue("Y", acct_matrix_flag);
					pContext.doModifyContent(record, vc);
				}
			}
		}
		rs.close();
		
		
		super.handleAfterModify(context);
	}
//record -- msid which is from getChiild
	private void updateRecord(final Adaptation record,
			final ProcedureContext pContext, String controllerID,HashMap<String, Adaptation> controlerMap,HashMap<String, String> originalControlerMap)
			throws OperationException {
		
		ValueContextForUpdate valueContext;
		//update child records with controller ID
		final Path controllerTablePath=Path.parse(controllerTable);
		AdaptationTable controllerDetailsTable = record.getContainerTable().getContainerAdaptation().getTable(controllerTablePath);
		for(String controller: controlerMap.keySet()){
			
		final String controllerDetailsPredicate = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID
				.format() + "='" + record.getOccurrencePrimaryKey().format()
				+ "' and "
				+ "osd:contains-case-insensitive("
				+ AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID
				.format()
				+ ",'"
				+ controller
				+ "')";
		RequestResult res=controllerDetailsTable.createRequestResult(controllerDetailsPredicate);
		LOG.info("controller : value in updateRecord : " +controller);
		if(res.getSize()>0)
		{
			LOG.debug(" updateRecord: if result set is  greater than 0");
			Adaptation controllerRecord=res.nextAdaptation();
			valueContext=pContext.getContext(controllerRecord.getAdaptationName());
			valueContext.setValue(controller, AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID);
			valueContext.setValue(controlerMap.get(controller).getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Comments), AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Comments);
			valueContext.setValue(controlerMap.get(controller).getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Region), AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Region);
			valueContext.setValue("Y", AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Auto_Populated);
			pContext.doModifyContent(controllerRecord,  valueContext);	
		}
		else{
			LOG.debug(" updateRecord: if result set is  less than 0");
			valueContext = pContext.getContextForNewOccurrence(controllerDetailsTable);
			valueContext.setValue(record.getOccurrencePrimaryKey().format(), AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID);
			valueContext.setValue(controller, AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID);
			valueContext.setValue(controlerMap.get(controller).getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Comments), AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Comments);
			valueContext.setValue(controlerMap.get(controller).getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Region), AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Region);
			valueContext.setValue("Y", AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Auto_Populated);
			pContext.doCreateOccurrence(valueContext,  controllerDetailsTable);
		}
		if(!originalControlerMap.isEmpty()){
			for(String newController: controlerMap.keySet()){
				if(originalControlerMap.containsKey(controller))
					{
						originalControlerMap.remove(newController);
					}
				}
		}
			}
		if(!originalControlerMap.isEmpty()){
				for(String modifiedControllerOriginal : originalControlerMap.keySet() ){
				final String oldControllerDetailsPredicate = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID
								.format() + "='" + record.getOccurrencePrimaryKey().format()
								+ "' and "
								+ "osd:contains-case-insensitive("
								+ AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID
								.format()
								+ ",'"
								+ modifiedControllerOriginal
								+ "')";
						RequestResult oldRequestResult=controllerDetailsTable.createRequestResult(oldControllerDetailsPredicate);
						if(oldRequestResult.getSize()>0)
						{
							pContext.doDelete(oldRequestResult.nextAdaptation().getAdaptationName(), false);
						}
					}
	}
				/*try {
					vc.setValue(
							"Y",
							AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Auto_Populated);
					pContext.setTriggerActivation(false);
					pContext.doModifyContent(record, vc);
				} catch (Exception e) {
					e.printStackTrace();
				}*/
		pContext.setTriggerActivation(true);
	}

	public void getChildRecord(String msID, String controllerID,
			AdaptationTable targetTable, AfterModifyOccurrenceContext context,HashMap<String, Adaptation> controlerMap,HashMap<String, String> originalControlerMap)
			throws OperationException {

		final String predicate = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Parent_MS_ID
				.format() + "='" + msID + "'";
		RequestResult rs = targetTable.createRequestResult(predicate);	
		ProcedureContext pContext = context.getProcedureContext();
		pContext.setAllPrivileges(true);
		LOG.info("rs.getSize()  :  " + rs.getSize() + " for MSID  " + msID);
		if (rs.getSize() > 0) {
			for (Adaptation record; (record = rs.nextAdaptation()) != null;) {
				boolean skip=record.get_boolean(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Skip_Auto_Populate);
				LOG.info("_Skip_Auto_Populate value :   " + skip);
				if(!skip){
				updateRecord(record, pContext, controllerID,controlerMap,originalControlerMap);
				}
				getChildRecord(record.getOccurrencePrimaryKey().format(),
						controllerID, targetTable, context,controlerMap,originalControlerMap);
			}
		}
		rs.close();
	}
	
}
