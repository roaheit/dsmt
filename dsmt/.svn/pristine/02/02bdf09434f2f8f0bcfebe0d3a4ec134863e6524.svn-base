package com.citi.ebx.workflow;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.SchemaTypeName;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.ValueContextForUpdate;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class SetDataSetFieldScriptTask extends ScriptTaskBean {
	private String dataSpace;
	private String dataSet;
	private Path path;
	private String value;
	
	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getPath() {
		return path.format();
	}

	public void setPath(String path) {
		this.path = Path.parse(path);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
				HomeKey.forBranchName(dataSpace));
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(
				AdaptationName.forName(dataSet));
		
		final Procedure updateProc = new Procedure() {
			@Override
			public void execute(ProcedureContext pContext) throws Exception {
				final ValueContextForUpdate vc = pContext.getContext(
						dataSetRef.getAdaptationName());
				final SchemaNode node = dataSetRef.getSchemaNode().getNode(path);
				final SchemaTypeName type = node.getXsTypeName();
				if (SchemaTypeName.XS_STRING.equals(type)) {
					vc.setValue((String) value, path);
				} else if (SchemaTypeName.XS_BOOLEAN.equals(type)) {
					vc.setValue(Boolean.valueOf(value), path);
				} else if (SchemaTypeName.XS_DECIMAL.equals(type)){
					vc.setValue(new BigDecimal(value), path);
				} else if (SchemaTypeName.XS_INT.equals(type)
						|| SchemaTypeName.XS_INTEGER.equals(type)) {
					vc.setValue(Integer.valueOf(value), path);
				} else if (SchemaTypeName.XS_DATE.equals(type)) {
					final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					vc.setValue(dateFormat.parse(value), path);
				} else if (SchemaTypeName.XS_TIME.equals(type)) {
					final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
					vc.setValue(dateFormat.parse(value), path);
				} else if (SchemaTypeName.XS_DATETIME.equals(type)) {
					final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
					vc.setValue(dateFormat.parse(value), path);
				} else {
					throw OperationException.createError("Unhandled data type: " + type.getNameWithoutPrefix());
				}
				pContext.setAllPrivileges(true);
				pContext.doModifyContent(dataSetRef, vc);
				pContext.setAllPrivileges(false);
			}
		};
		
		final ProgrammaticService updateSvc = ProgrammaticService.createForSession(context.getSession(), dataSpaceRef);
		final ProcedureResult procResult = updateSvc.execute(updateProc);
		if (procResult.getException() != null) {
			throw procResult.getException();
		}
		
		// explicitely ValidateDataSetChangesScriptTask
		
			ValidateDataSetChangesScriptTask validateDataSetChangesScriptTask = new ValidateDataSetChangesScriptTask();
			validateDataSetChangesScriptTask.setDataSet(dataSet);
			validateDataSetChangesScriptTask.setDataSpace(dataSpace);
			validateDataSetChangesScriptTask.executeScript(context);
	
	}
}
