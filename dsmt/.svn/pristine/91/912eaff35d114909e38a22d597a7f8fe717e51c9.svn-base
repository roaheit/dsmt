package com.citi.ebx.util;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class UpdateManagerProcedure implements Procedure {

	private Adaptation record;
	private String controllerID;
	private AdaptationTable table;
	private String manSegID;
	private boolean deletion;
	private boolean deleteController;
	private boolean insertController;
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	
	public UpdateManagerProcedure(Adaptation record,boolean deleteController) {
		super();
		this.record = record;
		this.deleteController = deleteController;
	}

	public UpdateManagerProcedure(AdaptationTable table, String controllerID,
			String manSegID) {
		super();
		this.table = table;
		this.controllerID = controllerID;
		this.manSegID = manSegID;

	}

	public UpdateManagerProcedure(AdaptationTable table, String manSegID,
			boolean deletion) {
		super();
		this.table = table;
		this.manSegID = manSegID;
		this.deletion = deletion;
		LOG.info("Managed Segment ID -----------------" + manSegID);

	}
	

	public UpdateManagerProcedure(AdaptationTable table, String controllerID,
			String manSegID,Adaptation record,boolean insertController) {
		super();
		this.table = table;
		this.controllerID = controllerID;
		this.manSegID = manSegID;
		this.record=record;
		this.insertController=insertController;

	}

	@Override
	public void execute(ProcedureContext pContext) throws Exception {

		if (deletion) {
			LOG.info("Inside Deletion");
			deleteRecord(pContext, table, manSegID);
		} else if(deleteController){
			deleteRecord(pContext, record);
		} else if (insertController){
			insertRecord(pContext, table, controllerID, manSegID,record);
		}else{
			deleteControllerRecord(pContext, table, manSegID);

		}

	}

	public void insertRecord(ProcedureContext pContext, AdaptationTable table,
			String controllerID, String manSegID,Adaptation record) throws OperationException {
		
		ValueContextForUpdate valueContext;
		valueContext = pContext.getContextForNewOccurrence(table);
		if (controllerID == null || controllerID.isEmpty()) {
			controllerID = "";
		}else{
		valueContext
				.setValue(
						controllerID,
					AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID);
				valueContext
					.setValue(
					manSegID,
					AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID);
				pContext.doCreateOccurrence(valueContext, table);
		}


	}

	public void deleteRecord(ProcedureContext pContext, AdaptationTable table,
			String manSegID) throws OperationException {
		pContext.setAllPrivileges(true);
		final String predicate = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_MAN_SEG_ID
				.format() + " = '" + manSegID + "'";
		LOG.info("table--" + table + "manSegID" + manSegID + "---predicate"
				+ predicate);
		RequestResult res = table.createRequestResult(predicate);
		if (res.getSize() > 0) {
			LOG.info("ResultSet is greater than 0");
			for (Adaptation Record; (Record = res.nextAdaptation()) != null;) {
				LOG.info("Record-------------------------" + Record);
				pContext.doDelete(Record.getAdaptationName(), false);
			}
		}
		res.close();
		pContext.setAllPrivileges(false);
	}

	public void deleteControllerRecord(ProcedureContext pContext,
			AdaptationTable table, String manSegID) throws OperationException {
		pContext.setAllPrivileges(true);
		
		Repository rep = pContext.getAdaptationHome().getRepository();
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(rep,
				AccountibilityMatrixConstants.DATASPACE_Accounting_Matrix,
				AccountibilityMatrixConstants.DATASET_Accounting_Matrix);
		

		final String predicate = "osd:contains-case-insensitive("
				+ AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID
						.format() + ",'" + manSegID + "|')";

		LOG.info("table--" + table + "manSegID" + manSegID + "---predicate"
				+ predicate);
		String LVID;
		RequestResult res = table.createRequestResult(predicate);
		if (res.getSize() > 0) {
			LOG.info("ResultSet is greater than 0");
			for (Adaptation Record; (Record = res.nextAdaptation()) != null;) {
				LOG.info("Record-------------------------" + Record);
				String msIDFK = Record
						.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID);
				final String separator = "\\|";
				String[] msIDArray = msIDFK.split(separator);
				LVID = msIDArray[1];
				pContext.doDelete(Record.getAdaptationName(), false);
			}
		}
		res.close();
		pContext.setAllPrivileges(false);
	}
	
	public void deleteRecord(ProcedureContext pContext,
			Adaptation record) throws OperationException{

			pContext.doDelete(record.getAdaptationName(), false);

	}

	
	public Adaptation getRecord() {
		return record;
	}

	public void setRecord(Adaptation record) {
		this.record = record;
	}

	public String getControllerID() {
		return controllerID;
	}

	public void setControllerID(String controllerID) {
		this.controllerID = controllerID;
	}

	public AdaptationTable getTable() {
		return table;
	}

	public void setTable(AdaptationTable table) {
		this.table = table;
	}

	public String getManSegID() {
		return manSegID;
	}

	public void setManSegID(String manSegID) {
		this.manSegID = manSegID;
	}
}
