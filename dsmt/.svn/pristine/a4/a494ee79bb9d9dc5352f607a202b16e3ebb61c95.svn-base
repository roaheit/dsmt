package com.orchestranetworks.ps.importer;

import java.io.BufferedWriter;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orchestranetworks.service.ServiceContext;

public class DataSetImportForm extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected BufferedWriter out;
	
	protected String servlet = "";
	protected String defaultImportConfigDataSpace = Constants.DEFAULT_IMPORT_CONFIG_DATASPACE;
	protected String defaultImportConfigDataSet = Constants.DEFAULT_IMPORT_CONFIG_DATASET;
	
	public String getServlet() {
		return servlet;
	}

	public void setServlet(String servlet) {
		this.servlet = servlet;
	}

	public String getDefaultImportConfigDataSpace() {
		return defaultImportConfigDataSpace;
	}

	public void setDefaultImportConfigDataSpace(String defaultImportConfigDataSpace) {
		this.defaultImportConfigDataSpace = defaultImportConfigDataSpace;
	}

	public String getDefaultImportConfigDataSet() {
		return defaultImportConfigDataSet;
	}

	public void setDefaultImportConfigDataSet(String defaultImportConfigDataSet) {
		this.defaultImportConfigDataSet = defaultImportConfigDataSet;
	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		out = new BufferedWriter(res.getWriter());
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		writeForm(sContext);
		out.flush();
	}
	
	protected void writeForm(ServiceContext sContext) throws IOException {
		writeln("<form id=\"form\" action=\"" + sContext.getURLForIncludingResource(servlet)
				+ "\" method=\"post\" autocomplete=\"off\">");
		writeFields(sContext);
		writeSubmitButton(sContext);
		writeln("</form>");
	}
	
	protected void writeFields(ServiceContext sContext) throws IOException {
		writeln("  <table>");
		writeln("    <tbody>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + Constants.PARAM_IMPORT_CONFIG_DATA_SPACE
				+ "\">Import Config Data Space</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + defaultImportConfigDataSpace
				+ "\" type=\"text\" id=\"" + Constants.PARAM_IMPORT_CONFIG_DATA_SPACE
				+ "\" name=\"" + Constants.PARAM_IMPORT_CONFIG_DATA_SPACE + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + Constants.PARAM_IMPORT_CONFIG_DATA_SET
				+ "\">Import Config Data Set</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + defaultImportConfigDataSet
				+ "\" type=\"text\" id=\"" + Constants.PARAM_IMPORT_CONFIG_DATA_SET
				+ "\" name=\"" + Constants.PARAM_IMPORT_CONFIG_DATA_SET + "\"/>");;
		writeln("        </td>");
		writeln("      </tr>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + Constants.PARAM_IMPORT_CONFIG_ID
				+ "\">Import Config ID</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + sContext.getCurrentAdaptation().getAdaptationName().getStringName()
				+ "\" type=\"text\" id=\"" + Constants.PARAM_IMPORT_CONFIG_ID
				+ "\" name=\"" + Constants.PARAM_IMPORT_CONFIG_ID + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + Constants.PARAM_DISABLE_TRIGGERS
				+ "\">Disable Triggers</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"true\" type=\"checkbox\" checked=\"checked\" id=\""
				+ Constants.PARAM_DISABLE_TRIGGERS
				+ "\" name=\"" + Constants.PARAM_DISABLE_TRIGGERS + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + Constants.PARAM_RUN_VALIDATION
				+ "\">Run Validation</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"true\" type=\"checkbox\" checked=\"checked\" id=\""
				+ Constants.PARAM_RUN_VALIDATION
				+ "\" name=\"" + Constants.PARAM_RUN_VALIDATION + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		writeln("    </tbody>");
		writeln("  </table>");
	}
	
	protected void writeSubmitButton(ServiceContext sContext) throws IOException {
		writeln("  <button type=\"submit\" class=\"ebx_Button\">Import</button>");
	}
	
	protected void write(final String str) throws IOException {
		out.write(str);
	}
	
	protected void writeln(final String str) throws IOException {
		write(str);
		out.newLine();
	}
}
