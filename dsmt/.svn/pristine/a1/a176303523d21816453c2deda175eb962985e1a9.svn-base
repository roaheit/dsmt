package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class GenericHierarchyLevelValueFunction implements ValueFunction {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private String parentFKField;
	private SchemaNode levelNode;
	
	public String getParentFKField() {
		return parentFKField;
	}

	public void setParentFKField(String parentFKField) {
		this.parentFKField = parentFKField;
	}

	@Override
	public Object getValue(Adaptation record) {
		
		String parentPK =  null;
		
		try {
			parentPK = record.getString(Path.SELF.add(parentFKField));
		} catch (Error e) {
			LOG.error(e + " caught for " + parentPK + ", error message = " + e.getMessage());
		}
		
		if (parentPK == null) {
			return 1;
		}
		String primaryKey= record.getOccurrencePrimaryKey().format();
		if(primaryKey.equals(parentPK))
		{
			return 1;
		}
		AdaptationTable table = record.getContainerTable();
		Adaptation parentRecord = table.lookupAdaptationByPrimaryKey(
				PrimaryKey.parseString(parentPK));
		if (parentRecord == null) {
			return 1;
		}
		return parentRecord.get_int(levelNode.getPathInAdaptation()) + 1;
	}

	@Override
	public void setup(ValueFunctionContext context) {
		levelNode = context.getSchemaNode();
		if (parentFKField == null) {
			context.addError("Must specify parentFKField.");
		} else {
			SchemaNode parentFKNode = levelNode.getNode(Path.PARENT.add(parentFKField));
			if (parentFKNode == null) {
				context.addError(parentFKField + " is not a node in the schema.");
			}
		}
	}
}
