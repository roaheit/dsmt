/*
 * Copyright Orchestra Networks 2000-2011. All rights reserved.
 */
package com.orchestranetworks.ps.validation.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.orchestranetworks.service.OperationException;

public class CSVFileExporterUtil {

	private String EXPORT_DIR = "csvExport";

	private final String exportFilePath;
	private String SEPARATOR = ",";
	private Locale locale;
	

	public CSVFileExporterUtil(final String servletContextPath, final String exportDir, final String separator, final Locale locale)
			throws Exception {
		/*
		 * Cr�ation du r�pertoire d'export s'il n'existe pas d�j�.
		 */
		final String exportDirPath = this.createExportDir(servletContextPath);

		final File exportFile = new File(exportDirPath + "/Accounting Matrix data.csv");
		exportFile.createNewFile();
		this.exportFilePath = exportFile.getAbsolutePath();
		this.SEPARATOR = separator;
		this.EXPORT_DIR = exportDir;
	}
	
public String doCSVExport(final ArrayList<String> list,final Map<String, String> map,int size) throws FileNotFoundException, IOException, OperationException {


		final BufferedWriter writer = new BufferedWriter(new FileWriter(this.exportFilePath));
			writer.write(AccountibilityMatrixConstants.LEM_TYPE_LEM+" : "+map.get(AccountibilityMatrixConstants.LEM_TYPE_LEM));
			writer.write(SEPARATOR);
			writer.write(AccountibilityMatrixConstants.DELEGATE+" : "+map.get(AccountibilityMatrixConstants.DELEGATE));
			writer.write(SEPARATOR);
			writer.newLine();
			writer.write(AccountibilityMatrixConstants.GOC+" : "+map.get(AccountibilityMatrixConstants.GOC));
			writer.write(SEPARATOR);
			writer.write(AccountibilityMatrixConstants.goc_desc+" : "+map.get(AccountibilityMatrixConstants.goc_desc));
			writer.write(SEPARATOR);
			writer.newLine();
			writer.write(AccountibilityMatrixConstants.msID+" : "+map.get(AccountibilityMatrixConstants.msID));
			writer.write(SEPARATOR);
			writer.write(AccountibilityMatrixConstants.mgID+" : "+map.get(AccountibilityMatrixConstants.mgID));
			writer.write(SEPARATOR);
			writer.newLine();
			writer.write(AccountibilityMatrixConstants.lvid+" : "+map.get(AccountibilityMatrixConstants.lvid));
			writer.write(SEPARATOR);
			writer.write(AccountibilityMatrixConstants.func+" : "+map.get(AccountibilityMatrixConstants.func));
			writer.write(SEPARATOR);
			writer.newLine();
			writer.write(AccountibilityMatrixConstants.BU+" : "+map.get(AccountibilityMatrixConstants.BU));
			writer.write(SEPARATOR);
			writer.write(AccountibilityMatrixConstants.OU+" : "+map.get(AccountibilityMatrixConstants.OU));
			writer.write(SEPARATOR);
			writer.newLine();
			writer.newLine();
			
			
		this.writeHeader(SEPARATOR, writer);

		for (final Iterator iterator = list.iterator(); iterator.hasNext();) {
			
			int list_size = size;
			String space="";
			String lev = "";
			
			
			while(iterator.hasNext()){
				String[] elements = iterator.next().toString().split(",");
				String level = (elements[5] == null || elements[5].equalsIgnoreCase("null"))?"":elements[5];
				elements[0] = (elements[0] == null || elements[0].equalsIgnoreCase("null"))?"":elements[0];
				elements[1] = (elements[1] == null || elements[1].equalsIgnoreCase("null"))?"":elements[1];
				elements[2] = (elements[2] == null || elements[2].equalsIgnoreCase("null"))?"":elements[2];
				elements[3] = (elements[3] == null || elements[3].equalsIgnoreCase("null"))?"":elements[3];
				elements[4] = (elements[4] == null || elements[4].equalsIgnoreCase("null"))?"":elements[4];
				elements[6] = (elements[6] == null || elements[6].equalsIgnoreCase("null"))?"":elements[6];
				if(!level.equalsIgnoreCase(lev)){
					space+= "   ";
				}
				writer.write(String.valueOf(list_size));
				writer.write(SEPARATOR);
				writer.write(elements[0]);
				writer.write(SEPARATOR);
				if(list_size==size){
					writer.write(elements[1]);
				}else{
					writer.write(space+elements[1]);
				}
				writer.write(SEPARATOR);
				writer.write(elements[2]);
				writer.write(SEPARATOR);
				writer.write(elements[6]);
				writer.write(SEPARATOR);
				writer.write(elements[4]);
				writer.write(SEPARATOR);
				writer.write(elements[3]);
				writer.newLine();
				list_size = list_size-1;
				lev = level;
			}

		}
		writer.flush();
		writer.newLine();

		writer.close();
		return this.exportFilePath;
	}

	private void writeHeader(final String separator, final BufferedWriter writer) throws IOException {
		writer.write("Escalation Level");
		writer.write(separator);
		writer.write("Managed Segment");
		writer.write(separator);
		writer.write("Managed Segment Description");
		writer.write(separator);
		writer.write("Controller ID");
		writer.write(separator);
		writer.write("Controller Name");
		writer.write(separator);
		writer.write("Managed Geography");
		writer.write(separator);
		writer.write("Comments");
		writer.write(separator);
		writer.flush();
		writer.newLine();
	}

	private String createExportDir(final String servletContextPath) {
		String dirPath = servletContextPath;
		dirPath += dirPath.endsWith("/") ? "" : "/";
		dirPath += this.EXPORT_DIR;
		final File dir = new File(dirPath);
		if (!dir.exists()) {
			dir.mkdir();
		}
		return dir.getAbsolutePath();
	}

}
