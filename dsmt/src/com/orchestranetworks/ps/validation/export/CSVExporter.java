/*
 * Copyright Orchestra Networks 2000-2011. All rights reserved.
 */
package com.orchestranetworks.ps.validation.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.ps.validation.bean.ValidationErrorElement;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class CSVExporter {

	private String EXPORT_DIR = "csvExport";

	private final String exportFilePath;
	private String SEPARATOR = ",";
	private Locale locale;

	public CSVExporter(final String servletContextPath, final String exportDir, final String separator, final Locale locale)
			throws Exception {
		/*
		 * Cr�ation du r�pertoire d'export s'il n'existe pas d�j�.
		 */
		final String exportDirPath = this.createExportDir(servletContextPath);

		final File exportFile = new File(exportDirPath + "/validationReport.csv");
		exportFile.createNewFile();
		this.exportFilePath = exportFile.getAbsolutePath();
		this.SEPARATOR = separator;
		this.EXPORT_DIR = exportDir;
	}

	public String doExport(final ArrayList<ValidationErrorElement> list) throws FileNotFoundException, IOException, OperationException {

		final BufferedWriter writer = new BufferedWriter(new FileWriter(this.exportFilePath));
		this.writeHeader(SEPARATOR, writer);

		for (final Iterator iterator = list.iterator(); iterator.hasNext();) {
			final ValidationErrorElement validationErrorElement = (ValidationErrorElement) iterator.next();
			final Adaptation record = validationErrorElement.getRecord();
			if (record.isTableOccurrence()) {
				writer.write(record.getContainerTable().getTableNode().getLabel(this.locale));
				writer.write(SEPARATOR);
				writer.write(record.getOccurrencePrimaryKey().format());
				writer.write(SEPARATOR);
				writer.write(validationErrorElement.getMessage());
				writer.write(SEPARATOR);
				writer.write(record.getLastUser().getUserId());
				writer.write(SEPARATOR);
				writer.write(record.getTimeOfLastModification().toString());
				LoggingCategory.getKernel().debug("---record.getLastUser().format()---"+record.getLastUser().format());
				LoggingCategory.getKernel().debug("---record.getLastUser().getLabel()---"+record.getLastUser().getLabel());
				LoggingCategory.getKernel().debug("---record.getLastUser().getUserId()---"+record.getLastUser().getUserId());
				LoggingCategory.getKernel().debug("---record.getTimeOfCreation()---"+record.getTimeOfCreation());
				LoggingCategory.getKernel().debug("---record.getTimeOfLastModification()---"+record.getTimeOfLastModification());
				writer.newLine();
			} else {
				writer.write("Data Set");
				writer.write(SEPARATOR);
				writer.write(record.getLabelOrName(this.locale));
				writer.write(SEPARATOR);
				writer.write(validationErrorElement.getMessage());
				writer.newLine();
			}

		}
		writer.flush();
		writer.newLine();

		writer.close();
		return this.exportFilePath;
	}

	private void writeHeader(final String separator, final BufferedWriter writer) throws IOException {
		writer.write("Table");
		writer.write(separator);
		writer.write("Record");
		writer.write(separator);
		writer.write("Message");
		writer.write(separator);
		writer.write("Last Modified User");
		writer.write(separator);
		writer.write("Last Modified Time");
		writer.flush();
		writer.newLine();
	}

	private String createExportDir(final String servletContextPath) {
		String dirPath = servletContextPath;
		dirPath += dirPath.endsWith("/") ? "" : "/";
		dirPath += this.EXPORT_DIR;
		final File dir = new File(dirPath);
		if (!dir.exists()) {
			dir.mkdir();
		}
		return dir.getAbsolutePath();
	}

}
