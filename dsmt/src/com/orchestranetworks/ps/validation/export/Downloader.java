/*
 * Copyright Orchestra Networks 2000-2011. All rights reserved.
 */
package com.orchestranetworks.ps.validation.export;

import java.io.*;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.*;

public class Downloader extends HttpServlet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8634849456064915548L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		
	
		String filePath = request.getParameter("filePath");
		if (filePath != null && !filePath.isEmpty())
		{
			String[] tmp = filePath.split("/");
			String fileName = tmp[tmp.length - 1];
			response.setHeader("Content-type", "application/octet-stream");
			response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
			//response.setHeader("Content-Type", "application/force-download");
			ServletOutputStream out = response.getOutputStream();
			InputStream in = new FileInputStream(filePath);
			byte[] buf = new byte[1024];
			int l;
			while ((l = in.read(buf)) != -1)
			{
				out.write(buf, 0, l);
			}
			buf = null;
			in.close();
		}

	}

}
