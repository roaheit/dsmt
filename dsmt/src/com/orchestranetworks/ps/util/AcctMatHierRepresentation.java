package com.orchestranetworks.ps.util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.ServiceKey;
import com.orchestranetworks.ui.UIHttpManagerComponent;

public class AcctMatHierRepresentation extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static LoggingCategory LOG = LoggingCategory.getKernel();
	protected BufferedWriter out;
	protected String msid="";
	protected String lvid="";
	protected String mgid="";
	public String getMgid() {
		return mgid;
	}

	public void setMgid(String mgid) {
		this.mgid = mgid;
	}

	public String getmsid() {
		return msid;
	}

	public void setmsid(String msid) {
		this.msid = msid;
	}
	public String getlvid() {
		return lvid;
	}

	public void setlvid(String lvid) {
		this.lvid = lvid;
	}

	protected String servlet = "";
		
	public String getServlet() {
		return servlet;
	}

	public void setServlet(String servlet) {
		this.servlet = servlet;
	}

	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		out = new BufferedWriter(res.getWriter());
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		writeForm(sContext);
		loadDataFromDS(req,res);
		out.flush();
	}
	
	protected void writeForm(ServiceContext sContext) throws IOException {
		writeln("<form id=\"form\" action=\"" + sContext.getURLForIncludingResource(servlet)
				+ "\" method=\"post\" autocomplete=\"off\">");
		writeFields(sContext);
		writeSubmitButton(sContext);
		writeln("</form>");
	}
	
	protected void writeFields(ServiceContext sContext) throws IOException {
		String serviceURL = sContext.getURLForServiceSelection(ServiceKey.forModuleServiceName("dsmt", "AccMatEscpopupService"));
		UIHttpManagerComponent httpFact = sContext.createWebComponentForSubSession();
		httpFact.selectHome(HomeKey.forBranchName("Accounting_Matrix"));
		httpFact.setService(ServiceKey.forModuleServiceName("dsmt", "AccMatEscpopupService"));
		String url = httpFact.getURIWithParameters();
		//sContext.createWebComponentForSubSession().setService(ServiceKey.forModuleServiceName("dsmt", "AccMatEscpopupService"));
		//String url = sContext.getURLForServiceSelection(ServiceKey.forModuleServiceName("dsmt", "AccMatEscpopupService"));
		
		writeln("  <table>");
		writeln("    <tbody>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + "MSID" + "\">Managed Segment ID</label>");
		writeln("          <label style='color:red' >*</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + msid
				+ "\" type=\"text\" id=\"" + "MSID"
				+ "\" name=\"" + "MSID" + "\" onkeyup=\"callAccMatService(this)\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + "LVID"+ "\">Legal Entity ID</label>");
		writeln("          <label style='color:red' >*</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + lvid
				+ "\" type=\"text\" id=\"" + "LVID"
				+ "\" name=\"" + "LVID" +  "\" onkeyup=\"callAccMatService(this)\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + "MGID" + "\">Managed Geography ID</label>");
		writeln("          <label style='color:red' >*</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + mgid
				+ "\" type=\"text\" id=\"" + "MGID"
				+ "\" name=\"" + "MGID" + "\" onkeyup=\"callAccMatService(this)\"/>");
		writeln("        <input type=\"hidden\" id=\"Hid_ajax_URL\" value="+url+"/></td>");
		writeln("      </tr>");
		writeln("    </tbody></table>");
		writeln("<div id=\"resultDIV\"></div>");
	}
	
	protected void writeSubmitButton(ServiceContext sContext) throws IOException {
		writeln("  <button type=\"submit\" class=\"ebx_Button\">Show Escalation Path</button>");
	}
	
	protected void write(final String str) throws IOException {
		out.write(str);
	}
	
	protected void writeln(final String str) throws IOException {
		write(str);
		out.newLine();
	}
	//Fetch the list of IDs from DataSet
	protected void loadDataFromDS(HttpServletRequest request, HttpServletResponse res) throws IOException {
		HttpSession ses = request.getSession();
		ServiceContext ctx = ServiceContext.getServiceContext(request);
		Path targetTabSchema = null;
   		String predicatexp = "";
   		Path idPath = null, descPath = null;
		Map<String,String> IDLst = null;
   		Repository repo = null;
		Adaptation metadataDataSet = null;
		AdaptationTable targetTable = null;
		RequestResult rs =null; 
   		for(int i=1;i<4;i++) {
	   		if(i==1) {
		   		//MSID Path	
		   		targetTabSchema = GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD.getPathInSchema();
				predicatexp = GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._C_DSMT_MAN_SEG_ID.format();
				idPath = GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._C_DSMT_MAN_SEG_ID;
				descPath = GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._DESCR80;
	   		}
	   		else if(i==2) {
	   			//LVID Path 
				targetTabSchema = GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_LVID_CHILD.getPathInSchema();
				predicatexp = GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_LVID_CHILD._C_DSMT_LVID.format();
				idPath = GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_LVID_CHILD._C_DSMT_LVID;
				descPath = GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_LVID_CHILD._DESCR;
	   		}	
	   		else {
	   			//MGID Path
				targetTabSchema = GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD.getPathInSchema();
				predicatexp = GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._C_DSMT_MAN_GEO_ID.format();
				idPath = GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._C_DSMT_MAN_GEO_ID; 
				descPath = GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._C_DSMT_DESCR90;
	   		}
	   		try {
				repo = ctx.getCurrentHome().getRepository();
				metadataDataSet = EBXUtils.getMetadataDataSet(repo, DSMTConstants.GOC, DSMTConstants.GOC);
				targetTable = metadataDataSet.getTable(targetTabSchema);
				IDLst = new TreeMap<String,String>();
				rs = targetTable.createRequestResult(null);
				if (null != rs) {
					for (Adaptation record; (record = rs.nextAdaptation()) != null;) {
						IDLst.put(record.get(idPath).toString(),record.get(descPath).toString());
		            }
				}
				if(i==1)
					ses.setAttribute("MSIDLST", IDLst);
				else if(i==2)
					ses.setAttribute("LVIDLST", IDLst);
				else
					ses.setAttribute("MGIDLST", IDLst);
			}
			catch(Exception e){
				LOG.error("Exception from AcctMatHierRepresentation::"+e);
				e.printStackTrace();
			}
			finally {
				rs = null;
				targetTable = null;
				metadataDataSet = null;
			}
   		}
	}
}
