package com.orchestranetworks.ps.util;

import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.orchestranetworks.service.LoggingCategory;
public class AccMatrixEscPopupserviceUtils {
	
	String srchType, paramID;
	HttpServletRequest request;
	public AccMatrixEscPopupserviceUtils(String srchType, String paramID,HttpServletRequest request){
		this.srchType = srchType;
		this.paramID = paramID;
		this.request = request;
	}
	public static LoggingCategory LOG = LoggingCategory.getKernel();
	public String prepareDatafromDS() {
		LOG.info("From prepareDatafromDS()::srchType="+srchType);
		HttpSession ses = request.getSession();
		Map<String,String> IDLst = new TreeMap<String,String>();
		if(srchType.equals("MSID"))
			IDLst = (TreeMap)ses.getAttribute("MSIDLST");
		else if(srchType.equals("LVID"))
			IDLst = (TreeMap)ses.getAttribute("LVIDLST");
		else
			IDLst = (TreeMap)ses.getAttribute("MGIDLST");
		String result = "";
		if(IDLst.size()>0)
   	  		result = prepareResponse(IDLst);
   	  	else
   	  		result = "<p class=\"errStyl\">Required Data is not available.</p>";
   	  	return result;
	   }
	public String prepareResponse(Map<String,String> lvIDLst) { 
		int i=1;
		String trStyl = "", resp = "", mainDivstyl = "", head ="";
		if(srchType.equals("MSID")){
			mainDivstyl = "msDivstyl";head="Select Managed Segment ID";
		}
		else if(srchType.equals("LVID")){
			mainDivstyl = "lvDivstyl";head="Select Legal Vehicle ID";
		}
		else{
			mainDivstyl = "mgDivstyl";head="Select Managed Geography ID";
		}
		if(lvIDLst.size()>0) {
			resp = "<div class="+mainDivstyl+"><div class=\"tabHDStyle\"><table><tr><th width=\"305px\" align=\"left\" style=\"padding:2px\">"+head+"</th><th width=\"15px\" align=\"left\" onclick=\"closePopUP();\"> X </th></tr></table></div><div class=\"innerDispStyle\"><table style=\"margin:2px;\">";
			for (Map.Entry<String, String> data : lvIDLst.entrySet()) {
				if(i%2==0)
					trStyl="trEven";
				else
					trStyl="trOdd";
				if(data.getKey().startsWith(paramID)) {
					resp += "<tr class="+trStyl+"><td width=\"320px\" onclick=\"selectRW('"+data.getKey()+"')\">"+data.getKey()+" --- "+data.getValue()+"</td></tr>";
					i++;
				}
			}
			resp += "</table></div></div>";
		}
		return resp;
	}
}
