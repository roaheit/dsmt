package com.orchestranetworks.ps.util;

import java.io.FileInputStream;
import java.text.MessageFormat;
import java.util.Properties;

public class DSMTPropertyHelper {	
	private String propParameter = "ebx.properties";
	private String propPrefix = "";
	private Properties props = null;

	public DSMTPropertyHelper(){
		this(null, null);
	}
	
	public DSMTPropertyHelper(final String propertyPrefix){
		this(propertyPrefix, null);
	}
	
	public DSMTPropertyHelper(final String propertyPrefix, final String propertyParameter) {
		if( propertyParameter!=null )
			propParameter = propertyParameter;
		if( propertyPrefix!=null )
			propPrefix = propertyPrefix;
		props = new Properties();
		try { 
			props.load(new FileInputStream(System.getProperty(propParameter))); 
		} catch (Exception e){
		}
	}
	
	public String getProp(final String key){
		final String value = this.props.getProperty(propPrefix + key);
		return value;
	}
	
	public MessageFormat getPropFormat(final String key){
		final String value = this.props.getProperty(propPrefix + key);
		if( value==null )
			return null;
		return new MessageFormat(value);
	}
	
	public void reloadProperties(){
		Properties newProps;
		try {
			newProps = new Properties();
			newProps.load(new FileInputStream(System.getProperty(propParameter))); 
			props = newProps;
		} catch (Exception e) {
		}
	}
	
	public void setPropertyParameter(final String propertyParameter){
		if( propertyParameter!=null )
			propParameter = propertyParameter;
		reloadProperties();
	}
}
