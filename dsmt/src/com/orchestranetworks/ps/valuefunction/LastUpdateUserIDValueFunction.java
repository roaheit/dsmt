package com.orchestranetworks.ps.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class LastUpdateUserIDValueFunction implements ValueFunction {
	@Override
	public Object getValue(Adaptation adaptation) {		
		return adaptation.getLastUser().getUserId();
	}
	
	@Override
	public void setup(ValueFunctionContext context) {
		// do nothing
	}
}
