package com.orchestranetworks.ps.trigger;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;

public class DefaultDateTrigger extends TableTrigger {
	private Path dateField;
	private Date date;

	public String getDateField() {
		return dateField.format();
	}

	public void setDateField(String dateField) {
		this.dateField = Path.parse(dateField);
	}

	@Override
	public void setup(TriggerSetupContext context) {
		// do nothing
	}

	@Override
	public void handleNewContext(NewTransientOccurrenceContext context) {
		// TODO: Code for creating the date to be what you want it to be
		// For now I just set to the current date.
		date = new Date();
		
		context.getOccurrenceContextForUpdate().setValue(date, dateField);
		super.handleNewContext(context);
	}

//	@Override
//	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
//			throws OperationException {
//		final ValueContext vc = context.getOccurrenceContext();
//		final Date contextDate = (Date) vc.getValue(dateField);
//		if (! date.equals(contextDate)) {
//			final SchemaNode tableNode = context.getTable().getTableNode();
//			final SchemaNode dateFieldNode = tableNode.getNode(dateField);
//			final String dateFieldLabel = dateFieldNode.getLabel(context.getSession().getLocale());
//			final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//			throw OperationException.createError("Can't change value of field " + dateFieldLabel
//					+ ". Must be '" + dateFormat.format(date) + "'.");
//		}
//		super.handleBeforeCreate(context);
//	}
}
