package com.orchestranetworks.ps.trigger;

import java.util.List;

import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeTransactionCancelContext;
import com.orchestranetworks.schema.trigger.BeforeTransactionCommitContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;

public abstract class CompoundTrigger extends TableTrigger {
	protected List<TableTrigger> triggers;

	@Override
	public void setup(TriggerSetupContext context) {
		initTriggers();
		if (triggers == null || triggers.isEmpty()) {
			context.addError("No triggers configured for compound trigger.");
			return;
		}
		for (TableTrigger trigger: triggers) {
			trigger.setup(context);
		}
	}
	
	@Override
	public void handleNewContext(NewTransientOccurrenceContext context) {
		for (TableTrigger trigger: triggers) {
			trigger.handleNewContext(context);
		}
	}
	
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		for (TableTrigger trigger: triggers) {
			trigger.handleBeforeCreate(context);
		}
	}
	
	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		for (TableTrigger trigger: triggers) {
			trigger.handleAfterCreate(context);
		}
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		for (TableTrigger trigger: triggers) {
			trigger.handleBeforeModify(context);
		}
	}

	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context)
			throws OperationException {
		for (TableTrigger trigger: triggers) {
			trigger.handleAfterModify(context);
		}
	}
	
	@Override
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
		for (TableTrigger trigger: triggers) {
			trigger.handleBeforeDelete(context);
		}
	}

	@Override
	public void handleAfterDelete(AfterDeleteOccurrenceContext context)
			throws OperationException {
		for (TableTrigger trigger: triggers) {
			trigger.handleAfterDelete(context);
		}
	}

	@Override
	public void handleBeforeTransactionCancel(
			BeforeTransactionCancelContext context) {
		for (TableTrigger trigger: triggers) {
			trigger.handleBeforeTransactionCancel(context);
		}
	}

	@Override
	public void handleBeforeTransactionCommit(
			BeforeTransactionCommitContext context) throws OperationException {
		for (TableTrigger trigger: triggers) {
			trigger.handleBeforeTransactionCommit(context);
		}
	}

	protected abstract void initTriggers();
}
