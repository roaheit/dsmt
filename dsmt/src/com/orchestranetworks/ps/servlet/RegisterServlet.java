package com.orchestranetworks.ps.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.onwbp.base.repository.ModulesRegister;

public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ModulesRegister.registerWebApp(this, config);
	}
	
	public void destroy() {
		ModulesRegister.unregisterWebApp(this, this.getServletConfig());
	}
}