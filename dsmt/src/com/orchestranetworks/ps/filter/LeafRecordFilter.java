package com.orchestranetworks.ps.filter;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;

public class LeafRecordFilter extends CurrentRecordFilter{
	private String PLFlag = "./PL_FLAG";

	public String getPLFlag() {
		return PLFlag;
	}

	public void setPLFlag(String pLFlag) {
		PLFlag = pLFlag;
	}

	@Override
	public boolean accept(Adaptation adaptation) {
		final String plFlag = adaptation.getString(Path.parse(PLFlag));

		if (!"L".equalsIgnoreCase(plFlag)) {
			return false;
		} else if ("L".equalsIgnoreCase(plFlag)) {
			return true;
		}
		return ("L".equalsIgnoreCase(plFlag));
	}
}