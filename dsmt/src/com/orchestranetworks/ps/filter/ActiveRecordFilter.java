package com.orchestranetworks.ps.filter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.ValidationReport;

public class ActiveRecordFilter implements AdaptationFilter {

	

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	final SimpleDateFormat dateFormat = new SimpleDateFormat(DSMTConstants.DEFAULT_DATE_FORMAT);
	
	private String endDateField = "./ENDDT";
	private String effectiveStatusField = "./EFF_STATUS";
	private String effectiveDateField = "./EFFDT";
	private String validRecordsOnly = "false";
	
	public String getEffectiveStatusField() {
		return effectiveStatusField;
	}

	public void setEffectiveStatusField(String effectiveStatusField) {
		this.effectiveStatusField = effectiveStatusField;
	}

	public String getEndDateField() {
		return endDateField;
	}

	public void setEndDateField(String endDateField) {
		this.endDateField = endDateField;
	}

	public String getValidRecordsOnly() {
		return validRecordsOnly;
	}

	public void setValidRecordsOnly(String validRecordsOnly) {
		this.validRecordsOnly = validRecordsOnly;
	}

	@Override
	public boolean accept(Adaptation adaptation) {
		
		final Date endDate = adaptation.getDate(Path.parse(endDateField));
		final String effectiveStatus = adaptation.getString(Path.parse(effectiveStatusField));
		
		if(! DSMTConstants.ACTIVE.equalsIgnoreCase(effectiveStatus)){
			return false;
		}
		else if (endDate == null && DSMTConstants.ACTIVE.equalsIgnoreCase(effectiveStatus)) {
			return true;
		} 
		
	
		final String dateStr = dateFormat.format(endDate);
		final boolean isActive = DSMTConstants.MAX_END_DATE.equals(dateStr) &&  DSMTConstants.ACTIVE.equalsIgnoreCase(effectiveStatus) && !isFutureEffectiveDate(adaptation) ;
		if (isActive && Boolean.valueOf(validRecordsOnly).booleanValue()) {
			final ValidationReport validationReport = adaptation.getValidationReport();
			return ! validationReport.hasItemsOfSeverityOrMore(Severity.ERROR);
		}
		return isActive;
	}

	private boolean isFutureEffectiveDate(final Adaptation adaptation){
		
		boolean isFutureEffectiveDate = false;
		final Date currentDate = Calendar.getInstance().getTime();
		final Date effectiveDate = adaptation.getDate(Path.parse(effectiveDateField));
		
		isFutureEffectiveDate = currentDate.before(effectiveDate);
		if(isFutureEffectiveDate){	
			
			LOG.debug("This date is in future and cant be sent to export = " + effectiveDate);
		}
		return isFutureEffectiveDate;
	}
	
	public void setEffectiveDateField(String effectiveDateField) {
		this.effectiveDateField = effectiveDateField;
	}

	public String getEffectiveDateField() {
		return effectiveDateField;
	}

	public static void main(String[] args) {
		
		final Date endDate = null;
		final SimpleDateFormat dateFormat = new SimpleDateFormat(DSMTConstants.DEFAULT_DATE_FORMAT);
		
		//System.out.println(dateFormat.format(endDate));
	}
}
