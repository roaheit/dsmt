package com.orchestranetworks.ps.filter;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;

public class ParentRecordFilter extends CurrentRecordFilter{
	private String PLFlag = "./PL_FLAG";
	private final String PARENT_ID = "./PARENT_ID";

	public String getPLFlag() {
		return PLFlag;
	}

	public void setPLFlag(String pLFlag) {
		PLFlag = pLFlag;
	}

	@Override
	public boolean accept(Adaptation adaptation) {
		
		boolean isAccepted = super.accept(adaptation);
		
		final String plFlag = adaptation.getString(Path.parse(PLFlag));
		final String parentID = adaptation.getString(Path.parse(PARENT_ID));

		if("1".equalsIgnoreCase(adaptation.getString(Path.parse("./C_DSMT_PRODUCT_TYP")))){
			LOG.debug("C_DSMT_PRODUCT_TYP = " + adaptation.getString(Path.parse("./C_DSMT_PRODUCT_TYP")) +  "x" + parentID + "y" );
		}
		
		if(null == parentID){
			
			isAccepted = false;
		}
		else {
			if (!"P".equalsIgnoreCase(plFlag)) {
		
			isAccepted = false;
			} else if ("P".equalsIgnoreCase(plFlag)) {
				isAccepted = true;
			}
		}
		
		return isAccepted;
	}

	
}