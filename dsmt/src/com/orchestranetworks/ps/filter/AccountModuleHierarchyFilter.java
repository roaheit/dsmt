package com.orchestranetworks.ps.filter;

import java.util.ArrayList;
import java.util.List;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;

public class AccountModuleHierarchyFilter extends CurrentRecordFilter{
	
	/**
	 * fieldName - field Name (e.g - "./PL_FLAG")
	 */
	private String fieldName;
	private String fieldValue;
	private String isParent;
	private String endDateField = "./ENDDT";	
	// private final String PARENT_ID = "./PARENT_ID";
	
	private String parentColumnID = "./PARENT_ID";
	
	private String codeColumnID ;
	private String multiTableRelation ;
	private String FDL ="/root/GLOBAL_STD/PMF_STD/F_ACC_STD/FDL_Account_Table";
	private String PMF ="/root/GLOBAL_STD/PMF_STD/F_ACC_STD/PMF_Account_Table";
	private String P2P = "/root/GLOBAL_STD/PMF_STD/F_ACC_STD/P2P_Account_Table";
	private String CITIGL ="/root/GLOBAL_STD/PMF_STD/F_ACC_STD/CITGL_Account_Table";
	
	

	@Override
	public boolean accept(Adaptation adaptation) {
		
		boolean isCurrentRecord = super.accept(adaptation);
		boolean isAccepted = false;
		
		final String plFlag = adaptation.getString(Path.parse(fieldName));
		
			
		if (!fieldValue.equalsIgnoreCase(plFlag)) {
		
			isAccepted = false;
		} else if (fieldValue.equalsIgnoreCase(plFlag)) {
			isAccepted = true;
		}
		
		final String parentID = adaptation.getString(Path.parse(parentColumnID));
		if(isCurrentRecord){
		
			if(null == parentID){	
									/*if("true".equalsIgnoreCase(isParent) && fieldValue.equalsIgnoreCase("P")){
										
												isAccepted = true;											
													}else{
														isAccepted = false;											
													}*/
								}else{
									if(!isAccepted && null!=multiTableRelation){	
											if(fieldValue.equalsIgnoreCase("P")){
												isAccepted = isValidPrentRecord(adaptation,adaptation.getString(Path.parse(codeColumnID)));								
											}
									 }
								}
		}
			
							
		return isCurrentRecord && isAccepted;
	}

	private boolean isValidPrentRecord(Adaptation record, String codeID ) {
		boolean isValid=false;
		
			Path foreignTablePath = Path.parse(FDL); 
			AdaptationTable ccTable = record.getContainer().getTable(foreignTablePath);
			String predicate = parentColumnID + "=\"" + codeID + "\" and "+ endDatePredicate()+  "\"";
			RequestResult reqRes = ccTable.createRequestResult(predicate);
			Adaptation ccRecord;
			try {
					ccRecord = reqRes.nextAdaptation();
			} finally {
						reqRes.close();
					}
					
			if (ccRecord == null) {
						
						isValid= false;
					}
					else{
						return true;
					}
			
		
		
		
		return isValid;
	}

	
	private boolean isValidChildRecord(Adaptation record, String codeID ) {
		boolean isValid=false;
		
			Path foreignTablePath = Path.parse(FDL); 
			AdaptationTable ccTable = record.getContainer().getTable(foreignTablePath);
			String predicate = parentColumnID + "=\"" + codeID + "\" and "+ endDatePredicate()+  "\"";
			RequestResult reqRes = ccTable.createRequestResult(predicate);
			Adaptation ccRecord;
			try {
					ccRecord = reqRes.nextAdaptation();
			} finally {
						reqRes.close();
					}
					
			if (ccRecord == null) {
									return true;
					}
					else{
						isValid= false;
						}
			
		
		
		
		return isValid;
	}

	public String getFieldName() {
		return fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public String getFieldValue() {
		return fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}


	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}


	public String getIsParent() {
		return isParent;
	}

	public void setParentColumnID(String parentColumnID) {
		this.parentColumnID = parentColumnID;
	}

	public String getParentColumnID() {
		return parentColumnID;
	}

	public String getMultiTableRelation() {
		return multiTableRelation;
	}

	public void setMultiTableRelation(String multiTableRelation) {
		this.multiTableRelation = multiTableRelation;
	}

	public String getCodeColumnID() {
		return codeColumnID;
	}

	public void setCodeColumnID(String codeColumnID) {
		this.codeColumnID = codeColumnID;
	}
private String endDatePredicate(){
		
		return " ( osd:is-null(" + endDateField + ") or date-equal(" + endDateField + ", '9999-12-31') )";
	}

	
}
