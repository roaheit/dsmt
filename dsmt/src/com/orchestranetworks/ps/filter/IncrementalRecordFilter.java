package com.orchestranetworks.ps.filter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class IncrementalRecordFilter implements AdaptationFilter {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
			
	private String updatedTime = "./CHNG_DTTM";
	private String reportEndDate;
	private String incReportType;
	
	

	@Override
	public boolean accept(Adaptation adaptation) {
		final Date recordChangeDate = adaptation.getDate(Path.parse(updatedTime));
		if (recordChangeDate == null) {
			return false;
		}
		else{
			return recodInDateRange(recordChangeDate);
		}
		
		
		
	}
	
	private boolean recodInDateRange(Date  recordDate)
    {
		 Date endDate=null;
		 boolean isValidDate =false;
		 Calendar calendar = Calendar.getInstance();
		 final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		if(reportEndDate== null)
		{
			  endDate =calendar.getTime();
		}
		else{
			
				try {
					endDate = dateFormat.parse(reportEndDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					LOG.error("Repor end Date is in wrong format"+ reportEndDate);
				}
			
		}
        endDate.setDate(endDate.getDate()-1);
        Date startDate=  calendar.getTime();
        if(incReportType.equalsIgnoreCase(DSMTConstants.DAILY)){
        	 calendar.add(Calendar.DATE, -1);
             startDate = calendar.getTime();
             LOG.info("Repor startDate in  "+DSMTConstants.DAILY+" report is :"+ startDate);
             LOG.info("Repor EndDate range in  "+DSMTConstants.DAILY+" report is :"+ endDate);
               isValidDate = recordDate.before(endDate)
                       && recordDate.after(startDate);
        	
        }else  if(incReportType.equalsIgnoreCase(DSMTConstants.WEEKLY)){
        	calendar.add(Calendar.DATE, -8);
            startDate = calendar.getTime();
            LOG.info("Repor startDate in  "+DSMTConstants.WEEKLY+" report is :"+ startDate);
            LOG.info("Repor EndDate range in  "+DSMTConstants.WEEKLY+" report is :"+ endDate);
              isValidDate = recordDate.before(endDate)
                      && recordDate.after(startDate);
        	
        }else  if(incReportType.equalsIgnoreCase(DSMTConstants.MONTHLY)){
        	 startDate.setMonth(endDate.getMonth()-1);
             calendar.set( 2, startDate.getMonth());
             int day= calendar.getActualMaximum(calendar.DAY_OF_MONTH);
             startDate.setDate(day);
             LOG.info("Repor startDate in  "+DSMTConstants.MONTHLY+" report is :"+ startDate);
             LOG.info("Repor EndDate range in  "+DSMTConstants.MONTHLY+" report is :"+ endDate);
             isValidDate = recordDate.before(endDate)
                     && recordDate.after(startDate);
        	
        }
       
        return isValidDate;
        
    }

	public String getReportEndDate() {
		return reportEndDate;
	}

	public void setReportEndDate(String reportEndDate) {
		this.reportEndDate = reportEndDate;
	}

	public String getIncReportType() {
		return incReportType;
	}

	public void setIncReportType(String incReportType) {
		this.incReportType = incReportType;
	}

	

}
