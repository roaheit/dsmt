package com.orchestranetworks.ps.filter;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.util.RosettaUtil;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class EffDateRecordFilter implements AdaptationFilter{
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
			
	private String effDate = "./EFFDT";
	private Date filterRange;
	private String reportType;
	final SimpleDateFormat dateFormat = new SimpleDateFormat(DSMTConstants.DEFAULT_DATE_FORMAT);
	
	@Override
	public boolean accept(Adaptation adaptation) {
		
		Repository repo = adaptation.getHome().getRepository();
		
		RosettaUtil util = new RosettaUtil();
		
		util.getDateValue(repo);
		
		final Date effectiveDate = adaptation.getDate(Path.parse(effDate));
		if (effectiveDate == null) {
			return true;
		}
		
		reportType=util.getReportType();
		LOG.info("printing report type :- " +reportType);
		filterRange = util.getEffectDate();
		
		String dateStr = dateFormat.format(effectiveDate);
		String reportDate = dateFormat.format(filterRange);
		return reportDate.equals(dateStr) ;
		

		
	}

	public String getEffDate() {
		return effDate;
	}

	public void setEffDate(String effDate) {
		this.effDate = effDate;
	}

	public Date getFilterRange() {
		return filterRange;
	}

	public void setFilterRange(Date filterRange) {
		this.filterRange = filterRange;
	}

	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}

	/**
	 * @param reportType the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	
	

	
	

	

}
