package com.orchestranetworks.ps.filter;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;

public class AccountModulePMFFilter extends CurrentRecordFilter{
	
	/**
	 * fieldName - field Name (e.g - "./PL_FLAG")
	 */
	private String fieldName;
	private String fieldValue;
	private String isParent;
	
	// private final String PARENT_ID = "./PARENT_ID";
	
	private String parentColumnID = "./PARENT_ID";
	private String codeColumnID = "./PM_Account_Code";
	private String PMF ="/root/GLOBAL_STD/PMF_STD/F_ACC_STD/PMF_Account_Table";
	

	@Override
	public boolean accept(Adaptation adaptation) {
		
		boolean isCurrentRecord = super.accept(adaptation);
		boolean isAccepted = false;
		
		final String plFlag = adaptation.getString(Path.parse(fieldName));
		
		
		
			final String parentID = adaptation.getString(Path.parse(parentColumnID));
			
			
		
		if (!fieldValue.equalsIgnoreCase(plFlag)) {
		
			isAccepted = false;
		} else if (fieldValue.equalsIgnoreCase(plFlag)) {
			isAccepted = true;
		}
		
		if(isCurrentRecord){
				if(fieldValue.equalsIgnoreCase("P")){
					if(null == parentID && fieldValue.equalsIgnoreCase(plFlag) ){
						isAccepted = true;
						}
					else {
							if (!fieldValue.equalsIgnoreCase(plFlag)) {				
								isAccepted = false;
							} else if (fieldValue.equalsIgnoreCase(plFlag)) {
								isAccepted =isValidPrentRecord(adaptation);
							}
					}
					
				}else if(fieldValue.equalsIgnoreCase("L")) {
					
						if(null == parentID){
						return false;
						}
					
					if (!fieldValue.equalsIgnoreCase(plFlag)) {				
						isAccepted = isValidChildRecord(adaptation);
						
					} else if (fieldValue.equalsIgnoreCase(plFlag)) {
						String codeID= adaptation.getString(Path.parse(codeColumnID));
						if(codeID.startsWith("RW")){
								isAccepted=false;
							}else{
								isAccepted = true;
							}
					}
					
				}
			}
		
		return isCurrentRecord && isAccepted;
	}
	
	private boolean isValidPrentRecord(Adaptation record ) {
		boolean isValid=false;
			
			String codeID= record.getString(Path.parse(codeColumnID));
			Path foreignTablePath = Path.parse(PMF); 
			AdaptationTable ccTable = record.getContainer().getTable(foreignTablePath);
			String predicate = parentColumnID + "=\"" + codeID + "\"";
			RequestResult reqRes = ccTable.createRequestResult(predicate);
			Adaptation ccRecord;
			try {
					ccRecord = reqRes.nextAdaptation();
			} finally {
						reqRes.close();
					}
					
			if (ccRecord == null) {						
						isValid= false;
					}
					else{
						String childRecord= ccRecord.getString(Path.parse(codeColumnID));
						if(childRecord.startsWith("RW")){
							isValid=false;
						}
						else{
							isValid=true;
						}
					}
		return isValid;
	}
	
	
	private boolean isValidChildRecord(Adaptation record ) {
		boolean isValid=false;
		
		String codeID= record.getString(Path.parse(codeColumnID));
		Path foreignTablePath = Path.parse(PMF); 
		AdaptationTable ccTable = record.getContainer().getTable(foreignTablePath);
		String predicate = parentColumnID + "=\"" + codeID + "\"";
		RequestResult reqRes = ccTable.createRequestResult(predicate);
		Adaptation ccRecord;
		try {
				ccRecord = reqRes.nextAdaptation();
		} finally {
					reqRes.close();
				}
				
		if (ccRecord == null) {						
					isValid= true;
				}
				else{
					String childRecord= ccRecord.getString(Path.parse(codeColumnID));
					if(childRecord.startsWith("RW")){
						isValid=true;
					}
					else{
						isValid=false;
					}
				}
	return isValid;
}

	public String getFieldName() {
		return fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public String getFieldValue() {
		return fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}


	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}


	public String getIsParent() {
		return isParent;
	}

	public void setParentColumnID(String parentColumnID) {
		this.parentColumnID = parentColumnID;
	}

	public String getParentColumnID() {
		return parentColumnID;
	}

	
}
