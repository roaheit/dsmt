package com.orchestranetworks.ps.filter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class CurrentRecordFilter implements AdaptationFilter {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private static final String MAX_END_DATE = "9999-12-31";
	private String effectiveDateField = "./EFFDT";
	
	private String endDateField = "./ENDDT";
	
	public String getEndDateField() {
		return endDateField;
	}

	public void setEndDateField(String endDateField) {
		this.endDateField = endDateField;
	}

	@Override
	public boolean accept(Adaptation adaptation) {
		final Date endDate = adaptation.getDate(Path.parse(endDateField));
		if (endDate == null) {
			return true;
		}
		
		String dateStr = dateFormat.format(endDate);
		return MAX_END_DATE.equals(dateStr) && !isFutureEffectiveDate(adaptation);
	}
	
	final SimpleDateFormat dateFormat = new SimpleDateFormat(DSMTConstants.DEFAULT_DATE_FORMAT);
	private boolean isFutureEffectiveDate(Adaptation adaptation){
		
		boolean isFutureEffectiveDate = false;
		Date currentDate = Calendar.getInstance().getTime();
		final Date effectiveDate = adaptation.getDate(Path.parse(effectiveDateField));
		
		isFutureEffectiveDate = currentDate.before(effectiveDate);
		if(isFutureEffectiveDate){	
			
			LOG.debug("This date is in future and cant be sent to export = " + effectiveDate + " for " + adaptation.getContainerTable().getTableNode().getLabel(Locale.getDefault()) + ":" + adaptation.getLabelOrName(Locale.getDefault()));
		}
		return isFutureEffectiveDate;
	}
}

