package com.orchestranetworks.ps.filter;

import java.util.Date;
import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class BD3RecordFilter implements AdaptationFilter {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	private String effectiveDateField = "./EFFDT";
	
	
	@Override
	public boolean accept(final Adaptation adaptation) {
		
		
		/* the effective date of the record is either previous month or before. */
		return  previousOrEarlierMonthRecord(adaptation);
	}

	/**
	 * return true only if effective date is of last month or earlier but not current month or future
	 * @param adaptation
	 * @return
	 */
	private boolean previousOrEarlierMonthRecord(Adaptation adaptation){
		
		boolean previousOrEarlierMonthRecord = true;
		final Date effectiveDate = adaptation.getDate(Path.parse(effectiveDateField));
		
		if(null != effectiveDate){
			/**set if effective date is not after the previous months effective date. */
			previousOrEarlierMonthRecord = ! effectiveDate.after(DSMTUtils.previousMonthEffectiveDate());
			if(previousOrEarlierMonthRecord){	
				
				LOG.debug("This date is in either current or future and cant be sent to export = " + effectiveDate + " for " + adaptation.getContainerTable().getTableNode().getLabel(Locale.getDefault()) + ":" + adaptation.getLabelOrName(Locale.getDefault()));
			}
		}
		
		return previousOrEarlierMonthRecord;
	}
	
	public void setEffectiveDateField(String effectiveDateField) {
		this.effectiveDateField = effectiveDateField;
	}

	public String getEffectiveDateField() {
		return effectiveDateField;
	}

	public static void main(String[] args) {
		
	}
}
