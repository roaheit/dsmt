package com.orchestranetworks.ps.filter;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;

public class GenericLeafRecordFilter extends CurrentRecordFilter{
	
	/**
	 * fieldName - field Name (e.g - "./PL_FLAG")
	 */
	private String fieldName;
	private String fieldValue;
	
	public String getFieldName() {
		return fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public String getFieldValue() {
		return fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}


	@Override
	public boolean accept(Adaptation adaptation) {
		
		boolean isCurrentRecord = super.accept(adaptation);
		boolean isAccepted = false;
		
		final String plFlag = adaptation.getString(Path.parse(fieldName));
		
		if (!fieldValue.equalsIgnoreCase(plFlag)) {
		
			isAccepted = false;
		} else if (fieldValue.equalsIgnoreCase(plFlag)) {
			
			isAccepted = true;
		}
		
		
		return isCurrentRecord && isAccepted;
	}

	
}