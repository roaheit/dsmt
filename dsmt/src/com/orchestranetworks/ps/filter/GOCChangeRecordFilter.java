package com.orchestranetworks.ps.filter;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class GOCChangeRecordFilter implements AdaptationFilter {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
			
	private String actionReq = "./ACTION_REQD";

	

	@Override
	public boolean accept(Adaptation adaptation) {
		final String recordChange = adaptation.getString(Path.parse(actionReq));
		if(null!=recordChange){
				if (recordChange.equalsIgnoreCase("Z")) {
					return false;
				}
				else{
					return true;
				}
		}else{
			return false;	
		}
		
		
		
		
	}
		

}
