package com.orchestranetworks.ps.filter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTUtils;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

/**
 * Duplicate of BD3FRSParentRecordFilter which refers to PL_FLAG instead of nodeFlag.
 * @author nc72931
 *
 */
public class BD3FRSParentRecordFilterWithPL extends CurrentRecordFilter  {

	
	private final String PARENT_ID = "./C_DSMT_FRS_O_PARNT";
	private final String nodeFlag = "./PL_FLAG";

	private String effectiveDateField = "./EFFDT";
	private String endDateField = "./ENDDT";
	
	@Override
	public boolean accept(Adaptation adaptation) {

		boolean isAccepted = super.accept(adaptation);

		final String parentID = adaptation.getString(Path.parse(PARENT_ID));
		final String rootNodeValue = adaptation.getString(Path
				.parse("./C_DSMT_FRS_OU"));
		final String rootNodeFlagValue = adaptation.getString(Path
				.parse(nodeFlag));
		final String rootNode = "700000";
		final String rootNodeFlag = "P";

		if ((null == parentID) && !(rootNode.equalsIgnoreCase(rootNodeValue)) && !(rootNodeFlag.equalsIgnoreCase(rootNodeFlagValue))) {
			// && "1".equals(adaptation.getString(Path.parse(level)))
			LOG.debug("returned false for C_DSMT_FRS_OU =  "	+ adaptation.getString(Path.parse("./C_DSMT_FRS_OU"))+ "rootNode" + rootNodeValue + "rootNodeFlag"	+ rootNodeFlagValue);
			isAccepted = false;

		}
		
		boolean isCurrentBD3Record = isCurrentBD3Record(adaptation, isAccepted);
		
		return isAccepted && isCurrentBD3Record;
		
	}

	
	
	public boolean isCurrentBD3Record(Adaptation adaptation, final boolean isAccepted) {
		final Date endDate = adaptation.getDate(Path.parse(endDateField));
		if (endDate == null) {
			return true;
		}
		final String dateStr = dateFormat.format(endDate);
		
		boolean isMaxEffectiveDated  = MAX_END_DATE.equals(dateStr);
		boolean isCurrentMonthEndDatedRecord = isCurrentMonthEndDatedRecord(dateStr) ;
		boolean ipreviousOrEarlierMonthRecords = previousOrEarlierMonthRecord(adaptation);
		
		LOG.debug( adaptation.getString(Path.parse("./C_DSMT_FRS_OU")) + " isAccepted = "  + isAccepted+ " - (isMaxEffectiveDated = " + isMaxEffectiveDated + ", isCurrentMonthEndDatedRecord =  " + isCurrentMonthEndDatedRecord + " ) and previousOrEarlierMonthRecord = " + ipreviousOrEarlierMonthRecords);
		return (isMaxEffectiveDated || isCurrentMonthEndDatedRecord) && ipreviousOrEarlierMonthRecords;
	
		// (all current records or current month end dated record ) && records less than or equal to previous month
	}
	
	/**
	 * return true only if effective date is of last month or earlier but not current month or future
	 * @param adaptation
	 * @return
	 */
	private boolean previousOrEarlierMonthRecord(Adaptation adaptation){
		
		boolean previousOrEarlierMonthRecord = true;
		final Date effectiveDate = adaptation.getDate(Path.parse(effectiveDateField));
		
		if(null != effectiveDate){
			/**set if effective date is not after the previous months effective date. */
			previousOrEarlierMonthRecord = ! effectiveDate.after(DSMTUtils.previousMonthEffectiveDate());
			if(! previousOrEarlierMonthRecord){	
				
				LOG.debug("This date is in either current or future and cant be sent to export = " + effectiveDate + " for " + adaptation.getContainerTable().getTableNode().getLabel(Locale.getDefault()) + ":" + adaptation.getLabelOrName(Locale.getDefault()));
			}
		}
		
		return previousOrEarlierMonthRecord;
	}
	
	/** returns true for a record which was end dated this month */
	private boolean isCurrentMonthEndDatedRecord(final String dateStr){
		
		final String currentMonthEffectiveDate = dateFormat.format(DSMTUtils.currentMonthEffectiveDate());
		if(currentMonthEffectiveDate.equalsIgnoreCase(dateStr)){
			return true;
		}else{
			return false;
		}
	}
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private static final String MAX_END_DATE = "9999-12-31";
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
}