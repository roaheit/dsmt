package com.orchestranetworks.ps.filter;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;


/**
 * 
 * Also make the same changes in the BD3FRSParentRecordFilter
 *
 */
public class FRSParentRecordFilterWithPL extends CurrentRecordFilter {
	private final String PARENT_ID = "./C_DSMT_FRS_O_PARNT";
	private final String plFlag = "./PL_FLAG";

	@Override
	public boolean accept(Adaptation adaptation) {

		boolean isAccepted = super.accept(adaptation);

		final String parentID = adaptation.getString(Path.parse(PARENT_ID));
		final String rootNodeValue = adaptation.getString(Path
				.parse("./C_DSMT_FRS_OU"));
		final String rootNodeFlagValue = adaptation.getString(Path
				.parse(plFlag));
		final String rootNode = "700000";
		final String rootNodeFlag = "P";

		if ((null == parentID) && !(rootNode.equalsIgnoreCase(rootNodeValue))
				&& !(rootNodeFlag.equalsIgnoreCase(rootNodeFlagValue))) {
			// && "1".equals(adaptation.getString(Path.parse(level)))
			LOG.debug("returned false for C_DSMT_FRS_OU =  "
					+ adaptation.getString(Path.parse("./C_DSMT_FRS_OU"))
					+ "rootNode" + rootNodeValue + "rootNodeFlag"
					+ rootNodeFlagValue);
			isAccepted = false;

		}
		return isAccepted;
	}

}