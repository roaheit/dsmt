package com.orchestranetworks.ps.filter;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.ValidationReport;

public class ValidationErrorRecordFilter implements AdaptationFilter {

	private String isError;
	

	public String getIsError() {
		return isError;
	}

	public void setIsError(String isError) {
		this.isError = isError;
	}

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	@Override
	public boolean accept(Adaptation record) {
		
			boolean inclusionflag=true;
			final ValidationReport validationReport = record.getValidationReport(false,false);
			if(isError.equals("true"))
			{
				if(validationReport.hasItemsOfSeverityOrMore(Severity.ERROR))
				{
					inclusionflag=true;
				}
				else
				{
					inclusionflag=false;
				}
			}
			else
			{
				if(validationReport.hasItemsOfSeverityOrMore(Severity.ERROR))
				{
					inclusionflag=false;
				}
				else{
					inclusionflag=true;
				}
			}
			return inclusionflag;
		}
}