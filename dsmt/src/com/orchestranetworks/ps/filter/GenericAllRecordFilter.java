package com.orchestranetworks.ps.filter;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class GenericAllRecordFilter implements AdaptationFilter   {
	
	/**
	 * fieldName - field Name (e.g - "./PL_FLAG")
	 */
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private String fieldName;
	private String fieldValue;
	private String isParent;
	
	// private final String PARENT_ID = "./PARENT_ID";
	
	private String parentColumnID = "./PARENT_ID";
	

	@Override
	public boolean accept(Adaptation adaptation) {
		
		boolean isAccepted = false;
		
		final String plFlag = adaptation.getString(Path.parse(fieldName));
		
		if("true".equalsIgnoreCase(isParent)){
		
			final String parentID = adaptation.getString(Path.parse(parentColumnID));
			if(null == parentID){
				return false;
			}
			
		}
		
		if (!fieldValue.equalsIgnoreCase(plFlag)) {
		
			isAccepted = false;
		} else if (fieldValue.equalsIgnoreCase(plFlag)) {
			isAccepted = true;
		}
				
		return  isAccepted;
	}

	public String getFieldName() {
		return fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public String getFieldValue() {
		return fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}


	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}


	public String getIsParent() {
		return isParent;
	}

	public void setParentColumnID(String parentColumnID) {
		this.parentColumnID = parentColumnID;
	}

	public String getParentColumnID() {
		return parentColumnID;
	}

	
}