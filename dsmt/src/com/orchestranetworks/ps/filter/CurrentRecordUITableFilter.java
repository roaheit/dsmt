package com.orchestranetworks.ps.filter;

import com.orchestranetworks.ui.UITableFilter;
import com.orchestranetworks.ui.UITableFilterRequestContext;
import com.orchestranetworks.ui.UITableFilterResponseContext;

public class CurrentRecordUITableFilter extends UITableFilter {
	private String endDateField;

	public String getEndDateField() {
		return endDateField;
	}

	public void setEndDateField(String endDateField) {
		this.endDateField = endDateField;
	}

	@Override
	public void addForEdit(UITableFilterResponseContext context) {
		// Do nothing, no custom UI needed
	}

	@Override
	public void handleApply(UITableFilterRequestContext context) {
		final CurrentRecordFilter filter = new CurrentRecordFilter();
		if (endDateField != null && ! "".equals(endDateField)) {
			filter.setEndDateField(endDateField);
		}
		context.setTableFilter(filter);
	}
}
