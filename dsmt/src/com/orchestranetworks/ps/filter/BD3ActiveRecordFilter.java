package com.orchestranetworks.ps.filter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.ValidationReport;

public class BD3ActiveRecordFilter implements AdaptationFilter {
private static final String YYYY_MM_DD = "yyyy-MM-dd";


private static final String A = "A";


private static final String MAX_END_DATE = "9999-12-31";
	

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	private String endDateField = "./ENDDT";
	private String effectiveStatusField = "./EFF_STATUS";
	private String effectiveDateField = "./EFFDT";
	private String validRecordsOnly = "false";
	
	public String getEffectiveStatusField() {
		return effectiveStatusField;
	}

	public void setEffectiveStatusField(String effectiveStatusField) {
		this.effectiveStatusField = effectiveStatusField;
	}

	public String getEndDateField() {
		return endDateField;
	}

	public void setEndDateField(String endDateField) {
		this.endDateField = endDateField;
	}

	public String getValidRecordsOnly() {
		return validRecordsOnly;
	}

	public void setValidRecordsOnly(String validRecordsOnly) {
		this.validRecordsOnly = validRecordsOnly;
	}

	@Override
	public boolean accept(final Adaptation adaptation) {
		
		final Date endDate = adaptation.getDate(Path.parse(endDateField));
		final String effectiveStatus = adaptation.getString(Path.parse(effectiveStatusField));
		
		if(! A.equalsIgnoreCase(effectiveStatus)){
			return false;
		}
		else if (endDate == null && A.equalsIgnoreCase(effectiveStatus)) {
			return true;
		} 
		
		final String dateStr = dateFormat.format(endDate);
		
		/* end date is either 12-31-9999 or current month and the record is effective && the effective date of the record is either previous month or before. */
		final boolean isActive = (MAX_END_DATE.equals(dateStr) || isCurrentMonthEndDatedRecord(dateStr))  &&  A.equalsIgnoreCase(effectiveStatus) && previousOrEarlierMonthRecord(adaptation) ;
		if (isActive && Boolean.valueOf(validRecordsOnly).booleanValue()) {
			final ValidationReport validationReport = adaptation.getValidationReport();
			return ! validationReport.hasItemsOfSeverityOrMore(Severity.ERROR);
		}
		return isActive;
	}

	/**
	 * return true only if effective date is of last month or earlier but not current month or future
	 * @param adaptation
	 * @return
	 */
	private boolean previousOrEarlierMonthRecord(Adaptation adaptation){
		
		boolean previousOrEarlierMonthRecord = true;
		final Date effectiveDate = adaptation.getDate(Path.parse(effectiveDateField));
		
		if(null != effectiveDate){
			/**set if effective date is not after the previous months effective date. */
			previousOrEarlierMonthRecord = ! effectiveDate.after(DSMTUtils.previousMonthEffectiveDate());
			if(previousOrEarlierMonthRecord){	
				
				LOG.debug("This date is in either current or future and cant be sent to export = " + effectiveDate + " for " + adaptation.getContainerTable().getTableNode().getLabel(Locale.getDefault()) + ":" + adaptation.getLabelOrName(Locale.getDefault()));
			}
		}
		
		return previousOrEarlierMonthRecord;
	}
	
	/** returns true for a record which was end dated this month */
	private boolean isCurrentMonthEndDatedRecord(final String dateStr){
		
		final String currentMonthEffectiveDate = dateFormat.format(DSMTUtils.currentMonthEffectiveDate());
		if(currentMonthEffectiveDate.equalsIgnoreCase(dateStr)){
			return true;
		}else{
			return false;
		}
	}
	

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	
	public void setEffectiveDateField(String effectiveDateField) {
		this.effectiveDateField = effectiveDateField;
	}

	public String getEffectiveDateField() {
		return effectiveDateField;
	}

	public static void main(String[] args) {
		
		final Date endDate = null;
		final SimpleDateFormat dateFormat = new SimpleDateFormat(YYYY_MM_DD);
		
		//System.out.println(dateFormat.format(endDate));
	}
}
