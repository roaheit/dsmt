package com.orchestranetworks.ps.filter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class BD3CurrentRecordFilter implements AdaptationFilter {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private static final String MAX_END_DATE = "9999-12-31";
	private String effectiveDateField = "./EFFDT";
	
	private String endDateField = "./ENDDT";
	
	public String getEndDateField() {
		return endDateField;
	}

	public void setEndDateField(String endDateField) {
		this.endDateField = endDateField;
	}

	@Override
	public boolean accept(Adaptation adaptation) {
		
		
		final Date endDate = adaptation.getDate(Path.parse(endDateField));
		if (endDate == null) {
			return true;
		}
		final String dateStr = dateFormat.format(endDate);
		
		return (MAX_END_DATE.equals(dateStr) || isCurrentMonthEndDatedRecord(dateStr)) && previousOrEarlierMonthRecord(adaptation);
	
		// (all current records or current month end dated record ) && records less than or equal to previous month
	}
	
	/**
	 * return true only if effective date is of last month or earlier but not current month or future
	 * @param adaptation
	 * @return
	 */
	private boolean previousOrEarlierMonthRecord(Adaptation adaptation){
		
		boolean previousOrEarlierMonthRecord = true;
		final Date effectiveDate = adaptation.getDate(Path.parse(effectiveDateField));
		
		if(null != effectiveDate){
			/**set if effective date is not after the previous months effective date. */
			previousOrEarlierMonthRecord = ! effectiveDate.after(DSMTUtils.previousMonthEffectiveDate());
			if(! previousOrEarlierMonthRecord){	
				
				LOG.debug("This date is in either current or future and cant be sent to export = " + effectiveDate + " for " + adaptation.getContainerTable().getTableNode().getLabel(Locale.getDefault()) + ":" + adaptation.getLabelOrName(Locale.getDefault()));
			}
		}
		
		return previousOrEarlierMonthRecord;
	}
	
	/** returns true for a record which was end dated this month */
	private boolean isCurrentMonthEndDatedRecord(final String dateStr){
		
		final String currentMonthEffectiveDate = dateFormat.format(DSMTUtils.currentMonthEffectiveDate());
		if(currentMonthEffectiveDate.equalsIgnoreCase(dateStr)){
			return true;
		}else{
			return false;
		}
	}
	

	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

}

