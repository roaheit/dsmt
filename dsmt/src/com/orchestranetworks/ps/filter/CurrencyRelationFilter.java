package com.orchestranetworks.ps.filter;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.service.LoggingCategory;

public class CurrencyRelationFilter implements AdaptationFilter {
	private String Ctry_Code = "./COUNTRY";	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();


	public String getCtry_Code() {
		return Ctry_Code;
	}

	public void setCtry_Code(String ctry_Code) {
		Ctry_Code = ctry_Code;
	}

	@Override
	public boolean accept(Adaptation adaptation) {

		boolean isAccepted = true;

		final String countryCode = adaptation.getString(Path.parse(Ctry_Code));

		if (null == countryCode) {
			try {
			LOG.debug("returned false for COUNTRY code =  "	+ adaptation.getString(Path.parse(Ctry_Code)));
			isAccepted = false;
			} catch (PathAccessException e) {
				e.printStackTrace();
			}
		}
		return isAccepted;
	}

}