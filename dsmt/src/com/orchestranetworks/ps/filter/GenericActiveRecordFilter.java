package com.orchestranetworks.ps.filter;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;

public class GenericActiveRecordFilter extends ActiveRecordFilter{
	
	/**
	 * fieldName - field Name (e.g - "./PL_FLAG")
	 */
	private String fieldName;
	private String fieldValue;
	private String isParent;
	
	private final String PARENT_ID = "./PARENT_ID";
	
	

	@Override
	public boolean accept(Adaptation adaptation) {
		
		boolean isRecordActive = super.accept(adaptation);
		boolean isAccepted = false;
		
		final String plFlag = adaptation.getString(Path.parse(fieldName));
		
		if("true".equalsIgnoreCase(isParent)){
		
			final String parentID = adaptation.getString(Path.parse(PARENT_ID));
			if(null == parentID){
				try {
					LOG.debug("returned false for C_DSMT_PRODUCT_TYP =  " + adaptation.getString(Path.parse("./C_DSMT_PRODUCT_TYP")));
				} catch (PathAccessException e) {
					e.printStackTrace();
				}
				return false;
			}
			
		}
		
		if (!fieldValue.equalsIgnoreCase(plFlag)) {
		
			isAccepted = false;
		} else if (fieldValue.equalsIgnoreCase(plFlag)) {
			isAccepted = true;
		}
				
		return isRecordActive && isAccepted;
	}

	public String getFieldName() {
		return fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public String getFieldValue() {
		return fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}


	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}


	public String getIsParent() {
		return isParent;
	}

	
}