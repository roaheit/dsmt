package com.orchestranetworks.ps.filter;

import java.util.Calendar;
import java.util.Date;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class FRSIncrementalRecordFilter implements AdaptationFilter{
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
			
	private String updatedTime = "./LAST_MODIFIED_TIME";
	private String filterRange;
	
	@Override
	public boolean accept(Adaptation adaptation) {
		
		
		// boolean isCurrent = super.accept(adaptation);
		final Date recordChangeDate = adaptation.getDate(Path.parse(updatedTime));
		if (recordChangeDate == null) {
			return false;
		}
		else{
			return  recodInDateRange(recordChangeDate);
		}
		
		
		
	}
	
	private boolean recodInDateRange(Date  recordDate)
    {
		 
		 boolean isValidDate =false;
		 Calendar calendar = Calendar.getInstance();
		 int dateRange=1;
		 Date endDate=calendar.getTime();
			if(filterRange!=null){
				dateRange=Integer.parseInt(filterRange);
			}
		
		
				Calendar startDate = Calendar.getInstance();
		
				startDate.set(Calendar.DATE, calendar.get(Calendar.DATE)-dateRange);
        
        	    isValidDate = recordDate.after(startDate.getTime()) && recordDate.before(endDate) ;
        return isValidDate;
        
    }

	
	/**
	 * @return the updatedTime
	 */
	public String getUpdatedTime() {
		return updatedTime;
	}

	/**
	 * @param updatedTime the updatedTime to set
	 */
	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}

	/**
	 * @return the filterRange
	 */
	public String getFilterRange() {
		return filterRange;
	}

	/**
	 * @param filterRange the filterRange to set
	 */
	public void setFilterRange(String filterRange) {
		this.filterRange = filterRange;
	}

	

	

}
