package com.orchestranetworks.ps.filter;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class ISOStandarrdFilter extends CurrentRecordFilter {

private String ISOStandard = "./ISO_Standard";
protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	public String getISOStandard() {
		return ISOStandard;
	}

	public void setISOStandard(String iSOStandard) {
		this.ISOStandard = iSOStandard;
	}

	@Override
	public boolean accept(Adaptation adaptation) {
		final String isoStandard = adaptation.getString(Path.parse(ISOStandard));
		if (isoStandard == null || isoStandard.equals("N") ) {
			LOG.debug("ISO Standard is set to NO");
			return false;
		
		}
		
		else
		{
			LOG.debug("ISO Standard is set to Yes");
			return true;
		}
		}

}
