package com.orchestranetworks.ps.filter;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;

public class AccountModuleRLFilter extends CurrentRecordFilter{
	
	/**
	 * fieldName - field Name (e.g - "./PL_FLAG")
	 */
	private String fieldName;
	private String fieldValue;
	private String isParent;
	
	// private final String PARENT_ID = "./PARENT_ID";
	
	private String parentColumnID = "./PARENT_ID";
	

	@Override
	public boolean accept(Adaptation adaptation) {
		
		boolean isCurrentRecord = super.accept(adaptation);
		boolean isAccepted = false;
		
		final String plFlag = adaptation.getString(Path.parse(fieldName));
		
		
		
			final String parentID = adaptation.getString(Path.parse(parentColumnID));
			if(null == parentID){
				
				return false;
			}
			
	
		
		if (!fieldValue.equalsIgnoreCase(plFlag)) {
		
			isAccepted = false;
		} else if (fieldValue.equalsIgnoreCase(plFlag)) {
			isAccepted = true;
		}
				
		return isCurrentRecord && isAccepted;
	}

	public String getFieldName() {
		return fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public String getFieldValue() {
		return fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}


	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}


	public String getIsParent() {
		return isParent;
	}

	public void setParentColumnID(String parentColumnID) {
		this.parentColumnID = parentColumnID;
	}

	public String getParentColumnID() {
		return parentColumnID;
	}

	
}
