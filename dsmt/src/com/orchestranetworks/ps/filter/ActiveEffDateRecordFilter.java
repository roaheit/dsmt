package com.orchestranetworks.ps.filter;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;

public class ActiveEffDateRecordFilter extends EffDateRecordFilter{
	
	
	
	private String effectiveStatusField = "./EFF_STATUS";
	
	public String getEffectiveStatusField() {
		return effectiveStatusField;
	}

	public void setEffectiveStatusField(String effectiveStatusField) {
		this.effectiveStatusField = effectiveStatusField;
	}
	
	
	@Override
	public boolean accept(Adaptation adaptation) {
		
		boolean isAccepted = super.accept(adaptation);
		final String effectiveStatus = adaptation.getString(Path.parse(effectiveStatusField));
		
		if(isAccepted && DSMTConstants.ACTIVE.equalsIgnoreCase(effectiveStatus)){
			return true;
		}
		else{
			return false;
		}
	}

}
