package com.orchestranetworks.ps.workflow;

import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.workflow.InsertApprovalWFReportingData;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Profile;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

public class ApproverCalculationConditionBean extends ConditionBean{

	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	private String ApproverLevel;
	private String Approver;
	private String loop_count;
	private String tableXPath;
	private String profile;
	boolean valid=false;
	InsertApprovalWFReportingData reporting= new InsertApprovalWFReportingData();
	
	public String getTableXPath() {
		return tableXPath;
	}
	public void setTableXPath(String tableXPath) {
		this.tableXPath = tableXPath;
	}
	public String getLoop_count() {
		return loop_count;
	}
	public void setLoop_count(String loop_count) {
		this.loop_count = loop_count;
	}
	public String getApproverLevel() {
		return ApproverLevel;
	}
	public void setApproverLevel(String approverLevel) {
		ApproverLevel = approverLevel;
	}
	public String getApprover() {
		return Approver;
	}
	public void setApprover(String approver) {
		Approver = approver;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	private Path getPath(final String fieldName) {

		return Path.parse(fieldName);
	}

	@Override
	public boolean evaluateCondition(ConditionBeanContext context)
			throws OperationException {
		final Profile profileRef = Profile.parse(profile);
		UserReference userRef = (UserReference) profileRef;
		String requestor=userRef.getUserId();
	int count=Integer.parseInt(loop_count);
	int level=Integer.parseInt(ApproverLevel);
	LOG.info("loop_count"+loop_count);
	LOG.info("ApproverLevel"+ApproverLevel);
	LOG.info("valid"+valid);
	if(count<=level)
	{
		Repository repo=context.getRepository();
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
				"DSMT_WF", "DSMT_WF");
		final AdaptationTable targetTable = metadataDataSet
				.getTable(getPath("/root/Approval"));	
		String[] tablePath=tableXPath.split("/");
		String tableName=tablePath[tablePath.length-1];
		String pred1= GOCWFPaths._Approval._Table_Path.format() + " = '"
				+ tableName + "'"+" and "+GOCWFPaths._Approval._Level.format()+" = '"+loop_count+"'";
		RequestResult rs = targetTable.createRequestResult(pred1);
		LOG.info("Record Count"+rs.getSize());
		String approver = "";
		String approver_list="";
		for(Adaptation record;(record = rs.nextAdaptation()) != null;)
		{
			approver=record.getString(GOCWFPaths._Approval._Approver);
			if(!approver.equalsIgnoreCase(requestor))
			{
				approver_list=approver_list+approver+",";
			}
		}
		Approver=approver_list;
		LOG.info("calling update approver ");
		reporting.updateApprover(Approver, count);
		count++;
		loop_count=String.valueOf(count);
		valid=true;
	}
	LOG.info("requestor"+requestor);
	LOG.info("Approver"+Approver);
	return valid;
	}
}