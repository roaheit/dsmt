/**
 * 
 */
package com.orchestranetworks.ps.workflow;

import java.util.Calendar;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.DSMT2WFPaths;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.workflow.InsertApprovalWFReportingData;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

/**
 * @author rk00242
 * 
 */
public class BDCalculationConditionBean extends ConditionBean {

	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	InsertApprovalWFReportingData reporting = new InsertApprovalWFReportingData();

	@Override
	public boolean evaluateCondition(ConditionBeanContext context)
			throws OperationException {
		// TODO Auto-generated method stub
		Repository repo = context.getRepository();
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
				"DSMT_WF", "DSMT_WF");
		final AdaptationTable targetTable = metadataDataSet
				.getTable(getPath("/root/C_DSMT_WF_CAL"));

		Date now = Calendar.getInstance().getTime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		month = month + 1;
		int date = cal.get(Calendar.DATE);

		final String pred = DSMT2WFPaths._C_DSMT_WF_CAL._MONTH.format() + "= '"
				+ month + "' and " + DSMT2WFPaths._C_DSMT_WF_CAL._YEAR.format()
				+ "= '" + year + "'";

		RequestResult reqRes = targetTable.createRequestResult(pred);
		Adaptation record = reqRes.nextAdaptation();
		if (record != null) {

			final int endDate = record
					.get_int(DSMT2WFPaths._C_DSMT_WF_CAL._REG_END_DATE);
			final int startDate = record
					.get_int(DSMT2WFPaths._C_DSMT_WF_CAL._REG_START_DATE);

			if (date >= startDate && date <= endDate) {
				LOG.info("Current Date is between Request Start and end Date, Proceeding for Data Space Merge ");
				return true;
			} else {
				LOG.info("Current Date does not lies in between Request Start and end date , Workflow status is Pending for Merge");
				reporting.updatePendingStatus(DSMTConstants.PENDING_STATUS);
				return false;
			}

		}

		LOG.info("No record is present for in DSMT2 WF calendar setup for current month and year, Proceeding for Data Space Merge  ");
		return true;
	}

	private Path getPath(final String fieldName) {

		return Path.parse(fieldName);
	}
}
