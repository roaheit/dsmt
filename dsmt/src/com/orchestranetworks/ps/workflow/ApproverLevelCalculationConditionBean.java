package com.orchestranetworks.ps.workflow;

import java.util.HashMap;

import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Profile;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

public class ApproverLevelCalculationConditionBean extends ConditionBean {

	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	private String ApproverLevel;
	private String Approver;
	private String loop_count;
	private String tableXPath;
	private String profile;
	boolean valid = false;
	private int requestID;
	private String tableName;
	
	

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public int getRequestID() {
		return requestID;
	}

	public void setRequestID(int requestID) {
		this.requestID = requestID;
	}

	public String getTableXPath() {
		return tableXPath;
	}

	public void setTableXPath(String tableXPath) {
		this.tableXPath = tableXPath;
	}

	public String getLoop_count() {
		return loop_count;
	}

	public void setLoop_count(String loop_count) {
		this.loop_count = loop_count;
	}

	public String getApproverLevel() {
		return ApproverLevel;
	}

	public void setApproverLevel(String approverLevel) {
		ApproverLevel = approverLevel;
	}

	public String getApprover() {
		return Approver;
	}

	public void setApprover(String approver) {
		Approver = approver;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	@Override
	public boolean evaluateCondition(ConditionBeanContext context)
			throws OperationException {
		final Profile profileRef = Profile.parse(profile);
		UserReference userRef = (UserReference) profileRef;
		String requestor = userRef.getUserId();
		String subject = "Workflow Approver Unavailable for "+tableName;
		String message="There is no approver configured for "+tableName+ " in Approver Table for Workflow ID - "+requestID;
		HashMap<String, String> map = new HashMap<String, String>();
		
		map.put("TableName", tableName);
		map.put("RequestID", String.valueOf(requestID));
		map.put("Requestor", profile.replace("U", ""));

		int count = Integer.parseInt(loop_count);

		if (count > 0) {
			valid = true;
		} else {
			LOG.info("requestor" + requestor);
			DSMTNotificationService.notifyEmailID(requestor, subject, message, false, map);
			
		}

		return valid;
	}
}