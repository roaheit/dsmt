package com.orchestranetworks.ps.workflow;

import java.util.HashMap;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.workflow.InsertApprovalWFReportingData;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.workflow.CreationWorkItemSpec;
import com.orchestranetworks.workflow.UserTask;
import com.orchestranetworks.workflow.UserTaskCreationContext;
import com.orchestranetworks.workflow.UserTaskWorkItemCompletionContext;

public class ApproverAllocatedUserTask extends UserTask {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	InsertApprovalWFReportingData reporting = new InsertApprovalWFReportingData();
	private String profile;
	private String allocatedUserParam;
	private String userComment;
	private String Approver;
	private String tableXPath;
	private String tableID;
	private String tableName;
	private String requestID;
	private String workflowStatus;
	private String label;
	private String loop_count;

	public String getApprover() {
		return Approver;
	}

	public void setApprover(String approver) {
		Approver = approver;
	}

	public String getLoop_count() {
		return loop_count;
	}

	public void setLoop_count(String loop_count) {
		this.loop_count = loop_count;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getAllocatedUserParam() {
		return allocatedUserParam;
	}

	public void setAllocatedUserParam(String allocatedUserParam) {
		this.allocatedUserParam = allocatedUserParam;
	}

	public void setTableXPath(String tableXPath) {
		this.tableXPath = tableXPath;
	}

	public String getTableXPath() {
		return tableXPath;
	}

	public String getTableID() {
		return tableID;
	}

	public void setTableID(String tableID) {
		this.tableID = tableID;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	@Override
	public void handleCreate(UserTaskCreationContext context)
			throws OperationException {
		LOG.info("Approver :::" + Approver);
		String[] approver_list = Approver.split(",");
		String subject = "(Simplified DSMT2 Workflow) Workflow Approval Pending";
		String message = "Workflow ID "
				+ requestID
				+ " is pending your approval. Please login to DSMT2 inbox and approve/reject the request.";
		LOG.info("Approver_list length " + approver_list.length);
		HashMap<String, String> map = new HashMap<String, String>();


		if (approver_list.length != 0 && !approver_list[0].isEmpty()
				&& !approver_list[0].equals(null)) {
			
			map.put("TableName", tableName);
			map.put("RequestID", requestID);
			map.put("Requestor", profile.replace("U", ""));
			//map.put("Approvers", approver_list.toString());
			
			for (int i = 0; i < approver_list.length; i++) {
				LOG.info("Approver_list" + approver_list[i]);
				if (approver_list[i] != null && !approver_list[i].equals(null)) {
					UserReference userRefApprover = UserReference
							.forUser(approver_list[i]);
					LOG.info("userRefApprover" + userRefApprover.getUserId());
					CreationWorkItemSpec creationWorkItemSpec;

					if (userRefApprover.getUserId().length() > DSMTConstants.CONSTANT_LENGTH) {
						Role dl = Role.forSpecificRole(userRefApprover
								.getUserId());
						creationWorkItemSpec = CreationWorkItemSpec
								.forOffer(dl);
					} else {
						creationWorkItemSpec = CreationWorkItemSpec
								.forAllocationWithPossibleReallocation(
										userRefApprover, (Role) Role
												.parse((String) context
														.getProfiles().get(0)));
					}
					creationWorkItemSpec.setNotificationMail(context
							.getAllocatedToNotificationMail());

					if (requestID != null) {
						creationWorkItemSpec.setSpecificLabel(UserMessage
								.createInfo("Workflow Request ID - "
										+ requestID));
					} else {
						creationWorkItemSpec.setSpecificLabel(UserMessage
								.createInfo("Request Changes for '" + tableID
										+ "'"));
					}
					context.createWorkItem(creationWorkItemSpec);
				}
				
		

				DSMTNotificationService.notifyEmailID(approver_list[i],
						subject, message, false, map);
			}

			return;
		} else {
			Repository repo = context.getRepository();
			String defaultApprover;
			int level = Integer.parseInt(loop_count);
			String subj = "(Simplified DSMT2 Workflow) Workflow Approval went to Default Approvers.";
			String mess = "Workflow ID "
					+ requestID
					+ " went to default Approvers since no approvers are configured for "
					+ tableName + " at level " + (level - 1);

			LOG.info("in approver allocated user task..cehck requestor "
					+ profile);
			LOG.info("check table name " + tableName);
			LOG.info("loop count " + loop_count);
			
			map.put("TableName", tableName);
			map.put("RequestID", requestID);
			map.put("Requestor", profile.replace("U", ""));
		//	map.put("Approvers", approver_list.toString());

			Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
					"DSMT_WF", "DSMT_WF");
			final AdaptationTable targetTable = metadataDataSet
					.getTable(getPath("/root/Approval"));

			final String pred = GOCWFPaths._Approval._Table_Path.format()
					+ " = '" + DSMTConstants.DEFAULT_APPROVER + "'";

			RequestResult req = targetTable.createRequestResult(pred);
			LOG.info("cehck aproval table size for default " + req.getSize());
			for (int i = 0; i < req.getSize(); i++) {

				Adaptation record = req.nextAdaptation();
				defaultApprover = record
						.getString(GOCWFPaths._Approval._Approver);
				String[] defaultApprover_list = defaultApprover.split(",");
				for (int j = 0; j < defaultApprover_list.length; j++) {

					UserReference userRefApprover = UserReference
							.forUser(defaultApprover_list[j]);
					LOG.info("userRefApprover" + userRefApprover.getUserId());
					CreationWorkItemSpec creationWorkItemSpec;
					
					if (userRefApprover.getUserId().length() > DSMTConstants.CONSTANT_LENGTH) {
						Role dl = Role.forSpecificRole(userRefApprover
								.getUserId());
						creationWorkItemSpec = CreationWorkItemSpec
								.forOffer(dl);
					} else {
						creationWorkItemSpec = CreationWorkItemSpec
								.forAllocationWithPossibleReallocation(
										userRefApprover, (Role) Role
												.parse((String) context
														.getProfiles().get(0)));
					}
					creationWorkItemSpec.setNotificationMail(context
							.getAllocatedToNotificationMail());

					if (requestID != null) {
						creationWorkItemSpec.setSpecificLabel(UserMessage
								.createInfo("Workflow Request ID - "
										+ requestID));
					} else {
						creationWorkItemSpec.setSpecificLabel(UserMessage
								.createInfo("Request Changes for '" + tableID
										+ "'"));
					}
					context.createWorkItem(creationWorkItemSpec);

					DSMTNotificationService.notifyEmailID(
							defaultApprover_list[j], subject, message, false,map);
				}
				DSMTNotificationService.notifyEmailID(profile.replace("U", ""),
						subj, mess, false,map);
				return;
			}
		}
		super.handleCreate(context);
	}

	@Override
	public void handleWorkItemCompletion(
			UserTaskWorkItemCompletionContext context)
			throws OperationException {
		final WorkItem workItem = context.getCompletedWorkItem();
		final UserReference userReference = workItem.getUserReference();
		
		context.setVariableString(allocatedUserParam, userReference.getUserId());
		context.setVariableString(userComment, context.getCompletedWorkItem()
				.getComment());
		reporting.updateAllocatedUser(userReference.getUserId());
		try {
			if (context.getCompletedWorkItem() != null) {
				Boolean workItemRejectionStatus = context
						.getCompletedWorkItem().isRejected();
				LOG.debug("Rejection status of the current work item is: "
						+ workItemRejectionStatus);
				if (workItemRejectionStatus != null && workItemRejectionStatus) {
					// complete the user task even if 1 checker rejects the work
					// item
					
					context.completeUserTask();
				}
			}
			super.handleWorkItemCompletion(context);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}

	private Path getPath(final String fieldName) {

		return Path.parse(fieldName);
	}
}
