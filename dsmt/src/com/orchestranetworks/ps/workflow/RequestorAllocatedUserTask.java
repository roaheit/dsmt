package com.orchestranetworks.ps.workflow;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Profile;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryHandler;
import com.orchestranetworks.workflow.CreationWorkItemSpec;
import com.orchestranetworks.workflow.UserTask;
import com.orchestranetworks.workflow.UserTaskCreationContext;
import com.orchestranetworks.workflow.UserTaskWorkItemCompletionContext;

public class RequestorAllocatedUserTask extends UserTask {
	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	private String profile;
	private String allocatedUserParam;
	private String userComment;
	
	private String tableXPath;
	private String tableID;
	private String tableName;
	private String requestID;
	private String workflowStatus;
	private String label;
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getAllocatedUserParam() {
		return allocatedUserParam;
	}

	public void setAllocatedUserParam(String allocatedUserParam) {
		this.allocatedUserParam = allocatedUserParam;
	}

	public void setTableXPath(String tableXPath) {
		this.tableXPath = tableXPath;
	}

	public String getTableXPath() {
		return tableXPath;
	}

	public String getTableID() {
		return tableID;
	}

	public void setTableID(String tableID) {
		this.tableID = tableID;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	@Override
	public void handleCreate(UserTaskCreationContext context)
			throws OperationException {
		LOG.info("Inside Handle Create");
		if (profile != null && ! "".equals(profile)) {
			final Profile profileRef = Profile.parse(profile);
			final DirectoryHandler dirHandler = DirectoryHandler.getInstance(
					context.getRepository());
			if (dirHandler.isProfileDefined(profileRef)) {
				final CreationWorkItemSpec creationWorkItemSpec;
				if (profileRef.isUserReference()) {
					final UserReference userRef = (UserReference) profileRef;
					creationWorkItemSpec = CreationWorkItemSpec.forAllocationWithPossibleReallocation(
							userRef, (Role) Role.parse((String) context.getProfiles().get(0)));
					creationWorkItemSpec.setNotificationMail(context.getAllocatedToNotificationMail());
				} else {
					final Role roleRef = (Role) profileRef;
					creationWorkItemSpec = CreationWorkItemSpec.forOffer(roleRef);
					creationWorkItemSpec.setNotificationMail(context.getOfferedToNotificationMail());
				}
				
				if(null != tableName){
					if(requestID!=null)
					{
					creationWorkItemSpec.setSpecificLabel(UserMessage.createInfo("Workflow Request ID - " + requestID + " . Request Changes for '" + tableName + "'"));
					}
					else
					{
						creationWorkItemSpec.setSpecificLabel(UserMessage.createInfo("Request Changes for '" + tableName + "'"));
					}
				}else if (null != profile){
					
					creationWorkItemSpec.setSpecificLabel(UserMessage.createInfo(profile.replace("U","")  + ": " + label));
				}
				context.createWorkItem(creationWorkItemSpec);
		        return;
		      }
		}
		super.handleCreate(context);
	}
	
/*	public static void main(String[] args) {
		System.out.println("nc72931".replace("U", ""));
	}*/
	
	@Override
	  public void handleWorkItemCompletion(UserTaskWorkItemCompletionContext context)
	      throws OperationException {
		LOG.info("Inside Handle Completion");

		final WorkItem workItem = context.getCompletedWorkItem();
	    final UserReference userReference = workItem.getUserReference();
	    context.setVariableString(allocatedUserParam, userReference.getUserId());
	    context.setVariableString(userComment,  context.getCompletedWorkItem().getComment());

	    
		try {
			super.handleWorkItemCompletion(context);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}
}
