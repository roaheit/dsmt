package com.orchestranetworks.ps.workflow;

import java.util.ArrayList;
import java.util.List;

import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;
import com.orchestranetworks.instance.Repository;

import java.util.Collections;

public class ApproverLevelCalculationScriptTask extends ScriptTaskBean {

	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	private String ApproverLevel;
	private String tableXPath;
	private String loop_count;

	public String getLoop_count() {
		return loop_count;
	}

	public void setLoop_count(String loop_count) {
		this.loop_count = loop_count;
	}

	public String getApproverLevel() {
		return ApproverLevel;
	}

	public void setApproverLevel(String approverLevel) {
		ApproverLevel = approverLevel;
	}

	private Path getPath(final String fieldName) {

		return Path.parse(fieldName);
	}

	public void setTableXPath(String tableXPath) {
		this.tableXPath = tableXPath;
	}

	public String getTableXPath() {
		return tableXPath;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		Repository repo = context.getRepository();
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
				"DSMT_WF", "DSMT_WF");
		final AdaptationTable targetTable = metadataDataSet
				.getTable(getPath("/root/Approval"));
		String[] tablePath = tableXPath.split("/");
		String tableName = tablePath[tablePath.length - 1];
		String pred1 = GOCWFPaths._Approval._Table_Path.format() + " = '"
				+ tableName + "'";
		RequestResult rs = targetTable.createRequestResult(pred1);
		List<Integer> levels = new ArrayList<Integer>();
		if (rs.getSize() > 0) {
			for (Adaptation record; (record = rs.nextAdaptation()) != null;) {
				levels.add(record.get_int(GOCWFPaths._Approval._Level));
			}
			ApproverLevel = String.valueOf(Collections.max(levels));
			LOG.info("Approval Level" + ApproverLevel);
			loop_count = "0";
			int count = Integer.parseInt(loop_count);
			count++;
			loop_count = String.valueOf(count);
		} else {
			loop_count = "0";
		}

	}
}
