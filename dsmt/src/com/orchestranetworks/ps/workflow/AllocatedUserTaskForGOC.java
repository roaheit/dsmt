package com.orchestranetworks.ps.workflow;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Profile;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryHandler;
import com.orchestranetworks.workflow.CreationWorkItemSpec;
import com.orchestranetworks.workflow.UserTask;
import com.orchestranetworks.workflow.UserTaskCreationContext;
import com.orchestranetworks.workflow.UserTaskWorkItemCompletionContext;

public class AllocatedUserTaskForGOC extends UserTask {
	
//	private static final String REQUEST_CHANGES_FOR = "Request Changes for ";
	private String profile;
	private String allocatedUserParam;
	private String userComment;
	
	private String tableName;
	private String maintenanceType;
	private String requestID;
	private String workflowStatus;
	private String workflowStepName;
	
	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getAllocatedUserParam() {
		return allocatedUserParam;
	}

	public void setAllocatedUserParam(String allocatedUserParam) {
		this.allocatedUserParam = allocatedUserParam;
	}

	

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	@Override
	public void handleCreate(UserTaskCreationContext context)
			throws OperationException {
		if (profile != null && ! "".equals(profile)) {
			final Profile profileRef = Profile.parse(profile);
			final DirectoryHandler dirHandler = DirectoryHandler.getInstance(
					context.getRepository());
			if (dirHandler.isProfileDefined(profileRef)) {
				final CreationWorkItemSpec creationWorkItemSpec;
				if (profileRef.isUserReference()) {
					final UserReference userRef = (UserReference) profileRef;
					creationWorkItemSpec = CreationWorkItemSpec.forAllocationWithPossibleReallocation(
							userRef, (Role) Role.parse((String) context.getProfiles().get(0)));
					creationWorkItemSpec.setNotificationMail(context.getAllocatedToNotificationMail());
				} else {
					final Role roleRef = (Role) profileRef;
					creationWorkItemSpec = CreationWorkItemSpec.forOffer(roleRef);
					creationWorkItemSpec.setNotificationMail(context.getOfferedToNotificationMail());
				}
				String prefix = "GOC Workflow Request ID - " + requestID + ". ";  //REQUEST_CHANGES_FOR;
				if(null != workflowStepName){
					prefix = "Rework requested at " + workflowStepName + ":";
				}
				
				String label = prefix + tableName;
				if(null != maintenanceType){
					label = prefix + tableName + ": " + capitalize(maintenanceType) +  " Maintenance";
				}
				
				creationWorkItemSpec.setSpecificLabel(UserMessage.createInfo(label));
				
				context.createWorkItem(creationWorkItemSpec);
		        return;
		      }
		}
		super.handleCreate(context);
	}
	
	private String capitalize(String line) {
		
		if(null != line){
			return Character.toUpperCase(line.charAt(0)) + line.substring(1);
		}else{
			return "";
		}
	}
	
		@Override
	  public void handleWorkItemCompletion(UserTaskWorkItemCompletionContext context)
	      throws OperationException {
		final WorkItem workItem = context.getCompletedWorkItem();
	    final UserReference userReference = workItem.getUserReference();
	  
	    context.setVariableString(allocatedUserParam, userReference.getUserId());
	    context.setVariableString(userComment,  context.getCompletedWorkItem().getComment());
	    
	    super.handleWorkItemCompletion(context);
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}

	public void setMaintenanceType(String maintenanceType) {
		this.maintenanceType = maintenanceType;
	}

	public String getMaintenanceType() {
		return maintenanceType;
	}

	public void setWorkflowStepName(String workflowStepName) {
		this.workflowStepName = workflowStepName;
	}

	public String getWorkflowStepName() {
		return workflowStepName;
	}
}
