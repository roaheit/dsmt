package com.orchestranetworks.ps.importer;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public abstract class DataSetImporterFactory {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	public DataSetImporter createImporter(final Adaptation importConfigDataSet, final String importConfigID)
			throws OperationException {
		LOG.debug("DataSetImporterFactory: createImporter");
		LOG.debug("DataSetImporterFactory: importConfigDataSet=" + importConfigDataSet);
		LOG.debug("DataSetImporterFactory: importConfigID=" + importConfigID);
		LOG.debug("DataSetImporterFactory: Loading import config adaptation.");
		final Adaptation importConfigAdaptation = loadImportConfigAdaptation(importConfigDataSet, importConfigID);
		LOG.debug("DataSetImporterFactory: importConfigAdaptation=" + importConfigAdaptation);
		if (importConfigAdaptation == null) {
			throw OperationException.createError("No configuration named '" + importConfigID + "' found.");
		}
		LOG.debug("DataSetImporterFactory: Loading import table config adaptations.");
		final Set<Adaptation> importTableConfigAdaptations = loadImportTableConfigAdaptations(importConfigDataSet, importConfigID);
		if (LOG.isDebug())
			LOG.debug("DataSetImporterFactory: importTableConfigAdaptations=" + importTableConfigAdaptations);
		final DataSetImportConfig importConfig = createImportConfigFromAdaptation(importConfigAdaptation, importTableConfigAdaptations);
		LOG.debug("DataSetImporterFactory: importConfig=" + importConfig);
		LOG.debug("DataSetImporterFactory: Calling doCreateImporter");
		return doCreateImporter(importConfig);
	}
	
	protected abstract DataSetImporter doCreateImporter(DataSetImportConfig config);
	
	protected DataSetImportConfig createImportConfigFromAdaptation(Adaptation importConfigAdaptation, Set<Adaptation> importTableConfigAdaptations) {
		LOG.debug("DataSetImporterFactory: createImportConfigFromAdaptation");
		final DataSetImportConfig config = new DataSetImportConfig();
		final File folder = new File(importConfigAdaptation.getString(Constants.PATH_IMPORT_CONFIG_FOLDER_NAME));
		LOG.debug("DataSetImporterFactory: folder=" + folder);
		config.setFolder(folder);
		config.setFileEncoding(importConfigAdaptation.getString(Constants.PATH_IMPORT_CONFIG_FILE_ENCODING));
		config.setColumnHeader(importConfigAdaptation.getString(Constants.PATH_IMPORT_CONFIG_COLUMN_HEADER));
		config.setSeparator(importConfigAdaptation.getString(Constants.PATH_IMPORT_CONFIG_SEPARATOR).charAt(0));
		config.setFormatted(importConfigAdaptation.get_boolean(Constants.PATH_IMPORT_CONFIG_FORMATTED));
		config.setDateFormat(importConfigAdaptation.getString(Constants.PATH_IMPORT_CONFIG_DATE_FORMAT));
		config.setDateTimeFormat(importConfigAdaptation.getString(Constants.PATH_IMPORT_CONFIG_DATE_TIME_FORMAT));
		config.setTimeFormat(importConfigAdaptation.getString(Constants.PATH_IMPORT_CONFIG_TIME_FORMAT));
		config.setBooleanFormat(importConfigAdaptation.getString(Constants.PATH_IMPORT_CONFIG_BOOLEAN_FORMAT));
		config.setImportSpecifiedTablesOnly(importConfigAdaptation.get_boolean(Constants.PATH_IMPORT_CONFIG_IMPORT_SPECIFIED_TABLES_ONLY));
		config.setImportMode(importConfigAdaptation.getString(Constants.PATH_IMPORT_CONFIG_IMPORT_MODE));
		@SuppressWarnings("unchecked")
		final List<AdditionalParametersBean> additionalParameters = importConfigAdaptation.getList(Constants.PATH_IMPORT_CONFIG_ADDITIONAL_PARAMETERS);
		config.setAdditionalParameters(additionalParameters);
		
		final HashSet<DataSetImportTableConfig> tableConfigs = new HashSet<DataSetImportTableConfig>();
		for (Adaptation importTableConfigAdaptation: importTableConfigAdaptations) {
			final DataSetImportTableConfig tableConfig = createImportTableConfigFromAdaptation(importTableConfigAdaptation);
			tableConfigs.add(tableConfig);
		}
		config.setTableConfigs(tableConfigs);
		LOG.debug("DataSetImporterFactory: config=" + config);
		
		return config;
	}
	
	protected DataSetImportTableConfig createImportTableConfigFromAdaptation(Adaptation importTableConfigAdaptation) {
		LOG.debug("DataSetImporterFactory: createImportTableConfigFromAdaptation");
		final DataSetImportTableConfig tableConfig = new DataSetImportTableConfig();
		final Path tablePath = Path.parse(importTableConfigAdaptation.getString(Constants.PATH_IMPORT_TABLE_CONFIG_TABLE_PATH));
		LOG.debug("DataSetImporterFactory: tablePath=" + tablePath);
		tableConfig.setTablePath(tablePath);
		
		final String fileName = importTableConfigAdaptation.getString(Constants.PATH_IMPORT_TABLE_CONFIG_FILE_NAME);
		LOG.debug("DataSetImporterFactory: fileName =" + fileName);
		tableConfig.setFilename(fileName);
		LOG.debug("DataSetImporterFactory: config default filename =" + tableConfig.getDefaultFilename());

		return tableConfig;
	}
	
	private Adaptation loadImportConfigAdaptation(final Adaptation dataSet, String importConfigID) {
		LOG.debug("DataSetImporterFactory: loadImportConfigAdaptation");
		final AdaptationTable importConfigTable = dataSet.getTable(
				Constants.PATH_IMPORT_CONFIG_TABLE);
		LOG.debug("DataSetImporterFactory: importConfigTable=" + importConfigTable);
		final Adaptation importConfig = importConfigTable.lookupAdaptationByPrimaryKey(
				PrimaryKey.parseString(importConfigID));
		LOG.debug("DataSetImporterFactory: importConfig=" + importConfig);
		return importConfig;
	}
	
	private Set<Adaptation> loadImportTableConfigAdaptations(final Adaptation dataSet, final String importConfigID) {
		LOG.debug("DataSetImporterFactory: loadImportTableConfigAdaptations");
		final AdaptationTable importTableConfigTable = dataSet.getTable(
				Constants.PATH_IMPORT_TABLE_CONFIG_TABLE);
		LOG.debug("DataSetImporterFactory: importTableConfigTable=" + importTableConfigTable);
		final RequestResult reqRes = importTableConfigTable.createRequestResult(
				Constants.PATH_IMPORT_TABLE_CONFIG_IMPORT_CONFIG_ID.format() + "='" + importConfigID + "'");
		final HashSet<Adaptation> importTableConfigs = new HashSet<Adaptation>();
		try {
			for (Adaptation importTableConfig; (importTableConfig = reqRes.nextAdaptation()) != null;) {
				LOG.debug("DataSetImporterFactory: Adding importTableConfig " + importTableConfig);
				importTableConfigs.add(importTableConfig);
			}
		} finally {
			reqRes.close();
		}
		return importTableConfigs;
	}
}
