package com.orchestranetworks.ps.importer;

import java.io.File;
import java.io.IOException;

import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;

public class BasicDataSetImporter extends DataSetImporter {
	public BasicDataSetImporter() {	
	}
	
	public BasicDataSetImporter(final DataSetImportConfig config) {
		super.config = config;
	}
	
	@Override
	protected void readTableFromFile(ProcedureContext pContext,
			AdaptationTable table, File file,
			DataSetImportTableConfig tableConfig)
			throws IOException, OperationException {
		doEBXImport(pContext, table, file);
	}
}
