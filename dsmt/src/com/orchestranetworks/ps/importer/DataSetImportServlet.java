package com.orchestranetworks.ps.importer;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ServiceContext;

public abstract class DataSetImportServlet extends HttpServlet {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		LOG.info("DataSetImportServlet: service");
		final PrintWriter out = res.getWriter();
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		final AdaptationHome dataSpace = sContext.getCurrentHome();
		LOG.info("DataSetImportServlet: dataSpace=" + dataSpace);
		final Repository repo = dataSpace.getRepository();
		final Adaptation dataSet = sContext.getCurrentAdaptation();
		LOG.info("DataSetImportServlet: dataSet=" + dataSet);

		final String importConfigDataSpaceParam = req.getParameter(Constants.PARAM_IMPORT_CONFIG_DATA_SPACE);
		LOG.info("DataSetImportServlet: importConfigDataSpaceParam=" + importConfigDataSpaceParam);
		final String importConfigDataSetParam = req.getParameter(Constants.PARAM_IMPORT_CONFIG_DATA_SET);
		LOG.info("DataSetImportServlet: importConfigDataSetParam=" + importConfigDataSetParam);
		final String importConfigID = req.getParameter(Constants.PARAM_IMPORT_CONFIG_ID);
		LOG.info("DataSetImportServlet: importConfigID=" + importConfigID);
		final String disableTriggersParam = req.getParameter(Constants.PARAM_DISABLE_TRIGGERS);
		LOG.info("DataSetImportServlet: disableTriggersParam=" + disableTriggersParam);
		final boolean disableTriggers = (disableTriggersParam == null || "".equals(disableTriggersParam))
				? false : Boolean.parseBoolean(disableTriggersParam);
		
		final String runValidationParam = req.getParameter(Constants.PARAM_RUN_VALIDATION);
		LOG.info("DataSetImportServlet: runValidation=" + runValidationParam);
		final boolean runValidation = (runValidationParam == null || "".equalsIgnoreCase(runValidationParam))
				? false : Boolean.parseBoolean(runValidationParam);
		
		LOG.info("DataSetImportServlet: Looking up import config data space & data set.");
		final AdaptationHome importConfigDataSpace = repo.lookupHome(
				HomeKey.forBranchName(importConfigDataSpaceParam));
		LOG.debug("DataSetImportServlet: importConfigDataSpace=" + importConfigDataSpace);
		final Adaptation importConfigDataSet = importConfigDataSpace.findAdaptationOrNull(
				AdaptationName.forName(importConfigDataSetParam));
		LOG.debug("DataSetImportServlet: importConfigDataSet=" + importConfigDataSet);
		
		LOG.info("DataSetImportServlet: Creating importer factory.");
		final DataSetImporterFactory factory = createImporterFactory();
		
		LOG.info("DataSetImportServlet: Creating importer.");
		try {
			final DataSetImporter importer = factory.createImporter(importConfigDataSet, importConfigID);
			importer.setDisableTriggers(disableTriggers);
			importer.setRunValidation(runValidation);
			LOG.info("DataSetImportServlet: Executing import.");
			importer.setImportConfigName(importConfigID);
			importer.executeImport(sContext.getSession(), dataSpace, dataSet);
		} catch (final OperationException ex) {
			LOG.error("DataSetImportServlet: Exception occurred.", ex);
			throw new ServletException(ex);
		}
		
		LOG.info("DataSetImportServlet: Writing redirection command.");
		writeRedirectionOnEnding(out, sContext);
		LOG.info("DataSetImportServlet: service complete.");
	}
	
	protected abstract DataSetImporterFactory createImporterFactory();
	
	protected void writeRedirectionOnEnding(PrintWriter out, ServiceContext sContext) {
		out.print("<script>window.location.href='" + sContext.getURLForEndingService() + "';</script>");
	}
}
