package com.orchestranetworks.ps.importer.task;

import org.apache.commons.lang.StringUtils;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.importer.BasicDataSetImporterFactory;
import com.orchestranetworks.ps.importer.DataSetImporter;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

public class MultiConfigImporterScheduledTask extends ScheduledTask {
	
	private static final String DEFAULT_Import_CONFIG_DATA_SPACE = "DataSetImport";
	private static final String DEFAULT_Import_CONFIG_DATA_SET = "DataSetImport";
	
	private String dataSpace;
	private String dataSet;
	private String ImportConfigDataSpace;
	private String ImportConfigDataSet;
	private String ImportConfigID;

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getImportConfigDataSpace() {
		return ImportConfigDataSpace;
	}

	public void setImportConfigDataSpace(String ImportConfigDataSpace) {
		this.ImportConfigDataSpace = ImportConfigDataSpace;
	}

	public String getImportConfigDataSet() {
		return ImportConfigDataSet;
	}

	public void setImportConfigDataSet(String ImportConfigDataSet) {
		this.ImportConfigDataSet = ImportConfigDataSet;
	}

	public String getImportConfigID() {
		return ImportConfigID;
	}

	public void setImportConfigID(String ImportConfigID) {
		this.ImportConfigID = ImportConfigID;
	}

	@Override
	public void execute(ScheduledExecutionContext context)
			throws OperationException, ScheduledTaskInterruption {
		final String ImportConfigDataSpaceParam =
				(ImportConfigDataSpace == null || "".equals(ImportConfigDataSpace))
				? DEFAULT_Import_CONFIG_DATA_SPACE : ImportConfigDataSpace;
		final String ImportConfigDataSetParam =
				(ImportConfigDataSet == null || "".equals(ImportConfigDataSet))
				? DEFAULT_Import_CONFIG_DATA_SET : ImportConfigDataSet;
		final String ImportConfigIDParam =
				(ImportConfigID == null || "".equals(ImportConfigID))
				? dataSet : ImportConfigID;

		final Repository repo = context.getRepository();
		final Session session = context.getSession();
		
		final AdaptationHome dataSpaceRef = repo.lookupHome(HomeKey.forBranchName(dataSpace));
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
		
		final AdaptationHome ImportConfigDataSpaceRef = repo.lookupHome(
				HomeKey.forBranchName(ImportConfigDataSpaceParam));
		final Adaptation ImportConfigDataSetRef = ImportConfigDataSpaceRef.findAdaptationOrNull(
				AdaptationName.forName(ImportConfigDataSetParam));
		
		final BasicDataSetImporterFactory factory = new BasicDataSetImporterFactory();
		// write code to split ImportConfigID based on token "," 
		String [] ImportConfigIDs=StringUtils.split(ImportConfigIDParam, ",");
		for(String importConfigIDParam:ImportConfigIDs){
		final DataSetImporter Importer = factory.createImporter(ImportConfigDataSetRef, importConfigIDParam);
		Importer.setImportConfigName(importConfigIDParam);
		try {
			Importer.executeImport(session, dataSpaceRef, dataSetRef);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}


}
