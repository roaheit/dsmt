package com.orchestranetworks.ps.importer;

import com.orchestranetworks.schema.Path;

public class DataSetImportTableConfig {
	private Path tablePath;
	private String filename;

	public Path getTablePath() {
		return tablePath;
	}
	
	public void setTablePath(Path tablePath) {
		this.tablePath = tablePath;
	}
	
	public String getFilename() {
		return filename;
	}
	
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public String getDefaultFilename() {
		return tablePath.getLastStep().format() + ".txt";
	}
}
