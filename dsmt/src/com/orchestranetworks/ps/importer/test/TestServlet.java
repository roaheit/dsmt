package com.orchestranetworks.ps.importer.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.importer.BasicDataSetImporterFactory;
import com.orchestranetworks.ps.importer.DataSetImporter;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String DEFAULT_IMPORT_CONFIG_DATA_SPACE = "DataSetImport";
	private static final String DEFAULT_IMPORT_CONFIG_DATA_SET = "DataSetImport";
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		final String login = req.getParameter("login");
		final String password = req.getParameter("password");
		final String dataSpaceName = req.getParameter("dataSpace");
		final String dataSetName = req.getParameter("dataSet");
		String importConfigDataSpaceName = req.getParameter("importConfigDataSpace");
		if (importConfigDataSpaceName == null || "".equals(importConfigDataSpaceName)) {
			importConfigDataSpaceName = DEFAULT_IMPORT_CONFIG_DATA_SPACE;
		}
		String importConfigDataSetName = req.getParameter("importConfigDataSet");
		if (importConfigDataSetName == null || "".equals(importConfigDataSetName)) {
			importConfigDataSetName = DEFAULT_IMPORT_CONFIG_DATA_SET;
		}
		String importConfigID = req.getParameter("importConfigID");
		if (importConfigID == null || "".equals(importConfigID)) {
			importConfigID = dataSetName;
		}

		final Repository repo = Repository.getDefault();
		final Session session = repo.createSessionFromLoginPassword(login, password);

		final AdaptationHome dataSpace = repo.lookupHome(HomeKey.forBranchName(dataSpaceName));
		final Adaptation dataSet = dataSpace.findAdaptationOrNull(AdaptationName.forName(dataSetName));
		
		final AdaptationHome importConfigDataSpace = repo.lookupHome(
				HomeKey.forBranchName(importConfigDataSpaceName));
		final Adaptation importConfigDataSet = importConfigDataSpace.findAdaptationOrNull(
				AdaptationName.forName(importConfigDataSetName));
		
		final BasicDataSetImporterFactory factory = new BasicDataSetImporterFactory();
		
		try {
			final DataSetImporter importer = factory.createImporter(importConfigDataSet, importConfigID);
			importer.executeImport(session, dataSpace, dataSet);
		} catch (final OperationException ex) {
			throw new ServletException(ex);
		}
	}
}
