package com.orchestranetworks.ps.importer;

import java.io.File;
import java.util.List;
import java.util.Set;

public class DataSetImportConfig {
	private File folder;
	private String fileEncoding;
	private String columnHeader;
	private char separator;
	private boolean formatted;
	private String dateFormat;
	private String dateTimeFormat;
	private String timeFormat;
	private String booleanFormat;
	private boolean importSpecifiedTablesOnly;
	private String importMode;
	private List<AdditionalParametersBean> additionalParameters;
	private Set<DataSetImportTableConfig> tableConfigs;
	
	public File getFolder() {
		return folder;
	}
	
	public void setFolder(File folder) {
		this.folder = folder;
	}
	
	public String getFileEncoding() {
		return fileEncoding;
	}
	
	public void setFileEncoding(String fileEncoding) {
		this.fileEncoding = fileEncoding;
	}
	
	public String getColumnHeader() {
		return columnHeader;
	}
	
	public void setColumnHeader(String columnHeader) {
		this.columnHeader = columnHeader;
	}
	
	public char getSeparator() {
		return separator;
	}
	
	public void setSeparator(char separator) {
		this.separator = separator;
	}
	
	public boolean isFormatted() {
		return formatted;
	}

	public void setFormatted(boolean formatted) {
		this.formatted = formatted;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getDateTimeFormat() {
		return dateTimeFormat;
	}

	public void setDateTimeFormat(String dateTimeFormat) {
		this.dateTimeFormat = dateTimeFormat;
	}

	public String getTimeFormat() {
		return timeFormat;
	}

	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}

	public String getBooleanFormat() {
		return booleanFormat;
	}

	public void setBooleanFormat(String booleanFormat) {
		this.booleanFormat = booleanFormat;
	}

	public boolean isImportSpecifiedTablesOnly() {
		return importSpecifiedTablesOnly;
	}

	public void setImportSpecifiedTablesOnly(boolean importSpecifiedTablesOnly) {
		this.importSpecifiedTablesOnly = importSpecifiedTablesOnly;
	}
	
	public String getImportMode() {
		return importMode;
	}

	public void setImportMode(String importMode) {
		this.importMode = importMode;
	}

	public List<AdditionalParametersBean> getAdditionalParameters() {
		return additionalParameters;
	}

	public void setAdditionalParameters(
			List<AdditionalParametersBean> additionalParameters) {
		this.additionalParameters = additionalParameters;
	}

	public Set<DataSetImportTableConfig> getTableConfigs() {
		return tableConfigs;
	}
	
	public void setTableConfigs(Set<DataSetImportTableConfig> tableConfigs) {
		this.tableConfigs = tableConfigs;
	}
}
