package com.orchestranetworks.ps.importer;

import com.orchestranetworks.schema.Path;

public interface Constants {
	static final String COLUMN_HEADER_NONE = "none";
	static final String COLUMN_HEADER_LABEL = "label";
	static final String COLUMN_HEADER_XPATH = "xpath";
	
	static final String IMPORT_MODE_INSERT = "insert";
	static final String IMPORT_MODE_REPLACE = "replace";
	static final String IMPORT_MODE_UPDATE = "update";
	static final String IMPORT_MODE_UPDATE_OR_INSERT = "update_or_insert";
	
	static final String PARAM_IMPORT_CONFIG_DATA_SPACE = "importConfigDataSpace";
	static final String PARAM_IMPORT_CONFIG_DATA_SET = "importConfigDataSet";
	static final String PARAM_IMPORT_CONFIG_ID = "importConfigID";
	static final String PARAM_DISABLE_TRIGGERS = "disableTriggers";
	
	static final String DEFAULT_IMPORT_CONFIG_DATASPACE = "DataSetImport";
	static final String DEFAULT_IMPORT_CONFIG_DATASET = "DataSetImport";
	
	static final String DEFAULT_TEMP_FILENAME_EXTENSION = ".tmp";
	
	static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	static final String DEFAULT_TIME_FORMAT = "HH:mm:ss.SSS";
	static final String DEFAULT_DATE_TIME_FORMAT = DEFAULT_DATE_FORMAT + "'T'" + DEFAULT_TIME_FORMAT;
	
	static final char BOOLEAN_FORMAT_SEPARATOR = '/';
	
	static final String EBX_DATE_FORMAT = "yyyy-MM-dd";
	static final String EBX_TIME_FORMAT = "HH:mm:ss.SSS";
	static final String EBX_DATE_TIME_FORMAT = EBX_DATE_FORMAT + "'T'" + EBX_TIME_FORMAT;
	
	static final Path PATH_IMPORT_CONFIG_TABLE = Path.parse("/root").add(Path.parse("ImportConfig"));
	static final Path PATH_IMPORT_TABLE_CONFIG_TABLE = Path.parse("/root").add(Path.parse("ImportTableConfig"));
	static final Path PATH_IMPORT_CONFIG_FOLDER_NAME = Path.parse("./folderName");
	static final Path PATH_IMPORT_CONFIG_FILE_ENCODING = Path.parse("./fileEncoding");
	static final Path PATH_IMPORT_CONFIG_COLUMN_HEADER = Path.parse("./columnHeader");
	static final Path PATH_IMPORT_CONFIG_SEPARATOR = Path.parse("./separator");
	static final Path PATH_IMPORT_CONFIG_FORMATTED = Path.parse("./formatted");
	static final Path PATH_IMPORT_CONFIG_DATE_FORMAT = Path.parse("./dateFormat");
	static final Path PATH_IMPORT_CONFIG_DATE_TIME_FORMAT = Path.parse("./dateTimeFormat");
	static final Path PATH_IMPORT_CONFIG_TIME_FORMAT = Path.parse("./timeFormat");
	static final Path PATH_IMPORT_CONFIG_BOOLEAN_FORMAT = Path.parse("./booleanFormat");
	static final Path PATH_IMPORT_CONFIG_IMPORT_SPECIFIED_TABLES_ONLY = Path.parse("./importSpecifiedTablesOnly");
	static final Path PATH_IMPORT_CONFIG_IMPORT_MODE = Path.parse("./importMode");
	static final Path PATH_IMPORT_CONFIG_ADDITIONAL_PARAMETERS = Path.parse("./additionalParameters");
	
	static final Path PATH_IMPORT_TABLE_CONFIG_IMPORT_CONFIG_ID = Path.parse("./importConfigID");
	static final Path PATH_IMPORT_TABLE_CONFIG_TABLE_PATH = Path.parse("./tablePath");
	static final Path PATH_IMPORT_TABLE_CONFIG_FILE_NAME = Path.parse("./fileName");
	
	
	static final String PARAM_RUN_VALIDATION = "runValidation";
	
}
