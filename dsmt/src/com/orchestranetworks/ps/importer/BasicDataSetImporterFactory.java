package com.orchestranetworks.ps.importer;

public class BasicDataSetImporterFactory extends DataSetImporterFactory {
	@Override
	protected DataSetImporter doCreateImporter(DataSetImportConfig config) {
		return new BasicDataSetImporter(config);
	}
}
