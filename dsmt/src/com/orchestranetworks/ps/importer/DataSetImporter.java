package com.orchestranetworks.ps.importer;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.ExportImportCSVSpec;
import com.orchestranetworks.service.ExportImportCSVSpec.Header;
import com.orchestranetworks.service.ImportSpec;
import com.orchestranetworks.service.ImportSpecMode;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValidationReportItemIterator;

public abstract class DataSetImporter {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	protected DataSetImportConfig config;
	protected boolean disableTriggers;
	protected boolean enableAllPrivileges;
	//private String notificationDL=DSMTConstants.DSMT2_DISTRIBUTION_LIST; // DSMTConstants.bundle.getString("notify.goc.sync.completion.dl");
	private String notificationDL = DSMTConstants.propertyHelper
			.getProp(DSMTConstants.CWM_TERMINATOR);
	private String subject= "Import Config Execution Notification";  // "DSMT2 GOC Synchronization Notification";
	private String message;  // "Process {process name} is complete. The synchronization file is present at location - " ;
	private String importConfigIDName;
	private boolean runValidation = true;
	public DataSetImportConfig getConfig() {
		return config;
	}

	public void setConfig(DataSetImportConfig config) {
		this.config = config;
	}
	
	public boolean isDisableTriggers() {
		return disableTriggers;
	}

	public void setDisableTriggers(boolean disableTriggers) {
		this.disableTriggers = disableTriggers;
	}

	public boolean isEnableAllPrivileges() {
		return enableAllPrivileges;
	}

	public void setEnableAllPrivileges(boolean enableAllPrivileges) {
		this.enableAllPrivileges = enableAllPrivileges;
	}

	public void executeImport(final Session session, final AdaptationHome dataSpace, final Adaptation dataSet)
			throws OperationException {
		LOG.info("Current user running the import is " + session.getUserReference().getUserId());
		
		LOG.debug("DataSetImporter: executeImport");
		LOG.debug("DataSetImporter: session=" + session);
		LOG.debug("DataSetImporter: dataSpace=" + dataSpace);
		LOG.debug("DataSetImporter: dataSet=" + dataSet);
		
		final Procedure importProc = new Procedure() {
			public void execute(ProcedureContext pContext) throws Exception {
				LOG.debug("DataSetImporter: Executing procedure");
				importDataSet(pContext, dataSet);
			}
		};
		final ProgrammaticService exportSvc = ProgrammaticService.createForSession(session, dataSpace);
		final ProcedureResult procRes = exportSvc.execute(importProc);
		final OperationException exception = procRes.getException();
		LOG.debug("DataSetImporter: ProcedureResult exception = " + exception);
		if (exception != null) {
				throw exception;
		}
	}

	protected void importDataSet(ProcedureContext pContext, Adaptation dataSet) throws IOException, OperationException {
		LOG.debug("DataSetImporter: importDataSet");
		final Set<DataSetImportTableConfig> tableConfigs = config.getTableConfigs();
		if (! config.isImportSpecifiedTablesOnly()) {
			LOG.debug("DataSetImporter: Creating default configs for unspecified tables");
			createDefaultTableConfigs(tableConfigs, dataSet);
		}
		for (DataSetImportTableConfig tableConfig: tableConfigs) {
			importTable(pContext, dataSet, tableConfig);
		}
	}
	public void setImportConfigName(String importConfigID)
	{
		importConfigIDName=importConfigID;
	}
	protected void importTable(ProcedureContext pContext, Adaptation dataSet, DataSetImportTableConfig tableConfig)
			throws IOException, OperationException {
		LOG.debug("DataSetImporter: importTable");
		LOG.debug("DataSetImporter: folder=" + config.getFolder());
		final String filename = (tableConfig.getFilename() == null)
				? tableConfig.getDefaultFilename() : tableConfig.getFilename();
		LOG.debug("DataSetImporter: filename=" + filename);
		LOG.debug("DataSetImporter: tablePath=" + tableConfig.getTablePath());
		final File file = new File(config.getFolder(), filename);
		LOG.debug("DataSetImporter: file=" + file);
		final AdaptationTable table = dataSet.getTable(tableConfig.getTablePath());
		LOG.debug("DataSetImporter: table=" + table);
		LOG.debug("DataSetImporter: Reading table from file.");
		if(file.exists() && !file.isDirectory())
		{
		try{
		readTableFromFile(pContext, table, file, tableConfig);
		LOG.info("Import Done");

		if(runValidation){	
			ValidationReportItemIterator record=table.getValidationReport(false, false).getItemsOfSeverity(Severity.ERROR);
			LOG.info("Validation called for "+importConfigIDName);
			if(record.hasNext())
			{
				message="Import Process Completed with Validation Errors for import config ID ::  ";
			}
			else
			{
				message="Import Process Completed for import config ID ::  ";
			}
		}else{
				message="Import Process Completed for import config ID ::  ";
			LOG.info("Skipping validations....");
		}
		

		DSMTNotificationService.notifyEmailID(notificationDL, subject, message + importConfigIDName, false);
		}
		catch(Exception e){
			LOG.error("DataSetImporter: Error during the import.", e);
			message="Import Process Failed for import config ID ::  ";
			DSMTNotificationService.notifyEmailID(notificationDL, subject, message + importConfigIDName + "\n   "+e.getMessage() + ". \nPlease verify Data for more details on the same.", false);
		}
		}
		else
		{
			LOG.error("DataSetImporter: Error during the import.");
			message="File Specified for import" +file+ "doesn't exist  ";
			DSMTNotificationService.notifyEmailID(notificationDL, subject, message, false);
		}
		LOG.debug("DataSetImporter: Done reading table from file.");
	}
	
	protected abstract void readTableFromFile(ProcedureContext pContext, AdaptationTable table, File file, DataSetImportTableConfig tableConfig)
			throws IOException, OperationException;
	
	protected void doEBXImport(final ProcedureContext pContext, final AdaptationTable table, final File sourceFile)
			throws OperationException {
		LOG.debug("DataSetImporter: doEBXExport");
		if (LOG.isDebug()) {
			LOG.debug("DataSetImporter: table=" + table);
			LOG.debug("DataSetImporter: sourceFile=" + sourceFile);
		}
		final ImportSpec importSpec = new ImportSpec();
		final ExportImportCSVSpec csvSpec = new ExportImportCSVSpec();

		
		LOG.debug("DataSetImporter: Setting encoding to '" + config.getFileEncoding() + "'.");
		csvSpec.setEncoding(config.getFileEncoding());
		
		LOG.debug("DataSetImporter: Setting field separator to '" + config.getSeparator() + "'.");
		csvSpec.setFieldSeparator(config.getSeparator());
		LOG.debug("DataSetImporter: column header = " + config.getColumnHeader());
		if (Constants.COLUMN_HEADER_NONE.equals(config.getColumnHeader())) {
			LOG.debug("DataSetImporter: Setting column header to " + Header.NONE);
			csvSpec.setHeader(Header.NONE);
		} else if (Constants.COLUMN_HEADER_LABEL.equals(config.getColumnHeader())) {
			LOG.debug("DataSetImporter: Setting column header to " + Header.LABEL);
			csvSpec.setHeader(Header.LABEL);
		} else if (Constants.COLUMN_HEADER_XPATH.equals(config.getColumnHeader())) {
			LOG.debug("DataSetImporter: Setting column header to " + Header.PATH_IN_TABLE);
			csvSpec.setHeader(Header.PATH_IN_TABLE);
		} else {
			throw new IllegalArgumentException("Invalid header value \"" + config.getColumnHeader() + "\"");
		}
		importSpec.setCSVSpec(csvSpec);
		// importSpec.setCloseStreamWhenFinished(true);
		importSpec.setTargetAdaptationTable(table);
		importSpec.setSourceFile(sourceFile);
		
		if (disableTriggers) {
			pContext.setTriggerActivation(false);
		}
		if (enableAllPrivileges) {
			pContext.setAllPrivileges(true);
		}
		LOG.debug("DataSetImporter: import mode = " + config.getImportMode());
		if (Constants.IMPORT_MODE_INSERT.equals(config.getImportMode())) {
			LOG.debug("DataSetImporter: Setting import mode to " + ImportSpecMode.INSERT);
			importSpec.setImportMode(ImportSpecMode.INSERT);
		} else if (Constants.IMPORT_MODE_REPLACE.equals(config.getImportMode())) {
			LOG.debug("DataSetImporter: Setting import mode to " + ImportSpecMode.REPLACE);
			importSpec.setImportMode(ImportSpecMode.REPLACE);
		} else if (Constants.IMPORT_MODE_UPDATE.equals(config.getImportMode())) {
			LOG.debug("DataSetImporter: Setting import mode to " + ImportSpecMode.UPDATE);
			importSpec.setImportMode(ImportSpecMode.UPDATE);
		} else if (Constants.IMPORT_MODE_UPDATE_OR_INSERT.equals(config.getImportMode())) {
			LOG.debug("DataSetImporter: Setting import mode to " + ImportSpecMode.UPDATE_OR_INSERT);
			importSpec.setImportMode(ImportSpecMode.UPDATE_OR_INSERT);
		} else {
			throw new IllegalArgumentException("Invalid import mode value \"" + config.getImportMode() + "\"");
		}
		
		LOG.debug("DataSetImporter: Performing EBX import");
		
	//	LOG.info("Original value of Threshold was set to " + pContext.getCommitThreshold() +". Now setting to 1");
	//	pContext.setCommitThreshold(1);
		
		pContext.doImport(importSpec);
		LOG.debug("DataSetImporter: Done performing EBX import");
		
		if (disableTriggers) {
			pContext.setTriggerActivation(true);
		}
		if (enableAllPrivileges) {
			pContext.setAllPrivileges(false);
		}
	}
	
	private static void createDefaultTableConfigs(final Set<DataSetImportTableConfig> tableConfigs, final Adaptation dataSet) {
		final HashSet<Path> tableConfigPaths = new HashSet<Path>();
		// Put the paths in a set so we can easily check if
		// there is already a config for them
		for (DataSetImportTableConfig tableConfig: tableConfigs) {
			tableConfigPaths.add(tableConfig.getTablePath());
		}
		final SchemaNode dataSetNode = dataSet.getSchemaNode();
		LOG.debug("DataSetExporter: dataSetNode=" + dataSetNode);
		final SchemaNode rootNode = dataSetNode.getNodeChildren()[0];
		LOG.debug("DataSetExporter: rootNode=" + rootNode);
		// Get all tables for the data set
		final SchemaNode[] tableNodes = rootNode.getNodeChildren();
		for (int i = 0; i < tableNodes.length; i++) {
			final SchemaNode tableNode = tableNodes[i];
			final Path tablePath = tableNode.getPathInSchema();
			if (! tableConfigPaths.contains(tablePath)) {
				final DataSetImportTableConfig tableConfig = new DataSetImportTableConfig();
				tableConfig.setTablePath(tableNode.getPathInSchema());
				tableConfigs.add(tableConfig);
			}
		}
	}

	public boolean isRunValidation() {
		return runValidation;
	}

	public void setRunValidation(boolean runValidation) {
		this.runValidation = runValidation;
	}
	
	
}
