package com.orchestranetworks.ps.importer;

public class BasicDataSetImportServlet extends DataSetImportServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected DataSetImporterFactory createImporterFactory() {
		return new BasicDataSetImporterFactory();
	}
}
