package com.orchestranetworks.ps.ui.editor;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.ui.UIBeanEditor;
import com.orchestranetworks.ui.UIResponseContext;

public class ReadOnlyEditor extends UIBeanEditor {

	@Override
	public void addForDisplay(UIResponseContext context) {
		context.addUIBestMatchingDisplay(Path.SELF, "");
	}

	@Override
	public void addForEdit(UIResponseContext context) {
		context.addUIBestMatchingDisplay(Path.SELF, "");
	}

	@Override
	public void addForDisplayInTable(UIResponseContext context) {
		context.addUIBestMatchingDisplay(Path.SELF, "");
	}

}
