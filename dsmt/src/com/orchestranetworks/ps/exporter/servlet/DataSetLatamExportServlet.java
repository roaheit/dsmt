package com.orchestranetworks.ps.exporter.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.exporter.LatamExporter;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExportConfigFactory;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.Session;

/**
 * A servlet for invoking the data set export. It is not required for invoking the exporter, but is
 * one way in which it can be invoked. It is required if you wish to invoke it via the GUI services menu.
 * 
 */
public class DataSetLatamExportServlet extends HttpServlet {
	// These are the parameters for the servlet
	public static final String PARAM_EXPORT_CONFIG_DATA_SPACE = "exportConfigDataSpace";
	public static final String PARAM_EXPORT_CONFIG_DATA_SET = "exportConfigDataSet";
	public static final String PARAM_EXPORT_CONFIG_ID = "exportConfigID";
	public static final String PARAM_VALIDATION_ACTION = "validationAction";
	
	public static final String VALIDATION_ACTION_NONE = "none";
	public static final String VALIDATION_ACTION_CONTINUE = "continue";
	public static final String VALIDATION_ACTION_STOP = "stop";
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private static final long serialVersionUID = 1L;
	public static final String PARAM_MERGE_README = "mergeReadme";
	public static final String PARAM_SQL_REPORTS = "runSQLReports";
	public static final String PARAM_HT_GEN = "runHTGenerator";
	
	private String exportConfigDataSpace;
	private String exportConfigDataSet;
	private String exportConfigID;
	
	protected boolean mergeReadme;
	protected boolean runSQLReports;
	protected boolean runHTGenerator;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		LOG.info("DataSetLatamExportServlet: service");
		final PrintWriter out = res.getWriter();
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		final Adaptation dataSet = sContext.getCurrentAdaptation();
		
		exportConfigDataSpace = req.getParameter(PARAM_EXPORT_CONFIG_DATA_SPACE);
		exportConfigDataSet = req.getParameter(PARAM_EXPORT_CONFIG_DATA_SET);
		exportConfigID = req.getParameter(PARAM_EXPORT_CONFIG_ID);
		//final String validationAction = req.getParameter(PARAM_VALIDATION_ACTION);
		final String mergeReadmeString = req.getParameter(PARAM_MERGE_README);
		mergeReadme = (mergeReadmeString == null || "".equals(mergeReadmeString))? false : Boolean.parseBoolean(mergeReadmeString);
		
		LOG.info("mergeReadme in DataSetLatamExportServlet = " + mergeReadme);
		
		final String runHTGenString = req.getParameter(PARAM_HT_GEN);
		runHTGenerator = (runHTGenString == null || "".equals(runHTGenString))? false : Boolean.parseBoolean(runHTGenString);
		
		final String runSQLReportString = req.getParameter(PARAM_SQL_REPORTS);
		runSQLReports = (runSQLReportString == null || "".equals(runSQLReportString))? false : Boolean.parseBoolean(runSQLReportString);
		 
		/*if (validationAction != null
				&& ! VALIDATION_ACTION_NONE.equalsIgnoreCase(validationAction)
				&& ! VALIDATION_ACTION_CONTINUE.equalsIgnoreCase(validationAction)
				&& ! VALIDATION_ACTION_STOP.equalsIgnoreCase(validationAction)) {
			throw new ServletException("validationAction parameter must be one of these values: "
					+ VALIDATION_ACTION_NONE + ", "
					+ VALIDATION_ACTION_CONTINUE + ", or "
					+ VALIDATION_ACTION_STOP + ".");
		}
		
		if (validationAction != null && ! VALIDATION_ACTION_NONE.equalsIgnoreCase(validationAction)) {
			// Perform the validation
			final ValidationReport validationReport = dataSet.getValidationReport();
			// Throw exception if there are validation errors and we are supposed to stop
			if (VALIDATION_ACTION_STOP.equalsIgnoreCase(validationAction)
					&& validationReport.hasItemsOfSeverityOrMore(Severity.ERROR)) {
				throw new ServletException("Validation failed.");
			}
		}*/
		
		try {
			export(sContext.getSession(), sContext.getCurrentHome().getRepository(), dataSet);
		} catch (final OperationException ex) {
			throw new ServletException(ex);
		}
		
		writeRedirectionOnEnding(out, sContext);
	}
	
	protected void export(Session session, Repository repo, Adaptation dataSet)
			throws IOException, OperationException {
		LOG.info("DataSetLatamExportServlet: export");
		final TableExporterFactory tableExporterFactory = createTableExporterFactory();
		final DataSetExportConfig config = createConfig(repo);
		final LatamExporter exporter = createExporter(tableExporterFactory, config);
		exporter.exportDataSet(session, dataSet);
	}
	
	protected LatamExporter createExporter(TableExporterFactory tableExporterFactory, DataSetExportConfig config) {
		LOG.info("DataSetLatamExportServlet: createExporter");
		return new LatamExporter(tableExporterFactory, config);
	}
	
	protected TableExporterFactory createTableExporterFactory() {
		LOG.info("DataSetLatamExportServlet: createTableExporterFactory");
		return new DefaultTableExporterFactory();
	}
	
	protected DataSetExportConfig createConfig(Repository repo)
			throws OperationException {
		LOG.info("DataSetLatamExportServlet: createConfig");
		// Look up the data space and data set for the export config records
		final AdaptationHome exportConfigDataSpaceRef = repo.lookupHome(
				HomeKey.forBranchName(exportConfigDataSpace));
		final Adaptation exportConfigDataSetRef = exportConfigDataSpaceRef.findAdaptationOrNull(
				AdaptationName.forName(exportConfigDataSet));
		final DataSetExportConfigFactory configFactory = new DataSetExportConfigFactory();
		return configFactory.createFromEBXRecord(
				exportConfigDataSetRef, exportConfigID);
	}
	
	protected void writeRedirectionOnEnding(PrintWriter out, ServiceContext sContext) {
		LOG.info("DataSetLatamExportServlet: writeRedirectionOnEnding");
		LOG.debug(" sContext.getURLForEndingService()  " +  sContext.getURLForEndingService() );
		out.print("<script>window.location.href='" + sContext.getURLForEndingService() + "';</script>");
	}
}
