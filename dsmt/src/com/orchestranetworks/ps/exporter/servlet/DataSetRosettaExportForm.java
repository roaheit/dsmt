package com.orchestranetworks.ps.exporter.servlet;

import java.io.BufferedWriter;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.ServiceContext;

/**
 * Servlet implementation class DataSetRosettaExportForm
 */
public class DataSetRosettaExportForm extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private static final String DEFAULT_EXPORT_CONFIG_DATASPACE = "DataSetExport";
	private static final String DEFAULT_EXPORT_CONFIG_DATASET = "DataSetExport";
	private static final String DEFAULT_EXPORT_CONFIG_ID = "";
	private static final String DEFAULT_SERVLET = "/DataSetExport";
	private static final String DEFAULT_VALIDATION_ACTION = DataSetExportServlet.VALIDATION_ACTION_NONE;
//	protected String mergeReadme = "true";
	
	
	protected BufferedWriter out;
	
	protected String exportConfigDataSpace = DEFAULT_EXPORT_CONFIG_DATASPACE;
	protected String exportConfigDataSet = DEFAULT_EXPORT_CONFIG_DATASET;
	protected String exportConfigID = DEFAULT_EXPORT_CONFIG_ID;
	protected String servlet = DEFAULT_SERVLET;
       
    
	
	/**
	 * @return the exportConfigDataSpace
	 */
	public String getExportConfigDataSpace() {
		return exportConfigDataSpace;
	}

	/**
	 * @param exportConfigDataSpace the exportConfigDataSpace to set
	 */
	public void setExportConfigDataSpace(String exportConfigDataSpace) {
		this.exportConfigDataSpace = exportConfigDataSpace;
	}

	/**
	 * @return the exportConfigDataSet
	 */
	public String getExportConfigDataSet() {
		return exportConfigDataSet;
	}

	/**
	 * @param exportConfigDataSet the exportConfigDataSet to set
	 */
	public void setExportConfigDataSet(String exportConfigDataSet) {
		this.exportConfigDataSet = exportConfigDataSet;
	}

	/**
	 * @return the exportConfigID
	 */
	public String getExportConfigID() {
		return exportConfigID;
	}

	/**
	 * @param exportConfigID the exportConfigID to set
	 */
	public void setExportConfigID(String exportConfigID) {
		this.exportConfigID = exportConfigID;
	}

	/**
	 * @return the servlet
	 */
	public String getServlet() {
		return servlet;
	}

	/**
	 * @param servlet the servlet to set
	 */
	public void setServlet(String servlet) {
		this.servlet = servlet;
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public DataSetRosettaExportForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    
    @Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		LOG.debug("DataSetExportForm: service");
		out = new BufferedWriter(res.getWriter());
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		writeForm(sContext);
		out.flush();
	}
	
	protected void writeForm(ServiceContext sContext) throws IOException {
		LOG.debug("DataSetExportForm: writeForm");
		writeln("<form id=\"form\" action=\"" + sContext.getURLForIncludingResource(servlet)
				+ "\" method=\"post\" autocomplete=\"off\">");
		writeFields(sContext);
		writeSubmitButton(sContext);
		writeln("</form>");
	}
	
	protected void writeFields(ServiceContext sContext) throws IOException {
		// Much of this HTML is replicating what gets produced by EBX to make it look
		// similar to the general look of EBX. However, It is not guaranteed that EBX
		// will continue to use these same css tags, etc in future versions.
		LOG.debug("DataSetExportForm: writeFields");
		writeln("  <table>");
		writeln("    <tbody>");
		
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + DataSetExportServlet.PARAM_EXPORT_CONFIG_DATA_SPACE
				+ "\">Export Config Data Space</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + exportConfigDataSpace
				+ "\" type=\"text\" id=\"" + DataSetExportServlet.PARAM_EXPORT_CONFIG_DATA_SPACE
				+ "\" name=\"" + DataSetExportServlet.PARAM_EXPORT_CONFIG_DATA_SPACE + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + DataSetExportServlet.PARAM_EXPORT_CONFIG_DATA_SET
				+ "\">Export Config Data Set</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + exportConfigDataSet
				+ "\" type=\"text\" id=\"" + DataSetExportServlet.PARAM_EXPORT_CONFIG_DATA_SET
				+ "\" name=\"" + DataSetExportServlet.PARAM_EXPORT_CONFIG_DATA_SET + "\"/>");;
		writeln("        </td>");
		writeln("      </tr>");
		
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + DataSetExportServlet.PARAM_EXPORT_CONFIG_ID
				+ "\">Export Config ID</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + exportConfigID
				+ "\" type=\"text\" id=\"" + DataSetExportServlet.PARAM_EXPORT_CONFIG_ID
				+ "\" name=\"" + DataSetExportServlet.PARAM_EXPORT_CONFIG_ID + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		
	/*	writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + DataSetExportServlet.PARAM_MERGE_README
				+ "\">Execute Readme</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"true\" type=\"checkbox\" checked=\"checked\" id=\""
				+ DataSetExportServlet.PARAM_MERGE_README
				+ "\" name=\"" + DataSetExportServlet.PARAM_MERGE_README + "\"/>");
		writeln("        </td>");
		
		writeln("      </tr>");
		
		
	*/	
		writeln("      <tr class=\"ebx_Field\" style=\"visibility:hidden\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + DataSetExportServlet.PARAM_SQL_REPORTS
				+ "\">Execute SQL Reports</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"false\" type=\"checkbox\" id=\""
				+ DataSetExportServlet.PARAM_SQL_REPORTS
				+ "\" name=\"" + DataSetExportServlet.PARAM_SQL_REPORTS + "\"/>");
		writeln("        </td>");
		
		writeln("      </tr>");
		
		
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + DataSetExportServlet.PARAM_HT_GEN
				+ "\">Execute HT Generator</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"true\" type=\"checkbox\" checked=\"checked\" id=\""
				+ DataSetExportServlet.PARAM_HT_GEN
				+ "\" name=\"" + DataSetExportServlet.PARAM_HT_GEN + "\"/>");
		writeln("        </td>");
		
		writeln("      </tr>");
		
		
		writeln("      <tr class=\"ebx_Field\" style=\"visibility:hidden\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + DataSetExportServlet.PARAM_VALIDATION_ACTION
				+ "\">Validation Action</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + DEFAULT_VALIDATION_ACTION
				+ "\" type=\"text\" id=\"" + DataSetExportServlet.PARAM_VALIDATION_ACTION
				+ "\" name=\"" + DataSetExportServlet.PARAM_VALIDATION_ACTION + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		
		writeln("    </tbody>");
		writeln("  </table>");
	}
	
	protected void writeSubmitButton(ServiceContext sContext) throws IOException {
		LOG.debug("DataSetExportForm: writeSubmitButton");
		writeln("  <button type=\"submit\" class=\"ebx_Button\">Export</button>");
	}
	
	protected void write(final String str) throws IOException {
		out.write(str);
	}
	
	protected void writeln(final String str) throws IOException {
		write(str);
		out.newLine();
	}
    
 
}
