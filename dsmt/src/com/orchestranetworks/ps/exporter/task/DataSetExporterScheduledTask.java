package com.orchestranetworks.ps.exporter.task;

import java.io.IOException;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExportConfigFactory;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

/**
 * A scheduled task that invokes the data set exporter. It is not required for executing
 * the exporter, but it is required if you wish to do it as a scheduled task within EBX.
 * 
 * EBX will introspect the class for its bean properties in order to allow the parameters
 * to be configured within EBX.
 */
public class DataSetExporterScheduledTask extends ScheduledTask {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private static final String DEFAULT_EXPORT_CONFIG_DATA_SPACE = "DataSetExport";
	private static final String DEFAULT_EXPORT_CONFIG_DATA_SET = "DataSetExport";
	
/*	private static final String VALIDATION_ACTION_NONE = "none";
	private static final String VALIDATION_ACTION_CONTINUE = "continue";
	private static final String VALIDATION_ACTION_STOP = "stop";
*/	
	protected String dataSpace;
	protected String dataSet;
	protected String exportConfigDataSpace = DEFAULT_EXPORT_CONFIG_DATA_SPACE;
	protected String exportConfigDataSet = DEFAULT_EXPORT_CONFIG_DATA_SET;
	protected String exportConfigID;
//	protected String validationAction = VALIDATION_ACTION_NONE;
	protected String mergeReadme = "false";
	protected String runSQLReports = "true";
	
	public String getRunSQLReports() {
		return runSQLReports;
	}

	public void setRunSQLReports(String runSQLReports) {
		this.runSQLReports = runSQLReports;
	}

	/**
	 * Get the ID of the data space to export
	 * 
	 * @return the ID of the data space
	 */
	public String getDataSpace() {
		return dataSpace;
	}

	/**
	 * Set the ID of the data space to export
	 * 
	 * @param dataSpace the ID of the data space
	 */
	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	/**
	 * Get the ID of the data set to export
	 * 
	 * @return the ID of the data set
	 */
	public String getDataSet() {
		return dataSet;
	}

	/**
	 * Set the ID of the data set to export
	 * 
	 * @param dataSet the ID of the data set
	 */
	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	/**
	 * Get the ID of the data space for the export config records
	 * 
	 * @return the ID of the data space
	 */
	public String getExportConfigDataSpace() {
		return exportConfigDataSpace;
	}

	/**
	 * Set the ID of the data space for the export config records
	 * 
	 * @param exportConfigDataSpace the ID of the data space
	 */
	public void setExportConfigDataSpace(String exportConfigDataSpace) {
		this.exportConfigDataSpace = exportConfigDataSpace;
	}

	/**
	 * Get the ID of the data set for the export config records
	 * 
	 * @return the ID of the data set
	 */
	public String getExportConfigDataSet() {
		return exportConfigDataSet;
	}

	/**
	 * Set the ID of the data set for the export config records
	 * 
	 * @param exportConfigDataSet the ID of the data set
	 */
	public void setExportConfigDataSet(String exportConfigDataSet) {
		this.exportConfigDataSet = exportConfigDataSet;
	}

	/**
	 * Get the ID of the export config
	 * 
	 * @return the ID
	 */
	public String getExportConfigID() {
		return exportConfigID;
	}

	/**
	 * Set the ID of the export config
	 * 
	 * @param exportConfigID the ID
	 */
	public void setExportConfigID(String exportConfigID) {
		this.exportConfigID = exportConfigID;
	}

	/**
	 * Get the validation action to take: either "none" (don't perform validation),
	 * "continue" (perform validation but keep processing),
	 * or "stop" (perform validation and stop processing if there are errors).
	 * 
	 * @return the validation action to take
	 *//*
	public String getValidationAction() {
		return validationAction;
	}

	*//**
	 * Set the validation action to take: either "none" (don't perform validation),
	 * "continue" (perform validation but keep processing),
	 * or "stop" (perform validation and stop processing if there are errors).
	 * 
	 * @param validationAction the validation action to take
	 *//*
	public void setValidationAction(String validationAction) {
		this.validationAction = validationAction;
	}*/

	@Override
	public void execute(ScheduledExecutionContext context)
			throws OperationException, ScheduledTaskInterruption {
		LOG.info("DataSetExporterScheduledTask: execute");
		final Repository repo = context.getRepository();
		final Session session = context.getSession();

		final AdaptationHome dataSpaceRef = repo.lookupHome(HomeKey.forBranchName(dataSpace));
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
		
		/*if (validationAction != null && ! VALIDATION_ACTION_NONE.equalsIgnoreCase(validationAction)) {
			// Perform the validation
			final ValidationReport validationReport = dataSetRef.getValidationReport();
			// Throw exception if there are validation errors and we are supposed to stop
			if (VALIDATION_ACTION_STOP.equalsIgnoreCase(validationAction)
					&& validationReport.hasItemsOfSeverityOrMore(Severity.ERROR)) {
				throw OperationException.createError("Validation failed.");
			}
		}
		*/
		final TableExporterFactory tableExporterFactory = createTableExporterFactory();
		final DataSetExportConfig config = createConfig(repo);
		final DataSetExporter exporter = createExporter(tableExporterFactory, config);
		try {
			exporter.exportDataSet(session, dataSetRef);
		} catch (final IOException ex) {
			throw OperationException.createError(ex);
		}
	}
	
	@Override
	public void validate(ValueContextForValidation context) {
		if (dataSpace == null) {
			context.addError("dataSpace parameter is required.");
		}
		if (dataSet == null) {
			context.addError("dataSet parameter is required.");
		}
		if (exportConfigDataSpace == null) {
			context.addError("exportConfigDataSpace parameter is required.");
		}
		if (exportConfigDataSet == null) {
			context.addError("exportConfigDataSet parameter is required.");
		}
		if (exportConfigID == null) {
			context.addError("exportConfigID parameter is required.");
		}
		/*if (validationAction != null
				&& ! VALIDATION_ACTION_NONE.equalsIgnoreCase(validationAction)
				&& ! VALIDATION_ACTION_CONTINUE.equalsIgnoreCase(validationAction)
				&& ! VALIDATION_ACTION_STOP.equalsIgnoreCase(validationAction)) {
			context.addError("validationAction parameter must be one of these values: "
					+ VALIDATION_ACTION_NONE + ", "
					+ VALIDATION_ACTION_CONTINUE + ", or "
					+ VALIDATION_ACTION_STOP + ".");
		}*/
	}
	
	protected DataSetExporter createExporter(TableExporterFactory tableExporterFactory, DataSetExportConfig config) {
		LOG.info("DataSetExporterScheduledTask: createExporter");
		
		return new DataSetExporter(tableExporterFactory, config);
	}
	
	protected TableExporterFactory createTableExporterFactory() {
		LOG.info("DataSetExporterScheduledTask: createTableExporterFactory");
		return new DefaultTableExporterFactory();
	}
	
	protected DataSetExportConfig createConfig(Repository repo)
			throws OperationException {
		LOG.info("DataSetExporterScheduledTask: createConfig");
		// Look up the data space and data set for the export config records
		final AdaptationHome exportConfigDataSpaceRef = repo.lookupHome(
				HomeKey.forBranchName(exportConfigDataSpace));
		final Adaptation exportConfigDataSetRef = exportConfigDataSpaceRef.findAdaptationOrNull(
				AdaptationName.forName(exportConfigDataSet));
		final DataSetExportConfigFactory configFactory = new DataSetExportConfigFactory();
		return configFactory.createFromEBXRecord(
				exportConfigDataSetRef, exportConfigID);
	}

	public String getMergeReadme() {
		return mergeReadme;
	}

	public void setMergeReadme(String mergeReadme) {
		this.mergeReadme = mergeReadme;
	}
}
