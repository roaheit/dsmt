package com.orchestranetworks.ps.exporter;

import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;

/**
 * An interface for creating table exporters
 */
public interface TableExporterFactory {
	/**
	 * Create a table exporter
	 * 
	 * @param tableConfig the export table config
	 * @param table the table
	 * @return the table exporter
	 */
	public TableExporter getTableExporter(DataSetExportTableConfig tableConfig, AdaptationTable table);
}
