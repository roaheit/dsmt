package com.orchestranetworks.ps.exporter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.citi.ebx.dsmt.util.DSMTExportBean;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

/**
 * This exports multiple tables from a data set using a specified configuration
 */
public class DataSetExporter {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private static final String FILE_NAME_NAME_VAR = "[NAME]";
	private static final String FILE_NAME_LABEL_VAR = "[LABEL]";
	private static final String DEFAULT_FILE_NAME = "[NAME].dat";
	
	protected DataSetExportConfig config;
	protected TableExporterFactory tableExporterFactory;
	
	public DataSetExporter() {
		this.tableExporterFactory = new DefaultTableExporterFactory();
	}
	
	public DataSetExporter(final TableExporterFactory tableExporterFactory,
			final DataSetExportConfig config) {
		this.tableExporterFactory = tableExporterFactory;
		this.config = config;
	}
	
	/**
	 * Get the config
	 * 
	 * @return the config
	 */
	public DataSetExportConfig getConfig() {
		return config;
	}

	/**
	 * Set the config
	 * 
	 * @param config the config
	 */
	public void setConfig(DataSetExportConfig config) {
		this.config = config;
	}
	
	/**
	 * Get the factory used to create the table exporters
	 * 
	 * @return the factory
	 */
	public TableExporterFactory getTableExporterFactory() {
		return tableExporterFactory;
	}

	/**
	 * Set the factory used to create the table exporters
	 * 
	 * @param tableExporterFactory the factory
	 */
	public void setTableExporterFactory(TableExporterFactory tableExporterFactory) {
		this.tableExporterFactory = tableExporterFactory;
	}

	public DSMTExportBean exportDataSet(Session session, Adaptation dataSet) throws IOException, OperationException {
		LOG.debug("DataSetExporter: exportDataSet");
		
		LOG.info("User running the export is " + session.getUserReference().getUserId() + ", export Config ID = " + config.getExportConfigID());
		
		LOG.debug("DataSetExporter: dataSet = " + dataSet.getAdaptationName().getStringName());
		final Set<DataSetExportTableConfig> tableConfigs = config.getTableConfigs();
		
		DSMTExportBean htBean = new DSMTExportBean();
		htBean.setOutputFilePath(config.getFolder().getAbsolutePath());
		htBean.setConfigID(config.getExportConfigID());
		
		
		// The same table path can apply to multiple table configs, so we will load them into a map
		// with the table path as key and a set of table configs as value.
		final HashMap<String, Set<DataSetExportTableConfig>> tableConfigMap = new HashMap<String, Set<DataSetExportTableConfig>>();
		for (DataSetExportTableConfig tableConfig: tableConfigs) {
			// Table configs for an external data set have to be treated special
			// so don't put them in the same collection with the others
			if (tableConfig.getExternalDataSet() == null) {
				final String tablePath = tableConfig.getTablePath();
				Set<DataSetExportTableConfig> tableConfigSet = tableConfigMap.get(tablePath);
				if (tableConfigSet == null) {
					tableConfigSet = new HashSet<DataSetExportTableConfig>();
					tableConfigSet.add(tableConfig);
					tableConfigMap.put(tablePath,  tableConfigSet);
				} else {
					tableConfigSet.add(tableConfig);
				}
			}
		}
		
		// Find all the table nodes in the data set. They could be within groups as well.
		final Set<SchemaNode> allTableNodes = ExporterUtils.getAllTableNodes(dataSet.getSchemaNode());
		for (SchemaNode tableNode: allTableNodes) {
			final Path tablePath = tableNode.getPathInSchema();
			// Find the table configs for the table node's path.
			final Set<DataSetExportTableConfig> tableConfigSet =
					getTableConfigsForPath(tablePath, tablePath.format(), tableConfigMap);
			if (tableConfigSet != null) {
				final AdaptationTable table = dataSet.getTable(tablePath);
				for (DataSetExportTableConfig tableConfig: tableConfigSet) {
					exportTable(session, table,htBean, tableConfig);
				}
			}
		}
		
		final List<String> fileNames = new ArrayList<String>();
		// Handle the table configs that are for an external data set (which we skipped earlier)
		for (DataSetExportTableConfig tableConfig: tableConfigs) {
			
			fileNames.add(tableConfig.getFileName()); 
			
			if (tableConfig.getExternalDataSet() != null) {
				final Path tablePath = Path.parse(tableConfig.getTablePath());
				final AdaptationTable table = tableConfig.getExternalDataSet().getTable(tablePath);
				
				exportTable(session, table,htBean, tableConfig);
			}
		}
		htBean.setFileNames(fileNames);
		
		return htBean;
	}
	
	protected File getFileForTable(SchemaNode tableNode, DataSetExportTableConfig tableConfig, Locale locale) {
		LOG.debug("DataSetExporter: getFileForTable");
		LOG.debug("DataSetExporter: tableNode = " + tableNode.getPathInSchema().format());
		String fileName = tableConfig.getFileName();
		LOG.debug("DataSetExporter: filename from config = " + fileName);
		// If not specifed, use default
		if (fileName == null) {
			fileName = DEFAULT_FILE_NAME;
		}
		if (fileName.contains(FILE_NAME_NAME_VAR)) {
			// Substitute the name of the table
			fileName = fileName.replace(FILE_NAME_NAME_VAR, tableNode.getPathInSchema().getLastStep().format());
		} else if (fileName.contains(FILE_NAME_LABEL_VAR)) {
			// Substitute the label of the table
			fileName = fileName.replace(FILE_NAME_LABEL_VAR, tableNode.getLabel(locale));
		}
		LOG.debug("DataSetExporter: filename = " + fileName);
		final File file = new File(config.getFolder(), fileName);
		file.setReadable(true, false);
		file.setWritable(true, true);
		file.setExecutable(true, false);
		return file;
	}
	
	private void exportTable(final Session session, final AdaptationTable table,DSMTExportBean exportBean,
			final DataSetExportTableConfig tableConfig) throws IOException, OperationException {
		// Get the file to output to
		final File file = getFileForTable(table.getTableNode(), tableConfig, session.getLocale());
		final FileOutputStream fileOut = new FileOutputStream(file);
		
		try {
			final TableExporter tableExporter = tableExporterFactory.getTableExporter(
					tableConfig, table);
			tableExporter.exportTable(session,table,exportBean, fileOut, tableConfig, tableExporterFactory);
		} finally {
			fileOut.close();
		}
	}
	
	private static Set<DataSetExportTableConfig> getTableConfigsForPath(
			final Path tablePath, final String mapKey,
			final Map<String, Set<DataSetExportTableConfig>> tableConfigMap) {
		LOG.debug("DataSetExporter: getTableConfigsForPath");
		LOG.debug("DataSetExporter: tablePath = " + tablePath.format());
		LOG.debug("DataSetExporter: mapKey = " + mapKey);
		// The table path may be a complete path to a table. It could match multiple
		// tables, such as '/root/*' or '/root/groupA/*'. So we start at the last step
		// and recursively move up the tree until we find one that matches.
		// This way we find the most specific match.
		final Set<DataSetExportTableConfig> tableConfigs = tableConfigMap.get(mapKey);
		if (tableConfigs == null) {
			Path parentPath = tablePath.getPathWithoutLastStep();
			if (parentPath != null && ! "/".equals(parentPath.format())) {
				final String key = parentPath.format() + "/*";
				return getTableConfigsForPath(parentPath, key, tableConfigMap);
			}
		}
		return tableConfigs;
	}
}
