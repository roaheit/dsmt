package com.orchestranetworks.ps.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.workflow.ProcessInstance;
import com.orchestranetworks.workflow.ProcessInstanceKey;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.UserTask;
import com.orchestranetworks.workflow.WorkflowEngine;

public class LaunchWorkflowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	
	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	private static final long WAIT_FOR_WORK_ITEM_MILLIS = 1000;
	private static final int MAX_NUM_OF_WAITS = 60;
	
	protected String workflowPublication;
	protected Map<String, String> inputParameters;
	protected boolean redirectToUserTask;
	
	public String getWorkflowPublication() {
		return workflowPublication;
	}

	public void setWorkflowPublication(String workflowPublication) {
		this.workflowPublication = workflowPublication;
	}

	public Map<String, String> getInputParameters() {
		return inputParameters;
	}

	public void setInputParameters(Map<String, String> inputParameters) {
		this.inputParameters = inputParameters;
	}

	public boolean isRedirectToUserTask() {
		return redirectToUserTask;
	}

	public void setRedirectToUserTask(boolean redirectToUserTask) {
		this.redirectToUserTask = redirectToUserTask;
	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		final WorkflowEngine wfEngine = WorkflowEngine.getFromServiceContext(sContext);
		final ProcessLauncher launcher = wfEngine.getProcessLauncher(PublishedProcessKey.forName(workflowPublication));
		if (inputParameters != null) {
			for (String key: inputParameters.keySet()) {
				launcher.setInputParameter(key, inputParameters.get(key));
			}
		}
		final ProcessInstanceKey processInstanceKey;
		try {
			processInstanceKey = launcher.launchProcess();
		} catch (OperationException ex) {
			throw new ServletException(ex);
		}
		final String redirectURL;
		if (redirectToUserTask) {
			ProcessInstance processInstance = null;
			try {
				int i;
				for (i = 0; i < MAX_NUM_OF_WAITS &&
						(processInstance = wfEngine.getProcessInstance(processInstanceKey)).getWorkItems().isEmpty(); i++) {
					Thread.sleep(WAIT_FOR_WORK_ITEM_MILLIS);
				}
				if (i == MAX_NUM_OF_WAITS) {
					LoggingCategory.getWorkflow().error("Max number of retries reached for wait of first user task work item");
				}
			} catch (InterruptedException ex) {
				LoggingCategory.getWorkflow().error("Waiting for first user task work item interrupted", ex);
			}
			@SuppressWarnings("unchecked")
			final List<UserTask.WorkItem> workItems = processInstance.getWorkItems();
			if (! workItems.isEmpty()) {
				final UserTask.WorkItem workItem = workItems.get(0);
				redirectURL = sContext.getURLForSelection(workItem.getWorkItemKey());
			} else {
				redirectURL = sContext.getURLForEndingService();
			}
		} else {
			redirectURL = sContext.getURLForEndingService();
		}
		final PrintWriter out = res.getWriter();
		out.print("<script>window.location.href='" + redirectURL + "';</script>");
	}
}
