package com.orchestranetworks.ps.procedure;

import java.util.Map;
import java.util.Set;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;

public class ModifyValuesProcedure extends ExecutableProcedure {
	private Map<Path, Object> pathValueMap;
	private boolean enableAllPrivileges;
	private boolean disableTriggerActivation;

	public static ProcedureResult execute(final Adaptation recordAdaptation, final Map<Path,Object> pathValueMap, final Session session) {
		return ModifyValuesProcedure.execute(recordAdaptation, pathValueMap, session, false, false);
	}
	
	public static ProcedureResult execute(final Adaptation recordAdaptation, final Map<Path,Object> pathValueMap, final Session session,
			final boolean enableAllPrivileges, final boolean disableTriggerActivation) {
		final ModifyValuesProcedure procedure = new ModifyValuesProcedure(
				recordAdaptation, pathValueMap);
		procedure.enableAllPrivileges = enableAllPrivileges;
		procedure.disableTriggerActivation = disableTriggerActivation;
		return procedure.executeFromService(session);
	}
	
	public ModifyValuesProcedure() {
	}

	public ModifyValuesProcedure(final Adaptation recordAdaptation, final Map<Path,Object> pathValueMap) {
		this.adaptation = recordAdaptation;
		this.pathValueMap = pathValueMap;
	}

	public Map<Path, Object> getPathValueMap() {
		return pathValueMap;
	}

	public void setPathValueMap(Map<Path, Object> pathValueMap) {
		this.pathValueMap = pathValueMap;
	}

	public boolean isEnableAllPrivileges() {
		return enableAllPrivileges;
	}

	public void setEnableAllPrivileges(boolean enableAllPrivileges) {
		this.enableAllPrivileges = enableAllPrivileges;
	}

	public boolean isDisableTriggerActivation() {
		return disableTriggerActivation;
	}

	public void setDisableTriggerActivation(boolean disableTriggerActivation) {
		this.disableTriggerActivation = disableTriggerActivation;
	}

	@Override
	public void execute(ProcedureContext pContext) throws Exception {
		final ValueContextForUpdate vc = pContext.getContext(
				adaptation.getAdaptationName());
		final Set<Path> paths = pathValueMap.keySet();
		for (Path path : paths) {
			vc.setValue(pathValueMap.get(path), path);
		}
		if (enableAllPrivileges) {
			pContext.setAllPrivileges(true);
		}
		if (disableTriggerActivation) {
			pContext.setTriggerActivation(false);
		}
		pContext.doModifyContent(adaptation, vc);
		if (enableAllPrivileges) {
			pContext.setAllPrivileges(false);
		}
		if (disableTriggerActivation) {
			pContext.setTriggerActivation(true);
		}
	}
}
