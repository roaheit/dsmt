package com.orchestranetworks.ps.procedure;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;

public abstract class ExecutableProcedure implements Procedure {
	protected Adaptation adaptation;

	public Adaptation getAdaptation() {
		return adaptation;
	}

	public void setAdaptation(Adaptation adaptation) {
		this.adaptation = adaptation;
	}

	@Override
	public abstract void execute(ProcedureContext pContext) throws Exception;

	public ProcedureResult executeFromService(final Session session) {
		final AdaptationHome dataSpace =  adaptation.isSchemaInstance()
				? adaptation.getHome() : adaptation.getContainer().getHome();
		return executeFromService(session, dataSpace);
	}
	
	public ProcedureResult executeFromService(final Session session, final AdaptationHome dataSpace) {
		final ProgrammaticService svc = ProgrammaticService.createForSession(
				session, dataSpace);
		return svc.execute(this);
	}
}
