package com.citi.ebx.rule;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryHandler;

public class RuleDimensionTrigger extends TableTrigger {
	private String deletedPK;
	
	@Override
	public void setup(TriggerSetupContext context) {
	}

	@Override
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
		deletedPK = context.getAdaptationOccurrence().getOccurrencePrimaryKey().format();
	}

	@Override
	public void handleAfterDelete(AfterDeleteOccurrenceContext context)
			throws OperationException {
		final DirectoryHandler dir = context.getSession().getDirectory();
		final UserReference user = context.getSession().getUserReference();
		// Don't do it for admins because they may want to have more control
		if (! dir.isUserInRole(user, UserReference.ADMINISTRATOR)) {
			final Path currTablePath = context.getTable().getTablePath();
			final boolean isRuleDimension = RulePaths._RuleDimension.getPathInSchema().equals(currTablePath);
			final Path tablePath = isRuleDimension
					? RulePaths._RuleDimensionTreeValueSet.getPathInSchema()
					: RulePaths._DimensionValueGroupTreeValueSet.getPathInSchema();
			final Adaptation dataSet = context.getTable().getContainerAdaptation();
			final AdaptationTable linkedTable = dataSet.getTable(tablePath);
			final Path fieldPath = isRuleDimension
					? RulePaths._RuleDimensionTreeValueSet._RuleDimension
					: RulePaths._DimensionValueGroupTreeValueSet._DimensionValueGroup;
			final String predicate = fieldPath.format() + "='" + deletedPK + "'";
			final RequestResult reqRes = linkedTable.createRequestResult(predicate);
			try {
				for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
					context.getProcedureContext().doDelete(record.getAdaptationName(), false);
				}
			} finally {
				reqRes.close();
			}
		}
	}
}
