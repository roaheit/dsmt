package com.citi.ebx.rule;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.ConstraintEnumeration;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;

public class RuleValueConstraintEnumeration implements ConstraintEnumeration {
	@Override
	public void checkOccurrence(Object obj, ValueContextForValidation context)
			throws InvalidSchemaException {
	}

	@Override
	public void setup(ConstraintContext context) {
		// do nothing
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext context)
			throws InvalidSchemaException {
		return "Value must be a row in the table defined by the rule.";
	}

	@Override
	public String displayOccurrence(Object obj, ValueContext context, Locale locale)
			throws InvalidSchemaException {
		final String pk = (String) obj;
		final AdaptationTable table = getTableForRule(context);
		if (table == null) {
			return pk;
		}
		final Adaptation record = table.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(pk));
		if (record == null) {
			return pk;
		}
		final String label = record.getLabelOrName(locale);
		return label;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getValues(ValueContext context) throws InvalidSchemaException {
		final AdaptationTable table = getTableForRule(context);
		final ArrayList<String> values = new ArrayList<String>();
		if (table == null) {
			return values;
		}
		final RequestResult reqRes = table.createRequestResult(null);
		try {
			for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
				values.add(record.getOccurrencePrimaryKey().format());
			}
		} finally {
			reqRes.close();
		}
		return values;
	}
	
	private static AdaptationTable getTableForRule(final ValueContext context) {
		final Path currNodePath = context.getNode().getTableNode().getPathInSchema();
		final boolean isRuleDimensionTreeValueSet = RulePaths._RuleDimensionTreeValueSet.getPathInSchema().equals(currNodePath);
		final Path treeFieldPath = isRuleDimensionTreeValueSet
				? Path.PARENT.add(RulePaths._RuleDimensionTreeValueSet._Tree)
				: Path.PARENT.add(RulePaths._DimensionValueGroupTreeValueSet._Tree);
		final String treePK = (String) context.getValue(treeFieldPath);
		final Repository repo = context.getHome().getRepository();
		final AdaptationTable table;
		if (treePK == null) {
			final Path dimensionRelatedFieldPath = isRuleDimensionTreeValueSet
					? Path.PARENT.add(RulePaths._RuleDimensionTreeValueSet._RuleDimension)
					: Path.PARENT.add(RulePaths._DimensionValueGroupTreeValueSet._DimensionValueGroup);
			final String dimensionRelatedPK = (String) context.getValue(dimensionRelatedFieldPath);
			table = (dimensionRelatedPK == null) ?
					null : getDimensionTable(dimensionRelatedPK, isRuleDimensionTreeValueSet, repo);
		} else {
			table = getTreeTable(treePK, repo);
		}
		return table;
	}
	
	private static AdaptationTable getDimensionTable(final String dimensionRelatedPK,
			final boolean isRuleDimensionTreeValueSet, final Repository repo) {
		final String[] pkValues = dimensionRelatedPK.split("\\|");
		final int dimensionStartIndex = isRuleDimensionTreeValueSet ? 2 : 1;
		final String dataSpace = pkValues[dimensionStartIndex];
		final String dataSet = pkValues[dimensionStartIndex + 1];
		final String tablePath = pkValues[dimensionStartIndex + 2];
		final AdaptationTable table = getTable(repo, dataSpace, dataSet, tablePath);
		return table;
	}
	
	private static AdaptationTable getTreeTable(final String treePK, final Repository repo) {
		final String[] pkValues = treePK.split("\\|");
		final String dataSpace = pkValues[0];
		final String dataSet = pkValues[1];
		// dimension table is index 2. We don't need it here.
		final String tablePath = pkValues[3];
		final AdaptationTable table = getTable(repo, dataSpace, dataSet, tablePath);
		return table;
	}
	
	private static AdaptationTable getTable(final Repository repo, final String dataSpace, final String dataSet, final String tablePath) {
		final AdaptationHome dataSpaceRef = repo.lookupHome(HomeKey.forBranchName(dataSpace));
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
		final AdaptationTable table = dataSetRef.getTable(Path.parse(tablePath));
		
		return table;
	}
}
