package com.citi.ebx.rule;

import java.util.ArrayList;
import java.util.List;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.SchemaTypeName;
import com.orchestranetworks.schema.info.SelectionLink;
import com.orchestranetworks.service.ServiceKey;
import com.orchestranetworks.ui.UIBeanEditor;
import com.orchestranetworks.ui.UIButtonIcon;
import com.orchestranetworks.ui.UIButtonLayout;
import com.orchestranetworks.ui.UIButtonSpecJSAction;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.UIHttpManagerComponent;
import com.orchestranetworks.ui.UIResponseContext;

public class ValueSetSelectionNodeEditor extends UIBeanEditor {
	private int maxValueSetCount = 10;
	private int maxValueCount = 10;

	public int getMaxValueSetCount() {
		return maxValueSetCount;
	}

	public void setMaxValueSetCount(int maxValueSetCount) {
		this.maxValueSetCount = maxValueSetCount;
	}

	public int getMaxValueCount() {
		return maxValueCount;
	}

	public void setMaxValueCount(int maxValueCount) {
		this.maxValueCount = maxValueCount;
	}

	@Override
	public void addForDisplay(UIResponseContext context) {
		final Adaptation currRecord = getCurrentRecord(context);
		// There should be a current record since selection nodes only display after saving.
		if (currRecord == null) {
			return;
		}
		final Path currNodePath = context.getNode().getTableNode().getPathInSchema();
		final boolean isRuleDimension = RulePaths._RuleDimension.getPathInSchema().equals(currNodePath);
		final Path dimensionPath = isRuleDimension
				? RulePaths._RuleDimension._Dimension
				: RulePaths._DimensionValueGroup._Dimension;
		final String dimensionPK = currRecord.getString(dimensionPath);
		// Shouldn't be null since it's part of primary key
		if (dimensionPK == null) {
			return;
		}
		final String selectionNodeXPath = context.getNode().getSelectionLink().getXPathString();
		final String predicate = createPredicate(selectionNodeXPath, context);
		
		final Adaptation thisDataSetRef = context.getValueContext().getAdaptationInstance();
		UIHttpManagerComponent uiComp = context.createWebComponentForSubSession();
		final Path linkedTablePath = isRuleDimension
				? RulePaths._RuleDimensionTreeValueSet.getPathInSchema()
				: RulePaths._DimensionValueGroupTreeValueSet.getPathInSchema();
		uiComp.selectNode(thisDataSetRef, linkedTablePath, predicate);
		
		final UIButtonSpecJSAction selectionButton = context.buildButtonPreview(
				uiComp.getURIWithParameters(), UserMessage.createInfo("View / Edit"));
		context.addButtonJavaScript(selectionButton);
		
		uiComp = context.createWebComponentForSubSession();
		uiComp.selectNode(thisDataSetRef, linkedTablePath, predicate);
		uiComp.setService(ServiceKey.CREATE);
		
		final UIButtonSpecJSAction createButton = context.buildButtonPreview(
				uiComp.getURIWithParameters(), UserMessage.createInfo(""));
		createButton.setButtonLayout(UIButtonLayout.ICON_ONLY);
		createButton.setButtonIcon(UIButtonIcon.ADD);
		context.addButtonJavaScript(createButton);
		
		final String[] pkValues = dimensionPK.split("\\|");
		final String dataSpace = pkValues[0];
		final String dataSet = pkValues[1];
		final Repository repo = thisDataSetRef.getHome().getRepository();
		final AdaptationHome dataSpaceRef = repo.lookupHome(HomeKey.forBranchName(dataSpace));
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
		
		final List<Adaptation> records = getSelectionNodeRecords(currRecord, context.getNode(), maxValueSetCount + 1);
		if (! records.isEmpty()) {
			context.add_cr("<table class='ebx_FieldList' border='1'>");
			context.add_cr("  <thead>");
			context.add_cr("    <tr class='ebx_Field'>");
			
			context.add_cr("        <th class='ebx_Label' style='font-weight:bold'>");
			final Path treeFieldPath = isRuleDimension
					? RulePaths._RuleDimensionTreeValueSet._Tree
					: RulePaths._DimensionValueGroupTreeValueSet._Tree;
			final SchemaNode treeFieldNode = records.get(0).getSchemaNode().getNode(treeFieldPath);
			final String treeFieldNodeLabel = treeFieldNode.getLabel(context.getLocale());
			context.addUILabel(new UIFormLabelSpec(treeFieldNodeLabel));
			context.add_cr();
			context.add_cr("</th>");
			
			context.add_cr("        <th class='ebx_Label' style='font-weight:bold'>");
			final Path valueFieldPath = isRuleDimension
					? RulePaths._RuleDimensionTreeValueSet._Value
					: RulePaths._DimensionValueGroupTreeValueSet._Value;
			final SchemaNode valueFieldNode = records.get(0).getSchemaNode().getNode(valueFieldPath);
			final String valueFieldNodeLabel = valueFieldNode.getLabel(context.getLocale());
			context.addUILabel(new UIFormLabelSpec(valueFieldNodeLabel));
			context.add_cr();
			context.add_cr("</th>");
			
			context.add_cr("    </tr>");
			context.add_cr("  </thead>");
			context.add_cr("  <tbody>");
			
			for (int i = 0; i < records.size() && i < maxValueSetCount; i++) {
				final Adaptation record = records.get(i);
				context.add_cr("    <tr class='ebx_Field'>");
				
				context.add_cr("      <td class='ebx_Label'>");
				final Adaptation treeRecord = treeFieldNode.getFacetOnTableReference().getLinkedRecord(record);
				if (treeRecord == null) {
					context.addUILabel(new UIFormLabelSpec("(All Records)"));
				} else {
					context.addUILabel(new UIFormLabelSpec(treeRecord.getLabel(context.getLocale())));
					uiComp = context.createWebComponentForSubSession();
					uiComp.selectInstanceOrOccurrence(treeRecord);
					final UIButtonSpecJSAction viewTreeButton = context.buildButtonPreview(
							uiComp.getURIWithParameters(), UserMessage.createInfo(""));
					viewTreeButton.setButtonLayout(UIButtonLayout.ICON_ONLY);
					context.addButtonJavaScript(viewTreeButton);
				}
				context.add_cr();
				context.add_cr("      </td>");

				context.add_cr("      <td class='ebx_Input'>");
				context.startExpandCollapseBlock(UserMessage.createInfo(""), true);
				context.add_cr();
				context.add_cr("<table class='ebx_FieldList'>");
				context.add_cr("  <tbody>");
				
				final List<Adaptation> valueRecords = getValueRecords(currRecord, dataSetRef, record, treeRecord, maxValueCount + 1);
				for (int j = 0; j < valueRecords.size() && j < maxValueCount; j++) {
					final Adaptation valueRecord = valueRecords.get(j);
					context.add_cr("    <tr class='ebx_Field'>");
					context.add_cr("      <td class='ebx_Label'>");
					if (valueRecord == null) {
						context.addUILabel(new UIFormLabelSpec("[not found]"));
					} else {
						context.addUILabel(new UIFormLabelSpec(valueRecord.getLabelOrName(context.getLocale())));
						uiComp = context.createWebComponentForSubSession();
						uiComp.selectInstanceOrOccurrence(valueRecord);
						final UIButtonSpecJSAction viewValueButton = context.buildButtonPreview(
								uiComp.getURIWithParameters(), UserMessage.createInfo(""));
						viewValueButton.setButtonLayout(UIButtonLayout.ICON_ONLY);
						context.addButtonJavaScript(viewValueButton);
					}
					context.add_cr();
					context.add_cr("      </td>");
					context.add_cr("    </tr>");
				}
				if (valueRecords.size() > maxValueCount) {
					context.add_cr("    <tr class='ebx_Field'>");
					context.add_cr("      <td class='ebx_Label'>");
					context.addUILabel(new UIFormLabelSpec("... more ..."));
					context.add_cr("      </td>");
					context.add_cr("    </tr>");
				}
				
				context.add_cr("  </tbody>");
				context.add_cr("</table>");
				context.endExpandCollapseBlock();
				context.add_cr();
				context.add_cr("      </td>");
				context.add_cr("    </tr>");
			}
			if (records.size() > maxValueSetCount) {
				context.add_cr("    <tr class='ebx_Field'>");
				context.add_cr("      <td class='ebx_Label' colspan='2'>");
				context.addUILabel(new UIFormLabelSpec("... more ..."));
				context.add_cr("      </td>");
				context.add_cr("    </tr>");
			}
			context.add_cr("  </tbody>");
			context.add_cr("</table>");
		}
	}

	@Override
	public void addForEdit(UIResponseContext context) {
		// No implementation needed because selection nodes aren't editable
	}
	
	private static String createPredicate(final String selectionNodeXpath, final UIResponseContext context) {
		final String selectionNodePredicate;
		final int predicateStartInd = selectionNodeXpath.indexOf('[');
		if (predicateStartInd == -1) {
			selectionNodePredicate = selectionNodeXpath;
		} else {
			final int predicateEndInd = selectionNodeXpath.indexOf(']', predicateStartInd);
			if (predicateEndInd == -1) {
				selectionNodePredicate = selectionNodeXpath;
			} else if (predicateEndInd == predicateStartInd + 1) {
				selectionNodePredicate = "";
			} else {
				selectionNodePredicate = selectionNodeXpath.substring(predicateStartInd + 1, predicateEndInd);
			}
		}
		final String[] splitXpath = selectionNodePredicate.split("\\$\\{");
		final StringBuffer strBuff = new StringBuffer();
		for (int i = 0; i < splitXpath.length; i++) {
			final String segment = splitXpath[i];
			final int endIndex = segment.indexOf("}");
			if (endIndex == -1) {
				strBuff.append(segment);
			} else {
				final String pathStr = segment.substring(0, endIndex);
				final Path path = Path.parse(pathStr);
				
				final Object value = context.getValue(path);
				if (value != null) {
					final SchemaNode node = context.getNode(path);
					final SchemaTypeName nodeType = node.getXsTypeName();
					if (! (SchemaTypeName.XS_BOOLEAN.equals(nodeType)
							|| SchemaTypeName.XS_DECIMAL.equals(nodeType)
							|| SchemaTypeName.XS_INT.equals(nodeType)
							|| SchemaTypeName.XS_INTEGER.equals(nodeType))) {
						strBuff.append("'");
					}
					strBuff.append(value);
					if (! (SchemaTypeName.XS_BOOLEAN.equals(nodeType)
							|| SchemaTypeName.XS_DECIMAL.equals(nodeType)
							|| SchemaTypeName.XS_INT.equals(nodeType)
							|| SchemaTypeName.XS_INTEGER.equals(nodeType))) {
						strBuff.append("'");
					}
				}
				if (segment.length() > endIndex + 1) {
					strBuff.append(segment.substring(endIndex + 1));
				}
			}
		}
		return strBuff.toString();
	}
	
	private static Adaptation getCurrentRecord(final UIResponseContext context) {
		final AdaptationTable table = context.getValueContext().getAdaptationTable();
		final Object[] pkValues = getPrimaryKeyValues(context);
		final PrimaryKey pk = table.computePrimaryKey(pkValues);
		final Adaptation record = table.lookupAdaptationByPrimaryKey(pk);
		return record;
	}
	
	private static Object[] getPrimaryKeyValues(final UIResponseContext context) {
		final SchemaNode node = context.getNode();
		final Path pathToRecordRoot = getPathToRoot(node);
		final SchemaNode[] pkNodes = node.getTableNode().getTablePrimaryKeyNodes();
		final Object[] pkValues = new Object[pkNodes.length];
		for (int i = 0; i < pkNodes.length; i++) {
			final Path pkNodePath = pathToRecordRoot.add(pkNodes[i].getPathInAdaptation());
			pkValues[i] = context.getValue(pkNodePath);
		}
		return pkValues;
	}
	
	private static Path getPathToRoot(final SchemaNode node) {
		final Path path = node.getPathInAdaptation();
		final int numOfSteps = path.getSize();
		final StringBuffer pathToRecordRootBuff = new StringBuffer();
		for (int i = 0; i < numOfSteps; i++) {
			pathToRecordRootBuff.append("..");
			if (i < numOfSteps - 1) {
				pathToRecordRootBuff.append("/");
			}
		}
		return Path.parse(pathToRecordRootBuff.toString());
	}
	
	private static List<Adaptation> getSelectionNodeRecords(final Adaptation currRecord, final SchemaNode node, final int maxCount) {
		final SelectionLink selectionLink = node.getSelectionLink();
		final RequestResult reqRes = selectionLink.getSelectionResult(currRecord);
		final ArrayList<Adaptation> records = new ArrayList<Adaptation>();
		int count = 0;
		try {
			for (Adaptation record; (record = reqRes.nextAdaptation()) != null && count < maxCount; count++) {
				records.add(record);
			}
		} finally {
			reqRes.close();
		}
		return records;
	}
	
	private List<Adaptation> getValueRecords(final Adaptation currRecord, final Adaptation dataSetRef,
			final Adaptation linkedToRecord, final Adaptation tree, final int maxCount) {
		final Path currNodePath = currRecord.getSchemaNode().getTableNode().getPathInSchema();
		final boolean isRuleDimension = RulePaths._RuleDimension.getPathInSchema().equals(currNodePath);
		final Path valueFieldPath = isRuleDimension
				? RulePaths._RuleDimensionTreeValueSet._Value
				: RulePaths._DimensionValueGroupTreeValueSet._Value;
		@SuppressWarnings("unchecked")
		final List<String> values = linkedToRecord.getList(valueFieldPath);
		final String tablePath;
		if (tree == null) {
			final Path dimensionFieldPath = isRuleDimension
					? RulePaths._RuleDimension._Dimension
					: RulePaths._DimensionValueGroup._Dimension;
			final SchemaNode dimensionFieldNode = currRecord.getSchemaNode().getNode(dimensionFieldPath);
			final Adaptation dimension = dimensionFieldNode.getFacetOnTableReference().getLinkedRecord(currRecord);
			tablePath = dimension.getString(RulePaths._Dimension._TablePath);
		} else {
			tablePath = tree.getString(RulePaths._DimensionTree._TablePath);
		}
		final AdaptationTable table = dataSetRef.getTable(Path.parse(tablePath));
		
		final ArrayList<Adaptation> valueRecords = new ArrayList<Adaptation>();
		for (int i = 0; i < values.size() && i < maxCount; i++) {
			final String value = values.get(i);
			final Adaptation record = table.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(value));
			valueRecords.add(record);
		}
		return valueRecords;
	}
}
