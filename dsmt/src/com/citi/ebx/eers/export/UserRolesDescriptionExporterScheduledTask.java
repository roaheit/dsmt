package com.citi.ebx.eers.export;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryDefault;

public class UserRolesDescriptionExporterScheduledTask extends ScheduledTask {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();

	private String dataSpace;
	private String dataSet;
	private String fileName;

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	BufferedWriter writer = null;

	DirectoryDefault dirDef;

	
	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public void execute(ScheduledExecutionContext context)
			throws OperationException, ScheduledTaskInterruption {
		try {

			dirDef = DirectoryDefault.getInstance(Repository.getDefault());
			
			List<UserReference> users = new ArrayList<UserReference>();
			List<Role> roles = new ArrayList<Role>();

			Locale loc = Locale.getDefault();
			
			
			users = dirDef.getAllUserReferences();
			roles = dirDef.getAllSpecificRoles();
			

			LOG.info("DataSpace : "+dataSpace);
			LOG.info("DataSet : "+dataSet);
			LOG.info("WRITING to FILE");

			String eersFilePath = fileName;
			LOG.info("EERS feed will be created at " + eersFilePath);
			writer = new BufferedWriter(new FileWriter(eersFilePath));

			for (UserReference user : users) {
				for (Role role : roles) {
					
					
					if (dirDef.isUserInRole(user, role)) {

						String roleId = role.getRoleName();
						String userId = user.getUserId();
						
						if(userId==null || "".equalsIgnoreCase(userId)){
							userId="";
						}
						
						LOG.info("userId : "+userId);

						String roleName = role.getRoleName();
						
						LOG.info("roleName : "+roleName);
						
						String roleDesc = dirDef.getRoleDescription(role, loc);
						
						LOG.info("Role Desc : "+roleDesc);
					

					}
				}
			}

			writer.close();
			LOG.info("DONE WRITING");

		} catch (IOException e) {
			System.err.println(e);

		}
	}
/*
	private static final String ISARole = "ISA";
	private static final String FuncIndCode = "F1";
	private static final String AdminIndCode = "PA1";
	private static final String UNDERSCORE = "_";
	private static final String COLON = ":";
	private static final String SPACE = " ";
	private static final String TAB = "\t";*/
}