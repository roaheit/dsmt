package com.citi.ebx.eers.export;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Profile;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryDefault;
import com.orchestranetworks.service.directory.DirectoryHandler;
import com.orchestranetworks.service.directory.ProfileListContextBridge;

public class UserRolesExporterScheduledTask extends ScheduledTask {

//	private static final String EBX_DIRECTORY_ADMIN_ROLE_ID = "ebx.directory.role.id";
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();

	private String dataSpace;
	private String dataSet;
	private String fileName;

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	BufferedWriter writer = null;

	DirectoryDefault dirDef;

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public void execute(ScheduledExecutionContext context)
			throws OperationException, ScheduledTaskInterruption {
		try {

			final Repository repo = context.getRepository();
			final Session session = context.getSession();

			DirectoryHandler dir = session.getDirectory();
			dirDef = DirectoryDefault.getInstance(repo);
			List<UserReference> users = new ArrayList<UserReference>();
			List<Role> roles = new ArrayList<Role>();

			Locale loc = Locale.getDefault();

			List<Profile> profi = dir.getProfiles(ProfileListContextBridge
					.getForOwningAdaptation());

			for (Profile prof : ((List<Profile>) dir
					.getProfiles(ProfileListContextBridge
							.getForOwningAdaptation()))) {
				if (prof.isUserReference()) {
					users.add((UserReference) prof);
				} else if ((!prof.isBuiltIn() && prof.isSpecificRole())
						|| (prof.isBuiltInAdministrator())) {
					roles.add((Role) prof);
				}
			}

			LOG.info("DataSpace : "+dataSpace);
			LOG.info("DataSet : "+dataSet);
			LOG.info("WRITING to FILE");

			String eersFilePath = fileName;
			LOG.info("EERS feed will be created at " + eersFilePath);
			writer = new BufferedWriter(new FileWriter(eersFilePath));

			for (UserReference user : users) {
				for (Role role : roles) {
					if (dir.isUserInRole(user, role)) {

						String roleId = role.getRoleName();
						String userId = user.getUserId();
						
						if(userId==null || "".equalsIgnoreCase(userId)){
							userId="";
						}
						
						LOG.info("userId : "+userId);

						String roleName = role.getRoleName();
						
						LOG.info("roleName : "+roleName);

						if (null != roleName && !roleName.contains("admin")
								&& !roleName.contains("Admin")
								&& !roleName.contains("ADMIN")
								&& !roleName.contains("Rosetta")
								&& !roleName.contains("CLEMS")) {
							
							LOG.info("roleName except Admin");

							if (roleName.contains(COLON)) {
								roleName = roleName.replace(COLON, UNDERSCORE);

							}

							String roleDesc = dirDef.getRoleDescription(role,
									loc);
							
							if(null==roleDesc || "".equalsIgnoreCase(roleDesc)){
								roleDesc="";
							}
							
							LOG.info("RoleDesc : "+roleDesc);

							String lineText = null;
							// Functional ID
							if (!(userId.toLowerCase())
									.matches("[a-z]{2}[0-9]{5}")) {

								String adminUser = propertyHelper
										.getProp("ebx.directory.adminuserID");

								lineText = "165635" + TAB + SPACE + TAB + SPACE
										+ TAB + userId + TAB + SPACE
										+ TAB + SPACE + TAB + roleName + TAB
										+ roleDesc + TAB + adminUser + TAB
										+ SPACE + TAB + SPACE + TAB + SPACE
										+ TAB + SPACE + TAB + SPACE + TAB
										+ FuncIndCode + TAB + AdminIndCode
										+ TAB + SPACE;

							}
							// FOR ISA Role
							else if ((roleId.equalsIgnoreCase("ISA_Role"))
									&& (userId.toLowerCase())
											.matches("[a-z]{2}[0-9]{5}")) {

								LOG.info("ISA_Role");

								LOG.info("roleName" + roleName
										+ ", roleDesc = " + roleDesc);

								lineText = "165635" + TAB + SPACE + TAB + SPACE
										+ TAB + userId + TAB + SPACE
										+ TAB + SPACE + TAB + roleName + TAB
										+ roleDesc + TAB + userId
										+ TAB + SPACE + TAB + SPACE + TAB
										+ SPACE + TAB + SPACE + TAB + SPACE
										+ TAB + SPACE + TAB + ISARole + TAB
										+ SPACE;

							}
							// FOR SOEIDs
							else if ((userId.toLowerCase())
									.matches("[a-z]{2}[0-9]{5}")) {

								LOG.info("roleName" + roleName
										+ ", roleDesc = " + roleDesc);
								lineText = "165635" + TAB + SPACE + TAB + SPACE
										+ TAB + userId + TAB + SPACE
										+ TAB + SPACE + TAB + roleName + TAB
										+ roleDesc + TAB + userId
										+ TAB + SPACE + TAB + SPACE + TAB
										+ SPACE + TAB + SPACE + TAB + SPACE
										+ TAB + SPACE + TAB + SPACE + TAB
										+ SPACE;

							} else {

								LOG.info(userId
										+ " is neither functional id, nor SOE or ISA id.. ");
							}
							if (null != lineText) {
								LOG.info("check line text : "+lineText);
								writer.write(lineText);
								writer.newLine();
							}
						}

					}
				}
			}

			writer.close();
			LOG.info("DONE WRITING");

		} catch (IOException e) {
			System.err.println(e);

		}
	}

	private static final String ISARole = "ISA";
	private static final String FuncIndCode = "F1";
	private static final String AdminIndCode = "PA1";
	private static final String UNDERSCORE = "_";
	private static final String COLON = ":";
	private static final String SPACE = " ";
	private static final String TAB = "\t";
}