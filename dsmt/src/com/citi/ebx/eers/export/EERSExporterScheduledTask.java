package com.citi.ebx.eers.export;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;



import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Profile;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryDefault;
import com.orchestranetworks.service.directory.DirectoryHandler;
import com.orchestranetworks.service.directory.ProfileListContextBridge;

public class EERSExporterScheduledTask extends ScheduledTask {

	private static final String EBX_DIRECTORY_ADMIN_ROLE_ID = "ebx.directory.role.id";
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	protected Properties configProp = new Properties();
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	private String dataSpace;
	private String dataSet;

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public void execute(ScheduledExecutionContext context)
			throws OperationException, ScheduledTaskInterruption {
		try {

			final Repository repo = context.getRepository();
			final Session session = context.getSession();

			final AdaptationHome dataSpaceRef = repo.lookupHome(HomeKey
					.forBranchName(dataSpace));
			final Adaptation dataSetRef = dataSpaceRef
					.findAdaptationOrNull(AdaptationName.forName(dataSet));

			DirectoryHandler dir = session.getDirectory();
			List<UserReference> users = new ArrayList<UserReference>();
			List<Role> roles = new ArrayList<Role>();
			DirectoryDefault dirDef = DirectoryDefault.getInstance(repo);
			Locale loc = Locale.getDefault();

			for (Profile prof : ((List<Profile>) dir
					.getProfiles(ProfileListContextBridge
							.getForOwningAdaptation()))) {
				if (prof.isUserReference()) {
					users.add((UserReference) prof);
				} else if ((!prof.isBuiltIn() && prof.isSpecificRole())
						|| (prof.isBuiltInAdministrator())) {
					roles.add((Role) prof);
				}
			}
			
			LOG.info("checking roles : "+roles);
			LOG.info("WRITING to FILE");
			BufferedWriter writer = null;

			String eersFilePath = propertyHelper
					.getProp("dsmt.eers.feed.file.path");
			LOG.info("EERS feed will be created at " + eersFilePath);
			writer = new BufferedWriter(new FileWriter(eersFilePath));

			for (UserReference user : users) {
				for (Role role : roles) {
					if (dir.isUserInRole(user, role)) {

						String roleId = role.getRoleName();
						String userId = user.getUserId();

						String roleName = propertyHelper.getProp(roleId);
						
						/*
						 * below code to be migrated only with 
						 * higher version of code 
						 * EBX 5.6.1-Fix I
						 */
					//	String roleName = role.getRoleName();

						// for administrator user - change to readable name
						if (roleId.equalsIgnoreCase(propertyHelper
								.getProp(EBX_DIRECTORY_ADMIN_ROLE_ID))) {
							roleName = propertyHelper
									.getProp("ebx.directory.role.admin");

						}

						if (null != roleName) {
							
							// get role description - replace colon with
							// underscore
							// because of property file limitation

							if (roleName.contains(COLON)) {
								roleName = roleName.replace(COLON, UNDERSCORE);

							}

							String roleDesc = propertyHelper.getProp(roleName);
							
							/*
							 * below code to be migrated only with 
							 * higher version of code 
							 * EBX 5.6.1-Fix I
							 */
						//	String roleDesc = dirDef.getRoleDescription(role, loc);

							String lineText = null;
							// Functional ID
							if (!(userId.toLowerCase())
									.matches("[a-z]{2}[0-9]{5}")) {

								String adminUser = propertyHelper
										.getProp("ebx.directory.adminuserID");

								lineText = "165635" + TAB + SPACE + TAB
										+ SPACE + TAB + user.getUserId() + TAB
										+ SPACE + TAB + SPACE + TAB + roleName
										+ TAB + roleDesc + TAB + adminUser
										+ TAB + SPACE + TAB + SPACE + TAB
										+ SPACE + TAB + SPACE + TAB + SPACE
										+ TAB + FuncIndCode + TAB + AdminIndCode + TAB
										+ SPACE;

							

							} 
							// FOR ISA Role
							else if ((roleId.equalsIgnoreCase("ISA_Role"))
									&& (userId.toLowerCase())
											.matches("[a-z]{2}[0-9]{5}")) {

								LOG.info("ISA_Role");
								
								LOG.info("roleName" + roleName
										+ ", roleDesc = " + roleDesc);

								lineText = "165635" + TAB + SPACE + TAB
										+ SPACE + TAB + user.getUserId() + TAB
										+ SPACE + TAB + SPACE + TAB + roleName
										+ TAB + roleDesc + TAB
										+ user.getUserId() + TAB + SPACE + TAB
										+ SPACE + TAB + SPACE + TAB + SPACE
										+ TAB + SPACE + TAB + SPACE + TAB
										+ ISARole + TAB + SPACE;
								

							}
							// FOR SOEIDs
							else if ((userId.toLowerCase())
									.matches("[a-z]{2}[0-9]{5}") && !roleId.equalsIgnoreCase(propertyHelper
											.getProp(EBX_DIRECTORY_ADMIN_ROLE_ID))) {

								LOG.info("roleName" + roleName
										+ ", roleDesc = " + roleDesc);
								lineText = "165635" + TAB + SPACE + TAB
								+ SPACE + TAB + user.getUserId() + TAB
								+ SPACE + TAB + SPACE + TAB + roleName
								+ TAB + roleDesc + TAB
								+ user.getUserId() + TAB + SPACE + TAB
								+ SPACE + TAB + SPACE + TAB + SPACE
								+ TAB + SPACE + TAB + SPACE + TAB
								+ SPACE + TAB + SPACE;
								
								// if soeID is in administrator role set Flag 
/*								if(roleId.equalsIgnoreCase(propertyHelper
								.getProp(EBX_DIRECTORY_ADMIN_ROLE_ID))){
									
									LOG.info("writing line for " + user.getUserId() + " who is an " + roleId);
									
									lineText = "165635" + TAB + SPACE + TAB
									+ SPACE + TAB + user.getUserId() + TAB
									+ SPACE + TAB + SPACE + TAB + roleName
									+ TAB + roleDesc + TAB	+ user.getUserId() 
									+ TAB + SPACE + TAB + SPACE + TAB 
									+ SPACE + TAB + SPACE + TAB + SPACE 
									+ TAB + SPACE + TAB + AdminIndCode + TAB
									+ SPACE;
									LOG.info(lineText);
								}*/
								
							}else{
								
								LOG.info(user.getUserId()  + " is neither functional id, nor SOE or ISA id.. ");
							}
							if(null!= lineText){
							writer.write(lineText);
							writer.newLine();
							}
						}

					}
				}
			}
			writer.close();
			LOG.info("DONE WRITING");

		} catch (IOException e) {
			LOG.error(e.getStackTrace().toString());

		}
	}

	private static final String ISARole = "ISA";
	private static final String FuncIndCode = "F1";
	private static final String AdminIndCode = "PA1";
	private static final String UNDERSCORE = "_";
	private static final String COLON = ":";
	private static final String SPACE = " ";
	private static final String TAB = "\t";
}