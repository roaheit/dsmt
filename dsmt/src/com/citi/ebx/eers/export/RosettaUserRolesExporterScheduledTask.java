package com.citi.ebx.eers.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Profile;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryDefault;
import com.orchestranetworks.service.directory.DirectoryHandler;
import com.orchestranetworks.service.directory.ProfileListContextBridge;

public class RosettaUserRolesExporterScheduledTask extends ScheduledTask {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();

	private static final ResourceBundle bundle = ResourceBundle.getBundle("com.citi.ebx.dsmt.util.RosettaResources");
	BufferedWriter writer = null;
	private static final String DOT = ".";
	

	DirectoryDefault dirDef;

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public void execute(ScheduledExecutionContext context)
			throws OperationException, ScheduledTaskInterruption {
		try {

			final Repository repo = context.getRepository();
			final Session session = context.getSession();

			DirectoryHandler dir = session.getDirectory();
			dirDef = DirectoryDefault.getInstance(repo);
			List<UserReference> users = new ArrayList<UserReference>();
			List<Role> roles = new ArrayList<Role>();
			HashMap< String, String> rolsD = new HashMap<String, String>();
			
			final String env = propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
			
			File input = new File(bundle.getString(env + DOT + "rosetta.eers.roles.desc.path"));

			
			Scanner sc = new Scanner(input);
			
			HashMap< String, String> map = new HashMap<String, String>();
			
			while (sc.hasNextLine()) {
				String s = sc.nextLine();
			String[] roleDes = s.split("=");
				for(String r : roleDes){
					map.put(roleDes[0], roleDes[1]);
				}
			}

			Locale loc = Locale.getDefault();

			List<Profile> profi = dir.getProfiles(ProfileListContextBridge
					.getForOwningAdaptation());

			for (Profile prof : ((List<Profile>) dir
					.getProfiles(ProfileListContextBridge
							.getForOwningAdaptation()))) {
				if (prof.isUserReference()) {
					users.add((UserReference) prof);
				} else if ((!prof.isBuiltIn() && prof.isSpecificRole())
						|| (prof.isBuiltInAdministrator())) {
					roles.add((Role) prof);
				}
			}

			LOG.info("WRITING to FILE");

			String rosettaFilePath = propertyHelper
					.getProp("rosetta.eers.file.path");
			LOG.info("Rosetta EERS feed will be created at " + rosettaFilePath);
			writer = new BufferedWriter(new FileWriter(rosettaFilePath));
	

			for (UserReference user : users) {
				for (Role role : roles) {
					if(role.getRoleName().startsWith("Rosetta")){
					
					if (dir.isUserInRole(user, role)) {

						String roleId = role.getRoleName();
						String userId = user.getUserId();
	
						
						rolesDescriptions = map.get(roleId);


						if (null != roleId) {

							if (roleId.contains(COLON)) {
								roleId = roleId.replace(COLON, UNDERSCORE);

							}	
					
						String lineText = null;
						

							
						 if ((null != rolesDescriptions) && (rolesDescriptions.length() != 0)
									&& (userId.toLowerCase())
											.matches("[a-z]{2}[0-9]{5}")) {
							 
							 LOG.info("roleName" + roleId
										+ ", roleDesc = " + rolesDescriptions);
							 
							 LOG.info("userID : "+user.getUserId() + " env :"+env);
							 
							 
							 if ((roleId.equalsIgnoreCase("Rosetta_ISA"))
										&& (userId.toLowerCase())
												.matches("[a-z]{2}[0-9]{5}")) {

									LOG.info("ISA_Role");
									
									
									lineText = "165232" + TAB + SPACE + TAB
											+ SPACE + TAB + user.getUserId() + TAB
											+ SPACE + TAB + SPACE + TAB + roleId
											+ TAB + rolesDescriptions + TAB
											+ user.getUserId() + TAB + SPACE + TAB
											+ SPACE + TAB + SPACE + TAB + SPACE
											+ TAB + SPACE + TAB + SPACE + TAB
											+ ISARole + TAB + env.toUpperCase();
									

								}else if ((userId.toLowerCase())
									.matches("[a-z]{2}[0-9]{5}")) {
								
								
								

								lineText = "165232" + TAB + SPACE + TAB
										+ SPACE + TAB + user.getUserId() + TAB
										+ SPACE + TAB + SPACE + TAB + roleId + TAB
										+ rolesDescriptions + TAB + user.getUserId() + TAB + SPACE + TAB
										+ SPACE + TAB + SPACE + TAB + SPACE
										+ TAB + SPACE + TAB + SPACE + TAB + SPACE
										+ TAB + env.toUpperCase();
								}

								else{
								LOG.info(user.getUserId()  + " is neither functional id, nor SOE or ISA id.. ");
							}
								writer.write(lineText);
								writer.newLine();
							}
						 else{
							 LOG.info("Role Description is not updated for roleID :"+roleId);
						 }	
	
				}
						
				}
			}
				
			}
			}
			writer.close();
			LOG.info("DONE WRITING");

		} catch (IOException e) {
			LOG.info("Exception :"+e.getMessage());
			System.err.println(e);

		}
	}

	private static final String UNDERSCORE = "_";
	private static final String COLON = ":";
	private static final String SPACE = " ";
	private static final String TAB = "\t";
	private String rolesDescriptions;
	private static final String ISARole = "ISA";
}