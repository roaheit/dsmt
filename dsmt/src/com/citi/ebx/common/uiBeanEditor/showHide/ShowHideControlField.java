package com.citi.ebx.common.uiBeanEditor.showHide;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.ui.UIBeanEditor;
import com.orchestranetworks.ui.UIResponseContext;
import com.orchestranetworks.ui.base.JsFunctionCall;
import com.orchestranetworks.ui.form.widget.UIComboBox;

/**
 * <p><b>ShowHideControlField:</b></br> When attached to an enumerated field will control the visibility
 * of other fields on the page.</p>
 * <p>It scans all of the other fields in the control fields parent table looking for an
 * Information tag that contains "HIDE-WHEN:", it then uses the "field-name=value[,value]*" to determine the 
 * conditions when the other fields are visible or hidden.</br>
 * </p>
 * <p><b>Known Limitations</b></br></br>
 * <b>This class must be re-tested for each new version of EBX in case the UI HTML "id" composition changes.</b></br>
 * This class must be attached to a <i>String Enumerated</i> field only.</br>
 * The field names for controlling fields must be unique in the table.</br>
 * The control field cannot implement any other UIBeanEditor components.</br>
 * The controlled fields cannot implement and other advanced features that require the Information 
 * tag to be populated, as it can currently only hold a single value.</p>
 * <p> <small>Based on the component <i>ComplexShowHideFieldsUIBean</i> written by Bojan K.</small></p>
 * @author Craig Cox	-	Orchestra Networks	March 2014
 * @author Steve Higgins - Orchestra Networks - Delegated javascript generation to ShowHideGenerator - May 2015
 * @version 2.0.0
 */
public class ShowHideControlField extends UIBeanEditor {

	/** Component parameter: Should hidden fields be blanked? */
	private boolean clearFields = true;
	
	/**
	 * <p>Compose the form page content for read only mode.</p>
	 * <p>All of the fields are published to the page setting the appropriate visibility
	 * of the controlled fields according to the value contained in the controlling field.</p> 
	 * @see com.orchestranetworks.ui.UIBeanEditor#addForDisplayInTable(com.orchestranetworks.ui.UIResponseContext)
	 */
	@Override
	public void addForDisplayInTable(UIResponseContext ctx) {
		ctx.addUIBestMatchingComponent(Path.SELF, null);
	}
	
	/**
	 * <p>Compose the form page content for read only mode.</p>
	 * <p>All of the fields are published to the page setting the appropriate visibility
	 * of the controlled fields according to the value contained in the controlling field.</p> 
	 * @see com.orchestranetworks.ui.UIBeanEditor#addForDisplay(com.orchestranetworks.ui.UIResponseContext)
	 */
	@Override
	public void addForDisplay(UIResponseContext ctx) {

		// FIXME ... If the control field is read-only then EBX doesn't produce the 
		//           required FormNodeIndex structures for the Javascript to work.
		//           Need to somehow pass the node value directly into the JS so that 
		//           it doesn't try to find the on-page node in FormNodeIndex.
		//           Also need to investigate the knock-on effects when cascading
		//           show/hide is in use.
		//String fieldValue = ctx.getValue().toString();
		
		// Render the default component
		ctx.addUIBestMatchingComponent(Path.SELF, null);

		//* but add javascript to set the controlled fields
		String jsListenerFnName = ctx.getWebNameFor("_ChangeListener");
		ShowHideGenerator generator = new ShowHideGenerator(ctx.getNode(), jsListenerFnName, isClearFields());
		generator.addJavascript(ctx);
		
	}

	/**
	 * <p>Compose the form page content for edit mode.</p>
	 * <p>All of the fields are published to the page setting the appropriate visibility
	 * of the controlled fields according to the value contained in the controlling field.</p> 
	 * @see com.orchestranetworks.ui.UIBeanEditor#addForEdit(com.orchestranetworks.ui.UIResponseContext)
	 */
	@Override
	public void addForEdit(UIResponseContext ctx) {

		//* Set the JavaScript listener function name
		String jsListenerFnName = ctx.getWebNameFor("_ChangeListener");

		//* Render the normal UI control but attach the listener function
		UIComboBox widget = ctx.newComboBox(Path.SELF);
		widget.setActionOnAfterValueChanged(JsFunctionCall.on(jsListenerFnName));
		//if(ajaxParam != null){
			//widget.setAjaxPrevalidationEnabled(true);
		//}
		widget.setAjaxValueSynchEnabled(true);
		ctx.addWidget(widget);
		
		//* Generate the JavaScript for the control field listener function.
		ShowHideGenerator generator = new ShowHideGenerator(ctx.getNode(), jsListenerFnName, isClearFields());
		generator.addJavascript(ctx);

	}

	/**
	 * Return whether this component will clear hidden fields. 
	 * @return true if hidden fields will be persisted blank, false otherwise
	 */
	public boolean isClearFields() {
		return clearFields;
	}

	/**
	 * Set whether this component will clear hidden fields. 
	 * @param clearFields true if hidden fields should be persisted blank, false otherwise
	 */
	public void setClearFields(boolean clearFields) {
		this.clearFields = clearFields;
	}
	
}
