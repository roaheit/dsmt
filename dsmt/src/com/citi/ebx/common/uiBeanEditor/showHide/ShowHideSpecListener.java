// Generated from ShowHideSpec.g4 by ANTLR 4.5
package com.citi.ebx.common.uiBeanEditor.showHide;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ShowHideSpecParser}.
 */
public interface ShowHideSpecListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ShowHideSpecParser#showWhen}.
	 * @param ctx the parse tree
	 */
	void enterShowWhen(ShowHideSpecParser.ShowWhenContext ctx);
	/**
	 * Exit a parse tree produced by {@link ShowHideSpecParser#showWhen}.
	 * @param ctx the parse tree
	 */
	void exitShowWhen(ShowHideSpecParser.ShowWhenContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpleExpr}
	 * labeled alternative in {@link ShowHideSpecParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSimpleExpr(ShowHideSpecParser.SimpleExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpleExpr}
	 * labeled alternative in {@link ShowHideSpecParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSimpleExpr(ShowHideSpecParser.SimpleExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code logicalExpr}
	 * labeled alternative in {@link ShowHideSpecParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLogicalExpr(ShowHideSpecParser.LogicalExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code logicalExpr}
	 * labeled alternative in {@link ShowHideSpecParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLogicalExpr(ShowHideSpecParser.LogicalExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bracketedExpr}
	 * labeled alternative in {@link ShowHideSpecParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBracketedExpr(ShowHideSpecParser.BracketedExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bracketedExpr}
	 * labeled alternative in {@link ShowHideSpecParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBracketedExpr(ShowHideSpecParser.BracketedExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link ShowHideSpecParser#logicOp}.
	 * @param ctx the parse tree
	 */
	void enterLogicOp(ShowHideSpecParser.LogicOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link ShowHideSpecParser#logicOp}.
	 * @param ctx the parse tree
	 */
	void exitLogicOp(ShowHideSpecParser.LogicOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link ShowHideSpecParser#fieldSpec}.
	 * @param ctx the parse tree
	 */
	void enterFieldSpec(ShowHideSpecParser.FieldSpecContext ctx);
	/**
	 * Exit a parse tree produced by {@link ShowHideSpecParser#fieldSpec}.
	 * @param ctx the parse tree
	 */
	void exitFieldSpec(ShowHideSpecParser.FieldSpecContext ctx);
	/**
	 * Enter a parse tree produced by {@link ShowHideSpecParser#ctrlField}.
	 * @param ctx the parse tree
	 */
	void enterCtrlField(ShowHideSpecParser.CtrlFieldContext ctx);
	/**
	 * Exit a parse tree produced by {@link ShowHideSpecParser#ctrlField}.
	 * @param ctx the parse tree
	 */
	void exitCtrlField(ShowHideSpecParser.CtrlFieldContext ctx);
	/**
	 * Enter a parse tree produced by {@link ShowHideSpecParser#fkIndexes}.
	 * @param ctx the parse tree
	 */
	void enterFkIndexes(ShowHideSpecParser.FkIndexesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ShowHideSpecParser#fkIndexes}.
	 * @param ctx the parse tree
	 */
	void exitFkIndexes(ShowHideSpecParser.FkIndexesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ShowHideSpecParser#fkIndex}.
	 * @param ctx the parse tree
	 */
	void enterFkIndex(ShowHideSpecParser.FkIndexContext ctx);
	/**
	 * Exit a parse tree produced by {@link ShowHideSpecParser#fkIndex}.
	 * @param ctx the parse tree
	 */
	void exitFkIndex(ShowHideSpecParser.FkIndexContext ctx);
	/**
	 * Enter a parse tree produced by {@link ShowHideSpecParser#operator}.
	 * @param ctx the parse tree
	 */
	void enterOperator(ShowHideSpecParser.OperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link ShowHideSpecParser#operator}.
	 * @param ctx the parse tree
	 */
	void exitOperator(ShowHideSpecParser.OperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link ShowHideSpecParser#ctrlValues}.
	 * @param ctx the parse tree
	 */
	void enterCtrlValues(ShowHideSpecParser.CtrlValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ShowHideSpecParser#ctrlValues}.
	 * @param ctx the parse tree
	 */
	void exitCtrlValues(ShowHideSpecParser.CtrlValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ShowHideSpecParser#ctrlValue}.
	 * @param ctx the parse tree
	 */
	void enterCtrlValue(ShowHideSpecParser.CtrlValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link ShowHideSpecParser#ctrlValue}.
	 * @param ctx the parse tree
	 */
	void exitCtrlValue(ShowHideSpecParser.CtrlValueContext ctx);
}