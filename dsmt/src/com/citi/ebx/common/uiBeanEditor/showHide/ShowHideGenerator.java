package com.citi.ebx.common.uiBeanEditor.showHide;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import com.citi.ebx.common.uiBeanEditor.showHide.ShowHideSpecParser.BracketedExprContext;
import com.citi.ebx.common.uiBeanEditor.showHide.ShowHideSpecParser.CtrlFieldContext;
import com.citi.ebx.common.uiBeanEditor.showHide.ShowHideSpecParser.CtrlValueContext;
import com.citi.ebx.common.uiBeanEditor.showHide.ShowHideSpecParser.FieldSpecContext;
import com.citi.ebx.common.uiBeanEditor.showHide.ShowHideSpecParser.FkIndexContext;
import com.citi.ebx.common.uiBeanEditor.showHide.ShowHideSpecParser.LogicOpContext;
import com.citi.ebx.common.uiBeanEditor.showHide.ShowHideSpecParser.OperatorContext;
import com.citi.ebx.common.uiBeanEditor.showHide.ShowHideSpecParser.ShowWhenContext;
import com.citi.ebx.common.utilities.ParseErrorListener;
import com.citi.ebx.common.utilities.Tools;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.Request;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.ui.UIResponseContext;

/**
 * Generate all of the JavaScript required to implement show/hide functionality. Searches for show-when specification
 * strings across all fields in a table and parses the strings to produce javascript expressions that can be executed 
 * on the page. 
 * 
 * @author Steve Higgins - Orchestra Networks - May 2015
 * 
 */
public class ShowHideGenerator {

	/** Logger. */
//	private static final Category log = Tools.getCategory();
	
	/** SchemaNode of a show/hide control field. Changes to this field will trigger show/hide functionality. */
	private SchemaNode controlField = null;

	/** Name of the JavaScript function to generate. */ 
	private String showHideFnName = null;

	/** Flag indicating whether the function should implement logic that blanks hidden fields on Save. */ 
	private boolean clearFields = true;
	
	/** Parse listener that interprets show-when strings. */
	private SpecParseListener listener = new SpecParseListener();

//	private static final String AJAX_COMPONENT_ID = "ShowHideAjax";
	
	/** 
	 * Create a new javascript generator from a given Show/Hide control field
	 * @param controlField The show/hide control field
	 * @param showHideFnName The name that the generated function should be given
	 * @param clearFields Flag indicating whether the function should implement logic 
	 * that blanks hidden fields on Save
	 */
	public ShowHideGenerator(SchemaNode controlField, String showHideFnName, boolean clearFields) {
		this.controlField = controlField;
		this.showHideFnName = showHideFnName;
		this.clearFields = clearFields;
	}
	
	/**
	 * Generate javascript code to show/hide fields based on the specifications that
	 * have been extracted from the data model, and insert it into the given response context.
	 * @param ctx A response context provided by EBX
	 */
	public void addJavascript(UIResponseContext ctx) {
		
		Session session = ctx.getSession();
		
		// Scan all nodes in the current table, looking for show-when specifications
		findTargetNodes(controlField.getTableNode().getTableOccurrenceRootNode(), ctx);

		// Add CSS to the page
		Tools.includeStylesheetFile(ctx, "ShowHideField.css");
		
		// Start the page refresh function
		ctx.addJS_cr(String.format("function %s() { ", showHideFnName));
		
		// Add a javascript breakpoint if debug logging is enabled
//		if (log.isDebugEnabled()) {
			ctx.addJS_cr("debugger; ");
//		}
		
		// Repeating groups ... note that a control field that's located in a repeating group will have many instances.
		// The path to each will include an index - ie, rg[0]/ctrlfld
			
			
		// Define and populate the control-field value object. Each one has a 'visible' and
		// 'value' property which will be used in the generated logic for each target field
		ctx.addJS_cr("");
		
	/*	// Ajax call to save value on server side
		// Define a Javascript handler object for Ajax progress responses
		ctx.addJS_cr("ImportAjaxHandler = function() {");
        
        // A successful Ajax request causes results from the service to be added to the page
		ctx.addJS_cr(" this.handleAjaxResponseSuccess = function(responseContent) {");
		ctx.addJS_cr(" };");
        
        // A failed Ajax request causes an error message to be added to the page 
		ctx.addJS_cr(" this.handleAjaxResponseFailed = function(responseContent) {");
		ctx.addJS_cr(" };");
                
        // Finalise the Ajax handlerFormNodeIndex
		ctx.addJS_cr("};");

        // Add full Ajax behaviours to our handler object
		ctx.addJS_cr("ImportAjaxHandler.prototype = new EBX_AJAXResponseHandler();");
		
		// Function to call the Ajax backend service to get the current import status
		ctx.addJS_cr("function setSessionVar(sessionVar) { ");
		ctx.addJS_cr("    var ajax = new ImportAjaxHandler(); ");
		ctx.addJS_cr("    var requestParameter = \"&parameter=\"+sessionVar");
		ctx.addJS   ("    ajax.sendRequest(\"").addJS(ctx.getURLForAjaxComponent(AJAX_COMPONENT_ID)).addJS_cr("\"+requestParameter); ");
		ctx.addJS_cr("} ");
	*/
		ctx.addJS_cr("  // Get the visibility status and value of each control-field ");
		Map<SchemaNode, String> ctrlFieldMap = listener.getCtrlFieldMap();
		for (SchemaNode ctrlFieldNode : ctrlFieldMap.keySet()) {
			String path = ctrlFieldNode.getPathInAdaptation().format();
			String varName = ctrlFieldMap.get(ctrlFieldNode); 
			ctx.addJS_cr(String.format("  var %s_node = EBX_FormNodeIndex.getFormNode(\"%s\");  // %s", varName, path, ctrlFieldNode.getLabel(session)));
			ctx.addJS_cr(String.format("  var %s = { visible : isVisible(%s_node), value : %s_node.getValue() }; ", 
 					varName, varName, varName));
			/*if(getField != null && getField.contains("[") ? path.contains(getField.substring(0, getField.length()-3)) : path.contains(getField)){			
				if(getField.contains("[")){
					String index = getField.substring(getField.length()-2, getField.length()-1);
					ctx.addJS_cr(String.format(" setSessionVar(normalise(%s_node.getValue(), \"%s\")); ", varName, index));
				}
				else{
					ctx.addJS_cr(String.format(" setSessionVar(%s_node.getValue()); ", varName));
				}
			}*/
 			// NOTE: Should really use 'ctx.addJS_getNodeValue(Path)' but it expects relative path, which would have to be calculated :-(
 			//       Besides, we're already sailing close to the wind by using 'FormNodeIndex.getFormNode(path)'
		}

		// Show the generated JS expressions for any target nodes that are also control-field nodes
		Map<SchemaNode, String> exprMap = listener.getExprMap();
		for (SchemaNode targetNode : exprMap.keySet()) {
			if (ctrlFieldMap.containsKey(targetNode)) {
				ctx.addJS_cr("");
				ctx.addJS_cr(String.format("  // Control/Target field : %s (%s)", targetNode.getPathInAdaptation().format(), targetNode.getLabel(session)));
				ctx.addJS_cr(String.format("  //   %s ", Tools.getNodeInfo(targetNode)));
				String expr = exprMap.get(targetNode);
				
				if(expr != null && expr.contains("null")){
					expr = expr.replace("\"null\"", "null");
				}
				// lookup table
				if(expr.contains("path:")){
					lookupTable(ctx, expr, targetNode);
				}
				else{				
					ctx.addJS_cr(String.format("  if (%s) { ", expr) );
				}
				
				ctx.addJS_cr(String.format("    showField(\"%s\"); ", targetNode.getPathInAdaptation().format()));
				ctx.addJS_cr(String.format("    %s.visible = true; ", ctrlFieldMap.get(targetNode)));
				ctx.addJS_cr("  } else { ");
				ctx.addJS_cr(String.format("    hideField(\"%s\"); ", targetNode.getPathInAdaptation().format()));
				ctx.addJS_cr(String.format("    %s.visible = false; ", ctrlFieldMap.get(targetNode)));
				ctx.addJS_cr("  } ");
			}
		}

		// Show the generated JS expressions for each target node
		for (SchemaNode targetNode : exprMap.keySet()) {
			if (!ctrlFieldMap.containsKey(targetNode)) {
				ctx.addJS_cr("");
				ctx.addJS_cr(String.format("  // Target field : %s (%s)", targetNode.getPathInAdaptation().format(), targetNode.getLabel(session)));
				ctx.addJS_cr(String.format("  //   %s ", Tools.getNodeInfo(targetNode)));
				
				String expr = exprMap.get(targetNode);
				if(expr != null && expr.contains("null")){
					expr = expr.replace("\"null\"", "null");
				}
				// lookup table
				if(expr.contains("path:")){
					lookupTable(ctx, expr, targetNode);
				}
				else{				
					ctx.addJS_cr(String.format("  if (%s) { ", expr) );
				}
				ctx.addJS_cr(String.format("    showField(\"%s\"); ", targetNode.getPathInAdaptation().format()));
				ctx.addJS_cr("  } else { ");
				ctx.addJS_cr(String.format("    hideField(\"%s\"); ", targetNode.getPathInAdaptation().format()));
				ctx.addJS_cr("  } ");
			}
		}

		// Add a method to normalise the control-field value
		ctx.addJS_cr("");
		ctx.addJS_cr("  // Convert a control-field value into something that can be compared ");
		ctx.addJS_cr("  function normalise(value, indexes) { ");
		ctx.addJS_cr("    if (value === null) { return null; } ");
		ctx.addJS_cr("    if (typeof value === 'object') { value = value.key; } ");
		ctx.addJS_cr("    if (arguments.length === 1 || indexes.length === 0) { return value; } ");
		ctx.addJS_cr("    var newValue = ''; ");
		ctx.addJS_cr("    var parts = value.split('|'); ");
		ctx.addJS_cr("    for (var i = 0; i < indexes.length; i++) { ");
		ctx.addJS_cr("      var offset = indexes[i] - 1; ");
		ctx.addJS_cr("      newValue += parts[offset] + '|'; ");
		ctx.addJS_cr("    } ");
		ctx.addJS_cr("    return newValue.slice(0,-1); ");
		ctx.addJS_cr("  } ");
		
		// Add a method to determine whether the control-field value is in a list
		ctx.addJS_cr("");
		ctx.addJS_cr("  // Return true if a value is in a list, false otherwise. ");
		ctx.addJS_cr("  function inList(value, list) { ");
		ctx.addJS_cr("    return value === null ? false : list.indexOf(value) >= 0; ");
		ctx.addJS_cr("  } ");
		
		// Add a method to make a field (table row) visible
		ctx.addJS_cr("");
		ctx.addJS_cr("  // Show a field. ");
		ctx.addJS_cr("  function showField(fieldName) { ");
		ctx.addJS_cr("    var node = EBX_FormNodeIndex.getFormNode(fieldName); ");
		ctx.addJS_cr("    if (node == null) return; ");
		ctx.addJS_cr("    var row = getTableRow(node); ");
		ctx.addJS_cr("    if (row != null && row.hidden) { ");
		if (clearFields) {
			ctx.addJS_cr("      node.setValue(row.showHideStash); ");
		}
		ctx.addJS_cr("      row.hidden = false; ");
		ctx.addJS_cr("    } ");
		ctx.addJS_cr("  } ");

		// Add a method to make a field (table row) hidden
		ctx.addJS_cr("");
		ctx.addJS_cr("  // Hide a field. ");
		ctx.addJS_cr("  function hideField(fieldName) { ");
		ctx.addJS_cr("    var node = EBX_FormNodeIndex.getFormNode(fieldName); ");
		ctx.addJS_cr("    if (node == null) return; ");
		ctx.addJS_cr("    var row = getTableRow(node); ");
		ctx.addJS_cr("    if (row != null && !row.hidden) { ");
		ctx.addJS_cr("      row.hidden = true; ");
		if (clearFields) {
			ctx.addJS_cr("      row.showHideStash = node.getValue(); ");
			ctx.addJS_cr("      if(typeof node.getValue() == 'string'){ ");
			ctx.addJS_cr("      node.setValue(''); ");		// added for string type to work in IE11
			ctx.addJS_cr("      } else { ");
			ctx.addJS_cr("      node.setValue(null); ");		
			ctx.addJS_cr("      }; ");
		}
		ctx.addJS_cr("    } ");
		ctx.addJS_cr("  } ");

		// Add a method to return the table row containing the target field
		ctx.addJS_cr("");
		ctx.addJS_cr("  // Retrieve the table row that contains a given field ");
		ctx.addJS_cr("  function getTableRow(node) { ");
		ctx.addJS_cr("    var tableRow; ");
		ctx.addJS_cr("    if (typeof node.getTRElement === 'function') { ");
		ctx.addJS_cr("      tableRow = node.getTRElement(); ");
		ctx.addJS_cr("    } else { ");
		ctx.addJS_cr("      var anchor = node.getDecoratorElement(); ");
		ctx.addJS_cr("      tableRow = YAHOO.util.Dom.getAncestorByTagName(anchor, \"TR\"); ");
		ctx.addJS_cr("    } ");
		ctx.addJS_cr("    return tableRow; ");
		ctx.addJS_cr("  } ");

		// Add a method to determine the current visibility of the target field
		ctx.addJS_cr("");
		ctx.addJS_cr("  // Derive the current visibility of a given field ");
		ctx.addJS_cr("  function isVisible(node) { ");
		ctx.addJS_cr("    var tableRow = getTableRow(node); ");
		ctx.addJS_cr("    return tableRow == null ? true : !tableRow.hidden; ");
		ctx.addJS_cr("  } ");

		// Terminate the object
		ctx.addJS_cr("");
		ctx.addJS_cr("}; ");
		
		// Add function call to initialise the target fields 
		ctx.addJS_cr(String.format("%s(); ", showHideFnName));
		
	}

	private void lookupTable(UIResponseContext ctx, String expr, SchemaNode targetNode){
		System.out.println("in method");
		String [] exprVar1 = expr.split("\"");
		String [] exprVar = exprVar1[1].split(":");
		AdaptationTable table = Tools.getTable(ctx, Path.parse(exprVar[1]));
		Request request = table.createRequest();
		System.out.println("targetNode: " + ctx.getValue());
		request.setXPathFilter(String.format("./"+exprVar[2]+"='%s'", ctx.getValue()));
		RequestResult resultSet = request.execute();
		Adaptation record = null;
		String result = null;
		int idx = 0;
		StringBuffer exprStr = new StringBuffer();
		if(resultSet != null){
			if(resultSet.getSize() > 1){
				String [] finalVar = null;
				if(expr.contains("!=")){
					finalVar = expr.split("!=");
				}
				else{
					finalVar = expr.split("=");
				}
				String [] listVar = finalVar[0].split("&&");
				
				record = resultSet.nextAdaptation();
				while (record != null){
					result = record.getString(Path.parse(exprVar[3]));
					if(idx == 0){		
						exprStr.append(listVar[0]).append(" && ").append("inList(").append(listVar[1].trim()).append(",[\"").append(result).append("\"");
					}
					else{
						exprStr.append(",\"").append(result).append("\"");
					}
					idx++;
					record = resultSet.nextAdaptation();
				}
				if(expr.contains("!=")){
					exprStr.append("])===false)");
				}
				else{
					exprStr.append("])===true)");
				}
			}
			else{
				record = resultSet.nextAdaptation();
				if(record != null){
					result = record.getString(Path.parse(exprVar[3]));
				}
				
				System.out.println("result: " + result);
				String [] finalVar = null;
				if(expr.contains("!=")){
					finalVar = expr.split("!=");
				}
				else{
					finalVar = expr.split("=");
				}
				if(expr.contains("!=")){
					exprStr.append(finalVar[0]).append("!==\"").append(result).append("\")");
				}
				else{
					exprStr.append(finalVar[0]).append("===\"").append(result).append("\")");
				}
				System.out.println("exprStr: " + exprStr);
			}
		}
		
		ctx.addJS_cr(String.format("  if (%s) { ", exprStr) );
	}
	
	/**
	 * Recursively scan each field in the current table looking for target fields - fields with a "show" 
	 * specification in their Information attribute. 
	 * @param node Node to scan.
	 */

	private void findTargetNodes(SchemaNode node, UIResponseContext ctx) {
		
		SchemaNode [] nodeChildren = node.getNodeChildren();
		for (SchemaNode child : nodeChildren){

			// TODO: Do we still need this test? What if there are 2 or more control fields?
			Path path = Path.SELF.add(child.getPathInAdaptation().getLastStep());     // Why only compare the last step?
			if (!path.equals(controlField.getPathInAdaptation().getLastStep())) {
				
				if (child.getNodeChildren() != null){
					findTargetNodes(child, ctx); 		// Recursive
				}

				parseInfo(child, ctx);

			} 
			  else {
				System.out.println("===> Ignored self : " + controlField.getPathInAdaptation().format());
			}

		}

	}

	/**
	 * 
	 * 
	 * 
	 * @param node The node to process
	 * @param buffer 
	 */
	private void parseInfo(SchemaNode node, UIResponseContext ctx) {
		
		// Nothing to do if we don't have any info, or it doesn't start "show-"
		String info = Tools.getNodeInfo(node);
		if (info == null || !info.toLowerCase().startsWith("show-")) {
			return;
		}

	    // Line everything up to pass the show- string through the lexer
	    ParseErrorListener errorListener = new ParseErrorListener(node, info);
	    ANTLRInputStream input = new ANTLRInputStream(info);
	    ShowHideSpecLexer lexer = new ShowHideSpecLexer(input);
	    lexer.removeErrorListeners();
	    lexer.addErrorListener(errorListener);
	    
	    // Connect the lexer to the parser
	    CommonTokenStream tokens = new CommonTokenStream(lexer);
	    ShowHideSpecParser parser = new ShowHideSpecParser(tokens);
	    parser.removeErrorListeners();
	    parser.addErrorListener(errorListener);
	    
	    // Perform the parse. Show an error report if anything went wrong.
	    ShowWhenContext parseTree = parser.showWhen();
	    if (errorListener.isInError()) {
	    	ctx.add(errorListener.getErrorReport("ParseSyntaxError"));
	    	return;
	    }
	    
        // Traverse the parse tree to extract enough info to build the javascript
	    ParseTreeWalker walker = new ParseTreeWalker();
	    listener.setTargetNode(node);
	    try {
	    	walker.walk(listener, parseTree);
	    } catch (Exception e) {
	    	ctx.add(String.format("<p class=\"ParseSyntaxError\">%s</p>", e.getMessage()));
	    }

	    // Note: The listener will collect information extracted from 
	    //       EVERY show-when string discovered. That's why I only 
	    //       instantiate one listener.
	    
    }


	/**
	 * Inner parse listener class to interpret output from the parser.
	 *
	 */
	private class SpecParseListener extends ShowHideSpecBaseListener {

		/** The target node that will be dynamically shown or hidden. */
		private SchemaNode targetNode = null;	

		/** A reference to the control field node from a single field spec. */
		private SchemaNode ctrlFieldNode = null;
		
		/** A list of indexes for extracting parts of a foreign-key value. */
		private List<String> fkIndexList = new ArrayList<String>();
		
		/** The <b>=</b> or <b>!=</b> operator. */
		private String operator = null;
		
		/** A list of control-field values. */
		private List<String> ctrlValueList = new ArrayList<String>();

		/** Buffer for constructing a javascript expression. */
		private StringBuilder expr = null;

		/** Incrememting number for building javascript variable names. */
		private int cfIndex = 1;

		/** Map of control-field nodes to their javascript variables. */
		private Map<SchemaNode,String> ctrlFieldMap = new LinkedHashMap<SchemaNode,String>();
		
		/** Map of target-field nodes to their complete show-when expressions. */
		private Map<SchemaNode,String> exprMap = new LinkedHashMap<SchemaNode,String>();

		
		/**
		 * Store the target node.
		 * @param node
		 */
		public void setTargetNode(SchemaNode node) {
			this.targetNode = node;
		}
		
		/**
		 * At the shart of a "show-when" string, reset the javascript expression buffer.
		 */
		@Override
		public void enterShowWhen(ShowWhenContext ctx) {
			expr = new StringBuilder();
		}

		/**
		 * At the end of a "show-when" string, save the javascript expression for later retrieval. 
		 */
		@Override
		public void exitShowWhen(ShowWhenContext ctx) {
			exprMap.put(targetNode, expr.toString());
		}
		
		/**
		 * At the start of a single field-spec reset the lists of foreign-key indexes and control-field 
		 * values. These will be re-populated as we process the rest of the field-spec.
		 */
		@Override
		public void enterFieldSpec(FieldSpecContext ctx) {
			fkIndexList.clear();
			ctrlValueList.clear();
		}
		
		/**
		 * We've encountered a control-field path. Check that its valid within the current table and 
		 * if it is then store it. If it's not then arrange for a message to be displayed. 
		 */
		@Override
		public void enterCtrlField(CtrlFieldContext ctx) {
			
			// Try the path exactly as entered by the user
			Path path = Path.parse(ctx.getText());
			SchemaNode node = targetNode.getNode(path, true, false);
			if (node != null) {
				ctrlFieldNode = node; 
			} else {
				// It didn't exists so try one step up ... it's a common mistake
				node = targetNode.getNode(Path.PARENT.add(path), true, false);
				if (node != null) {
					ctrlFieldNode = node; 
				} else {
					node = targetNode.getNode(Path.PARENT.add(Path.PARENT.add(path)), true, false);
					if (node != null) {
						ctrlFieldNode = node; 
					} else {
						throw new PathAccessException(String.format("Show/Hide error on field <b>%s</b> : Control field <b>%s</b> not found in this table.", 
								targetNode.getLabel(Locale.getDefault()), ctx.getText()));
					}
				}
			}

		}
		
		/**
		 * We've encountered a foreign-key index number. Add it to the list.
		 */
		@Override
		public void enterFkIndex(FkIndexContext ctx) {
			fkIndexList.add(ctx.getText());
		}

		/**
		 * We've encountered the operator - "=", "!=" or "<>". Store it.
		 */
		@Override
		public void enterOperator(OperatorContext ctx) {
			operator = ctx.getText();
		}	

		/**
		 * We've encountered a control-field value. Store it.
		 */
		@Override
		public void enterCtrlValue(CtrlValueContext ctx) {
			ctrlValueList.add(ctx.getText());
		}

		/**
		 * We've finished parsing an individual field-spec so we now have enough information
		 * to build a javascript expression from it. The general format of the expression is:
		 * <code>inList(normalise(<i>varName</i>[,<i>fkIndexList</i>]),<i>valueList</i>) (=== true | === false)</code>
		 * <p>Where:
		 * <ul><li><code>inList(value,list)</code> is a generated javascript function that tests whether a value exists in a list. 
		 * Returns <b>true</b> or <b>false</b>.</li>
		 * <li><code>normalise(value[,list])</code> is a generated javascript function that converts a value into a form that can be 
		 * compared successfully. It takes care of extracting the <i>key</i> value from a foreign-key value object, and optionally 
		 * constructing a value from parts of an EBX foreign-key string using the supplied list of indexes.</li> 
		 * <li><code>varName</code> is a javascript variable that will be populated with a control-field value.</li> 
		 * <li><code>fkIndexList</code> is an optional list of index values that control how a foreign-key value is transformed. For
		 * example, if the control-field value is <code>ABC|DEF|XYZ</code> and the list is <code>[3,1]</code> then the final value 
		 * returned by <code>normalise()</code> will be <code>XYZ|ABC</code></li> 
		 * <li><code>valueList</code> is the list of values that the normalised control-field value should be tested against</li> 
		 * <li>The result of <code>inList(...)</code> is tested against <code>true</code> or <code>false</code> depending on the 
		 * field-spec operator. If it's <code>=</code> then we're interested in whether the value is in the list. if it's <code>!=</code>
		 * (or <code><></code>) then we need to know whether the value isn't in the list.</li> 
		 * </ul>
		 * <p>An expression built here will be integrated into a larger <code>if (<i>expression</i>) then <i>show-field</i> else 
		 * <i>hide-field</i></code> statement that may include many expressions built by this method.</p> 
		 */
		@Override
		public void exitFieldSpec(FieldSpecContext ctx) {
			
			// Invent a variable name for the raw control-field value and store it in the map
			String cfVarName = ctrlFieldMap.get(ctrlFieldNode);
			if (cfVarName == null || cfVarName.isEmpty()) {
				cfVarName =	String.format("cf%03d", cfIndex++);
				ctrlFieldMap.put(ctrlFieldNode, cfVarName);
			}
			
			// Start the expression with a check on whether the control field is visible or not
			// If a control field is hidden then all target fields that depend on it may also be hidden
			expr.append("(").append(cfVarName).append(".visible && ");

			// Test the number of control values that were extracted
			if (ctrlValueList.size() == 1) {
				
				// If there's only one value to test we can simplify the expression
				// to "normalise(varName[,fkIndexList]) ===/!== valueList(0)"
				
				expr.append("normalise(").append(cfVarName).append(".value");	

				if (!fkIndexList.isEmpty()) {
					expr.append(",");
					appendList(fkIndexList);
				}
				
				expr.append(")");

				// The final test depends on the operator
				if (operator.equals("=")) {
					expr.append("===").append(ctrlValueList.get(0));
				} else {
					expr.append("!==").append(ctrlValueList.get(0));
				}
			
			} else {

				// Otherwise we need to test whether the control-field value 
				// is or isn't in the value list
				
				expr.append("inList(normalise(").append(cfVarName).append(".value");	
	
				// Add the foreign-key index list if there is one
				if (!fkIndexList.isEmpty()) {
					expr.append(",");
					appendList(fkIndexList);
				}
				
				expr.append("),");
				appendList(ctrlValueList);
				expr.append(")");
	
				// The final test depends on the operator
				if (operator.equals("=")) {
					expr.append("===true");
				} else {
					expr.append("===false");
				}

			}

			expr.append(")");			

		}
		
		/** 
		 * Add a list of strings to the current expression, in the
		 * form of a Javascript list object.
		 * @param list The list of strings to add
		 */
		private void appendList(List<String> list) {
			expr.append("[");
			String separator = "";
			for (String entry : list) {
				expr.append(separator).append(entry);
				separator = ",";
			}
			expr.append("]");
		}

		/**
		 * We've encountered a logical operator (and/or) that separates individual
		 * field-spec expressions. Add the appropriate javascript and/or clause.
		 */
		@Override
		public void enterLogicOp(LogicOpContext ctx) {
			if (ctx.op.getType() == ShowHideSpecParser.LOGICAL_AND) {
				expr.append("\n\t && ");
			} else {
				expr.append("\n\t || ");
			}
		}
		
		/**
		 * We've encountered the start of a bracketed expression so add
		 * an opening bracket to the javascript expression.
		 */
		@Override
		public void enterBracketedExpr(BracketedExprContext ctx) {
			expr.append("(");
		}

		/**
		 * We've encountered the end of a bracketed expression so add
		 * a closing bracket to the javascript expression.
		 */
		@Override
		public void exitBracketedExpr(BracketedExprContext ctx) {
			expr.append(")");
		}
		
		/**
		 * Make the control-field/variable-name map available.
		 * @return The control-field map
		 */
		public Map<SchemaNode, String> getCtrlFieldMap() {
			return ctrlFieldMap;
		}

		/**
		 * Make the target-field/expression map available.
		 * @return The expression map
		 */
		public Map<SchemaNode, String> getExprMap() {
			return exprMap;
		}

	}
	
}
