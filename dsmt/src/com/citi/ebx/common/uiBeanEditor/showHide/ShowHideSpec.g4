grammar ShowHideSpec;

//
// Antlr grammar file for the ShowHide specification string
//
// Author: Steve Higgins - Orchestra Networks - March 2014 
//

// === Parser Rules ===
showWhen   : SHOW_WHEN ':'? expression
           | SHOW_IF ':'? expression
           ; 
expression : expression logicOp expression			# logicalExpr
           | '(' expression ')' 					# bracketedExpr
           | fieldSpec 								# simpleExpr
           ;
logicOp    : op=(LOGICAL_AND | LOGICAL_OR);
fieldSpec  : ctrlField ('[' fkIndexes ']')? operator ctrlValues ;
ctrlField  : '/'? IDENTIFIER ('/' IDENTIFIER)* ;	
fkIndexes  : fkIndex (',' fkIndex)* ;
fkIndex    : NUMBER;
operator   : OPERATOR_EQ | OPERATOR_NE ;
ctrlValues : ctrlValue ( ',' ctrlValue)* ;
ctrlValue  : QUOTED_STRING;

// ==== Lexer Rules ===
SHOW_WHEN        : [Ss][Hh][Oo][Ww]'-'?[Ww][Hh][Ee][Nn] ;
SHOW_IF          : [Ss][Hh][Oo][Ww]'-'?[Ii][Ff] ;
LOGICAL_AND      : [Aa][Nn][Dd] ; 
LOGICAL_OR       : [Oo][Rr] ; 
PATH_SEPARATOR   : '/' ;
IDENTIFIER       : LETTER (LETTER|DIGIT|SPECIAL)* | '.'; 
OPERATOR_EQ      : '='  ;
OPERATOR_NE      : '!=' | '<>' ;						// Two versions of 'not equal'
NUMBER           : DIGIT+ ;
QUOTED_STRING    : '"' ('\\"'|.)*? '"';					// Anything allowed between quotes
fragment LETTER  : [A-Za-z] ;
fragment DIGIT   : [0-9] ;
fragment SPECIAL : '_' | '-' ;
WS               : [ \t\r\n]+ -> skip ; 

