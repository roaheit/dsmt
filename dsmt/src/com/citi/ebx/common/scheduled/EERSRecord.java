package com.citi.ebx.common.scheduled;

public class EERSRecord {
	
	
	String LOGIN_ID;
	
	String APP_FUNC_TITLE_CD;
	
	String APP_FUNC_TITLE_DESC;
	
	String SOE_ID;
	
	String SECOND_LEVEL_ENTITLEMENT_DESC;
	
	String ATTR_FLAG;
	
	String ENV_FLAG;

	public String getLOGIN_ID() {
		if(LOGIN_ID==null)return "";
		return LOGIN_ID.equals("null")?"":LOGIN_ID;
	}

	public void setLOGIN_ID(String lOGIN_ID) {
		LOGIN_ID = lOGIN_ID;
	}

	public String getAPP_FUNC_TITLE_CD() {
		if(APP_FUNC_TITLE_CD==null)return "";
		return APP_FUNC_TITLE_CD.equals("null")?"":APP_FUNC_TITLE_CD;
		
	}

	public void setAPP_FUNC_TITLE_CD(String aPP_FUNC_TITLE_CD) {
		APP_FUNC_TITLE_CD = aPP_FUNC_TITLE_CD;
	}

	public String getAPP_FUNC_TITLE_DESC() {
		if(APP_FUNC_TITLE_DESC==null)return "";
		return APP_FUNC_TITLE_DESC.equals("null")?"":APP_FUNC_TITLE_DESC;
		
		
	}

	public void setAPP_FUNC_TITLE_DESC(String aPP_FUNC_TITLE_DESC) {
		APP_FUNC_TITLE_DESC = aPP_FUNC_TITLE_DESC;
	}

	public String getSOE_ID() {
		if(SOE_ID==null)return "";
		return SOE_ID.equals("null")?"":SOE_ID;
	}

	public void setSOE_ID(String sOE_ID) {
		SOE_ID = sOE_ID;
	}

	public String getSECOND_LEVEL_ENTITLEMENT_DESC() {
		if(SECOND_LEVEL_ENTITLEMENT_DESC==null)return "";
		return SECOND_LEVEL_ENTITLEMENT_DESC.equals("null")?"":SECOND_LEVEL_ENTITLEMENT_DESC;
				
	}

	public void setSECOND_LEVEL_ENTITLEMENT_DESC(
			String sECOND_LEVEL_ENTITLEMENT_DESC) {
		SECOND_LEVEL_ENTITLEMENT_DESC = sECOND_LEVEL_ENTITLEMENT_DESC;
	}

	public String getATTR_FLAG() {
		if(ATTR_FLAG==null)return "";
		return ATTR_FLAG.equals("null")?"":ATTR_FLAG;
		
	}

	public void setATTR_FLAG(String aTTR_FLAG) {
		ATTR_FLAG = aTTR_FLAG;
	}

	public String getENV_FLAG() {
		if(ENV_FLAG==null)return "";
		return ENV_FLAG.equals("null")?"":ENV_FLAG;
	}

	public void setENV_FLAG(String eNV_FLAG) {
		ENV_FLAG = eNV_FLAG;
	}
	
	

}
