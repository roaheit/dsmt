package com.citi.ebx.common.scheduled;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.citi.ebx.dsmt.jdbc.connector.SQLConnectReportUtil;
import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.service.LoggingCategory;

public class ClemsEERSDaoImpl {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private static final String FUNC_ENT_SQL = "EERS_CLEMS_FunctionalEntitlements.sql";
	protected String sqlID;
	protected String dataSource;
	 

	

	public  List<EERSRecord>  getFunctionalEntitlements() throws SQLException {
		List<EERSRecord> list=new ArrayList<EERSRecord>();
		// read sql file
		String sql = getInputQuery(getSqlID(),FUNC_ENT_SQL);
		LOG.info("EERS getFunctionalEntitlements--->" + sql);
		// fire the query
		Connection conn = null;
		conn = ClemsSQLReportConnectionUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		// Creating prepare statement.
		try {
			preparedStatement = conn.prepareStatement(sql);
			// execute select SQL statement
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()){
				EERSRecord record=new EERSRecord();
				record.setAPP_FUNC_TITLE_CD(rs.getString("APP_FUNC_TITLE_CD"));
				record.setAPP_FUNC_TITLE_DESC(rs.getString("APP_FUNC_TITLE_DESC"));
				//record.setATTR_FLAG(rs.getString("ATTR_FLAG"));
				record.setENV_FLAG(rs.getString("ENV_FLAG"));
				record.setLOGIN_ID(rs.getString("LOGIN_ID"));
				//record.setSECOND_LEVEL_ENTITLEMENT_DESC(rs.getString("SECOND_LEVEL_ENTITLEMENT_DESC"));
				record.setSOE_ID(rs.getString("SOE_ID"));
				list.add(record);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			conn.close();
		}

		// send result back
		return list;
	}

	

	/**
	 * Helper method to read Sql from file
	 * 
	 * @param sqlFileDirectory
	 *            , sqlFile
	 * @throws
	 */

	protected static String getInputQuery(String sqlFileDirectory,
			String sqlFile) {
		LOG.info("getInputQuery  " + sqlFileDirectory + "    " + sqlFile);
		StringBuffer inputquery = new StringBuffer();
		BufferedReader br = null;
		String line = null;
		try {
			br = new BufferedReader(new FileReader(sqlFileDirectory + sqlFile));
			while ((line = br.readLine()) != null) {

				inputquery.append(line);
				inputquery.append(DSMTConstants.NEXT_LINE);

			}

		} catch (FileNotFoundException e) {

			LOG.error("Unable to find SQl file  >>>>" + sqlFile
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} catch (IOException e) {

			LOG.error("Unable to read SQl file " + sqlFile + ". Exception ->"
					+ e + " , message " + e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return inputquery.toString();
	}
	
	public String getSqlID() {
		return sqlID;
	}

	public void setSqlID(String sqlID) {
		this.sqlID = sqlID;
	}



	public String getDataSource() {
		return dataSource;
	}



	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

}
