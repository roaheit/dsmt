package com.citi.ebx.common.scheduled;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Profile;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryHandler;
import com.orchestranetworks.service.directory.ProfileListContextBridge;

public class ClemsEERSExporterScheduledTask extends ScheduledTask {

	private static final String EERS_APPID = "167348";
	
	protected String sqlID;
	protected String exportFileDirectoryPath;
	protected String exportFileName;
	protected String dataSource;
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public void execute(ScheduledExecutionContext context)
			throws OperationException, ScheduledTaskInterruption {
		List eers=new ArrayList();
		ClemsEERSDaoImpl dao=new ClemsEERSDaoImpl();
		dao.setSqlID(getSqlID());
		dao.setDataSource(dataSource);
		// get Data 
		try {
			eers.addAll(dao.getFunctionalEntitlements());
			LOG.info("getFunctionalEntitlements-->"+eers.size());
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// write to file 
		LOG.info("WRITING to FILE");
		BufferedWriter writer = null;

		//String eersFilePath = propertyHelper.getProp("dsmt.eers.feed.file.path");
		
		String eersFilePath = exportFileDirectoryPath+exportFileName;
		LOG.info("CLEMS EERS feed will be created at " + eersFilePath);
		try {
			writer = new BufferedWriter(new FileWriter(eersFilePath));
			
			// iterator 
			// build single record 
			Iterator itr=eers.iterator();
			int i=0;
			while(itr.hasNext()){
				i++;
				EERSRecord record=(EERSRecord)itr.next();
			String lineText = "167348" + TAB + SPACE + TAB
					+ SPACE + TAB + record.getLOGIN_ID() + TAB
					+ SPACE + TAB + SPACE + TAB + record.getAPP_FUNC_TITLE_CD()
					+ TAB + record.getAPP_FUNC_TITLE_DESC() + TAB + record.getSOE_ID()
					+ TAB + SPACE + TAB + SPACE + TAB
					+ SPACE + TAB + SPACE + TAB + SPACE
					+ TAB + SPACE + TAB + SPACE + TAB
					+ record.getENV_FLAG();
			
			writer.write(lineText);		
			writer.newLine();
			}
			LOG.info("File written");
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		}
	
	
	

	private static final String ISARole = "ISA";
	private static final String FuncIndCode = "F1";
	private static final String AdminIndCode = "PA1";
	private static final String UNDERSCORE = "_";
	private static final String COLON = ":";
	private static final String SPACE = " ";
	private static final String TAB = "\t";

	public String getSqlID() {
		return sqlID;
	}
	public void setSqlID(String sqlID) {
		this.sqlID = sqlID;
	}
	public String getExportFileDirectoryPath() {
		return exportFileDirectoryPath;
	}
	public void setExportFileDirectoryPath(String exportFileDirectoryPath) {
		this.exportFileDirectoryPath = exportFileDirectoryPath;
	}
	public String getExportFileName() {
		return exportFileName;
	}
	public void setExportFileName(String exportFileName) {
		this.exportFileName = exportFileName;
	}
	public String getDataSource() {
		return dataSource;
	}
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
}