package com.citi.ebx.common.scheduled;

import java.sql.Connection
;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.orchestranetworks.service.LoggingCategory;



public class ClemsSQLReportConnectionUtils { 

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
    public static Connection getConnection(String dataSourceJNDI){
    	
    	Connection connection = null;
    	//final String dataSourceJNDI = "jdbc/SQLReports";
    
    	try{
    		
			Context aContext = new InitialContext();
			DataSource aDataSource = (DataSource)aContext.lookup(dataSourceJNDI);			
			connection = aDataSource.getConnection();
			LOG.info("DATASOURCE "+dataSourceJNDI);
			//LOG.info(connection.getClientInfo().toString());
			LOG.info("Connection Established with CLEMS Schema");
    	}
		catch (Exception e)
		{
			
			e.printStackTrace();
		}
		
		return connection;
    }
    
    
}
