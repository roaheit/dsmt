package com.citi.ebx.common.utilities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Collection;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;





import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.org.apache.log4j.Category;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.info.SchemaNodeInformation;
import com.orchestranetworks.schema.trigger.TableTriggerExecutionContext;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.ui.ResourceType;
import com.orchestranetworks.ui.UIContext;
import com.orchestranetworks.ui.UIRequestContext;
import com.orchestranetworks.ui.UIResourceLocator;
import com.orchestranetworks.ui.UIResponseContext;

/**
 * Various static utility methods.
 * 
 * @author Steve Higgins - Orchestra Networks - Dec 2013
 *
 */
public class Tools {

	/**
	 * Logger.
	 */
	private static Category LOG = getCategory();

	/**
	 * Determine whether the passed schema node has something in 
	 * its "information" attribute. 
	 * @param node the node to examine
	 * @return true if the information attribute contains a non-empty 
	 * string, false otherwise.
	 */
	public static boolean nodeHasInfo(SchemaNode node) {
		return getNodeInfo(node) != null;
	}

	/** 
	 * Extract the contents of the "information" attribute of a given node.  
	 * @param node the node to examine
	 * @return The contents of the information attribute. This may be null.  
	 */
	public static String getNodeInfo(SchemaNode node) {

		if (node == null) {
			return null;
		}

		SchemaNodeInformation nodeInfo = node.getInformation();
		if (nodeInfo == null) {
			return null;
		}

		String spec = nodeInfo.getInformation();
		if (spec == null || spec.trim().isEmpty()) {
			return null;
		}

		return spec;
	}

	/**
	 * Find the first field that has the given category. 
	 * @param tableNode The table to examine
	 * @param targetCategory The category to search for
	 * @return The path of the field, or null if no such field was found.
	 */
	public static Path findFieldWithCategory(SchemaNode tableNode, String targetCategory) {

		if (!tableNode.isTableNode()) {
			return null;
		}

		for (SchemaNode field : tableNode.getTableOccurrenceRootNode().getNodeChildren()) {
			String category = field.getCategory();
			if (category != null && category.equals(targetCategory)) {
				return Path.SELF.add(field.getPathInAdaptation());
			}
		}

		return null;

	}

	/**
	 * Return a value stored in a node's "information" attribute, where the value is stored
	 * against a key in the format <code>key=value</code>. For example, if the information attribute 
	 * is <b>DATE_FORMAT=yyyy-MM-dd</b> then, when called with a key of <b>DATE_FORMAT</b>, this method
	 * will return <b>yyyy-MM-dd</b>. If the node has no information attribute value, or the key isn't
	 * the one requested, then this method will return null;
	 * @param node The node in question
	 * @param key The key to look for
	 * @return The value associated with the key if it was found, null otherwise. 
	 */
	public static String getNodeInfoByKey(SchemaNode node, String key) {

		String text = getNodeInfo(node);
		if (text != null) {
			String[] textParts = text.split("=");
			if (textParts.length == 2 && textParts[0].equalsIgnoreCase(key)) {
				return textParts[1];
			}
		}
		return null;
	}


	/**
	 * Casts an an undefined collection to the specified type.
	 * @param type the class to cast to
	 * @param <T> type
	 * @param collection the collection to cast
	 * @return a list of the specified type
	 */
	public static <T> List<T> castList(final Class<? extends T> type, final Collection<?> collection) {
		final List<T> result = new ArrayList<T>(collection.size());
		for (final Object obj : collection) {
			result.add(type.cast(obj));
		}
		return result;
	}


	/**
	 * Gets a log4j Category for the calling object.
	 * @return a log4j Category for the calling object
	 */
	public static Category getCategory() {
		final StackTraceElement element = new Throwable().fillInStackTrace().getStackTrace()[1];
		return Category.getInstance(element.getClassName());
	}

	/**
	 * Get a table reference using an Adaptation.
	 * @param a An adaptation
	 * @param path Path to the required table
	 * @return An reference to the required table
	 */
	public static AdaptationTable getTable(final Adaptation a, final Path path) {
		return a.isRootAdaptation() ? a.getTable(path) : a.getContainer().getTable(path);
	}

	/**
	 * Get a table reference using a Service Context.
	 * @param context A Service Context
	 * @param path Path to the required table
	 * @return An reference to the required table
	 */
	public static AdaptationTable getTable(final ServiceContext context, final Path path) {
		return context.getCurrentAdaptation().getTable(path);
	}
	/**
	 * Get a table reference using <b>any</b> Table Trigger context.
	 * @param context A UI Response Context
	 * @param path Path to the required table
	 * @return An reference to the required table
	 */
	public static AdaptationTable getTable(final TableTriggerExecutionContext context, final Path path) {
		return getTable(context.getOccurrenceContext(), path);
	}

	/**
	 * Get a table reference using UI Request/Response contexts.
	 * @param context A UI Response Context
	 * @param path Path to the required table
	 * @return A reference to the required table
	 */
	public static AdaptationTable getTable(final UIContext context, final Path path) {
		if (context instanceof UIResponseContext) {
			return getTable(((UIResponseContext) context).getValueContext(), path);
		} else if (context instanceof UIRequestContext) {
			return getTable(((UIRequestContext) context).getValueContext(), path);
		} else {
			return null;
		}
	}

	/**
	 * Get a table reference using a Value Context, or any subclass of Value Context.
	 * @param context A UI Response Context
	 * @param path Path to the required table
	 * @return An reference to the required table
	 */
	public static AdaptationTable getTable(final ValueContext context, final Path path) {
		return context.getAdaptationInstance().getTable(path);
	}


	/**
	 * Dynamically load a named Java class, and check that it's of a particular type.
	 * <p> Typical invocation will be of this form:
	 * <pre>SuperClass p = Tools.getInstance("com.mycompany.SubClass", SuperClass.class)</pre></p>
	 * <p>That will attempt to load <b>com.mycompany.SubClass</b>, check that it's castable to 
	 * <b>SuperClass</b>, and return a instance of the class, cast to <b>SuperClass</b>.</p>
	 * 
	 * @param className The name of the class
	 * @param clazz The class type to check.
	 */
	public static <T> T getInstance(String className, Class<T> clazz) {

		try {
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			Class<?> loadedClass = loader.loadClass(className);
			if (clazz.isAssignableFrom(loadedClass)) {
				Object classInstance = loadedClass.newInstance();
				return clazz.cast(classInstance);
			} else {
				LOG.error(String.format("Class [%s] isn't of type [%s]", loadedClass.getCanonicalName(), clazz.getCanonicalName()));
			}
		} catch (final ClassNotFoundException e) {
			LOG.error(String.format("Couldn't find class [%s]", className));
		} catch (final Exception e) {
			LOG.error(String.format("Couldn't instantiate class [%s] - %s", className, e.getMessage()));
		}

		return null;
	}

	/**
	 * Retrieve the parent of a given record by following a given foreign key field.
	 * @param record The child record
	 * @param fkPath Path to a field in the child record that's a foreign key
	 * @return The parent record, according to the foreign key
	 */
	public static Adaptation getParentViaForeignKey(Adaptation record, Path fkPath) {
		SchemaNode fkNode = record.getSchemaNode().getNode(fkPath);
		return fkNode.getFacetOnTableReference().getLinkedRecord(record);
	}

	/**
	 * Retrieve the parent of a given record by following a given string field containing a foreign key value.
	 * @param record The child record
	 * @param fkPath Path to a field in the child record that's a foreign key
	 * @return The parent record, according to the foreign key
	 */
	public static Adaptation getParentViaForeignKeyString(AdaptationTable table, Adaptation record, Path fkPath) {
		try{
			PrimaryKey pk = PrimaryKey.parseString(record.getString(fkPath));
			Adaptation parentRecord = table.lookupAdaptationByPrimaryKey(pk);
			return parentRecord;
		}catch (PathAccessException e){
			return null;			
		}
	}

	/**
	 * Ask the server for a datasource object using a JNDI lookup.
	 * @param defn A lookup definition 
	 * @return A datasource object provided by the server, or null if no datasource could be located.
	 */
	/*
	public static DataSource getDataSource(String jndiName) {

		DataSource dataSource = null;

		try {

			LOG.debug(String.format("Obtaining a data source with JNDI name [%s]", jndiName));

			String jndiNamingContext = ConfigProperties.CITI.getProperty("jndi.naming.context");
			LOG.debug(String.format("Naming context is [%s]", jndiNamingContext));

			// Get a lookup context 
			Context jndiContext = null;
			if (jndiNamingContext == null || jndiNamingContext.isEmpty()) {
				jndiContext = new InitialContext();
			} else {
				Context initCtx = new InitialContext(); 
				jndiContext = (Context) initCtx.lookup(jndiNamingContext);
			}

			// Now use the lookup context to get a datasource
			Object obj = jndiContext.lookup(jndiName);                   // eg: "jdbc/SOME_JNDI_NAME"
			if (obj != null && obj instanceof DataSource) {
				dataSource = (DataSource) obj;
				LOG.debug("Datasource obtained");
			} else {
				LOG.error(String.format("JNDI name [%s] is not bound to a data source", jndiName));
			}

		} catch (NamingException e) {
			LOG.error(String.format("JNDI name [%s] was not found - %s", jndiName, e.getMessage()));
			e.printStackTrace();
		}

		return dataSource;

	}
*/
	/**
	 * Create a JavaScript snippet that appends links to the document header that
	 * will cause the browser to load CSS and JavaScript just once.
	 * @param ctx The EBX response context
	 * @param jsFile File object of JavaScript to include
	 * @param cssFile File object of the CSS to include 
	 */
	public static void includeJavascriptFile(final UIResponseContext ctx, final String jsFile) {
		ctx.addJS_cr(getJavascriptInclude(ctx, jsFile));
	}

	/**
	 * Create a JavaScript snippet that appends links to the document header that
	 * will cause the browser to load CSS and JavaScript just once.
	 * @param locator A resource locator provided by EBX (ie, a response context)
	 * @param jsFile Name of the javascript file to include
	 */
	public static String getJavascriptInclude(final UIResourceLocator locator, final String jsFile) {

		StringBuilder buffer = new StringBuilder(2048);
		
		if (jsFile != null && !jsFile.isEmpty()) {

			final String includeVar = jsFile.replace('.', '_').replace('/', '_').toLowerCase();
			final String url = locator.getURLForResource(ResourceType.JSCRIPT, jsFile);

			buffer.append("if (typeof("+includeVar+") == \"undefined\") { \n");
			buffer.append("  var "+includeVar+" = document.createElement(\"script\");  \n");
			buffer.append("  "+includeVar+".src = \""+url+"\"; \n");   
			buffer.append("  "+includeVar+".type = \"text/javascript\";  \n");
			buffer.append("  document.getElementsByTagName(\"head\")[0].appendChild("+includeVar+");  \n");
			buffer.append("} \n");

		}

		return buffer.toString();
		
	}

	/**
	 * Create a JavaScript snippet that appends links to the document header that
	 * will cause the browser to load CSS and JavaScript just once.
	 * @param ctx The EBX response context that will receive the generated JavaScript 
	 * @param cssFile Name of the CSS file to include 
	 */
	public static void includeStylesheetFile(final UIResponseContext ctx, final String cssFile) {
		ctx.addJS_cr(getStylesheetInclude(ctx, cssFile));
	}

	/**
	 * Create a JavaScript snippet that appends a link to the document header that
	 * will cause the browser to load CSS and JavaScript just once.
	 * @param locator A resource locator provided by EBX (ie, a response context)
	 * @param cssFile Name of the CSS file to include 
	 */
	public static String getStylesheetInclude(final UIResourceLocator locator, final String cssFile) {

		StringBuilder buffer = new StringBuilder(2048);
		
		// Create a snippet of JavaScript to add the CSS to the document header
		if (cssFile != null && !cssFile.isEmpty()) {

			final String includeVar = cssFile.replace('.', '_').replace('/', '_').toLowerCase();
			final String cssURI = locator.getURLForResource(ResourceType.STYLESHEET, cssFile);

			buffer.append("if (typeof("+includeVar+") == \"undefined\") { \n");
			buffer.append("  var "+includeVar+" = document.createElement(\"link\");  \n");
			buffer.append("  "+includeVar+".setAttribute(\"rel\", \"stylesheet\");  \n");
			buffer.append("  "+includeVar+".setAttribute(\"type\", \"text/css\");  \n");
			buffer.append("  "+includeVar+".setAttribute(\"href\", \""+cssURI+"\");  \n");
			buffer.append("  document.getElementsByTagName(\"head\")[0].appendChild("+includeVar+");  \n");
			buffer.append("}  \n");

		}

		return buffer.toString();

	}


	/**
	 * Scan all fields within a table, looking for a complex node with a type name that indicates that it's the effective-date structure. 
	 * @param fields An array of nodes to check
	 * @param infoType The 
	 * @return The complex node
	 */
	public static SchemaNode findComplexFieldWithType(SchemaNode[] fields, String targetTypeName) {

		for (SchemaNode field : fields) {

			if (field.isComplex()) {
				String typeName=null;
				try{
					typeName = field.getXsTypeName().getNameWithoutPrefix();
				} catch (NullPointerException e)
				{
					typeName="*";
				}
				if (typeName.equals(targetTypeName)) {
					return field;
				} else {
					return findComplexFieldWithType(field.getNodeChildren(), targetTypeName);
				}
			}

		}

		return null;

	}

	/**
	 * Create a date initialised to a given day, mon and year.
	 * @param year The year
	 * @param month The month - 0=January, 1=February etc.
	 * @param day The day within the month
	 * @return A date object representing the given date. The time portion is set to 00:00:00. 
	 */
	public static Date newDate(int year, int month, int day) {
		Calendar c= Calendar.getInstance();
		c.set(year, month, day, 0, 0, 0);
		return c.getTime();
	}

    /**
     * Convert a path that's relative to the table root to a path that's relative to
     * a given node.
     * @param node The given node
     * @param path The table-root-relative path
     * @return The path relative to the current node
     */
    public static Path getPathRelativeToNode(SchemaNode node, String path) {
    	
    	int currentPathLen = node.getPathInSchema().getSize();
    	int tablePathLen = node.getTableNode().getPathInSchema().getSize();
    	int distance = currentPathLen - tablePathLen;
    	
    	Path pathToParent = Path.PARENT;
    	for (int i=1; i<distance; i++) {
    		pathToParent = pathToParent.add(Path.PARENT);
    	}
    	
    	return pathToParent.add(path);
    	
    }

    /**
     * Determine whether the current node is part of the primary key.
     * @param node A node representing a field in the table
     * @return true if the node is in the primary key, false otherwise
     */
    public static boolean isNodePrimaryKey(SchemaNode node) {
    	
    	SchemaNode[] pkNodes = node.getTableNode().getTablePrimaryKeyNodes();
    	for (SchemaNode pkNode : pkNodes) {
    		if (node.equals(pkNode)) {
    			return true;
    		}
    	}
    	
    	return false;
    	
    }


}
