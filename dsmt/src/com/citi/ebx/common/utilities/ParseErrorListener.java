package com.citi.ebx.common.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.misc.Nullable;

import com.orchestranetworks.schema.SchemaNode;

/**
 * Basic class for listening to ANTLR parse errors. Collects error information
 * into a simple HTML report that can be displayed to a user.
 * 
 * @author Steve Higgins - Orchestra Networks - April 2015
 *
 */
public class ParseErrorListener  extends BaseErrorListener {
	
	private SchemaNode node = null;
	private String parseString = null;
	private List<String> errorList = new ArrayList<String>();
	
	/**
	 * Construct a new listener for parsing the given string.
	 * @param node The node that held the string being parsed
	 * @param parseString The string being parsed.
	 */
	public ParseErrorListener(SchemaNode node, String parseString) {
		this.node = node;
		this.parseString = parseString;
	}
	
	/**
	 * Construct a new listener for parsing the given string.
	 * @param parseString The string being parsed.
	 */
	public ParseErrorListener(String parseString) {
		this.parseString = parseString;
	}
	
	/**
	 * Add an error message to the message list
	 */
	@Override
	public void syntaxError(@NotNull Recognizer<?, ?> recognizer,
			@Nullable Object offendingSymbol, int line,
			int charPositionInLine, @NotNull String msg,
			@Nullable RecognitionException e) {

		errorList.add(String.format("Position %d : %s", charPositionInLine + 1, msg));

	}
	
	/**
	 * Return a flag indicating whether the parse spotted any errors.
	 * @return true if there were any errors
	 */
	public boolean isInError() {
		return errorList.size() > 0;
	}
	
	/**
	 * Format an error report with a default CSS class name.
	 * @return The error report
	 */
	public String getErrorReport() {
		return getErrorReport("ParseSyntaxError");
	}
	
	/**
	 * Format an error report as HTML.
	 * @param cssClassName The name of the CSS class to assign to the report's HTML container.
	 * @return The error report
	 */
	public String getErrorReport(String cssClassName) {
		
		// No report if no errors!
		if (errorList.isEmpty()) {
			return null;
		}
		
		// Wrap the error report in a div for styling purposes
		StringBuffer buffer = new StringBuffer();
		buffer.append(String.format("<div class=\"%s\">",cssClassName));
		
		// Add a brief summary 
		if (node == null) {
			buffer.append("<p>Error(s) while processing string ")
				.append("<span class=\"ParseString\">").append(parseString).append("</span>:<p>");
		} else {
			buffer.append("<p>Error(s) while processing string ")
				.append("<span class=\"ParseString\">").append(parseString).append("</span> on field <b>")
				.append(node.getLabel(Locale.getDefault())).append("</b>:</p>");
		}
		
		// Add the error messages as a list
		buffer.append("<ul>");
		for (String msg : errorList) {
			buffer.append("<li>").append(msg).append("</li>");
		}
		buffer.append("</ul>");

		// Finish the enclosing DIV
		buffer.append("</div>");
		
		// Return the buffer contents
		return buffer.toString();
		
	}
	
	/**
	 * Format an error report as plain text.
	 * @return The error report
	 */
	public String getPlainErrorReport() {
		
		// No report if no errors!
		if (errorList.isEmpty()) {
			return null;
		}
		
		// Start with a brief summary 
		StringBuffer buffer = new StringBuffer();
		buffer.append("Error(s) while processing string [").append(parseString).append("]");
		if (node != null) {
			buffer.append(" on field [").append(node.getLabel(Locale.getDefault())).append("]");
		}
		buffer.append(": \n");
		
		// Add the error messages 
		for (String msg : errorList) {
			buffer.append(msg).append("\n");
		}
		
		// Return the buffer contents
		return buffer.toString();
		
	}
	
	/**
	 * Return the raw list of errors.
	 * @return The list of errors generated while parsing 
	 */
	public List<String> getErrors() {
		return errorList;
	}
	
}