package com.citi.ebx.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orchestranetworks.service.OperationException;

public class AcctMatDisplayServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		  try {
			AccountingMatrixRepresentationService report = new AccountingMatrixRepresentationService(req, res);
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
}
