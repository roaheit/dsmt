package com.citi.ebx.service;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.ActionPermission;
import com.orchestranetworks.service.ServicePermission;
import com.orchestranetworks.service.Session;

public class MasterDataSpaceOnlyServicePermission implements ServicePermission {
	@Override
	public ActionPermission getPermission(SchemaNode schemaNode,
			Adaptation adaptation, Session session) {
		final AdaptationHome dataSpace = adaptation.getHome();
		final AdaptationHome parentDataSpace = dataSpace.getParentBranch();
		if (parentDataSpace == null) {
			return ActionPermission.getHidden();
		}
		if (parentDataSpace.getParentBranch() == null) {
			return ActionPermission.getEnabled();
		}
		return ActionPermission.getHidden();
	}
}