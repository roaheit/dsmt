package com.citi.ebx.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.util.IGWSaveUtil;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.service.LaunchWorkflowServlet;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;

public class IGWLaunchGOCRequestChangesWorkflowServlet extends LaunchWorkflowServlet {
	private static final long serialVersionUID = 1L;
	
	public static final String REQUEST_PARAM_SID = "sid";
	public static final String REQUEST_PARAM_MAINTENANCE_TYPE = "maintenanceType";
	public static final String REQUEST_PARAM_FUNCTION_ID = "functionalID";
	
	
	
	private static final String WF_PARAM_PARENT_DATA_SPACE = "parentDataSpace";
	private static final String WF_PARAM_DATA_SET = "dataSet";
	private static final String WF_PARAM_PROFILE = "profile";
	private static final String WF_PARAM_REQUEST_ID ="requestID";
	

	public IGWLaunchGOCRequestChangesWorkflowServlet() {
		workflowPublication = GOCConstants.IGW_WORKFLOW_PUBLICATION;
		redirectToUserTask = true;
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		
		final String maintance_Type = req.getParameter(REQUEST_PARAM_MAINTENANCE_TYPE);
		//String function_Id = req.getParameter(REQUEST_PARAM_FUNCTION_ID);
		
		LOG.info("LaunchGOCRequestChangesWorkflowServlet: service,  maintenanceId -->" +maintance_Type );
		
		
		String managementOnly= req.getParameter("managementOnly");
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		Session session=sContext.getSession();
		final AdaptationHome dataSpace = sContext.getCurrentHome();
		final Adaptation dataSet = sContext.getCurrentAdaptation();
		final UserReference userRef = sContext.getSession().getUserReference();
		final Repository repo= dataSet.getHome().getRepository();
		
		
				
		inputParameters = new HashMap<String, String>();
		inputParameters.put(WF_PARAM_PARENT_DATA_SPACE, dataSpace.getKey().getName());
		inputParameters.put(WF_PARAM_DATA_SET, dataSet.getAdaptationName().getStringName());
		inputParameters.put(WF_PARAM_PROFILE, userRef.format());
		inputParameters.put(GOCConstants.WF_PARAM_MAINTENANCE_TYPE, maintance_Type);
		inputParameters.put(GOCConstants.WF_PARAM_MANAGEMENTONLY, managementOnly);
		
		/*Explicitely set functional ID to null if maintenance type is "regular" 
		if(GOCConstants.MAINTENANCE_TYPE_REGULAR.equalsIgnoreCase(maintance_Type)){
			function_Id = null;
		}
		inputParameters.put(GOCConstants.WF_PARAM_FUNCTION_ID, function_Id);
		*/
		
		String userID = userRef.getUserId();
		LOG.info("LaunchGOCRequestChangesWorkflowServlet: User ID = " + userID);
		
		final String requestID;
		try {
			
			String environment = getEnvironment();
			
			if(null == environment || "local".equalsIgnoreCase(environment)){
				requestID = Integer.toString(getRequestId(repo,session));// Use some test value and Do not create new request ID for local testing
			}else {
				
				//requestID = new BPMWSUtil().getRequestIDFromBPM(userID); // Get Request ID from CWM
				requestID=Integer.toString(getRequestId(repo,session));
			}
			
			LOG.info("LaunchGOCRequestChangesWorkflowServlet: request ID = " + requestID);
			inputParameters.put(WF_PARAM_REQUEST_ID, requestID);
		} catch (Exception e) {
			
			//TODO handle exception and show the error message on screen.
			throw new ServletException(e);
			
		}
		
		LOG.info("LaunchGOCRequestChangesWorkflowServlet: workflow input parameters = " + inputParameters);
		
		super.service(req, res);
	}

	private String getEnvironment() {
		String environment = null;
		
		try {
			environment = propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
		} catch (Exception e) {
			// Digest the exception and continue.
		}
		return environment;
	}
	private Integer getRequestId(Repository repo,Session session ){

		
		Map<Path, Object>outputMap= new HashMap<Path, Object>();
		outputMap.put(GOCWFPaths._C_DSMT_INT_GOC_REPORT._REQUEST_ID, null);
		
		IGWSaveUtil saveUtil = new IGWSaveUtil(repo,DSMTConstants.GOC_WF_DATASPACE,DSMTConstants.GOC_WF_DATASET,GOCWFPaths._C_DSMT_INT_GOC_REPORT.getPathInSchema(),new HashMap<Path, Object>(),null,false,outputMap, null );
		try {
			Utils.executeProcedure(saveUtil, session, repo.lookupHome(
					HomeKey.forBranchName(DSMTConstants.GOC_WF_DATASPACE)));
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOG.info("result"+(Integer)outputMap.get(GOCWFPaths._C_DSMT_INT_GOC_REPORT._REQUEST_ID));
		return (Integer)outputMap.get(GOCWFPaths._C_DSMT_INT_GOC_REPORT._REQUEST_ID);
	}
	
	
}
