package com.citi.ebx.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.util.BPMWSUtil;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.ps.service.LaunchWorkflowServlet;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.UserReference;

public class LaunchRequestChangesWorkflowServlet extends LaunchWorkflowServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String WORKFLOW_PUBLICATION = "RequestorAssignedWorkflow";
	// private static final String PRODUCT_TABLE = "/root/Product";
	
	private static final String PARAM_PARENT_DATA_SPACE = "parentDataSpace";
	private static final String PARAM_DATA_SET = "dataSet";
	private static final String PARAM_PROFILE = "profile";
	private static final String PARAM_TABLE_XPATH = "tableXPath";
	
	private static final String PARAM_TABLE_ID= "tableID";
	private static final String PARAM_TABLE_NAME ="tableName";
	private static final String REQUEST_ID ="requestID";

	public LaunchRequestChangesWorkflowServlet() {
		workflowPublication = WORKFLOW_PUBLICATION;
		redirectToUserTask = true;
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		final AdaptationHome dataSpace = sContext.getCurrentHome();
		final Adaptation dataSet = sContext.getCurrentAdaptation();
		final UserReference userRef = sContext.getSession().getUserReference();
		
		inputParameters = new HashMap<String, String>();
		inputParameters.put(PARAM_PARENT_DATA_SPACE, dataSpace.getKey().getName());
		inputParameters.put(PARAM_DATA_SET, dataSet.getAdaptationName().getStringName());
		inputParameters.put(PARAM_PROFILE, userRef.format());
	
		final String tablePath = sContext.getCurrentNode().getPathInSchema().format();
		inputParameters.put(PARAM_TABLE_XPATH, tablePath); //"/root/GLOBAL_STD/GFTDS_STD/C_DSMT_ACTCLASS");
		
		Locale locale = null;
		String[] tableSplit=tablePath.split("/");
		final String tableID = tableSplit[tableSplit.length-1];		
		inputParameters.put(PARAM_TABLE_ID, tableID); //C_DSMT_ACTCLASS
		
		final String tableName = sContext.getCurrentNode().getLabel(sContext.getLocale());		
		inputParameters.put(PARAM_TABLE_NAME, tableName); //C_DSMT_ACTCLASS
		
		String userID = userRef.getUserId();
		LOG.info("User ID = " + userID);
		
		String requestID = null;
		try {
			
			requestID = new BPMWSUtil().getRequestIDFromBPM(userID);
			LOG.info("request ID = " + requestID);
			inputParameters.put(REQUEST_ID, requestID);
		} catch (Exception e) {
			LOG.error("Exception caught" + e.getMessage());
			throw new ServletException(e.getMessage());
		}
		
		LOG.info("workflow input parameters = " + inputParameters);
		
		super.service(req, res);
	}

}
