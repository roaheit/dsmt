package com.citi.ebx.service;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.exporter.task.ACCTMTXSQLExportUtil;
import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.ps.validation.export.CSVFileExporter;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.ui.UIComponentWriter;

public class AccountingMatrixDisplayService {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	String environment = getEnvironment();

	private static final String DOT = ".";

	private final String TABLE_STYLE = "margin-top:12px;border:1px solid lightgrey;border-collapse:collapse;";

	private final String DIV_STYLE = "padding-top:20px; padding-left:20px;height:90%;z-index:100;";

	private final String OCCURRENCE_STYLE = "padding:5px;height:20px;"
			+ "border:1px solid lightgrey;" + "text-align:left;"
			+ "vertical-align:center;" + "white-space:nowrap;"
			+ "cursor:pointer;";
	private final String TEXT_CEN_STYLE = "padding:5px;height:20px;"
			+ "border:1px solid lightgrey;" + "text-align:center;"
			+ "vertical-align:center;" + "white-space:nowrap;"
			+ "cursor:pointer;";
	private final String No_BOR_STYLE = "border-right:1px solid lightgrey;"
			+ "border-top:1px solid #FFFFFF;border-bottom: 1px solid #FFFFFF;"
			+ "padding:4px;border-collapse:collapse;background-color:#FFFFFF;";

	Adaptation metadataDataSet = null, record = null;
	AdaptationTable targetTable = null;
	RequestResult rs, rs2 = null;
	ResultSet rsDTL = null;
	String region = "";

	public AccountingMatrixDisplayService(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException,
			ServletException, OperationException {
		String msidValue = request.getParameter("MSID");
		String lvIDValue = request.getParameter("LVID");
		String mgidValue = request.getParameter("MGID");
		msidValue = (null == msidValue) ? "" : msidValue;
		lvIDValue = (null == lvIDValue) ? "" : lvIDValue;
		mgidValue = (null == mgidValue) ? "" : mgidValue;
		String env;

		Map<String, String> map = new HashMap<String, String>();
		try {

			final ServiceContext sContext = ServiceContext
					.getServiceContext(request);
			LOG.info("Inside Service");

			if (msidValue.equals("") || lvIDValue.equals("") || mgidValue.equals("")) {
				final UIComponentWriter uiComponentWriter = sContext
						.getUIComponentWriter();
				uiComponentWriter.add_cr("<style type=\"text/css\">   ");
				uiComponentWriter.add_cr("    .pg-normal { ");
				uiComponentWriter.add_cr("        color: black;");
				uiComponentWriter.add_cr("         font-weight: normal;");
				uiComponentWriter.add_cr("        text-decoration: none;");
				uiComponentWriter.add_cr("        cursor: pointer;");
				uiComponentWriter.add_cr("    }");
				uiComponentWriter.add_cr("    .pg-selected {");
				uiComponentWriter.add_cr("        color: black;");
				uiComponentWriter.add_cr("        font-weight: bold;");
				uiComponentWriter.add_cr("        text-decoration: underline;");
				uiComponentWriter.add_cr("        cursor: pointer;");
				uiComponentWriter.add_cr("    }");
				uiComponentWriter.add_cr("</style>");
				uiComponentWriter
						.add_cr("<div style=\"padding-left:15px; padding-top: 15px;\">");
				uiComponentWriter
						.add_cr("<p style=\"font-size:14px; font-weight:bold; color: red;\">");
				uiComponentWriter
						.add_cr("Managed Segment ,LVID and Managed Geography are mandatory values to be entered");
				uiComponentWriter.add_cr("</p>");
				uiComponentWriter.add_cr("</div>");
				throw OperationException
						.createError("Please enter Managed Segment Id Legal Entity Id And Managed Geography ID");
			}
			
//			Adaptation container = sContext.getCurrentAdaptation();
			AdaptationTable managedGeoTable;
			final AdaptationHome dataSpace = sContext.getCurrentHome();
			Repository repo = dataSpace.getRepository();
			//	metadataDataSet = EBXUtils.getMetadataDataSet(repo, "GOC", "GOC");
			metadataDataSet=EBXUtils.getMetadataDataSet(repo, DSMTConstants.DATASPACE_DSMT2Hier,DSMTConstants.DATASET_DSMT2Hier);
			managedGeoTable=metadataDataSet.getTable(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW.getPathInSchema());
			getRegion(mgidValue, managedGeoTable);
			map.put(AccountibilityMatrixConstants.msID, msidValue);
			map.put(AccountibilityMatrixConstants.mgID, mgidValue);
			map.put(AccountibilityMatrixConstants.lvid, lvIDValue);
			if(region.equalsIgnoreCase("North America") || region.contains("North"))
				region="NAM";
			else if(region.equalsIgnoreCase("ASIA") || region.contains("Asia") || region.contains("ASIA"))
				region="ASIA";
			if (null == environment
					|| "local".equalsIgnoreCase(environment)) {
				env = "local";
			}
			else {
				env = DSMTConstants.propertyHelper
						.getProp(DSMTConstants.DSMT_ENVIRONMENT);
			}
			LOG.info("env" + env);
			final File input = new File(
					DSMTConstants.bundle.getString(env + DOT
							+ "acct.matrix.service.sql.report"));

			ACCTMTXSQLExportUtil accMtx = new ACCTMTXSQLExportUtil();
			String sql = accMtx.getInputQuery(input.getAbsolutePath());
			
			LOG.info(sql);
			final Connection dbConnection = SQLReportConnectionUtils.getConnection();
			LOG.info("Connection Created");
			PreparedStatement preparedStatement = null;
			try {
				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/plain");
				preparedStatement = dbConnection.prepareStatement(sql);
				preparedStatement.setString(1, lvIDValue);
				preparedStatement.setString(2, lvIDValue);
				preparedStatement.setString(3, msidValue);
				preparedStatement.setString(4, lvIDValue);
				preparedStatement.setString(5, lvIDValue);
				preparedStatement.setString(6, msidValue);
				rsDTL = preparedStatement.executeQuery();
				LOG.info("SQL Executed");
				final ArrayList<String> list = new ArrayList<String>();
				while (rsDTL.next()) {
					list.add(rsDTL.getString(1) + "::"
					+ rsDTL.getString(2) + "::"
					+ rsDTL.getString(3) + "::"
					+ rsDTL.getString(4) + "::"
					+ rsDTL.getString(5) + "::"
					+ rsDTL.getString(6) + "::"
					+ rsDTL.getString(7) + "::"
					+ rsDTL.getString(8) + "::"
					+ rsDTL.getString(9) + "::"
					+ rsDTL.getString(10) + "::"
					+ rsDTL.getString(11) + "::"
					+ rsDTL.getString(12) + "::"
					+ rsDTL.getString(13) + "::"
					+ rsDTL.getString(14) + "::"
					+ rsDTL.getString(15) + "::"
					+ rsDTL.getString(16) + "::"
					+ rsDTL.getString(17) + "::"
					+ rsDTL.getString(18) + "::"
					+ rsDTL.getString(19));
				}
				LOG.info("Creating Screen");
				final UIComponentWriter uiComponentWriter = sContext
						.getUIComponentWriter();
				uiComponentWriter
						.add_cr("<style type=\"text/css\">   ");
				uiComponentWriter.add_cr("    .pg-normal { ");
				uiComponentWriter.add_cr("        color: black;");
				uiComponentWriter
						.add_cr("         font-weight: normal;");
				uiComponentWriter
						.add_cr("        text-decoration: none;");
				uiComponentWriter.add_cr("        cursor: pointer;");
				uiComponentWriter.add_cr("    }");
				uiComponentWriter.add_cr("    .pg-selected {");
				uiComponentWriter.add_cr("        color: black;");
				uiComponentWriter.add_cr("        font-weight: bold;");
				uiComponentWriter
						.add_cr("        text-decoration: underline;");
				uiComponentWriter.add_cr("        cursor: pointer;");
				uiComponentWriter.add_cr("    }");
				uiComponentWriter.add_cr("</style>");
				uiComponentWriter.add_cr("<div style=\"" + DIV_STYLE+ "\">");
				uiComponentWriter.add_cr("<table id=\"results\" class=\"text\" style=\"border-style:hidden;\">");
				uiComponentWriter.add_cr("<tr style=\"background-color:#FFFBE6;\"><td style=\""
								+ OCCURRENCE_STYLE
								+ "\">"
								+ "<b>LVID</b> : "
								+ lvIDValue
								+ "</tr>");
				uiComponentWriter.add_cr("<tr style=\"background-color:#FFFBE6;\"><td style=\""
								+ OCCURRENCE_STYLE
								+ "\">"
								+ "<b>Managed Segment</b> : "
								+ msidValue + "</tr>");
				if (!mgidValue.equals(""))
					uiComponentWriter.add_cr("<tr style=\"background-color:#FFFBE6;\"><td style=\""
									+ OCCURRENCE_STYLE
									+ "\">"
									+ "<b>Managed Geography</b> : "
									+ mgidValue + "</tr>");
					uiComponentWriter.add_cr("<tr style=\"border-style:hidden;\">");
					uiComponentWriter.add_cr("<td style=\"border-style:hidden;\">");
					uiComponentWriter.add_cr("</tr>");
					uiComponentWriter
						.add_cr("<tr style=\"border-style:hidden;\">");
					uiComponentWriter
							.add_cr("<td style=\"border-style:hidden;"
									+ "\">");
					uiComponentWriter.add_cr("</tr>");
					uiComponentWriter.add_cr("</table>");
					LOG.info("Creating SQL GRID");
					
					int list_size = list.size();
					String space = "";
					String lev = "";
					if (list.size() != 0) {
						uiComponentWriter
						.add_cr("<table id=\"results\" class=\"text\" style=\""
								+ TABLE_STYLE + "\">");
						uiComponentWriter
								.add_cr("<tr style=\"background-color:#FFFBE6;\">");
						uiComponentWriter
								.add_cr("<th rowspan=2 style=\""
										+ OCCURRENCE_STYLE
										+ "\">Escalation Level</th>");
						uiComponentWriter.add_cr("<th rowspan=2 style=\""
								+ OCCURRENCE_STYLE + "\">Managed Segment</th>");
						uiComponentWriter.add_cr("<th rowspan=2 style=\""
								+ OCCURRENCE_STYLE
								+ "\">Managed Segment Description</th>");
						uiComponentWriter.add_cr("<th rowspan=2 style=\""
								+ No_BOR_STYLE + "\"></th>");
						uiComponentWriter.add_cr("<th colspan=4 style=\""
								+ TEXT_CEN_STYLE + "\">NAM</th>");
						uiComponentWriter.add_cr("<th rowspan=2 style=\""
								+ No_BOR_STYLE + "\"></th>");
						uiComponentWriter.add_cr("<th colspan=4 style=\""
								+ TEXT_CEN_STYLE + "\">ASIA</th>");
						uiComponentWriter.add_cr("<th rowspan=2 style=\""
								+ No_BOR_STYLE + "\"></th>");
						uiComponentWriter.add_cr("<th colspan=4 style=\""
								+ TEXT_CEN_STYLE + "\">LATAM</th>");
						uiComponentWriter.add_cr("<th rowspan=2 style=\""
								+ No_BOR_STYLE + "\"></th>");
						uiComponentWriter.add_cr("<th colspan=4 style=\""
								+ TEXT_CEN_STYLE + "\">EMEA</th>");
		
						uiComponentWriter.add_cr("</tr>");
						uiComponentWriter
								.add_cr("<tr style=\"background-color:#FFFBE6;\">");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE + "\">Controller ID</th>");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE + "\">Controller Name</th>");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE
								+ "\">Managed Geography</th>");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE + "\">Comments</th>");
		
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE + "\">Controller Id</th>");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE + "\">Controller Name</th>");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE
								+ "\">Managed Geography</th>");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE + "\">Comments</th>");
		
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE + "\">Controller Id</th>");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE + "\">Controller Name</th>");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE
								+ "\">Managed Geography</th>");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE + "\">Comments</th>");
		
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE + "\">Controller Id</th>");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE + "\">Controller Name</th>");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE
								+ "\">Managed Geography</th>");
						uiComponentWriter.add_cr("<th style=\""
								+ OCCURRENCE_STYLE + "\">Comments</th>");
						uiComponentWriter.add_cr("</tr>");
						for (int i = 0; i < list.size(); i++) {
							String[] dataColumns = list.get(i).split("::");
							dataColumns[0] = (dataColumns[0] == null || dataColumns[0]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[0];
							dataColumns[1] = (dataColumns[1] == null || dataColumns[1]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[1];
							dataColumns[2] = (dataColumns[2] == null || dataColumns[2]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[2];
							dataColumns[3] = (dataColumns[3] == null || dataColumns[3]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[3];
							dataColumns[4] = (dataColumns[4] == null || dataColumns[4]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[4];
							dataColumns[5] = (dataColumns[5] == null || dataColumns[5]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[5];
							dataColumns[6] = (dataColumns[6] == null || dataColumns[6]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[6];
							dataColumns[7] = (dataColumns[7] == null || dataColumns[7]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[7];
							dataColumns[8] = (dataColumns[8] == null || dataColumns[8]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[8];
							dataColumns[9] = (dataColumns[9] == null || dataColumns[9]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[9];
							dataColumns[10] = (dataColumns[10] == null || dataColumns[10]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[10];
							dataColumns[11] = (dataColumns[11] == null || dataColumns[11]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[11];
							dataColumns[12] = (dataColumns[12] == null || dataColumns[12]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[12];
							dataColumns[13] = (dataColumns[13] == null || dataColumns[13]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[13];
							dataColumns[14] = (dataColumns[14] == null || dataColumns[14]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[14];
							dataColumns[15] = (dataColumns[15] == null || dataColumns[15]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[15];
							dataColumns[16] = (dataColumns[16] == null || dataColumns[16]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[16];
							dataColumns[17] = (dataColumns[17] == null || dataColumns[17]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[17];
							dataColumns[18] = (dataColumns[18] == null || dataColumns[18]
										.equalsIgnoreCase("null")) ? ""
										: dataColumns[18];

							String level = dataColumns[2];

							if (!level.equalsIgnoreCase(lev)) {
								space += "&nbsp;&nbsp;&nbsp;";
							}

							uiComponentWriter
									.add_cr("<tr style=\"padding:5px;height:20px;border:1px solid lightgrey;text-align:center;vertical-align:center;white-space:nowrap;cursor:pointer;\">");
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">" + list_size);

							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[0]);
							if (i == 0) {
								uiComponentWriter.add_cr("<td style=\""
										+ OCCURRENCE_STYLE + "\">"
										+ dataColumns[2]);
							} else
								uiComponentWriter.add_cr("<td style=\""
										+ OCCURRENCE_STYLE + "\">" + space
										+ dataColumns[2]);
							uiComponentWriter.add_cr("<td style=\""
									+ No_BOR_STYLE + "\"></td>");
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[7]);
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[8]);
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[10]);
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[9]);
							uiComponentWriter.add_cr("<td style=\""
									+ No_BOR_STYLE + "\"></td>");
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[3]);
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[4]);
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[6]);
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[5]);
							uiComponentWriter.add_cr("<td style=\""
									+ No_BOR_STYLE + "\"></td>");
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[11]);
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[12]);
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[14]);
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[13]);
							uiComponentWriter.add_cr("<td style=\""
									+ No_BOR_STYLE + "\"></td>");
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[15]);
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[16]);
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[18]);
							uiComponentWriter.add_cr("<td style=\""
									+ OCCURRENCE_STYLE + "\">"
									+ dataColumns[17]);
							uiComponentWriter.add_cr("</tr>");
							list_size = list_size - 1;
							lev = level;
						}
					} 
					else {
						uiComponentWriter
								.add_cr("<tr style=\"border-style:hidden;\">");
						uiComponentWriter
								.add_cr("<td style=\"border-style:hidden;"
										+ "\">");
						uiComponentWriter.add_cr("</tr>");
						uiComponentWriter
								.add_cr("<tr style=\"padding:5px;height:20px;text-align:center;vertical-align:center;white-space:nowrap;\">");
						uiComponentWriter
								.add_cr("<td style=\"font-weight:bold; color: red;"
										+ OCCURRENCE_STYLE
										+ "\">"
										+ "Accountibility Matrix is not available for the Supplied Managed Segment &LVID ");
						uiComponentWriter.add_cr("</tr>");
					}
					uiComponentWriter.add_cr("</table>");

					final String servletContextPath = request.getSession()
								.getServletContext().getRealPath("/");
					CSVFileExporter exporter;
					try {
						exporter = new CSVFileExporter(servletContextPath,
									"csvExport", ",", sContext.getLocale());
						final String filePath = exporter.doCSVExport(list,
									map, list.size());
						String downloadURL = sContext
									.getURLForResource("/Downloader")
									+ "?filePath=" + filePath;
						downloadURL = downloadURL.replaceAll("\\\\", "/");
						uiComponentWriter.add_cr("<a href='" + downloadURL
									+ "'>Download Report</a></div>");
					}
					catch (final OperationException ex) {
							uiComponentWriter
									.add_cr("<div style=\"padding-left:15px; padding-top: 15px;\">");
							uiComponentWriter
									.add_cr("<p style=\"font-size:12px; font-weight:bold; color: red;\">");
							uiComponentWriter.add_cr(ex.getMessage());
							uiComponentWriter.add_cr("</p>");
							uiComponentWriter.add_cr("</div>");
					} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
					}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (UnsupportedOperationException e) {
						e.printStackTrace();
					}
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String getEnvironment() {
		String environment = null;

		try {
			environment = propertyHelper
					.getProp(DSMTConstants.DSMT_ENVIRONMENT);
		} catch (Exception e) {
			// Digest the exception and continue.
		}
		return environment;
	}
	public void getRegion(String mgid,AdaptationTable mangeoTable) {
		int level;
		String managedGeoParent;
		final String pred1=DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_MAN_GEO_ID.format() + "='" + mgid + "'";
		Path endDateFieldPath = Path.parse("./ENDDT");
		final String pred2 = "date-equal(" + endDateFieldPath.format()
				+ ",'9999-12-31')";
		final String pred3 = "osd:is-null(" + endDateFieldPath.format() + ")";
		final String predicate = pred1  + " and (" + pred2
				+ " or " + pred3 + ")";
		RequestResult result;
		result=mangeoTable.createRequestResult(predicate);
		if(result.getSize()>0){
			Adaptation record=result.nextAdaptation();
			level=record.get_int(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._LEVEL);
			managedGeoParent=record.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._PARENT_ID);
			if(level!=2) 
				getRegion(managedGeoParent,mangeoTable);
			else
				region=record.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_DESCR90);
		}
	}
}