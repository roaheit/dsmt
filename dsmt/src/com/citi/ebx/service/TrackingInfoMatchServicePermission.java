package com.citi.ebx.service;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.ActionPermission;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.ServicePermission;
import com.orchestranetworks.service.Session;

public class TrackingInfoMatchServicePermission implements ServicePermission {
	protected String matchingTrackingInfo;
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	public String getMatchingTrackingInfo() {
		return matchingTrackingInfo;
	}

	public void setMatchingTrackingInfo(String matchingTrackingInfo) {
		this.matchingTrackingInfo = matchingTrackingInfo;
	}

	@Override
	public ActionPermission getPermission(SchemaNode schemaNode,
			Adaptation adaptation, Session session) {
		final String trackingInfo = session.getTrackingInfo();
		
		LOG.debug("trackingInfo = " + trackingInfo + "matchingTrackingInfo = " + matchingTrackingInfo);
		if(trackingInfo == null){
			ActionPermission.getHidden();
		}
		
		else if (null != trackingInfo && trackingInfo.startsWith(matchingTrackingInfo)) {
			return ActionPermission.getEnabled();
		}
		return ActionPermission.getHidden();
	}
}