/**
 * 
 */
package com.citi.ebx.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.jms.CRAJMSMessageHelper;
import com.citi.ebx.dsmt.jms.JMSMessageHelper;
import com.citi.ebx.dsmt.jms.JMSTopicMessageHelper;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.ValueContextForUpdate;

/**
 * @author rk00242
 *
 */
public class LaunchCRARequestServlet extends HttpServlet{
	
	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	String topicName;
	String reportingPeriod = "";
	private static String requstId="";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		final String redirectURL;
		
		
		
		reportingPeriod = req.getParameter("reportingperiod");
		reportingPeriod = (reportingPeriod == null)?"":reportingPeriod.toString();
		
		LOG.info("reportingperiod is:::::>>"+reportingPeriod);

		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		redirectURL = sContext.getURLForEndingService();
		final AdaptationHome dataSpace = sContext.getCurrentHome();
		final Adaptation dataSet = sContext.getCurrentAdaptation();
		final UserReference userRef = sContext.getSession().getUserReference();
		final Repository repo= dataSet.getHome().getRepository();
		Session session=sContext.getSession();
		
		final String tablePath = sContext.getCurrentNode().getPathInSchema().format();
		String[] tableSplit=tablePath.split("/");
		final String tableID = tableSplit[tableSplit.length-1];			
		
		LOG.info("dataspace :"+dataSpace);
		LOG.info("dataSet :"+dataSet);
		LOG.info("table :"+tableID);
		
		String requestID = createRequestID(sContext,tableID,reportingPeriod);
		LOG.info("requestID :"+requestID);
		try {
			final JMSTopicMessageHelper helper = createJMSMessageHelper(repo, session, dataSpace.getKey().format().toString(), requestID, tableID);
			
			String message = helper.createMessageContent();
			
			LOG.info("Mesasge :"+message);
			
			helper.browseMessage(message);
			
			final PrintWriter out = res.getWriter();
			out.print("<script>window.location.href='" + redirectURL + "';</script>");
			
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	
	protected JMSTopicMessageHelper createJMSMessageHelper(Repository repository, Session session, String dataSpace, String requestID, String table) throws OperationException {
		final CRAJMSMessageHelper helper = new CRAJMSMessageHelper();
		LOG.info("createJMSMessageHelper: entered");
		LOG.info("createJMSMessageHelper: dataspace:"+dataSpace);
		LOG.info("createJMSMessageHelper: requestID:"+requestID);
		helper.setRepository(repository);
		helper.setSession(session);
		//String reportingPeriod = "20160507";
		
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		String eventTimestamp = sdf.format(date);
	
		eventTimestamp = sdf.format(date);
		
		helper.setEventTimestamp(eventTimestamp);
		helper.setEventType("CRA_PROCESS_TRIGGER");
		helper.setReportingFrequency("Daily");
		helper.setRequestId(requestID);
		helper.setSubjectArea(table);
		helper.setSystemName("DSMT_Redtables");
		
	//	CRARequest.ReportingList reportingList = new CRARequest.ReportingList();
		LOG.info("reportingperiod is: "+reportingPeriod);
		helper.setReportingPeriod(reportingPeriod);
		return helper;
	}
	protected String createRequestID(ServiceContext sContext,final String tablePath,final String reportingPeriod){
		final AdaptationHome dataSpace = sContext.getCurrentHome();
		final Adaptation dataSet = sContext.getCurrentAdaptation();
		Session session=sContext.getSession();
		final String user = session.getUserReference().getUserId();
		String reqID ="";
		ProgrammaticService progService = ProgrammaticService.createForSession(session, dataSpace);
		Procedure procInsertRecord = new Procedure() {
			public void execute(ProcedureContext context) throws Exception {
				AdaptationTable table = dataSet.getTable(Path.parse("/root/CRA_DSMT2/CRA_GEN_REQ_KEY"));
				ValueContextForUpdate vc = context.getContextForNewOccurrence(table);
				try {
					vc.setValue(tablePath, Path.parse("./TABLE_PATH"));
					vc.setValue(reportingPeriod, Path.parse("./REPORTING_PERIOD"));
					vc.setValue(user, Path.parse("./User"));
					context.doCreateOccurrence(vc, table);
					requstId = vc.getValue(Path.parse("./REQ_ID")).toString();
					LOG.info("new ReqId value::>>>"+requstId);
				} 
				catch (OperationException e) {
					e.getMessage();
				}
			}
		};
		ProcedureResult pResult = progService.execute(procInsertRecord);
		LOG.info(" checking: "+pResult.hasFailed());
		return requstId;
	}
}
