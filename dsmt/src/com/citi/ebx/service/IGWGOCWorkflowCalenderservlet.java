package com.citi.ebx.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.util.IGWGOCWorkflowUtils;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.ServiceContext;

/**
 * Servlet implementation class GOCWorkflowCalenderservlet
 */
public class IGWGOCWorkflowCalenderservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
		@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		String REQUEST_PARAM_SID = "sid";
		String REQUEST_PARAM_MAINTENANCE_TYPE = "maintenanceType";
		String FUNCTIONAL_ID = "functionid";
		final String sid = req.getParameter(REQUEST_PARAM_SID);
		
		
		Repository repo=  sContext.getCurrentHome().getRepository();
		final String ajaxOperation = req.getParameter("ajaxOperation");
		LOG.info("::::::::::::::::ajaxOperation::::::::: "+ajaxOperation);
		String datevalue=DSMTConstants.BLANK_STRING;
		String managementOnly="false";
		
		if(ajaxOperation.equals("loadCalender"))
			{	
			 final String maintance_Type = req.getParameter(REQUEST_PARAM_MAINTENANCE_TYPE);
			 List<Object> outList= IGWGOCWorkflowUtils.getCalenderDate(repo, sid,maintance_Type);
			 if(null!=outList){
				 if(null!=outList.get(0)){
					 Date endDate = (Date)outList.get(0);
					 if((outList.get(1)).equals("true")){
						 managementOnly="true";
						 
					 }
					 
					 SimpleDateFormat formatter = new SimpleDateFormat(DSMTConstants.YYYY_MM_DD_HH_MM_SS);
					 if(endDate!=null)
						{
							
						 datevalue = formatter.format(endDate);
							//long dateDiff= endDate.getTime() -cal.getTimeInMillis();
							 // dateDiffDays=dateDiff/(24 * 60 * 60 * 1000);
						
						}
						 else{
							 datevalue="error";
						 }
				 }
				 else{
					 datevalue="error";
				 }
				 
				}
			 
		else{
			 datevalue="error";
		}
			}	
			else if(ajaxOperation.equals("getSID"))
				{	
				List<String> outList=IGWGOCWorkflowUtils.isValidSID(repo, sid);
				if(null!=outList){
					 datevalue = String.valueOf(outList.get(0));
					 if((outList.get(1)).equals("true")){
						 managementOnly= "true";
						
					 }
				}
				else{
					datevalue= "false";
				}
				
				
				
				}
		
		
		
			else if(ajaxOperation.equals("validateFunctionID"))
			{
				LOG.info("inside validateFunctionID in IGWGOCWorkflowCalenderServlet");
				final String functionid = null;
			List<String> outList=IGWGOCWorkflowUtils.isValidFunctionID(repo, functionid);
			if(null!=outList){
				 datevalue = String.valueOf(outList.get(0));
			}
			else{
				datevalue= "false";
			}
			}
		LOG.info("inside load calaender 6");
		
		res.reset();
		LOG.debug(" GOCWorkflowCalenderservlet >> datevalue is  " + datevalue);
		//res.getWriter().flush();
		 res.setContentType("text/plain");     
		 res.getWriter().print( datevalue+";"+managementOnly );
		//res.getWriter().write(datevalue);
		
		}
		
		
		





	
}
