package com.citi.ebx.service;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.ActionPermission;
import com.orchestranetworks.service.ServicePermission;
import com.orchestranetworks.service.Session;

public class GOCWorkflowPermission implements ServicePermission{

	protected String gocPermissionValue;
	
	public String getGocPermissionValue() {
		return gocPermissionValue;
	}

	public void setGocPermissionValue(String gocPermissionValue) {
		this.gocPermissionValue = gocPermissionValue;
	}

	@Override
	public ActionPermission getPermission(SchemaNode arg0, Adaptation arg1,
			Session arg2) {
		if ("GOCWorkflow".equalsIgnoreCase(gocPermissionValue)){
		
			return ActionPermission.getEnabled();
		}
		else return ActionPermission.getHidden();
		
	}

}
