package com.citi.ebx.service;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.ps.service.LaunchWorkflowServlet;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.UserReference;

public class LaunchCreateProductWorkflowServlet extends LaunchWorkflowServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String WORKFLOW_PUBLICATION = "CreateProduct";
	private static final String PRODUCT_TABLE = "/root/Product";
	
	private static final String PARAM_PARENT_DATA_SPACE = "parentDataSpace";
	private static final String PARAM_DATA_SET = "dataSet";
	private static final String PARAM_PROFILE = "profile";
	private static final String PARAM_PRODUCT_TABLE = "productTable";

	public LaunchCreateProductWorkflowServlet() {
		workflowPublication = WORKFLOW_PUBLICATION;
		redirectToUserTask = true;
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		final AdaptationHome dataSpace = sContext.getCurrentHome();
		final Adaptation dataSet = sContext.getCurrentAdaptation();
		final UserReference userRef = sContext.getSession().getUserReference();
		
		inputParameters = new HashMap<String, String>();
		inputParameters.put(PARAM_PARENT_DATA_SPACE, dataSpace.getKey().getName());
		inputParameters.put(PARAM_DATA_SET, dataSet.getAdaptationName().getStringName());
		inputParameters.put(PARAM_PROFILE, userRef.format());
		inputParameters.put(PARAM_PRODUCT_TABLE, PRODUCT_TABLE);
		
		super.service(req, res);
	}
}
