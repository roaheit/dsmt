package com.citi.ebx.service;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.util.BPMWSUtil;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.GOCConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.ps.service.LaunchWorkflowServlet;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.UserReference;

public class LaunchGOCRequestChangesWorkflowServlet extends LaunchWorkflowServlet {
	private static final long serialVersionUID = 1L;
	
	public static final String REQUEST_PARAM_SID = "sid";
	public static final String REQUEST_PARAM_MAINTENANCE_TYPE = "maintenanceType";
	public static final String REQUEST_PARAM_FUNCTION_ID = "functionalID";
	
	
	
	private static final String WF_PARAM_PARENT_DATA_SPACE = "parentDataSpace";
	private static final String WF_PARAM_DATA_SET = "dataSet";
	private static final String WF_PARAM_PROFILE = "profile";
	private static final String WF_PARAM_REQUEST_ID ="requestID";
	

	public LaunchGOCRequestChangesWorkflowServlet() {
		workflowPublication = GOCConstants.WORKFLOW_PUBLICATION;
		redirectToUserTask = true;
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		LOG.info("LaunchGOCRequestChangesWorkflowServlet: service, SID -->"+ req.getParameter(REQUEST_PARAM_SID) + ", maintenanceId -->" +req.getParameter(REQUEST_PARAM_MAINTENANCE_TYPE) + ", functional ID -->" + req.getParameter(REQUEST_PARAM_FUNCTION_ID));
		
		final String sid = req.getParameter(REQUEST_PARAM_SID);
		final String maintance_Type = req.getParameter(REQUEST_PARAM_MAINTENANCE_TYPE);
		String function_Id = req.getParameter(REQUEST_PARAM_FUNCTION_ID);
		if (sid == null || "".equals(sid)) {
			throw new ServletException("Must specify SID.");
		}
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		final AdaptationHome dataSpace = sContext.getCurrentHome();
		final Adaptation dataSet = sContext.getCurrentAdaptation();
		final UserReference userRef = sContext.getSession().getUserReference();
		
		
				
		inputParameters = new HashMap<String, String>();
		inputParameters.put(WF_PARAM_PARENT_DATA_SPACE, dataSpace.getKey().getName());
		inputParameters.put(WF_PARAM_DATA_SET, dataSet.getAdaptationName().getStringName());
		inputParameters.put(WF_PARAM_PROFILE, userRef.format());
		inputParameters.put(GOCConstants.WF_PARAM_SID, sid);
		inputParameters.put(GOCConstants.WF_PARAM_MAINTENANCE_TYPE, maintance_Type);
		
		/*Explicitely set functional ID to null if maintenance type is "regular" */
		if(GOCConstants.MAINTENANCE_TYPE_REGULAR.equalsIgnoreCase(maintance_Type)){
			function_Id = null;
		}
		inputParameters.put(GOCConstants.WF_PARAM_FUNCTION_ID, function_Id);
		
		
		String userID = userRef.getUserId();
		LOG.info("LaunchGOCRequestChangesWorkflowServlet: User ID = " + userID);
		
		final String requestID;
		try {
			
			String environment = getEnvironment();
			
			if(null == environment || "local".equalsIgnoreCase(environment)){
				requestID = "101240PID18" ;// Use some test value and Do not create new request ID for local testing
			}else {
				
				requestID = new BPMWSUtil().getRequestIDFromBPM(userID); // Get Request ID from CWM
			}
			
			LOG.info("LaunchGOCRequestChangesWorkflowServlet: request ID = " + requestID);
			inputParameters.put(WF_PARAM_REQUEST_ID, requestID);
		} catch (Exception e) {
			
			//TODO handle exception and show the error message on screen.
			throw new ServletException(e);
			
		}
		
		LOG.info("LaunchGOCRequestChangesWorkflowServlet: workflow input parameters = " + inputParameters);
		
		super.service(req, res);
	}

	private String getEnvironment() {
		String environment = null;
		
		try {
			environment = propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
		} catch (Exception e) {
			// Digest the exception and continue.
		}
		return environment;
	}
	
	
}
