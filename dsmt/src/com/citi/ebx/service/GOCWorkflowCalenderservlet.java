package com.citi.ebx.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.ServiceContext;

/**
 * Servlet implementation class GOCWorkflowCalenderservlet
 */
public class GOCWorkflowCalenderservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
		@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		String REQUEST_PARAM_SID = "sid";
		String REQUEST_PARAM_MAINTENANCE_TYPE = "maintenanceType";
		final String sid = req.getParameter(REQUEST_PARAM_SID);
		
		
		Repository repo=  sContext.getCurrentHome().getRepository();
		final String ajaxOperation = req.getParameter("ajaxOperation");
		String datevalue=DSMTConstants.BLANK_STRING;
		
		if(ajaxOperation.equals("loadCalender"))
			{	
			 final String maintance_Type = req.getParameter(REQUEST_PARAM_MAINTENANCE_TYPE);
			 Date endDate = GOCWorkflowUtils.getCalenderDate(repo, sid,maintance_Type);
			 SimpleDateFormat formatter = new SimpleDateFormat(DSMTConstants.YYYY_MM_DD_HH_MM_SS);
			 if(endDate!=null)
				{
					
				 datevalue = formatter.format(endDate);
					//long dateDiff= endDate.getTime() -cal.getTimeInMillis();
					 // dateDiffDays=dateDiff/(24 * 60 * 60 * 1000);
				
				}
				 else{
					 datevalue="error";
				 }
			}
			else if(ajaxOperation.equals("getSID"))
				{	
				
				 datevalue = String.valueOf(GOCWorkflowUtils.isValidSID(repo, sid));
				}
		
		res.reset();
		LOG.debug(" GOCWorkflowCalenderservlet >> datevalue is  " + datevalue);
		//res.getWriter().flush();
		 res.setContentType("text/plain");     
		 res.getWriter().print( datevalue );
		//res.getWriter().write(datevalue);
		
		}
		
		
		





	
}
