package com.citi.ebx.service;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.exporter.task.ACCTMTXSQLExportUtil;
import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.ps.validation.export.CSVFileExporterUtil;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.ui.UIComponentWriter;

public class AccountingMatrixRepresentationService {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	String environment = getEnvironment();
	
	private static final String DOT = ".";
	
	private final String TABLE_STYLE = "border:1px solid lightgrey;border-collapse:collapse;";

	private final String DIV_STYLE = "padding-top:20px; padding-left:20px;height:90%;z-index:100;";

	private final String OCCURRENCE_STYLE = "padding:5px;" + "height:20px;"
			+ "border:1px solid lightgrey;" + "text-align:left;"
			+ "vertical-align:center;" + "white-space:nowrap;" + "cursor:pointer;";
	
	private final String OCCURRENCE_STYLE_1 = "padding:5px;" + "height:20px;"
			+ "border:1px solid lightgrey;" + "text-align:left;"
			+ "vertical-align:center;" + "cursor:pointer;";

	Adaptation metadataDataSet,metadataDataSet2 = null,record = null;
	AdaptationTable targetTable,managerTable,managedGeoTable,lvidLemMappingTable = null;
	RequestResult rs,rs2 = null;
	ResultSet     rsDTL = null;
	String region="";
	String lem="";
	String delegate="";
	public AccountingMatrixRepresentationService(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException,
			ServletException, OperationException {
		String gocValue = request.getParameter("GOC");
		String lvID;
		String msID;
		String BU;
		String OU;
		String MGID;
		String FUNC;
		String gocDescription;
		Adaptation managerRecord;
		String env;
		Map<String, String> map = new HashMap<String, String>();

		try {
			
			final ServiceContext sContext = ServiceContext
					.getServiceContext(request);
			Adaptation container = sContext.getCurrentAdaptation();
			final AdaptationHome dataSpace = container.getHome();
			Session session = sContext.getSession();
			Repository repo=dataSpace.getRepository();
			metadataDataSet = EBXUtils.getMetadataDataSet(repo, "GOC","GOC");
			metadataDataSet2=EBXUtils.getMetadataDataSet(repo, DSMTConstants.DATASPACE_DSMT2Hier,DSMTConstants.DATASET_DSMT2Hier);
			targetTable = metadataDataSet.getTable(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC.getPathInSchema());
			managerTable= container.getTable(AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER.getPathInSchema());
			//		metadataDataSet2.getTable(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER.getPathInSchema());
			managedGeoTable=metadataDataSet2.getTable(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW.getPathInSchema());
			lvidLemMappingTable = container.getTable(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping.getPathInSchema());
			final String predicate="osd:contains-case-insensitive("
			+ GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC.format()
			+ ",'"
			+ gocValue
			+ "')";
			rs = targetTable.createRequestResult(predicate);
			//System.out.println(rs.getAdaptation(0));
			if(null!=rs){
				if(rs.getSize()>0){
					record=rs.getAdaptation(0);
					lvID = record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LVID);
					msID=record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
					BU=record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU);
					OU=record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_OU);
					MGID=record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID);
					gocDescription=record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCR);
					FUNC=record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FUNCTION);
					map.put(AccountibilityMatrixConstants.GOC, gocValue);
					map.put(AccountibilityMatrixConstants.goc_desc, gocDescription);
					map.put(AccountibilityMatrixConstants.msID, msID);
					map.put(AccountibilityMatrixConstants.mgID,MGID);
					map.put(AccountibilityMatrixConstants.lvid,lvID);
					map.put(AccountibilityMatrixConstants.func,FUNC);
					map.put(AccountibilityMatrixConstants.BU,BU);
					map.put(AccountibilityMatrixConstants.OU,OU);
					getRegion(MGID,managedGeoTable);
					if(region.equalsIgnoreCase("North America") || region.contains("North"))
					{
						region="NAM";
					}
					else if(region.equalsIgnoreCase("ASIA") || region.contains("Asia") || region.contains("ASIA"))
					{
						region="ASIA";
					}
					if(null == environment || "local".equalsIgnoreCase(environment)){
						env="local";
					}else{
						env = DSMTConstants.propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
					}
					map.put(AccountibilityMatrixConstants.Region, region);
					
					getLEMDelegate(lvidLemMappingTable, lvID);
					
					map.put(AccountibilityMatrixConstants.LEM_TYPE_LEM, lem);
					map.put(AccountibilityMatrixConstants.DELEGATE, delegate);
					
					
					
					final File input = new File(DSMTConstants.bundle.getString(env + DOT
							+region+DOT+ "acct.matrix.sql.report"));
					
					ACCTMTXSQLExportUtil accMtx = new ACCTMTXSQLExportUtil();
					String sql = accMtx.getInputQuery(input.getAbsolutePath());

					final Connection dbConnection =   SQLReportConnectionUtils.getConnection();

					PreparedStatement preparedStatement = null;

					try {
						response.setCharacterEncoding("UTF-8"); 
						response.setContentType("text/plain"); 
						preparedStatement = dbConnection.prepareStatement(sql);
						preparedStatement.setString(1, lvID);
						preparedStatement.setString(2, lvID);
						preparedStatement.setString(3, msID);
						

						rsDTL = preparedStatement.executeQuery();
						final ArrayList<String> list=new ArrayList<String>();

						while(rsDTL.next())
						{
							list.add(rsDTL.getString(1)+"," +rsDTL.getString(3)+"," +rsDTL.getString(4)+"," +rsDTL.getString(5)+"," +rsDTL.getString(6)+"," +rsDTL.getString(2)+","+rsDTL.getString(7));

						}

						final UIComponentWriter uiComponentWriter = sContext
								.getUIComponentWriter();
						uiComponentWriter.add_cr("<style type=\"text/css\">   ");
						uiComponentWriter.add_cr("    .pg-normal { ");
						uiComponentWriter.add_cr("        color: black;");
						uiComponentWriter.add_cr("         font-weight: normal;");
						uiComponentWriter.add_cr("        text-decoration: none;");
						uiComponentWriter.add_cr("        cursor: pointer;");
						uiComponentWriter.add_cr("    }");
						uiComponentWriter.add_cr("    .pg-selected {");
						uiComponentWriter.add_cr("        color: black;");
						uiComponentWriter.add_cr("        font-weight: bold;");
						uiComponentWriter.add_cr("        text-decoration: underline;");
						uiComponentWriter.add_cr("        cursor: pointer;");
						uiComponentWriter.add_cr("    }");
						uiComponentWriter.add_cr("</style>");

						uiComponentWriter.add_cr("<div style=\"" + DIV_STYLE + "\">");
						uiComponentWriter.add_cr("<table id=\"results\" class=\"text\" style=\"border-style:hidden;\">");
						uiComponentWriter.add_cr("</tr>");
						uiComponentWriter.add_cr("<tr style=\"background-color:#FFFBE6;\">");
						uiComponentWriter.add_cr("<td style=\"font-weight: bold;" + OCCURRENCE_STYLE + "\">"
								+"LEM : " +lem );
						uiComponentWriter.add_cr("<td style=\"font-weight: bold; max-width:10px; word-wrap: break-word;" + OCCURRENCE_STYLE_1 + "\">"
								+"Delegate:" +delegate );
						uiComponentWriter.add_cr("</tr>");
						uiComponentWriter.add_cr("<tr style=\"background-color:#FFFBE6;\">");
						uiComponentWriter.add_cr("<td style=\"font-weight: bold;" + OCCURRENCE_STYLE + "\">"
								+"GOC : " +gocValue);
						uiComponentWriter.add_cr("<td style=\"font-weight: bold;" + OCCURRENCE_STYLE + "\">"
								+"GOC Description : " + gocDescription);
						uiComponentWriter.add_cr("</tr>");
						uiComponentWriter.add_cr("<tr style=\"background-color:#FFFBE6;\">");
						uiComponentWriter.add_cr("<td style=\"font-weight: bold;" + OCCURRENCE_STYLE + "\">"
								+"Managed Segment : " +msID );
						uiComponentWriter.add_cr("<td style=\"font-weight: bold;" + OCCURRENCE_STYLE + "\">"
								+"Managed Geography : " +MGID );
						uiComponentWriter.add_cr("</tr>");
						uiComponentWriter.add_cr("<tr style=\"background-color:#FFFBE6;\">");
						uiComponentWriter.add_cr("<td style=\"font-weight: bold;" + OCCURRENCE_STYLE + "\">"
								+"LVID : " +lvID );
						uiComponentWriter.add_cr("<td style=\"font-weight: bold;" + OCCURRENCE_STYLE + "\">"
								+"Function : " +FUNC );
						uiComponentWriter.add_cr("</tr>");
						uiComponentWriter.add_cr("<tr style=\"background-color:#FFFBE6;\">");
						uiComponentWriter.add_cr("<td style=\"font-weight: bold;" + OCCURRENCE_STYLE + "\">"
								+"Business Unit : " +BU );
						uiComponentWriter.add_cr("<td style=\"font-weight: bold;" + OCCURRENCE_STYLE + "\">"
								+"Organization Unit : " +OU );

						uiComponentWriter.add_cr("<tr style=\"border-style:hidden;\">");
						uiComponentWriter.add_cr("<td style=\"border-style:hidden;" +"\">");
						uiComponentWriter.add_cr("</tr>");
						uiComponentWriter.add_cr("<tr style=\"border-style:hidden;\">");
						uiComponentWriter.add_cr("<td style=\"border-style:hidden;" +"\">");
						uiComponentWriter.add_cr("</tr>");
						uiComponentWriter.add_cr("<tr style=\"background-color:#FFFBE6;\">");
						uiComponentWriter.add_cr("<td style=\"font-weight: bold;" + OCCURRENCE_STYLE + "\">"
								+"Region : " +region );
						uiComponentWriter.add_cr("</tr>");
						uiComponentWriter.add_cr("<tr style=\"border-style:hidden;\">");
						uiComponentWriter.add_cr("<td style=\"border-style:hidden;" +"\">");
						uiComponentWriter.add_cr("</tr>");
						uiComponentWriter.add_cr("<tr style=\"border-style:hidden;\">");
						uiComponentWriter.add_cr("<td style=\"border-style:hidden;" +"\">");
						uiComponentWriter.add_cr("</tr>");
						uiComponentWriter.add_cr("</table>");
						uiComponentWriter.add_cr("<table id=\"results\" class=\"text\" style=\""
								+ TABLE_STYLE + "\">");
						uiComponentWriter.add_cr("<tr style=\"background-color:#FFFBE6;\">");
						uiComponentWriter.add_cr("<th style=\"" + OCCURRENCE_STYLE
								+ "\">Escalation Level</th>");
						uiComponentWriter.add_cr("<th style=\"" + OCCURRENCE_STYLE
								+ "\">Managed Segment</th>");
						uiComponentWriter.add_cr("<th style=\"" + OCCURRENCE_STYLE
								+ "\">Managed Segment Description</th>");
						uiComponentWriter.add_cr("<th style=\"" + OCCURRENCE_STYLE
								+ "\">Controller ID</th>");
						uiComponentWriter.add_cr("<th style=\"" + OCCURRENCE_STYLE
								+ "\">Controller Name</th>");
						uiComponentWriter.add_cr("<th style=\"" + OCCURRENCE_STYLE
								+ "\">Managed Geography</th>");
						uiComponentWriter.add_cr("<th style=\"" + OCCURRENCE_STYLE
								+ "\">Comments</th>");
						uiComponentWriter.add_cr("</tr>");
						int list_size=list.size();
						String space="";
						//String managerName="";
						String lev = "";
						if(list.size()!=0)
						{
						for (int i = 0; i < list.size(); i++) {
							String[] dataColumns = list.get(i).split(","); 
							String level = dataColumns[5];
							
							
							
							String manSeg = (dataColumns[0] == null || dataColumns[0].equalsIgnoreCase("null"))? "" : dataColumns[0];
							String manSegDesc = (dataColumns[1] == null || dataColumns[1].equalsIgnoreCase("null"))? "" : dataColumns[1];
							String controllerID = (dataColumns[2] == null || dataColumns[2].equalsIgnoreCase("null"))? "" : dataColumns[2];
							String controllerName = (dataColumns[6] == null || dataColumns[6].equalsIgnoreCase("null") )? "" : dataColumns[6];
							String manGeo = (dataColumns[4] == null || dataColumns[4].equalsIgnoreCase("null") )? "" : dataColumns[4];
							String comments = (dataColumns[3] == null || dataColumns[3].equalsIgnoreCase("null"))? "" : dataColumns[3];
							
							if(!level.equalsIgnoreCase(lev) && !lev.isEmpty()){
								space+="&nbsp;&nbsp;&nbsp;";
							}
							
							uiComponentWriter
							.add_cr("<tr style=\"padding:5px;height:20px;border:1px solid lightgrey;text-align:center;vertical-align:center;white-space:nowrap;cursor:pointer;\">");
							uiComponentWriter.add_cr("<td style=\"" + OCCURRENCE_STYLE + "\">"
									+list_size);
							uiComponentWriter.add_cr("<td style=\"" + OCCURRENCE_STYLE + "\">"
									 +manSeg);
							if(i==0){
								uiComponentWriter.add_cr("<td style=\"" + OCCURRENCE_STYLE + "\">"
										+manSegDesc);
							}else
							uiComponentWriter.add_cr("<td style=\"" + OCCURRENCE_STYLE + "\">"
									+space+manSegDesc);
							uiComponentWriter.add_cr("<td style=\"" + OCCURRENCE_STYLE + "\">"
									+controllerID);
							
							/*final String managerPredicate = "osd:contains-case-insensitive("+ DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._MANAGER_ID.format()+",'" + dataColumns[2] + "')";
							rs2=managerTable.createRequestResult(managerPredicate);
							if(rs2.getSize()>0)
							{
								managerRecord=rs2.nextAdaptation();	
								managerName=managerRecord.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._C_GDW_EMP_NAME);
							}*/
							
							uiComponentWriter.add_cr("<td style=\"" + OCCURRENCE_STYLE + "\">"
									+controllerName);
							
							uiComponentWriter.add_cr("<td style=\"" + OCCURRENCE_STYLE + "\">"
									+manGeo);
							uiComponentWriter.add_cr("<td style=\"" + OCCURRENCE_STYLE + "\">"
									+comments);
							uiComponentWriter.add_cr("</tr>");
							list_size=list_size-1;
							//managerName="";
							lev = level;
							
						}
						}
						else{
							uiComponentWriter.add_cr("<tr style=\"border-style:hidden;\">");
							uiComponentWriter.add_cr("<td style=\"border-style:hidden;" +"\">");
							uiComponentWriter.add_cr("</tr>");
							uiComponentWriter
							.add_cr("<tr style=\"padding:5px;height:20px;text-align:center;vertical-align:center;white-space:nowrap;\">");
							uiComponentWriter.add_cr("<td style=\"font-weight:bold; color: red;" + OCCURRENCE_STYLE + "\">"
									+"Accountibility Matrix is not available for GOC");
							uiComponentWriter.add_cr("</tr>");
						}
						uiComponentWriter.add_cr("</table>");
						

						final String servletContextPath = request.getSession()
								.getServletContext().getRealPath("/");
						CSVFileExporterUtil exporter;
						try {
							exporter = new CSVFileExporterUtil(servletContextPath, "csvExport", ",",
									sContext.getLocale());
							final String filePath = exporter.doCSVExport(list,map,list.size());
							String downloadURL = sContext.getURLForResource("/Downloader")
									+ "?filePath=" + filePath;
							downloadURL = downloadURL.replaceAll("\\\\", "/");

							uiComponentWriter.add_cr("<a href='" + downloadURL
									+ "'>Download Report</a></div>");
						}

						catch (final OperationException ex) {
							uiComponentWriter
							.add_cr("<div style=\"padding-left:15px; padding-top: 15px;\">");
							uiComponentWriter
							.add_cr("<p style=\"font-size:12px; font-weight:bold; color: red;\">");
							uiComponentWriter.add_cr(ex.getMessage());
							uiComponentWriter.add_cr("</p>");
							uiComponentWriter.add_cr("</div>");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					catch(UnsupportedOperationException e)
					{

					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else{
					LOG.info("GOC is not valid");
					final UIComponentWriter uiComponentWriter = sContext
							.getUIComponentWriter();
					uiComponentWriter.add_cr("<style type=\"text/css\">   ");
					uiComponentWriter.add_cr("    .pg-normal { ");
					uiComponentWriter.add_cr("        color: black;");
					uiComponentWriter.add_cr("         font-weight: normal;");
					uiComponentWriter.add_cr("        text-decoration: none;");
					uiComponentWriter.add_cr("        cursor: pointer;");
					uiComponentWriter.add_cr("    }");
					uiComponentWriter.add_cr("    .pg-selected {");
					uiComponentWriter.add_cr("        color: black;");
					uiComponentWriter.add_cr("        font-weight: bold;");
					uiComponentWriter.add_cr("        text-decoration: underline;");
					uiComponentWriter.add_cr("        cursor: pointer;");
					uiComponentWriter.add_cr("    }");
					uiComponentWriter.add_cr("</style>");
					uiComponentWriter
					.add_cr("<div style=\"padding-left:15px; padding-top: 15px;\">");
					uiComponentWriter
					.add_cr("<p style=\"font-size:14px; font-weight:bold; color: red;\">");
					uiComponentWriter.add_cr("Entered GOC  " +gocValue+ "  is not Valid");
					uiComponentWriter.add_cr("</p>");
					uiComponentWriter.add_cr("</div>");					
				}
			}
			
		}

		catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void getRegion(String mgid,AdaptationTable mangeoTable)
	{
		int level;
		String managedGeoParent;
		final String pred1=DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_MAN_GEO_ID.format() + "='" + mgid + "'";
		Path endDateFieldPath = Path.parse("./ENDDT");
		final String pred2 = "date-equal(" + endDateFieldPath.format()
				+ ",'9999-12-31')";
		final String pred3 = "osd:is-null(" + endDateFieldPath.format() + ")";
		final String predicate = pred1  + " and (" + pred2
				+ " or " + pred3 + ")";
		RequestResult result;
		result=mangeoTable.createRequestResult(predicate);
		if(result.getSize()>0)
		{
		Adaptation record=result.nextAdaptation();
		level=record.get_int(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._LEVEL);
		managedGeoParent=record.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._PARENT_ID);
		if(level!=2)
		{
			getRegion(managedGeoParent,mangeoTable);
		}
		else
		{
			region=record.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_DESCR90);
		}
		}
	}
	private String getEnvironment() {
		String environment = null;
		
		try {
			environment = propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
		} catch (Exception e) {
			// Digest the exception and continue.
		}
		return environment;
	}
	
	public void getLEMDelegate(AdaptationTable table , String lvID){
		
		String predicate = AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID.format() + "='" +lvID+ "'";
		
		List<String> del = new ArrayList<String>();
		StringBuilder sb = new StringBuilder();
		
		RequestResult rs = table.createRequestResult(predicate);
		
		for (Adaptation Record; (Record = rs.nextAdaptation()) != null;) {
			
			String managerType = Record.getString(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM_TYPE);
			
			if(!managerType.equalsIgnoreCase("LEM")){
				String delegates =  Record.getString(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM);
				del.add(delegates);
			}else{
				lem = Record.getString(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM);
			}
			
		}
		for(int i=0; i < del.size() ; i++){
			
			if(i>0){
				sb.append(",").append(del.get(i));
			}else
				sb.append(del.get(i));
			
			
		}
		
		delegate = sb.toString();
		
		
	}

}
