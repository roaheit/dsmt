package com.citi.ebx.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.DSMT2WFPaths;
import com.citi.ebx.util.IGWSaveUtil;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.service.LaunchWorkflowServlet;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;

public class LaunchAltHierMaintenanceWorkflowServlet  extends LaunchWorkflowServlet  {
	private static final long serialVersionUID = 1L;
		
	private static final String WF_PARAM_PARENT_DATA_SPACE = "parentDataSpace";
	private static final String WF_PARAM_DATA_SET = "dataSet";
	private static final String WF_PARAM_PROFILE = "profile";
	private static final String PARAM_TABLE_XPATH = "tableXPath";
	private static final String WF_PARAM_REQUEST_ID ="requestID";
	private static final String PARAM_TABLE_NAME ="tableName";
	private static final String PARAM_TABLE_ID= "tableID";
	

	public LaunchAltHierMaintenanceWorkflowServlet() {
		workflowPublication = "ApprovalWF";
		redirectToUserTask = true;
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		LOG.info("LaunchAltHierMaintenanceWorkflowServlet: service start");
		
		
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		final AdaptationHome dataSpace = sContext.getCurrentHome();
		final Adaptation dataSet = sContext.getCurrentAdaptation();
		final UserReference userRef = sContext.getSession().getUserReference();
		final Repository repo= dataSet.getHome().getRepository();
		Session session=sContext.getSession();
				
		inputParameters = new HashMap<String, String>();
		inputParameters.put(WF_PARAM_PARENT_DATA_SPACE, dataSpace.getKey().getName());
		inputParameters.put(WF_PARAM_DATA_SET, dataSet.getAdaptationName().getStringName());
		inputParameters.put(WF_PARAM_PROFILE, userRef.format());
		
		final String tablePath = sContext.getCurrentNode().getPathInSchema().format();
		inputParameters.put(PARAM_TABLE_XPATH, tablePath);//"/root/GLOBAL_STD/GFTDS_STD/C_DSMT_ACTCLASS");
		
		String[] tableSplit=tablePath.split("/");
		final String tableID = tableSplit[tableSplit.length-1];		
		inputParameters.put(PARAM_TABLE_ID, tableID); //C_DSMT_ACTCLASS
		
		final String tableName = sContext.getCurrentNode().getLabel(sContext.getLocale());		
		inputParameters.put(PARAM_TABLE_NAME, tableName); //ACCT CLASS
		
		String userID = userRef.getUserId();
		
		final String requestID;
		try {
			
			String environment = getEnvironment();
			
			if(null == environment || "local".equalsIgnoreCase(environment)){
				requestID = Integer.toString(getRequestId(repo,session));// Use some test value and Do not create new request ID for local testing
			}else {
				
				requestID = Integer.toString(getRequestId(repo,session)); // Get Request ID from CWM
			}
			
			LOG.info("LaunchGOCRequestChangesWorkflowServlet: request ID = " + requestID);
			inputParameters.put(WF_PARAM_REQUEST_ID, requestID);
		} catch (Exception e) {
			
			//TODO handle exception and show the error message on screen.
			throw new ServletException(e);
			
		}
		
		LOG.info("LaunchAltHierMaintenanceWorkflowServlet: workflow input parameters = " + inputParameters );
		
		super.service(req, res);
		
	}

	
	private String getEnvironment() {
		String environment = null;
		
		try {
			environment = propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
		} catch (Exception e) {
			// Digest the exception and continue.
		}
		return environment;
	}
	
private Integer getRequestId(Repository repo,Session session ){

		
		Map<Path, Object>outputMap= new HashMap<Path, Object>();
		outputMap.put(DSMT2WFPaths._C_APP_WF_REPORTING._REQUEST_ID, null);
		
		IGWSaveUtil saveUtil = new IGWSaveUtil(repo,DSMTConstants.DATASPACE_DSMT2_WF,DSMTConstants.DATASET_DSMT2_WF,DSMT2WFPaths._C_APP_WF_REPORTING.getPathInSchema(),new HashMap<Path, Object>(),null,false,outputMap, null );
		try {
			Utils.executeProcedure(saveUtil, session, repo.lookupHome(
					HomeKey.forBranchName(DSMTConstants.DATASPACE_DSMT2_WF)));
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOG.info("result"+(Integer)outputMap.get(DSMT2WFPaths._C_APP_WF_REPORTING._REQUEST_ID));
		return (Integer)outputMap.get(DSMT2WFPaths._C_APP_WF_REPORTING._REQUEST_ID);
	}
	
}
