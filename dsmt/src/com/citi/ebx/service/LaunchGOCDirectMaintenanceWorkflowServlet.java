package com.citi.ebx.service;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.ps.service.LaunchWorkflowServlet;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.UserReference;

public class LaunchGOCDirectMaintenanceWorkflowServlet extends LaunchWorkflowServlet {
	private static final long serialVersionUID = 1L;
		
	private static final String WF_PARAM_PARENT_DATA_SPACE = "parentDataSpace";
	private static final String WF_PARAM_DATA_SET = "dataSet";
	private static final String WF_PARAM_PROFILE = "profile";
	
	

	public LaunchGOCDirectMaintenanceWorkflowServlet() {
		workflowPublication = "GOCDirectMaintenance";
		redirectToUserTask = true;
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		LOG.info("LaunchGOCDirectMaintenanceWorkflowServlet: service start");
		
		
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		final AdaptationHome dataSpace = sContext.getCurrentHome();
		final Adaptation dataSet = sContext.getCurrentAdaptation();
		final UserReference userRef = sContext.getSession().getUserReference();
				
		inputParameters = new HashMap<String, String>();
		inputParameters.put(WF_PARAM_PARENT_DATA_SPACE, dataSpace.getKey().getName());
		inputParameters.put(WF_PARAM_DATA_SET, dataSet.getAdaptationName().getStringName());
		inputParameters.put(WF_PARAM_PROFILE, userRef.format());
	
		LOG.info("LaunchGOCDirectMaintenanceWorkflowServlet: workflow input parameters = " + inputParameters );
		
		super.service(req, res);
	}


	
	
}
