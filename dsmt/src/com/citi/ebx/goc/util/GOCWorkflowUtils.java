package com.citi.ebx.goc.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class GOCWorkflowUtils {
	
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	
	
	
	public static String getCurrentSID(final Adaptation dataSet) {
		return dataSet.getString(GOCPaths._CurrentSID);
	}
	
	/*
	public static String getFunctionalID(final Adaptation dataSet) {
		return dataSet.getString(GOCPaths._FunctionalID);
	}
	*/
	
	public static String getMaintenanceType(final Adaptation dataSet) {
		return dataSet.getString(GOCPaths._MaintenanceType);
	}
	
	public static boolean isValidateGOC(final Adaptation dataSet) {
		return dataSet.get_boolean(GOCPaths._ValidateGOC);
	}
	/**
	 * 
	 * @param workflowParameterMap
	 * @param dataSet
	 * @param pContext
	 * @throws OperationException
	 */
	public static void setWorkflowParameters(final Map<String, String> workflowParameterMap, final Adaptation dataSet,
			final ProcedureContext pContext) throws OperationException {
		final ValueContextForUpdate vc = pContext.getContext(
				dataSet.getAdaptationName());
		
		vc.setValue(workflowParameterMap.get(GOCConstants.WF_PARAM_SID), GOCPaths._CurrentSID);
		vc.setValue(workflowParameterMap.get(GOCConstants.WF_PARAM_MAINTENANCE_TYPE), GOCPaths._MaintenanceType);
		//vc.setValue(workflowParameterMap.get(GOCConstants.WF_PARAM_FUNCTION_ID), GOCPaths._FunctionalID);
		
		if(null != workflowParameterMap){
			LOG.info("Set workflow Parameters" +  workflowParameterMap  + " to " + dataSet.getAdaptationName().getStringName());
		}
		
		pContext.setAllPrivileges(true);
		pContext.doModifyContent(dataSet, vc);
		pContext.setAllPrivileges(false);
	}
	
	public static boolean isSidMatching(
			ValueContext defineGoc, Adaptation dataSet) {
		String currentSid = getCurrentSID(dataSet);

		if (currentSid == null || "".equals(currentSid)) {
			// Not in WF, do not check
			return true;
		}

		Adaptation sidRecord = Utils.getLinkedRecord(defineGoc,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);

		if (sidRecord == null) {
			return false;
		}
		if (currentSid
				.equals(sidRecord
						.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID))) {
			return true;
		}
		return false;
	}
	
	public static Adaptation getMetadataDataSet(final Repository repo)
			throws OperationException {
		final AdaptationHome metadataDataSpace = repo.lookupHome(
				HomeKey.forBranchName(GOCConstants.METADATA_DATA_SPACE));
		if (metadataDataSpace == null) {
			throw OperationException.createError("Metadata data space \"" + GOCConstants.METADATA_DATA_SPACE + "\" not found.");
		}
		final Adaptation metadataDataSet = metadataDataSpace.findAdaptationOrNull(
				AdaptationName.forName(GOCConstants.METADATA_DATA_SET));
		if (metadataDataSet == null) {
			throw OperationException.createError("Metadata data set \"" + GOCConstants.METADATA_DATA_SET + "\" not found.");
		}
		return metadataDataSet;
	}
	public static Adaptation getMetadataDataSetGOCPartnerRefData(final Repository repo)
			throws OperationException {
		final AdaptationHome metadataDataSpace = repo.lookupHome(
				HomeKey.forBranchName(GOCConstants.GOC_REF_DATA_SPACE));
		if (metadataDataSpace == null) {
			throw OperationException.createError("Metadata data space \"" + GOCConstants.METADATA_DATA_SPACE + "\" not found.");
		}
		final Adaptation metadataDataSet = metadataDataSpace.findAdaptationOrNull(
				AdaptationName.forName(GOCConstants.GOC_REF_METADATA_DATA_SET));
		if (metadataDataSet == null) {
			throw OperationException.createError("Metadata data set \"" + GOCConstants.METADATA_DATA_SET + "\" not found.");
		}
		return metadataDataSet;
	}
	
	public static AdaptationTable getTableInInitialSnapshot(final AdaptationTable table) {
		final Adaptation dataSet = table.getContainerAdaptation();
		final AdaptationHome dataSpace = dataSet.getHome();
		
		final AdaptationHome initialSnapshot = dataSpace.getParent();
		final Adaptation initialDataSet = initialSnapshot.findAdaptationOrNull(
				dataSet.getAdaptationName());
		return initialDataSet.getTable(table.getTablePath());
	}
	
	// This just returns first of month of the current month,
	// but in future may need to do a lookup in a calendar
	public static final Date calculateGOCEffectiveDateOld(final Adaptation gocRecord) {
		final Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}
	
	public static final Date calculateGOCEffectiveDate(final Adaptation dataSet, final ProcedureContext pContext) {
		final Calendar cal = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		final String  currentSid= getCurrentSID(dataSet);
		final String  maintance_Type= getMaintenanceType(dataSet);
		final Adaptation ccRecord=getCalenderDatefromDaterange(pContext.getAdaptationHome().getRepository(), currentSid, maintance_Type);
		Date calendarDate =null;
		
		
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		
		if(null == ccRecord){
			return cal.getTime();
		}
		else{
			String calendertype= ccRecord.getString(GOCWFPaths._C_DSMT_CAL._CAL_TYPE_ID);
			if("1".equalsIgnoreCase(calendertype)){
				if("functional".equals(maintance_Type))
				{
					calendarDate = ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._FNL_END_DT);
				}else
				{
					calendarDate = ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._REG_END_DT);	
				}
			}
			else if("2".equalsIgnoreCase(calendertype)){
				if("functional".equals(maintance_Type))
				{
					calendarDate = ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._FNL_END_DT);
				}
				else{
					String calanderPeriod = String.valueOf(ccRecord.get_int(GOCWFPaths._C_DSMT_CAL._PERIOD));
					cal.set(Calendar.MONTH, Integer.parseInt(calanderPeriod)-1);
					return cal.getTime();
				}
				
			}
			
		}
		
			
			
			
			if(  null != calendarDate && cal2.getTime().compareTo(calendarDate)>0){
				LOG.debug("cal is after calendarDate");
				cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH)+1));
				
	    	}
    	
		
		LOG.debug("currentSid : "+currentSid +" maintance_Type : "+ maintance_Type + " new eff date " +cal.getTime() );
		
		return cal.getTime();
	}
	
	
	
	public static String checkFunctionEndDate(final Adaptation dataSet, Repository repo) {
		final Calendar cal = Calendar.getInstance();
		final String  currentSid= getCurrentSID(dataSet);
		final String  maintance_Type= getMaintenanceType(dataSet);
		String isvalidRequest ="true";
		//to check for current Data
		cal.add(Calendar.DATE, -1);
		if(maintance_Type.equals(DSMTConstants.FUNCTIONAL))	{
				final Date functionalEndDate=getCalenderDate(repo, currentSid, maintance_Type);		
				if(null == functionalEndDate){
					return "true";
				}
				
				// if (!(cal.getTime().before(calendarDate)
					//		&& cal.getTime().after(calendarDate)))
					 if( cal.getTime().compareTo(functionalEndDate)>=0){
					LOG.info("checkFunctionEndDate : sysdate date is not in functional Date Range.");
					SimpleDateFormat sfd= new SimpleDateFormat("yyyy-MM-dd");
					 String date = sfd.format(functionalEndDate);
					isvalidRequest=date;
					
		    	}
				
				LOG.info("checkFunctionEndDate >>> currentSid : "+currentSid +" maintance_Type : "+ maintance_Type + " new eff date " +functionalEndDate );
				}
				
		return isvalidRequest;
	}
	
	
	
/*	public static String getLocalCostCode(final ValueContext vc) {
		final String glLCC = (String) vc.getValue(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GLLocalCostCode);
		if (glLCC == null || "".equals(glLCC)) {
			final String reLCC = (String) vc.getValue(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._RELocalCostCode);
			if (reLCC == null || "".equals(reLCC)) {
				final String p2pLCC = (String) vc.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._P2PLocalCostCode);
				if (p2pLCC == null || "".equals(p2pLCC)) {
					return null;
				}
				return p2pLCC;
			}
			return reLCC;
		}
		return glLCC;
	}*/

	private GOCWorkflowUtils() {
	}
	
	
	
	public static String isValidSID(Repository repo, String sid)
	{
		
		final Adaptation metadataDataSet ;
		 
		String isValidSID ="false";
		
		try {
			
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(repo);
			String calenderType_ID= getCalenderTypeId (metadataDataSet,sid);
			
			if(calenderType_ID!=null)
			{
				
				isValidSID= getCalenderDataFull(repo,sid , calenderType_ID);
				//isValidSID= "true";
			}
			
			
			
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
		
		}
		return isValidSID;
		
		
}
	
	public static Date getCalenderDate(Repository repo, String sid, String maintance_Type)
	{
		
		final Adaptation metadataDataSet ;
		 Calendar cal = new GregorianCalendar();
		
		 int period=(cal.get(Calendar.MONTH) + 1);
		 int year =cal.get(Calendar.YEAR); 
		 Date endDate =null ;

		 
	//	 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 

		
		try {
			
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(repo);
			String calenderType_ID= getCalenderTypeId (metadataDataSet,sid);
			if(null != calenderType_ID && "2".equals(calenderType_ID) && "regular".equals(maintance_Type)){
				 endDate= getCalenderEndDateregular(calenderType_ID,Integer.toString(year),metadataDataSet ,maintance_Type);	
			}
			else{
				 endDate=getCalendarDate(calenderType_ID,Integer.toString(year),Integer.toString(period),metadataDataSet ,maintance_Type);
			}
			 
				/*if(endDate!=null)
				{
					
					calenderDate = formatter.format(endDate);
					//long dateDiff= endDate.getTime() -cal.getTimeInMillis();
					 // dateDiffDays=dateDiff/(24 * 60 * 60 * 1000);
				
				}*/
			
			
			
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
		
		}
		
		return endDate;
		
	}
	public static String getCalenderDataFull(Repository repo, String sid , String calenderType_ID)
	{
		
		final Adaptation metadataDataSet ;
		 Calendar cal = new GregorianCalendar();
		
		 int period=(cal.get(Calendar.MONTH) + 1);
		 int year =cal.get(Calendar.YEAR); 
		 List<Date> endDate = new ArrayList<Date>() ;
		 String isValidDate="true";
		 
	//	 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 

		
		try {
			
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(repo);
			
			
			 endDate=getCalendarDataWildCard(calenderType_ID,Integer.toString(year),Integer.toString(period),metadataDataSet );
			 
			 if(endDate!=null)
				{
				 // datevalue = formatter.format(endDate);
				 Calendar calender = Calendar.getInstance();
				 if (calender.getTime().before(endDate.get(0))
							&& calender.getTime().after(endDate.get(1))) {
						//ok everything is fine, date in range
						LOG.debug("date is in the range");
						
					} else {
							LOG.debug("date is out of range");
							isValidDate="error";
						
					}
				 
				}
				
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
		
		}
		
		return isValidDate;
		
	}

	private static String getCalenderTypeId(final Adaptation metadataDataSet, 
			final String sid) {
			
		final AdaptationTable ccTable =	metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
		final String predicate = GOCWFPaths._C_DSMT_SID_ENH._SID.format()
				+ "=\"" + sid + "\" and " + GOCWFPaths._C_DSMT_SID_ENH._EFF_STATUS.format() + "=\"A\"";
		
		LOG.debug("predicate to check if an Active SID exists in the SID enhancement table - " + predicate);
		final RequestResult reqRes = ccTable.createRequestResult(predicate);
		final Adaptation ccRecord;
		try {
			ccRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (null == ccRecord ) {
			
			if(null != sid){
			LOG.error("No record found for predicate " + predicate
					+ " in table " + ccTable.getTablePath().format());
			}
			
			return null;
		}
		
			int calenderType = ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._CalendarType);
		LOG.debug("CalendarType for SID: "+ sid + "   is : "+ calenderType); 
		
		/* set default value to 1 */
		if (calenderType != 1 && calenderType !=2 ) {
			LOG.debug("CalendarType for SID is defaulted to 1"); 
			calenderType = 1;
		}
		
		return Integer.toString(calenderType);
	}
	private static Date getCalendarDate(
			final String calendertType, final String year, final String period,
			final Adaptation metadataDataSet, final String maintance_Type) {
		final Date endDate;
//		final Date currDate = new Date();
		
		final AdaptationTable ccTable =	metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_CAL.getPathInSchema());
		final String predicate = GOCWFPaths._C_DSMT_CAL._CAL_TYPE_ID.format()
				+ "=\"" + calendertType + "\" and "
				+ GOCWFPaths._C_DSMT_CAL._YEAR.format()
				+ "=\"" + year + "\" and "
				+ GOCWFPaths._C_DSMT_CAL._PERIOD.format()
				+ "=\"" + period + "\"";
		final RequestResult reqRes = ccTable.createRequestResult(predicate);
		final Adaptation ccRecord;
		try {
			ccRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (ccRecord == null ) {

			if(null !=  calendertType){
				LOG.error("No record found for predicate " + predicate
					+ " in table " + ccTable.getTablePath().format());
			}
			return null;
		}
		if(maintance_Type.equals(DSMTConstants.FUNCTIONAL))
		{
			endDate = ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._FNL_END_DT);
		}else
		{
			 endDate = ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._REG_END_DT);	
		}
		
		//ccRecord.getString(GOCWFPaths._C_DSMT_CAL._REG_END_DT);
		
		LOG.debug("End Date for maintance_Type : "+ maintance_Type +"   is : "+ endDate ); 
		
		return endDate;
	}
	
	private static List<Date> getCalendarDataWildCard(
			final String calendertType, final String year, final String period,
			final Adaptation metadataDataSet) {
		
//		final Date currDate = new Date();
		List<Date> calenderdates= new ArrayList<Date>();
		final AdaptationTable ccTable =	metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_CAL.getPathInSchema());
		final String predicate = GOCWFPaths._C_DSMT_CAL._CAL_TYPE_ID.format()
				+ "=\"" + calendertType + "\" and "
				+ GOCWFPaths._C_DSMT_CAL._YEAR.format()
				+ "=\"" + year + "\" and "
				+ GOCWFPaths._C_DSMT_CAL._PERIOD.format()
				+ "=\"" + period + "\"";
		final RequestResult reqRes = ccTable.createRequestResult(predicate);
		final Adaptation ccRecord;
		try {
			ccRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (ccRecord == null) {
			if(null != calendertType){
			LOG.error("No record found for predicate " + predicate
					+ " in table " + ccTable.getTablePath().format());
			}
			return null;
		}else{
			
			calenderdates.add(ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._FNL_END_DT));
			calenderdates.add(ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._FNL_START_DT));
			calenderdates.add(ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._REG_END_DT));
			calenderdates.add(ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._REG_START_DT));
			
		}
		
		//ccRecord.getString(GOCWFPaths._C_DSMT_CAL._REG_END_DT);
		
		LOG.debug("End Date for maintance_Type   is : "+ calenderdates ); 
		
		return calenderdates;
	}
	
	public static String getMaintenanceOrder(Repository repo, String sid, String bu)
	{
		
		final Adaptation metadataDataSet ;
		String maintenanceOrder =DSMTConstants.MAINTENANCEORDER;		
		int seq=0;
		
		Map<String , Integer> orginalMap = new LinkedHashMap<String, Integer>();
		
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(repo);
			final AdaptationTable ccTable =	metadataDataSet.getTable(
					GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
			final String predicate = GOCWFPaths._C_DSMT_SID_ENH._SID.format()
					+ "=\"" + sid + "\"";
			final RequestResult reqRes = ccTable.createRequestResult(predicate);
			final Adaptation ccRecord;
			try {
				ccRecord = reqRes.nextAdaptation();
			} finally {
				reqRes.close();
			}
			if (ccRecord == null) {
				if(null != sid){
					
					final AdaptationTable sidBUReferenceTable = metadataDataSet.getTable(
							GOCWFPaths._C_DSMT_SID_BU_REF.getPathInSchema());
					final String prd = GOCWFPaths._C_DSMT_SID_BU_REF._SID.format()+ "=\"" + sid + "\"" +
							" and "+ GOCWFPaths._C_DSMT_SID_BU_REF._C_DSMT_FRS_BU.format() + "=\"" + bu + "\"" + 
							" and " +endDatePredicate(GOCWFPaths._C_DSMT_SID_BU_REF._ENDDT.format()) + 
							" and " + GOCWFPaths._C_DSMT_SID_BU_REF._EFF_STATUS.format() +  "=\"A\"";
					final RequestResult result = sidBUReferenceTable.createRequestResult(prd);
					final Adaptation record;
					try {
						record = result.nextAdaptation();
					} finally {
						result.close();
					}
					
					if(record != null){
						if(record.get_int(GOCWFPaths._C_DSMT_SID_BU_REF._GLMaintenanceOrder)==0){
							orginalMap.put(DSMTConstants.GLSYSTEM, 3);
							seq++;
								}else{
									orginalMap.put(DSMTConstants.GLSYSTEM, record.get_int(GOCWFPaths._C_DSMT_SID_BU_REF._GLMaintenanceOrder));
								}
							if(record.get_int(GOCWFPaths._C_DSMT_SID_BU_REF._RptEngineMaintenanceOrder)==0){
								orginalMap.put(DSMTConstants.REPSYSTEM, 3);
								}else{
									orginalMap.put(DSMTConstants.REPSYSTEM, record.get_int(GOCWFPaths._C_DSMT_SID_BU_REF._RptEngineMaintenanceOrder));
								}
							if(record.get_int(GOCWFPaths._C_DSMT_SID_BU_REF._P2PMaintenanceOrder)==0){
								orginalMap.put(DSMTConstants.P2PSYSTEM, 3);
								}else{
									orginalMap.put(DSMTConstants.P2PSYSTEM, record.get_int(GOCWFPaths._C_DSMT_SID_BU_REF._P2PMaintenanceOrder));
								}
						
					}else{
						
						LOG.error("No record found for predicate " + predicate
								+ " in table " + GOCWFPaths._C_DSMT_SID_BU_REF.getPathInSchema().format());
					}
					
					
				}
			
			}else{
					if(ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._GLMaintenanceOrder)==0){
					orginalMap.put(DSMTConstants.GLSYSTEM, 3);
					seq++;
						}else{
							orginalMap.put(DSMTConstants.GLSYSTEM, ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._GLMaintenanceOrder));
						}
					if(ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._RptEngineMaintenanceOrder)==0){
						orginalMap.put(DSMTConstants.REPSYSTEM, 3);
						}else{
							orginalMap.put(DSMTConstants.REPSYSTEM, ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._RptEngineMaintenanceOrder));
						}
					if(ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._P2PMaintenanceOrder)==0){
						orginalMap.put(DSMTConstants.P2PSYSTEM, 3);
						}else{
							orginalMap.put(DSMTConstants.P2PSYSTEM, ccRecord.get_int(GOCWFPaths._C_DSMT_SID_ENH._P2PMaintenanceOrder));
						}
					
			}
			if(seq!=3){
				maintenanceOrder= generateSeq(orginalMap);
			}
			
			
			
			
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
		
		}
		LOG.debug(" maintenanceOrder >> "+ maintenanceOrder);
		return maintenanceOrder;
		
	}
	
	private static String endDatePredicate(String endDateField){
		
		
		return " ( osd:is-null(" + endDateField + ") or date-equal(" + endDateField + ", '9999-12-31') )";
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static  String generateSeq(Map<String , Integer> orginalMap ){
		
		String maintenanceOrder = DSMTConstants.MAINTENANCEORDER;
		
		List<LinkedList<Entry<String, Integer>>> list = new LinkedList(orginalMap.entrySet());
		Collections.sort(list, new Comparator() {
			
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue())
                                       .compareTo(((Map.Entry) (o2)).getValue());
			}
		});
		
		
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey().toString(), (Integer)entry.getValue());
		}
		StringBuffer maintenaceseq =new StringBuffer() ;
		for (Map.Entry entry : sortedMap.entrySet()) {
			LOG.debug("Key : " + entry.getKey() 
                                   + " Value : " + entry.getValue());
			maintenaceseq.append(entry.getKey());
			maintenaceseq.append("_");
		}
		
		try {
			maintenanceOrder = maintenaceseq.substring(0, 9);
		} catch (Exception e) {
			LOG.error(e +  " caught while trying to create maintenance order string. Default value will be set GL_P2P_RE.");
		}
		return maintenanceOrder;
	}


	public static Adaptation getCalenderDatefromDaterange(Repository repo, String sid, String maintance_Type)
	{
		
		final Adaptation metadataDataSet ;
		 Calendar cal = new GregorianCalendar();
		
		 int period=(cal.get(Calendar.MONTH) + 1);
		 int year =cal.get(Calendar.YEAR); 
		//  Date endDate =null ;
		  Adaptation resultSet =null ;
		
		try {
			
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(repo);
			String calenderType_ID= "1"; //getCalenderTypeId (metadataDataSet,sid);
			
			resultSet = getCalendarDatefromDateRange(calenderType_ID,Integer.toString(year),Integer.toString(period),metadataDataSet ,maintance_Type);
			
			
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
		
		}
		catch (Exception ex) {
			LOG.error("Exception caught", ex);
		
		}
		return resultSet;
		
	}
	
	private static Adaptation getCalendarDatefromDateRange(
			final String calendertType, final String year, final String period,
			final Adaptation metadataDataSet, final String maintance_Type) {
	//	final Date endDate;
//		final Date currDate = new Date();
		 Calendar cal = Calendar.getInstance();
		final AdaptationTable ccTable =	metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_CAL.getPathInSchema());
		String endDatePredicate = null;
		final String predicate ;
		
		if("functional".equals(maintance_Type))
		{
			endDatePredicate= functionalDatePredicate(cal.getTime());
		}
		else
		{
			endDatePredicate= regularDatePredicate(cal.getTime());	
		}
		
		if("1".equals(calendertType)){
					predicate = GOCWFPaths._C_DSMT_CAL._CAL_TYPE_ID.format()
						+ "=\"" + calendertType + "\" and "
						+ GOCWFPaths._C_DSMT_CAL._YEAR.format()
						+ "=\"" + year + "\" and "
						+ GOCWFPaths._C_DSMT_CAL._PERIOD.format()
						+ "=\"" + period + "\"";
		}
		else {
				predicate = GOCWFPaths._C_DSMT_CAL._CAL_TYPE_ID.format()
					+ "=\"" + calendertType + "\" and "
					+ GOCWFPaths._C_DSMT_CAL._YEAR.format()
					+ "=\"" + year + "\" and "
					+ endDatePredicate ;
		
		
			
		}
		
		LOG.debug("predicate to search calendar data - " + predicate);
		final RequestResult reqRes = ccTable.createRequestResult(predicate);
		final Adaptation ccRecord;
		try {
			ccRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (ccRecord == null) {
			
			if(null != calendertType){
			LOG.error("No record found for predicate " + predicate
					+ " in table " + ccTable.getTablePath().format());
			}
			return null;
		}
		LOG.debug("Calender period for maintance_Type : "+ maintance_Type  +"   is : "+ ccRecord.get(GOCWFPaths._C_DSMT_CAL._PERIOD) );
	
		
		return ccRecord;
	}
	
private static String functionalDatePredicate( Date sysdate){
	
		final String date = DSMTUtils.formatDate(sysdate, DSMTConstants.DEFAULT_DATETIME_FORMAT);
		String predicate = " ( date-less-than("+  GOCWFPaths._C_DSMT_CAL._FNL_START_DT.format() + ",'" + date+ "') and date-greater-than("+  GOCWFPaths._C_DSMT_CAL._FNL_END_DT.format() + ",'" + date+ "')) ";
		
		return predicate; 
	}


public static void main(String[] args) {
	
//	System.out.println(functionalDatePredicate(Calendar.getInstance().getTime()));
}

private static String regularDatePredicate( Date sysdate ){
	
	final String date = DSMTUtils.formatDate(sysdate, DSMTConstants.DEFAULT_DATETIME_FORMAT);
	final String predicate = " ( date-less-than("+  GOCWFPaths._C_DSMT_CAL._REG_START_DT.format() + ",'" + date+ "') and date-greater-than("+  GOCWFPaths._C_DSMT_CAL._REG_END_DT.format() + ",'" + date+ "')) ";
	
	return predicate ;
}


public static Date getCalenderEndDateregular(
		final String calendertType, final String year,
		final Adaptation metadataDataSet, final String maintance_Type)
{
		final Date endDate;
//	final Date currDate = new Date();
	 Calendar cal = Calendar.getInstance();
	final AdaptationTable ccTable =	metadataDataSet.getTable(
			GOCWFPaths._C_DSMT_CAL.getPathInSchema());
	String endDatePredicate = regularDatePredicate(cal.getTime());	
	final String predicate ;

	predicate = GOCWFPaths._C_DSMT_CAL._CAL_TYPE_ID.format()
			+ "=\"" + calendertType + "\" and "
			+ GOCWFPaths._C_DSMT_CAL._YEAR.format()
			+ "=\"" + year + "\" and "
			+ endDatePredicate ;
	
	
	
	
	LOG.debug("getCalenderEndDateregular >> predicate to search calendar data - " + predicate);
	final RequestResult reqRes = ccTable.createRequestResult(predicate);
	final Adaptation ccRecord;
	try {
		ccRecord = reqRes.nextAdaptation();
	} finally {
		reqRes.close();
	}
	if (ccRecord == null) {
		
		if(null != calendertType){
		LOG.error("No record found for predicate " + predicate
				+ " in table " + ccTable.getTablePath().format());
		}
		return null;
	}
	LOG.debug("Calender period for maintance_Type : "+ maintance_Type  +"   is : "+ ccRecord.get(GOCWFPaths._C_DSMT_CAL._PERIOD) );

	endDate = ccRecord.getDate(GOCWFPaths._C_DSMT_CAL._REG_END_DT);
	 return  endDate;	
}

	public static Adaptation getQueryRecord (Repository repo , Path tablePath, Path [] tableColumns , String ... params   ) throws OperationException{
		Adaptation metadata = getMetadataDataSet(repo);
		AdaptationTable table = metadata.getTable(tablePath);
		RequestResult reqRes = null;
		Adaptation record = null;
		int length = tableColumns.length;
		/*for(int i = 0; i<length; i++){
			
		}*/
		String predicate = tableColumns[0].format() + "=\"" + params[0] + "\" ";
		if(tableColumns[1] != null){
			predicate =  predicate+" and "  + tableColumns[1].format() + "=\"" + params[1] + "\" ";
		}
		//System.err.println("predicate from getQueryRecord  ==  " +predicate);
		reqRes = table.createRequestResult(predicate);
		
		if (null != reqRes) {
			
			record = reqRes.nextAdaptation();
		}
		reqRes.close();
		return record;
		
	}

}

	

