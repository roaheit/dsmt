package com.citi.ebx.goc.task;

import java.io.File;
import java.io.IOException;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.onwbp.org.apache.commons.io.FileUtils;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class NotifyProcessCompletionScriptTask extends ScriptTaskBean {

	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private String exportFilePath;
	private String fileName ;

	
	private String notificationDL; // DSMTConstants.bundle.getString("notify.goc.sync.completion.dl");		
	private String subject;  // "DSMT2 GOC Synchronization Notification";
	private String message;  // "Process {process name} is complete. The synchronization file is present at location - " ;

	
	
	// *CSS Global Data Standards Operations <dl.css.global.data.standards.operations@imcnam.ssmb.com>
	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		
		String newFileLocation = null;
		LOG.info("exportFilePath = " + exportFilePath + ", fileName = " + fileName);
		
		/** Append date to the newly generated file. */
		if ( DSMTUtils.isStringNotEmptyOrNull(exportFilePath) && DSMTUtils.isStringNotEmptyOrNull(fileName)){
			
			if(! exportFilePath.endsWith(DSMTConstants.FORWARD_SLASH)){
				exportFilePath = exportFilePath + DSMTConstants.FORWARD_SLASH;
			}
			
			final String originalFileLocation = exportFilePath + fileName;
			final String todaysDate = DSMTUtils.getTodaysDate("MM-dd-yyyy-HH-mm-ss");
			newFileLocation = getNewFileName(originalFileLocation, todaysDate);
			LOG.info("Renaming file = " + originalFileLocation + " to " + newFileLocation);
			
			final boolean status = renameFile(originalFileLocation, newFileLocation);
			if(status){
				LOG.info("File Successfully renamed.. ");
			}
			else{
				LOG.info("File Rename failed.. ");
			}
		}
		
		
		/** Notify users that GOC synchronization is complete*/
		notifyUsers(newFileLocation);
		
	}
	

	public String getNotificationDL() {
		return notificationDL;
	}


	public void setNotificationDL(String notificationDL) {
		this.notificationDL = notificationDL;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 
	 */
	private void notifyUsers(final String fileLocation){
		
		DSMTNotificationService.notifyEmailID(notificationDL, subject, message  + fileLocation, false);
	}
	
	
	private String getNewFileName(final String originalFileLocation, final String todaysDate) {
		
		if(DSMTUtils.isStringNotEmptyOrNull(originalFileLocation)){
			
			return originalFileLocation.replace(DSMTConstants.DOT, DSMTConstants.UNDERSCORE + todaysDate +  DSMTConstants.DOT );
			
		}else{
			
			return null;
		}
		
	}

	/*public static void main(String[] args) {
	
		String originalFileLocation = "/citi/goc/GOC_FK_CORRECTION.csv";
		final String todaysDate = DSMTUtils.getTodaysDate("MM-dd-yyyy-HH-mm-ss");
		String newFileLocation = getNewFileName(originalFileLocation, todaysDate);
		 renameFile(originalFileLocation, newFileLocation);
		
	
		
	}*/
	
	
	private static boolean renameFile(final String originalFileLocation,
			String newFileLocation) {

		boolean isSuccessFullyRenamed = false;

		try {
			final File oldfile = new File(originalFileLocation);
			final File newfile = new File(newFileLocation);

			newfile.setReadable(true, false);
			newfile.setWritable(true, true);
			
			oldfile.setReadable(true, false);
			oldfile.setWritable(true, true);
			
			
			try {
				FileUtils.moveFile(oldfile, newfile);
			} catch (IOException e) {
				
				// wait for 30 seconds and retry..
				Thread.sleep(30000);
				FileUtils.moveFile(oldfile, newfile);
			}
			isSuccessFullyRenamed = true;
			
		} catch (Exception e) {
			LOG.error("Exception " + e + " caught while trying to rename file" );
		}

		return isSuccessFullyRenamed;
	}


	public void setExportFilePath(String exportFilePath) {
		this.exportFilePath = exportFilePath;
	}


	public String getExportFilePath() {
		return exportFilePath;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	
	
}
