package com.citi.ebx.goc.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.citi.ebx.dsmt.jdbc.connector.SQLExportUtil;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

/**
 * 
 * @author nc72931
 *
 */
public class GenerateSQLExportFileTask extends ScriptTaskBean {


	public static final String FILE_NAME_TO_BE_GENERATED = "fileNameToBeGenerated";
	public static final String EXPORT_FILE_DIRECTORY = "exportFileDirectory";
	public static final String SQL_FILE_PATH = "sqlFilePath";
	
	private String sqlFilePath = "/citi/goc/abc.sql";
	private String exportFileDirectory = "/citi/goc";
	private String fileNameToBeGenerated = "tempFile.txt";
	private String headerRow = null;
	
	private String recordCount = "0" ;
	
	private String errorMessage;
	private String errorDetails;
	


	@Override
	public void executeScript(ScriptTaskBeanContext arg0)
			throws OperationException {
		
		LOG.info("GenerateSQLExportFileTask.executeScript - start");
		
		LOG.info("Input Parameters are : {exportFileDirectory = " + exportFileDirectory );
		generateSQLReport(exportFileDirectory, sqlFilePath );
		LOG.info("GenerateSQLExportFileTask.executeScript - end");

	}
	
	public Map<String, Integer> generateSQLReport (final String exportFileDirectoryPath, final String sqlFilePath){
		
		LOG.info("SQL Report generation started !! "); 
		
		Map<String, Integer> sqlReportMap = null;
		
		final SQLExportUtil sqlExportUtil = new SQLExportUtil();
		sqlExportUtil.setReportType(DSMTConstants.GOC);
		
		try{
		
				Map<String, String> sqlExportMap = new HashMap<String, String>();
				sqlExportMap.put(SQL_FILE_PATH, sqlFilePath);
				sqlExportMap.put(EXPORT_FILE_DIRECTORY, exportFileDirectory);
				sqlExportMap.put(FILE_NAME_TO_BE_GENERATED, fileNameToBeGenerated);
				sqlExportMap.put("headerRow", headerRow);
		
			sqlReportMap = sqlExportUtil.generateSQLReport(sqlExportMap);
			
			
			List<String> sqlreportList = new ArrayList<String>(sqlReportMap.keySet());
			
			int numberOfRecordsInResultSet = sqlReportMap.get(sqlreportList.get(0));
			if(numberOfRecordsInResultSet > 0){
			
					this.recordCount = String.valueOf(numberOfRecordsInResultSet);
					this.fileNameToBeGenerated =  sqlreportList.get(0);
			}
			
			LOG.info("Report generated successfully !!!  for  reports :  " + sqlreportList + ", record count set as " + recordCount);	
		
		}
		catch (final Exception e){
			
			LOG.error("Report generation terminated with Exception in GenerateGOCSyncFileTask  -> " + e + ", message = " + e.getMessage());
		}
		
		return sqlReportMap;
	}
	
	
	public String getFileNameToBeGenerated() {
		return fileNameToBeGenerated;
	}

	public void setFileNameToBeGenerated(String fileNameToBeGenerated) {
		this.fileNameToBeGenerated = fileNameToBeGenerated;
	}


	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	

	public String getRecordCount() {
		return recordCount;
	}


	public void setRecordCount(String recordCount) {
		this.recordCount = recordCount;
	}

	

	public String getSqlFilePath() {
		return sqlFilePath;
	}

	public void setSqlFilePath(String sqlFilePath) {
		this.sqlFilePath = sqlFilePath;
	}

	public String getExportFileDirectory() {
		return exportFileDirectory;
	}

	public void setExportFileDirectory(String exportFileDirectory) {
		this.exportFileDirectory = exportFileDirectory;
	}

	public String getHeaderRow() {
		return headerRow;
	}

	public void setHeaderRow(String headerRow) {
		this.headerRow = headerRow;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

}
