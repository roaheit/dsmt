package com.citi.ebx.goc.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.openjpa.lib.util.Files;

import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class ManagerDiffHelperScriptTask extends ScriptTaskBean {

	private String inputFile=null;
	private String targetFolder=null;
	
	
	public String getTargetFolder() {
		return targetFolder;
	}

	public void setTargetFolder(String targetFolder) {
		this.targetFolder = targetFolder;
	}

	public String getInputFile() {
		return inputFile;
	}

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	public static void diff(String file1, String file2, String targetfile) {
		File fileOne = new File(file1);
		File fileTwo = new File(file2);
		HashSet<String> hashSetOne = new HashSet<String>();
		Map<String, String> hashUtil = new HashMap<String, String>();
		try {
			LineIterator itr1 = FileUtils.lineIterator(fileOne);
			LineIterator itr2 = FileUtils.lineIterator(fileTwo);
			while (itr1.hasNext()) {
				hashSetOne.add(createHashkey(itr1.next()));
			}
			LineIterator.closeQuietly(itr1);
			while (itr2.hasNext()) {
				String line = itr2.next();
				String hashKey = createHashkey(line);
				hashUtil.put(hashKey, line);
			}
			LineIterator.closeQuietly(itr2);
			hashUtil.keySet().removeAll(hashSetOne);
			// serializeHashCodes(hashSetTwo);
			
			File diffFile = new File(targetfile);
			
			FileUtils.writeLines(diffFile, hashUtil.values());
			// deserializeHashCodes();
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	public static void diff(String file1, String file2, String targetfile,
			boolean serialize) {
		boolean deserialize = false;

		if (!serialize) {
			diff(file1, file2, targetfile);

		}

		else {
			String fileOneName = file1.substring(0, file1.lastIndexOf("."));

			File fileOne = new File(fileOneName + "_hashCode.ser");
			HashSet<String> hashSetOne = new HashSet<String>();
			if (fileOne.exists()) {
				hashSetOne = deserializeHashCodes(fileOneName + "_hashCode.ser");

			} else {
				fileOne = new File(file1);
				LineIterator itr1 = null;
				try {
					itr1 = FileUtils.lineIterator(fileOne);
					while (itr1.hasNext()) {
						hashSetOne.add(createHashkey(itr1.next()));

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					LineIterator.closeQuietly(itr1);
				}
			}
			File fileTwo = new File(file2);
			Map<String, String> hashUtil = new HashMap<String, String>();
			HashSet<String> hashSetTwo = new HashSet<String>();
			LineIterator itr2 = null;
			try {

				itr2 = FileUtils.lineIterator(fileTwo);
				while (itr2.hasNext()) {
					String line = itr2.next();
					String hashKey = createHashkey(line);
					hashSetTwo.add(hashKey);
					hashUtil.put(hashKey, line);
				}
				LineIterator.closeQuietly(itr2);

				// deserializeHashCodes();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (null != itr2) {
					LineIterator.closeQuietly(itr2);
				}
			}

			hashUtil.keySet().removeAll(hashSetOne);
			serializeHashCodes(fileOneName, hashSetTwo);
			File diffFile = new File(targetfile);
			
			try {
				FileUtils.writeLines(diffFile, hashUtil.values());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static String createHashkey(String text)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(text.getBytes("UTF-8"));

		return new String(hash, "UTF-8");

	}

	private static void serializeHashCodes(String fileOneName,
			Set<String> keyValues) {
		try {
			FileOutputStream fos = new FileOutputStream(fileOneName
					+ "_hashCode.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(keyValues);
			oos.close();
			fos.close();

		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
}
	
		private static HashSet<String> deserializeHashCodes(String fileLocation) {
		HashSet<String> set = null;
		try {
			FileInputStream fis = new FileInputStream(fileLocation);
			ObjectInputStream ois = new ObjectInputStream(fis);
			set = (HashSet<String>) ois.readObject();
			ois.close();
			fis.close();
			Iterator<String> itr = set.iterator();

			return set;
		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		DateFormat df2 = new SimpleDateFormat("MMddyyyy");
		String currentDate = df2.format(new Date());
		File input_File=new File(inputFile);
		String targetFileName=targetFolder+input_File.getName();
		File deltaFile=new File(targetFileName);
		Calendar prevDay = Calendar.getInstance();
		prevDay.add(Calendar.DATE, -1);

		if(deltaFile.exists()){
			StringTokenizer deltaFiletoken=new StringTokenizer(targetFileName,".");
			StringBuffer previousdeltaFile = new StringBuffer(df2.format(prevDay.getTime()));
			StringBuffer prevDayDeltaFileName = new StringBuffer(deltaFiletoken.nextToken()+"_"+previousdeltaFile+ "." +deltaFiletoken.nextToken());
			File prevDayDeltaFile=new File(prevDayDeltaFileName.toString());
			deltaFile.renameTo(prevDayDeltaFile);
		}

		if (input_File.exists()) {

			StringTokenizer inputfileToken1=new StringTokenizer(inputFile, ".");
			
			//Creating backup file		
			StringBuffer backupFileName = new StringBuffer(inputfileToken1.nextToken()+"_"+currentDate+ "." +inputfileToken1.nextToken());
			File movedFile = new File(backupFileName.toString());
			input_File.renameTo(movedFile);
			//Last Day Run File
			StringTokenizer inputfileToken2=new StringTokenizer(inputFile, ".");
			StringBuffer previousrunFile = new StringBuffer(df2.format(prevDay.getTime()));
			StringBuffer prevDayFileName = new StringBuffer(inputfileToken2.nextToken()+"_"+previousrunFile+ "." +inputfileToken2.nextToken());
			File prevDayFile=new File(prevDayFileName.toString());
			
			if(prevDayFile.exists())
			{
				diff(prevDayFileName.toString(),backupFileName.toString(),targetFileName,false);
				try {
					RandomAccessFile file = new RandomAccessFile(targetFileName, "rws");
					byte[] text = new byte[(int) file.length()];
					file.readFully(text);
					file.seek(0);
					file.writeBytes("SetID|MgrID|GDW Emp Name|GDW Emp Status|GDW Term Date|RITS Emp TypeCD|Eff Date\n");
					file.write(text);
					file.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	 
			}
			
			else
			{
				File manager_data_file=new File(targetFileName);

				try {
					Files.copy(movedFile, manager_data_file);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}						
		}	
	}
}
