package com.citi.ebx.goc.task;

import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ProcessInstanceKey;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.WorkflowEngine;

/**
 * This task is to be called from SynchronizeGOCFK workflow in EBX5
 * @author nc72931
 *
 */
public class WorkflowLauncherScheduledTask extends ScheduledTask {

	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();

	private String workFlowName; // eg "ReactivateGOC"
	private String inputVariable; // eg "WF_param Name"
	private String inputVariableOutPut; // eg "WF_param value"

	public String getWorkFlowName() {
		return workFlowName;
	}

	public void setWorkFlowName(String workFlowName) {
		this.workFlowName = workFlowName;
	}

	
	@Override
	public void execute(ScheduledExecutionContext context)	throws OperationException, ScheduledTaskInterruption {

		final Repository repo = context.getRepository();
		final Session session = context.getSession();

		WorkflowEngine wrkflowEngine = WorkflowEngine.getFromRepository(repo, session);
		LOG.info("Workflow - " + workFlowName + " is now being started.. ");
		ProcessLauncher launcher = wrkflowEngine.getProcessLauncher(PublishedProcessKey.forName(workFlowName));
		
		LOG.info("inputVariable : "+inputVariable +"  inputVariableOutPut : "+ inputVariableOutPut);
		if (null != inputVariable) {
			launcher.setInputParameter(inputVariable, inputVariableOutPut);
		}
		
		
		ProcessInstanceKey processKey  = launcher.launchProcess();
		LOG.info("Workflow - " + workFlowName + " has been started successfully.. Process ID " +  processKey);

	}

	public String getInputVariable() {
		return inputVariable;
	}

	public void setInputVariable(String inputVariable) {
		this.inputVariable = inputVariable;
	}

	public String getInputVariableOutPut() {
		return inputVariableOutPut;
	}

	public void setInputVariableOutPut(String inputVariableOutPut) {
		this.inputVariableOutPut = inputVariableOutPut;
	}
	
	

	
}
