package com.citi.ebx.goc.task;

import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.naming.NamingException;

import com.citi.ebx.dsmt.jms.connector.CitiJMSConnector;
import com.citi.ebx.dsmt.jms.connector.impl.CitiJMSConnectorImpl;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class SynchronizeGOCFKTask extends ScheduledTask {

	
	
	private String exportFilePath = "/citi/goc"; // this should be set from the task in EBX configuration
	private String startDate = null; // "2014-05-02 00:00:00";
	private String endDate = null; //"2014-05-02 23:59:59";

	public void execute(ScheduledExecutionContext arg0)	throws OperationException, ScheduledTaskInterruption {

		log.info("SynchronizeGOCFKTask executeScript started");

		final String yesterdaysDate = DSMTUtils.getYesterdaysDate(DSMTConstants.YYYY_MM_DD_HH_MM_SS);
		final String todaysDate = DSMTUtils.getTodaysDate(DSMTConstants.YYYY_MM_DD_HH_MM_SS);
		
		sendMessage(getXMLMessage(yesterdaysDate, todaysDate));

		log.info("SynchronizeGOCFKTask executeScript finished");
	}

	
	public static void main(String[] args) {
	
		String todaysDate = DSMTUtils.getTodaysDate(DSMTConstants.YYYY_MM_DD_HH_MM_SS);
		String yesterdaysDate = DSMTUtils.getYesterdaysDate(DSMTConstants.YYYY_MM_DD_HH_MM_SS);
		
		System.out.println(yesterdaysDate);
		System.out.println(todaysDate);
	}
	
	
	public void sendMessage(String msg) throws OperationException {
		
		final Map<String, String> jmsMap = createJMSMap(startDate);
		
		final String queueCF = propertyHelper.getProp("queue.connection.factory");
		final String sendQueue = propertyHelper.getProp("receive.queue.jndi");
		
		final CitiJMSConnector connector = new CitiJMSConnectorImpl();
		
		try {
			log.info("SynchronizeGOCFKTask: {jmsMap = " + jmsMap + "}, { queueCF = " +  queueCF + "}, {sendQueue = " + sendQueue + "}");
			connector.sendMessage(msg, jmsMap, queueCF, sendQueue);
		} catch (NamingException e) {
			log.error("SynchronizeGOCFKTask: Naming Exception " + e + ", Exception Message " + e.getMessage());
			throw OperationException.createError(e);
		} catch (JMSException e) {
			log.error("SynchronizeGOCFKTask: JMSException Exception " + e + ", Exception Message " + e.getMessage());
			throw OperationException.createError(e);
		}
	}
	
	
	protected Map<String, String> createJMSMap(final String dataSpace) {
		
		final HashMap<String, String> jmsMap = new HashMap<String, String>();
		
		jmsMap.put("JMSCorrelationID", dataSpace);
		
		return jmsMap;
	}
	
	
	private String getXMLMessage(final String startDate, final String endDate){
		
		return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:ebx-schemas:dataservices_1.0\" " +
				"xmlns:sec=\"http://schemas.xmlsoap.org/ws/2002/04/secext\">    <soapenv:Header>       " +
					"<sec:Security>          " +
						"<UsernameToken>             " +
							"<Username>wfadmin</Username>             " +
							"<Password>wfadmin123</Password>          " +
						"</UsernameToken>       " +
					"</sec:Security>    " +
					"</soapenv:Header>    " +
						"<soapenv:Body>       " +
							"<urn:workflowProcessInstanceStart>          " +
								"<publishedProcessKey>SynchronizeGOCs</publishedProcessKey>          " +
								"<documentation>             " +
									"<locale>Locale</locale>                " +
									"<label>Synchronize GOC FKs on " + startDate + "</label>             " +
								"</documentation>          " +
								"<parameters>             " +
									"<parameter>                " +
										"<name>exportFilePath</name>                " +
										"<value>" + exportFilePath		+ "</value>" +
									" </parameter>             " +
									"<parameter>                " +
										"<name>startDate</name>                " +
										"<value>" + startDate		+ "</value>" +
									"</parameter>             " +
									"<parameter>                " +
										"<name>endDate</name>                " +
										"<value>" + endDate		+ "</value>" +
									"</parameter>          " +
								"</parameters>       " +
							"</urn:workflowProcessInstanceStart>    " +
						"</soapenv:Body> " +
					"</soapenv:Envelope>";
	}
	
	
	protected final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	protected LoggingCategory log = LoggingCategory.getWorkflow();


	public String getExportFilePath() {
		return exportFilePath;
	}


	public void setExportFilePath(String exportFileDirectoryPath) {
		this.exportFilePath = exportFileDirectoryPath;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	
	
}
