package com.citi.ebx.goc.task;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.citi.ebx.dsmt.jdbc.connector.GOCUploadUtil;
import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

/**
 * This task is to be called from SynchronizeGOCFK workflow in EBX5
 * @author nc72931
 *
 */
public class GenerateGOCSyncFileTask extends ScriptTaskBean {

	
	private String exportFilePath = "/citi/goc";
	private String startDate = null ; // "2014-05-02 00:00:00"; // SAMPLE
	private String endDate = null ; // "2014-05-02 23:59:59"; // SAMPLE
	private String recordCount = null ;
	private String generatedfileName = null;
	
	@Override
	public void executeScript(ScriptTaskBeanContext arg0)
			throws OperationException {
		
		LOG.info("GenerateGOCSyncFileTask.executeScript - start");
		
		if (! DSMTUtils.isStringNotEmptyOrNull(startDate)) {
			startDate = DSMTUtils.getYesterdaysDate(DSMTConstants.YYYY_MM_DD_HH_MM_SS);
		}
		if (! DSMTUtils.isStringNotEmptyOrNull(endDate)) {
			endDate = DSMTUtils.getTodaysDate(DSMTConstants.YYYY_MM_DD_HH_MM_SS);
		}
		
		LOG.info("Input Parameters are : {startDate = " + startDate + ", endDate = " + endDate + ", exportFilePath = " + exportFilePath );
		generateSQLReport(startDate, endDate, exportFilePath);
		LOG.info("GenerateGOCSyncFileTask.executeScript - end");

	}
	
	public Map<String, Integer> generateSQLReport (final String startDate, final String endDate, final String exportFileDirectoryPath){
		
		LOG.info("GOC Sync File  generation started !! "); 
		
		Map<String, Integer> sqlReportMap = null;
		Connection conn=null;
		
		final GOCUploadUtil connectReportUtil = new GOCUploadUtil();
		connectReportUtil.setReportType(DSMTConstants.GOC);
		connectReportUtil.setStartDate(startDate);
		connectReportUtil.setEndtDate(endDate);
		
		try{
			
			conn = SQLReportConnectionUtils.getConnection();
			sqlReportMap = connectReportUtil.generateReport(conn, exportFileDirectoryPath);
			
			
			List<String> sqlreportList = new ArrayList<String>(sqlReportMap.keySet());
			
			int recordCount = sqlReportMap.get(sqlreportList.get(0));
			if(recordCount > 0){
			
					this.recordCount = String.valueOf(recordCount);
					this.generatedfileName =  sqlreportList.get(0);
			}
			
			LOG.info("Report generated successfully !!!  for  reports :  " + sqlreportList + ", record count set as " + recordCount);	
		
		}
		catch (final Exception e){
			
			LOG.error("Report generation terminated with Exception in GenerateGOCSyncFileTask  -> " + e + ", message = " + e.getMessage());
		}
		
		return sqlReportMap;
	}
	
	
	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String getExportFilePath() {
		return exportFilePath;
	}


	public void setExportFilePath(String exportFileDirectoryPath) {
		this.exportFilePath = exportFileDirectoryPath;
	}


	public String getStartDate() {
		return startDate;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public String getRecordCount() {
		return recordCount;
	}


	public void setRecordCount(String recordCount) {
		this.recordCount = recordCount;
	}

	public void setGeneratedfileName(String generatedfileName) {
		this.generatedfileName = generatedfileName;
	}

	public String getGeneratedfileName() {
		return generatedfileName;
	}


	
}
