package com.citi.ebx.goc.accessrule;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryHandler;

public class LVIDAccessRule implements AccessRule {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path lvidPath = null;
	public static final Path LVID_LEMTable = Path
			.parse("/root/ACC_MAT/LVID_LEM_Mapping");
	public static final Path NAMTable = Path
			.parse("/root/ACC_MAT/ACC_REG/ACC_MS_Hier_NAM");
	public static final Path LATAMTable = Path
			.parse("/root/ACC_MAT/ACC_REG/ACC_MS_Hier_LATAM");
	public static final Path EMEATable = Path
			.parse("/root/ACC_MAT/ACC_REG/ACC_MS_Hier_EMEA");
	public static final Path ASIATable = Path
			.parse("/root/ACC_MAT/ACC_REG/ACC_MS_Hier_ASIA");

	public LVIDAccessRule(SchemaNode tablePath, Path childPath)
			throws OperationException {
		lvidPath = childPath;
	}

	@Override
	public AccessPermission getPermission(Adaptation record, Session session,
			SchemaNode schmnode) {
		
		
		LOG.debug("Adaptation Home ::: "+record.getHome().toString());
		if(record.getHome().toString().contains("Accounting_Matrix") || record.getHome().toString().contains("Accounting Matrix"))
		{
		UserReference userID = session.getUserReference();
		DirectoryHandler directory = session.getDirectory();
		
		String sessionLVID = "";
		if (session.getAttribute("LVID") != null) {
			sessionLVID = session.getAttribute("LVID").toString();
		}
		String recordLvId = record.getString(lvidPath);
		String[] LVIDParts = recordLvId.split("-");
		recordLvId = LVIDParts[0];
		LOG.debug("recordLVID"+recordLvId);
		if (directory.isUserInRole(
				userID,
				Role.forSpecificRole(AccountibilityMatrixConstants.Accountability_Matrix_Admin))) {
			LOG.debug("Admin/ReadOnly Role");
			return AccessPermission.getReadWrite();
		}
		if (directory.isUserInRole(
						userID,
						Role.forSpecificRole(AccountibilityMatrixConstants.Accountability_Matrix_ReadOnly))) {
			LOG.debug("Admin/ReadOnly Role");
			return AccessPermission.getReadOnly();
		}
		if (record.getContainerTable().getTablePath().toString()
				.contains("LVID_LEM_Mapping")) {
			LOG.debug("LEM Tagged");
			return AccessPermission.getReadWrite();
		} 
		if (recordLvId.equals(sessionLVID) || recordLvId == sessionLVID) {
			LOG.debug("LVID matches" + recordLvId + sessionLVID);
			return AccessPermission.getReadWrite();
		} else {
			LOG.debug("LVID doesnt match" + recordLvId + sessionLVID);
			return AccessPermission.getHidden();
		}
	}
		else{
			return AccessPermission.getReadWrite();
		}
	}
}