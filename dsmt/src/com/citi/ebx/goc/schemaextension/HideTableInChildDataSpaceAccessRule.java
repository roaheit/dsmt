package com.citi.ebx.goc.schemaextension;

import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;

public class HideTableInChildDataSpaceAccessRule implements AccessRule {
	private AccessPermission permissionToSet = AccessPermission.getHidden();
	
	// A flag that can be used to allow admins full access
	private boolean adminReadWrite = false;
	
	public HideTableInChildDataSpaceAccessRule() {
	}
	
	public HideTableInChildDataSpaceAccessRule(AccessPermission permissionToSet) {
		this.permissionToSet = permissionToSet;
	}
	
	public AccessPermission getPermissionToSet() {
		return permissionToSet;
	}

	public void setPermissionToSet(AccessPermission permissionToSet) {
		this.permissionToSet = permissionToSet;
	}

	public boolean isAdminReadWrite() {
		return adminReadWrite;
	}

	public void setAdminReadWrite(boolean adminReadWrite) {
		this.adminReadWrite = adminReadWrite;
	}

	@Override
	public AccessPermission getPermission(Adaptation adaptation,
			Session session, SchemaNode node) {
		if (adminReadWrite) {
			if (session.isUserInRole(Role.ADMINISTRATOR)) {
				return AccessPermission.getReadWrite();
			}
		}
		final String sid = GOCWorkflowUtils.getCurrentSID(adaptation);
		if (sid != null) {
			return permissionToSet;
		}
		return AccessPermission.getReadWrite();
	}
}
