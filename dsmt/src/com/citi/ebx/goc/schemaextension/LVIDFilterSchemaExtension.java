package com.citi.ebx.goc.schemaextension;

import com.citi.ebx.goc.accessrule.LVIDAccessRule;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaExtensions;
import com.orchestranetworks.schema.SchemaExtensionsContext;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class LVIDFilterSchemaExtension  implements SchemaExtensions {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	SchemaExtensionsContext ctx=null;
	@Override
	public void defineExtensions(SchemaExtensionsContext context) {
		// TODO Auto-generated method stub

		LOG.debug("Inside define extensions");
		ctx=context;
		registerLVID(context.getSchemaNode());
	}

	private void registerLVID(SchemaNode node) {
        if (node.isTableNode()) {
               for (SchemaNode child : node.getTableOccurrenceRootNode().getNodeChildren()) {
                     Path nodePath = Path.SELF.add(child.getPathInAdaptation());
                     if (nodePath.equals(Path.SELF.add("C_DSMT_LVID"))){
                            try {
                                   ctx.setAccessRuleOnOccurrence(node.getPathInSchema(), new LVIDAccessRule(node, child.getPathInAdaptation()));
                            } catch (OperationException e) {
                                   e.printStackTrace();
                            }
                     }
               }

        } else {
               // Check this nodes children
               for (SchemaNode child : node.getNodeChildren()) {
                     registerLVID(child);
               }

        }
 }
}
