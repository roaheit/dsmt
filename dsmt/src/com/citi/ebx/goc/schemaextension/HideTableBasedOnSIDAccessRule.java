package com.citi.ebx.goc.schemaextension;

import com.citi.ebx.goc.GOCConstants;
import java.util.HashSet;
import java.util.Set;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.IGWGOCWorkflowUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;

/**
 * Hide a table based on whether the SID enhancement table says that it should be shown
 */
public class HideTableBasedOnSIDAccessRule implements AccessRule {
	// A flag that can be used to allow admins full access
	private boolean adminReadWrite = false;
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	
	private static final Set<Path> ALWAYS_HIDE_PATHS = new HashSet<Path>();
	static {
		
		ALWAYS_HIDE_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD.getPathInSchema());
		ALWAYS_HIDE_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD.getPathInSchema());
		
		ALWAYS_HIDE_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_FRS_BU_CHILD.getPathInSchema());
		
		ALWAYS_HIDE_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_FRS_OU_CHILD.getPathInSchema());
		
		ALWAYS_HIDE_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_Function_CHILD.getPathInSchema());
		
		ALWAYS_HIDE_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_LVID_CHILD.getPathInSchema());
		
		ALWAYS_HIDE_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_GOC_USE_CHILD.getPathInSchema());
		
		ALWAYS_HIDE_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_SID_CHILD.getPathInSchema());
		
		
	}
	
	

	public boolean isAdminReadWrite() {
		return adminReadWrite;
	}

	public void setAdminReadWrite(boolean adminReadWrite) {
		this.adminReadWrite = adminReadWrite;
	}

	@Override
	public AccessPermission getPermission(Adaptation adaptation,
			Session session, SchemaNode node) {
		if (adminReadWrite) {
			if (session.isUserInRole(Role.ADMINISTRATOR)) {
				return AccessPermission.getReadWrite();
			}
		}
		final String sid = IGWGOCWorkflowUtils.getCurrentSID(adaptation);
		LOG.debug("SID retrieved in HideTableBasedOnSIDAccessRule = " + sid);
		final String maintenanceType = IGWGOCWorkflowUtils.getMaintenanceType(adaptation);
		
		if (GOCConstants.MAINTENANCE_TYPE_FUNCTIONAL.equalsIgnoreCase(maintenanceType)){
			if(ALWAYS_HIDE_PATHS.contains(node.getPathInSchema())){
				return AccessPermission.getHidden();
			}else{
				return AccessPermission.getReadWrite();
			}
		}
		// Will be null if not in workflow
		if (sid == null || "".equals(sid)) {
			return AccessPermission.getReadWrite();
		}
		// If table should be shown for this SID, make it read/write. Otherwise it's hidden.
		String managementOnly= IGWGOCWorkflowUtils.getManagementOnly(adaptation);
		
		if("true".equalsIgnoreCase(managementOnly)){
			return AccessPermission.getReadWrite();
		}
		else{
			if (isTableShownForSID(adaptation.getHome().getRepository(),
					node.getPathInSchema(), sid)) {
				return AccessPermission.getReadWrite();
			}
			return AccessPermission.getHidden();
		}
		
	}
	
	private static boolean isTableShownForSID(final Repository repo, final Path tablePath, final String sid) {
		final AdaptationHome metadataDataSpace = repo.lookupHome(
				HomeKey.forBranchName(GOCConstants.METADATA_DATA_SPACE));
		final Adaptation metadataDataSet = metadataDataSpace.findAdaptationOrNull(
				AdaptationName.forName(GOCConstants.METADATA_DATA_SET));
		
		final AdaptationTable sidEnhancementTable = metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
		// Query for a record with same SID & table path
		final RequestResult reqRes = sidEnhancementTable.createRequestResult(
				GOCWFPaths._C_DSMT_SID_ENH._SID.format() + "='" + sid
				+ "' and (" + GOCWFPaths._C_DSMT_SID_ENH._GLTable.format() + "='" + tablePath.format()
				+ "' or " + GOCWFPaths._C_DSMT_SID_ENH._P2PTable.format() + "='" + tablePath.format()
				+ "' or " + GOCWFPaths._C_DSMT_SID_ENH._RptEngineTable.format() + "='" + tablePath.format() + "')");
		
		
		// If a record was found, then the table should be shown for this SID
		return ! reqRes.isEmpty();
	}
}
