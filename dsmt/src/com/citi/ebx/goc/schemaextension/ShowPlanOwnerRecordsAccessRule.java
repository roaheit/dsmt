package com.citi.ebx.goc.schemaextension;

import java.util.ArrayList;
import java.util.List;

import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;

public class ShowPlanOwnerRecordsAccessRule implements AccessRule {

	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	//private static final Path SETID_PATH = Path.parse("./SETID");
	private static final Path CBL_CD_PATH = Path.parse("./CBL_CD");
	private static final Path CO_CD_PATH = Path.parse("./CO_CD");
	private static final Path MLE_CD_PATH = Path.parse("./MLE_CD");
	private static final Path GF_CD_PATH = Path.parse("./GF_CD");
	
	private static final Path PLAN_ID_PATH = Path.parse("./PLAN_ID");
	
	
	private Path userPlanTablePath = Path.parse("/root/ENTITLEMENTS/USER_PLANS");
	private Path SoeID = Path.parse("./SOEID");

	@Override
	public AccessPermission getPermission(Adaptation adaptation, Session session, SchemaNode node) {
		UserReference user=session.getUserReference();
		String Soeid = session.getUserReference().getUserId();
		LOG.info("inside getPermission");
		
		List<String> CBL_CD=adaptation.getList(CBL_CD_PATH);
		
		List<String> CO_CD=adaptation.getList(CO_CD_PATH);
		
		List<String> MLE_CD=adaptation.getList(MLE_CD_PATH);
		
		List<String> GF_CD=adaptation.getList(GF_CD_PATH);
		
		Role role=Role.forSpecificRole("Rosetta_Plan_Owner");
		
		List<String> plans = new ArrayList<String>();
		
		Repository repo = adaptation.getHome().getRepository();
		Adaptation metadataDataSet;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(repo,"Rosetta", "Rosetta");
			final AdaptationTable targetTable = metadataDataSet.getTable(userPlanTablePath);
			
			String predicate = SoeID.format() + "= \"" + Soeid+"\"";
			
			RequestResult reqRes = targetTable.createRequestResult(predicate);
			for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
				String planID =  record.getString(Path.SELF.add(PLAN_ID_PATH));
				plans.add(planID);
			}
		
		} catch (OperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		boolean isHidden = true;
		if(session.getDirectory().isUserInRole(user, role)){
			LOG.info("user is in role " );
			for(String tablePlan :CBL_CD){
				if("NO_CBL".equalsIgnoreCase(tablePlan)){
					isHidden = true;
				}else{
				if(plans.contains(tablePlan)){
					return AccessPermission.getReadWrite();
				}else{
					isHidden = true;
				}
			 }
			}
			for(String tablePlan :CO_CD){
				if("NO_CO".equalsIgnoreCase(tablePlan)){
					isHidden = true;
				}else{
				if(plans.contains(tablePlan)){
					return AccessPermission.getReadWrite();
				}else{
					isHidden = true;
				}
			 }	
			}
				
			
			for(String tablePlan :MLE_CD){
				if("NO_MLE".equalsIgnoreCase(tablePlan)){
					isHidden = true;
				}else{
				
				if(plans.contains(tablePlan)){
					return AccessPermission.getReadWrite();
				}else{
					isHidden = true;
				}
			 }
			}
			
			for(String tablePlan :GF_CD){
				if("NO_GF".equalsIgnoreCase(tablePlan)){
					isHidden = true;
					
				}else{
				if(plans.contains(tablePlan)){
					return AccessPermission.getReadWrite();
					
				}else{
					isHidden = true;
					
				}
			 }
			}
			
			if(isHidden){
				return AccessPermission.getHidden();
			}else{
				return AccessPermission.getReadWrite();
			}
			
			}
		
		
			else{
				return AccessPermission.getReadWrite();
				//	return AccessPermission.getHidden();
			}
	}
}
