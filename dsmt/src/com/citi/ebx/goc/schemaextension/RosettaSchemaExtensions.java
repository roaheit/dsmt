package com.citi.ebx.goc.schemaextension;

import org.apache.soap.providers.com.Log;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaExtensions;
import com.orchestranetworks.schema.SchemaExtensionsContext;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.LoggingCategory;

public class RosettaSchemaExtensions implements SchemaExtensions {

	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	//public static final Path PATHOFTABLE = Path.parse("/root/GLOBAL_STD/PMF_STD/F_ACC_STD/TEST_ACCT_TBLE");
	public static final Path PATHOFTABLE = Path.parse("/root/MANUAL_TAGGING/KEY_MGT_EMP");
	public static final Path Reports_Table_Path = Path.parse("/root/MANUAL_TAGGING/RP_KEY_MGT_REPORTS");
	
	@Override
	public void defineExtensions(SchemaExtensionsContext schemaContext) {
		LOG.info("inside defineExtension");
		applyRuleToRecords(schemaContext.getSchemaNode().getNodeChildren(), schemaContext, new ShowPlanOwnerRecordsAccessRule(), new ShowPlanRecordForEmployeeAccessRule());

	}

	private void applyRuleToRecords(SchemaNode[] nodeChildren, SchemaExtensionsContext schemaContext,
			ShowPlanOwnerRecordsAccessRule showPlanOwnerRecordsAccessRule, ShowPlanRecordForEmployeeAccessRule showPlanRecordForEmployeeAccessRule) {
			for (int i = 0; i < nodeChildren.length; i++) {
			final SchemaNode node = nodeChildren[i];
			LOG.info("node" + node.toString());
			
			if (node.isTableNode()) {
				final Path nodePath = node.getPathInSchema();
				LOG.info("compiling now for:" + nodePath);
				if ((node.getPathInSchema().equals(Reports_Table_Path)) ) {
					LOG.info("match found");
					schemaContext.setAccessRuleOnOccurrence(Reports_Table_Path, showPlanOwnerRecordsAccessRule);
					
				}
				if( (node.getPathInSchema().equals(PATHOFTABLE))){
					schemaContext.setAccessRuleOnOccurrence(PATHOFTABLE, showPlanRecordForEmployeeAccessRule);
				}
			} else {
				applyRuleToRecords(node.getNodeChildren(), schemaContext, showPlanOwnerRecordsAccessRule, showPlanRecordForEmployeeAccessRule);
			}
			// TODO Auto-generated method stub

		}
	}

}
