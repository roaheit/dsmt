package com.citi.ebx.goc.schemaextension;

import java.util.HashSet;
import java.util.Set;

import com.citi.ebx.goc.path.GOCPaths;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaExtensions;
import com.orchestranetworks.schema.SchemaExtensionsContext;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessRule;

public class GOCSchemaExtensions implements SchemaExtensions {
	// These are the paths that should always be shown
	private static final Set<Path> ALWAYS_SHOW_PATHS = new HashSet<Path>();
	static {
		ALWAYS_SHOW_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC.getPathInSchema());
	
	}
	
	// These are the paths that should be hidden based on what the SID is
	private static final Set<Path> HIDE_BASED_ON_SID_PATHS = new HashSet<Path>();
	static {
		
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_ASIA.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_JAPAN.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_WEUROPE.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_CEECA.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_Flexcube.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_FMS.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_TGL.getPathInSchema());
		
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_CMI.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_CCC01.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_CMN01.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_CRCBS.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_DAU.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_P2P_C_PTNR_P2P_AESM.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_P2P_C_PTNR_P2P_NAM.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_ALFA_PMBB.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_ALFA_PMBNB.getPathInSchema());
		
		
		
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD.getPathInSchema());
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD.getPathInSchema());
		
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_FRS_BU_CHILD.getPathInSchema());
		
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_FRS_OU_CHILD.getPathInSchema());
		
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_Function_CHILD.getPathInSchema());
		
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_LVID_CHILD.getPathInSchema());
		
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_GOC_USE_CHILD.getPathInSchema());
		
		HIDE_BASED_ON_SID_PATHS.add(
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_SID_CHILD.getPathInSchema());
	}
	
	@Override
	public void defineExtensions(SchemaExtensionsContext context) {
		// Apply rules to all child nodes of the root.
		// This will recursively go through all of those children.
		applyRulesToNodes(context.getSchemaNode().getNodeChildren(), context,
				new HideTableBasedOnSIDAccessRule(),
				new HideTableInChildDataSpaceAccessRule());
	}
	
	private static final void applyRulesToNodes(final SchemaNode[] nodes, final SchemaExtensionsContext context,
			final AccessRule hideBasedOnSIDRule, final AccessRule hideInChildDataSpaceRule) {
		for (int i = 0; i < nodes.length; i++) {
			final SchemaNode node = nodes[i];
			// If this is a table node
			if (node.isTableNode()) {
				final Path nodePath = node.getPathInSchema();
				if (! ALWAYS_SHOW_PATHS.contains(nodePath)) {
					// Set the appropriate rule
					if (HIDE_BASED_ON_SID_PATHS.contains(nodePath)) {
						context.setAccessRuleOnNode(nodePath, hideBasedOnSIDRule);
					} else {
						context.setAccessRuleOnNode(nodePath, hideInChildDataSpaceRule);
					}
				}
			// Otherwise look through all children of this node for table nodes
			} else {
				applyRulesToNodes(node.getNodeChildren(), context, hideBasedOnSIDRule,
						hideInChildDataSpaceRule);
			}
		}
	}
}
