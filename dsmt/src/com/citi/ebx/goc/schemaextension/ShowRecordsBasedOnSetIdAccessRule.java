package com.citi.ebx.goc.schemaextension;

import com.citi.ebx.goc.path.GOCPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;

public class ShowRecordsBasedOnSetIdAccessRule implements AccessRule {
	@Override
	public AccessPermission getPermission(Adaptation adaptation, Session session,
			SchemaNode node) {
		final String setId = adaptation.getString(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._SETID);
		final UserReference user = session.getUserReference();
		final Role role = getRoleForSetId(setId);
		return session.getDirectory().isUserInRole(user, role)
			? AccessPermission.getReadWrite()
			: AccessPermission.getHidden();
	}

	private static final Role getRoleForSetId(final String setId) {
		return Role.forSpecificRole("DSMT_GOC_" + setId);
	}
}
