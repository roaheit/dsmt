package com.citi.ebx.goc.trigger;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.citi.ebx.dsmt.trigger.HierarchyTrigger;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.goc.util.IGWGOCWorkflowUtils;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.UpdateGOCWFDtlTable;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class GOCTrigger extends HierarchyTrigger {
	// Assuming the field name in each sub-table will be same as main GOC table
	private static final Path GOC_ID_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC;
	String goc_rep_Status = "", childDataSpaceId = "";
	private static final Set<Path> SUBTABLE_PATHS = new HashSet<Path>();
	static {

		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_ASIA
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_JAPAN
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_WEUROPE
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_CEECA
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_Flexcube
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_FMS
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_TGL
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_CMN01
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_CCC01
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_DAU
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_CRCBS
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_CMI
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_P2P_C_PTNR_P2P_AESM
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_P2P_C_PTNR_P2P_NAM
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_ALFA_PMBB
						.getPathInSchema());
		SUBTABLE_PATHS
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_ALFA_PMBNB
						.getPathInSchema());
	}

	// Assuming the foreign key field name in each sub-table will be same,
	// so just picking one of the tables here to get the field from
	private static final Path GOC_FK_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM._C_GOC_FK;

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {

		super.handleAfterCreate(context);

		/*
		 * final Repository repo = context.getAdaptationHome().getRepository();
		 * final AdaptationHome targetDataSpace = repo.lookupHome(
		 * HomeKey.forBranchName(GOCConstants.DSMT2_HIERARCHY_DATA_SPACE));
		 * final Adaptation targetDataSet =
		 * targetDataSpace.findAdaptationOrNull(
		 * AdaptationName.forName(GOCConstants.DSMT2_HIERARCHY_DATA_SET)); final
		 * AdaptationTable targetTable =
		 * targetDataSet.getTable(DSMT2HierarchyPaths
		 * ._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY.getPathInSchema());
		 * 
		 * final ProcedureContext pContext = context.getProcedureContext();
		 * pContext.setAllPrivileges(true);
		 * pContext.setTriggerActivation(false); Adaptation record =
		 * context.getAdaptationOccurrence();
		 * 
		 * copyRecord(record, pContext, targetTable);
		 * 
		 * pContext.setTriggerActivation(true);
		 * pContext.setAllPrivileges(false);
		 */

	}

	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context)
			throws OperationException {
		// code to update the GOC data to GOC reporting table when data got
		// updated by GL/RE/P2p maintenance team.
		// Code written by fv66203

		AdaptationHome chilDataSpace = context.getAdaptationHome();
		Adaptation record = context.getAdaptationOccurrence();
		String maintenaceType = IGWGOCWorkflowUtils.getMaintenanceType(record
				.getContainer());
		String tableID = GOCWFPaths._C_GOC_WF_DTL.getPathInSchema().format();
		if (isInChildDataSpace(chilDataSpace) && (null != maintenaceType)) {

			UpdateGOCWFDtlTable updateGOCWFDtlTable = new UpdateGOCWFDtlTable(
					record, record.getHome().getRepository(), tableID,
					GOCConstants.METADATA_DATA_SPACE,
					GOCConstants.METADATA_DATA_SPACE);
			Utils.executeProcedure(
					updateGOCWFDtlTable,
					context.getSession(),
					record.getHome()
							.getRepository()
							.lookupHome(
									HomeKey.forBranchName(GOCConstants.METADATA_DATA_SPACE)));
		}
		super.handleAfterModify(context);

	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		context.setAllPrivileges();
		Adaptation record = context.getAdaptationOccurrence();
		String GOC_pmkey = record.getOccurrencePrimaryKey().format().toString();
		AdaptationHome chilDataSpace = context.getAdaptationHome();

		String maintenaceType = IGWGOCWorkflowUtils.getMaintenanceType(record
				.getContainer());
		if (isInChildDataSpace(chilDataSpace) && (null != maintenaceType)) {
			if (!editChildDataSpace(record.getHome().getRepository(),
					chilDataSpace))
				throw OperationException
						.createError("User is not allowed to update data in child dataspace is in approval queue");

			// To check and update child D.Space record based on request status.
			/*
			 * if(!chkChildDataSpace(record.getHome().getRepository(),
			 * chilDataSpace, GOC_pmkey)) throw OperationException.createError(
			 * "User is not allowed to update GOC record "
			 * +GOC_pmkey+" as it's status is "+goc_rep_Status);
			 */

			// To check and update Functional Id value
			checkFunctionalId(record.getHome().getRepository(), GOC_pmkey,
					context, maintenaceType);

		}
		//code fix for GOC Direct maintenance
		super.handleBeforeModify(context);
	}

	@Override
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
		// To restrict the delete a record when it's status is in pending for
		// approval
		Adaptation record = context.getAdaptationOccurrence();
		String GOC_pmkey = record.getOccurrencePrimaryKey().format().toString();
		AdaptationHome chilDataSpace = context.getAdaptationHome();
		String maintenaceType = IGWGOCWorkflowUtils.getMaintenanceType(record
				.getContainer());
		if (isInChildDataSpace(chilDataSpace) && (null != maintenaceType)) {
			/*
			 * if(!chkChildDataSpace(record.getHome().getRepository(),
			 * chilDataSpace, GOC_pmkey)) throw OperationException.createError(
			 * "User is not allowed to delete GOC record "
			 * +GOC_pmkey+" as it's status is "+goc_rep_Status);
			 */
		}
	}

	private static boolean isInChildDataSpace(final AdaptationHome dataSapce) {

		LOG.debug("isInChildDataSpace called ");

		if (dataSapce.toString().contains("GOC")) {
			LOG.debug("Is in master DataSpace");
			return false;
		} else {
			LOG.debug("Is in Child DataSpace");
			return true;
		}
	}

	@SuppressWarnings("deprecation")
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {

		final ValueContextForUpdate vc = context
				.getOccurrenceContextForUpdate();
		Adaptation record = vc.getAdaptationInstance();

		AdaptationHome chilDataSpace = context.getAdaptationHome();
		String userID = context.getSession().getUserReference().getUserId();
		Path requestedByFieldPath = DSMTConstants.requestedByFieldPath;
		Path requestedDateFieldPath = DSMTConstants.requestedDateFieldPath;

		/** Capitalize the userID */
		if (null != userID) {
			userID = userID.toUpperCase();
		}

		// handle webservice update scenarios
		if (null != userID) {
			vc.setValue(userID, requestedByFieldPath);
		}

		if (!editChildDataSpace(record.getHome().getRepository(), chilDataSpace)) {

			throw OperationException
					.createError("User are not allowed to update data in child dataspace as it's pending in approval queue.");
		}
		Date now = new Date();
		vc.setValue(now, requestedDateFieldPath);

		super.handleBeforeCreate(context);
	}

	public boolean editChildDataSpace(Repository repo,
			AdaptationHome childDataspace) {

		Adaptation metadataDataSet;
		boolean status = false;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(repo, "GOC_WF",
					"GOC_WF");

			final AdaptationTable targetTable = metadataDataSet
					.getTable(GOCWFPaths._C_GOC_WF_REPORTING.getPathInSchema());
			String childDataSpace = childDataspace.getKey().getName();

			StringBuffer predicate = new StringBuffer(
					"osd:contains-case-insensitive("
							+ GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID
									.format() + ",'" + childDataSpace + "')");

			RequestResult rs = targetTable.createRequestResult(predicate
					.toString());
			Adaptation record = rs.nextAdaptation();
			if (record != null) {
				String reqStatus = record
						.getString(GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);

				if (reqStatus.equalsIgnoreCase(GOCConstants.CREATED)
						|| reqStatus.contains("Rework")
						|| reqStatus.contains("MAINTENANCE")) {
					status = true;
				}

			} else {
				status = true;
			}

		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return status;
	}

	protected static final void updateSubtables(final PrimaryKey gocPK,
			final String gocId, final Adaptation dataSet,
			final ProcedureContext pContext) throws OperationException {
		final String predicate = GOC_FK_PATH.format() + "=\"" + gocPK.format()
				+ "\"";
		for (Path subtablePath : SUBTABLE_PATHS) {
			final AdaptationTable subtable = dataSet.getTable(subtablePath);
			final RequestResult reqRes = subtable
					.createRequestResult(predicate);
			final Adaptation subRecord;
			try {
				subRecord = reqRes.nextAdaptation();
			} finally {
				reqRes.close();
			}
			if (subRecord != null) {
				final ValueContextForUpdate vc = pContext.getContext(subRecord
						.getAdaptationName());
				vc.setValue(gocId, GOC_ID_PATH);
				pContext.setAllPrivileges(true);
				pContext.setTriggerActivation(false);
				pContext.doModifyContent(subRecord, vc);
				pContext.setTriggerActivation(true);
				pContext.setAllPrivileges(false);
			}
		}
	}

	protected void copyRecord(final Adaptation record,
			final ProcedureContext pContext, final AdaptationTable targetTable)
			throws OperationException {
		final Object setId = record
				.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._SETID);
		final Object goc = record
				.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC);
		final Object effDate = record
				.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFFDT);
		final PrimaryKey pk = targetTable.computePrimaryKey(new Object[] {
				setId, goc, effDate });
		Adaptation targetRecord = targetTable.lookupAdaptationByPrimaryKey(pk);

		final ValueContextForUpdate vc;
		if (targetRecord == null) {
			vc = pContext.getContextForNewOccurrence(targetTable);
			vc.setValue(
					setId,
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._SETID);
			vc.setValue(
					goc,
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_GOC_ID);
			vc.setValue(
					effDate,
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._EFFDT);
		} else {
			vc = pContext.getContext(targetRecord.getAdaptationName());
		}
		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFF_STATUS),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._EFF_STATUS);

		final Adaptation mgrRecord = Utils
				.getLinkedRecord(
						record,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._MANAGER_ID);
		if (mgrRecord != null) {
			vc.setValue(
					mgrRecord
							.get(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._MANAGER_ID),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._MANAGER_ID);
		}

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_SID_FK);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_FRS_BU_FK);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_OU),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_FRS_OU_FK);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_MAN_GEO_FK);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_MAN_SEG_FK);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FUNCTION),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_FUNCTION_FK);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LVID),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_LVID_FK);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._HEADCOUNT_GOC),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._HEADCOUNT_GOC);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCRSHORT),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._DESCRSHORT);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCR),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._DESCR);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_USAGE),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_GOC_USAGE_FK);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_LOC_COST_CD);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_COMMENT),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_GOC_COMMENT);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_USAGE),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_GOC_USAGE_FK);

		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CREATED_OPERID),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._CREATED_OPERID);
		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ORIG_DTTM),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._ORIG_DTTM);
		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LASTUPDOPRID),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._LASTUPDOPRID);
		vc.setValue(
				record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CHNG_DTTM),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._CHNG_DTTM);

		vc.setValue(
				Calendar.getInstance().getTime(),
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._MERGE_DATE);

		if (targetRecord == null) {
			targetRecord = pContext.doCreateOccurrence(vc, targetTable);
		} else {
			targetRecord = pContext.doModifyContent(targetRecord, vc);
		}
	}

	/*
	 * Method to restrict user to modiify any record in new WF Request It checks
	 * whether current record is in either in Pending for Approval or Rework
	 * status in other WF Requests and restrict user to modify respective
	 * record.
	 */
	public boolean chkChildDataSpace(Repository repo,
			AdaptationHome childDataspace, String GOC_pmkey) {
		// To restrict a record while updating in child dataspace on specific
		// params..
		Adaptation metadataDataSet = null, record = null, recordDTL = null;
		AdaptationTable targetTable = null, targetTableDTL = null;
		RequestResult rs = null, rsDTL = null;
		boolean status = true;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(repo, "GOC_WF",
					"GOC_WF");
			childDataSpaceId = childDataspace.getKey().getName();
			targetTableDTL = metadataDataSet.getTable(GOCWFPaths._C_GOC_WF_DTL
					.getPathInSchema());
			String predicate = GOCWFPaths._C_GOC_WF_DTL._GOC_PK.format()
					+ "=\"" + GOC_pmkey + "\"";
			rsDTL = targetTableDTL.createRequestResult(predicate.toString());
			recordDTL = rsDTL.nextAdaptation();
			if (null != recordDTL) {
				String reqId = recordDTL
						.getString(GOCWFPaths._C_GOC_WF_DTL._REQUEST_ID);
				String predicat = GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_ID
						.format() + "=\"" + reqId + "\"";
				targetTable = metadataDataSet
						.getTable(GOCWFPaths._C_GOC_WF_REPORTING
								.getPathInSchema());
				rs = targetTable.createRequestResult(predicat.toString());
				record = rs.nextAdaptation();
				if (record != null) {
					goc_rep_Status = record
							.getString(GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
					if (goc_rep_Status.contains("Pending")
							|| goc_rep_Status.contains("Rework"))
						status = false;
				}
			}
		} catch (OperationException e) {
			e.printStackTrace();
		} finally {
			if (recordDTL != null)
				recordDTL = null;
			if (rsDTL != null)
				rsDTL = null;
			if (targetTableDTL != null)
				targetTableDTL = null;
			if (record != null)
				record = null;
			if (rs != null)
				rs = null;
			if (targetTable != null)
				targetTable = null;
			if (metadataDataSet != null)
				metadataDataSet = null;
		}
		return status;
	}

	/*
	 * check value of functional ID of current record and previous For
	 * Functional WF - Value should change For Regular WF - IF value is changed,
	 * new value should be N/A
	 */
	public void checkFunctionalId(Repository repo, String GOC_pmkey,
			BeforeModifyOccurrenceContext context, String maintainenceType)
			throws OperationException {
		final AdaptationTable table = context.getTable();

		String originalValue;
		String changedValue;

		Object currentFunctionalId = context
				.getOccurrenceContextForUpdate()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Functional_ID);

		if (null != currentFunctionalId) {

			final AdaptationTable initialTable = GOCWorkflowUtils
					.getTableInInitialSnapshot(table);

			// Get the record in the initial snapshot, to compare
			// against
			final Adaptation initialRecord = initialTable
					.lookupAdaptationByPrimaryKey(context
							.getOccurrenceContextForUpdate());

			if (null != initialRecord) {

				Object parentDSFunctionId = initialRecord
						.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Functional_ID);

				LOG.info("check parent DS Function ID : " + parentDSFunctionId);

				originalValue = (parentDSFunctionId == null) ? ""
						: parentDSFunctionId.toString();

				changedValue = (currentFunctionalId == null) ? ""
						: currentFunctionalId.toString();

				if (maintainenceType
						.equalsIgnoreCase(GOCConstants.MAINTENANCE_TYPE_FUNCTIONAL)) {

					if (originalValue.equalsIgnoreCase(changedValue))

						throw OperationException
								.createError("Please change Functional ID value for GOC: Functional Maintenance.");
				}

				else {

					if (!originalValue.equalsIgnoreCase(changedValue)
							&& (!changedValue.equalsIgnoreCase("N/A")))
						throw OperationException
								.createError("Please change Functional ID value as"
										+ "N/A"
										+ " for GOC: Regular Maintenance.");

				}
			}

		} else {
			LOG.info(" Current Function ID is null.");

			if (maintainenceType
					.equalsIgnoreCase(GOCConstants.MAINTENANCE_TYPE_FUNCTIONAL)) {

				throw OperationException
						.createError("Please change Functional ID value for GOC: Functional Maintenance.");

			}

		}

	}

}
