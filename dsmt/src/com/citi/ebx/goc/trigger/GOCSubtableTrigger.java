package com.citi.ebx.goc.trigger;

import java.util.Date;

import com.citi.ebx.goc.path.GOCPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.schema.trigger.ValueChange;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class GOCSubtableTrigger extends TableTrigger {
	private static final Path GOC_TABLE_PATH =
			GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC.getPathInSchema();
	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private static Path creationUserFieldPath = Path.parse("./CREATED_OPERID");
	private static Path creationDateFieldPath = Path.parse("./ORIG_DTTM");
	private static Path lastUpdateUserFieldPath = Path.parse("./LASTUPDOPRID");
	private static Path lastUpdateDateFieldPath = Path.parse("./CHNG_DTTM");
	
	
	
	public String getCreationUserFieldPath() {
		return this.creationUserFieldPath.format();
	}
	
	public void setCreationUserFieldPath(String creationUserFieldPath) {
		this.creationUserFieldPath = Path.parse(creationUserFieldPath);
	}
	
	public String getCreationDateFieldPath() {
		return creationDateFieldPath.format();
	}

	public void setCreationDateFieldPath(String creationDateFieldPath) {
		this.creationDateFieldPath = Path.parse(creationDateFieldPath);
	}

	public String getLastUpdateUserFieldPath() {
		return lastUpdateUserFieldPath.format();
	}

	public void setLastUpdateUserFieldPath(String lastUpdateUserFieldPath) {
		this.lastUpdateUserFieldPath = Path.parse(lastUpdateUserFieldPath);
	}

	public String getLastUpdateDateFieldPath() {
		return lastUpdateDateFieldPath.format();
	}

	public void setLastUpdateDateFieldPath(String lastUpdateDateFieldPath) {
		this.lastUpdateDateFieldPath = Path.parse(lastUpdateDateFieldPath);
	}
	
	// Assuming the field name in each sub-table will be same as main GOC table
	private static final Path GOC_ID_PATH =
			GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC;
	// Assuming the foreign key field name in each sub-table will be same,
	// so just picking one of the tables here to get the field from
	private static final Path GOC_FK_PATH =
			GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM._C_GOC_FK;
	
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		updateGOCForeignKey(context.getOccurrenceContextForUpdate(),
				context.getTable().getContainerAdaptation(), context.getSession().getUserReference()
				.getUserId());
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		final ValueChange gocIdChange = context.getChanges().getChange(GOC_ID_PATH);
		if (gocIdChange != null) {
			updateGOCForeignKey(context.getOccurrenceContextForUpdate(),
					context.getTable().getContainerAdaptation(),context.getSession().getUserReference()
					.getUserId());
		}
	}

	@Override
	public void setup(TriggerSetupContext context) {
		// do nothing
	}
	
	private static void updateGOCForeignKey(final ValueContextForUpdate vc,
			final Adaptation dataSet ,String userID) throws OperationException {
		final String gocId = (String) vc.getValue(GOC_ID_PATH);
		if (gocId != null) {
			final Adaptation gocRecord = lookupGOCRecord(dataSet, gocId);
			if (gocRecord == null) {/*
				throw OperationException.createError(
						"No record exists for GOC " + gocId);*/
				LOG.info("No record exists for GOC " + gocId );
				vc.setValue("", GOC_FK_PATH);
				
			}
			else{
			vc.setValue(gocRecord.getOccurrencePrimaryKey().format(), GOC_FK_PATH);
			}
			
			
			
		}
		if (userID != null) {
		updateAuditColumns(vc,userID );
		}
	}
	
	private static Adaptation lookupGOCRecord(final Adaptation dataSet, final String gocId) {
		final AdaptationTable gocTable = dataSet.getTable(GOC_TABLE_PATH);
		final RequestResult reqRes = gocTable.createRequestResult(
				GOC_ID_PATH.format() + "=\"" + gocId + "\"");
		final Adaptation gocRecord;
		try {
			gocRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		return gocRecord;
	}
	private static void updateAuditColumns (final ValueContextForUpdate vc, String userID) {
		
		
		
		vc.setValue(userID, creationUserFieldPath);
		vc.setValue(userID, lastUpdateUserFieldPath);
		Date now = new Date();
		vc.setValue(now, creationDateFieldPath);
		vc.setValue(now, lastUpdateDateFieldPath);
		
	}
	
}
