package com.citi.ebx.goc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.instance.HomeCreationSpec;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.service.extensions.LockSpec;

/**
 * Copies data from one data space to new sibling data spaces
 */
public class CopyIntoSiblingDataSpaces {
	private static final String DATASPACE_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS";
	
	private Set<AdaptationTable> tables;
	private String dataSpaceNamePrefix;
	private Path deciderField;
	private AdaptationHome dataSpaceToCopyPermissionsFrom;
	
	/**
	 * Get the tables to copy
	 * 
	 * @return the tables to copy
	 */
	public Set<AdaptationTable> getTables() {
		return tables;
	}

	/**
	 * Set the tables to copy
	 * 
	 * @param tables the tables to copy
	 */
	public void setTables(Set<AdaptationTable> tables) {
		this.tables = tables;
	}

	/**
	 * Get the prefix for each data space created
	 * 
	 * @return the prefix
	 */
	public String getDataSpaceNamePrefix() {
		return dataSpaceNamePrefix;
	}

	/**
	 * Set the prefix for each data space created
	 * 
	 * @param dataSpaceNamePrefix the prefix
	 */
	public void setDataSpaceNamePrefix(String dataSpaceNamePrefix) {
		this.dataSpaceNamePrefix = dataSpaceNamePrefix;
	}

	/**
	 * Get the field that decides which data space the record goes into
	 * 
	 * @return the field that decides which data space the record goes into
	 */
	public Path getDeciderField() {
		return deciderField;
	}

	/**
	 * Set the field that decides which data space the record goes into
	 * 
	 * @param deciderField the field that decides which data space the record goes into
	 */
	public void setDeciderField(Path deciderField) {
		this.deciderField = deciderField;
	}

	/**
	 * Get the data space to copy permissions from when creating the sibling data spaces
	 * 
	 * @return the data space to copy permissions from
	 */
	public AdaptationHome getDataSpaceToCopyPermissionsFrom() {
		return dataSpaceToCopyPermissionsFrom;
	}

	/**
	 * Set the data space to copy permissions from when creating the sibling data spaces
	 * 
	 * @param dataSpaceToCopyPermissionsFrom the data space to copy permissions from
	 */
	public void setDataSpaceToCopyPermissionsFrom(
			AdaptationHome dataSpaceToCopyPermissionsFrom) {
		this.dataSpaceToCopyPermissionsFrom = dataSpaceToCopyPermissionsFrom;
	}

	/**
	 * Execute the data copy and data space creation
	 * 
	 * @param session the session
	 * @return a map with keys being the values of the decider field and
	 *         values being the data spaces that were created for those decider values
	 * @throws OperationException if there's an error creating the data spaces or copying the data
	 */
	public Map<String,AdaptationHome> execute(Session session) throws OperationException {
		final HashMap<String,AdaptationHome> createdDataSpaceMap = new HashMap<String,AdaptationHome>();
		// All tables should be from the same data set, so just get the first one
		// in the set and get its container data set.
		final Adaptation dataSet = tables.iterator().next().getContainerAdaptation();
		final AdaptationHome dataSpace = dataSet.getHome();
		final Repository repo = dataSpace.getRepository();
		// We want to compare against the initial version of the data in the data space.
		// We get this from the first snapshot of the data space, which is the parent of the data space.
		final AdaptationHome initialSnapshot = dataSpace.getParent();
		// The parent data space is the parent of the initial snapshot
		final AdaptationHome parentDataSpace = initialSnapshot.getParent();
		
		try {
			// Lock the data space if it's not already locked
			if (dataSpace.isLockedBranch()) {
				throw OperationException.createError("Data space " + dataSpace.getKey().getName() + " is locked.");
			} else {
				lock(dataSpace, session, false);
			}
			// Lock the parent data space if it's not already locked
			if (parentDataSpace.isLockedBranch()) {
				throw OperationException.createError("Data space " + parentDataSpace.getKey().getName() + " is locked.");
			} else {
				// Allow child branch creation when locking it, since this is the parent
				lock(parentDataSpace, session, true);
			}
			
			// Get the same data set from the initial snapshot
			final Adaptation initialDataSet = initialSnapshot.findAdaptationOrNull(dataSet.getAdaptationName());
			for (AdaptationTable table: tables) {
				// Get the same table from the initial snapshot's data set
				final AdaptationTable initialTable = initialDataSet.getTable(table.getTablePath());
				
				// Compare the table with the same table in the initial snapshot's data set
				final DifferenceBetweenTables diff = DifferenceHelper.compareAdaptationTables(initialTable, table, false);
				// All extra occurrences on the right will be all records that exist in data space
				// that are not in initial snapshot, i.e. those added.
				@SuppressWarnings("unchecked")
				List<ExtraOccurrenceOnRight> adds = diff.getExtraOccurrencesOnRight();
				for (ExtraOccurrenceOnRight add: adds) {
					final Adaptation record = add.getExtraOccurrence();
					// Figure out which data space it goes to, creating it if necessary
					final AdaptationHome siblingDataSpace = getSiblingDataSpace(
							record, repo, session, parentDataSpace, createdDataSpaceMap);
					// Get the same table in the sibling data space
					final AdaptationTable siblingTable = getSiblingTable(siblingDataSpace, table);
					// Copy the new record to the sibling data space
					final CopyNewRecordProcedure proc = new CopyNewRecordProcedure(record, siblingTable);
					Utils.executeProcedure(proc, session, siblingDataSpace);
				}
				// Do similar to what we did for adds, but for updates
				@SuppressWarnings("unchecked")
				List<DifferenceBetweenOccurrences> deltas = diff.getDeltaOccurrences();
				for (DifferenceBetweenOccurrences delta: deltas) {
					final Adaptation record = delta.getOccurrenceOnRight();
					final AdaptationHome siblingDataSpace = getSiblingDataSpace(
							record, repo, session, parentDataSpace, createdDataSpaceMap);
					final AdaptationTable siblingTable = getSiblingTable(siblingDataSpace, table);
					final CopyUpdatedRecordProcedure proc = new CopyUpdatedRecordProcedure(record, siblingTable);
					Utils.executeProcedure(proc, session, siblingDataSpace);
				}
			}
		} finally {
			// Unlock the data spaces that we locked.
			// This is done inside a finally so that it happens even if an exception occurred.
			if (dataSpace.isLockedBranch()) {
				unlock(dataSpace, session);
			}
			if (parentDataSpace.isLockedBranch()) {
				unlock(parentDataSpace, session);
			}
			for (AdaptationHome createdDataSpace: createdDataSpaceMap.values()) {
				if (createdDataSpace.isLockedBranch()) {
					unlock(createdDataSpace, session);
				}
			}
		}
		
		return createdDataSpaceMap;
	}
	
	private AdaptationHome getSiblingDataSpace(final Adaptation record, final Repository repo, final Session session,
			final AdaptationHome parentDataSpace, final Map<String,AdaptationHome> createdDataSpaceMap)
			throws OperationException {
		String deciderValue = record.getString(deciderField);
		if (deciderValue == null) {
			throw OperationException.createError("Unable to determine data space for record "
					+ record.getOccurrencePrimaryKey() + " in table "
					+ record.getContainerTable().getTablePath().format() + ".");
		}
		AdaptationHome siblingDataSpace = createdDataSpaceMap.get(deciderValue);
		if (siblingDataSpace == null) {
			final SimpleDateFormat dateFormat = new SimpleDateFormat(DATASPACE_DATE_PATTERN);
			final String siblingDataSpaceName = dataSpaceNamePrefix + dateFormat.format(new Date());
			final HomeKey siblingDataSpaceKey = HomeKey.forBranchName(siblingDataSpaceName);
			final HomeCreationSpec homeCreationSpec = new HomeCreationSpec();
			homeCreationSpec.setParent(parentDataSpace);
			homeCreationSpec.setKey(siblingDataSpaceKey);
			// Give the new data space the same owner as the parent data space
			homeCreationSpec.setOwner(parentDataSpace.getOwner());
			homeCreationSpec.setHomeToCopyPermissionsFrom(dataSpaceToCopyPermissionsFrom);
			// Create the sibling data space
			siblingDataSpace = repo.createHome(homeCreationSpec, session);
			createdDataSpaceMap.put(deciderValue, siblingDataSpace);
			// Lock it
			lock(siblingDataSpace, session, false);
		}
		return siblingDataSpace;
	}

	private static AdaptationTable getSiblingTable(final AdaptationHome siblingDataSpace, final AdaptationTable table)
			throws OperationException {
		final Adaptation siblingDataSet = siblingDataSpace.findAdaptationOrNull(
				table.getContainerAdaptation().getAdaptationName());
		return siblingDataSet.getTable(table.getTablePath());
	}
	
	private static void lock(final AdaptationHome dataSpace, final Session session,
			final boolean childBranchCreationAllowed) throws OperationException {
		final LockSpec lockSpec = new LockSpec();
		lockSpec.setOwnerPrivileges(true);
		lockSpec.setChildBranchCreationAllowed(childBranchCreationAllowed);
		lockSpec.lock(dataSpace,  session);
	}
	
	private static void unlock(final AdaptationHome dataSpace, final Session session)
			throws OperationException {
		final LockSpec lockSpec = new LockSpec();
		lockSpec.setOwnerPrivileges(true);
		lockSpec.unlock(dataSpace, session);
	}
	
	private class CopyNewRecordProcedure implements Procedure {
		private Adaptation record;
		private AdaptationTable siblingTable;
		
		public CopyNewRecordProcedure(final Adaptation record, final AdaptationTable siblingTable) {
			this.record = record;
			this.siblingTable = siblingTable;
		}
		
		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			final ValueContextForUpdate vc = pContext.getContextForNewOccurrence(record, siblingTable);
			// auto-increment fields need to be explicitly copied over because the
			// getContextForNewOccurrence will assume it should be generated new.
			copyAutoIncrementPKValues(vc, record);
			
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			
			pContext.doCreateOccurrence(vc, siblingTable);
			
			pContext.setTriggerActivation(true);
			pContext.setAllPrivileges(false);
		}
		
		private void copyAutoIncrementPKValues(final ValueContextForUpdate vc, final Adaptation record) {
			final SchemaNode tableNode = record.getSchemaNode().getTableNode();
			final SchemaNode[] pkNodes = tableNode.getTablePrimaryKeyNodes();
			// Loop through each primary key field
			for (int i = 0; i < pkNodes.length; i++) {
				final SchemaNode pkNode = pkNodes[i];
				// If it's an auto increment field, copy the value over from the existing record
				if (pkNode.isAutoIncrement()) {
					final Path path = Path.SELF.add(pkNode.getPathInAdaptation());
					vc.setValue(record.get(path), path);
				}
			}
		}
	}
	
	private class CopyUpdatedRecordProcedure implements Procedure {
		private Adaptation record;
		private AdaptationTable siblingTable;
		
		public CopyUpdatedRecordProcedure(final Adaptation record, final AdaptationTable siblingTable) {
			this.record = record;
			this.siblingTable = siblingTable;
		}
		
		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			final Adaptation siblingRecord = siblingTable.lookupAdaptationByPrimaryKey(
					record.getOccurrencePrimaryKey());
			final ValueContextForUpdate vc = pContext.getContext(siblingRecord.getAdaptationName());
			final SchemaNode recordNode = record.getSchemaNode();
			// Copy all values from the source record to the target record.
			// This recursively copies at every level in the record structure.
			copyNodeValues(recordNode, record, vc);
			
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			
			pContext.doModifyContent(siblingRecord, vc);
			
			pContext.setTriggerActivation(true);
			pContext.setAllPrivileges(false);
		}
		
		private void copyNodeValues(final SchemaNode node, final Adaptation record, final ValueContextForUpdate vc) {
			// If it's a terminal node, meaning it contains an actual value
			if (node.isTerminalValue()) {
				// Skip computed fields and selection nodes
				if (! node.isValueFunction() && ! node.isSelectNode()) {
					final Path path = node.getPathInAdaptation();
					final Object value = record.get(path);
					vc.setValue(value, path);
				}
			// Else it's not terminal so loop through all of its children and do the same for those
			} else {
				final SchemaNode[] nodeChildren = node.getNodeChildren();
				for (int i = 0; i < nodeChildren.length; i++) {
					copyNodeValues(nodeChildren[i], record, vc);
				}
			}
		}
	}
}
