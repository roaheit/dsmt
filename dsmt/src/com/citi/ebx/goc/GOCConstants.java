package com.citi.ebx.goc;

public class GOCConstants {
	public static final String METADATA_DATA_SPACE = "GOC_WF";
	public static final String METADATA_DATA_SET = "GOC_WF";
	
	public static final String DSMT2_HIERARCHY_DATA_SPACE = "DSMT2Hier";
	public static final String DSMT2_HIERARCHY_DATA_SET = "DSMT2Hier";
	
	public static final String DSMT2_RELATIONAL_DATASET = "DSMT2_REL";
	
	public static final String DSMT2_HIERARCHY_NEW_DATA_SPACE = "DSMT2Hierarchy";
	public static final String DSMT2_HIERARCHY_NEW_DATA_SET = "DSMT2Hierarchy";
	
	public static final String WORKFLOW_PUBLICATION = "GOCWorkflow";
	public static final String DSMT_WORKFLOW_PUBLICATION = "DSMTWorkflow";
	
	public static final String TRACKING_INFO_WORKFLOW_ID = "RequestChanges";
	public static final String TRACKING_INFO_SEPARATOR = "_";
	
	public static final String ACTION_CREATE_GOC_LEG_CCS = "A";
	public static final String ACTION_MODIFY_GOC_DESC_ONLY = "F";
	public static final String ACTION_MODIFY_GOC_USAGE_ONLY = "G";
	public static final String ACTION_MODIFY_FRS_BU_ONLY = "H";
	public static final String ACTION_MODIFY_FRS_OU_ONLY = "J";
	public static final String ACTION_MODIFY_MANAGED_SEG_ONLY = "K";
	public static final String ACTION_MODIFY_MANAGED_GEO_ONLY = "L";
	public static final String ACTION_MODIFY_FUNCTION_ONLY = "M";
	public static final String ACTION_MODIFY_MULTI_GOC_ATTRS = "U";
	public static final String ACTION_DEACTIVATE_GOC_LEG_CCS = "B";
	public static final String ACTION_PURGE_GOC_LEG_CCS = "D";
	public static final String ACTION_REACTIVATE_GOC_LEG_CCS = "E";
	public static final String ACTION_MODIFY_GL_ONLY = "N";
	public static final String ACTION_MODIFY_P2P_ONLY = "O";
	public static final String ACTION_MODIFY_RE_ONLY = "P";
	public static final String ACTION_NO_CHANGE = "Z";
	
	public static final String DECIDER_VALUE_SEPARATOR = "|";
	
	public static final String MAINTENANCE_TYPE_REGULAR = "regular";
	public static final String MAINTENANCE_TYPE_FUNCTIONAL = "functional";
	
	
	public static final String WF_PARAM_SID = "sid";
	public static final String WF_PARAM_MAINTENANCE_TYPE = "maintenanceType";
	public static final String WF_PARAM_FUNCTION_ID = "functionalID";
	public static final String WF_PARAM_MANAGEMENTONLY="managementOnly";
	
	public static final String GOC_REF_DATA_SPACE = "GOCPartnerSystemReferenceData";
	public static final String GOC_REF_METADATA_DATA_SET = "GOCPartnerSystemReferenceData";
	public static final String CREATED = "Created";
	public static final String SUBMITTED = "Submitted";
	public static final String REJECTED = "Rejected";
	public static final String DELETED ="Deleted";
	public static final String APPROVED ="Approved";
	public static final String IGW_WORKFLOW_PUBLICATION = "IGWGOCWorkflow";
	public static final String CONSTANT_YES = "Y";
	
	private GOCConstants() {
	}
}
