package com.citi.ebx.goc.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class GOCLocalCostCodeValueFunction implements ValueFunction {
	//private static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	@Override
	public Object getValue(Adaptation adaptation) {
		final String lcc =  null; /*GOCWorkflowUtils.getLocalCostCode(
				adaptation.createValueContext());
		if (lcc == null) {
			LOG.error("No Local Cost Code can be determined for GOC record "
					+ adaptation.getOccurrencePrimaryKey().format() + ".");
			return null;
		}
		*/
		return lcc;
	}

	@Override
	public void setup(ValueFunctionContext context) {
		// do nothing
	}
}
