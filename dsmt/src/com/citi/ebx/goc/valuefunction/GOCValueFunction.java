package com.citi.ebx.goc.valuefunction;

import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class GOCValueFunction implements ValueFunction {
	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	@Override
	public Object getValue(Adaptation adaptation) {
		final Adaptation sidRecord = Utils.getLinkedRecord(adaptation,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
		if (sidRecord == null) {
			LOG.error("No SID record can be found for GOC record "
					+ adaptation.getOccurrencePrimaryKey().format() + ".");
			return null;
		}
		final String sid = sidRecord.getString(
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID);
		
		final String lcc = adaptation.getString(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD);
		if (lcc == null) {
			LOG.error("No Local Cost Code can be determined for GOC record "
					+ adaptation.getOccurrencePrimaryKey().format() + ".");
			return sid;
		}
		
		return sid + lcc;
	}

	@Override
	public void setup(ValueFunctionContext context) {
		// do nothing
	}
}
