package com.citi.ebx.goc.valuefunction;

import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class PartnerLocalCostCodeValueFunction implements ValueFunction {
	// Assume all partner tables use same name for the GOC foreign key
	private static final Path GOC_FK_PATH =
			GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM._C_GOC_FK;
	
	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private Path gocLCCField;
	
	@Override
	public Object getValue(Adaptation adaptation) {
		final Adaptation gocRecord = Utils.getLinkedRecord(adaptation, GOC_FK_PATH);
		if (gocRecord == null) {
			LOG.error("Could not find GOC record for record " + adaptation.getOccurrencePrimaryKey().format()
					+ " in table " + adaptation.getContainerTable().getTablePath().format() + ".");
			return null;
		}
		return gocRecord.get(gocLCCField);
	}

	@Override
	public void setup(ValueFunctionContext context) {
	/*	final SchemaNode tableGroupNode = context.getSchemaNode()
				.getNode(Path.PARENT).getNode(Path.PARENT);
		if (GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL.equals(
				tableGroupNode.getPathInSchema())) {
			gocLCCField = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GLLocalCostCode;
		} else if (GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE.equals(
				tableGroupNode.getPathInSchema())) {
			gocLCCField = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._RELocalCostCode;
		} else if (GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_P2P.equals(
				tableGroupNode.getPathInSchema())) {
			gocLCCField = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._P2PLocalCostCode;
		} else {
			context.addError("Can't determine partner table type.");
		}
		*/
	}
}
