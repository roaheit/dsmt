package com.citi.ebx.goc.valuefunction;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.goc.util.IGWGOCWorkflowUtils;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValidationReport;


public class GridRoutingValueFunction implements ValueFunction {

	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();

	private static final String COUNTRY_CONTROLLER_DEFAULT_ROLE 	= "COUNTRY_CONTROLLER_DEFAULT_ROLE";
	private static final String REGIONAL_DQ_DEFAULT_ROLE 			= "REGIONAL_DQ_DEFAULT_ROLE";
	private static final String COUNTRY_BUSINESS_HR_DEFAULT_ROLE 	= "COUNTRY_BUSINESS_HR_DEFAULT_ROLE";
	private static final String REGIONAL_FINANCE_DEFAULT_ROLE 	= "REGIONAL_FINANCE_DEFAULT_ROLE";
	// private static final String FINANCE_ROLE_1_DEFAULT_ROLE 		= "FINANCE_ROLE_1_DEFAULT_ROLE";
	// private static final String FINANCE_ROLE_2_DEFAULT_ROLE 		= "FINANCE_ROLE_2_DEFAULT_ROLE";
	
	private static boolean isInWorkflow(final Adaptation dataSet) {
		final String currentSid = GOCWorkflowUtils.getCurrentSID(dataSet);
		final String maintenanceType = GOCWorkflowUtils.getMaintenanceType(dataSet);
		if(StringUtils.isBlank(currentSid) && GOCConstants.MAINTENANCE_TYPE_REGULAR.equalsIgnoreCase(maintenanceType)){
			return true;
		}else if(StringUtils.isBlank(currentSid) &&  GOCConstants.MAINTENANCE_TYPE_FUNCTIONAL.equalsIgnoreCase(maintenanceType)){
			return true;	
		}
		return (currentSid != null && ! "".equals(currentSid) );

	}
	
	@Override
	public Object getValue(Adaptation adaptation) {
		
		StringBuilder gridRouting = null;
		
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(adaptation.getHome().getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return null;
		}
		 
		if (isInWorkflow(adaptation.getContainerTable().getContainerAdaptation())) {
			Adaptation dataSet =   adaptation.getContainerTable().getContainerAdaptation();
			String workflowStatus = IGWGOCWorkflowUtils.getWorkflowStatus(dataSet);
			final String actionRequired = adaptation.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD);
			
			final ArrayList<String> gridRoutingList = new ArrayList<String>();
			
			final String sid = getSID(adaptation);
			
			final String bu = getBU(adaptation);
			
			final String manageSegment = adaptation.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
			
			final String headCount = adaptation.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._HEADCOUNT_GOC);
			
			//final String msAP = getManagedSegControlPoint(adaptation);
			final List<String> mgAP = getManagedGeoApprovalPoint(adaptation);
			final List<String> msAP = getManagedSegApprovalPoint(adaptation);
			//ak76503  added code to remove falat error if mandatory field are not present
			if(msAP == null || mgAP == null){
				return null;
			}
			
			Repository repo = adaptation.getHome().getRepository();
			Path tablePath = GOCWFPaths._C_DSMT_ACT_ROLE_MAP.getPathInSchema();
			Path[] tableColumns =  new Path[10] ;
			tableColumns[0] = GOCWFPaths._C_DSMT_ACT_ROLE_MAP._ACTION_REQ_CODE;
			Adaptation armRecord  = null;
			String params = actionRequired;
			
			String ccApproval = GOCConstants.CONSTANT_YES;
			String regFinApproval = GOCConstants.CONSTANT_YES;
			String manSegApproval = GOCConstants.CONSTANT_YES;
			String dqhrApproval = GOCConstants.CONSTANT_YES;
			
			try {
				armRecord = GOCWorkflowUtils.getQueryRecord(repo, tablePath, tableColumns, params);
			} catch (OperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(null !=  armRecord){
				ccApproval = armRecord.getString(GOCWFPaths._C_DSMT_ACT_ROLE_MAP._CC_APP);
				regFinApproval = armRecord.getString(GOCWFPaths._C_DSMT_ACT_ROLE_MAP._REG_FIN_APP);
				manSegApproval = armRecord.getString(GOCWFPaths._C_DSMT_ACT_ROLE_MAP._MAN_SEG_APP);
				dqhrApproval = armRecord.getString(GOCWFPaths._C_DSMT_ACT_ROLE_MAP._DQ_HR_APP);
			}
			
				if(GOCConstants.CONSTANT_YES.equalsIgnoreCase(ccApproval)){
					
					final List<String> countryController = getCountryController(sid, bu, msAP, metadataDataSet);
					if (countryController == null) {
						return null;
					}
					gridRoutingList.addAll(countryController);
				}else{
					//LOG.info("insde else of ccApproval");
					gridRoutingList.add(null);
				}
				
				List<String> regionalDQTeam =  null;
				if(GOCConstants.CONSTANT_YES.equalsIgnoreCase(dqhrApproval)){
					 regionalDQTeam = getRegionalDQTeam(sid, msAP, metadataDataSet, headCount);
					if (regionalDQTeam == null) {
						return null;
					}
					gridRoutingList.add(regionalDQTeam.get(0));
				}else{
					gridRoutingList.add(null);
				}
				
				
				if (includeBusinessFunctionFinanceHR(actionRequired)) {
					
					List<String> businessFunctionFinanceHR = null;
					if(GOCConstants.CONSTANT_YES.equalsIgnoreCase(manSegApproval)){
						businessFunctionFinanceHR = getBusinessFunctionFinanceHR(msAP, mgAP, metadataDataSet, headCount);
					}
					if (businessFunctionFinanceHR == null) {
						
						// set empty values 
						for (int i = 0; i < 9; i++) {
							if(i==5){
								
									gridRoutingList.add((GOCConstants.CONSTANT_YES.equalsIgnoreCase(dqhrApproval)) ? regionalDQTeam.get(1) : null);
								
							}
							gridRoutingList.add(null);
						}
					}else{
							for (int i = 0; i < 9; i++) {
								if(i==5){
									
										gridRoutingList.add((GOCConstants.CONSTANT_YES.equalsIgnoreCase(dqhrApproval)) ? regionalDQTeam.get(1) : null);
									
								}
								
									gridRoutingList.add((GOCConstants.CONSTANT_YES.equalsIgnoreCase(manSegApproval)) ? businessFunctionFinanceHR.get(i) : null);
								
						}
					}
					
				} else {
					for (int i = 0; i < 9; i++) {
						if(i==5){
							
								gridRoutingList.add((GOCConstants.CONSTANT_YES.equalsIgnoreCase(dqhrApproval)) ? regionalDQTeam.get(1) : null);
						
						}
						gridRoutingList.add(null);
					}
				}
				
				
				
			
			final List<String> maintenanceTeam = getMaintenanceTeam(sid,bu, metadataDataSet);
			if (maintenanceTeam == null) {
				/** set empty values */
				for (int i = 0; i < 3; i++) {
					gridRoutingList.add(null);
				}				
			}else{
				gridRoutingList.addAll(maintenanceTeam);
			}
			
			// added the conentmanagment in Grid routing ID
			
			if (!"Resubmitted".equalsIgnoreCase(workflowStatus)) {
				
				if (actionRequired.equalsIgnoreCase("A")) {
					// LOG.info("action requried  : " +actionRequired);
					boolean isWarning = false;
					try {
						final ValidationReport recordReport = adaptation
								.getValidationReport(false, false);
						isWarning = recordReport
								.hasItemsOfSeverity(Severity.WARNING);
					} catch (Exception e) {
						e.printStackTrace();
					}

					if (isWarning && !(manageSegment.startsWith("98"))) {
						LOG.info("Managed segemt id for content managment is "+ manageSegment + "and isWarning " + isWarning);
						gridRoutingList.add("DSMT_CONTENT_MANAGMENT");
					} else {
						gridRoutingList.add(null);
					}
				} else {
					gridRoutingList.add(null);

				}
			} else {

				String contentManageGRID = resubmitGRID(metadataDataSet,adaptation);
				gridRoutingList.add(contentManageGRID);
			}
			
			// add new level of approver 'regional finance' at the end of grid routing id. make sure for old request this should not be added in resubmit.
			
			
			
			if(GOCConstants.CONSTANT_YES.equalsIgnoreCase(regFinApproval)){
				
				final List<String> regionalFinannce = getRegionalFinanceRole(sid, bu, msAP, metadataDataSet);
				if (regionalFinannce == null) {
					return null;
				}
				gridRoutingList.addAll(regionalFinannce);
			}else{
				//LOG.info("inside else of regional finance");
				gridRoutingList.add(null);
			}
			
			if(manageSegment.startsWith("98")){
				gridRoutingList.add("DSMT_PLANNING_UNIT_APPROVER");
			}else {
				gridRoutingList.add(null);
			}
			
			gridRouting = new StringBuilder();
			
			for (int i = 0; i < gridRoutingList.size(); i++) {
				final String str = gridRoutingList.get(i);
				gridRouting.append(str == null ? "" : str);
				if (i < gridRoutingList.size() - 1) {
					gridRouting.append(GOCConstants.DECIDER_VALUE_SEPARATOR);
				}
			}
			LOG.debug("Grid Routing ID for GOC PK" + gridRouting);
			return gridRouting.toString();
		}
		else{
			return null;
		}
			
	}
	
	
	private String resubmitGRID(Adaptation metadataDataSet, Adaptation adaptation){

		// ckech old record.
		LOG.info("Inside resubmitted condition");
		final AdaptationTable WF_DTLTable =	metadataDataSet.getTable(GOCWFPaths._C_GOC_WF_DTL.getPathInSchema());
		final AdaptationTable WF_ReportTable =	metadataDataSet.getTable(GOCWFPaths._C_GOC_WF_REPORTING.getPathInSchema());
		
		RequestResult reqRes = null;
		Adaptation WF_DTLRecord = null;
		Adaptation WF_ReportRecord = null;
		
		String requestID = null;
		String originalRoutingID = null;
		String gocPK = Integer.toString(adaptation.get_int(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK));
		final String predicate = GOCWFPaths._C_GOC_WF_DTL._GOC_PK.format()
				+ "=\"" + gocPK + "\"";
		
		// LOG.info( " predicate for getWF detail  is "+ predicate);
		reqRes = WF_DTLTable.createRequestResult(predicate);
		if (null != reqRes) {
			WF_DTLRecord = reqRes.nextAdaptation();
			if(null !=  WF_DTLRecord){
				requestID = WF_DTLRecord.getString(GOCWFPaths._C_GOC_WF_DTL._REQUEST_ID);
				LOG.info("work flow record with requets id "+ requestID);
			}
			if(StringUtils.isNotBlank(requestID)){
				final String predicate2 = GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_ID.format()
						+ "=\"" + requestID + "\"";
				
				reqRes = WF_ReportTable.createRequestResult(predicate2);
				WF_ReportRecord = reqRes.nextAdaptation();
				originalRoutingID = WF_ReportRecord.getString(GOCWFPaths._C_GOC_WF_REPORTING._GRID_ROUTING_ID);
				LOG.info("Original routing id "+ originalRoutingID);
			}
			reqRes.close();
		}
		if(StringUtils.isNotBlank(originalRoutingID) && originalRoutingID.contains("DSMT_CONTENT_MANAGMENT")){
			return "DSMT_CONTENT_MANAGMENT";
		}else{
			return null;
		}
	
	}
	@Override
	public void setup(ValueFunctionContext context) {
		// do nothing
	}
	
	private static String getSID(final Adaptation gocRecord) {
		final Adaptation sidRecord = Utils.getLinkedRecord(gocRecord,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
		if (sidRecord == null) {
			LOG.error("No SID found for record " + gocRecord.getOccurrencePrimaryKey().format());
			return null;
		}
		return sidRecord.getString(
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID);
	}
	
	private static String getBU(final Adaptation gocRecord) {
		final Adaptation buRecord = Utils.getLinkedRecord(gocRecord,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU);
		if (buRecord == null) {
			LOG.error("No FRS BU found for record " + gocRecord.getOccurrencePrimaryKey().format());
			return null;
		}
		return buRecord.getString(
				DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_BU_STD_C_DSMT_FRS_BU._C_DSMT_FRS_BU);
	}
	
	/* 
	 * private static String getManagedSegControlPoint(final Adaptation gocRecord) {
		Adaptation msRecord = Utils.getLinkedRecord(gocRecord,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
		if (msRecord == null) {
			LOG.error("No Managed Segment found for record " + gocRecord.getOccurrencePrimaryKey().format());
			return null;
		}
		return msRecord.getString(
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._ControlPointManagedSegmentID);
	}
	
	private static String getManagedGeoControlPoint(final Adaptation gocRecord) {
		Adaptation mgRecord = Utils.getLinkedRecord(gocRecord,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID);
		if (mgRecord == null) {
			LOG.error("No Managed Geography found for record " + gocRecord.getOccurrencePrimaryKey().format());
			return null;
		}
		return mgRecord.getString(
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._ControlPointManagedGeographyID);
	} */
	
	private static boolean includeBusinessFunctionFinanceHR(final String actionRequired) {
		return GOCConstants.ACTION_CREATE_GOC_LEG_CCS.equals(actionRequired)
				|| GOCConstants.ACTION_MODIFY_GOC_DESC_ONLY.equals(actionRequired)
				|| GOCConstants.ACTION_MODIFY_GOC_USAGE_ONLY.equals(actionRequired)
				|| GOCConstants.ACTION_MODIFY_MULTI_GOC_ATTRS.equals(actionRequired)
				|| GOCConstants.ACTION_DEACTIVATE_GOC_LEG_CCS.equals(actionRequired)
				|| GOCConstants.ACTION_REACTIVATE_GOC_LEG_CCS.equals(actionRequired)
				|| GOCConstants.ACTION_MODIFY_FRS_OU_ONLY.equals(actionRequired)
				|| GOCConstants.ACTION_MODIFY_MANAGED_SEG_ONLY.equals(actionRequired)
				|| GOCConstants.ACTION_MODIFY_MANAGED_GEO_ONLY.equals(actionRequired)
				|| GOCConstants.ACTION_MODIFY_FUNCTION_ONLY.equals(actionRequired);
	}
	
	private static List<String> getCountryController(
			final String sid, final String bu, final List<String> msAP,
			final Adaptation metadataDataSet) {
		
		final AdaptationTable ccTable =	metadataDataSet.getTable(GOCWFPaths._C_DSMT_CC_FRSBU.getPathInSchema());
		
		RequestResult reqRes = null;
		Adaptation ccRecord = null;
		
		for (String msApprovalPoint : msAP) {
			
			final String predicate = GOCWFPaths._C_DSMT_CC_FRSBU._SID.format()
			+ "=\"" + sid + "\" and "
			+ GOCWFPaths._C_DSMT_CC_FRSBU._FRS_BU.format()
			+ "=\"" + bu + "\" and "
			+ GOCWFPaths._C_DSMT_CC_FRSBU._MNG_SEG_APP_POINT.format() 
			+ "=\"" + msApprovalPoint + "\"";
			
			LOG.debug( " predicate for getCountryController  is "+ predicate);
			reqRes = ccTable.createRequestResult(predicate);
			
			if (null != reqRes) {
				
				ccRecord = reqRes.nextAdaptation();
				if(null !=  ccRecord){
					LOG.debug("Country Controller Role mapping found for sid " + sid + ", BU " + bu + " and MS "+ msApprovalPoint );
					break; //stop as soon as first record is found from bottom in the Managed Segment Hierarchy
				}
				
				reqRes.close();
				
			}
		}
		
		final ArrayList<String> roles = new ArrayList<String>();
		if (ccRecord == null) {
			
			LOG.error("No record found for sid " + sid + ", BU " + bu + " and MS " + msAP + " in table " + ccTable.getTablePath().format() +". Default Country Controller will be set." );
			// Setting default role for Country controller review.
			roles.add(COUNTRY_CONTROLLER_DEFAULT_ROLE);
		//	return null;
		}else{
		
			LOG.debug("Country Controller Role ID found for sid = " + sid + " and msAP = " + msAP + " is "  + ccRecord.getString(GOCWFPaths._C_DSMT_CC_FRSBU._CC_ROLE_ID));
			roles.add(ccRecord.getString(GOCWFPaths._C_DSMT_CC_FRSBU._CC_ROLE_ID));
		}
		return roles;
	}
	
	@Deprecated
	protected static List<String> getCountryControllerOld(
			final String sid, final String bu, final List<String> msAP,
			final Adaptation metadataDataSet) {
		final AdaptationTable ccTable =	metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_CC_FRSBU.getPathInSchema());
		
		 String msPredicate = createPredicateMngSeg(msAP);
		msPredicate =msPredicate.replaceAll("MAN_SEG_APPROVAL_POINT", "MNG_SEG_APP_POINT");
		final String predicate = GOCWFPaths._C_DSMT_CC_FRSBU._SID.format()
				+ "=\"" + sid + "\" and "
				+ GOCWFPaths._C_DSMT_CC_FRSBU._FRS_BU.format()
				+ "=\"" + bu + "\" and "
				+ msPredicate ;
		LOG.debug( " predicate for getCountryController  is "+ predicate);
		final RequestResult reqRes = ccTable.createRequestResult(predicate);
		final Adaptation ccRecord;
		try {
			ccRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		
		final ArrayList<String> roles = new ArrayList<String>();
		if (ccRecord == null) {
			
			LOG.error("No record found for predicate " + predicate + " in table " + ccTable.getTablePath().format() );
			// Setting default role for Country controller review.
			roles.add(COUNTRY_CONTROLLER_DEFAULT_ROLE);
		//	return null;
		}else{
		
			LOG.debug("Country Controller Role ID found for sid = " + sid + " and msAP = " + msAP + " is "  + ccRecord.getString(GOCWFPaths._C_DSMT_CC_FRSBU._CC_ROLE_ID));
			roles.add(ccRecord.getString(GOCWFPaths._C_DSMT_CC_FRSBU._CC_ROLE_ID));
		}
		return roles;
	}
	
	@Deprecated
	protected static List<String> getRegionalDQTeamOld(
			final String sid, final List<String> msAP,
			final Adaptation metadataDataSet) {
		final AdaptationTable dqTeamHRTable = metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_SID_ROLE.getPathInSchema());
	
		String msPredicate = createPredicateMngSeg(msAP);
		msPredicate =msPredicate.replaceAll("MAN_SEG_APPROVAL_POINT", "MAN_SEG_APPROVAL");
		final String predicate = GOCWFPaths._C_DSMT_SID_ROLE._SID.format()
		+ "=\"" + sid + "\" and "+ msPredicate;
		
		final RequestResult reqRes = dqTeamHRTable.createRequestResult(predicate);
		final  Adaptation dqTeamHRRecord ;
		
		try {
		 dqTeamHRRecord = getDQApproval(reqRes,msAP);
			
		} finally {
			reqRes.close();
		}
		
		final ArrayList<String> roles = new ArrayList<String>();
		if (dqTeamHRRecord == null) {
			
			LOG.error("No record found for predicate " + predicate + " in table " + dqTeamHRTable.getTablePath().format());
			
			// Setting default role for Country controller review.
			roles.add(REGIONAL_DQ_DEFAULT_ROLE);
			roles.add(COUNTRY_BUSINESS_HR_DEFAULT_ROLE);
			// return null;
		}
		else{
			
			roles.add(dqTeamHRRecord.getString(GOCWFPaths._C_DSMT_SID_ROLE._TEAM_ROLE_ID));
			roles.add(dqTeamHRRecord.getString(GOCWFPaths._C_DSMT_SID_ROLE._HR_ROLE_ID));
			LOG.debug("Regional DQ Controller Role ID found for sid = " + sid + " and mscp = " + msAP + " is " + roles);
		}
		return roles;
	}
	
	@Deprecated
	private static Adaptation getDQApproval(RequestResult reqRes, List<String> msAP) {
		Adaptation  dqTeamHRRecord =null;
		
		if(reqRes!=null)
		{
			
			LOG.debug("Size of resultset in the Regional DQ Team = " +reqRes.getSize());
			
			for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
				dqTeamHRRecord=record;
			}
			
		}
		else{
			dqTeamHRRecord = null;
			}
		return dqTeamHRRecord;
	}

	private static List<String> getRegionalDQTeam(
			final String sid, final List<String> msAP,
			final Adaptation metadataDataSet,String headCount) {
		final AdaptationTable dqTeamHRTable = metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_SID_ROLE.getPathInSchema());
	
		Adaptation dqTeamHRRecord = null ;
		RequestResult reqRes = null;
		
		for (String msApprovalPoint : msAP) {
				
				final String predicate = GOCWFPaths._C_DSMT_SID_ROLE._SID.format()	+ "=\"" + sid + "\" and "+ GOCWFPaths._C_DSMT_SID_ROLE._MAN_SEG_APPROVAL.format() + "=\"" + msApprovalPoint +"\"";
				reqRes = dqTeamHRTable.createRequestResult(predicate);
				
				if (null != reqRes) {
					
					dqTeamHRRecord = reqRes.nextAdaptation();
					if(null !=  dqTeamHRRecord){
						LOG.debug("DQ Team, HR Role mapping found for sid " + sid + " and MS "+ msApprovalPoint );
						break; //stop as soon as first record is found from bottom in the Managed Segment Hierarchy
					}
					
					reqRes.close();
					
				}
			}
		
		
		final ArrayList<String> roles = new ArrayList<String>();
		if (null == dqTeamHRRecord) {
			
			LOG.error("No record found for MS approval points " + msAP + " and SID " + sid + " in table " + dqTeamHRTable.getTablePath().format() + ". Default roles will be assigned.");
			
			// Setting default role for Country controller review.
			//roles.add(REGIONAL_DQ_DEFAULT_ROLE);
			
			// setting Regional DQ Role to null, always to sync the grid routing to CWM xml
			roles.add(null);
			if("Y".equalsIgnoreCase(headCount)){
				roles.add(COUNTRY_BUSINESS_HR_DEFAULT_ROLE);
			}else{
				roles.add(null);
			}
			// return null;
		}
		else{
			
			//roles.add(dqTeamHRRecord.getString(GOCWFPaths._C_DSMT_SID_ROLE._TEAM_ROLE_ID));
			
			// setting Regional DQ Role to null always, to sync the grid routing to CWM xml
			roles.add(null);
			if("Y".equalsIgnoreCase(headCount)){
				roles.add(dqTeamHRRecord.getString(GOCWFPaths._C_DSMT_SID_ROLE._HR_ROLE_ID));
			}else{
				roles.add(null);
			}
			LOG.debug("Regional DQ Controller Role ID found for sid = " + sid + " and mscp = " + msAP + " is " + roles);
		}
		return roles;
	}
	
	
	private static List<String> getMaintenanceTeam(
			final String sid, String bu, final Adaptation metadataDataSet) {
		final AdaptationTable sidEnhancementTable = metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
		final String predicate = GOCWFPaths._C_DSMT_SID_ENH._SID.format()
				+ "=\"" + sid + "\"";
		final RequestResult reqRes = sidEnhancementTable.createRequestResult(predicate);
		final Adaptation sidEnhancementRecord;
		try {
			sidEnhancementRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (sidEnhancementRecord == null) {
			LOG.error("No record found for predicate " + predicate
					+ " in table " + sidEnhancementTable.getTablePath().format());
			
			final AdaptationTable sidBUReferenceTable = metadataDataSet.getTable(
					GOCWFPaths._C_DSMT_SID_BU_REF.getPathInSchema());
			final String prd = GOCWFPaths._C_DSMT_SID_BU_REF._SID.format()+ "=\"" + sid + "\"" +
					" and "+ GOCWFPaths._C_DSMT_SID_BU_REF._C_DSMT_FRS_BU.format() + "=\"" + bu + "\"" + 
					" and " +endDatePredicate(GOCWFPaths._C_DSMT_SID_BU_REF._ENDDT.format()) + 
					" and " + GOCWFPaths._C_DSMT_SID_BU_REF._EFF_STATUS.format() +  "=\"A\"";
			final RequestResult result = sidBUReferenceTable.createRequestResult(prd);
			final Adaptation record;
			try {
				record = result.nextAdaptation();
			} finally {
				result.close();
			}
			if(null!=record){
				final ArrayList<String> roles = new ArrayList<String>();
				roles.add(record.getString(GOCWFPaths._C_DSMT_SID_BU_REF._GLMaintenanceTeam));
				roles.add(record.getString(GOCWFPaths._C_DSMT_SID_BU_REF._RptEngineMaintenanceTeam));
				roles.add(record.getString(GOCWFPaths._C_DSMT_SID_BU_REF._P2PMaintenanceTeam));
				return roles;	
			}
			else{
				return null;
			}
			
		}
		
		final ArrayList<String> roles = new ArrayList<String>();
		roles.add(sidEnhancementRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._GLMaintenanceTeam));
		roles.add(sidEnhancementRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._RptEngineMaintenanceTeam));
		roles.add(sidEnhancementRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._P2PMaintenanceTeam));
		return roles;
	}
	


	private static List<String> getManagedSegApprovalPoint(final Adaptation record){
		ArrayList<String> msAppPoint = new ArrayList<String>();
		
		
		final Adaptation metadataDataSet;
		String msFK ;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(record.getHome().getRepository(),DSMTConstants.GOC,DSMTConstants.GOC);
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return null;
		}
			
		Adaptation msRecord = Utils.getLinkedRecord(record,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
		if (msRecord == null) {
			LOG.error("No Managed Segment found for record " + record.getOccurrencePrimaryKey().format());
			return null;
		}
		else{
			 msFK =msRecord.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._Parent_FK);
			msAppPoint.add(msRecord.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._C_DSMT_MAN_SEG_ID));
			AdaptationTable table =  metadataDataSet.getTable(
					GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD.getPathInSchema());
			getParentNode(table,msFK ,
					GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._Parent_FK,
					GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._C_DSMT_MAN_SEG_ID,
					msAppPoint);
		}
		
		if(!msAppPoint.contains("1")){
			msAppPoint.add("1");			
		}
		LOG.debug("Manage SEG List >> " + msAppPoint);
		
		return msAppPoint;
		
	}
	
	private static List<String> getManagedGeoApprovalPoint(final Adaptation record) {
		
		ArrayList<String> mgAppPoint = new ArrayList<String>();
		
		
		final Adaptation metadataDataSet;
		String msFK ;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(record.getHome().getRepository(),DSMTConstants.GOC,DSMTConstants.GOC);
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return null;
		}
			
		Adaptation msRecord = Utils.getLinkedRecord(record,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID);
		if (msRecord == null) {
			LOG.error("No Managed Geo found for record " + record.getOccurrencePrimaryKey().format());
			return null;
		}
		else{
			 msFK =msRecord.getString(
					 GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._Parent_FK);
			 mgAppPoint.add(msRecord.getString(
					 GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._C_DSMT_MAN_GEO_ID));
			AdaptationTable table =  metadataDataSet.getTable(
					 GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD.getPathInSchema());
			getParentNode(table,msFK ,
					 GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._Parent_FK,
					 GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._C_DSMT_MAN_GEO_ID,
					mgAppPoint);
		}
		
		if(!mgAppPoint.contains("1")){
			mgAppPoint.add("1");			
		}

		LOG.debug("Manage GEO List >> " + mgAppPoint);
		
		return mgAppPoint;
		
	
	}
	
	public static List<String> getParentNode(AdaptationTable table , String tableFK ,Path parentFK, Path setColunName, ArrayList<String> msList){
		
		Adaptation parentRecord = table.lookupAdaptationByPrimaryKey(PrimaryKey
				.parseString(tableFK));
		if(parentRecord ==null){
			return msList;
		}
		else{
			
			msList.add(parentRecord.getString(setColunName));
			String msFK= parentRecord.getString(parentFK);
			if(msFK!=null){
			getParentNode(table, msFK,parentFK, setColunName,msList);
			}
			
			
		}
		
		return msList;
	}
	
	private static List<String> getBusinessFunctionFinanceHR(
			final List<String> msCP, final List<String> mgCP,
			final Adaptation metadataDataSet, String headCount) {
		final ArrayList<String> roles = new ArrayList<String>();
		final Adaptation managedSegGeoMapRecord;
		/*String mngSeg = createPredicateMngSeg(msCP);
		String mngGeo = createPredicateMngGeo(mgCP);
		final AdaptationTable managedSegGeoMapTable = metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_MANSEG_MANGEO.getPathInSchema());
		final String predicate = mngSeg + "and "+mngGeo ;
		final RequestResult reqRes = managedSegGeoMapTable.createRequestResult(predicate);
		
		try {
			managedSegGeoMapRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}*/
		
		managedSegGeoMapRecord = getRecordFromManSegMangeo(metadataDataSet,msCP,mgCP);
		if (managedSegGeoMapRecord == null) {
			
			LOG.error("No record found for manageSeg list : " + msCP
					+ " and mangeGeo list : "+ mgCP +" in table ManagedSeg Geography Map" );
			return null;
		}
		else{
			roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._FINANCE_ROLE_ID1));
			roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._FINANCE_ROLE_ID2));
			roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._FINANCE_ROLE_ID3));
			roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._FINANCE_ROLE_ID4));
			roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._FINANCE_ROLE_ID5));
			
			if("Y".equalsIgnoreCase(headCount)){
				roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._HR_ROLE_ID1));
				roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._HR_ROLE_ID2));
				roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._HR_ROLE_ID3));
				roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._HR_ROLE_ID4));
			}else{
				roles.add(null);
				roles.add(null);
				roles.add(null);
				roles.add(null);
			}
			
		}
		return roles;
	}
	
	public static String  createPredicateMngSeg(List<String > pkList){
		
		StringBuffer predicBuffer = new StringBuffer();
		predicBuffer.append("(" +GOCWFPaths._C_DSMT_MANSEG_MANGEO._MAN_SEG_APPROVAL_POINT.format()	+ "=\"");
		for (int count=0 ; count<pkList.size();count++){
						if(count <(pkList.size()-1)){
			predicBuffer.append(pkList.get(count) + "\" or "+  GOCWFPaths._C_DSMT_MANSEG_MANGEO._MAN_SEG_APPROVAL_POINT.format()+ "=\"");
			}
			else{
				predicBuffer.append(pkList.get(count)+ "\" )");
			}
			
		}
		
		return	predicBuffer.toString();
	}
	
	public static String  createPredicateMngGeo(List<String > pkList){
		
		StringBuffer predicBuffer = new StringBuffer();
		predicBuffer.append(" ("+GOCWFPaths._C_DSMT_MANSEG_MANGEO._MAN_GEO_APPROVAL_POINT.format()	+ "=\"");
		for (int count=0 ; count<pkList.size();count++){
			if(count<(pkList.size()-1)){
			predicBuffer.append(pkList.get(count) + "\" or "+  GOCWFPaths._C_DSMT_MANSEG_MANGEO._MAN_GEO_APPROVAL_POINT.format()+ "=\"");
			}
			else{
				predicBuffer.append(pkList.get(count)+ "\" )");
			}
			
		}
		
		return	predicBuffer.toString();
	}

	private static Adaptation getRecordFromManSegMangeo(
			final Adaptation metadataDataSet,
			final List<String> msCP, final List<String> mgCP
			) {
		 Adaptation managedSegGeoMapRecord= null;
		 final AdaptationTable managedSegGeoMapTable = metadataDataSet.getTable(
					GOCWFPaths._C_DSMT_MANSEG_MANGEO.getPathInSchema());
		 // make loop for Manage Segment table
		 for (String manSegment : msCP) {
			 //	make loop for Manage Geo table	
			 for (String manGeo : mgCP) {
				 final String predicate = GOCWFPaths._C_DSMT_MANSEG_MANGEO._MAN_SEG_APPROVAL_POINT.format()
							+ "=\"" + manSegment + "\" and "
							+ GOCWFPaths._C_DSMT_MANSEG_MANGEO._MAN_GEO_APPROVAL_POINT.format()
							+ "=\"" + manGeo + "\"";
				 final RequestResult reqRes = managedSegGeoMapTable.createRequestResult(predicate);
				 try {
					 	// return the Adaption when user get first record in table ManagedSeg Geography Map 
					 		if(!reqRes.isEmpty()){
					 			LOG.debug("Record found for manSegment " + manSegment
					 					+ " and manGeo " +manGeo);
					 			managedSegGeoMapRecord = reqRes.nextAdaptation();
					 			return managedSegGeoMapRecord; 
					 		}
					} finally {
						reqRes.close();
					}
				 
				
			}
		}
		 
		 return   managedSegGeoMapRecord;
		
	}

/*
private static List<String> getBusinessFunctionFinanceHR(
		final String msCP, final String mgCP,
		final Adaptation metadataDataSet) {
	final ArrayList<String> roles = new ArrayList<String>();
	
	final AdaptationTable managedSegGeoMapTable = metadataDataSet.getTable(
			GOCWFPaths._C_DSMT_MANSEG_MANGEO.getPathInSchema());
	final String predicate = GOCWFPaths._C_DSMT_MANSEG_MANGEO._MAN_SEG_APPROVAL_POINT.format()
			+ "=\"" + msCP + "\" and "
			+ GOCWFPaths._C_DSMT_MANSEG_MANGEO._MAN_GEO_APPROVAL_POINT.format()
			+ "=\"" + mgCP + "\"";
	final RequestResult reqRes = managedSegGeoMapTable.createRequestResult(predicate);
	final Adaptation managedSegGeoMapRecord;
	try {
		managedSegGeoMapRecord = reqRes.nextAdaptation();
	} finally {
		reqRes.close();
	}
	if (managedSegGeoMapRecord == null) {
		
		LOG.error("No record found for predicate " + predicate
				+ " in table " + managedSegGeoMapTable.getTablePath().format());
		return null;
	}
	else{
		
		roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._FINANCE_ROLE_ID1));
		roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._FINANCE_ROLE_ID2));
		roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._FINANCE_ROLE_ID3));
		roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._FINANCE_ROLE_ID4));
		roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._FINANCE_ROLE_ID5));
		roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._HR_ROLE_ID1));
		roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._HR_ROLE_ID2));
		roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._HR_ROLE_ID3));
		roles.add(managedSegGeoMapRecord.getString(GOCWFPaths._C_DSMT_MANSEG_MANGEO._HR_ROLE_ID4));
	}
	return roles;
}
*/	
private static String endDatePredicate(String endDateField){
		
		
		return " ( osd:is-null(" + endDateField + ") or date-equal(" + endDateField + ", '9999-12-31') )";
	}


private static List<String> getRegionalFinanceRole(
		final String sid, final String bu, final List<String> msAP,
		final Adaptation metadataDataSet) {
	
	final AdaptationTable ccTable =	metadataDataSet.getTable(GOCWFPaths._C_DSMT_CC_FRSBU.getPathInSchema());
	
	RequestResult reqRes = null;
	Adaptation ccRecord = null;
	
	for (String msApprovalPoint : msAP) {
		
		final String predicate = GOCWFPaths._C_DSMT_CC_FRSBU._SID.format()
		+ "=\"" + sid + "\" and "
		+ GOCWFPaths._C_DSMT_CC_FRSBU._FRS_BU.format()
		+ "=\"" + bu + "\" and "
		+ GOCWFPaths._C_DSMT_CC_FRSBU._MNG_SEG_APP_POINT.format() 
		+ "=\"" + msApprovalPoint + "\"";
		
		//LOG.info( " predicate for getRegionalFinanceRole  is "+ predicate);
		reqRes = ccTable.createRequestResult(predicate);
		
		if (null != reqRes) {
			
			ccRecord = reqRes.nextAdaptation();
			if(null !=  ccRecord){
				LOG.info("Regional Finance Role mapping found for sid " + sid + ", BU " + bu + " and MS "+ msApprovalPoint );
				break; //stop as soon as first record is found from bottom in the Managed Segment Hierarchy
			}
			
			reqRes.close();
			
		}
	}
	
	final ArrayList<String> roles = new ArrayList<String>();
	if (ccRecord == null) {
		
		LOG.info("No record found for sid " + sid + ", BU " + bu + " and MS " + msAP + " in table " + ccTable.getTablePath().format() +". Default Country Controller will be set." );
	
		roles.add(REGIONAL_FINANCE_DEFAULT_ROLE);
	
	}else{
	
		LOG.info("Regional finance Role ID found for sid = " + sid + " and msAP = " + msAP + " is "  + ccRecord.getString(GOCWFPaths._C_DSMT_CC_FRSBU._CC_ROLE_ID));
		roles.add(ccRecord.getString(GOCWFPaths._C_DSMT_CC_FRSBU._REG_FIN));
	}
	return roles;
}
}
