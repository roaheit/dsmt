package com.citi.ebx.goc.valuefunction;

import com.citi.ebx.goc.path.GOCPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class SubtableApproverGroupValueFunction implements ValueFunction {
	private static final Path GOC_TABLE_PATH =
			GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC.getPathInSchema();
	private static final Path APPROVER_GROUP_FIELD =
			GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup;
	
	// Assuming the foreign key field name in each sub-table will be same,
	// so just picking one of the tables here to get the field from
	private Path gocFKField =
			GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM._C_GOC_FK;
	
	public String getGocFKField() {
		return gocFKField.format();
	}

	public void setGocFKField(String gocFKField) {
		this.gocFKField = Path.parse(gocFKField);
	}

	@Override
	public Object getValue(Adaptation adaptation) {
		final String fkValue = adaptation.getString(gocFKField);
		if (fkValue == null) {
			return null;
		}
		final Adaptation dataSet = adaptation.getContainer();
		final AdaptationTable gocTable = dataSet.getTable(GOC_TABLE_PATH);
		final Adaptation gocRecord = gocTable.lookupAdaptationByPrimaryKey(
				PrimaryKey.parseString(fkValue));
		if (gocRecord == null) {
			return null;
		}
		return gocRecord.getString(APPROVER_GROUP_FIELD);
	}

	@Override
	public void setup(ValueFunctionContext context) {
		if (gocFKField == null) {
			context.addError("gocFKField parameter is required.");
		} else {
			SchemaNode gocFKNode = context.getSchemaNode().getNode(Path.PARENT.add(gocFKField));
			if (gocFKNode == null) {
				context.addError("gocFKField parameter " + gocFKField.format() + " is not a node in the schema.");
			}
		}
	}
}
