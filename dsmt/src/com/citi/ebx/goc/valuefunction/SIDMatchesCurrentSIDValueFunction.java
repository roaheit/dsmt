package com.citi.ebx.goc.valuefunction;

import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class SIDMatchesCurrentSIDValueFunction implements ValueFunction {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	@Override
	public Object getValue(Adaptation adaptation) {
	
	/** set to true if not in workflow mode. */
	if (! isInWorkflow(adaptation.getContainerTable().getContainerAdaptation())) {
		
		return true;
	
	}else{
	
	/** filter based on SID */
		
		final String currentSID = GOCWorkflowUtils.getCurrentSID(adaptation.getContainer());
		if (currentSID == null || "".equals(currentSID)) {
			return true;
		}
		
		final Path tablePath = adaptation.getContainerTable().getTablePath();
		final Adaptation gocRecord;
		if (GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC.getPathInSchema()
				.equals(tablePath)) {
			gocRecord = adaptation;
		} else {
			// Assume all subtables use same field name for the goc fk
			gocRecord = Utils.getLinkedRecord(adaptation,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM._C_GOC_FK);
			if (gocRecord == null) {
				LOG.debug("GOC record " + adaptation.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM._C_GOC_FK)
						+ " not found for record " + adaptation.getOccurrencePrimaryKey()
						+ " in table " + tablePath);
				return null;
			}
		}
		
		final Adaptation sidRecord = Utils.getLinkedRecord(gocRecord,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
		if (sidRecord == null) {
			LOG.debug("SID record " + adaptation.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID)
					+ " not found for GOC record " + gocRecord.getOccurrencePrimaryKey()
					+ " (GOC = " + gocRecord.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC) + ")");
			return null;
		}
		
		final String sid = sidRecord.getString(
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID);
		
		return currentSID.equals(sid);
	}
	
	}

	@Override
	public void setup(ValueFunctionContext context) {
		// do nothing
	}
	

	private static boolean isInWorkflow(final Adaptation dataSet) {
		final String currentSid = GOCWorkflowUtils.getCurrentSID(dataSet);
		return (currentSid != null && ! "".equals(currentSid));
	}
}
