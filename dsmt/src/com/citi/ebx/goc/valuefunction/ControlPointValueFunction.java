package com.citi.ebx.goc.valuefunction;

import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class ControlPointValueFunction implements ValueFunction {
	private Path thisPath;
	
	@Override
	public Object getValue(Adaptation adaptation) {
		final Path tablePath = adaptation.getContainerTable().getTablePath();
		final Path cpPath;
		final Path idPath;
		final Path fkPath;
		if (DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW.getPathInSchema()
				.equals(tablePath)) {
			cpPath = DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MS_CNTL_PT;
			idPath = DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MAN_SEG_ID;
			fkPath = DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._Parent_FK;
		} else {
			cpPath = DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_MG_CNTL_PT;
			idPath = DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_MAN_GEO_ID;
			fkPath = DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._Parent_FK;
		}
		
		final String isCP = adaptation.getString(cpPath);
		if ("Y".equalsIgnoreCase(isCP)) {
			return adaptation.getString(idPath);
		}
		final Adaptation parentRecord = Utils.getLinkedRecord(adaptation, fkPath);
		if (parentRecord == null) {
			return adaptation.getString(idPath);
		}
		return parentRecord.getString(Path.SELF.add(thisPath));
	}

	@Override
	public void setup(ValueFunctionContext context) {
		thisPath = context.getSchemaNode().getPathInAdaptation();
		
	}
}
