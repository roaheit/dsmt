package com.citi.ebx.goc.constraint;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.goc.util.IGWGOCWorkflowUtils;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.SchemaTypeName;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;

/**
 * Validates that a change hasn't been made on the table that isn't consistent
 * with the action required value
 */
public class GOCRecordConstraint implements
		ConstraintOnTableWithRecordLevelCheck {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	
	private static final Path ACTION_REQUIRED_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD;
	private static final Path GOC_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC;
	private static final Path COMMENT_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_COMMENT;
	private static final Path SHORT_DESCRIPTION_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCRSHORT;
	private static final Path DESCRIPTION_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCR;
	private static final Path USAGE_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_USAGE;
	private static final Path FRS_BU_ID_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU;
	private static final Path FRS_OU_ID_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_OU;
	private static final Path MAN_SEG_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID;
	private static final Path MAN_GEO_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID;
	private static final Path FUNCTION_ID_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FUNCTION;
	private static final Path LVID_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LVID;
	private static final Path STATUS_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFF_STATUS;
	private static final Path EFF_DATE_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFFDT;
	private static final Path LOC_COST_CD_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD;
	private static final String SID_PUCO_RULE = "PLAN";
	private static final Path LAST_ACTION_REQUIRED = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LAST_ACTION_REQUIRED;
	private static final Path MERGE_DATE = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._MERGE_DATE;
	private static final Path Functional_ID = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Functional_ID;
	private static final Path SID_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID;
	private static final Path ENDDT = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ENDDT;

	private static final Map<String, Set<Path>> ALLOWED_PATH_MAP = new HashMap<String, Set<Path>>();
	static {
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_MODIFY_GOC_DESC_ONLY,
				getAllowedPaths(GOCConstants.ACTION_MODIFY_GOC_DESC_ONLY));
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_MODIFY_GOC_USAGE_ONLY,
				getAllowedPaths(GOCConstants.ACTION_MODIFY_GOC_USAGE_ONLY));
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_MODIFY_MANAGED_SEG_ONLY,
				getAllowedPaths(GOCConstants.ACTION_MODIFY_MANAGED_SEG_ONLY));
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_MODIFY_MANAGED_GEO_ONLY,
				getAllowedPaths(GOCConstants.ACTION_MODIFY_MANAGED_GEO_ONLY));
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_MODIFY_FUNCTION_ONLY,
				getAllowedPaths(GOCConstants.ACTION_MODIFY_FUNCTION_ONLY));
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_DEACTIVATE_GOC_LEG_CCS,
				getAllowedPaths(GOCConstants.ACTION_DEACTIVATE_GOC_LEG_CCS));
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_REACTIVATE_GOC_LEG_CCS,
				getAllowedPaths(GOCConstants.ACTION_REACTIVATE_GOC_LEG_CCS));
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_MODIFY_FRS_BU_ONLY,
				getAllowedPaths(GOCConstants.ACTION_MODIFY_FRS_BU_ONLY));
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_MODIFY_FRS_OU_ONLY,
				getAllowedPaths(GOCConstants.ACTION_MODIFY_FRS_OU_ONLY));
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_NO_CHANGE,
				getAllowedPaths(GOCConstants.ACTION_NO_CHANGE));
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_MODIFY_GL_ONLY,
				getAllowedPaths(GOCConstants.ACTION_MODIFY_GL_ONLY));
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_MODIFY_P2P_ONLY,
				getAllowedPaths(GOCConstants.ACTION_MODIFY_P2P_ONLY));
		ALLOWED_PATH_MAP.put(GOCConstants.ACTION_MODIFY_RE_ONLY,
				getAllowedPaths(GOCConstants.ACTION_MODIFY_RE_ONLY));
	}

	@Override
	public void checkTable(ValueContextForValidationOnTable context) {
		/*
		 * final AdaptationTable table = context.getTable();
		 * LOG.debug("Inside mothod checkTable : "); final Adaptation dataSet =
		 * table.getContainerAdaptation(); if (isInWorkflow(dataSet)) { final
		 * DifferenceBetweenTables diff = getDifference(context);
		 * validateRoutingIDs(context, diff);
		 * 
		 * final boolean validateGOC = GOCWorkflowUtils.isValidateGOC(dataSet);
		 * if (validateGOC) { validateUniqueGOC(context, diff); } }
		 */
	}

	@Override
	public void setup(ConstraintContextOnTable context) {
		// do nothing
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext context)
			throws InvalidSchemaException {
		return "Only the appropriate combination of fields can be modified based on the action required code, and routing must be the same for every record.";
	}

	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
		final AdaptationTable table = context.getTable();
		final Adaptation dataSet = table.getContainerAdaptation();
		final String maintenanceType = GOCWorkflowUtils
				.getMaintenanceType(dataSet);
		boolean performChanges = false;
		if (isInChildDataSpace(dataSet)) {
			if (editChildDataSpace(dataSet, context)) {
				final ValueContext vc = context.getRecord();
				ValueContext oldVC = null;
				// remove any of the previous validation error messages
				context.removeRecordFromMessages(vc);

				final String actionRequired = (String) vc
						.getValue(ACTION_REQUIRED_PATH);
				final String locCostCode = (String) vc
						.getValue(LOC_COST_CD_PATH);
				final String sid = (String) vc
						.getValue(SID_PATH);
				if (actionRequired != null) {

					if((null==locCostCode || locCostCode.isEmpty()) && actionRequired.equalsIgnoreCase("A")){
						performChanges = true;
					}
					else{
						
						final String predicate = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID
								.format()
								+ " = \""
								+ sid
								+ "\" and "
								+ GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD
										.format()
								+ " = \""
								+ locCostCode
								+ "\" ";
						
						final List<Adaptation> duplicateSIDLCC = context.getTable().selectOccurrences(predicate);
						
						if(duplicateSIDLCC.size()>1){
							context.addMessage(UserMessage.createError("SID and Local Cost Code combination should be unique."));
						}else
							performChanges = true;
						
					}
						
				if(performChanges)	{
					
					// ak76503
					checkForUniqueGOCContraints(context);
					final AdaptationTable initialTable = GOCWorkflowUtils
							.getTableInInitialSnapshot(table);

					// Get the record in the initial snapshot, to compare
					// against
					final Adaptation initialRecord = initialTable
							.lookupAdaptationByPrimaryKey(vc);

					Repository repo = context.getTable()
							.getContainerAdaptation().getHome().getRepository();
					Path tablePath = GOCWFPaths._C_DSMT_ACT_ROLE_MAP
							.getPathInSchema();
					Path[] tableColumns = new Path[10];
					tableColumns[0] = GOCWFPaths._C_DSMT_ACT_ROLE_MAP._ACTION_REQ_CODE;
					Adaptation record = null;

					String params = actionRequired;
					try {
						record = GOCWorkflowUtils.getQueryRecord(repo,
								tablePath, tableColumns, params);
					} catch (OperationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					String functionalWF;
					String regularWF;
					// if(GOCConstants.ACTION_NO_CHANGE.equals(actionRequired)){
					if (null != record
							&& !GOCConstants.ACTION_NO_CHANGE
									.equalsIgnoreCase(actionRequired)) {
						LOG.info(" in action record Check actionRequired = " + actionRequired );
						functionalWF = record
								.getString(GOCWFPaths._C_DSMT_ACT_ROLE_MAP._FUNCT_WF);
						regularWF = record
								.getString(GOCWFPaths._C_DSMT_ACT_ROLE_MAP._GOC_WF);

						

						if (GOCConstants.MAINTENANCE_TYPE_FUNCTIONAL
								.equalsIgnoreCase(maintenanceType)
								&& !GOCConstants.CONSTANT_YES
										.equalsIgnoreCase(functionalWF)) {

							context.addMessage(UserMessage
									.createError("The action "
											+ actionRequired
											+ " is not allowed in Functional Maintenance Cycle."));

						} else if (GOCConstants.MAINTENANCE_TYPE_REGULAR
								.equalsIgnoreCase(maintenanceType)
								&& !GOCConstants.CONSTANT_YES
										.equalsIgnoreCase(regularWF)) {

							context.addMessage(UserMessage
									.createError("The action "
											+ actionRequired
											+ " is not allowed in Regular Maintenance Cycle."));

						}
					}

					// If it's a new record just need to check that it's a
					// create action
					if (initialRecord == null) {
						if (!GOCConstants.ACTION_CREATE_GOC_LEG_CCS
								.equals(actionRequired)) {
							context.addMessage(UserMessage
									.createError("Action must be "
											+ GOCConstants.ACTION_CREATE_GOC_LEG_CCS
											+ " for a new record."));
						}

						// context.addMessage(UserMessage.createInfo("Please make sure that a corresponding entry in Partner system tables is present."));

						// Else it's a modify
					} else {

						oldVC = initialRecord.createValueContext();

						if (GOCConstants.ACTION_CREATE_GOC_LEG_CCS
								.equals(actionRequired)) {
							context.addMessage(UserMessage
									.createError("Action "
											+ GOCConstants.ACTION_CREATE_GOC_LEG_CCS
											+ " can't be used when modifying a record."));
						} else {
							final Set<Path> allowedPaths = ALLOWED_PATH_MAP
									.get(actionRequired);

							final SchemaNode node = vc.getNode();

							validateFunctionalActionRequired(context,
									actionRequired, dataSet, vc, oldVC);

							if (allowedPaths != null) {

								validateChanges(node, allowedPaths, context,
										oldVC, vc);
							}

							/**
							 * Local cost code and LVID cannot be changed even
							 * in case of Modify Multiple attributes
							 */
							if (actionRequired
									.equalsIgnoreCase(GOCConstants.ACTION_MODIFY_MULTI_GOC_ATTRS)) {

								validateLocalCostCodeChange(context, oldVC, vc);
								validateLVIDChange(context, oldVC, vc);
							}

						}
					}
				 }
				}
				/*
				 * if (! GOCWorkflowUtils.isSidMatching(vc,
				 * context.getTable().getContainerAdaptation())) {
				 * context.addMessage(UserMessage.createError(
				 * "You're trying to create/modify an unauthorized record. The SID does not correspond to the workflows's SID"
				 * )); }
				 */
				final boolean validateGOC = GOCWorkflowUtils
						.isValidateGOC(dataSet);

				if (validateGOC) {
					final String lcc = String.valueOf(vc
							.getValue(LOC_COST_CD_PATH)); // GOCWorkflowUtils.getLocalCostCode(vc);
					final String currentSID = GOCWorkflowUtils
							.getCurrentSID(dataSet);
					final String gocValue = String.valueOf(vc
							.getValue(GOC_PATH));

					LOG.info("sid = " + currentSID + ", lcc = " + lcc
							+ " and goc = " + gocValue);
					if (lcc == null || "".equals(lcc)) {
						context.addMessage(UserMessage
								.createError("Local Cost Code is required."));
					}
				}
				// TODO-- commented for IGW
				String workflowStatus = IGWGOCWorkflowUtils
						.getWorkflowStatus(dataSet);
				LOG.info("::::::::::::::::::::::::::GOC REWORK REQUEST:::::::::::::::::: "
						+ workflowStatus);
				if ("Resubmitted".equalsIgnoreCase(workflowStatus)) {
					LOG.info("inside resubmitted if condition");
					validateApprovalGroup(context);

				}
				final String env = DSMTUtils.propertyHelper
						.getProp(DSMTConstants.DSMT_ENVIRONMENT);

				/**
				 * perform these validations on UAT and Prod only to speed up
				 * the testing
				 */
				if (!("local".equalsIgnoreCase(env) || "development"
						.equalsIgnoreCase(env))) {
					if (actionRequired != null) {
						// Validation modified as part of CR322 by ak76503
						validateGOCOnActionRequired(actionRequired, context,
								oldVC, vc);

					}
				}

				// check for function table
				/*String functioalID = GOCWorkflowUtils.getFunctionalID(context
						.getTable().getContainerAdaptation());
						*/
				
				String functioalID = (null==vc.getValue(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Functional_ID)?null: (String)vc.getValue(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Functional_ID));
				// methods to validate functionla table
				if (functioalID != null && GOCConstants.MAINTENANCE_TYPE_FUNCTIONAL
						.equalsIgnoreCase(maintenanceType)) {
					validateFunctionalRequest(actionRequired, context, oldVC,
							vc, functioalID);
				}
					if(GOCConstants.MAINTENANCE_TYPE_FUNCTIONAL
							.equalsIgnoreCase(maintenanceType)&& null== functioalID){
						context.addMessage(UserMessage
								.createError("Functional Id can't be null for Functioanl request."));
				}
			} else {
				checkForUniqueGOCContraints(context);
				context.addMessage(UserMessage
						.createError("Cannot edit data in child dataspace as it's pending in approval queue."));
			}
		}
	}

	private void validateFunctionalRequest(String actionRequired,
			ValueContextForValidationOnRecord context, ValueContext oldVC,
			ValueContext vc, String functionalId) {

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		LOG.info("validateFunctionalRequest ::::" + functionalId);
		String tableID = GOCWFPaths._FUNCTIONAL_ID_VALIDATION.getPathInSchema()
				.format();
		final AdaptationTable functionalRestrictionsTable = metadataDataSet
				.getTable(getPath(tableID));

		String predicate = GOCWFPaths._FUNCTIONAL_ID_VALIDATION._FUNCTIONAL_ID
				.format() + " = \"" + functionalId + "\"";
		Adaptation targetRecord = null;
		RequestResult reqRes = functionalRestrictionsTable
				.createRequestResult(predicate);
		if (null != reqRes) {

			targetRecord = reqRes.nextAdaptation();
			reqRes.close();
		}

		if (null != targetRecord
				&& !GOCConstants.ACTION_NO_CHANGE.equalsIgnoreCase(actionRequired)) {
			String functionalAction = targetRecord
					.getString(GOCWFPaths._FUNCTIONAL_ID_VALIDATION._ACTION_REQ_CODE);

			if (null != functionalAction) {
				if (!functionalAction.equalsIgnoreCase(actionRequired)) {
					LOG.info("Action requried " + actionRequired
							+ " is not allowed for Functioal ID  "
							+ functionalId);
					context.addMessage(UserMessage
							.createError("Action requried " + actionRequired
									+ " is not allowed for Functioal ID  "
									+ functionalId));
				} else {

					validateRecOnActionReq(actionRequired,context,oldVC,vc,functionalId,targetRecord);

				}
			} else {
				
					validateRecOnActionReq(actionRequired,context,oldVC,vc,functionalId,targetRecord);
				
		
				/*				
				LOG.info("Action requried  is not Updated in Functional validation table for Functioal ID "
						+ functionalId);
				context.addMessage(UserMessage
						.createError("Action requried  is not Updated in Functional validation table for Functioal ID "
								+ functionalId));
				 */			
				}

		} else {
			if(null == targetRecord){
			LOG.info("Functional ID "+ functionalId+ "is not valid in FunctionalID validation table."
					);
			context.addMessage(UserMessage
					.createError("Functional ID "+ functionalId+ "is not valid in FunctionalID validation table."
							+ functionalId));
		}if(GOCConstants.ACTION_NO_CHANGE.equalsIgnoreCase(actionRequired)){
			LOG.info("Action requried  "+ actionRequired + "is not valid for Functional request.");
			context.addMessage(UserMessage
					.createError("Action requried  "+ actionRequired + "is not valid for Functional request."));
		}
			
		}
		

	}
	
	private void validateRecOnActionReq(String actionRequired,ValueContextForValidationOnRecord context, 
			ValueContext oldVC,ValueContext vc, String functionalId,Adaptation targetRecord){
	
		char actionReq = actionRequired.charAt(0);
		switch (actionReq) {
		case 'K':
			validateMSChange(context, oldVC, vc, targetRecord,
					functionalId);
			break;
		case 'L':
			validateMGChange(context, oldVC, vc, targetRecord,
					functionalId);
			break;
			
		case 'U':
			validateMGChange(context, oldVC, vc, targetRecord,
					functionalId);
			validateMSChange(context, oldVC, vc, targetRecord,
					functionalId);
			break;
		}
	}

	private void validateMSChange(ValueContextForValidationOnRecord context,
			ValueContext oldVC, ValueContext vc, Adaptation functionalReq,
			String functionalId) {

		final Adaptation mSRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
		if (mSRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}

		final Adaptation oldMSRecord = Utils
				.getLinkedRecord(
						oldVC,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
		if (oldMSRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here fv66203
			return;
		}
		

		String mngSeg = mSRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MAN_SEG_ID);

		String oldMNgSeg = oldMSRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MAN_SEG_ID);
		if (mngSeg.equalsIgnoreCase(oldMNgSeg)) {
			// if MS is not changed don't do validation
			// error here fv66203
			return;
		}

		LOG.info("validateMSChange ::::" + functionalId);
		
		if (null != functionalReq
				.getString(GOCWFPaths._FUNCTIONAL_ID_VALIDATION._MS_SE)
				&& null != functionalReq
						.getString(GOCWFPaths._FUNCTIONAL_ID_VALIDATION._MS_RE)) {
			if (!validateMSApprovalPoint(
					oldMSRecord,
					functionalReq
							.getString(GOCWFPaths._FUNCTIONAL_ID_VALIDATION._MS_SE))) {
				LOG.info("MS from " + oldMNgSeg + "  to MS " + mngSeg
						+ " is not allowed for Functioal ID  " + functionalId);
				context.addMessage(UserMessage.createError("MS from "
						+ oldMNgSeg + "  to MS " + mngSeg
						+ " is not allowed for Functioal ID  " + functionalId));
			}
			if (!validateMSApprovalPoint(
					mSRecord,
					functionalReq
							.getString(GOCWFPaths._FUNCTIONAL_ID_VALIDATION._MS_RE))) {
				LOG.info("MS from " + oldMNgSeg + "  to MS " + mngSeg
						+ " is not allowed to Functioal ID  " + functionalId);
				context.addMessage(UserMessage.createError("MS from "
						+ oldMNgSeg + "  to MS " + mngSeg
						+ " is not allowed for Functioal ID  " + functionalId));
			}

		}

	}

	private boolean validateMSApprovalPoint(Adaptation mSRecord,
			String msControlPoint) {
		List<String> msAppPoint = getFunctionalManagedSegApprovalPoint(
				mSRecord, msControlPoint);
		if (msAppPoint.contains(msControlPoint)) {
			return true;
		} else {
			return false;
		}

	}

	private static List<String> getFunctionalManagedSegApprovalPoint(
			final Adaptation msRecord, String msControlPoint) {
		ArrayList<String> msAppPoint = new ArrayList<String>();

		final Adaptation metadataDataSet;
		String msFK;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(msRecord.getHome()
					.getRepository(), DSMTConstants.GOC, DSMTConstants.GOC);
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return null;
		}

		msFK = msRecord
				.getString(GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._Parent_FK);
		msAppPoint
				.add(msRecord
						.getString(GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._C_DSMT_MAN_SEG_ID));
		AdaptationTable table = metadataDataSet
				.getTable(GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD
						.getPathInSchema());
		getFunctionalParentNode(
				table,
				msFK,
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._Parent_FK,
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._C_DSMT_MAN_SEG_ID,
				msAppPoint, msControlPoint);

		LOG.info("Manage SEG List >> " + msAppPoint);

		return msAppPoint;

	}

	private void validateMGChange(ValueContextForValidationOnRecord context,
			ValueContext oldVC, ValueContext vc, Adaptation functionalReq,
			String functionalId) {

		final Adaptation mGRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID);
		if (mGRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}

		final Adaptation oldMGRecord = Utils
				.getLinkedRecord(
						oldVC,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID);
		if (oldMGRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here fv66203
			return;
		}
		
	//	ValueContext vc1 = mGRecord.createValueContext();
		
		final Adaptation record = Utils.getLinkedRecord(mGRecord, GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._HISTORY_FK);
		final Adaptation record1 = Utils.getLinkedRecord(oldMGRecord, GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._HISTORY_FK);
		
		String newEMDMAttr = record.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_EM_DM_ATTR);
		String oldEMDMAttr = record1.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_EM_DM_ATTR);
		
		LOG.info("check new EMDM attribute "+newEMDMAttr);
		LOG.info("check old EMDM attribute "+oldEMDMAttr);
		
		
	
		String mngGeo = mGRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_MAN_GEO_ID);

		String oldMG = oldMGRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_MAN_GEO_ID);
		
		if (mngGeo.equalsIgnoreCase(oldMG)) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		
		if(!newEMDMAttr.equalsIgnoreCase(oldEMDMAttr)){
			context.addMessage(UserMessage.createWarning("MG from " + oldMG
					+ "  to MG " + mngGeo
					+ " is not allowed for Functioal ID  " + functionalId + " as EM/DM Attribute for both are different."));
			return;
		}
		
		LOG.info("validateMGChange ::::" + functionalId);
		if (null != functionalReq
				.getString(GOCWFPaths._FUNCTIONAL_ID_VALIDATION._MG_SE)
				&& null != functionalReq
						.getString(GOCWFPaths._FUNCTIONAL_ID_VALIDATION._MG_RE)) {

			if (!validateMGApprovalPoint(
					oldMGRecord,
					functionalReq
							.getString(GOCWFPaths._FUNCTIONAL_ID_VALIDATION._MG_SE))) {
				LOG.info("MG from " + oldMG + "  to MG " + mngGeo
						+ " is not allowed for Functioal ID  " + functionalId);
				context.addMessage(UserMessage.createError("MG from " + oldMG
						+ "  to MG " + mngGeo
						+ " is not allowed for Functioal ID  " + functionalId));
			}
			if (!validateMGApprovalPoint(
					mGRecord,
					functionalReq
							.getString(GOCWFPaths._FUNCTIONAL_ID_VALIDATION._MG_RE))) {
				LOG.info("MG from " + oldMG + "  to MG " + mngGeo
						+ " is not allowed to Functioal ID  " + functionalId);
				context.addMessage(UserMessage.createError("MG from " + oldMG
						+ "  to MG " + mngGeo
						+ " is not allowed for Functioal ID  " + functionalId));
			}
		}

	}

	private boolean validateMGApprovalPoint(Adaptation mGRecord,
			String mgControlPoint) {
		List<String> mgAppPoint = getFunctionalManagedGEOApprovalPoint(
				mGRecord, mgControlPoint);
		if (mgAppPoint.contains(mgControlPoint)) {
			return true;
		} else {
			return false;
		}

	}

	private static List<String> getFunctionalManagedGEOApprovalPoint(
			final Adaptation msRecord, String mgControlPoint) {
		ArrayList<String> msAppPoint = new ArrayList<String>();

		final Adaptation metadataDataSet;
		String msFK;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(msRecord.getHome()
					.getRepository(), DSMTConstants.GOC, DSMTConstants.GOC);
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return null;
		}

		msFK = msRecord
				.getString(GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._Parent_FK);
		msAppPoint
				.add(msRecord
						.getString(GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._C_DSMT_MAN_GEO_ID));
		AdaptationTable table = metadataDataSet
				.getTable(GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD
						.getPathInSchema());
		getFunctionalParentNode(
				table,
				msFK,
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._Parent_FK,
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._C_DSMT_MAN_GEO_ID,
				msAppPoint, mgControlPoint);

		LOG.info("Manage Geo List >> " + msAppPoint);

		return msAppPoint;

	}

	public static List<String> getFunctionalParentNode(AdaptationTable table,
			String tableFK, Path parentFK, Path setColunName,
			ArrayList<String> msList, String controlPoint) {

		Adaptation parentRecord = table.lookupAdaptationByPrimaryKey(PrimaryKey
				.parseString(tableFK));
		if (parentRecord == null) {
			return msList;
		} else {
			String value = parentRecord.getString(setColunName);
			msList.add(value);
			String msFK = parentRecord.getString(parentFK);
			if (msFK != null) {
				if (controlPoint.equalsIgnoreCase(value)) {
					return msList;
				} else {
					getFunctionalParentNode(table, msFK, parentFK,
							setColunName, msList, controlPoint);
				}

			}

		}

		return msList;
	}

	private Path getPath(final String fieldName) {

		return Path.parse(fieldName);
	}

	private static void validateGOCOnActionRequired(String actionRequired,
			ValueContextForValidationOnRecord context, ValueContext oldVC,
			ValueContext vc) {

		char actionReq = actionRequired.charAt(0);
		LOG.info("Validation on GOC for action required : " + actionReq);
		switch (actionReq) {
		case 'A':
			validateBUNodeRestrictions(context);
			validateGOCUsage(context);
			validateGOCWithBuLvid(context);
			validateGOCMsgFuncCross(context);
			validateOuToManagedSegGeoMappingNew(context);
			validateLvidNodeRestrictions(context);
			validatePUCORule(context);

			break;
		case 'B':
			break;
		case 'D':
			break;

		case 'E':
			validateBUNodeRestrictions(context);
			validateGOCUsage(context);
			validateGOCWithBuLvid(context);
			validateGOCMsgFuncCross(context);
			validateOuToManagedSegGeoMappingNew(context);
			validateLvidNodeRestrictions(context);
			validatePUCORule(context);

			break;
		case 'F':
			break;
		case 'G':
			validateGOCUsage(context);
			break;
		case 'H':
			validateBUNodeRestrictions(context);
			validateGOCWithBuLvid(context);
			break;
		case 'J':
			validateOuToManagedSegGeoMappingNew(context);
			break;
		case 'K':
			validateLvidNodeRestrictions(context);
			validateGOCMsgFuncCross(context);
			validateHLNodeRestrictions(context);
			validateOuToManagedSegGeoMappingNew(context);

			break;
		case 'L':
			validateOuToManagedSegGeoMappingNew(context);
			break;
		case 'M':
			validateGOCMsgFuncCross(context);
			break;
		case 'N':
			break;
		case 'O':
			break;
		case 'P':
			break;
		case 'U':
			validateBUNodeRestrictions(context);
			validateGOCUsage(context);
			validateGOCWithBuLvid(context);
			validateGOCMsgFuncCross(context);
			validateOuToManagedSegGeoMappingNew(context);
			validateLvidNodeRestrictions(context);
			validatePUCORule(context);
			break;
		}
	}

	private static void validateLocalCostCodeChange(
			ValueContextForValidationOnRecord context, ValueContext oldVC,
			ValueContext vc) {

		/* Local Cost Code cannot be changed. */
		String lccValue = String.valueOf(vc.getValue(LOC_COST_CD_PATH));
		String oldLccValue = String.valueOf(oldVC.getValue(LOC_COST_CD_PATH));

		if (!lccValue.equalsIgnoreCase(oldLccValue)) {
			LOG.info("Local Cost Code can not be changed. Kindly use original Local Cost Code "
					+ oldLccValue);
			context.addMessage(UserMessage
					.createError("Local Cost Code can not be changed. Kindly use original Local Cost Code "
							+ oldLccValue));
		}

	}

	private static void validateLVIDChange(
			ValueContextForValidationOnRecord context, ValueContext oldVC,
			ValueContext vc) {

		final Adaptation currentLVIDRecord = Utils.getLinkedRecord(vc,
				LVID_PATH);
		final Adaptation previousLVIDRecord = Utils.getLinkedRecord(oldVC,
				LVID_PATH);

		if (currentLVIDRecord == null || previousLVIDRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String currentLVID = currentLVIDRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_C_DSMT_LVID_TBL._C_DSMT_LVID);
		final String previousLVID = previousLVIDRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_C_DSMT_LVID_TBL._C_DSMT_LVID);

		if (!currentLVID.equalsIgnoreCase(previousLVID)) {
			LOG.info("LVID can not be changed. Original LVID : " + previousLVID
					+ " and current LVID : " + currentLVID
					+ ". Please use original LVID to proceed");
			context.addMessage(UserMessage
					.createError("LVID can not be changed. Original LVID : "
							+ previousLVID + " and current LVID : "
							+ currentLVID
							+ ". Please use original LVID to proceed."));
		}
	}

	/**
	 * GOC_Rule_055 GOC_BU_CROSS_VALIDATION No GOC should be allowed to be
	 * mapped to BU = 00000 BU Node Restrictions Table The BU in the request
	 * should not roll up to a BU present in the BU Node Restrictions Table else
	 * the validation fails
	 * 
	 * @param context
	 */
	private static void validateBUNodeRestrictions(
			ValueContextForValidationOnRecord context) {

		LOG.debug("BU Node Restrictions validation start ");

		final ValueContext vc = context.getRecord();
		// get BU
		final Adaptation frsBuRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU);
		if (frsBuRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String frsBu = frsBuRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_BU_STD_C_DSMT_FRS_BU._C_DSMT_FRS_BU);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		// get Table handler for BU Node restrictions table under GOC_WF
		// dataspace.
		final AdaptationTable buNodeRestrictionsTable = metadataDataSet
				.getTable(GOCWFPaths._C_DSMT_BUND_RES.getPathInSchema());

		/** Check if the table is present */
		if (null != buNodeRestrictionsTable) {

			final PrimaryKey buNodeRestrictionsTablePK = buNodeRestrictionsTable
					.computePrimaryKey(new Object[] { frsBu });
			final Adaptation buNodeRestrictionsRecord = buNodeRestrictionsTable
					.lookupAdaptationByPrimaryKey(buNodeRestrictionsTablePK);

			LOG.debug("validateBUNodeRestrictions frsBu  = " + frsBu
					+ ", buNodeRestrictionsRecord = "
					+ buNodeRestrictionsRecord);

			if (buNodeRestrictionsRecord != null) {
				/**
				 * if BU ID is found in the BU Node restrictions table, the
				 * request should NOT be allowed to proceed.
				 */
				LOG.info("The BU "
						+ frsBu
						+ " is present in the BU Node Restrictions Table. Kindly use a different BU to proceed.");
				context.addMessage(UserMessage
						.createError("The BU "
								+ frsBu
								+ " is present in the BU Node Restrictions Table. Please use a different BU to proceed."));
			} else {
				/** proceed. */
				LOG.debug("BU Node Restrictions validation passed ");
				return;
			}

		} else {
			LOG.error("BU Node restrictions table not found under the GOC_WF dataspace.");
		}

		LOG.debug("BU Node Restrictions validation finished ");

	}

	private static boolean isInWorkflow(final Adaptation dataSet) {
		final String currentSid = GOCWorkflowUtils.getCurrentSID(dataSet);
		return (currentSid != null && !"".equals(currentSid));
	}

	private static boolean isInChildDataSpace(final Adaptation dataSet) {

		LOG.debug("isInChildDataSpace called ");
		AdaptationHome dataspace = dataSet.getHome();

		if (dataspace.toString().contains("GOC")) {
			LOG.debug("Is in master DataSpace");
			return false;
		} else {
			LOG.debug("Is in Child DataSpace");
			return true;
		}
	}

	/**
	 * Business Rule - If the Action Required Code is either of J, K, L, or M
	 * (involving remaps of GOCs� OU, Managed Segment, Managed Geography, or
	 * Function), the Processing Type should be �Functional� and the Functional
	 * maintenance cycle should be open (refer section 10.9.1.1.3 Calendar Set
	 * up) A WARNING should be given to the user that �The required action can
	 * only be requested during the Functional Maintenance Cycle�
	 * 
	 * @param context
	 * @param actionRequired
	 * @param dataSet
	 */
	private static void validateFunctionalActionRequired(
			final ValueContextForValidationOnRecord context,
			final String actionRequired, final Adaptation dataSet,
			final ValueContext vc, final ValueContext oldVC

	) {

		final String maintenanceType = GOCWorkflowUtils
				.getMaintenanceType(dataSet);

		if (GOCConstants.MAINTENANCE_TYPE_REGULAR
				.equalsIgnoreCase(maintenanceType)) {

			if (GOCConstants.ACTION_MODIFY_FRS_OU_ONLY.equals(actionRequired)
					|| GOCConstants.ACTION_MODIFY_MANAGED_SEG_ONLY
							.equals(actionRequired)
					|| GOCConstants.ACTION_MODIFY_MANAGED_GEO_ONLY
							.equals(actionRequired)
					|| GOCConstants.ACTION_MODIFY_FUNCTION_ONLY
							.equals(actionRequired)) {

				context.addMessage(UserMessage
						.createError("The action "
								+ actionRequired
								+ " can only be requested during the Functional Maintenance Cycle."));

			} else if (GOCConstants.ACTION_MODIFY_MULTI_GOC_ATTRS
					.equals(actionRequired)) {

				if (isFunctionalChangePerformed(context, vc, oldVC)) {
					LOG.info("The requested action "
							+ actionRequired
							+ " contains functional changes where as maintenance type selected was Regular.");
				}
			}
			/*
			 * else if
			 * (GOCConstants.ACTION_MODIFY_FRS_BU_ONLY.equals(actionRequired)) {
			 * 
			 * context.addMessage(UserMessage.createWarning("The action " +
			 * actionRequired +
			 * " should only be requested during Functional Maintenance Cycle."
			 * ));
			 * 
			 * }
			 */
		} 
	}

	private static boolean isFunctionalChangePerformed(
			ValueContextForValidationOnRecord context, final ValueContext vc,
			final ValueContext oldVC) {

		boolean isOUChanged = isFieldChanged(
				context,
				vc,
				oldVC,
				FRS_OU_ID_PATH,
				"FRS OU",
				DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OU_STD_C_DSMT_FRS_OU._C_DSMT_FRS_OU,
				DSMTConstants.ERROR);
		boolean isBUChanged = isFieldChanged(
				context,
				vc,
				oldVC,
				FRS_BU_ID_PATH,
				"FRS BU",
				DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_BU_STD_C_DSMT_FRS_BU._C_DSMT_FRS_BU,
				DSMTConstants.WARNING);

		boolean isMSChanged = isFieldChanged(
				context,
				vc,
				oldVC,
				MAN_SEG_PATH,
				"Managed Segment ID",
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MAN_SEG_ID,
				DSMTConstants.ERROR);
		boolean isMGChanged = isFieldChanged(
				context,
				vc,
				oldVC,
				MAN_GEO_PATH,
				"Managed Geography ID",
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_MAN_GEO_ID,
				DSMTConstants.ERROR);
		boolean isFunctionChanged = isFieldChanged(
				context,
				vc,
				oldVC,
				FUNCTION_ID_PATH,
				"Function ID",
				DSMT2HierarchyPaths._GLOBAL_STD_PMF_STD_F_FUNCTION_STD_Function_New2._C_DSMT_FUNCTION,
				DSMTConstants.ERROR);

		if (isOUChanged || isMSChanged || isMGChanged || isFunctionChanged) {
			return true;
		}

		/*
		 * if (isBUChanged) { LOG.info(
		 * "GOC Workflow Warning message - The requested action contains BU changes where as maintenance type selected was Regular."
		 * ); }
		 */
		return false;
	}

	private static boolean isFieldChanged(
			final ValueContextForValidationOnRecord context,
			final ValueContext vc, final ValueContext oldVC,
			final Path fieldPath, final String fieldName,
			final Path targetTableFieldPath, final String messageType) {

		final Adaptation currentRecord = Utils.getLinkedRecord(vc, fieldPath);
		final Adaptation previousRecord = Utils.getLinkedRecord(oldVC,
				fieldPath);

		if (currentRecord == null || previousRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return false;
		}
		final String currentValue = currentRecord
				.getString(targetTableFieldPath);
		final String previousValue = previousRecord
				.getString(targetTableFieldPath);

		if (!currentValue.equalsIgnoreCase(previousValue)) {

			String message = "Functional Change is not possible during regular maintenance. "
					+ fieldName
					+ " can not be changed. Original value : "
					+ previousValue
					+ " and current value : "
					+ currentValue
					+ ". Please use original value to proceed";

			if (fieldName.equalsIgnoreCase("FRS OU")) {
				message = fieldName + " is being changed.  Original value : "
						+ previousValue + " and current value : "
						+ currentValue + ".";
			}

			LOG.info(message);
			if (DSMTConstants.ERROR.equalsIgnoreCase(messageType)) {
				context.addMessage(UserMessage.createError(message));
			} else if (DSMTConstants.WARNING.equalsIgnoreCase(messageType)) {
				context.addMessage(UserMessage.createWarning(message));
			} else if (DSMTConstants.INFO.equalsIgnoreCase(messageType)) {
				context.addMessage(UserMessage.createInfo(message));
			}
			return true;
		}
		return false;

	}

	private static boolean errorMsg = true;

	private static void validateChanges(final SchemaNode node,
			final Set<Path> allowedPaths,
			final ValueContextForValidationOnRecord context,
			final ValueContext oldVC, final ValueContext newVC) {
		HashSet<Path> auditColumns = new HashSet<Path>();
		addAuditFieldPath(auditColumns);
		auditColumns.add(ACTION_REQUIRED_PATH);
		auditColumns.add(COMMENT_PATH);
		auditColumns.add(EFF_DATE_PATH);
		auditColumns.add(LAST_ACTION_REQUIRED);
		auditColumns.add(MERGE_DATE);
		auditColumns.add(Functional_ID);

		// If it's a terminal node, meaning it contains an actual value
		if (node.isTerminalValue()) {
			// Skip computed fields and selection nodes
			if (!node.isValueFunction() && !node.isSelectNode()) {
				// If the path isn't in the list of paths that's allowed to be
				// changed
				if (!allowedPaths.contains(Path.SELF.add(node
						.getPathInAdaptation()))) {
					final Object oldValue = oldVC.getValue(node);
					final Object newValue = newVC.getValue(node);
					// Add an error message if there's a change to that field
					if (!Utils.objectsEqual(oldValue, newValue)) {
						context.addMessage(createInvalidFieldMessage(newVC,
								node, oldValue, newValue));
					}
				} else if (auditColumns.contains(Path.SELF.add(node
						.getPathInAdaptation()))) {
					// System.out.println("inside else if 2"+node.getPathInAdaptation().toString());
				} else {
					final Object oldValue = oldVC.getValue(node);
					final Object newValue = newVC.getValue(node);
					// System.out.println("inside else 2 = "+oldValue+"  new value  = "
					// + newValue);
					// System.out.println("oldValue = "+oldValue.toString()+
					// "   new Value = "+newValue.toString());
					if ((oldValue.toString()).equalsIgnoreCase(newValue
							.toString())) {
						System.out.println(" node path "
								+ node.getPathInAdaptation());
						// }
						// if (Utils.objectsEqual(oldValue, newValue)) {
						// System.out.println("inside if 4  old value = "+oldValue+"  new value  = "
						// + newValue+ " errorMsg  = " +errorMsg);

					} else {
						// System.out.println("inside else 4");
						errorMsg = false;

					}
				}
			}
			// Else it's not terminal so loop through all of its children and do
			// the same for those
		} else {
			final SchemaNode[] nodeChildren = node.getNodeChildren();
			for (int i = 0; i < nodeChildren.length; i++) {
				validateChanges(nodeChildren[i], allowedPaths, context, oldVC,
						newVC);
			}
			final String actionRequired = (String) newVC
					.getValue(ACTION_REQUIRED_PATH);
			if (errorMsg
					&& !(GOCConstants.ACTION_NO_CHANGE
							.equalsIgnoreCase(actionRequired))) {
				// System.out.println("inside if 6");
				context.addMessage(createNoFieldChangeMessage(newVC, node));
			}
			errorMsg = true;
		}
	}

	private static DifferenceBetweenTables getDifference(
			final ValueContextForValidationOnRecord context) {
		final AdaptationTable table = context.getTable();
		final AdaptationTable initialTable = GOCWorkflowUtils
				.getTableInInitialSnapshot(table);
		return DifferenceHelper.compareAdaptationTables(initialTable, table,
				false);
	}

	private static void validateRoutingIDs(
			final ValueContextForValidationOnRecord context,
			DifferenceBetweenTables diff) {
		String routingID = null;

		@SuppressWarnings("unchecked")
		List<ExtraOccurrenceOnRight> adds = diff.getExtraOccurrencesOnRight();
		for (ExtraOccurrenceOnRight add : adds) {
			final String thisRoutingID = add
					.getExtraOccurrence()
					.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup);
			if (routingID == null) {
				routingID = thisRoutingID;
			} else if (!Utils.objectsEqual(routingID, thisRoutingID)) {
				context.addMessage(UserMessage
						.createError("Workflow routing roles aren't the same for every record in the change set."));
				LOG.error("Workflow routing roles aren't the same for every record in the change set. Please use one combination of SID, BU, Managed Segment Control Point and Managed Geography Control Point.");
				return;
			}
		}
		@SuppressWarnings("unchecked")
		final List<DifferenceBetweenOccurrences> deltas = diff
				.getDeltaOccurrences();
		for (DifferenceBetweenOccurrences delta : deltas) {
			final String thisRoutingID = delta
					.getOccurrenceOnRight()
					.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup);
			if (routingID == null) {
				routingID = thisRoutingID;
			} else if (!Utils.objectsEqual(routingID, thisRoutingID)) {
				context.addMessage(UserMessage
						.createError("Workflow routing roles aren't the same for every record in the change set."));
				LOG.error("Workflow routing roles aren't the same for every record in the change set.");
				return;
			}
		}
	}

	protected static void validateUniqueGOC(
			final ValueContextForValidationOnTable context,
			final DifferenceBetweenTables diff) {
		// Track the ones we've processed here, so that it can potentially
		// cut down on the number of queries we need to run
		final HashSet<String> processedSemanticPKs = new HashSet<String>();
		@SuppressWarnings("unchecked")
		List<ExtraOccurrenceOnRight> adds = diff.getExtraOccurrencesOnRight();
		for (ExtraOccurrenceOnRight add : adds) {
			final Adaptation record = add.getExtraOccurrence();
			if (!validateRecordForUniqueGOC(context, record,
					processedSemanticPKs)) {
				return;
			}
		}
		@SuppressWarnings("unchecked")
		final List<DifferenceBetweenOccurrences> deltas = diff
				.getDeltaOccurrences();
		for (DifferenceBetweenOccurrences delta : deltas) {
			final Adaptation record = delta.getOccurrenceOnRight();
			if (!validateRecordForUniqueGOC(context, record,
					processedSemanticPKs)) {
				return;
			}
		}
	}

	private static boolean validateRecordForUniqueGOC(
			final ValueContextForValidationOnTable context,
			final Adaptation record, final Set<String> processedSemanticPKs) {
		final int pk = record
				.get_int(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK);
		final String setId = record
				.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._SETID);
		final String goc = record
				.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC);
		final Date effDt = record
				.getDate(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFFDT);
		// if any of these are null, other constraints will catch those so just
		// ignore here
		if (setId == null || "".equals(setId) || goc == null || "".equals(goc)
				|| effDt == null) {
			return true;
		} else {
			final SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd");
			final String effDtStr = dateFormat.format(effDt);
			final String semanticPK = setId + PrimaryKey.SEPARATOR_CHAR + goc
					+ PrimaryKey.SEPARATOR_CHAR + effDtStr;
			if (processedSemanticPKs.contains(semanticPK)) {
				context.addMessage(createDuplicateGOCMessage(pk));
				return false;
			}
			final String predicate = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK
					.format()
					+ " != "
					+ pk
					+ " and "
					+ GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._SETID
							.format()
					+ " = \""
					+ setId
					+ "\" and "
					+ GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC
							.format()
					+ " = \""
					+ goc
					+ "\" and date-equal("
					+ GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFFDT
							.format() + ",\"" + effDtStr + "\")";
			final RequestResult reqRes = context.getTable()
					.createRequestResult(predicate);
			try {
				if (reqRes.nextAdaptation() != null) {
					context.addMessage(createDuplicateGOCMessage(pk));
					return false;
				}
			} finally {
				reqRes.close();
			}
			processedSemanticPKs.add(semanticPK);
			return true;
		}
	}

	private static void validateGOCUsage(
			final ValueContextForValidationOnRecord context) {
		LOG.debug("validateGOCUsage start");
		final ValueContext vc = context.getRecord();
		final Adaptation sidRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
		if (sidRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String sid = sidRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID);

		final Adaptation gocUsageRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_USAGE);
		if (gocUsageRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String gocUsage = gocUsageRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_USE._C_DSMT_GOC_USAGE);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable sidUsageTable = metadataDataSet
				.getTable(GOCWFPaths._C_DSMT_SID_USG_01.getPathInSchema());
		final PrimaryKey sidUsagePK = sidUsageTable
				.computePrimaryKey(new Object[] { sid, gocUsage });
		final Adaptation sidUsageRecord = sidUsageTable
				.lookupAdaptationByPrimaryKey(sidUsagePK);

		// If the combination of sid & usage wasn't found
		if (sidUsageRecord == null) {
			// Check if there are any records with that sid
			RequestResult reqRes = sidUsageTable
					.createRequestResult(GOCWFPaths._C_DSMT_SID_USG_01._SID
							.format() + "=\"" + sid + "\"");
			// If not, need to proceed to check managed segment
			if (reqRes.isEmpty()) {
				final Adaptation msRecord = Utils
						.getLinkedRecord(
								vc,
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
				if (msRecord == null) {
					// Other validation rules handle this situation so no need
					// to add an error here
					return;
				}
				final String msId = msRecord
						.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MAN_SEG_ID);

				final AdaptationTable msUsageTable = metadataDataSet
						.getTable(GOCWFPaths._C_DSMT_SID_USG_02
								.getPathInSchema());
				final PrimaryKey msUsagePK = msUsageTable
						.computePrimaryKey(new Object[] { msId, gocUsage });
				final Adaptation msUsageRecord = msUsageTable
						.lookupAdaptationByPrimaryKey(msUsagePK);

				// If the combination of managed seg & usage wasn't found
				if (msUsageRecord == null) {
					// Check if there are any records with that managed seg
					reqRes = msUsageTable
							.createRequestResult(GOCWFPaths._C_DSMT_SID_USG_02._MAN_SEG_ID
									.format() + "=\"" + msId + "\"");

					// If there are, it's an error
					if (!reqRes.isEmpty()) {
						context.addMessage(UserMessage.createError("GOC Usage "
								+ gocUsage
								+ " not allowed for Managed Segment " + msId));
					}
				}
				// There are records with that sid, but not for this usage so
				// it's an error
			} else {
				context.addMessage(UserMessage.createError("GOC Usage "
						+ gocUsage + " not allowed for SID " + sid));
			}
		}
		LOG.debug("validateGOCUsage end ");
	}

	private static UserMessage createDuplicateGOCMessage(final int pk) {
		return UserMessage.createError("Duplicate GOC found in record " + pk
				+ ". GOC (SID + Local Cost Code) must be unique.");
	}

	private static Set<Path> getAllowedPaths(final String actionRequired) {
		HashSet<Path> allowedPaths = new HashSet<Path>();
		allowedPaths.add(ACTION_REQUIRED_PATH);
		allowedPaths.add(COMMENT_PATH);
		allowedPaths.add(EFF_DATE_PATH);
		allowedPaths.add(LAST_ACTION_REQUIRED);
		allowedPaths.add(MERGE_DATE);
		allowedPaths.add(Functional_ID);

		if (GOCConstants.ACTION_MODIFY_FRS_BU_ONLY.equals(actionRequired)) {
			allowedPaths.add(FRS_BU_ID_PATH);
		} else if (GOCConstants.ACTION_MODIFY_FRS_OU_ONLY
				.equals(actionRequired)) {
			allowedPaths.add(FRS_OU_ID_PATH);
		} else if (GOCConstants.ACTION_MODIFY_FUNCTION_ONLY
				.equals(actionRequired)) {
			allowedPaths.add(FUNCTION_ID_PATH);
		} else if (GOCConstants.ACTION_MODIFY_GOC_DESC_ONLY
				.equals(actionRequired)) {
			allowedPaths.add(SHORT_DESCRIPTION_PATH);
			allowedPaths.add(DESCRIPTION_PATH);
		} else if (GOCConstants.ACTION_MODIFY_GOC_USAGE_ONLY
				.equals(actionRequired)) {
			allowedPaths.add(USAGE_PATH);
		} else if (GOCConstants.ACTION_MODIFY_MANAGED_SEG_ONLY
				.equals(actionRequired)) {
			allowedPaths.add(MAN_SEG_PATH);
		} else if (GOCConstants.ACTION_MODIFY_MANAGED_GEO_ONLY
				.equals(actionRequired)) {
			allowedPaths.add(MAN_GEO_PATH);
		} else if (GOCConstants.ACTION_REACTIVATE_GOC_LEG_CCS
				.equals(actionRequired)) {
			allowedPaths.add(STATUS_PATH);
		} else if (GOCConstants.ACTION_DEACTIVATE_GOC_LEG_CCS
				.equals(actionRequired)) {
			allowedPaths.add(STATUS_PATH);
		} else if (GOCConstants.ACTION_NO_CHANGE.equals(actionRequired)) {
			allowedPaths.remove(COMMENT_PATH);
		}

		addAuditFieldPath(allowedPaths);

		return allowedPaths;
	}

	private static HashSet<Path> addAuditFieldPath(HashSet<Path> allowedPaths) {
		allowedPaths
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CREATED_OPERID);
		allowedPaths
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ORIG_DTTM);
		allowedPaths
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LASTUPDOPRID);
		allowedPaths
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CHNG_DTTM);
		allowedPaths
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ENDDT);
		allowedPaths
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Requested_Date);
		allowedPaths
				.add(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Requested_by);
		return allowedPaths;
	}

	private static UserMessage createInvalidFieldMessage(final ValueContext vc,
			final SchemaNode node, final Object oldValue, final Object newValue) {
		final String actionRequired = (String) vc
				.getValue(ACTION_REQUIRED_PATH);
		// Can create multiple labels for different locales. Only assuming
		// English currently.
		final String label = node.getLabel(Locale.getDefault());
		final StringBuilder msg = new StringBuilder(
				"Invalid change for action \"");
		msg.append(actionRequired);
		msg.append("\". Field \"");
		if (label == null) {
			msg.append(node.getPathInAdaptation().format());
		} else {
			msg.append(label);
		}
		msg.append("\" was changed from ");
		msg.append(getValueString(oldValue, node));
		msg.append(" to ");
		msg.append(getValueString(newValue, node));
		msg.append(".");
		return UserMessage.createError(msg.toString());
	}

	private static UserMessage createNoFieldChangeMessage(
			final ValueContext vc, final SchemaNode node) {
		final String actionRequired = (String) vc
				.getValue(ACTION_REQUIRED_PATH);
		// Can create multiple labels for different locales. Only assuming
		// English currently.
		final String label = node.getLabel(Locale.getDefault());
		final StringBuilder msg = new StringBuilder(
				"No Field change for action \"");
		msg.append(actionRequired);

		msg.append(".");
		return UserMessage.createError(msg.toString());
	}

	private static final String getValueString(final Object value,
			final SchemaNode node) {
		if (value == null) {
			return "null";
		}
		final SchemaTypeName type = node.getXsTypeName();
		if (SchemaTypeName.XS_DATE.equals(type)) {
			final SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd");
			return dateFormat.format((Date) value);
		}
		if (SchemaTypeName.XS_DATETIME.equals(type)) {
			final SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			return dateFormat.format((Date) value);
		}
		if (SchemaTypeName.XS_TIME.equals(type)) {
			final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
			return dateFormat.format((Date) value);
		}
		if (SchemaTypeName.XS_BOOLEAN.equals(type)
				|| SchemaTypeName.XS_INTEGER.equals(type)
				|| SchemaTypeName.XS_INT.equals(type)
				|| SchemaTypeName.XS_DECIMAL.equals(type)) {
			return value.toString();
		}
		return "\"" + value.toString() + "\"";
	}

	/**
	 * GOC should be assigned with a valid/existing BU and LVID combination
	 * which has an ACTIVE status Rule reference table
	 * /root/GLOBAL_STD/OFD_STD/OTH_STD/C_DSMT_FRS_LVID
	 * 
	 * @param context
	 */
	private static void validateGOCWithBuLvid(
			final ValueContextForValidationOnRecord context) {
		LOG.debug("validateGOCWithBuLvid start ");

		final ValueContext vc = context.getRecord();
		// get BU
		final Adaptation frsBuRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU);
		if (frsBuRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String frsBu = frsBuRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_BU_STD_C_DSMT_FRS_BU._C_DSMT_FRS_BU);

		// get LVID
		final Adaptation lvidRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LVID);
		if (lvidRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String lvid = lvidRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_C_DSMT_LVID_TBL._C_DSMT_LVID);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}

		// LOG.info("frsBu"+frsBu+"lvid"+lvid);
		// check if GOC is in result set of C_DSMT_FRS_LVID with above BU and OU
		// and status for record is active.
		final AdaptationTable lvidBuCrossRefTable = metadataDataSet
				.getTable(GOCWFPaths._C_DSMT_FRS_LVID.getPathInSchema());
		// added by fv66203 on 11/16/2015
		// Added perdicate to get current record.
		final String pred2 = "date-equal(" + ENDDT.format() + ",'9999-12-31')";
		final String pred3 = "osd:is-null(" + ENDDT.format() + ")";

		final String predicate = GOCWFPaths._C_DSMT_FRS_LVID._C_DSMT_FRS_B_CHILD
				.format()
				+ " = \""
				+ frsBu
				+ "\" and "
				+ GOCWFPaths._C_DSMT_FRS_LVID._C_DSMT_LVID_CHILD.format()
				+ " = \""
				+ lvid
				+ "\" and "
				+ GOCWFPaths._C_DSMT_FRS_LVID._EFF_STATUS.format()
				+ "=\""
				+ "A" + "\" and ( " + pred2 + " or " + pred3 + ")";
		LOG.debug("validateGOCWithBuLvid - predicate-->" + predicate);

		final RequestResult reqRes = lvidBuCrossRefTable
				.createRequestResult(predicate);
		LOG.debug("size of result set-->" + reqRes.getSize());
		try {
			if (reqRes.nextAdaptation() == null) {
				context.addMessage(UserMessage
						.createError("The combination of BU "
								+ frsBu
								+ " and LVID "
								+ lvid
								+ " is either not present or inactive in the LVID to FRS BU cross reference validation table."));
			}
		} finally {
			reqRes.close();
		}

		LOG.debug("validateGOCWithBuLvid end ");
	}

	/**
	 * GOC should be assigned with Function and Managed Segment attribute/node
	 * based on the Managed Segment to Function roll-up table Step 1 Check the
	 * Managed Segment and look for its control point in Table #1 Step 2 Check
	 * the Function ID and look for its control point in Table #2 Step 3 The
	 * control point in both tables should be equal Step 4 If the control point
	 * did not match, an error message should be prompted; otherwise, the
	 * request is valid based on this check
	 * 
	 * @param context
	 */
	private static void validateGOCMsgFuncCross(
			final ValueContextForValidationOnRecord context) {
		LOG.debug("validateGOCMsgFuncCross start ");
		final ValueContext vc = context.getRecord();
		// get Managed Segment
		final Adaptation mngSegRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
		if (mngSegRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String mngSeg = mngSegRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MAN_SEG_ID);

		// get Function
		final Adaptation funcRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FUNCTION);
		if (funcRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String func = funcRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_PMF_STD_F_FUNCTION_STD_Function_New2._C_DSMT_FUNCTION);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		// check if GOC is assigned with Function and Managed Segment
		// attribute/node based on the Managed Segment to Function roll-up table
		final AdaptationTable mngFuncRefTable = metadataDataSet
				.getTable(GOCWFPaths._C_DSMT_MS_FUN_CP.getPathInSchema());
		final AdaptationTable funcRefTable = metadataDataSet
				.getTable(GOCWFPaths._C_DSMT_FUN_CP.getPathInSchema());

		if (mngFuncRefTable != null && funcRefTable != null) {

			final PrimaryKey mngFuncRefTablePK = mngFuncRefTable
					.computePrimaryKey(new Object[] { mngSeg });
			final Adaptation mngFuncRefRecord = mngFuncRefTable
					.lookupAdaptationByPrimaryKey(mngFuncRefTablePK);

			final PrimaryKey funcRefTablePK = funcRefTable
					.computePrimaryKey(new Object[] { func });
			final Adaptation funcRefRecord = funcRefTable
					.lookupAdaptationByPrimaryKey(funcRefTablePK);
			LOG.debug("funcRefRecord " + funcRefRecord + ", mngFuncRefRecord "
					+ mngFuncRefRecord);
			if (mngFuncRefRecord != null && funcRefRecord != null) {
				final String mngCtrlPt = mngFuncRefRecord
						.getString(GOCWFPaths._C_DSMT_MS_FUN_CP._FUNCTION_ID_CP);
				LOG.debug("mngCtrlPt " + mngCtrlPt);
				final String funcCtrlPt = funcRefRecord
						.getString(GOCWFPaths._C_DSMT_FUN_CP._FUNCTION_ID_CP);
				LOG.debug("funcCtrlPt " + funcCtrlPt);
				if (mngCtrlPt.equals(funcCtrlPt))
					return;
				else
					context.addMessage(UserMessage
							.createError("Function ID Control point values {"
									+ funcCtrlPt
									+ ", "
									+ mngCtrlPt
									+ "} corresponding to the Function ID("
									+ func
									+ ") and Managed Segment("
									+ mngSeg
									+ ") do not match in the Managed Segment to Function Tables under GOC_WF."));
				// "GOC should be assigned with Function and Managed Segment attribute/node based on the Managed Segment to Function roll-up table"));
			} else {
				LOG.error("Managed Segment to Function tables validation could not be run since the validation tables under GOC_WF dataspace are not available.");
			}

		}
		LOG.debug("validateGOCMsgFuncCross end ");
	}

	/*
	 * 1 Workflow will first validate if the OU exist on this cross reference
	 * table 2 If answer to Step 1 is YES, workflow will validate that the
	 * Managed Segment rollup and Managed Geography rollup of the "leaf" values
	 * in the request are mapped to the OU 3 If answer to Step 1 is NO, this
	 * validation rule should reject the request 4 If answer to Step 2 is NO,
	 * this validation rule should reject the request 5 If answer to Step 2 is
	 * YES, this validation rule should approve the request
	 */

	protected static void validateOuToManagedSegGeoMapping(
			final ValueContextForValidationOnRecord context) {
		LOG.debug("validateOuToManagedSegGeoMapping start ");
		// get OU
		final ValueContext vc = context.getRecord();
		// get OU
		final Adaptation frsOURecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_OU);
		if (frsOURecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String frsOU = frsOURecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OU_STD_C_DSMT_FRS_OU._C_DSMT_FRS_OU);

		// get Managed Segment
		final Adaptation mngSegRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
		if (mngSegRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String mngSeg = mngSegRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._ControlPointManagedSegmentID);

		// get Managed Geo
		final Adaptation mngGeoRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID);
		if (null == mngGeoRecord) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}

		final String mngGeo = mngGeoRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._ControlPointManagedGeographyID);

		/*
		 * String predicate = GOCWFPaths._C_DSMT_MNSEG_OU._MAN_GEO_ID.format() +
		 * " = \"" + mngGeo + "\" and " +
		 * GOCWFPaths._C_DSMT_MNSEG_OU._MAN_SEG_ID.format() + " = \"" + mngSeg +
		 * "\" and " + GOCWFPaths._C_DSMT_MNSEG_OU._FRS_OU.format() + " = \"" +
		 * frsOU + "\"";
		 */

		final String predicate = GOCWFPaths._C_DSMT_MNSEG_OU._FRS_OU.format()
				+ " = \"" + frsOU + "\" and "
				+ GOCWFPaths._C_DSMT_MNSEG_OU._MAN_SEG_ID.format() + " = \""
				+ mngSeg + "\" and " + "("
				+ GOCWFPaths._C_DSMT_MNSEG_OU._MAN_GEO_ID.format() + " = \""
				+ mngGeo + "\" " + "	or "
				+ GOCWFPaths._C_DSMT_MNSEG_OU._MAN_GEO_ID.format()
				+ " = \"1000\") and "
				+ GOCWFPaths._C_DSMT_MNSEG_OU._EFF_STATUS.format() + "  = \""
				+ "A" + "\"";

		LOG.debug("validateOuToManagedSegGeoMapping - predicate-->" + predicate);
		// get OU record in 'OT OU to Managed Segment Table
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable ouMSMGtable = metadataDataSet
				.getTable(GOCWFPaths._C_DSMT_MNSEG_OU.getPathInSchema());
		final RequestResult reqRes = ouMSMGtable.createRequestResult(predicate);
		LOG.debug("reqRes-->" + reqRes.getSize());
		try {
			if (reqRes.nextAdaptation() == null) {
				LOG.info("The combination - {OU = "
						+ frsOU
						+ ", Managed Segment Control Point = "
						+ mngSeg
						+ " and Managed Geography Control point = ("
						+ mngGeo
						+ " or 1} is either inactive or not found in the OU MS MG validation table under GOC_WF");
				context.addMessage(UserMessage
						.createError("The combination - {OU = "
								+ frsOU
								+ ", Managed Segment Control Point = "
								+ mngSeg
								+ " and Managed Geography Control point = ("
								+ mngGeo
								+ " or 1000} is either inactive or not found in the OU MS MG validation table under GOC_WF"));
			}
		} finally {
			reqRes.close();
		}

		LOG.debug("validateOuToManagedSegGeoMapping completed. ");
	}

	/***
	 * This validation needs to run if the Managed Segment in the request is
	 * present in the LVID Node Restrictions Table. If yes, the Managed Segment
	 * and LVID combination in the record should not match the ones in the LVID
	 * Node Restrictions Table. If it matches, the validation fails
	 * 
	 * @param context
	 */
	private static void validateLvidNodeRestrictions(
			final ValueContextForValidationOnRecord context) {

		final ValueContext vc = context.getRecord();
		// get Managed Segment
		final Adaptation mngSegRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
		if (mngSegRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String mngSeg = mngSegRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MAN_SEG_ID);

		// get LVID
		final Adaptation lvidRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LVID);
		if (lvidRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String lvid = lvidRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_C_DSMT_LVID_TBL._C_DSMT_LVID);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}

		LOG.debug("mngSeg,lvid-->" + mngSeg + "_" + lvid);
		final AdaptationTable lvidNodeRestrictionsTable = metadataDataSet
				.getTable(GOCWFPaths._C_DSMT_LVID_RES.getPathInSchema());
		// if lvid is mapped to MS 1 user is not allowed to create GOC for
		// particular LVID.
		PrimaryKey lvidNodeRestrictionsPK;
		lvidNodeRestrictionsPK = lvidNodeRestrictionsTable
				.computePrimaryKey(new Object[] { "1", lvid });
		Adaptation lvidNodeRestrictionsRecord;
		lvidNodeRestrictionsRecord = lvidNodeRestrictionsTable
				.lookupAdaptationByPrimaryKey(lvidNodeRestrictionsPK);
		if (lvidNodeRestrictionsRecord != null) {
			LOG.debug("lvidNodeRestrictionsRecord--->"
					+ lvidNodeRestrictionsRecord);
			context.addMessage(UserMessage
					.createError("GOC should be assigned with another LVID : as LVID : "
							+ lvid
							+ " is restricted from all Managed Segments."));

		} else {
			lvidNodeRestrictionsPK = lvidNodeRestrictionsTable
					.computePrimaryKey(new Object[] { mngSeg, lvid });
			lvidNodeRestrictionsRecord = lvidNodeRestrictionsTable
					.lookupAdaptationByPrimaryKey(lvidNodeRestrictionsPK);
			LOG.debug("lvidNodeRestrictionsRecord--->"
					+ lvidNodeRestrictionsRecord);
			if (lvidNodeRestrictionsRecord != null) {
				context.addMessage(UserMessage
						.createError("GOC should be assigned with Managed Segment and LVID combination  which are not mapped in LVID Node Restrictions Table"));
			}

		}

	}

	public void validateApprovalGroup(ValueContextForValidationOnRecord context) {
		final AdaptationTable table = context.getTable();
		// LOG.debug("Inside method validateApprovalGroup : ");
		final Adaptation dataSet = table.getContainerAdaptation();
		if (isInWorkflow(dataSet)) {
			final DifferenceBetweenTables diff = getDifference(context);
			validateRoutingIDs(context, diff);

		}
	}

	/**
	 * There should be a validation to ensure that only PU/CO Managed Segment
	 * (i.e. Those prefixed by 98xxxxx) should be used to a GOC with SID PLAN.
	 * There will be a reference table that will used for the validation.
	 * Validation Logic: 1. Workflow will first validate if the SID used is
	 * PLAN. 2. If answer to Step 1 is YES, workflow will check that only the
	 * Managed Segment included in the table will be allowed. 3. If answer to
	 * Step 1 is NO, this validation rule will not take effect.
	 * 
	 * @param context
	 */

	private static void validatePUCORule(
			final ValueContextForValidationOnRecord context) {

		final ValueContext vc = context.getRecord();

		Adaptation sidRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);

		if (sidRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		String sid = sidRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID);
		LOG.debug("validatePUCORule - sidRecord-->" + sidRecord);
		// Workflow will first validate if the SID used is PLAN.
		if (sidRecord != null && sid.equals(SID_PUCO_RULE)) {

			final Adaptation mngSegRecord = Utils
					.getLinkedRecord(
							vc,
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
			if (mngSegRecord == null) {
				// Other validation rules handle this situation so no need to
				// add an error here
				return;
			}

			final String mngSeg = mngSegRecord
					.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MAN_SEG_ID);

			// workflow will check that only the Managed Segment included in the
			// table will be allowed.
			final Adaptation metadataDataSet;
			try {
				metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(vc
						.getHome().getRepository());

			} catch (OperationException ex) {
				LOG.error("Error looking up metadata data set", ex);
				return;
			}

			LOG.debug("mngSeg-->" + mngSeg);
			final AdaptationTable pucoRestrictionsTable = metadataDataSet
					.getTable(GOCWFPaths._C_DSMT_PLAN_SID_VALIDATION
							.getPathInSchema());

			final PrimaryKey pucoRestrictionsTablePK = pucoRestrictionsTable
					.computePrimaryKey(new Object[] { sid, mngSeg });

			final Adaptation pucoRestrictionsTableRecord = pucoRestrictionsTable
					.lookupAdaptationByPrimaryKey(pucoRestrictionsTablePK);
			LOG.debug("pucoRestrictionsTableRecord--->"
					+ pucoRestrictionsTableRecord);
			if (pucoRestrictionsTableRecord == null) {
				context.addMessage(UserMessage
						.createError("Only PU/CO Managed Segment (i.e. Those prefixed by 98xxxxx) should be used to a GOC with SID PLAN "));
			}

		}

	}

	/*
	 * 1 Workflow will first validate if the OU exist on this cross reference
	 * table 2 If answer to Step 1 is YES, workflow will validate that the
	 * Managed Segment rollup and Managed Geography rollup of the "leaf" values
	 * in the request are mapped to the OU 3 If answer to Step 1 is NO, this
	 * validation rule should reject the request 4 If answer to Step 2 is NO,
	 * this validation rule should reject the request 5 If answer to Step 2 is
	 * YES, this validation rule should approve the request
	 */

	protected static void validateOuToManagedSegGeoMappingNew(
			final ValueContextForValidationOnRecord context) {
		LOG.debug("validateOuToManagedSegGeoMapping start ");

		final ValueContext vc = context.getRecord();
		// get OU
		final Adaptation frsOURecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_OU);
		if (frsOURecord == null) {
			return;
		}
		final String frsOU = frsOURecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OU_STD_C_DSMT_FRS_OU._C_DSMT_FRS_OU);

		// get Managed Segment
		final Adaptation mngSegRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
		if (mngSegRecord == null) {
			return;
		}
		// get Managed Geo
		final Adaptation mngGeoRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID);
		if (null == mngGeoRecord) {
			return;
		}

		String mngGeo = mngGeoRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_MAN_GEO_ID);
		String mngSeg = mngSegRecord
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MAN_SEG_ID);

		final List<String> msAP = getManagedSegApprovalPoint(mngSegRecord);
		final List<String> mgAP = getManagedGeoApprovalPoint(mngGeoRecord);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}

		/*
		 * final Adaptation mnSegApprovalRecord
		 * =getOUMsApprovalPoint(frsOU,msAP,metadataDataSet);
		 * if(mnSegApprovalRecord!=null){ final Adaptation
		 * mnSegMnGeoApprovalRecord =
		 * getOUMsMgApprovalPoint(frsOU,mgAP,mnSegApprovalRecord
		 * ,metadataDataSet); if(mnSegMnGeoApprovalRecord==null){
		 * LOG.info("The combination - {OU = " +frsOU + ", Managed Segment  = "
		 * + mngSeg + " and Managed Geography  = "+ mngGeo +
		 * "  is either inactive or not found in the OU MS MG validation table under GOC_WF"
		 * );
		 * context.addMessage(UserMessage.createError("The combination - {OU = "
		 * +frsOU + ", Managed Segment  = " + mngSeg +
		 * " and Managed Geography  = "+ mngGeo +
		 * "  is either inactive or not found in the OU MS MG validation table under GOC_WF"
		 * )); }
		 * 
		 * }else{
		 * 
		 * LOG.info("The combination - {OU = " +frsOU + ", Managed Segment  = "
		 * + mngSeg +
		 * "  is either inactive or not found in the OU MS MG validation table under GOC_WF"
		 * );
		 * context.addMessage(UserMessage.createError("The combination - {OU = "
		 * +frsOU + ", Managed Segment  = " + mngSeg +
		 * "  is either inactive or not found in the OU MS MG validation table under GOC_WF"
		 * )); }
		 */
		final Adaptation mnSegMnGeoApprovalRecord = getOUMsMgApprovalPoint(
				metadataDataSet, msAP, mgAP, frsOU);
		if (mnSegMnGeoApprovalRecord == null) {
			LOG.info("The combination - {OU = "
					+ frsOU
					+ ", Managed Segment  = "
					+ mngSeg
					+ " and Managed Geography  = "
					+ mngGeo
					+ "  is either inactive or not found in the OU MS MG validation table under GOC_WF");
			context.addMessage(UserMessage
					.createError("The combination - {OU = "
							+ frsOU
							+ ", Managed Segment  = "
							+ mngSeg
							+ " and Managed Geography  = "
							+ mngGeo
							+ "  is either inactive or not found in the OU MS MG validation table under GOC_WF"));
		}

		LOG.info("validateOuToManagedSegGeoMapping completed. ");
	}

	private static List<String> getManagedGeoApprovalPoint(
			final Adaptation mgRecord) {

		ArrayList<String> mgAppPoint = new ArrayList<String>();

		final Adaptation metadataDataSet;
		String mgFK;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(mgRecord.getHome()
					.getRepository(), DSMTConstants.GOC, DSMTConstants.GOC);
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return null;
		}

		mgFK = mgRecord
				.getString(GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._Parent_FK);
		mgAppPoint
				.add(mgRecord
						.getString(GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._C_DSMT_MAN_GEO_ID));
		AdaptationTable table = metadataDataSet
				.getTable(GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD
						.getPathInSchema());
		getParentNode(
				table,
				mgFK,
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._Parent_FK,
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD._C_DSMT_MAN_GEO_ID,
				mgAppPoint);

		if (!mgAppPoint.contains("1000")) {
			mgAppPoint.add("1000");
		}

		LOG.debug("Manage GEO List >> " + mgAppPoint);

		return mgAppPoint;

	}

	private static List<String> getManagedSegApprovalPoint(
			final Adaptation msRecord) {
		ArrayList<String> msAppPoint = new ArrayList<String>();

		final Adaptation metadataDataSet;
		String msFK;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(msRecord.getHome()
					.getRepository(), DSMTConstants.GOC, DSMTConstants.GOC);
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return null;
		}

		msFK = msRecord
				.getString(GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._Parent_FK);
		msAppPoint
				.add(msRecord
						.getString(GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._C_DSMT_MAN_SEG_ID));
		AdaptationTable table = metadataDataSet
				.getTable(GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD
						.getPathInSchema());
		getParentNode(
				table,
				msFK,
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._Parent_FK,
				GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD._C_DSMT_MAN_SEG_ID,
				msAppPoint);

		if (!msAppPoint.contains("1")) {
			msAppPoint.remove("1");
		}
		LOG.debug("Manage SEG List >> " + msAppPoint);

		return msAppPoint;

	}

	public static List<String> getParentNode(AdaptationTable table,
			String tableFK, Path parentFK, Path setColunName,
			ArrayList<String> msList) {

		Adaptation parentRecord = table.lookupAdaptationByPrimaryKey(PrimaryKey
				.parseString(tableFK));
		if (parentRecord == null) {
			return msList;
		} else {

			msList.add(parentRecord.getString(setColunName));
			String msFK = parentRecord.getString(parentFK);
			if (msFK != null) {
				getParentNode(table, msFK, parentFK, setColunName, msList);
			}

		}

		return msList;
	}

	/*
	 * private static Adaptation getOUMsApprovalPoint(final String frsOU , final
	 * List<String> msAP, final Adaptation metadataDataSet) {
	 * 
	 * final AdaptationTable ouMSMGtable =
	 * metadataDataSet.getTable(GOCWFPaths._C_DSMT_MNSEG_OU.getPathInSchema());
	 * 
	 * Adaptation oUMSRecord = null ; RequestResult reqRes = null;
	 * 
	 * for (String msApprovalPoint : msAP) {
	 * 
	 * final String predicate = GOCWFPaths._C_DSMT_MNSEG_OU._FRS_OU.format() +
	 * "=\"" + frsOU + "\" and "+
	 * GOCWFPaths._C_DSMT_MNSEG_OU._MAN_SEG_ID.format() + "=\"" +
	 * msApprovalPoint +"\""; reqRes =
	 * ouMSMGtable.createRequestResult(predicate);
	 * 
	 * if (null != reqRes) {
	 * 
	 * oUMSRecord = reqRes.nextAdaptation(); if(null != oUMSRecord){
	 * LOG.debug("MS ApprovalPoint matched for OU  " + frsOU + " and MS "+
	 * msApprovalPoint ); break; //stop as soon as first record is found from
	 * bottom in the Managed Segment Hierarchy } reqRes.close(); } }
	 * 
	 * return oUMSRecord; }
	 * 
	 * private static Adaptation getOUMsMgApprovalPoint(final String frsOU ,
	 * final List<String> mgAP,Adaptation mngSegApprovalRecord, final Adaptation
	 * metadataDataSet) { final AdaptationTable ouMSMGtable =
	 * metadataDataSet.getTable(GOCWFPaths._C_DSMT_MNSEG_OU.getPathInSchema());
	 * 
	 * Adaptation oUMSMGRecord = null ; RequestResult reqRes = null; String
	 * mNSegMent
	 * =mngSegApprovalRecord.getString(GOCWFPaths._C_DSMT_MNSEG_OU._MAN_SEG_ID);
	 * for (String mgApprovalPoint : mgAP) {
	 * 
	 * final String predicate = GOCWFPaths._C_DSMT_MNSEG_OU._FRS_OU.format() +
	 * " = \"" + frsOU + "\" and " +
	 * GOCWFPaths._C_DSMT_MNSEG_OU._MAN_SEG_ID.format() + " = \"" + mNSegMent +
	 * "\" and " + GOCWFPaths._C_DSMT_MNSEG_OU._MAN_GEO_ID.format() + " = \"" +
	 * mgApprovalPoint + "\""; reqRes =
	 * ouMSMGtable.createRequestResult(predicate);
	 * 
	 * if (null != reqRes) {
	 * 
	 * oUMSMGRecord = reqRes.nextAdaptation(); if(null != oUMSMGRecord){
	 * LOG.debug("MS ApprovalPoint matched for OU  " + frsOU + " and MS "+
	 * mNSegMent +" and MN GEO"+ mgApprovalPoint); break; //stop as soon as
	 * first record is found from bottom in the Managed Segment Hierarchy }
	 * 
	 * reqRes.close();
	 * 
	 * } }
	 * 
	 * 
	 * 
	 * return oUMSMGRecord; }
	 */

	private static Adaptation getOUMsMgApprovalPoint(
			final Adaptation metadataDataSet, final List<String> msCP,
			final List<String> mgCP, final String frsOU) {
		Adaptation oUMSMGRecord = null;
		final AdaptationTable ouMSMGtable = metadataDataSet
				.getTable(GOCWFPaths._C_DSMT_MNSEG_OU.getPathInSchema());
		// make loop for Manage Segment table
		for (String manSegment : msCP) {
			// make loop for Manage Geo table
			for (String manGeo : mgCP) {
				final String predicate = GOCWFPaths._C_DSMT_MNSEG_OU._FRS_OU
						.format()
						+ " = \""
						+ frsOU
						+ "\" and "
						+ GOCWFPaths._C_DSMT_MNSEG_OU._MAN_SEG_ID.format()
						+ " = \""
						+ manSegment
						+ "\" and "
						+ GOCWFPaths._C_DSMT_MNSEG_OU._MAN_GEO_ID.format()
						+ " = \"" + manGeo + "\"";

				final RequestResult reqRes = ouMSMGtable
						.createRequestResult(predicate);
				try {
					// return the Adaption when user get first record in table
					// ManagedSeg Geography Map
					if (!reqRes.isEmpty()) {
						LOG.info("Record found for manSegment " + manSegment
								+ " and manGeo " + manGeo);
						oUMSMGRecord = reqRes.nextAdaptation();
						return oUMSMGRecord;
					}
				} finally {
					reqRes.close();
				}

			}
		}

		return oUMSMGRecord;

	}

	private static void validateHLNodeRestrictions(
			final ValueContextForValidationOnRecord context) {
		LOG.debug("validate High Level Node Restrictions start ");
		final ValueContext vc = context.getRecord();
		// get MNGSEG
		final Adaptation mngSegRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
		if (mngSegRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String mngSeg = mngSegRecord
				.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
		// get SID
		final Adaptation sidRecord = Utils
				.getLinkedRecord(
						vc,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
		if (sidRecord == null) {
			// Other validation rules handle this situation so no need to add an
			// error here
			return;
		}
		final String sid = sidRecord
				.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}

		final AdaptationTable HLNodeRestrictionRefTable = metadataDataSet
				.getTable(GOCWFPaths._C_DSMT_DEF_RES.getPathInSchema());
		final String mngSegPredicate = GOCWFPaths._C_DSMT_DEF_RES._MAN_SEG_ID
				.format() + " = \"" + mngSeg + "\"";
		final RequestResult mngSegResultSet = HLNodeRestrictionRefTable
				.createRequestResult(mngSegPredicate);
		try {
			if (mngSegResultSet.nextAdaptation() != null) {
				final String predicate = GOCWFPaths._C_DSMT_DEF_RES._MAN_SEG_ID
						.format()
						+ " = \""
						+ mngSeg
						+ "\" and "
						+ GOCWFPaths._C_DSMT_DEF_RES._SID.format()
						+ " = \""
						+ sid + "\" ";
				final RequestResult reqRes = HLNodeRestrictionRefTable
						.createRequestResult(predicate);
				try {
					if (reqRes.nextAdaptation() == null) {
						context.addMessage(UserMessage
								.createError("High Level Default managed segments cannot be used with the current SID. Please select a lower level value"));
					}
				} finally {
					reqRes.close();
				}
			} else {
				return;
			}
		} finally {
			mngSegResultSet.close();
		}
		LOG.debug("validateHLNodeRestrictions end ");
	}

	public void checkForUniqueGOCContraints(
			final ValueContextForValidationOnRecord context) {

		final ValueContext vc = context.getRecord();

		final String sidValue = (String) vc.getValue(SID_PATH);
		final String buValue = (String) vc.getValue(FRS_BU_ID_PATH);
		final String ouValue = (String) vc.getValue(FRS_OU_ID_PATH);
		final String lvidValue = (String) vc.getValue(LVID_PATH);
		final String mgValue = (String) vc.getValue(MAN_GEO_PATH);
		final String msValue = (String) vc.getValue(MAN_SEG_PATH);
		final String funcValue = (String) vc.getValue(FUNCTION_ID_PATH);
		final String comment = (String) vc.getValue(COMMENT_PATH);

		try {

			final String predicate = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID
					.format()
					+ " = \""
					+ sidValue
					+ "\" and "
					+ GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU
							.format()
					+ " = \""
					+ buValue
					+ "\" and "
					+ GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_OU
							.format()
					+ " = \""
					+ ouValue
					+ "\" and "
					+ GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LVID
							.format()
					+ " = \""
					+ lvidValue
					+ "\" and "
					+ GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID
							.format()
					+ " = \""
					+ mgValue
					+ "\" and "
					+ GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID
							.format()
					+ " = \""
					+ msValue
					+ "\" and "
					+ GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FUNCTION
							.format() + " = \"" + funcValue + "\" ";

			final List<Adaptation> listOfDuplicate = context.getTable()
					.selectOccurrences(predicate);
			if (listOfDuplicate.size() > 1) {
				ArrayList<String> gocID = new ArrayList<String>();
				ArrayList<String> gocPK = new ArrayList<String>();
				for (Adaptation dupRec : listOfDuplicate) {
					String gocIDValue = String
							.valueOf(dupRec
									.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC));

					if (gocIDValue.equals("null")) {

						gocPK.add(String.valueOf(dupRec
								.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK)));

					} else {

						gocID.add(String.valueOf(dupRec
								.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC)));

					}
				}
				if (StringUtils.isBlank(comment)) {
					context.addMessage(UserMessage
							.createError("Please fill comments to justify for duplicate GOC"));
				}
				context.addMessage(UserMessage
						.createWarning("*Duplicate GOC ID are " + gocID
								+ " and GOC PK (in-flight request) are "
								+ gocPK));

			}
		} catch (Exception e) {
			LOG.debug("Error while fetching duplicate GOCs in checkForUniqueGOCContraints method.");
		}
	}

	public boolean editChildDataSpace(Adaptation dataSet,
			ValueContextForValidationOnRecord context) {

		Repository repo = dataSet.getHome().getRepository();
		Adaptation metadataDataSet;
		boolean status = false;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(repo, "GOC_WF",
					"GOC_WF");

			final AdaptationTable targetTable = metadataDataSet
					.getTable(getPath("/root/C_GOC_WF_REPORTING"));

			
			StringBuffer predicate = new StringBuffer("osd:contains-case-insensitive("+ GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID
                    .format()+",'" + dataSet.getHome().getKey().getName() + "')");


			RequestResult rs = targetTable.createRequestResult(predicate.toString());
			Adaptation record = rs.nextAdaptation();
			if (record != null) {
				String reqStatus = record
						.getString(GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);

				if (reqStatus.equalsIgnoreCase(GOCConstants.CREATED)
						|| reqStatus.contains("Rework")
						|| reqStatus.contains("MAINTENANCE")) {
					status = true;
				}

			}
			else{
				status = true;
			}

		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return status;
	}
	


}
