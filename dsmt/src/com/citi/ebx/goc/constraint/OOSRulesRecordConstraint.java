package com.citi.ebx.goc.constraint;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.service.LoggingCategory;

public class OOSRulesRecordConstraint implements ConstraintOnTableWithRecordLevelCheck{

	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	@Override
	public void checkTable(ValueContextForValidationOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
		final ValueContext value = context.getRecord();
		context.removeRecordFromMessages(value);
		String orgChain =(String) value.getValue(DSMTConstants.ORG_CHAIN);      
		String managedSegID =(String) value.getValue(DSMTConstants.MS_ID);
		String managedGeoID =(String) value.getValue(DSMTConstants.MG_ID);
		String legalEntityID =(String) value.getValue(DSMTConstants.LEID);
		LOG.info("Values == "+orgChain+","+managedSegID+","+managedGeoID+","+legalEntityID);
		
		int count = 0;
		if(StringUtils.isNotBlank(orgChain)){
			count++;
		}
		if(StringUtils.isNotBlank(managedSegID) || StringUtils.isNotBlank(managedGeoID)){
			count++;
			if(StringUtils.isBlank(managedSegID) || StringUtils.isBlank(managedGeoID)){
				context.addMessage(UserMessage
						.createError("Please enter both Managed Segment and Managed Geography"));
			}
			
		}
		
		if(StringUtils.isNotBlank(legalEntityID)){
			count++;
		}
		if(count != 1){
			context.addMessage(UserMessage
					.createError("Please enter either ORG Chain or (Managed Segment and Managed Geography) or Legal Entity"));
		}
		
	
		
	}
	
	
}
