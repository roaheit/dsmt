package com.citi.ebx.goc.constraint;

import java.util.List;
import java.util.Locale;

import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

/**
 * Validates that a change hasn't been made on a sub-table that isn't consistent
 * with the action required value
 */
public class GOCSubtableRecordConstraint implements
		ConstraintOnTableWithRecordLevelCheck {
	private static final Path ACTION_REQUIRED_PATH =
			GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD;
	private static final Path GOC_PK =
			GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK;
	// Assuming it's named the same in each subtable
	private static final Path GOC_FK_PATH =
			GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM._C_GOC_FK;
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();	
	private static enum SubtableType {
		GL, P2P, RE
	}
	
	@Override
	public void checkTable(ValueContextForValidationOnTable context) {
		// do nothing
	}

	@Override
	public void setup(ConstraintContextOnTable context) {
		// do nothing
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext context)
			throws InvalidSchemaException {
		return "Only the appropriate combination of fields can be modified based on the action required code.";
	}
	
	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
		if (isInWorkflow(context.getTable().getContainerAdaptation())) {
			final ValueContext vc = context.getRecord();
			final AdaptationTable table = context.getTable();
			// remove any of the previous validation error messages
			context.removeRecordFromMessages(vc);
			
			
			final Adaptation gocRecord = Utils.getLinkedRecord(vc, GOC_FK_PATH);
			if (gocRecord == null) {
				return;
			}
			final AdaptationTable initialTable = GOCWorkflowUtils.getTableInInitialSnapshot(table);
			final Adaptation initialRecord = initialTable
					.lookupAdaptationByPrimaryKey(vc);
			
			
			final String actionRequired = gocRecord.getString(ACTION_REQUIRED_PATH);
			LOG.info("GOC PK value is "+ gocRecord.get_int(GOC_PK) + " Action requried is -->> " + actionRequired);
			if (actionRequired != null) {
				
				if (GOCConstants.ACTION_MODIFY_FRS_BU_ONLY.equals(actionRequired)
						|| GOCConstants.ACTION_MODIFY_FRS_OU_ONLY.equals(actionRequired)
						|| GOCConstants.ACTION_MODIFY_FUNCTION_ONLY.equals(actionRequired)
						|| GOCConstants.ACTION_MODIFY_GOC_DESC_ONLY.equals(actionRequired)
						|| GOCConstants.ACTION_MODIFY_GOC_USAGE_ONLY.equals(actionRequired)
						|| GOCConstants.ACTION_MODIFY_MANAGED_SEG_ONLY.equals(actionRequired)
						|| GOCConstants.ACTION_MODIFY_MANAGED_GEO_ONLY.equals(actionRequired)
						|| GOCConstants.ACTION_REACTIVATE_GOC_LEG_CCS.equals(actionRequired)
						|| GOCConstants.ACTION_DEACTIVATE_GOC_LEG_CCS.equals(actionRequired)
						|| GOCConstants.ACTION_NO_CHANGE.equals(actionRequired)) {
					// TODO: If we need more detailed messages about what exactly changed it will require looping through
					//       the change set
					context.addMessage(UserMessage.createError(
							"This change/addition is not allowed because record is associated to a GOC with action required = \""
							+ actionRequired + "\"."));
					return;
				}
				if(!GOCConstants.ACTION_CREATE_GOC_LEG_CCS.equals(actionRequired)){
					if (initialRecord != null) {
					final ValueContext oldVC =  initialRecord.createValueContext();
					validateGOCFk(context, oldVC, vc);
					}
					
				}
				final Adaptation metadataDataSet;
				try {
					metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(
							vc.getHome().getRepository());
				} catch (OperationException ex) {
					context.addMessage(UserMessage.createError("Error looking up metadata data set", ex));
					return;
				}
						
				final SubtableType subtableType = getSubtableType(context.getTable().getTablePath(), metadataDataSet);
				if (subtableType == null) {
					context.addMessage(UserMessage.createError("Error determining partner table type."));
					return;
				}
				if (GOCConstants.ACTION_MODIFY_RE_ONLY.equals(actionRequired)
						&& ! SubtableType.RE.equals(subtableType)) {
					context.addMessage(UserMessage.createError(
							"This change/addition is not allowed because record is associated to a GOC with action required = \""
							+ actionRequired + "\" and this is not a RE table."));
					return;
				}
				if (GOCConstants.ACTION_MODIFY_GL_ONLY.equals(actionRequired)
						&& ! SubtableType.GL.equals(subtableType)) {
					context.addMessage(UserMessage.createError(
							"This change/addition is not allowed because record is associated to a GOC with action required = \""
							+ actionRequired + "\" and this is not a GL table."));
					return;
				}
				if (GOCConstants.ACTION_MODIFY_P2P_ONLY.equals(actionRequired)
						&& ! SubtableType.P2P.equals(subtableType)) {
					context.addMessage(UserMessage.createError(
							"This change/addition is not allowed because record is associated to a GOC with action required = \""
							+ actionRequired + "\" and this is not a P2P table."));
					return;
				}
			
		}
			
			
			if (! GOCWorkflowUtils.isSidMatching(gocRecord.createValueContext(),
					context.getTable().getContainerAdaptation())) {
				context.addMessage(UserMessage.createError(
						"You're trying to create/modify an unauthorized record. The SID does not correspond to the workflows's SID"));
			}
			
		}
	}
		
	private static boolean isInWorkflow(final Adaptation dataSet) {
		final String currentSid = GOCWorkflowUtils.getCurrentSID(dataSet);
		return (currentSid != null && ! "".equals(currentSid));
	}
	
	private static SubtableType getSubtableType(final Path subtablePath, final Adaptation metadataDataSet) {
		final String subtablePathStr = subtablePath.format();
		final SchemaNode sidEnhancementNode =
				metadataDataSet.getSchemaNode().getNode(
				GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema())
				.getTableOccurrenceRootNode();
		
		@SuppressWarnings("unchecked")
		final List<String> glEnumList = sidEnhancementNode.getNode(
				GOCWFPaths._C_DSMT_SID_ENH._GLTable)
				.getFacetEnumeration().getValues();
		if (glEnumList.contains(subtablePathStr)) {
			return SubtableType.GL;
		}
		
		@SuppressWarnings("unchecked")
		final List<String> reEnumList = sidEnhancementNode.getNode(
				GOCWFPaths._C_DSMT_SID_ENH._RptEngineTable)
				.getFacetEnumeration().getValues();
		if (reEnumList.contains(subtablePathStr)) {
			return SubtableType.RE;
		}
		
		@SuppressWarnings("unchecked")
		final List<String> p2pEnumList = sidEnhancementNode.getNode(
				GOCWFPaths._C_DSMT_SID_ENH._P2PTable)
				.getFacetEnumeration().getValues();
		if (p2pEnumList.contains(subtablePathStr)) {
			return SubtableType.P2P;
		}
		return null;
	}
	
	private static void validateGOCFk(
			ValueContextForValidationOnRecord context, ValueContext oldVC,
			ValueContext vc) {
		
		/* Local Cost Code cannot be changed. */
		String gocPkValue = String.valueOf(vc.getValue(GOC_FK_PATH));
		String oldGocPkValue = String.valueOf(oldVC.getValue(GOC_FK_PATH));
		
		if(! gocPkValue.equalsIgnoreCase(oldGocPkValue)){
			LOG.info("GOC PK can not be changed. Kindly use original GOC PK " + oldGocPkValue);
			context.addMessage(UserMessage.createError("GOC Pk can not be changed. Kindly use original GOC PK " + oldGocPkValue));
		}
		
		
	}
}
