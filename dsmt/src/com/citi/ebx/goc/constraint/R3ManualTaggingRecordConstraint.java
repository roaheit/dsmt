package com.citi.ebx.goc.constraint;

import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;

public class R3ManualTaggingRecordConstraint implements ConstraintOnTableWithRecordLevelCheck{

	@Override
	public void checkTable(ValueContextForValidationOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
	
		final ValueContext value = context.getRecord();
		context.removeRecordFromMessages(value);
		String cblVal =(String) value.getValue(DSMTConstants.CBL_CD);
		String coVal =(String) value.getValue(DSMTConstants.CO_CD);
		String mleVal =(String) value.getValue(DSMTConstants.MLE_CD);
		String gfVal =(String) value.getValue(DSMTConstants.GF_CD);
		//System.out.println(cblVal+","+coVal+","+mleVal+","+gfVal);
		
		int count = 0;
		if(!"NO_CBL".equalsIgnoreCase(cblVal)){
			count++;
		}
		if(!"NO_CO".equalsIgnoreCase(coVal)){
			count++;
		}
		if(!"NO_MLE".equalsIgnoreCase(mleVal)){
			count++;
		}
		if(!"NO_GF".equalsIgnoreCase(gfVal)){
			count++;
		}
		if(count != 1){
			context.addMessage(UserMessage
					.createError("Please select only one from CBL Code, CO Code, MLE Code, GF Code"));
		}
		
	}

}
