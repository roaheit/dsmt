package com.citi.ebx.goc.constraint;

import java.util.Locale;









import org.apache.tools.ant.util.StringUtils;

import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPartnerSystemPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

/**
 * Validation rules on changes made to records in DBL General Ledger table
 * 
 * @author se08819
 * 
 */

public class DBSRecordConstraint implements
		ConstraintOnTableWithRecordLevelCheck {

	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();

	private static final Path ACTION_REQUIRED_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD;
	// Assuming it's named the same in each subtable
	private static final Path GOC_FK_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM._C_GOC_FK;

	@Override
	public void checkTable(ValueContextForValidationOnTable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
		final ValueContext vc = context.getRecord();

		// remove any of the previous validation error messages
		context.removeRecordFromMessages(vc);

		final Adaptation gocRecord = Utils.getLinkedRecord(vc, GOC_FK_PATH);
		if (gocRecord == null) {
			return;
		}
		final String actionRequired = gocRecord.getString(ACTION_REQUIRED_PATH);
		LOG.debug("actionRequired-->"+actionRequired);
		
		if (!(GOCConstants.ACTION_CREATE_GOC_LEG_CCS.equals(actionRequired) || GOCConstants.ACTION_REACTIVATE_GOC_LEG_CCS
				.equals(actionRequired))) {
			validateDepartment(context);
		}
		validateCountry(context);
		validateFirmLegalEntity(context);
		validatePMProduct(context);
		validateSubProduct(context);
		validateLocation(context);
		validateSubStrategyCodefinal(context);
		validateSuccessorDepartment(context, actionRequired);

	}

	/**
	 * Should check in department table for duplicates and then only perform
	 * action For 'create' requests, GL admin provides after approvals, for
	 * other requests it should be an existing and valid department in the All
	 * Departments
	 * 
	 * @param context
	 */
	private static void validateDepartment(
			final ValueContextForValidationOnRecord context) {

		final ValueContext vc = context.getRecord();

		final String department = (String) context
				.getRecord()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS._Department);
		LOG.debug("validateDepartment-->" + department);
		if(department==null)return;
		final String predicate = GOCPartnerSystemPaths._DBS_REF_DATA_ODMAllDepartments._Center
				.format() + " = \"" + department + "\"";

		LOG.debug("predicate-->" + predicate);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable odmAllDepts = metadataDataSet
				.getTable(GOCPartnerSystemPaths._DBS_REF_DATA_ODMAllDepartments
						.getPathInSchema());
		final RequestResult reqRes = odmAllDepts.createRequestResult(predicate);
		LOG.debug("reqRes-->" + reqRes.getSize());
		try {
			if (reqRes.nextAdaptation() == null) {

				context.addMessage(UserMessage
						.createError("The Department should be a valid value in ODMAllDepartments Table"));
			}
		} finally {
			reqRes.close();
		}

	}

	/**
	 * Mandatory for deactivations i.e. for the following action required codes
	 * - B (Deactivate GOC and Any Legacy CCs)- Q (Deactivate GL CC Only) for
	 * above action required codes should be an existing and valid department in
	 * the All Departments File**
	 * 
	 * @param context
	 */
	private static void validateSuccessorDepartment(
			final ValueContextForValidationOnRecord context,
			String actionRequired) {
		final String successorDepartment = (String) context
				.getRecord()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS._Successor_Dept);
		if ((GOCConstants.ACTION_DEACTIVATE_GOC_LEG_CCS.equals(actionRequired))) {/*
			if (StringUtils.isEmpty(successorDepartment)) {
				// add error message for empty value

				context.addMessage(UserMessage
						.createError("Please Enter Successor Department "));
			} else {
				final ValueContext vc = context.getRecord();

				final String department = (String) context
						.getRecord()
						.getValue(
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS._Department);
				LOG.debug("validateDepartment-->" + department);
				final String predicate = GOCPartnerSystemPaths._DBS_REF_DATA_ODMAllDepartments._Center
						.format() + " = \"" + department + "\"";

				LOG.debug("predicate-->" + predicate);

				final Adaptation metadataDataSet;
				try {
					metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc
							.getHome().getRepository());
				} catch (OperationException ex) {
					LOG.error("Error looking up metadata data set", ex);
					return;
				}
				final AdaptationTable odmAllDepts = metadataDataSet
						.getTable(GOCPartnerSystemPaths._DBS_REF_DATA_ODMAllDepartments
								.getPathInSchema());
				final RequestResult reqRes = odmAllDepts
						.createRequestResult(predicate);
				LOG.debug("reqRes-->" + reqRes.getSize());
				try {
					if (reqRes.nextAdaptation() == null) {

						context.addMessage(UserMessage
								.createError("The Department should be a valid value in ODMAllDepartments Table"));
					}
				} finally {
					reqRes.close();
				}
			}

		*/}

	}

	/**
	 * Should be a valid Substrategy code in the Substrategies** reference table
	 * 
	 * @param context
	 */
	private static void validateSubStrategyCodefinal(
			final ValueContextForValidationOnRecord context) {
		final ValueContext vc = context.getRecord();

		final String substrategyCode = (String) context
				.getRecord()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS._Substrategy_Code);
		LOG.debug("validateSubStrategyCodefinal-->" + substrategyCode);
		if(substrategyCode ==null)return;
		final String predicate = GOCPartnerSystemPaths._DBS_REF_DATA_ODMSubstrategy._SubstrategyCode
				.format() + " = \"" + substrategyCode + "\"";

		LOG.debug("predicate-->" + predicate);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc
					.getHome().getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable odmSubstrategies = metadataDataSet
				.getTable(GOCPartnerSystemPaths._DBS_REF_DATA_ODMSubstrategy
						.getPathInSchema());
		final RequestResult reqRes = odmSubstrategies
				.createRequestResult(predicate);
		LOG.debug("reqRes-->" + reqRes.getSize());
		try {
			if (reqRes.nextAdaptation() == null) {

				context.addMessage(UserMessage
						.createError("The Substrategy Code should be a valid value in ODM Substrategy Table"));
			}
		} finally {
			reqRes.close();
		}

	}

	/**
	 * Should be a valid PM_Product in the  PMProduct_SubProduct reference table
	 *  check Identify the Bus Unit , LVID and SID from ODM_Legal_Entities for the user entered Firm Legal Entity.
		The values must match with FRS BU ,LVID & SID of the associated GOC record.
	 * @param context
	 */
	
	private static void validatePMProduct(
			final ValueContextForValidationOnRecord context) {
//		System.out.println("inside validatePMProduct");
		final ValueContext vc = context.getRecord();

		final String pmproduct = (String) context
				.getRecord()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS._PM_Product);
		LOG.debug("validatePMProduct-->" + pmproduct);
		if(pmproduct ==null)return;
		final String predicate = GOCPartnerSystemPaths._DBS_REF_DATA_PMProduct_SubProduct._PRODUCT_ID
				.format() + " = \"" + pmproduct + "\"";
//        System.out.println("--------------pmproduct"+pmproduct);
		LOG.debug("predicate-->" + predicate);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc
					.getHome().getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable pmproductsubproduct = metadataDataSet
				.getTable(GOCPartnerSystemPaths._DBS_REF_DATA_PMProduct_SubProduct
						.getPathInSchema());
		final RequestResult reqRes = pmproductsubproduct
				.createRequestResult(predicate);
		LOG.debug("reqRes-->" + reqRes.getSize());
		try {
			if (reqRes.nextAdaptation() == null) {

				context.addMessage(UserMessage
						.createError("The PM PRODUCT should be a valid value in PMProduct_SubProduct table "));
			}
		} finally {
			reqRes.close();
		}

	}
	
	/**
	 * Should be a valid Sub_Product in the  PMProduct_SubProduct reference table
	 * 
	 * @param context
	 */
	
	private static void validateSubProduct(
			final ValueContextForValidationOnRecord context) {
//		System.out.println("inside validateSubProduct");
		final ValueContext vc = context.getRecord();

		final String subproduct = (String) context
				.getRecord()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS._SubProduct);
		LOG.debug("validateSubProduct-->" + subproduct);
		if(subproduct ==null)return;
		final String predicate = GOCPartnerSystemPaths._DBS_REF_DATA_PMProduct_SubProduct._Sub_product
				.format() + " = \"" + subproduct + "\"";
		LOG.debug("predicate-->" + predicate);
//		System.out.println("--------------subproduct"+subproduct);
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc
					.getHome().getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable pmproductsubproduct = metadataDataSet
				.getTable(GOCPartnerSystemPaths._DBS_REF_DATA_PMProduct_SubProduct
						.getPathInSchema());
		final RequestResult reqRes = pmproductsubproduct
				.createRequestResult(predicate);
		LOG.debug("reqRes-->" + reqRes.getSize());
		try {
			if (reqRes.nextAdaptation() == null) {

				context.addMessage(UserMessage
						.createError("The Sub PRODUCT should be a valid value in PMProduct_SubProduct table "));
			}
		} finally {
			reqRes.close();
		}

	}
	private static void validateFirmLegalEntity(
			final ValueContextForValidationOnRecord context) {
		final ValueContext vc = context.getRecord();

		final String firmLegalEntity = (String) context
				.getRecord()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS._Firm_Legal_Entity);
		LOG.debug("validateFirmLegalEntity-->" + firmLegalEntity);
		if(firmLegalEntity==null)return ;
		final String predicate = GOCPartnerSystemPaths._DBS_REF_DATA_ODMLegalEntities._LegalEntity
				.format() + " = \"" + firmLegalEntity + "\"";

		LOG.debug("predicate-->" + predicate);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc
					.getHome().getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable odmLegalEntities = metadataDataSet
				.getTable(GOCPartnerSystemPaths._DBS_REF_DATA_ODMLegalEntities
						.getPathInSchema());
		final RequestResult reqRes = odmLegalEntities
				.createRequestResult(predicate);
		LOG.debug("reqRes-->" + reqRes.getSize());
		try {
			if (reqRes.isEmpty()) {

				context.addMessage(UserMessage
						.createError("The Legal Entity should be a valid value in ODM Legal Entity Table"));
			}else{
				Adaptation record=reqRes.nextAdaptation();
				String buRef=(String) record.get(Path.parse("./Bus_Unit"));
				String lvidRef=(String) record.get(Path.parse("./LVID"));
				String sidRef=(String) record.get(Path.parse("./SID"));
				if(null==buRef || null==lvidRef || null==sidRef){
					context.addMessage(UserMessage
							.createError("Incomplete Reference data for entered Firm Legal Entity.Cannot proceed with validation "));
					
					return;
				}
				LOG.debug("ref data -->"+buRef+"|"+lvidRef+"|"+sidRef);
				// check Identify the Bus Unit , LVID and SID from ODM_Legal_Entities for the user entered Firm Legal Entity.
				//The values must match with FRS BU ,LVID & SID of the associated GOC record.
				
				// get FRS BU from GOC
				final Adaptation gocRecord = Utils.getLinkedRecord(vc, GOC_FK_PATH);
				if (gocRecord == null) {
					return;// handled in GOC pk check
				}
				LOG.debug("gocRecord-->" + gocRecord);				
				// get BU
				
				final Adaptation frsBURecord = Utils.getLinkedRecord(gocRecord,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU);
				if (frsBURecord == null) {
					return;// handled in GOC validation
				}
				
				// get FRS BU value
				final String frsBu= frsBURecord.getString(
						DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_BU_STD_C_DSMT_FRS_BU._C_DSMT_FRS_BU);
				LOG.debug("frsBu-->"+frsBu);
				// get Lvid Record
				final Adaptation lvidRecord = Utils.getLinkedRecord(gocRecord,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LVID);
				if (lvidRecord == null) {
					return;// handled in GOC validation
				}
				// get Lvid
				
				final String lvid= lvidRecord.getString(
						DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_C_DSMT_LVID_TBL._C_DSMT_LVID);
				
				LOG.debug("lvid-->"+lvid);
				// get SID 
				
				final Adaptation sidRecord = Utils.getLinkedRecord(gocRecord,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
				if (sidRecord == null) {
					return;// handled in GOC validation
				}
				// get SID 
				final String sid= sidRecord.getString(
						DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID);
				
				LOG.debug("sid-->"+sid);
				if(!(frsBu.equals(frsBu)&& lvid.equals(lvidRef) && sid.equals(sid))){
					context.addMessage(UserMessage
							.createError("Firm Legal Entity must be linked with valid FRS BU ,LVID and  SID associated with GOC"));
					
				}
				
			}
			
			
			
		} finally {
			reqRes.close();
		}

	}

	/**
	 * Should be a valid country in the ODMCountryandJenaEm reference table
	 * 
	 * @param context
	 */
	private static void validateCountry(
			final ValueContextForValidationOnRecord context) {
		final ValueContext vc = context.getRecord();

		final String country = (String) context
				.getRecord()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS._Country);
		LOG.debug("validateCountry-->" + country);
		final String predicate = GOCPartnerSystemPaths._DBS_REF_DATA_ODMCountryandJenaEm._CountryID
				.format() + " = \"" + country + "\"";
		if(country==null)return;
		LOG.debug("predicate-->" + predicate);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc
					.getHome().getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable odmCountries= metadataDataSet
				.getTable(GOCPartnerSystemPaths._DBS_REF_DATA_ODMCountryandJenaEm
						.getPathInSchema());
		final RequestResult reqRes = odmCountries
				.createRequestResult(predicate);
		LOG.debug("reqRes-->" + reqRes.getSize());
		try {
			if (reqRes.nextAdaptation() == null) {

				context.addMessage(UserMessage
						.createError("The Country should be a valid value in ODMCountryandJenaEm Table"));
			}
		} finally {
			reqRes.close();
		}

	}

	/**
	 * Should be a valid Location in the ODMLocation reference
	 * table
	 * 
	 * @param context
	 */
	private static void validateLocation(
			final ValueContextForValidationOnRecord context) {
		final ValueContext vc = context.getRecord();

		final String location = (String) context
				.getRecord()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS._Location);
		LOG.debug("validateLocation-->" + location);
		if(location==null)return;
		final String predicate = GOCPartnerSystemPaths._DBS_REF_DATA_ODMLocation._Location
				.format() + " = \"" + location + "\"";

		LOG.debug("predicate-->" + predicate);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc
					.getHome().getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable odmLocations= metadataDataSet
				.getTable(GOCPartnerSystemPaths._DBS_REF_DATA_ODMLocation
						.getPathInSchema());
		final RequestResult reqRes = odmLocations
				.createRequestResult(predicate);
		LOG.debug("reqRes-->" + reqRes.getSize());
		try {
			if (reqRes.nextAdaptation() == null) {

				context.addMessage(UserMessage
						.createError("The Location should be a valid value in ODM Location Table"));
			}
		} finally {
			reqRes.close();
		}


	}

	
}
