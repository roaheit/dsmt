package com.citi.ebx.goc.constraint;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.citi.ebx.goc.path.DSMT2Paths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.RepositoryAccessException;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.ValidationReport;

public class AccountingCatRecordConstraint implements ConstraintOnTableWithRecordLevelCheck {

	private Path accounCategoryPath = Path.parse("/root/GLOBAL_STD/ENT_STD/F_BAL_TYPE/C_DSMT_ACC_CTGR");
	private static final Path ACCOUNT_CAT_STATUS_PATH=DSMT2Paths._GLOBAL_STD_ENT_STD_F_BAL_TYPE_C_DSMT_ACC_CTGR._EFF_STATUS;
	private static final Path ACCOUNT_CAT_CODE_PATH=DSMT2Paths._GLOBAL_STD_ENT_STD_F_BAL_TYPE_C_DSMT_ACC_CTGR._C_DSMT_ACC_CATGORY;
	private static final Path BAL_TYPE_STATUS_PATH=DSMT2Paths._GLOBAL_STD_ENT_STD_F_BAL_TYPE_C_DSMT_BAL_TYPE._EFF_STATUS;
	private static final Path BAL_TYPE_END_DATE=DSMT2Paths._GLOBAL_STD_ENT_STD_F_BAL_TYPE_C_DSMT_BAL_TYPE._ENDDT;
	private Path endDateFieldPath = DSMT2Paths._GLOBAL_STD_ENT_STD_F_BAL_TYPE_C_DSMT_ACC_CTGR._ENDDT;
	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	@Override
	public void checkTable(ValueContextForValidationOnTable arg0) {
		// TODO Auto-generated method stub
		AdaptationTable table=arg0.getTable();
		final String pred1 = "date-equal(" + BAL_TYPE_END_DATE.format()
			+ ",'9999-12-31')";
		final String pred2 = "osd:is-null(" + BAL_TYPE_END_DATE.format() + ")";
		
		final String predicate = pred1  + " and (" + pred1
			+ " or " + pred2 + ")";
	
		RequestResult reqRes = table.createRequestResult(predicate);
		for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
		
		try{
		final ValidationReport recordResult=record.getValidationReport();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		}	
	}
	

	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
		
		try {
			// TODO Auto-generated method stub
			final ValueContext vc = context.getRecord();
			Adaptation dataSet = vc.getAdaptationInstance();
			context.removeRecordFromMessages(vc);
			AdaptationTable table = dataSet.getTable(accounCategoryPath);
			final String acctCode=String.valueOf(vc.getValue(ACCOUNT_CAT_CODE_PATH));
			final String balStatus=String.valueOf(vc.getValue(BAL_TYPE_STATUS_PATH));
			final String balEndDate=String.valueOf(vc.getValue(BAL_TYPE_END_DATE));
			DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
			Date date = (Date)formatter.parse(balEndDate);
						
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			String formatedDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" +cal.get(Calendar.DATE);
			if((formatedDate.equals("9999-12-31") || balEndDate==null) && balStatus.equalsIgnoreCase("A"))
			{
				final String pred1 = ACCOUNT_CAT_CODE_PATH.format() + "= '" + acctCode + "'";
				final String pred2 = "date-equal(" + endDateFieldPath.format()
					+ ",'9999-12-31')";
				final String pred3 = "osd:is-null(" + endDateFieldPath.format() + ")";
				
				final String predicate = pred1  + " and (" + pred2
					+ " or " + pred3 + ")";
			
				RequestResult reqRes = table.createRequestResult(predicate);
				Adaptation record=reqRes.nextAdaptation();
				final String accStatus=record.getString(ACCOUNT_CAT_STATUS_PATH);
				if(accStatus.equalsIgnoreCase("I"))
				{
					context.addMessage(UserMessage.createError(
					"Status of Accounting Category Code  Must be Active for Active Balance Type Status Code."));
				}
			}
			
		} catch (PathAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
