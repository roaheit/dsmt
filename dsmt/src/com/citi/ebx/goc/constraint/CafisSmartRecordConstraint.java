package com.citi.ebx.goc.constraint;

import java.util.Locale;

import com.citi.ebx.dsmt.util.SmartUtils;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPartnerSystemPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.misc.StringUtils;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class CafisSmartRecordConstraint implements
		ConstraintOnTableWithRecordLevelCheck {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private static final Path ACTION_REQUIRED_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD;
	// Assuming it's named the same in each subtable
	private static final Path GOC_FK_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._C_GOC_FK;


	@Override
	public void checkTable(ValueContextForValidationOnTable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
		
		final ValueContext vc = context.getRecord();
		// remove any of the previous validation error messages
		context.removeRecordFromMessages(vc);
//		if (isInWorkflow(context.getTable().getContainerAdaptation()))  {
//			final String currentSid = GOCWorkflowUtils.getCurrentSID(context.getTable().getContainerAdaptation());	
			final Adaptation gocRecord = Utils.getLinkedRecord(vc, GOC_FK_PATH);
			if (gocRecord == null) {
				return;
			}
			LOG.debug("gocRecord-->" + gocRecord);
			final Adaptation Sid = Utils.getLinkedRecord(gocRecord,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
			if (Sid == null) {
				// Other validation rules handle this situation so no need to add an error here
				return;
			}
			final String currentSid= Sid.getString(
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID);	
		
		LOG.debug("currentSid"+currentSid.toString());
		// get Region value from SID and then apply validation
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(vc.getHome().getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		
	
		final AdaptationTable sidEnhancementTable = metadataDataSet.getTable(GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
		
		final PrimaryKey sidEnhancementTablePK = sidEnhancementTable.computePrimaryKey(new Object[] {currentSid});
		
		final Adaptation sidEnhancementTableRecord = sidEnhancementTable.lookupAdaptationByPrimaryKey(sidEnhancementTablePK);
		
		String region=sidEnhancementTableRecord.get(GOCWFPaths._C_DSMT_SID_ENH._Region).toString();
		
		if("CAFIS".equalsIgnoreCase(region)){
			LOG.info("ready to validate CAFIS");
		validateMIS_Country_OU(context);
		validateCondiRegulatoryVehicle(context);
		validateSynergySubproductCheck1(context);
		//validateSynergySubproductCheck2(context);
		validateCountryBranch(context);
		validateCustomerSegmentCheck(context);
		validateOUC(context);
			}
//		}

	}

	/**
	 * Mis_country and OU combination must be consistent Combination of
	 * mis_country and f1_ou must exist in ref table
	 * 
	 * @param context
	 */
	private static void validateMIS_Country_OU(
			final ValueContextForValidationOnRecord context) {
		LOG.debug("validateMIS_Country_OU-->");
		// get F1_OU
		final ValueContext vc = context.getRecord();

		final String F1_OU = (String) context
				.getRecord()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._F1_OU);		
		if(null==F1_OU)return;
		// GET MIS_Country
		final String mis_country = (String) context
				.getRecord()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._MIS_Country);	
		
		if(null==mis_country)return;
		final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_F1_GEOGRAPHY._F1_OU.format() 
				+ " = \"" + F1_OU + "\" and"
				+ GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_F1_GEOGRAPHY._MIS_Country.format()+ " = \"" + mis_country + "\"";
	

		LOG.debug("predicate-->" + predicate);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		
		final AdaptationTable smartLATIN_AMERICA_CAFISGeo = metadataDataSet
				.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_F1_GEOGRAPHY
						.getPathInSchema());
		final RequestResult reqRes = smartLATIN_AMERICA_CAFISGeo.createRequestResult(predicate);
		
		try {
			if (reqRes.nextAdaptation() == null) {

				context.addMessage(UserMessage
						.createError("Mis_country and OU combination must be consistent combination . mis_country and f1_ou must exist in ref table SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_F1_GEOGRAPHY"));
			}
		} finally {
			reqRes.close();
		}

		
	}

	/**
	 * 
	 * Condi Vehicle must be consistent with the BU Combination of BU,
	 * condi_vehicle must exist in ref table
	 * 
	 * @param context
	 */
	private static void validateCondiRegulatoryVehicle(
			final ValueContextForValidationOnRecord context) {
		LOG.debug("validateCondiRegulatoryVehicle-->");
		// get BU from GOC
		final ValueContext vc = context.getRecord();
		
		final Adaptation gocRecord = Utils.getLinkedRecord(vc, GOC_FK_PATH);
		if (gocRecord == null) {
			return;
		}
		LOG.debug("gocRecord-->" + gocRecord);
		// get BU
		final Adaptation frsBuRecord = Utils.getLinkedRecord(gocRecord,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU);
		LOG.debug("frsBuRecord-->" + frsBuRecord);
		if (frsBuRecord == null) {
			
			return;
		}
		LOG.debug("frsBuRecord-->" + frsBuRecord);
		final String frsBu= frsBuRecord.getString(
				DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_BU_STD_C_DSMT_FRS_BU._C_DSMT_FRS_BU);

			
		// GET Condi Vehicle
		final String condiVehicle = (String) context
				.getRecord()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._CONDI_VEH);	
		
		
		final Adaptation sid = Utils.getLinkedRecord(gocRecord,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
		if (sid == null) {
			// Other validation rules handle this situation so no need to add an error here
			return;
		}
		final String currentSid= sid.getString(
				DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID);	
		
		
		if(SmartUtils.isSIDforCondiStatic(currentSid)){
					if(StringUtils.isEmpty(condiVehicle)){
						context.addMessage(UserMessage
								.createError("Condi Vehicle must have value for SID ="+currentSid));
						return;
					}else {
						return;
					}			
		}
		if(condiVehicle==null)return;
		final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU._BU.format() 
				+ " = \"" + frsBu + "\" and "
				+ GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU._Condi_Vehicle.format()+ " = \"" + condiVehicle + "\"";
		
		

		LOG.debug("predicate-->" + predicate);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		
		final AdaptationTable smartLATIN_AMERICA_CAFISSidBu = metadataDataSet
				.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU
						.getPathInSchema());
		final RequestResult reqRes = smartLATIN_AMERICA_CAFISSidBu.createRequestResult(predicate);
		
		try {
			if (reqRes.isEmpty()) {

				context.addMessage(UserMessage
						.createError("Condi Vehicle must be consistent with the BU.Combination of BU, condi_vehicle must exist in SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU table"));
			}
		} finally {
			reqRes.close();
		}
	}

	/**
	 * Subproduct must be valid
			sub_product must exist in ref table
	 * 
	 * @param context
	 */
	private static void validateSynergySubproductCheck1(
			final ValueContextForValidationOnRecord context) {
		
		LOG.debug("validateSynergySubproductCheck1-->");
			final ValueContext vc = context.getRecord();
			
			final String subProd = (String) context
							.getRecord()
							.getValue(
									GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._SYN_SUBPROD);	
		
			LOG.debug("predicate-->" + subProd);
			if(null==subProd)return;
			final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SIDES_FULL_SUB_PRODUCT._Sub_Product
					.format() + " = \"" + subProd + "\"";
			
			final Adaptation metadataDataSet;
			try {
				metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
						.getRepository());
			} catch (OperationException ex) {
				LOG.error("Error looking up metadata data set", ex);
				return;
			}
			final AdaptationTable subPrdTable = metadataDataSet
					.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SIDES_FULL_SUB_PRODUCT
							.getPathInSchema());
			final RequestResult reqRes = subPrdTable.createRequestResult(predicate);
			
			try {
				if (reqRes.isEmpty()) {

					context.addMessage(UserMessage
							.createError("Subproduct must be valid sub_product which must exist in ref SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SIDES_FULL_SUB_PRODUCT"));
				}
			} finally {
				reqRes.close();
			}

			
	}
	
	

	/**
	 * the logic should be changed to = if gfmis / MSEG is related to ICG, the subproduct entered by the requestor in the SMART Partner System table must map to the gfmis / MSEG of the GOC if product = 9900.
	 * So the driver of the validation should be the gfmis / MSEG and not the subproduct.
	 * @param context
	 */
	private static void validateSynergySubproductCheck2(
			final ValueContextForValidationOnRecord context) {
			LOG.debug("validateSynergySubproductCheck2-->");
		
			final ValueContext vc = context.getRecord();
			
			final String subProd = (String) context
							.getRecord()
							.getValue(
									GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._SYN_SUBPROD);
			
			if(null==subProd) return;			
			
				// get ManagedSegment from GOC
				final Adaptation gocRecord = Utils.getLinkedRecord(vc, GOC_FK_PATH);
				if (gocRecord == null) {
					return;
				}
				LOG.debug("gocRecord-->" + gocRecord);				
				// get BU
				// get Managed Segment
				final Adaptation mngSegRecord = Utils.getLinkedRecord(gocRecord,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
				if (mngSegRecord == null) {
					// Other validation rules handle this situation so no need to add an error here
					return;
				}
				final String mngSeg= mngSegRecord.getString(
						DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MAN_SEG_ID);
				
				if(mngSeg==null)return;
				final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SIDES_FULL_SUB_PRODUCT._GFMIS_Product
						.format() + " = \"" + mngSeg + "\"and "
						+ GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SIDES_FULL_SUB_PRODUCT._Product.format()+ " = \"" + "9900" + "\"";
				
				LOG.debug("predicate-->" +predicate);
				
				final Adaptation metadataDataSet;
				try {
					metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
							.getRepository());
				} catch (OperationException ex) {
					LOG.error("Error looking up metadata data set", ex);
					return;
				}
				final AdaptationTable oucTable = metadataDataSet
						.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SIDES_FULL_SUB_PRODUCT
								.getPathInSchema());
				final RequestResult reqRes = oucTable.createRequestResult(predicate);
				LOG.debug("reqRes-->" + reqRes.getSize());
				
				try {
					if(reqRes.isEmpty()){
						//context.addMessage(UserMessage
								//.createError("No record matching 9900 synergy product in SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SIDES_FULL_SUB_PRODUCT"));
					}else {
					
						boolean isInValid=true;
					for (Adaptation record; (record = reqRes.nextAdaptation()) != null;)
			 		{						
						String subProdRec=record.get(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SIDES_FULL_SUB_PRODUCT._Sub_Product).toString();						
						LOG.debug("subProdRec"+subProdRec);
						if(subProd.equalsIgnoreCase(subProdRec)){isInValid=false;break;}
						
			 		}
					
					if (isInValid) {
	
						context.addMessage(UserMessage
								.createError("If subproduct is related to ICG, the subproduct must map to the gfmis (managed segment) of the GOC"));
					}
					
					}
				} finally {
					reqRes.close();
				}
			
	}
	/**
	 * Country and branch combination must be valid
		country and branch should exist in ref table
	 * 
	 * @param context
	 */
	private static void validateCountryBranch(final ValueContextForValidationOnRecord context ){
				LOG.debug("validateCountryBranch-->");

				final ValueContext vc = context.getRecord();

				final String country = (String) context
						.getRecord()
						.getValue(
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._Country);		
				if(null==country)return;
				// GET MIS_Country
				final String branch = (String) context
						.getRecord()
						.getValue(
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._Branch);	
				
				if(null==branch)return;
				final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU._Country.format() 
						+ " = \"" + country + "\" and"
						+ GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU._Branch.format()+ " = \"" + branch + "\"";
				
				

				LOG.debug("predicate-->" + predicate);

				final Adaptation metadataDataSet;
				try {
					metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
							.getRepository());
				} catch (OperationException ex) {
					LOG.error("Error looking up metadata data set", ex);
					return;
				}
				
				final AdaptationTable smartLATIN_AMERICA_CAFISGeo = metadataDataSet
						.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU
								.getPathInSchema());
				final RequestResult reqRes = smartLATIN_AMERICA_CAFISGeo.createRequestResult(predicate);
				
				try {
					if (reqRes.isEmpty()) {

						context.addMessage(UserMessage
								.createError("Country and branch combination must be valid .country and branch should exist in ref table SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU"));
					}
				} finally {
					reqRes.close();
				}

	}
	
	/**
	 * ouc_check
	 * For NEW requests, OUC must not be existing in ref table
		For REMAP, DEACTIVATE requests, OUC must be existing in ref table
	 * @param context
	 */
	private static void validateOucNewDeactivate(final ValueContextForValidationOnRecord context ){
		LOG.debug("validateOucNewDeactivate-->");
		
	}
	/**
	 * customer segment must be included valid
		customer_segment should exist in ref tableRef Table 4
		Field: customer_segment
	 * @param context
	 */
	private static void validateCustomerSegment(final ValueContextForValidationOnRecord context ){
		
		LOG.debug("validateCustomerSegment-->");
			// 	branches check
			final ValueContext vc = context.getRecord();
			// get OUC from record
			final String custSeg = (String) context
							.getRecord()
							.getValue(
									GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._CUS_SEGMENT);		
		
			final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_STATIC_BRANCH_OFFSETS._ORG_Unit
					.format() + " = \"" + custSeg + "\"";
			
			final Adaptation metadataDataSet;
			try {
				metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
						.getRepository());
			} catch (OperationException ex) {
				LOG.error("Error looking up metadata data set", ex);
				return;
			}
			final AdaptationTable oucTable = metadataDataSet
					.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_STATIC_BRANCH_OFFSETS
							.getPathInSchema());
			final RequestResult reqRes = oucTable.createRequestResult(predicate);
			LOG.debug("reqRes-->" + reqRes.getSize());
			try {
				if (reqRes.nextAdaptation() != null) {

					context.addMessage(UserMessage
							.createWarning("ouc must not exist in reference table SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_STATIC_BRANCH_OFFSETS"));
				}
			} finally {
				reqRes.close();
			}	
	}
	/**
	 * Validate OUC
	 * 
	 * @param context
	 */
	
	private static void validateOUC(final ValueContextForValidationOnRecord context){
		LOG.debug("validateOUC-->");
		final ValueContext vc = context.getRecord();
		
		final Adaptation gocRecord = Utils.getLinkedRecord(vc, GOC_FK_PATH);
		if (gocRecord == null) {
			return;
		}
		final String actionRequired = gocRecord.getString(ACTION_REQUIRED_PATH);
		LOG.debug("actionRequired-->"+actionRequired);
		
		if ((GOCConstants.ACTION_CREATE_GOC_LEG_CCS.equals(actionRequired) || GOCConstants.ACTION_REACTIVATE_GOC_LEG_CCS
				.equals(actionRequired))) {
			validateStaticOUC(context);
			return;
		}
		if ((GOCConstants.ACTION_DEACTIVATE_GOC_LEG_CCS.equals(actionRequired) || GOCConstants.ACTION_MODIFY_RE_ONLY
				.equals(actionRequired))) {
			validateRemapStaticOUC(context);
			
		}
		
		
			// get OUC from record
			final String ouc = (String) context
							.getRecord()
							.getValue(
									GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._OUC);		
			LOG.debug("predicate-->" + ouc);
			if(null==ouc)return;
			
			validateOUCBranchesCheck(context);
			validateOUCBranch_offsets_check(context);
			validateOUCVehiclesCheck(context);
			validateVehicleOffsetsCheck(context);
			validateCondiMapsCheck(context);
			validateCondiMapsExtraCheck(context);
	}
	
	
	
	private static void validateOUCBranchesCheck(final ValueContextForValidationOnRecord context){
		
		LOG.debug("validateOUCBranchesCheck-->");
		// 	branches check
		final ValueContext vc = context.getRecord();
		// get OUC from record
		final String ouc = (String) context
						.getRecord()
						.getValue(
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._OUC);		
		LOG.debug("predicate-->" + ouc);
		if(null==ouc)return;
		final String predicateBranchesCheck = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_BRANCHES._Default_OUC.format() 
				+ " = \"" + ouc + "\" or "
				+ GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_BRANCHES._Pool_Offset_OUC.format()+ " = \"" + ouc + "\"";
		
		
		
		
		LOG.debug("predicate-->" + predicateBranchesCheck);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable odmAllDepts = metadataDataSet
				.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_BRANCHES
						.getPathInSchema());
		final RequestResult reqRes = odmAllDepts.createRequestResult(predicateBranchesCheck);
		
		try {
			if (!reqRes.isEmpty()) {

				context.addMessage(UserMessage
						.createWarning("ouc must not exist in reference table SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_BRANCHES"));
			}
		} finally {
			reqRes.close();
		}

		
		
	}
	
	private static void validateOUCBranch_offsets_check(final ValueContextForValidationOnRecord context){
		LOG.debug("validateOUCBranch_offsets_check-->");
		// 	branches check
		final ValueContext vc = context.getRecord();
		// get OUC from record
		final String ouc = (String) context
						.getRecord()
						.getValue(
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._OUC);		
		if(null==ouc)return;
		final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_STATIC_BRANCH_OFFSETS._ORG_Unit
				.format() + " = \"" + ouc + "\"";
		
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable oucTable = metadataDataSet
				.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_STATIC_BRANCH_OFFSETS
						.getPathInSchema());
		final RequestResult reqRes = oucTable.createRequestResult(predicate);
		
		try {
			if (!reqRes.isEmpty()) {

				context.addMessage(UserMessage
						.createWarning("ouc must not exist in reference table SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_STATIC_BRANCH_OFFSETS"));
			}
		} finally {
			reqRes.close();
		}

	}
	
	private static void validateOUCVehiclesCheck(final ValueContextForValidationOnRecord context){
		LOG.debug("validateOUCVehiclesCheck-->");
		// 	branches check
		final ValueContext vc = context.getRecord();
		// get OUC from record
		final String ouc = (String) context
						.getRecord()
						.getValue(
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._OUC);		
		if(null==ouc)return;
		final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_VEHICLES._Tax_Offset_OUC
				.format() + " = \"" + ouc + "\"";
		
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable oucTable = metadataDataSet
				.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_VEHICLES
						.getPathInSchema());
		final RequestResult reqRes = oucTable.createRequestResult(predicate);
		
		try {
			if (!reqRes.isEmpty()) {

				context.addMessage(UserMessage
						.createWarning("ouc must not exist in reference table SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_VEHICLES"));
			}
		} finally {
			reqRes.close();
		}

	}
	
	
	private static void validateVehicleOffsetsCheck(final ValueContextForValidationOnRecord context){
		LOG.debug("validateVehicleOffsetsCheck-->");
		// 	branches check
		final ValueContext vc = context.getRecord();
		// get OUC from record
		final String ouc = (String) context
						.getRecord()
						.getValue(
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._OUC);		
		if(null==ouc)return;
		final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_VEHICLE_OFFSETS._Org_Unit
				.format() + " = \"" + ouc + "\"";
		
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable oucTable = metadataDataSet
				.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_VEHICLE_OFFSETS
						.getPathInSchema());
		final RequestResult reqRes = oucTable.createRequestResult(predicate);
	
		try {
			if (!reqRes.isEmpty()) {

				context.addMessage(UserMessage
						.createWarning("ouc must not exist in reference table SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_VEHICLE_OFFSETS"));
			}
		} finally {
			reqRes.close();
		}

	}
	
	
	
	private static void validateCondiMapsCheck(final ValueContextForValidationOnRecord context){
		// 	branches check
		final ValueContext vc = context.getRecord();
		// get OUC from record
		final String ouc = (String) context
						.getRecord()
						.getValue(
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._OUC);		
		if(null==ouc)return;
		final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_MANUAL_CONDI_MAPS._Default_Org_Unit
				.format() + " = \"" + ouc + "\"";
		
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable oucTable = metadataDataSet
				.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_MANUAL_CONDI_MAPS
						.getPathInSchema());
		final RequestResult reqRes = oucTable.createRequestResult(predicate);
		
		try {
			if (!reqRes.isEmpty()) {

				context.addMessage(UserMessage
						.createWarning("ouc must not exist in reference table SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_MANUAL_CONDI_MAPS"));
			}
		} finally {
			reqRes.close();
		}

	}
	
	private static void validateCondiMapsExtraCheck(final ValueContextForValidationOnRecord context){
		// 	branches check
		final ValueContext vc = context.getRecord();
		// get OUC from record
		final String ouc = (String) context
						.getRecord()
						.getValue(
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._OUC);		
		if(null==ouc)return;
		final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_MANUAL_CONDI_MAPS_EXTRA._Default_Org_Unit
				.format() + " = \"" + ouc + "\"";
		
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable oucTable = metadataDataSet
				.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_MANUAL_CONDI_MAPS_EXTRA
						.getPathInSchema());
		final RequestResult reqRes = oucTable.createRequestResult(predicate);
		LOG.debug("reqRes-->" + reqRes.getSize());
		try {
			if (!reqRes.isEmpty()) {

				context.addMessage(UserMessage
						.createWarning("ouc must not exist in reference table SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_MANUAL_CONDI_MAPS_EXTRA"));
			}
		} finally {
			reqRes.close();
		}

	}
	
	private static void validateStaticOUC(final ValueContextForValidationOnRecord context){
		// 	branches check
		final ValueContext vc = context.getRecord();
		// get OUC from record
		final String ouc = (String) context
						.getRecord()
						.getValue(
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._OUC);		
	
		final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_OUC._ORG_unit
				.format() + " = \"" + ouc + "\"";
		if(null==ouc)return;
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable oucTable = metadataDataSet
				.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_OUC
						.getPathInSchema());
		final RequestResult reqRes = oucTable.createRequestResult(predicate);
	
		try {
			if (!reqRes.isEmpty()) {

				context.addMessage(UserMessage
						.createWarning("ouc must not exist in reference table SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_OUC"));
			}
		} finally {
			reqRes.close();
		}

	}
	private static void validateRemapStaticOUC(final ValueContextForValidationOnRecord context){
		// 	branches check
		final ValueContext vc = context.getRecord();
		// get OUC from record
		final String ouc = (String) context
						.getRecord()
						.getValue(
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._OUC);		
		if(null==ouc)return;
		final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_OUC._ORG_unit
				.format() + " = \"" + ouc + "\"";
		
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable oucTable = metadataDataSet
				.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_OUC
						.getPathInSchema());
		final RequestResult reqRes = oucTable.createRequestResult(predicate);
		
		try {
			if (reqRes.isEmpty()) {

				context.addMessage(UserMessage
						.createWarning("ouc must  exist in reference table SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_OUC"));
			}
		} finally {
			reqRes.close();
		}

	}
	

	/**
	 * customer segment must be included valid
	 * customer_segment should exist in ref table
	 * @param context
	 */
	private static void validateCustomerSegmentCheck(final ValueContextForValidationOnRecord context){
		// 	branches check
		final ValueContext vc = context.getRecord();
		// get OUC from record
		final Integer custSeg = (Integer) context
						.getRecord()
						.getValue(
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._CUS_SEGMENT);		
		if(null==custSeg)return;
		final String predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_ORG_UNIT_ATTRIBUTES._ORG_Unit_Attribute
				.format() + " = \"" + custSeg.toString() + "\"";
		
		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
			return;
		}
		final AdaptationTable oucTable = metadataDataSet
				.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_ORG_UNIT_ATTRIBUTES
						.getPathInSchema());
		final RequestResult reqRes = oucTable.createRequestResult(predicate);
		
		try {
			if (reqRes.isEmpty()) {

				context.addMessage(UserMessage
						.createError("customer segment  must  exist in reference table _SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_STATIC_ORG_UNIT_ATTRIBUTES"));
			}
		} finally {
			reqRes.close();
		}

	}
	
	private static boolean isInWorkflow(final Adaptation dataSet) {
		final String currentSid = GOCWorkflowUtils.getCurrentSID(dataSet);
		return (currentSid != null && ! "".equals(currentSid));
	}
	
	
}
