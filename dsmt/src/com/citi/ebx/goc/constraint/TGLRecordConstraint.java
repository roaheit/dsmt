package com.citi.ebx.goc.constraint;

import java.util.Locale;

import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPartnerSystemPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class TGLRecordConstraint implements
							ConstraintOnTableWithRecordLevelCheck{
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	private static final Path GOC_FK_PATH = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_ASIA._C_GOC_FK;
	
	
	@Override
	public void checkTable(ValueContextForValidationOnTable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
		
		final ValueContext vc = context.getRecord();
		// remove any of the previous validation error messages
		context.removeRecordFromMessages(vc);
		LOG.debug("isCheckOnUserInput--->"+context.isCheckOnUserInput());
		if(!context.isCheckOnUserInput()){
		validateCrop(context);
		validateTreasureySegment(context);
		}
	}

	/**
	 * Corp code must must exist in ref table _TGL_REFERENCE_DATA_TGL_FDBU1_TABLE
	 * 
	 * @param context
	 */
	private static void validateCrop(
			final ValueContextForValidationOnRecord context) {
		LOG.debug("TGLRecordConstraint-->validateCrop");
		
		final ValueContext vc = context.getRecord();
		boolean isInValid=true;
		// get cropCode
		final String cropCode = (String) context
				.getRecord()
				.getValue(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_TGL._Corp);	
		LOG.debug("validateCrop -->cropCode : " + cropCode);
		if(null==cropCode)return;
			final Adaptation gocRecord = Utils.getLinkedRecord(vc, GOC_FK_PATH);
			if (gocRecord == null) {
				return;
			}
		LOG.debug("gocRecord-->" + gocRecord);
		// get BU
		final Adaptation frsBuRecord = Utils.getLinkedRecord(gocRecord,
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU);
		
		
		
		final String predicate = GOCPartnerSystemPaths._TGL_REFERENCE_DATA_TGL_FDBU1_TABLE._GLCORP.format()
				+ " = \"" + cropCode +  "\"";
	

		LOG.debug("validateCrop -->predicate : " + predicate);

		final Adaptation metadataDataSet;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(vc.getHome()
					.getRepository());
			} catch (OperationException ex) {
				LOG.error("Error looking up metadata data set", ex);
				return;
				}
		
		
		final AdaptationTable tglFdbu1 = metadataDataSet
				.getTable(GOCPartnerSystemPaths._TGL_REFERENCE_DATA_TGL_FDBU1_TABLE
						.getPathInSchema());
		final RequestResult reqRes = tglFdbu1.createRequestResult(predicate);
		
		try {
			if (reqRes.isEmpty() ) {
				context.addMessage(UserMessage
						.createError("Corp Code should be a valid GLCorp in the table TGL_REFERENCE_DATA_TGL_FDBU1_TABLE"));
			}
			else{
					LOG.debug("frsBuRecord-->" + frsBuRecord);
					if (frsBuRecord == null) {
						return;
					}
					
					final String frsBu= frsBuRecord.getString(
							DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_BU_STD_C_DSMT_FRS_BU._C_DSMT_FRS_BU);
					LOG.debug("frsBu-->" + frsBu);
					for (Adaptation record; (record = reqRes.nextAdaptation()) != null;)
			 		{
						String entity =record.getString(GOCPartnerSystemPaths._TGL_REFERENCE_DATA_TGL_FDBU1_TABLE._ENTITY);
						
						
						if(null!=entity && frsBu.equalsIgnoreCase(entity)){
							isInValid=false;
							
						}
			 		}
					LOG.debug("isInValid-->" + isInValid);
					if(isInValid){
						context.addMessage(UserMessage
								.createError("GOC BU must be valid Entity in _TGL_REFERENCE_DATA_TGL_FDBU1_TABLE"));
					}
			}
		} finally {
			reqRes.close();
		}
	}
		private static void validateTreasureySegment(
				final ValueContextForValidationOnRecord context) {
			LOG.debug("TGLRecordConstraint-->validateTreasureySegment");
			
			final ValueContext vc = context.getRecord();
			// get cropCode
			final String cropCode = (String) context
					.getRecord()
					.getValue(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_TGL._Corp);	
			final String treasureSegment = (String) context
			.getRecord()
			.getValue(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_TGL._Treasury_Segment);	
			LOG.debug("validateCrop -->cropCode : " + cropCode);
			if(null==cropCode)return;
				final Adaptation gocRecord = Utils.getLinkedRecord(vc, GOC_FK_PATH);
				if (gocRecord == null) {
					return;
				}
			LOG.debug("gocRecord-->" + gocRecord);
			LOG.debug("treasureSegment-->" + treasureSegment);
			// get BU
			if((cropCode.equals("00001"))&&(treasureSegment==null||treasureSegment=="")){
				context.addMessage(UserMessage
						.createError("Treasury Segment shouldn't be kept blank for Corp code 00001"));
			}
		
			} 
		}
			



	

		

