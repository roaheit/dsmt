package com.citi.ebx.goc.constraint;

import java.util.Locale;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;

public class LEMMappingRecordConstraint implements ConstraintOnTableWithRecordLevelCheck{

	@Override
	public void checkTable(ValueContextForValidationOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
	
		final ValueContext vc = context.getRecord();
		final String lvid=String.valueOf(vc.getValue(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID));
		AdaptationTable table= context.getTable();
		final String lemPredicate = AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID
				.format()
				+ "= '"
				+ lvid
				+ "' and "
				+ AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM_TYPE
						.format() + "= '" 
				+ AccountibilityMatrixConstants.LEM_TYPE_LEM  
				+ "' and "
				+ AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._EFF_STATUS
						.format() + "= '" 
				+ AccountibilityMatrixConstants.ACTIVESTATUS + 
				"'";
		
		
		RequestResult reqRes = table.createRequestResult(lemPredicate);
		if(reqRes.getSize()>1){
			context.addMessage(UserMessage.createError(
					"There should be only one active LEM for LE "+ lvid));
		}
		
		
	}

}
