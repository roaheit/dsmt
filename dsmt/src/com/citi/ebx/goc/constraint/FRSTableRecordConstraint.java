/**
 * 
 */
package com.citi.ebx.goc.constraint;

import java.util.Locale;

import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

/**
 * @author rk00242
 * 
 */
public class FRSTableRecordConstraint implements
		ConstraintOnTableWithRecordLevelCheck {

	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	public String primaryKeyField;
	
	

	public String getPrimaryKeyField() {
		return primaryKeyField;
	}

	public void setPrimaryKeyField(String primaryKeyField) {
		this.primaryKeyField = primaryKeyField;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.orchestranetworks.schema.ConstraintOnTable#checkTable(com.
	 * orchestranetworks.instance.ValueContextForValidationOnTable)
	 */
	@Override
	public void checkTable(ValueContextForValidationOnTable arg0) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.orchestranetworks.schema.ConstraintOnTable#setup(com.orchestranetworks
	 * .schema.ConstraintContextOnTable)
	 */
	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.orchestranetworks.schema.ConstraintOnTable#toUserDocumentation(java
	 * .util.Locale, com.orchestranetworks.instance.ValueContext)
	 */
	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck#
	 * checkRecord
	 * (com.orchestranetworks.instance.ValueContextForValidationOnRecord)
	 */
	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
		// TODO Auto-generated method stub

		final AdaptationTable table = context.getTable();
		LOG.info("check table name : " + table);

		final ValueContext vc = context.getRecord();

		String ou = vc.getValue(Path.parse(primaryKeyField)) != null ? vc
				.getValue(Path.parse(primaryKeyField)).toString() : "";
		LOG.info("check ou : " + ou);

		String predicate = Path.parse(primaryKeyField).format() + "='" + ou
				+ "'";

		RequestResult res = table.createRequestResult(predicate);

		if (res.getSize() > 1) {
			context.addMessage(UserMessage
					.createWarning("Record with same OU already exists."));
		}

		String level = vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_C_DSMT_LVID_TBL._LEVEL) != null ? vc
				.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_C_DSMT_LVID_TBL._LEVEL).toString() : "";
		if(!level.isEmpty()){

		int levl = Integer.parseInt(level);

		if (levl > 24) {
			context.addMessage(UserMessage
					.createError("Level is more than 24."));
		}else if(levl > 19 && levl < 24 ){
			context.addMessage(UserMessage
					.createWarning("Alert !!! Level is in between 20 and 24"));
		}

		}
	}

}
