/**
 * 
 */
package com.citi.ebx.goc.constraint;

import java.util.List;
import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.NodeDifference;

/**
 * @author rk00242
 * 
 */
public class DSMTRecordConstraint implements
		ConstraintOnTableWithRecordLevelCheck {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();

	public String code;
	public String status;
	private String ACTION_REQUIRED_PATH = "./ACTION_REQ";
	String STATUS = "./EFF_STATUS";
	private String effEndDateFieldPath = "./ENDDT";
	


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.orchestranetworks.schema.ConstraintOnTable#checkTable(com.
	 * orchestranetworks.instance.ValueContextForValidationOnTable)
	 */
	@Override
	public void checkTable(ValueContextForValidationOnTable arg0) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.orchestranetworks.schema.ConstraintOnTable#setup(com.orchestranetworks
	 * .schema.ConstraintContextOnTable)
	 */
	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.orchestranetworks.schema.ConstraintOnTable#toUserDocumentation(java
	 * .util.Locale, com.orchestranetworks.instance.ValueContext)
	 */
	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck#
	 * checkRecord
	 * (com.orchestranetworks.instance.ValueContextForValidationOnRecord)
	 */
	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
		LOG.info("in DSMT record constraint");
		final AdaptationTable table = context.getTable();
		final Adaptation dataSet = table.getContainerAdaptation();
		if (isInChildDataSpace(dataSet)) {
			ValueContext vc = context.getRecord();
			ValueContext oldVC = null;
			// remove any of the previous validation error messages
			context.removeRecordFromMessages(vc);

			final String actionRequired = (String) vc.getValue(Path
					.parse(ACTION_REQUIRED_PATH));

			final AdaptationTable initialTable = GOCWorkflowUtils
					.getTableInInitialSnapshot(table);

			// Get the record in the initial snapshot, to compare against
			Adaptation initialRecord = initialTable
					.lookupAdaptationByPrimaryKey(vc);
			Adaptation currentRecord = table
					.lookupAdaptationByPrimaryKey(vc);
			
			String primaryCode = String.valueOf(vc.getValue(Path.parse(code)));
			
			final String pred1 = code +"= '" + primaryCode + "' ";
		
			final String pred2 = "date-equal(" + effEndDateFieldPath
					+ ",'9999-12-31')";
			
			final String pred3 = "osd:is-null(" + effEndDateFieldPath+ ")";
			

			final String predicate = pred1  + " and (" + pred2
					+ " or " + pred3 + ")";
			LOG.info("checking predicate : "+predicate);
		
			RequestResult req = initialTable.createRequestResult(predicate);

			LOG.info("Check initial record " + initialRecord);
			LOG.info("check current record " + currentRecord);

			if (initialRecord == null && req.getSize()<1) {
				if (!DSMTConstants.AR_ADDITION.equals(actionRequired)|| actionRequired==null) {
					context.addMessage(UserMessage
							.createError("Action must be "
									+ "selected as Create for a new record."));
				}
			} else {
				
				Adaptation previousRec = req.nextAdaptation();
				
				if (initialRecord == null){
					String initialStatus = previousRec.getString(Path.parse(STATUS));
					LOG.info("checking initial status : "+initialStatus);
					String currentStatus = String.valueOf(vc.getValue((Path
							.parse(STATUS))));
					LOG.info("checking current status : "+currentStatus);
					if (hasStatusChanged(currentStatus,initialStatus)) {

							validateActiveDeactiveChange(context,
									currentStatus, actionRequired);
					}
					else {
						if (actionRequired==null || DSMTConstants.AR_DEACTIVATION
								.equals(actionRequired) ) {
							context.addMessage(UserMessage
									.createError("Action Required cannot be Deactivate "
											+ "if status has not changed."));
						} else if (actionRequired==null || DSMTConstants.AR_REACTIVATION
								.equals(actionRequired) ) {
							context.addMessage(UserMessage
									.createError("Action Required cannot be Reactivate "
											+ "if status has not changed."));
						}
					}
					if (!DSMTConstants.AR_UPDATION.equals(actionRequired)
							&& !DSMTConstants.AR_DEACTIVATION
									.equals(actionRequired)
							&& !DSMTConstants.AR_REACTIVATION
									.equals(actionRequired)|| actionRequired==null) {
						System.out.println("in warning while modify");
						context.addMessage(UserMessage
								.createError("Action must be "
										+ "selected while modifying a record."));
					}
				}
				else{
				DifferenceBetweenOccurrences diff = DifferenceHelper
						.compareOccurrences(initialRecord, currentRecord, true);
				oldVC = initialRecord.createValueContext();
				
				if (diff == null || diff.isEmpty() ) {
					LOG.info("difference is null");
				}

				else {
					String currStatus = String.valueOf(vc.getValue((Path
							.parse(STATUS))));
					String initialSts = String.valueOf(oldVC.getValue(Path
							.parse(STATUS)));
					
					LOG.info("modification of record is performed");
					if (hasStatusChanged(currStatus, initialSts)) {
							validateActiveDeactiveChange(context,
									currStatus, actionRequired);
						
					} else {
						if (DSMTConstants.AR_DEACTIVATION
								.equals(actionRequired)) {
							context.addMessage(UserMessage
									.createError("Action Required cannot be Deactivate "
											+ "if status has not changed."));
						} else if (DSMTConstants.AR_REACTIVATION
								.equals(actionRequired)) {
							context.addMessage(UserMessage
									.createError("Action Required cannot be Reactivate "
											+ "if status has not changed."));
						}
					}
					if (!DSMTConstants.AR_UPDATION.equals(actionRequired)
							&& !DSMTConstants.AR_DEACTIVATION
									.equals(actionRequired)
							&& !DSMTConstants.AR_REACTIVATION
									.equals(actionRequired)) {
						if(checkauditColumnChange(diff)){
						System.out.println("in warning while modify");
						context.addMessage(UserMessage
								.createError("Action must be "
										+ "selected while modifying a record."));
						}
					}
				}
				}
			}
		}
	}

	private static boolean isInChildDataSpace(final Adaptation dataSet) {

		LOG.info("isInChildDataSpace called ");
		AdaptationHome dataspace = dataSet.getHome();

		if (dataspace.toString().contains("DSMT2")) {
			LOG.info("Is in master DataSpace");
			return false;
		} else {
			LOG.info("Is in Child DataSpace");
			return true;
		}
	}

	private void validateActiveDeactiveChange(
			ValueContextForValidationOnRecord context, String statusValue,
			String actionRequired) {
		LOG.info("In validateActiveDeactiveChange");
		LOG.info("check status value : " + statusValue);

		if (statusValue.equals(DSMTConstants.ACTIVE)
				&& (actionRequired==null ||!actionRequired.equals(DSMTConstants.AR_REACTIVATION)
						|| actionRequired.equals(DSMTConstants.AR_NO_CHANGE) || actionRequired
							.equals(DSMTConstants.AR_UPDATION))) {
			LOG.info("Action selected must not be Deactivate ");
			context.addMessage(UserMessage
					.createError("Action selected should be Reactivate"));
		} else if (statusValue.equals(DSMTConstants.INACTIVE)
				&& (actionRequired==null || !actionRequired.equals(DSMTConstants.AR_DEACTIVATION)) ) {
			LOG.info("Action selected must not be Reactivate ");
			context.addMessage(UserMessage
					.createError("Action selected should be Deactivate"));
		}

	}

	private boolean hasStatusChanged(String currentStatus, String initialStatus) {
		LOG.info("In hasStatusChanged");
		boolean checkStatus = false;

		if (!currentStatus.equals(initialStatus)) {
			checkStatus = true;
		}
		return checkStatus;
	}

	private boolean checkauditColumnChange(DifferenceBetweenOccurrences diff){
		List<NodeDifference> changedRecord = diff.getNodeDifferences();
		for (NodeDifference i : changedRecord){
			
			String chgRec = changedRecord.toString();
			System.out.println("check chg rec " +chgRec);
					

			if (i.getLeftValuePath().format().equalsIgnoreCase("/ENDDT") && chgRec.contains("/CHNG_DTTM") ){
				return false;
		}
		
			}
		
		return true;
	}
	
	
	public String getCode() {
	return code;
	}

	public void setCode(String code) {
	this.code = code;
	}



	public String getStatus() {
	return status;
	}

	public void setStatus(String status) {
	this.status = status;
	}
	
}
