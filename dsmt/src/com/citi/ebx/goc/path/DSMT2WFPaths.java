package com.citi.ebx.goc.path;

import com.orchestranetworks.schema.Path;

public interface DSMT2WFPaths {
	
	public static final Path _Root = Path.parse("/root");

	
	
	public final class _C_DSMT_WF_REPORTING
	
	
	{
		public static final Path _C_DSMT_WF_REPORTING = _Root.add(Path.parse("C_DSMT_WF_REPORTING"));
		public static Path getPathInSchema()
		{
			return _C_DSMT_WF_REPORTING;
		}
		public static final Path _REQUEST_ID = Path.parse("./REQUEST_ID");
		public static final Path _TABLE_NAME=Path.parse("./TABLE_NAME");
		public static final Path _BAL_CD=Path.parse("C_DSMT_BAL_TYPE_CD");
		public static final Path _Child_DataSpace_ID=Path.parse("./CHILD_DATASPACE_ID");
		public static final Path _DataSet_Name=Path.parse("./DATASET_NAME");
		public static final Path _Request_Status=Path.parse("./REQUEST_STATUS");
		public static final Path _Submitted_Date=Path.parse("./SUBIMITTED_DT");
		public static final Path _REQ_DT=Path.parse("./REQ_DT");
		
		
	}
	
	public final class _C_APP_WF_REPORTING{
		
		public static final Path _C_APP_WF_REPORTING = _Root.add(Path.parse("C_APP_WF_REPORTING"));
		public static Path getPathInSchema()
		{
			return _C_APP_WF_REPORTING;
		}
		public static final Path _REQUEST_ID = Path.parse("./REQUEST_ID");
		public static final Path _TABLE_NAME=Path.parse("./TABLE_NAME");
		public static final Path _REQUEST_TYPE = Path.parse("./REQUEST_TYPE");
		public static final Path _CHILD_DATASPACE_ID = Path.parse("./CHILD_DATASPACE_ID");
		public static final Path _REQ_DT = Path.parse("./REQ_DT");
		public static final Path _REQ_BY = Path.parse("./REQ_BY");
		public static final Path _REQUEST_STATUS = Path.parse("./REQUEST_STATUS");
		public static final Path _SUBIMITTED_DT = Path.parse("./SUBIMITTED_DT");
		public static final Path _Requester_Comment = Path.parse("./Requester_Comment");
		public static final Path _Table_Path = Path.parse("./TABLE_PATH");
		public static final Path _CHNG_DTTM = Path.parse("./CHNG_DTTM");
	}
	
	public final class _C_DSMT_WF_CAL{
		
		public static final Path _C_DSMT_WF_CAL = _Root.add(Path.parse("C_DSMT_WF_CAL"));
		public static Path getPathInSchema()
		{
			return _C_DSMT_WF_CAL;
		}
		public static final Path _YEAR = Path.parse("./YEAR");
		public static final Path _MONTH =Path.parse("./MONTH");
		public static final Path _REG_START_DATE = Path.parse("./REG_START_DATE");
		public static final Path _REG_END_DATE = Path.parse("./REG_END_DATE");
		
	}
	
	
	
	}
	
	

