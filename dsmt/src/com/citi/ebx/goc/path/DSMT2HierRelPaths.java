package com.citi.ebx.goc.path;

import com.orchestranetworks.schema.Path;
/**
 * Generated by EBX5 5.5.0 fix D [0953:0008], at  2014/11/29 11:16:56 [EST].
 * WARNING: Any manual changes to this class may be overwritten by generation process.
 * DO NOT MODIFY THIS CLASS.
 * 
 * This interface defines constants related to schema [Publication: DSMT2_REL].
 * 
 * Root paths in this interface: 
 * 	'/root'   relativeToRoot: false
 * 
 */
public interface DSMT2HierRelPaths
{
	// ===============================================
	// Constants for nodes under '/root'.
	// Prefix:  ''.
	// Statistics:
	//		100 path constants.
	//		87 leaf nodes.
	public static final Path _Root = Path.parse("/root");
	public static final Path _GLOBAL_STD = _Root.add("GLOBAL_STD");
	public static final Path _GLOBAL_STD_ENT_STD = _GLOBAL_STD.add("ENT_STD");
	public static final Path _GLOBAL_STD_ENT_STD_F_GOC_STD = _GLOBAL_STD_ENT_STD.add("F_GOC_STD");

	// Table type path
	public final class _GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY {
		private static final Path _GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY = _GLOBAL_STD_ENT_STD_F_GOC_STD.add("C_DSMT_GOC_HISTORY");
		public static Path getPathInSchema()
		{
			return _GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY;
		}
	public static final Path _SETID = Path.parse("./SETID");
	public static final Path _C_GOC_ID = Path.parse("./C_GOC_ID");
	public static final Path _EFFDT = Path.parse("./EFFDT");
	public static final Path _EFF_STATUS = Path.parse("./EFF_STATUS");
	public static final Path _DESCR = Path.parse("./DESCR");
	public static final Path _DESCRSHORT = Path.parse("./DESCRSHORT");
	public static final Path _C_DSMT_SID_FK = Path.parse("./C_DSMT_SID_FK");
	public static final Path _SID_EFF_DATE = Path.parse("./SID_EFF_DATE");
	public static final Path _C_DSMT_MAN_GEO_FK = Path.parse("./C_DSMT_MAN_GEO_FK");
	public static final Path _MAN_GEO_EFF_DATE = Path.parse("./MAN_GEO_EFF_DATE");
	public static final Path _C_DSMT_MAN_SEG_FK = Path.parse("./C_DSMT_MAN_SEG_FK");
	public static final Path _MAN_SEG_EFF_DATE = Path.parse("./MAN_SEG_EFF_DATE");
	public static final Path _C_DSMT_FRS_BU_FK = Path.parse("./C_DSMT_FRS_BU_FK");
	public static final Path _BU_EFF_DATE = Path.parse("./BU_EFF_DATE");
	public static final Path _C_DSMT_FRS_OU_FK = Path.parse("./C_DSMT_FRS_OU_FK");
	public static final Path _OU_EFF_DATE = Path.parse("./OU_EFF_DATE");
	public static final Path _C_DSMT_FUNCTION_FK = Path.parse("./C_DSMT_FUNCTION_FK");
	public static final Path _FUNCTION_EFF_DATE = Path.parse("./FUNCTION_EFF_DATE");
	public static final Path _C_DSMT_LVID_FK = Path.parse("./C_DSMT_LVID_FK");
	public static final Path _LVID_EFF_DATE = Path.parse("./LVID_EFF_DATE");
	public static final Path _C_DSMT_GOC_USAGE_FK = Path.parse("./C_DSMT_GOC_USAGE_FK");
	public static final Path _GOC_USAGE_EFF_DATE = Path.parse("./GOC_USAGE_EFF_DATE");
	public static final Path _HEADCOUNT_GOC = Path.parse("./HEADCOUNT_GOC");
	public static final Path _WF_REQUEST_ID = Path.parse("./WF_REQUEST_ID");
	public static final Path _DATASPACE_ID = Path.parse("./DATASPACE_ID");
	public static final Path _MERGE_DATE = Path.parse("./MERGE_DATE");
	public static final Path _ApproverSOEID = Path.parse("./ApproverSOEID");
	public static final Path _COMPANY = Path.parse("./COMPANY");
	public static final Path _SETID_LOCATION = Path.parse("./SETID_LOCATION");
	public static final Path _LOCATION = Path.parse("./LOCATION");
	public static final Path _MANAGER_ID = Path.parse("./MANAGER_ID");
	public static final Path _MANAGER_NAME = Path.parse("./MANAGER_NAME");
	public static final Path _TAX_LOCATION_CD = Path.parse("./TAX_LOCATION_CD");
	public static final Path _MANAGER_POSN = Path.parse("./MANAGER_POSN");
	public static final Path _BUDGET_YR_END_DT = Path.parse("./BUDGET_YR_END_DT");
	public static final Path _BUDGET_LVL = Path.parse("./BUDGET_LVL");
	public static final Path _GL_EXPENSE = Path.parse("./GL_EXPENSE");
	public static final Path _EEO4_FUNCTION = Path.parse("./EEO4_FUNCTION");
	public static final Path _CAN_IND_SECTOR = Path.parse("./CAN_IND_SECTOR");
	public static final Path _ACCIDENT_INS = Path.parse("./ACCIDENT_INS");
	public static final Path _SI_ACCIDENT_NUM = Path.parse("./SI_ACCIDENT_NUM");
	public static final Path _HAZARD = Path.parse("./HAZARD");
	public static final Path _ESTABID = Path.parse("./ESTABID");
	public static final Path _RISKCD = Path.parse("./RISKCD");
	public static final Path _GVT_DESCR40 = Path.parse("./GVT_DESCR40");
	public static final Path _GVT_SUB_AGENCY = Path.parse("./GVT_SUB_AGENCY");
	public static final Path _GVT_PAR_LINE2 = Path.parse("./GVT_PAR_LINE2");
	public static final Path _GVT_PAR_LINE3 = Path.parse("./GVT_PAR_LINE3");
	public static final Path _GVT_PAR_LINE4 = Path.parse("./GVT_PAR_LINE4");
	public static final Path _GVT_PAR_LINE5 = Path.parse("./GVT_PAR_LINE5");
	public static final Path _GVT_PAR_DESCR2 = Path.parse("./GVT_PAR_DESCR2");
	public static final Path _GVT_PAR_DESCR3 = Path.parse("./GVT_PAR_DESCR3");
	public static final Path _GVT_PAR_DESCR4 = Path.parse("./GVT_PAR_DESCR4");
	public static final Path _GVT_PAR_DESCR5 = Path.parse("./GVT_PAR_DESCR5");
	public static final Path _CLASS_UNIT_NZL = Path.parse("./CLASS_UNIT_NZL");
	public static final Path _ORG_UNIT_AUS = Path.parse("./ORG_UNIT_AUS");
	public static final Path _WORK_SECTOR_AUS = Path.parse("./WORK_SECTOR_AUS");
	public static final Path _APS_AGENT_CD_AUS = Path.parse("./APS_AGENT_CD_AUS");
	public static final Path _IND_COMMITTEE_BEL = Path.parse("./IND_COMMITTEE_BEL");
	public static final Path _NACE_CD_BEL = Path.parse("./NACE_CD_BEL");
	public static final Path _FTE_EDIT_INDC = Path.parse("./FTE_EDIT_INDC");
	public static final Path _DEPT_TENURE_FLG = Path.parse("./DEPT_TENURE_FLG");
	public static final Path _TL_DISTRIB_INFO = Path.parse("./TL_DISTRIB_INFO");
	public static final Path _USE_BUDGETS = Path.parse("./USE_BUDGETS");
	public static final Path _USE_ENCUMBRANCES = Path.parse("./USE_ENCUMBRANCES");
	public static final Path _USE_DISTRIBUTION = Path.parse("./USE_DISTRIBUTION");
	public static final Path _BUDGET_DEPTID = Path.parse("./BUDGET_DEPTID");
	public static final Path _DIST_PRORATE_OPTN = Path.parse("./DIST_PRORATE_OPTN");
	public static final Path _HP_STATS_DEPT_CD = Path.parse("./HP_STATS_DEPT_CD");
	public static final Path _HP_STATS_FACULTY = Path.parse("./HP_STATS_FACULTY");
	public static final Path _ACCOUNTING_OWNER = Path.parse("./ACCOUNTING_OWNER");
	public static final Path _COUNTRY_GRP = Path.parse("./COUNTRY_GRP");
	public static final Path _BUDGETARY_ONLY = Path.parse("./BUDGETARY_ONLY");
	public static final Path _SYNCID = Path.parse("./SYNCID");
	public static final Path _SYNCDTTM = Path.parse("./SYNCDTTM");
	public static final Path _C_DSMT_DESCR25 = Path.parse("./C_DSMT_DESCR25");
	public static final Path _C_DSMT_DESCR120 = Path.parse("./C_DSMT_DESCR120");
	public static final Path _C_DSMT_GOC_USAGE = Path.parse("./C_DSMT_GOC_USAGE");
	public static final Path _C_DSMT_LOC_COST_CD = Path.parse("./C_DSMT_LOC_COST_CD");
	public static final Path _C_CLOSED = Path.parse("./C_CLOSED");
	public static final Path _C_DSMT_GOC_COMMENT = Path.parse("./C_DSMT_GOC_COMMENT");
	public static final Path _CREATED_OPERID = Path.parse("./CREATED_OPERID");
	public static final Path _ORIG_DTTM = Path.parse("./ORIG_DTTM");
	public static final Path _LASTUPDOPRID = Path.parse("./LASTUPDOPRID");
	public static final Path _CHNG_DTTM = Path.parse("./CHNG_DTTM");
	public static final Path _ENDDT = Path.parse("./ENDDT");
	public static final Path _PRT_COMMENT = Path.parse("./PRT_COMMENT");
	} 
	// ===============================================

}
