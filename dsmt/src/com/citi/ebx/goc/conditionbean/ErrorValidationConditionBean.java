package com.citi.ebx.goc.conditionbean;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.path.DSMT2Paths;
import com.citi.ebx.goc.path.DSMT2WFPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValidationReport;
import com.orchestranetworks.service.comparison.DifferenceBetweenInstances;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

public class ErrorValidationConditionBean extends ConditionBean {
	private String dataSpace;
	private String dataSet;
	private String allocatedUser;
	private String requestID;
	List<String> actionCode = new ArrayList<String>();
	private String table;
	private String tablePath;
	
	/**
	 * @return the actionCode
	 */
	public List<String> getActionCode() {
		return actionCode;
	}

	/**
	 * @param actionCode the actionCode to set
	 */
	public void setActionCode(List<String> actionCode) {
		this.actionCode = actionCode;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	@Override
	public boolean evaluateCondition(ConditionBeanContext context)
			throws OperationException {
		
		LOG.info("Evaluating Workflow Request for Request ID " + requestID);
		
		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
				HomeKey.forBranchName(dataSpace));
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(
				AdaptationName.forName(dataSet));
				
		final AdaptationHome initialSnapshot = dataSpaceRef.getParent();
		final Adaptation initialDataSet = initialSnapshot.findAdaptationOrNull(
				dataSetRef.getAdaptationName());
		
		Repository repo = context.getRepository();
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
				"DSMT_WF", "DSMT_WF");
		final AdaptationTable targetTable = metadataDataSet
				.getTable(getPath("/root/C_APP_WF_REPORTING"));
		
		boolean errorFound = false;
		
		int numberOfModification = 0, numberOfAddition = 0;
		
		final DifferenceBetweenInstances dataSetDiffs = DifferenceHelper.compareInstances(initialDataSet, dataSetRef, false);
		@SuppressWarnings("unchecked")
			
			final List<DifferenceBetweenTables> tableDiffsList = dataSetDiffs.getDeltaTables();
		

			final String predicate = DSMT2WFPaths._C_APP_WF_REPORTING._REQUEST_ID.format() + "= '" +requestID + "'";
		
			RequestResult rs = targetTable.createRequestResult(predicate);
			
			for (Adaptation record; (record = rs.nextAdaptation()) != null;){
				table = record.getString(DSMT2WFPaths._C_APP_WF_REPORTING._TABLE_NAME);
				tablePath = record.getString(DSMT2WFPaths._C_APP_WF_REPORTING._Table_Path);
			}

			if(null != tableDiffsList){
				numberOfModification = tableDiffsList.size();
			}
			final Iterator<DifferenceBetweenTables> tableDiffsIter = tableDiffsList.iterator();
			
				final DifferenceBetweenTables tableDiffs = tableDiffsIter.next();
				
				AdaptationTable tbl = tableDiffs.getRight();
				
				if(tbl.getTablePath().format().equalsIgnoreCase(tablePath)){
					errorFound = false;
				}else{
					final String message = "The workflow request " + requestID + " has validation error(s).Workflow request Table and updated tables are different.";
					DSMTNotificationService.notifyEmailID(allocatedUser, "DSMT2 Notification : Workflow Request ID - " + requestID , message, true);
					errorFound=true;
				}
				
				
				
				@SuppressWarnings("unchecked")
				final List<ExtraOccurrenceOnRight> adds = tableDiffs.getExtraOccurrencesOnRight();
				LOG.info("adds retrieved in ChangeSetContainsErrorsCondition for request ID " + requestID + " = " + adds);
				numberOfAddition =  adds.size();
				
				final Iterator<ExtraOccurrenceOnRight> addsIter = adds.iterator();
				while (! errorFound && addsIter.hasNext()) {
					final ExtraOccurrenceOnRight add = addsIter.next();
					final Adaptation record = add.getExtraOccurrence();
					final ValidationReport recordReport = record.getValidationReport(false, false);
					errorFound = recordReport.hasItemsOfSeverity(Severity.ERROR);
					Date endDt = record.getDate(DSMT2Paths._GLOBAL_STD_ENT_STD_F_BAL_TYPE_C_DSMT_ACC_CTGR._ENDDT);

					if(( endDt==null || endDt.toString().contains("9999")) &&  errorFound){
						final String message = "The workflow request " + requestID + " has validation error(s).";
						DSMTNotificationService.notifyEmailID(allocatedUser, DSMTUtils.getSubject(allocatedUser, requestID) , message, true);
						LOG.info("errorFound in additions for requestID  : "+ requestID + " : " + errorFound);
					}
					else{
						errorFound = false;
					}
					
			}
			
			@SuppressWarnings("unchecked")
			final List<DifferenceBetweenOccurrences> deltas = tableDiffs.getDeltaOccurrences();
			
			LOG.info("deltas retrieved in ChangeSetContainsErrorsCondition " + deltas.toString());
			final Iterator<DifferenceBetweenOccurrences> deltasIter = deltas.iterator();
			while (! errorFound && deltasIter.hasNext()) {
				final DifferenceBetweenOccurrences delta = deltasIter.next();
				final Adaptation record = delta.getOccurrenceOnRight();
				final ValidationReport recordReport = record.getValidationReport(false, false);
				Date endDt = record.getDate(DSMT2Paths._GLOBAL_STD_ENT_STD_F_BAL_TYPE_C_DSMT_ACC_CTGR._ENDDT);

				errorFound = recordReport.hasItemsOfSeverity(Severity.ERROR);
				if(( endDt==null || endDt.toString().contains("9999")) && errorFound){
					final String message = "The workflow request " + requestID + " has validation error(s).";
					DSMTNotificationService.notifyEmailID(allocatedUser, "DSMT2 Notification : Workflow Request ID - " + requestID , message, true);
					LOG.info("errorFound in modifications for requestID  : "+ requestID + " : " + errorFound);
					
				}else{
					errorFound = false;
				}

			}
			
			/* return true if errors are found either for modifications or additions */
			if (errorFound) {
				return  errorFound;
			}
		
		if(numberOfAddition + numberOfModification == 0){
			
			final String message = "Number of addition "+ numberOfAddition+ " and Number of modification "+numberOfModification + ". Task cannot be completed if there is no Change"; 
			errorFound=true;
			//DSMTNotificationService.notifyEmailID(allocatedUser, DSMTUtils.getSubject(allocatedUser, requestID) , message, true);
		}
	
		if(errorFound){
			LOG.info("Errors found for requestID " + requestID + " : " + errorFound);
		}
		return errorFound;
	}	
		
	
	public void setAllocatedUser(String allocatedUser) {
		this.allocatedUser = allocatedUser;
	}

	public String getAllocatedUser() {
		return allocatedUser;
	}


	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getRequestID() {
		return requestID;
	}


	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	private Path getPath(final String fieldName) {

		return Path.parse(fieldName);
	}
}
