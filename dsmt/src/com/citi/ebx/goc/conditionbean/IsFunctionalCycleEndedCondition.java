package com.citi.ebx.goc.conditionbean;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

public class IsFunctionalCycleEndedCondition  extends ConditionBean{

	
	private String dataSpace;
	private String dataSet;
	private String requestID;

	private static String allocatedUser = DSMTConstants.propertyHelper.getProp(DSMTConstants.WORKFLOW_ADMIN_DISTRIBUTION_LIST);
	
	
	public boolean evaluateCondition(ConditionBeanContext context)
			throws OperationException {

		LOG.info("IsFunctionalCycleEndedCondition.evaluateCondition started..");

		boolean isFunctionalDatePassed = false;

		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
				HomeKey.forBranchName(dataSpace));
		final Adaptation dataSetRef = dataSpaceRef
				.findAdaptationOrNull(AdaptationName.forName(dataSet));
		
		String function_endDate= functionalDateValidation(context.getRepository(),dataSetRef);
		
		if(!function_endDate.equals("true")){
			final String message = "Request ID "+ requestID +" for DataSpace ID " +  dataSpace	+ " can not be merged as functional maintenance cycle ended on "+ function_endDate + " The request needs to be approved before the functional cycle ends.";
			DSMTNotificationService.notifyEmailID(allocatedUser, DSMTUtils.getSubject(allocatedUser, requestID), message, false);
			LOG.error("Notification sent to DL : " + allocatedUser + " message -> " + message);
			isFunctionalDatePassed= true;	
			
		}
		

		return isFunctionalDatePassed;
	}

		protected String functionalDateValidation(Repository repo , Adaptation dataSetRef ){
			
			return GOCWorkflowUtils.checkFunctionEndDate(dataSetRef,repo);
				
		}
	
	

	
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
}
