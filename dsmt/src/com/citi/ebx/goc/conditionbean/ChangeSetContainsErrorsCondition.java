package com.citi.ebx.goc.conditionbean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValidationReport;
import com.orchestranetworks.service.comparison.DifferenceBetweenInstances;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

public class ChangeSetContainsErrorsCondition extends ConditionBean {
	private String dataSpace;
	private String dataSet;
	private String allocatedUser;
	private String requestID;
//	private String workflowStatus;
	
	
	
/*	public String getWorkflowStatus() {
		return workflowStatus;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}*/

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	@Override
	public boolean evaluateCondition(ConditionBeanContext context)
			throws OperationException {
		
		LOG.info("Evaluating Workflow Request for Request ID " + requestID);
		
		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
				HomeKey.forBranchName(dataSpace));
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(
				AdaptationName.forName(dataSet));
		int gocAddCount =0;
		int nonGOCAddCount =0;
		
		// Do a validation so report is up to date
	//	@SuppressWarnings("unused")
	//	final ValidationReport report = dataSetRef.getValidationReport();
		
		final AdaptationHome initialSnapshot = dataSpaceRef.getParent();
		final Adaptation initialDataSet = initialSnapshot.findAdaptationOrNull(
				dataSetRef.getAdaptationName());
		
		boolean errorFound = false;
		
		int numberOfModification = 0, numberOfAddition = 0;
		
		final DifferenceBetweenInstances dataSetDiffs = DifferenceHelper.compareInstances(initialDataSet, dataSetRef, false);
		@SuppressWarnings("unchecked")
			
			final List<DifferenceBetweenTables> tableDiffsList = dataSetDiffs.getDeltaTables();
			
			if(null != tableDiffsList){
				numberOfModification = tableDiffsList.size();
			}
			final Iterator<DifferenceBetweenTables> tableDiffsIter = tableDiffsList.iterator();
			while (! errorFound && tableDiffsIter.hasNext()) {
				final DifferenceBetweenTables tableDiffs = tableDiffsIter.next();
				
				@SuppressWarnings("unchecked")
				final List<ExtraOccurrenceOnRight> adds = tableDiffs.getExtraOccurrencesOnRight();
				Path  table = tableDiffs.getPathOnRight();
							if(table.format().contains("C_DSMT_DFN_GOC")){
								gocAddCount= adds.size();
									LOG.info("gocAddCount :  " +gocAddCount);
								
							}
							else{
								nonGOCAddCount= nonGOCAddCount+adds.size();
								LOG.info("nonGOCAddCount : " +nonGOCAddCount);
							}
			
				LOG.info("adds retrieved in ChangeSetContainsErrorsCondition for request ID " + requestID + " = " + adds);
				numberOfAddition =  adds.size();
				
				final Iterator<ExtraOccurrenceOnRight> addsIter = adds.iterator();
				while (! errorFound && addsIter.hasNext()) {
					final ExtraOccurrenceOnRight add = addsIter.next();
					final Adaptation record = add.getExtraOccurrence();
					final ValidationReport recordReport = record.getValidationReport(false, false);
					errorFound = recordReport.hasItemsOfSeverity(Severity.ERROR);
					
					if(errorFound){
						final String message = "The GOC workflow request " + requestID + " has validation error(s).";
						DSMTNotificationService.notifyEmailID(allocatedUser, DSMTUtils.getSubject(allocatedUser, requestID) , message, true);
						LOG.info("errorFound in additions for requestID  : "+ requestID + " : " + errorFound);
					}
			}
				//Code to find multiple grid routing in case of resubmit.
				/*String addRoutingId = null;
				if("Resubmitted".equalsIgnoreCase(workflowStatus))
				{
				LOG.info("inside Changesetcontainserrors for resubmitted for add");	
					List<String> gridRoutingID = new ArrayList<String>();
					for (int i = 0; i < adds.size(); i++) {
						final ExtraOccurrenceOnRight add = adds.get(i);
						final Adaptation record = add.getExtraOccurrence();
						if(addRoutingId == null){
							addRoutingId = record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup);
						}else{
							if(!addRoutingId.equalsIgnoreCase(record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup))){
								throw OperationException.createError("Multiple grid routing id not allowed ");
							}
						}
						
						
					}
				}*/
			
			@SuppressWarnings("unchecked")
			final List<DifferenceBetweenOccurrences> deltas = tableDiffs.getDeltaOccurrences();
			
			LOG.info("deltas retrieved in ChangeSetContainsErrorsCondition " + deltas.toString());
			final Iterator<DifferenceBetweenOccurrences> deltasIter = deltas.iterator();
			while (! errorFound && deltasIter.hasNext()) {
				final DifferenceBetweenOccurrences delta = deltasIter.next();
				final Adaptation record = delta.getOccurrenceOnRight();
				final ValidationReport recordReport = record.getValidationReport(false, false);
				errorFound = recordReport.hasItemsOfSeverity(Severity.ERROR);
				if(errorFound){
					final String message = "The GOC workflow request " + requestID + " has validation error(s).";
					DSMTNotificationService.notifyEmailID(allocatedUser, DSMTUtils.getSubject(allocatedUser, requestID) , message, true);
					LOG.info("errorFound in modifications for requestID  : "+ requestID + " : " + errorFound);
					
				}
			}
			/* return true if errors are found either for modifications or additions */
			if (errorFound) {
				return  errorFound;
			}
			
			//Code to find multiple grid routing in case of resubmit.
			/*String deltaRoutingId = null;
			if("Resubmitted".equalsIgnoreCase(workflowStatus))
			{
				LOG.info("inside Changesetcontainserrors for resubmitted for delta");
				List<String> gridRoutingID = new ArrayList<String>();
				for (int i = 0; i < deltas.size(); i++) {
					final DifferenceBetweenOccurrences delta = deltas.get(i);
					final Adaptation record = delta.getOccurrenceOnRight();
					if(deltaRoutingId == null){
						deltaRoutingId = record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup);
					}else{
						if(!deltaRoutingId.equalsIgnoreCase(record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup))){
							throw OperationException.createError("Multiple grib routing id not allowed ");
						}
					}
				}
			}
			
			if(addRoutingId != null && deltaRoutingId != null && !(addRoutingId.equalsIgnoreCase(deltaRoutingId))){
				throw OperationException.createError("Multiple grib routing id not allowed ");
			}*/
		}
	
		// method to find the new GOC is inserted in all the partner systems
		/** Check for GOC Primary Key use in Partner system tables if additions are done to the GOC table 
		if(numberOfAddition > 0){
				errorFound = getPartnerSysData(tableDiffsList , context.getRepository() , dataSetRef.getString(GOCPaths._CurrentSID));
		}
		// diabled the validation to check for 1:1:1:1 mapping of new additions.
		
		*/
		
		/** Task should not be submitted for approval if there is no change. */
		if(numberOfAddition + numberOfModification == 0){
			
			final String message = "Number of addition "+ numberOfAddition+ " and Number of modification "+numberOfModification + ". Task cannot be completed if there is no Change"; 
			errorFound=true;
			DSMTNotificationService.notifyEmailID(allocatedUser, DSMTUtils.getSubject(allocatedUser, requestID) , message, true);
		}
	
		if(errorFound){
			LOG.info("Errors found for requestID " + requestID + " : " + errorFound);
		}
		return errorFound;
	}

	
	@SuppressWarnings("unchecked")
	protected boolean getPartnerSysData(List<DifferenceBetweenTables> tableDiffsList ,Repository repo,String sid ) {
		
		LOG.info("Now Checking Partner System data whether all new GOC Primary keys are used or not.. ");
		Map<String, List<ExtraOccurrenceOnRight>> addMapNonGOC=  new HashMap<String, List<ExtraOccurrenceOnRight>>();
		final Iterator<DifferenceBetweenTables> tableDiffsIter = tableDiffsList.iterator();
		List<ExtraOccurrenceOnRight> addsGOC =null;
		List<String> gocPKList =new ArrayList<String>() ;
		List<ExtraOccurrenceOnRight> addsNonGOC = null;
		List<String> partSysTables = getPartnerSysTables(repo ,sid );
		if(partSysTables.size()==0 &&partSysTables.isEmpty())
		{
			return false;
		}
		else{
		
		while ( tableDiffsIter.hasNext()) {
			final DifferenceBetweenTables tableDiffs = tableDiffsIter.next();
			
			final Path  table = tableDiffs.getPathOnRight();
						if(table.format().contains("C_DSMT_DFN_GOC")){
							//nonGOCAdd
							addsGOC =tableDiffs.getExtraOccurrencesOnRight();
						}
						else{
							//nonGOCAdds
							
							addMapNonGOC.put(table.format(), tableDiffs.getExtraOccurrencesOnRight());
						}			
      		}
		
			for(ExtraOccurrenceOnRight extraOccuregGOC:addsGOC){
				final Adaptation record = extraOccuregGOC.getExtraOccurrence();
				String gocPK =""+record.get_int(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK);
				if(null!=gocPK){
					gocPKList.add(gocPK);
				}
			}
			
			LOG.info("GOC PK List = " + gocPKList);
			
			for(String partnerSysTab :partSysTables){
				List<String> tempGOCPKlist = new ArrayList<String>();
							 tempGOCPKlist.addAll(gocPKList);
						if(addMapNonGOC.containsKey(partnerSysTab)){
							addsNonGOC= addMapNonGOC.get(partnerSysTab);
								for(ExtraOccurrenceOnRight extraOccureNonGOC:addsNonGOC){
									final Adaptation record = extraOccureNonGOC.getExtraOccurrence();
									String gocFKPatner =record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS._C_GOC_FK);
											if(tempGOCPKlist.contains(gocFKPatner)){
												tempGOCPKlist.remove(gocFKPatner);
											}else{
												
												final String message = "GOC Primary key " + gocFKPatner + "found in " + partnerSysTab + " is not found in the Define GOC table";
												DSMTNotificationService.notifyEmailID(allocatedUser, DSMTUtils.getSubject(allocatedUser, requestID), message, true);
												return true;
											}
								}
								
								if(!tempGOCPKlist.isEmpty()){
									final String message = "Please add corresponding entries for the following GOCs "+ tempGOCPKlist + " in Partner System tables" ;
									DSMTNotificationService.notifyEmailID(allocatedUser, DSMTUtils.getSubject(allocatedUser, requestID), message, true);
									
									return true;
								}
						}
						else{
							final String message = "There is no change in Partner system table : " + partnerSysTab + ". Please use the GOC Primary key list " +  gocPKList + " as foreign keys in other tables.";
							DSMTNotificationService.notifyEmailID(allocatedUser, DSMTUtils.getSubject(allocatedUser, requestID), message, true);
							return true;
						}
			}
			
		
		return false;
		}
	}

	
	
	public static List<String> getPartnerSysTables(Repository repo, String sid)	{
		final Adaptation metadataDataSet ;
		final List<String> partSysTables = new ArrayList<String>() ;
		    try {
				metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(repo);
				final AdaptationTable ccTable =	metadataDataSet.getTable(
						GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
				final String predicate = GOCWFPaths._C_DSMT_SID_ENH._SID.format()
						+ "=\"" + sid + "\"";
				final RequestResult reqRes = ccTable.createRequestResult(predicate);
				final Adaptation ccRecord;
				try {
					ccRecord = reqRes.nextAdaptation();
				} finally {
					reqRes.close();
				}
				if (ccRecord == null) {
					LOG.error("No record found for predicate " + predicate
							+ " in table " + ccTable.getTablePath().format());
					return null;
				}
				if(null!= ccRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._GLTable)){
				partSysTables.add(ccRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._GLTable));
				}
				if(null!= ccRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._RptEngineTable)){
					partSysTables.add(ccRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._RptEngineTable));
					}
				if(null!= ccRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._P2PTable)){
					partSysTables.add(ccRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._P2PTable));
					}
				
			
		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);
		
		}
		return partSysTables;
		
}
	
	
	
	public void setAllocatedUser(String allocatedUser) {
		this.allocatedUser = allocatedUser;
	}

	public String getAllocatedUser() {
		return allocatedUser;
	}


	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getRequestID() {
		return requestID;
	}


	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
}
