package com.citi.ebx.goc.conditionbean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.comparison.DifferenceBetweenInstances;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

public class CheckForDuplicateGOCsCondition extends ConditionBean {

	
	private static final String TRAILER_MESSAGE = ". Task has been approved but cannot be completed.";
	private String dataSpace;
	private String dataSet;
	private String requestID;

	private boolean writeErrorToContext = true;
	private String gridRoutingID;
	private String approverList;
	private String errorMessage;
	private String errorDetails;

	private String allocatedUser = DSMTConstants.propertyHelper.getProp(DSMTConstants.WORKFLOW_ADMIN_DISTRIBUTION_LIST);

	private String distributionList;
	
	public boolean isWriteErrorToContext() {
		return writeErrorToContext;
	}

	public void setWriteErrorToContext(boolean writeErrorToContext) {
		this.writeErrorToContext = writeErrorToContext;
	}

	public String getGridRoutingID() {
		return gridRoutingID;
	}

	public void setGridRoutingID(String gridRoutingID) {
		this.gridRoutingID = gridRoutingID;
	}

	public String getApproverList() {
		return approverList;
	}

	public void setApproverList(String approverList) {
		this.approverList = approverList;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	public  String getAllocatedUser() {
		return allocatedUser;
	}

	public void setAllocatedUser(String allocatedUser) {
		this.allocatedUser = allocatedUser;
	}

	@SuppressWarnings("unchecked")
	public boolean evaluateCondition(ConditionBeanContext context)
			throws OperationException {

		LOG.info("CheckForDuplicateGOCsCondition.evaluateCondition started..");

		boolean duplicateFound = true;

		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
				HomeKey.forBranchName(dataSpace));
		
		final Adaptation dataSetRef = dataSpaceRef
				.findAdaptationOrNull(AdaptationName.forName(dataSet));
		// get the parent GOC dataSpace
		final AdaptationHome initialSnapshot = dataSpaceRef.getParent();
		final Adaptation initialDataSet = initialSnapshot
				.findAdaptationOrNull(dataSetRef.getAdaptationName());

		final DifferenceBetweenInstances dataSetDiffs = DifferenceHelper
				.compareInstances(initialDataSet, dataSetRef, false);
		final List<DifferenceBetweenTables> tableDiffsList = dataSetDiffs
				.getDeltaTables();
		List<ExtraOccurrenceOnRight> addsGOC = new ArrayList<ExtraOccurrenceOnRight>();
		List<String> gocPKwithBlankLCC = new ArrayList<String>();
		List<String> gocWithMismatchedLCCSID = new ArrayList<String>();

		final Iterator<DifferenceBetweenTables> tableDiffsIter = tableDiffsList
				.iterator();
		while (tableDiffsIter.hasNext()) {
			final DifferenceBetweenTables tableDiffs = tableDiffsIter.next();
			final Path table = tableDiffs.getPathOnRight();
			if (table.format().contains("C_DSMT_DFN_GOC")) {
				// GOCAdds
				addsGOC = tableDiffs.getExtraOccurrencesOnRight();
			}
		}

		if (!addsGOC.isEmpty() &&  null!= addsGOC) {
			
			gocPKwithBlankLCC = valiadteLCC(addsGOC);
			
			if (!gocPKwithBlankLCC.isEmpty() && gocPKwithBlankLCC != null) {
				duplicateFound = false;

				distributionList = DSMTConstants.propertyHelper.getProp(DSMTConstants.WORKFLOW_ADMIN_DISTRIBUTION_LIST);
				final String message = "GOC PK  " + gocPKwithBlankLCC
						+ " have blank or empty Local Cost Code in dataspace "
						+ dataSpace + TRAILER_MESSAGE;
				DSMTNotificationService.notifyEmailID(distributionList, DSMTUtils.getSubject(allocatedUser, requestID), message, true);
				LOG.error("LCC blank found : " + message);

			}
			//am05884 added code for CR 481 �GOC ID validation for SID+LCC to be manually corrected when merging WF request�  
			gocWithMismatchedLCCSID=valiadteSIDLCC(addsGOC);
			if (!gocWithMismatchedLCCSID.isEmpty() && gocWithMismatchedLCCSID != null) {
				duplicateFound = false;
				distributionList = DSMTConstants.propertyHelper.getProp(DSMTConstants.WORKFLOW_ADMIN_DISTRIBUTION_LIST);
				final String message = "GOC  " + gocWithMismatchedLCCSID
						+ " has Mismatch from SID and Local Cost Code in dataspace "
						+ dataSpace + TRAILER_MESSAGE;
				DSMTNotificationService.notifyEmailID(distributionList, DSMTUtils.getSubject(allocatedUser, requestID), message, true);
				LOG.error("GOC Mismatch with SID and LCC Combination : " + message);

			}

			final Map<String, String> duplicateGOCMap = validateDuplicateGOC(
					context.getRepository(), addsGOC);
			if (!duplicateGOCMap.isEmpty() && duplicateGOCMap != null) {
				
				distributionList = DSMTConstants.propertyHelper.getProp(DSMTConstants.WORKFLOW_ADMIN_DISTRIBUTION_LIST);
				duplicateFound = false;
				final StringBuffer errorMessage = new StringBuffer();
				final List<String> duplicateGOCList = new ArrayList<String>(
						duplicateGOCMap.keySet());
				for (final String goc : duplicateGOCList) {
					errorMessage.append("GOC code " + goc
							+ " is duplicate in Goc PK "
							+ duplicateGOCMap.get(goc)
							+ DSMTConstants.NEXT_LINE);
				}
				errorMessage.append("DataSpace ID = " + dataSpace
						+ TRAILER_MESSAGE);
				DSMTNotificationService.notifyEmailID(distributionList,	DSMTUtils.getSubject(allocatedUser, requestID),	String.valueOf(errorMessage), true);
				LOG.error("GOC duplicate found : " + errorMessage.toString());

			}
		}

		return duplicateFound;
	}

	protected List<String> valiadteLCC(List<ExtraOccurrenceOnRight> addsGOC) {

		List<String> gocPKwithBlankLCCtemp = new ArrayList<String>();

		for (ExtraOccurrenceOnRight extraOccuregGOC : addsGOC) {
			final Adaptation record = extraOccuregGOC.getExtraOccurrence();
			String lCC = record
					.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD);
			if (lCC == null) {
				gocPKwithBlankLCCtemp
						.add(Integer.toString(record
								.get_int(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK)));
			}

		}
		LOG.info("GOC pk for blank LCC: " + gocPKwithBlankLCCtemp);

		return gocPKwithBlankLCCtemp;

	}

	//am05884
	protected List<String> valiadteSIDLCC(List<ExtraOccurrenceOnRight> addsGOC) {

		List<String> gocMismatchWithLCCSID = new ArrayList<String>();

		for (ExtraOccurrenceOnRight extraOccuregGOC : addsGOC) {
			final Adaptation record = extraOccuregGOC.getExtraOccurrence();
			String lCC = record
					.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD);
			
			final Adaptation sidRecord = Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
			String sid = sidRecord
					.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
			
			
			String goc = record
					.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC);
			
			if (!((sid+lCC).equalsIgnoreCase(goc))) {
				gocMismatchWithLCCSID
						.add(Integer.toString(record
								.get_int(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK)));
			}

		}
		LOG.info("GOC for Mismatched LCC and SID Combination: " + gocMismatchWithLCCSID);

		return gocMismatchWithLCCSID;

	}

	protected Map<String, String> validateDuplicateGOC(Repository repo,
			List<ExtraOccurrenceOnRight> addsGOC) {

		Map<String, String> duplicateGOC = new HashMap<String, String>();

		Map<String, String> duplicateGOCT = new HashMap<String, String>();
		List<String> gocTempList = new ArrayList<String>();

		for (ExtraOccurrenceOnRight extraOccuregGOC : addsGOC) {
			final Adaptation record = extraOccuregGOC.getExtraOccurrence();
			String goc = record
					.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC);
			String gocPK = Integer
					.toString(record
							.get_int(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK));

			if (gocTempList.contains(goc)) {
				if (duplicateGOC.containsKey(goc)) {
					String gocid = duplicateGOC.get(goc);
					gocid = gocid + DSMTConstants.COMMA + gocPK;
					duplicateGOC.put(goc, gocid);

				} else {

					String gocid = duplicateGOCT.get(goc);
					gocid = gocid + DSMTConstants.COMMA + gocPK;
					duplicateGOC.put(goc, gocid);
				}
			}
			gocTempList.add(goc);
			duplicateGOCT.put(goc, gocPK);

		}

		duplicateGOC = validateDuplicateGOCMaster(repo, duplicateGOCT,
				duplicateGOC);

		LOG.info("GOC Code : " + duplicateGOC.keySet() + " is duplicate for "
				+ duplicateGOC.entrySet() + "in dataSpace " + dataSpace);
		return duplicateGOC;

	}

	protected Map<String, String> validateDuplicateGOCMaster(Repository repo,
			Map<String, String> gocTempList,
			Map<String, String> duplicateGOCTemp) {

		Map<String, String> duplicateGOC = duplicateGOCTemp;

		for (String gocTemp : gocTempList.keySet()) {

			Adaptation record = getGocData(repo, gocTemp);
			if (record != null) {
				String gocMaster = record
						.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC);
				if (duplicateGOC.containsKey(gocMaster)) {
					String gocid = duplicateGOC.get(gocMaster);
					gocid = gocid
							+ DSMTConstants.COMMA
							+ Integer
									.toString(record
											.get_int(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK));
					duplicateGOC.put(gocMaster, gocid);

				} else {

					String gocid = Integer
							.toString(record
									.get_int(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK));
					gocid = gocid + DSMTConstants.COMMA + gocTempList.get(gocMaster);
					duplicateGOC.put(gocMaster, gocid);

				}
			}
		}
		return duplicateGOC;

	}

	public static Adaptation getGocData(Repository repo, String goc) {
		final Adaptation metadataDataSet;
		Adaptation ccRecord = null;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(repo, "GOC", "GOC");
			final AdaptationTable ccTable = metadataDataSet
					.getTable(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC
							.getPathInSchema());
			final String predicate = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC
					.format() + "=\"" + goc + "\"";
			final RequestResult reqRes = ccTable.createRequestResult(predicate);

			try {
				ccRecord = reqRes.nextAdaptation();
			} finally {
				reqRes.close();
			}
			if (ccRecord == null) {

				return null;
			}

		} catch (OperationException ex) {
			LOG.error("Error looking up metadata data set", ex);

		}
		return ccRecord;

	}

	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

}
