package com.citi.ebx.goc.service;

import java.io.BufferedWriter;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.service.LaunchGOCRequestChangesWorkflowServlet;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.ServiceContext;

public class IGWGOCRequestChangesForm extends HttpServlet {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	private static final String DEFAULT_SERVLET = "/IGWGOCRequestChanges";
	
	private static final long serialVersionUID = 1L;
	
	protected BufferedWriter out;
	
	private String servlet = DEFAULT_SERVLET;
	
	/**
	 * Get the string identifying the servlet to invoke upon submit. This needs to match the name configured
	 * in the application server config (i.e. server.xml). For example, "/GOCRequestChanges".
	 * 
	 * @return the servlet
	 */
	public String getServlet() {
		return servlet;
	}

	/**
	 * Set the string identifying the servlet to invoke upon submit. This needs to match the name configured
	 * in the application server config (i.e. server.xml). For example, "/GOCRequestChanges".
	 * 
	 * @param servlet the servlet
	 */
	public void setServlet(String servlet) {
		this.servlet = servlet;
	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		out = new BufferedWriter(res.getWriter());
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		writeForm(sContext);
		out.flush();
	}
	
	private void writeForm(final ServiceContext sContext) throws IOException {
	
		writeln("<form id=\"form\" action=\"" + sContext.getURLForIncludingResource(servlet)
				+ "\" method=\"post\" autocomplete=\"off\">");
		writeln("  <table>");
		writeln("    <tbody>");
		
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + LaunchGOCRequestChangesWorkflowServlet.REQUEST_PARAM_SID
				+ "\">SID</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input type=\"text\" id=\"" + LaunchGOCRequestChangesWorkflowServlet.REQUEST_PARAM_SID
				+ "\" name=\"" + LaunchGOCRequestChangesWorkflowServlet.REQUEST_PARAM_SID + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		
		writeln("    </tbody>");
		writeln("  </table>");
		
		writeln("  <button type=\"submit\" class=\"ebx_Button\">Submit</button>");
		
		writeln("</form>");
	}

	protected void write(final String str) throws IOException {
		out.write(str);
	}
	
	protected void writeln(final String str) throws IOException {
		write(str);
		out.newLine();
	}
}
