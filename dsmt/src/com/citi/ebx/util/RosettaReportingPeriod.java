package com.citi.ebx.util;

import java.util.Date;

public class RosettaReportingPeriod {

	
	String reportingTable;
	Date reprtingDate;
	Date reportingCOBDate;
	
	public String getReportingTable() {
		return reportingTable;
	}
	public void setReportingTable(String reportingTable) {
		this.reportingTable = reportingTable;
	}
	public Date getReprtingDate() {
		return reprtingDate;
	}
	public void setReprtingDate(Date reprtingDate) {
		this.reprtingDate = reprtingDate;
	}
	public Date getReportingCOBDate() {
		return reportingCOBDate;
	}
	public void setReportingCOBDate(Date reportingCOBDate) {
		this.reportingCOBDate = reportingCOBDate;
	}
	
	
	
}
