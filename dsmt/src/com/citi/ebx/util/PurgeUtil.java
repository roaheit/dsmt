package com.citi.ebx.util;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class PurgeUtil implements Procedure {
	Adaptation record ;
	Repository repo; 
	public String tablePath ;
	public String parentFk;
	String columnId ;
	
	
	public PurgeUtil(Adaptation record,
			Repository repo,String tablePath,String parentFk,String columnId ) {
		this.repo = repo;
		this.record = record;
		this.tablePath=tablePath;
		this.parentFk=parentFk;
		this.columnId=  columnId ;
	}
	
	public void execute(ProcedureContext pContext) throws Exception {
		
		
		//AdaptationTable adTable = record.getContainerTable();
		//Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,DSMTConstants.ARCHIVE_DATA_SPACE_NAME, DSMTConstants.ARCHIVE_DATA_SET_NAME);
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,DSMTConstants.GOC, DSMTConstants.GOC);
		AdaptationTable targetTable = metadataDataSet.getTable(getPath(tablePath));
		String fkValue =null;
		pContext.setAllPrivileges(true);
		
		
		String keyValue1 = record.getString(getPath(columnId));
		final PrimaryKey pk = targetTable.computePrimaryKey(new String[] { keyValue1});
		Adaptation targetRecord = targetTable.lookupAdaptationByPrimaryKey(pk);
		final ValueContextForUpdate valueContext ;
		if (targetRecord == null) {
			valueContext = pContext.getContextForNewOccurrence(record,targetTable);
			valueContext.setValue(keyValue1, getPath(columnId));
		} else {
			valueContext = pContext.getContextForNewOccurrence(record,targetTable);
		}
		try{
			if(null!=parentFk){
				
				if(parentFk.contains("FK")||(parentFk.contains("LVID_PARENT")))
				{	
					String oldFkValue =record.getString(getPath(parentFk));
					
					if(oldFkValue!=null){
						
						fkValue =oldFkValue.substring(oldFkValue.indexOf("|")+1,oldFkValue.lastIndexOf("|"));
						
						}
				}
				valueContext.setValue(fkValue,getPath(parentFk));
				
			}
			valueContext.setValue(record.getOccurrencePrimaryKey().format(),getPath("/HISTORY_FK"));
		
		if (targetRecord == null) {
			targetRecord = pContext.doCreateOccurrence(valueContext,  targetTable);
		} else {
			targetRecord = pContext.doModifyContent(targetRecord,  valueContext);
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		pContext.setTriggerActivation(true);
		pContext.setAllPrivileges(false);
		
		
		
	}
	
private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}


}
