package com.citi.ebx.util;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class HARPTableDuplication implements Procedure {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	Adaptation record ;
	Repository repo; 
	public String tablePath ;
	public String parentFk;
	String columnId ;
	public String dataSpace;
	public String dataSet;
	
	public HARPTableDuplication(Adaptation record,
			Repository repo,String tablePath,String columnId, String dataSpace, String dataSet ,String parentFk) {
		this.repo = repo;
		this.record = record;
		this.tablePath=tablePath;
		this.columnId=  columnId ;
		this.dataSpace=  dataSpace ;
		this.dataSet=  dataSet ;
		this.parentFk=parentFk;
	}

	public void execute(ProcedureContext pContext) throws Exception {
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,dataSpace,dataSet);
		AdaptationTable targetTable = metadataDataSet.getTable(getPath(tablePath));
		String fkValue =null;
		pContext.setAllPrivileges(true);
		String keyValue1 = record.getString(getPath(columnId));
		final PrimaryKey pk = targetTable.computePrimaryKey(new String[] { keyValue1});
		Adaptation targetRecord = targetTable.lookupAdaptationByPrimaryKey(pk);
		final ValueContextForUpdate valueContext ;
		if (targetRecord == null) {
			valueContext = pContext.getContextForNewOccurrence(record,targetTable);
			valueContext.setValue(keyValue1, getPath(columnId));
		} else {
			valueContext = pContext.getContextForNewOccurrence(record,targetTable);
		}
		try{
			if(null!=parentFk){
				if(parentFk.contains("FK")||(parentFk.contains("LVID_PARENT")))
				{	
					String oldFkValue =record.getString(getPath(parentFk));
					
					if(oldFkValue!=null){
						
						fkValue =oldFkValue.substring(oldFkValue.indexOf("|")+1,oldFkValue.lastIndexOf("|"));
						}
				}
				valueContext.setValue(fkValue,getPath(parentFk));
			}
			SchemaNode node = targetTable.getTableNode();
            if (node.getNode(getPath("/HISTORY_FK")) != null){
                    valueContext.setValue(record.getOccurrencePrimaryKey().format(),getPath("/HISTORY_FK")); 

            }
		if (targetRecord == null) {
			targetRecord = pContext.doCreateOccurrence(valueContext,  targetTable);
		} else {
			targetRecord = pContext.doModifyContent(targetRecord,  valueContext);
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		pContext.setTriggerActivation(true);
		pContext.setAllPrivileges(false);		
	}
	
private Path getPath(final String fieldName) {	
		return Path.parse(fieldName);
	}
}
