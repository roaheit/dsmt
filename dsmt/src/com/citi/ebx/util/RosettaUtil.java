/**
 * 
 */
package com.citi.ebx.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.citi.ebx.goc.path.RosettaPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

/**
 * @author rk00242
 *
 */
public class RosettaUtil   {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	public Path Eff_RPT_Date = Path.parse("/root/Rosetta_Hierarchies/ReportingPeriod");
	public Path _EFF_DATE = Path.parse("./RptPrd");
	public Path _REPORT_DATE = Path.parse("./RptCobDate");
	private Date effectDate;
	private Date rptDate;
	private Path _REPORT_TYPE=  Path.parse("./RptType");
	private String reportType;
	private String dataSpace="Rosetta";
	private String dataSet= "Rosetta";
	
	List<RosettaReportingPeriod> reportPeriod = new ArrayList<RosettaReportingPeriod>();
	

		public List<RosettaReportingPeriod> getDateValue(Repository repo){

	try {
		final AdaptationHome metadataDataSpace = repo.lookupHome(
				HomeKey.forBranchName(dataSpace));
		final Adaptation metadataDataSet = metadataDataSpace.findAdaptationOrNull(
				AdaptationName.forName(dataSet));
		final AdaptationTable dateTable = metadataDataSet.getTable(
				Eff_RPT_Date);
		
		//String pred1= "date-equal(" + RosettaPaths.Eff_RPT_Dates._EFF_DATE.format()

                //+ ",'2015-10-28')";


		
		RequestResult rs = dateTable.createRequestResult(null);
		for(Adaptation record; (record = rs.nextAdaptation()) != null; ){
			
			/*Adaptation record = rs.nextAdaptation();
			effectDate = record.getDate(_EFF_DATE);
			rptDate = record.getDate(_REPORT_DATE); 
			reportType= record.getString(_REPORT_TYPE);
			*/
			
			
			RosettaReportingPeriod rptPrd = new RosettaReportingPeriod();
			rptPrd.setReportingTable(record.getString(_REPORT_TYPE));
			rptPrd.setReprtingDate(record.getDate(_EFF_DATE));
			rptPrd.setReportingCOBDate(record.getDate(_REPORT_DATE));
			System.out.println("TYPE == "+ record.getString(_REPORT_TYPE) + " reporting date = " +record.getDate(_EFF_DATE));
			reportPeriod.add(rptPrd);
		}


		
		}
			catch(Exception e){
			LOG.info("Exception occured while fetching date from tabel " + e.getMessage());
			}
	
		return reportPeriod;
		}
	/**
	 * @return the rptDate
	 */
	public Date getRptDate() {
		return rptDate;
	}

	/**
	 * @param rptDate the rptDate to set
	 */
	public void setRptDate(Date rptDate) {
		this.rptDate = rptDate;
	}


	/**
	 * @return the effectDate
	 */
	public Date getEffectDate() {
		return effectDate;
	}


	/**
	 * @param effectDate the effectDate to set
	 */
	public void setEffectDate(Date effectDate) {
		this.effectDate = effectDate;
	}


	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}


	/**
	 * @param reportType the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}


	
	
	

}


