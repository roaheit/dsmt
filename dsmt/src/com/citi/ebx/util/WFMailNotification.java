package com.citi.ebx.util;

import java.util.HashMap;

import com.citi.ebx.dsmt.util.ApprovalMonitoringTableBean;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.goc.path.DSMT2WFPaths;
import com.onwbp.adaptation.Adaptation;

public class WFMailNotification {
	
	static String subject;
	static String message;
	
	public static void sendMail (char status,Adaptation record,ApprovalMonitoringTableBean bean){
		
		HashMap<String, String> map = new HashMap<String, String>();

		String requestor = record.getString(DSMT2WFPaths._C_APP_WF_REPORTING._REQ_BY);
		String requestID = String.valueOf(bean.getRequestID());
		String tableName = record.getString(DSMT2WFPaths._C_APP_WF_REPORTING._TABLE_NAME);
				
		switch(status){
			
			case 'R':
				subject = "(Simplified DSMT2 Workflow) Workflow Request Rejected.";
				message = "Workflow ID " +requestID	+ " was rejected by "+bean.getAllocatedApprover();
				break;
			
			case 'P':
				subject="(Simplified DSMT2 Workflow) Workflow Request ID "+requestID+ " is Pending for Merge.";
				message="Workflow ID "+requestID+" for "+tableName +" is Approved"
					+ " and Pending for Merge.";
				break;
			
			case 'C':
				subject="(Simplified DSMT2 Workflow) Workflow Request ID "+requestID+ " is Completed.";
				message="Workflow ID "+requestID+" for "+tableName +" is Approved"
					+ " and changed data is merged.";
				break;
			
		}		
		
		map.put("TableName", tableName);
		map.put("RequestID", requestID);
		map.put("Requestor", requestor);
		
		DSMTNotificationService.notifyEmailID(requestor,
				subject, message, false,map);
	}

}
