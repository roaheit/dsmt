package com.citi.ebx.util;

import java.util.ArrayList;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class SaveUserData implements Procedure{
	Adaptation record ;
	Repository repo; 
	public String tablePath ;
	String columnId ;
	
	
	public SaveUserData(Adaptation record,
			Repository repo,String tablePath,String columnId ) {
		this.repo = repo;
		this.record = record;
		this.tablePath=tablePath;
		this.columnId=  columnId ;
	}
	
	public void execute(ProcedureContext pContext) throws Exception {
		
		
		pContext.setAllPrivileges(true);
		
		
		boolean value =false;
		
		//System.out.println("test :::: ");
		Adaptation metadataDataSetDirecory = EBXUtils.getMetadataDataSet(repo,"ebx-directory", "ebx-directory");
		AdaptationTable userTable = metadataDataSetDirecory.getTable(getPath(tablePath));
		final ValueContextForUpdate valueContextUser   ;
		String keyValue1 = "VIK123";
		final PrimaryKey pk = userTable.computePrimaryKey(new String[] { keyValue1});
		Adaptation targetRecord = userTable.lookupAdaptationByPrimaryKey(pk);
		
		
		
		
		if (targetRecord == null) {
			valueContextUser = pContext.getContextForNewOccurrence(userTable);
			valueContextUser.setValue(keyValue1, getPath(columnId));
			//System.out.println("test null :::: ");
		} else {
			valueContextUser = pContext.getContextForNewOccurrence(targetRecord,userTable);
			//System.out.println("test not null :::: ");
		}
		try{
			
			ArrayList<String> role = new ArrayList<String>();
			role.add("test");
			//System.out.println("test :::: ");
			valueContextUser.setValue("testVIK", getPath("/lastName"));
			valueContextUser.setValue("test", getPath("/firstName"));
			valueContextUser.setValue(value, getPath("/builtInRoles/administrator"));
			valueContextUser.setValue(value, getPath("/builtInRoles/readOnly"));
			valueContextUser.setValue(role, getPath("/specificRoles"));
			
		
		if (targetRecord == null) {
			targetRecord = pContext.doCreateOccurrence(valueContextUser,  userTable);
		} else {
			targetRecord = pContext.doModifyContent(targetRecord,  valueContextUser);
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		pContext.setTriggerActivation(true);
		pContext.setAllPrivileges(false);
		
		
		
	}
	
private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}


}
