package com.citi.ebx.util;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class TableRecordPrcodeureUtil implements Procedure {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	ValueContextForUpdate  vcTarget;
	AdaptationTable table;
	Adaptation record;
	String operation;
	public TableRecordPrcodeureUtil(ValueContextForUpdate vcTarget,AdaptationTable table,Adaptation record,String operation){
		this.vcTarget = vcTarget;
		this.table = table;
		this.record = record;
		this.operation = operation;
	}
	public void execute(ProcedureContext pContext) throws OperationException {
		LOG.info("TableRecordPrcodeureUtil operation="+operation);
		try {
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			if(operation.equals("create"))
				pContext.doCreateOccurrence(vcTarget, table);
			else if(operation.equals("update")) 
				pContext.doModifyContent(record, vcTarget);
			else if(operation.equals("delete"))
				pContext.doDelete(record.getAdaptationName(), false);
			pContext.setTriggerActivation(true);
			pContext.setAllPrivileges(false);
		}
		catch(Exception e) {
			LOG.error("Exception from TableRecordPrcodeureUtil:::"+e);e.printStackTrace();
		}
	}
}
