package com.citi.ebx.util;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;
import com.orchestranetworks.ui.UIButtonLayout;
import com.orchestranetworks.ui.UIButtonSpecJSAction;
import com.orchestranetworks.ui.UIComponentWriter;
import com.orchestranetworks.ui.UIHttpManagerComponent;

public class PurgeArchival {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private String effectiveStatusField = "./EFF_STATUS";
	private String nodeFlag = "./nodeFlag";

	private final String TABLE_STYLE = "border:1px solid lightgrey;border-collapse:collapse;";

	private final String DIV_STYLE = "padding-top:20px; padding-left:20px;height:90%;";

	private final String OCCURRENCE_STYLE = "padding:5px;" + "height:20px;"
			+ "border:1px solid lightgrey;" + "text-align:left;"
			+ "vertical-align:center;" + "white-space:nowrap;"
			+ "cursor:pointer;";
	
	private boolean purgeRec = false;
	private String dataSet;

	public PurgeArchival(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException,
			ServletException, OperationException, Exception {

		final ServiceContext sContext = ServiceContext
				.getServiceContext(request);
		List<Adaptation> errRecords = new ArrayList<Adaptation>();
		Repository repo = sContext.getCurrentHome().getRepository();

		Map<String, String> tableMap = new HashMap<String, String>();
		Map<Adaptation, String> errMessage = new HashMap<Adaptation, String>();

		tableMap.put("C_DSMT_MAN_GEO_NEW", "./C_DSMT_MAN_GEO_ID");
		tableMap.put("C_DSMT_MAN_SEG_NEW", "./C_DSMT_MAN_SEG_ID");
		tableMap.put("C_DSMT_SID", "./C_DSMT_SID");
		tableMap.put("C_DSMT_LVID_TBL", "./C_DSMT_LVID");
		tableMap.put("C_DSMT_FRS_BU", "./C_DSMT_FRS_BU");
		tableMap.put("C_DSMT_FRS_OU", "./C_DSMT_FRS_OU");
		tableMap.put("Function_New2", "./C_DSMT_FUNCTION");

		try {

			Adaptation container = sContext.getCurrentAdaptation();
			final AdaptationHome dataSpace = container.getHome();
			Session session = sContext.getSession();
		

			List<Adaptation> selectedRecord = sContext.getSelectedOccurrences();

			for (int i = 0; i < selectedRecord.size(); i++) {
				final Adaptation record = (Adaptation) selectedRecord.get(i);
				final AdaptationTable table = record.getContainerTable();
				String primaryKeyPath = null;
				LOG.info("Check table name : " + table.getTablePath());
				dataSet =record.getContainer().getLabel(Locale.getDefault());
				
				//dataSet = record.getAdaptationName().toString();
				

				String[] tableSplit = table.getTablePath().format().toString()
						.split("/");
				final String tableID = tableSplit[tableSplit.length - 1];

				PrimaryKey key = record.getOccurrencePrimaryKey();

				Path[] pKeyPaths = table.getPrimaryKeySpec();

				LOG.info("PkeyPaths : " + pKeyPaths);

				String pKey = key.format().toString();

				LOG.info("pKey : " + pKey);

				for (Path k : pKeyPaths) {
					if (!k.format().toString().equalsIgnoreCase("/EFFDT")
							&& !k.format().toString()
									.equalsIgnoreCase("/SETID")) {
						primaryKeyPath = k.format().toString();
						LOG.info("primaryKeyPath :" + primaryKeyPath);

					}
				}

				AdaptationHome archiveDataSpace = dataSpace
						.getRepository()
						.lookupHome(
								HomeKey.forBranchName(DSMTConstants.ARCHIVE_DATA_SPACE_NAME));

				final String status = record.getString(Path
						.parse(effectiveStatusField));

				purgeRec = purgeUtil(purgeRec, record, tableMap, tableID, repo,
						primaryKeyPath, table, errRecords, errMessage,status);

		/*		if (!status.equalsIgnoreCase(DSMTConstants.ACTIVE)) {

					

				}*/ /*else {
					//if(checkEndDatedRecord(record)){
						purgeRec = purgeUtil(purgeRec, record, tableMap, tableID, repo,
								primaryKeyPath, table, errRecords, errMessage);
					//}
					else{
					errRecords.add(record);
					errMessage.put(record, DSMTConstants.ACTIVE_ERROR_MESG);
					}
					// throw
					// OperationException.createError("Active Records cannot be Purged");
				}*/

				/*
				 * Copying record to Archival Dataspace
				 */
				if (purgeRec) {
					ProgrammaticService progService = ProgrammaticService
							.createForSession(session, archiveDataSpace);

					Procedure procedureCopyRecord = new Procedure() {
						public void execute(ProcedureContext context)
								throws Exception {
							AdaptationTable table = record.getContainerTable();
							Path initialTable = table.getTablePath();
							AdaptationHome archiveDataSpace = dataSpace
									.getRepository()
									.lookupHome(
											HomeKey.forBranchName(DSMTConstants.ARCHIVE_DATA_SPACE_NAME));
							Adaptation archiveDataSet = archiveDataSpace
									.findAdaptationOrNull(AdaptationName
											.forName(dataSet));
							AdaptationTable archiveTable = archiveDataSet
									.getTable(initialTable);

							ValueContextForUpdate valueContext = context
									.getContextForNewOccurrence(record,
											archiveTable);
							try {
								context.doCreateOccurrence(valueContext,
										archiveTable);
							} catch (OperationException e) {
								e.getMessage();
							}

						}
					};

					ProcedureResult pResult = progService
							.execute(procedureCopyRecord);
					if (pResult.hasFailed()) {
						LOG.info("Exception has occured in copying record :- "
								+ pResult.getException());
					}

					else {
						LOG.info("Record Copied into Archival");

						/*
						 * Deleting record after archiving
						 */
						ProgrammaticService svc = ProgrammaticService
								.createForSession(session, dataSpace);

						Procedure procDeleteRecord = new Procedure() {
							public void execute(ProcedureContext context)
									throws Exception {
								context.setAllPrivileges(true);
								context.setTriggerActivation(false);

								context.doDelete(record.getAdaptationName(),
										false);

								context.setAllPrivileges(false);
								context.setTriggerActivation(true);

							}
						};
						ProcedureResult deleteResult = svc
								.execute(procDeleteRecord);

						if (deleteResult.hasFailed()) {

							LOG.info("Exception has occured in deleting record :- "
									+ deleteResult.getException());
						}

						else {
							LOG.info("Deleting record" + selectedRecord.get(i));
						}
					}
				}
			}

			selectedRecord.clear();

		}

		catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		final UIComponentWriter uiComponentWriter = sContext
				.getUIComponentWriter();
		uiComponentWriter.add_cr("<style type=\"text/css\">   ");
		uiComponentWriter.add_cr("    .pg-normal { ");
		uiComponentWriter.add_cr("        color: black;");
		uiComponentWriter.add_cr("         font-weight: normal;");
		uiComponentWriter.add_cr("        text-decoration: none;");
		uiComponentWriter.add_cr("        cursor: pointer;");
		uiComponentWriter.add_cr("    }");
		uiComponentWriter.add_cr("    .pg-selected {");
		uiComponentWriter.add_cr("        color: black;");
		uiComponentWriter.add_cr("        font-weight: bold;");
		uiComponentWriter.add_cr("        text-decoration: underline;");
		uiComponentWriter.add_cr("        cursor: pointer;");
		uiComponentWriter.add_cr("    }");
		uiComponentWriter.add_cr("</style>");

		uiComponentWriter.add_cr("<div style=\"" + DIV_STYLE + "\">");

		uiComponentWriter.add_cr("<tr style=\"background-color:#FFFBE6;\">");

		if (errRecords.size() > 0) {
			uiComponentWriter
					.add_cr("<div style=\"padding-left:10px; padding-top: 15px;\">");
			uiComponentWriter
					.add_cr("<p style=\"font-size:14px; font-weight:bold; color: red;\">");
			uiComponentWriter.add_cr("Below Record(s) are not Purged");
			uiComponentWriter.add_cr("</p>");
			uiComponentWriter.add_cr("</div>");
		} else if (purgeRec){
			uiComponentWriter
					.add_cr("<p style=\"font-size:14px; font-weight:bold; color: #008ae6;\">");
			uiComponentWriter
					.add_cr("Selected Record(s) are Successfully Purged");
			uiComponentWriter.add_cr("</p>");

		}else{
      	  uiComponentWriter
				.add_cr("<p style=\"font-size:14px; font-weight:bold; color: red;\">");
      	  uiComponentWriter.add_cr("Exception Occurred while purging Record. Please contact Support team for assistance");
      	  uiComponentWriter.add_cr("</p>");
        }

		uiComponentWriter.add_cr("</tr>");

		for (int i = 0; i < errRecords.size(); i++) {

			uiComponentWriter.add_cr("<table  class=\"text\" style=\""
					+ TABLE_STYLE + "\">");

			uiComponentWriter
					.add_cr("<tr style=\"padding:5px;height:20px;border:1px solid lightgrey;text-align:center;vertical-align:center;white-space:nowrap;cursor:pointer;\">");

			final Adaptation rec = errRecords.get(i);
			PrimaryKey key = rec.getOccurrencePrimaryKey();
			String pkey = key.format().toString();

			final UIHttpManagerComponent managerComponent = UIHttpManagerComponent
					.createOnServiceContext(sContext);
			managerComponent.selectInstanceOrOccurrence(rec);
			final String url = managerComponent.getURIWithParameters();

			final UIButtonSpecJSAction buttonCreateOrganisation = uiComponentWriter
					.buildButtonPreview(url, UserMessage.createInfo("Link"));
			buttonCreateOrganisation.setButtonLayout(UIButtonLayout.TEXT_ONLY);

			uiComponentWriter.add_cr("<td style=\"" + OCCURRENCE_STYLE + "\">"
					+ pkey);
			uiComponentWriter.add_cr("</td>");
			uiComponentWriter.add_cr("<td style=\"" + OCCURRENCE_STYLE + "\">");
			uiComponentWriter.addButtonJavaScript(buttonCreateOrganisation);
			uiComponentWriter.add_cr("</td>");
			uiComponentWriter.add_cr("<td style=\"" + OCCURRENCE_STYLE + "\">"
					+ "Reason : " + errMessage.get(errRecords.get(i)));
			uiComponentWriter.add_cr("</td>");
			uiComponentWriter.add_cr("</tr>");
			uiComponentWriter.add_cr("</table>");
		}

		uiComponentWriter.add_cr("</div>");
	}

	public boolean purgeUtil(boolean purgeRec, Adaptation record,
			Map<String, String> tableMap, String tableID, Repository repo,
			String primaryKeyPath, AdaptationTable table,
			List<Adaptation> errRecords, Map<Adaptation, String> errMessage,String status) throws ParseException {

		final String nodeFl = record.getString(Path.parse(nodeFlag));
		final String pkValue = record.getString(Path.parse(primaryKeyPath));
	

		if (null != tableMap.get(tableID)) {
			if(!checkEndDatedRecord(record))	{		
			checkGOCRecords(repo, tableMap, tableID, pkValue, errRecords,
					record, errMessage);
			}
		}else{
			if (status.equalsIgnoreCase(DSMTConstants.ACTIVE) && !checkEndDatedRecord(record)) {
				errRecords.add(record);
				errMessage.put(record, DSMTConstants.ACTIVE_ERROR_MESG);
				
			}
		}
		if (errRecords.isEmpty()) {
			if (nodeFl.equalsIgnoreCase("P")&& !checkEndDatedRecord(record)) {
				purgeRec = true;
				String predicate = DSMT2HierarchyPaths._GLOBAL_STD_PMF_STD_F_CHANNEL_C_DSMT_CHANNEL._PARENT_ID
						.format() + "= '" + pkValue + "'";

				RequestResult rs = table.createRequestResult(predicate);

				for (Adaptation childRec; (childRec = rs.nextAdaptation()) != null;) {
					String childStatus = childRec.getString(Path
							.parse(effectiveStatusField));

					if (childStatus.equalsIgnoreCase("A")) {
						purgeRec = false;

						errRecords.add(record);
						errMessage.put(record,
								DSMTConstants.CHILD_RECORD_ACTIVE_ERROR_MESG);
						// throw
						// OperationException.createError("Parent Records with Active Child Records cannot be Purged");

					}
				}

			} else {
				purgeRec = true;

			}
		}
		return purgeRec;

	}

	private void checkGOCRecords(Repository repo, Map<String, String> tableMap,
			String tableID, String pkValue, List<Adaptation> errRecords,
			Adaptation record, Map<Adaptation, String> errMessage) {

		// check if this record is used in goc
		String column = tableMap.get(tableID);
		final Date effDate = record.getDate(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._EFFDT);
		final SimpleDateFormat dateFormat = new SimpleDateFormat(DSMTConstants.DEFAULT_DATE_FORMAT);
		Adaptation metadataDataSet;
		Map<String, Path> childTable = new HashMap<String, Path>();
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(repo,
					DSMTConstants.GOC, DSMTConstants.GOC);
			
			
			childTable.put("C_DSMT_MAN_GEO_NEW",GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_GEO_CHILD.getPathInSchema());
			childTable.put("C_DSMT_MAN_SEG_NEW",GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_MAN_SEG_CHILD.getPathInSchema());
			childTable.put("C_DSMT_SID", GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_SID_CHILD.getPathInSchema());
			childTable.put("C_DSMT_LVID_TBL", GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_LVID_CHILD.getPathInSchema());
			childTable.put("C_DSMT_FRS_BU", GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_FRS_BU_CHILD.getPathInSchema());
			childTable.put("C_DSMT_FRS_OU", GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_C_DSMT_FRS_OU_CHILD.getPathInSchema());
			childTable.put("Function_New2", GOCPaths._GLOBAL_STD_ENT_STD_DSMTHier_Tables_Function_CHILD.getPathInSchema());
			
			Path targetTablePath = childTable.get(tableID);
			
			AdaptationTable targetTable = metadataDataSet
					.getTable(targetTablePath);
			
			String dateStr = null;
			dateStr = dateFormat.format(effDate);

			final String predicate = Path.parse(column).format() + "='"
					+ pkValue + "'";
			final String predicate1 = " date-equal(" + DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._EFFDT.format() + ", '"+ dateStr + "')";

			RequestResult resultSet = targetTable
					.createRequestResult(predicate + " and " +predicate1);

			if (resultSet.getSize() > 0) {
				
			
				
					Adaptation childRec =resultSet.nextAdaptation();
										
					String childID = childRec.getString(Path.parse(column));
					

					final String pred = Path.parse(column).format() + "='"
							+ childID + "'";
				//	final String pred1 = " date-equal(" + DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._EFFDT.format() + ", '"+ dateStr + "')";
					AdaptationTable gocTable = metadataDataSet.getTable(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC.getPathInSchema());
					
					RequestResult rs = gocTable
							.createRequestResult(pred/* + " and " +pred1*/);
					if (rs.getSize() > 0) {
							errRecords.add(record);
							errMessage.put(record, DSMTConstants.GOC_REFERENCE);
					}
			}
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private boolean checkEndDatedRecord(Adaptation record) throws ParseException{
		Date enddt = record
				.getDate(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._ENDDT);

		LOG.info("check end date: " + enddt);
		boolean endDated = true;

		final SimpleDateFormat dateFormat = new SimpleDateFormat(DSMTConstants.DEFAULT_DATE_FORMAT);
		String formatedDate = null;
		if (null != enddt) {
		
			formatedDate = dateFormat.format(enddt);

			
		}
		
		if(formatedDate.equalsIgnoreCase("9999-12-31")){
			endDated = false;
		}
		return endDated;
		
		
	}

}
