package com.citi.ebx.util;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class TableDuplication implements Procedure {
	Adaptation record ;
	Repository repo; 
	public String tablePath ;
	String columnId ;
	public String dataSpace;
	public String dataSet;
	
	public TableDuplication(Adaptation record,
			Repository repo,String tablePath,String columnId,String dataSpace,String dataSet ) {
		this.repo = repo;
		this.record = record;
		this.tablePath=tablePath;
		this.columnId=  columnId ;
		this.dataSpace= dataSpace ;
		this.dataSet= dataSet ;

	}


	public void execute(ProcedureContext pContext) throws Exception {
		
		try{
			pContext.setAllPrivileges(true);
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,dataSpace, dataSet);
		AdaptationTable targetTable = metadataDataSet.getTable(getPath(tablePath));
		String keyValue1 = record.getString(getPath(columnId));
		final PrimaryKey pk = targetTable.computePrimaryKey(new String[] { keyValue1});
		Adaptation targetRecord = targetTable.lookupAdaptationByPrimaryKey(pk);
		final ValueContextForUpdate valueContext ;
		if (targetRecord == null) {
			valueContext = pContext.getContextForNewOccurrence(record,targetTable);
			valueContext.setValue(keyValue1, getPath(columnId));
		} else {
			valueContext = pContext.getContextForNewOccurrence(record,targetTable);
		}
		
		if (targetRecord == null) {
			targetRecord = pContext.doCreateOccurrence(valueContext,  targetTable);
		} else {
			targetRecord = pContext.doModifyContent(targetRecord,  valueContext);
		}
		
		pContext.setTriggerActivation(true);
		pContext.setAllPrivileges(false);
		}
		catch(Exception e){
			e.printStackTrace();
		}	
	}
	
private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}
}
