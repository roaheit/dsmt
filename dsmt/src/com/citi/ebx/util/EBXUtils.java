package com.citi.ebx.util;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class EBXUtils {

	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	
	public static Adaptation getMetadataDataSet(final Repository repo,final String dataSpace,final String dataSet)
			throws OperationException {
		final AdaptationHome metadataDataSpace = repo.lookupHome(
				HomeKey.forBranchName(dataSpace));
		if (metadataDataSpace == null) {
			throw OperationException.createError("Metadata data space \"" + dataSpace + "\" not found.");
		}
		final Adaptation metadataDataSet = metadataDataSpace.findAdaptationOrNull(
				AdaptationName.forName(dataSet));
		if (metadataDataSet == null) {
			throw OperationException.createError("Metadata data set \"" + dataSet + "\" not found.");
		}
		return metadataDataSet;
	}
}
