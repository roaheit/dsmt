package com.citi.ebx.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.IGWMonitoringTableBean;
import com.citi.ebx.dsmt.util.MonitoringTableBean;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.workflow.vo.DsmtGocVO;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class IGWInsertMonitoringdata implements Procedure {
	IGWMonitoringTableBean mntBean;
	public Repository repo;
	public String tablePath ;
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	
	public IGWInsertMonitoringdata (IGWMonitoringTableBean mntBean,
			Repository repo, String tablePath) {
		this.repo = repo;
		this.mntBean = mntBean;
		this.tablePath = tablePath;
	}
	
	public void execute(ProcedureContext pContext) throws Exception {
	
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,"GOC_WF", "GOC_WF");
		final AdaptationTable targetTable = metadataDataSet.getTable(getPath(tablePath));
			
		LOG.info("IGWInsertMonitoringdata :  execute Started ");
			insertRecord(mntBean, pContext, targetTable);
			LOG.debug("IGWInsertMonitoringdata :  operation " +mntBean.getOperation());
			if(DSMTConstants.UPDATE.equalsIgnoreCase(mntBean.getOperation()))
			{
				LOG.debug("IGWInsertMonitoringdata :  requestType " +mntBean.getRequestID());
				if(!DSMTConstants.DIRECTUPLOAD.equalsIgnoreCase(mntBean.getRequestID())){
					ArrayList<DsmtGocVO> gocVOs = mntBean.getDsmtGocVOs();
					for (DsmtGocVO dsmtGocVO : gocVOs) {
						insertGOCRecord(dsmtGocVO, pContext, metadataDataSet.getTable(getPath("/root/C_GOC_WF_DTL")));
					}
					
				}
			}
		
			LOG.info("IGWInsertMonitoringdata :  execute End ");
		
	}
	
	private void insertRecord(IGWMonitoringTableBean tableBean, final ProcedureContext pContext,
			final AdaptationTable targetTable) throws OperationException {
		
		LOG.info("IGWInsertMonitoringdata :  insertRecord Started ");
		Adaptation targetRecord =  getHistoryRecord(tableBean,targetTable); 
		final ValueContextForUpdate valueContext;
		Date now = Calendar.getInstance().getTime();
		if(tablePath.contains("GOC_WF")){
			if (targetRecord == null) {
				valueContext = pContext.getContextForNewOccurrence(targetTable);
				valueContext.setValue(tableBean.getRequestID(),GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_ID);
				valueContext.setValue(tableBean.getRequestType(),GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_TYPE);
				valueContext.setValue(tableBean.getChildDataSpaceID(),GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID );
				valueContext.setValue(tableBean.getRequestBy(),GOCWFPaths._C_GOC_WF_REPORTING._REQ_BY);
				valueContext.setValue(now,GOCWFPaths._C_GOC_WF_REPORTING._REQ_DT);
				valueContext.setValue(tableBean.getRequestStatus(), GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
				valueContext.setValue(tableBean.getMaintenanceType(), GOCWFPaths._C_GOC_WF_REPORTING._Maintenance_type);
			} else {
				valueContext = pContext.getContext(targetRecord.getAdaptationName());
				ArrayList<DsmtGocVO> gocVOs = tableBean.getDsmtGocVOs();
				for (DsmtGocVO dsmtGocVO : gocVOs) {
					dsmtGocVO.getApproverGroup();
					valueContext.setValue(dsmtGocVO.getApproverGroup(),GOCWFPaths._C_GOC_WF_REPORTING._GRID_ROUTING_ID);
				}
				valueContext.setValue(now,GOCWFPaths._C_GOC_WF_REPORTING._SUBIMITTED_DT);
				valueContext.setValue(tableBean.getRequestorComment(),GOCWFPaths._C_GOC_WF_REPORTING._Requester_Comment);
				valueContext.setValue(tableBean.getGocRecordCount(),GOCWFPaths._C_GOC_WF_REPORTING._GOC_COUNT);
				valueContext.setValue(tableBean.getRequestStatus(),GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
				valueContext.setValue(tableBean.getMaintenanceType(), GOCWFPaths._C_GOC_WF_REPORTING._Maintenance_type);
			}
			
			
			if (targetRecord == null) {
				 targetRecord = pContext.doCreateOccurrence(valueContext,  targetTable);
			} else {
				targetRecord = pContext.doModifyContent(targetRecord,  valueContext);
			}
			
			pContext.setTriggerActivation(true);
			pContext.setAllPrivileges(false);
		}
		else{
			String chldDsp= targetRecord.getString(GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID);
			if (chldDsp == null) {
				valueContext = pContext.getContext(targetRecord.getAdaptationName());
				//valueContext.setValue(Integer.parseInt()tableBean.getRequestID(),GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_ID);
				valueContext.setValue(tableBean.getRequestType(),GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_TYPE);
				valueContext.setValue(tableBean.getChildDataSpaceID(),GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID );
				valueContext.setValue(tableBean.getRequestBy(),GOCWFPaths._C_GOC_WF_REPORTING._REQ_BY);
				valueContext.setValue(now,GOCWFPaths._C_GOC_WF_REPORTING._REQ_DT);
				valueContext.setValue(tableBean.getRequestStatus(), GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
				LOG.info("maintenance type :: "+tableBean.getMaintenanceType());
				valueContext.setValue(tableBean.getMaintenanceType(), GOCWFPaths._C_GOC_WF_REPORTING._Maintenance_type);
				//valueContext.setValue(tableBean.getFunctionalID(), GOCWFPaths._C_DSMT_INT_GOC_REPORT._C_DSMT_FUNCTION);
			} else {
				valueContext = pContext.getContext(targetRecord.getAdaptationName());
				/*ArrayList<DsmtGocVO> gocVOs = tableBean.getDsmtGocVOs();
				for (DsmtGocVO dsmtGocVO : gocVOs) {
					dsmtGocVO.getApproverGroup();
					valueContext.setValue(dsmtGocVO.getApproverGroup(),GOCWFPaths._C_GOC_WF_REPORTING._GRID_ROUTING_ID);
				}*/
				valueContext.setValue(now,GOCWFPaths._C_DSMT_INT_GOC_REPORT._SUBIMITTED_DT);
				valueContext.setValue(tableBean.getRequestorComment(),GOCWFPaths._C_DSMT_INT_GOC_REPORT._Requester_Comment);
				valueContext.setValue(tableBean.getTotalMod(),GOCWFPaths._C_DSMT_INT_GOC_REPORT._TOT_MOD);
				valueContext.setValue(tableBean.getUpdatedGOCCount(),GOCWFPaths._C_DSMT_INT_GOC_REPORT._MOD_GOC_CNT);
				valueContext.setValue(tableBean.getNewNonGOCCount(),GOCWFPaths._C_DSMT_INT_GOC_REPORT._NEW_NONGOC_CNT);
				valueContext.setValue(tableBean.getUpdatedNonGOCCount(),GOCWFPaths._C_DSMT_INT_GOC_REPORT._MOD_NONGOC_CNT);
				valueContext.setValue(tableBean.getGocRecordCount(),GOCWFPaths._C_DSMT_INT_GOC_REPORT._NEW_NONGOC_CNT);
				valueContext.setValue(GOCConstants.SUBMITTED,GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
				//valueContext.setValue(tableBean.getFunctionalID(), GOCWFPaths._C_DSMT_INT_GOC_REPORT._C_DSMT_FUNCTION);
				valueContext.setValue(tableBean.getMaintenanceType(), GOCWFPaths._C_DSMT_INT_GOC_REPORT._Maintenance_type);
			}
			
			
			
				targetRecord = pContext.doModifyContent(targetRecord,  valueContext);
		
			
			pContext.setTriggerActivation(true);
			pContext.setAllPrivileges(false);
		}
		
		
		
		LOG.info("IGWInsertMonitoringdata :  insertRecord Ends ");
		
		
	}
	
protected Adaptation getHistoryRecord(final IGWMonitoringTableBean bean, final AdaptationTable targetTable){
	
	if(tablePath.contains("GOC_WF_")){
Adaptation targetRecord = null;
		
		
		final String predicate = 	GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID.format()  + " = \"" + bean.getChildDataSpaceID() + "\"";
		
		RequestResult reqRes = targetTable.createRequestResult(predicate);
		if (null != reqRes) {
			
			targetRecord = reqRes.nextAdaptation();
			reqRes.close();
			
		}
	
		return targetRecord;
	}
	else{
		

Adaptation targetRecord = null;
		
		final String predicate = 	GOCWFPaths._C_DSMT_INT_GOC_REPORT._REQUEST_ID.format()+" = " + Integer.parseInt(bean.getRequestID())  ;
		LOG.info(predicate);
		
		RequestResult reqRes = targetTable.createRequestResult(predicate);
		if (null != reqRes) {
			
			targetRecord = reqRes.nextAdaptation();
			
			reqRes.close();
			
		}
	
		return targetRecord;
	
	}
		
		
	}
	
private void insertGOCRecord(DsmtGocVO gocVO, final ProcedureContext pContext,
		final AdaptationTable targetTable) throws OperationException {
	
	LOG.debug("IGWInsertMonitoringdata :  insertGOCRecord Started ");
	final ValueContextForUpdate valueContext = pContext.getContextForNewOccurrence(targetTable);
	valueContext.setValue(mntBean.getRequestID(), GOCWFPaths._C_GOC_WF_DTL._REQUEST_ID);
	valueContext.setValue(gocVO.getGOCPK(), GOCWFPaths._C_GOC_WF_DTL._GOC_PK);
	valueContext.setValue(gocVO.getGOC(), GOCWFPaths._C_GOC_WF_DTL._GOC);
	valueContext.setValue(gocVO.getMANAGER_ID(), GOCWFPaths._C_GOC_WF_DTL._OWNER);
	valueContext.setValue(gocVO.getSETID(), GOCWFPaths._C_GOC_WF_DTL._SID);
	valueContext.setValue(gocVO.getC_DSMT_MAN_GEO_ID(), GOCWFPaths._C_GOC_WF_DTL._C_DSMT_MAN_GEO);
	valueContext.setValue(gocVO.getC_DSMT_MAN_SEG_ID(), GOCWFPaths._C_GOC_WF_DTL._C_DSMT_MAN_SEG);
	valueContext.setValue(gocVO.getC_DSMT_FRS_BU(),GOCWFPaths._C_GOC_WF_DTL._C_DSMT_FRS_BU);
	valueContext.setValue(gocVO.getC_DSMT_FRS_OU(),GOCWFPaths._C_GOC_WF_DTL._C_DSMT_FRS_OU);
	valueContext.setValue(gocVO.getC_DSMT_FUNCTION(),GOCWFPaths._C_GOC_WF_DTL._C_DSMT_FUNCTION);
	valueContext.setValue(gocVO.getC_DSMT_LVID(),GOCWFPaths._C_GOC_WF_DTL._C_DSMT_LVID);
	valueContext.setValue(gocVO.getDesc(),GOCWFPaths._C_GOC_WF_DTL._DESCR);
	valueContext.setValue(gocVO.getDescShort(),GOCWFPaths._C_GOC_WF_DTL._DESCRSHORT);
	valueContext.setValue(gocVO.getGocUsages(),GOCWFPaths._C_GOC_WF_DTL._C_DSMT_GOC_USAGE);
	valueContext.setValue(gocVO.getC_DSMT_LOC_COST_CD(), GOCWFPaths._C_GOC_WF_DTL._C_DSMT_LOC_COST_CD);
	valueContext.setValue(gocVO.getACTION_REQD(), GOCWFPaths._C_GOC_WF_DTL._ACTION_REQD);
	pContext.doCreateOccurrence(valueContext,  targetTable);
	LOG.debug("IGWInsertMonitoringdata :  insertGOCRecord Started ");
	
}
private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}

}
