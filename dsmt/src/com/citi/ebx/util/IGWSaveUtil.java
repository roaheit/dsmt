package com.citi.ebx.util;

import java.util.Map;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class IGWSaveUtil implements Procedure {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	Repository repo;
	Adaptation record;
	String DataSetName;
	String DataSpaceName;
	Path TablePath;
	Map<Path, Object> inputMap;
	Map<Path, Object> outputMap;
	boolean isUpdate;
	String predicate;

	public IGWSaveUtil(Repository repo, String dataSpaceName, String dataSetName, Path tablePath, Map<Path, Object> inputMap,
			Adaptation record, boolean isUpdate, Map<Path, Object> outputMap, String predicate) {
		this.repo = repo;
		this.inputMap = inputMap;
		this.DataSetName = dataSetName;
		this.DataSpaceName = dataSpaceName;
		this.TablePath = tablePath;
		this.isUpdate = isUpdate;
		this.outputMap = outputMap;
		this.predicate = predicate;
		this.record = record;

	}

	public void execute(ProcedureContext pContext) throws Exception {
		LOG.info("IGWSaveUtil:Excute starts");

		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo, DataSpaceName, DataSetName);

		AdaptationTable targetTable = metadataDataSet.getTable(TablePath);
		Path[] pkPaths = targetTable.getPrimaryKeySpec();
		PrimaryKey key = null;
		
		if(null!=record){
			LOG.info("IGWSaveUtil:Copy record is not null ");
			if(!isUpdate){
				LOG.info("IGWSaveUtil:Copy record's new request !! ");				
				key = targetTable.computePrimaryKey(record);
				ValueContextForUpdate vc = pContext.getContextForNewOccurrence(record, targetTable);
				vc.setValue(Integer.parseInt(key.format()), pkPaths[0]);
				pContext.setTriggerActivation(false);
				pContext.setAllPrivileges(true);

				Adaptation adp = pContext.doCreateOccurrence(vc, targetTable);
				if(null!=inputMap){
					for (Path path : inputMap.keySet()) {
						vc.setValue(inputMap.get(path), path);
					}
				}
				pContext.setTriggerActivation(true);
				pContext.setAllPrivileges(false);
				if (null != outputMap) {
					for (Path path : outputMap.keySet()) {

						outputMap.put(path, adp.get(path));
					}
				}

				LOG.debug("IGWSaveUtil:Save done ");
			}
			else{
				LOG.info("IGWSaveUtil:Copy record's update request !! ");
                key = targetTable.computePrimaryKey(record);				
                Adaptation adp = targetTable.lookupAdaptationByPrimaryKey(key);
                ValueContextForUpdate vc = pContext.getContextForNewOccurrence(record, targetTable);					
				vc.setValue(Integer.parseInt(key.format()), pkPaths[0]);
				if(null!=adp){
					if(null!=inputMap){
						for (Path path : inputMap.keySet()) {
							vc.setValue(inputMap.get(path), path);
						}
					}
					adp = pContext.doModifyContent(adp, vc);
					if (null != outputMap) {
						for (Path path : outputMap.keySet()) {

							outputMap.put(path, adp.get(path));
						}
					}
				}
				else{
					pContext.setTriggerActivation(false);
					pContext.setAllPrivileges(true);
					if(null!=inputMap){
						for (Path path : inputMap.keySet()) {
							vc.setValue(inputMap.get(path), path);
						}
					}

					 adp = pContext.doCreateOccurrence(vc, targetTable);

					pContext.setTriggerActivation(true);
					pContext.setAllPrivileges(false);
					if (null != outputMap) {
						for (Path path : outputMap.keySet()) {

							outputMap.put(path, adp.get(path));
						}
					}

					LOG.debug("IGWSaveUtil:Save done ");
				}
				
				
			}
		}
		else{
			if(!isUpdate){
				ValueContextForUpdate vc = pContext.getContextForNewOccurrence(targetTable);
				for (Path path : inputMap.keySet()) {
					vc.setValue(inputMap.get(path), path);
				}
				if(null!=inputMap){
					for (Path path : inputMap.keySet()) {
						vc.setValue(inputMap.get(path), path);
					}
				}

				pContext.setTriggerActivation(true);
				pContext.setAllPrivileges(true);

				Adaptation adp = pContext.doCreateOccurrence(vc, targetTable);

				pContext.setTriggerActivation(true);
				pContext.setAllPrivileges(false);
				if (null != outputMap) {
					for (Path path : outputMap.keySet()) {

						outputMap.put(path, adp.get(path));
					}
				}

				LOG.debug("IGWSaveUtil:Save done !! ");

			}
			else{
				Adaptation adp = null;
				if (null != predicate) {
					RequestResult rs = targetTable.createRequestResult(predicate);
					adp = rs.nextAdaptation();
				}
				if (null != adp) {
					
						ValueContextForUpdate vc = pContext.getContext(adp.getAdaptationName());
					

					for (Path path : inputMap.keySet()) {
						vc.setValue(inputMap.get(path), path);
					}

					adp = pContext.doModifyContent(adp, vc);
					if (null != outputMap) {
						for (Path path : outputMap.keySet()) {

							outputMap.put(path, adp.get(path));
						}
					}

				} else {
					LOG.info("IGWSaveUtil:Inside update's new !! ");
					
					
					 ValueContextForUpdate vc = pContext.getContextForNewOccurrence(targetTable);
					
					for (Path path : inputMap.keySet()) {
						vc.setValue(inputMap.get(path), path);
					}

					adp = pContext.doCreateOccurrence(vc, targetTable);

					pContext.setTriggerActivation(true);
					pContext.setAllPrivileges(false);
					if (null != outputMap) {
						for (Path path : outputMap.keySet()) {

							outputMap.put(path, adp.get(path));
						}
					}

				}

				LOG.debug("IGWSaveUtil:Update Done!! ");
			}
		}

		

	}

}
