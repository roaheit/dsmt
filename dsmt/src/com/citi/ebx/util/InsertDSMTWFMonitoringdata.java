package com.citi.ebx.util;

import java.util.Calendar;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.MonitoringTableBean;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class InsertDSMTWFMonitoringdata implements Procedure{
	MonitoringTableBean mntBean;
	public Repository repo;
	public String tablePath ;
	public String tableupdated;
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	
	
	public InsertDSMTWFMonitoringdata(MonitoringTableBean mntBean,
			Repository repo, String tablePath, String tableUpdated) {
		super();
		this.mntBean = mntBean;
		this.repo = repo;
		this.tablePath = tablePath;
		this.tableupdated = tableUpdated;
	}

	@Override
	public void execute(ProcedureContext pContext) throws Exception {
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,"DSMT_WF", "DSMT_WF");
		final AdaptationTable targetTable = metadataDataSet.getTable(getPath(tablePath));
			
		LOG.info("InsertDSMTWFMonitoringdata :  execute Started ");
			insertRecord(mntBean, pContext, targetTable);
			LOG.debug("InsertDSMTWFMonitoringdata :  operation " +mntBean.getOperation());
			LOG.info("InsertDSMTWFMonitoringdata :  execute End ");
	}

	
	private void insertRecord(MonitoringTableBean tableBean, final ProcedureContext pContext,
			final AdaptationTable targetTable) throws OperationException {
		
		LOG.info("InsertMonitoringdata :  insertRecord Started ");
		Adaptation targetRecord =  getHistoryRecord(tableBean.getChildDataSpaceID(),targetTable); 
		final ValueContextForUpdate valueContext;
		Date now = Calendar.getInstance().getTime();
		if (targetRecord == null) {
			valueContext = pContext.getContextForNewOccurrence(targetTable);
			valueContext.setValue(tableBean.getRequestID(),GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_ID);
			valueContext.setValue(tableBean.getRequestType(),GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_TYPE);
			valueContext.setValue(tableBean.getChildDataSpaceID(),GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID );
			valueContext.setValue(tableBean.getRequestBy(),GOCWFPaths._C_GOC_WF_REPORTING._REQ_BY);
			valueContext.setValue(now,GOCWFPaths._C_GOC_WF_REPORTING._REQ_DT);
			valueContext.setValue(tableBean.getRequestStatus(), GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
			valueContext.setValue(tableBean.getDataSet(), DSMTConstants.DATA_SET);
			valueContext.setValue(tableBean.getTableUpdated(), DSMTConstants.tableId);
		} else {
			
			LOG.info("InsertMonitoringdata :  insertRecord inside else ");
			valueContext = pContext.getContext(targetRecord.getAdaptationName());

			valueContext.setValue(now,GOCWFPaths._C_GOC_WF_REPORTING._SUBIMITTED_DT);
			valueContext.setValue(tableBean.getRequestorComment(),GOCWFPaths._C_GOC_WF_REPORTING._Requester_Comment);
			valueContext.setValue(tableBean.getRequestStatus(),GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
		//	valueContext.setValue(tableBean.getTableUpdated(), DSMTConstants.tableId);
			valueContext.setValue(tableBean.getRecordCount(), DSMTConstants.RECORD_COUNT);
		}
		
		
		if (targetRecord == null) {
			 targetRecord = pContext.doCreateOccurrence(valueContext,  targetTable);
		} else {
			LOG.info("InsertMonitoringdata :  insertRecord inside else else");
			targetRecord = pContext.doModifyContent(targetRecord,  valueContext);
		}
		
		pContext.setTriggerActivation(true);
		pContext.setAllPrivileges(false);
		       
		LOG.info("InsertMonitoringdata :  insertRecord Ends ");
		
		
	}
	
protected Adaptation getHistoryRecord(final Object childataspace, final AdaptationTable targetTable){
		
		Adaptation targetRecord = null;
		final String predicate = 	GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID.format()  + " = \"" + childataspace + "\"";
		
		RequestResult reqRes = targetTable.createRequestResult(predicate);
		if (null != reqRes) {
			
			targetRecord = reqRes.nextAdaptation();
			reqRes.close();
			
		}
	
		return targetRecord;
	}
	
private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}
}
