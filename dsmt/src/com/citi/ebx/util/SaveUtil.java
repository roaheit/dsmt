package com.citi.ebx.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class SaveUtil implements Procedure {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	final String env = DSMTConstants.propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT); // "local" ; 
	
	Repository repo;
	String DataSetName;
	String DataSpaceName;
	Path TablePath;
	Map<Path, Object> inputMap;
	boolean isUpdate;

	public SaveUtil(Repository repo,String dataSpaceName,String dataSetName,  Path tablePath, Map<Path, Object> inputMap , boolean isUpdate  ) {
		this.repo = repo;
		this.inputMap = inputMap;
		this.DataSetName= dataSetName;
		this.DataSpaceName= dataSpaceName;
		this.TablePath= tablePath;
		this.isUpdate=isUpdate;
		

	}

	public void execute(ProcedureContext pContext)  {
		LOG.debug("Save started !! "); 
		try{
			if(DataSetName.equalsIgnoreCase(DSMTConstants.DATASET_NEW_DSMT2)){
				if(env.equalsIgnoreCase("uat")){
					DataSetName=DSMTConstants.DATASET_NEW_DSMT2_UAT;
				}
				else if(env.equalsIgnoreCase("prod")){
					DataSetName=DSMTConstants.DATASET_NEW_DSMT2_PROD;
				}
				else if(env.equalsIgnoreCase("local")){
					DataSetName=DSMTConstants.DATASET_NEW_DSMT2_UAT;
				}
				
				
			}
			else if(DataSetName.equalsIgnoreCase(DSMTConstants.DATASET_LA)){
				if(env.equalsIgnoreCase("uat")){
					DataSetName=DSMTConstants.DATASET_LA_UAT;
				}
				else if(env.equalsIgnoreCase("prod")){
					DataSetName=DSMTConstants.DATASET_LA_PROD;
				}
				
			}
			Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo, DataSpaceName, DataSetName);

			AdaptationTable targetTable = metadataDataSet.getTable(TablePath);
			
			pContext.setAllPrivileges(true);
            pContext.setTriggerActivation(false);
            
			if (!isUpdate){
				ValueContextForUpdate vc = pContext.getContextForNewOccurrence(targetTable);
				
				for(Path path:inputMap.keySet()){
					vc.setValue(inputMap.get(path), path);
				}

				pContext.doCreateOccurrence(vc, targetTable);

				LOG.debug("Save done !! "); 
			}
			else{
				RequestResult rs=targetTable.createRequestResult("(" + DSMTConstants.CATALOG_TABLE_FILENAME.format()+ " = '" + inputMap.get(DSMTConstants.CATALOG_TABLE_FILENAME) + "')");
				Adaptation adp=rs.nextAdaptation();
				
				int size=rs.getSize();
				if(size!=0){
					ValueContextForUpdate vc = pContext.getContext(adp.getAdaptationName());
					
					if (null != vc.getValue(DSMTConstants.lastUpdateDateFieldPath)) {
						
						Integer prev_count = (Integer) vc.getValue(DSMTConstants.CATALOG_TABLE_COUNT);
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						Date one = (Date) vc.getValue(DSMTConstants.lastUpdateDateFieldPath);
						Integer record_count = (Integer) inputMap.get(DSMTConstants.CATALOG_TABLE_COUNT);
						Date now = new Date();
						if (sdf.format(now).compareTo(sdf.format(one)) > 0) {
							vc.setValue(prev_count, DSMTConstants.CATALOG_TABLE_PREV_COUNT);
							Integer variance = record_count - prev_count;
							vc.setValue(variance, DSMTConstants.CATALOG_TABLE_VARIANCE);
						}

						vc.setValue(record_count, DSMTConstants.CATALOG_TABLE_COUNT);

						pContext.doModifyContent(adp, vc);
					}
				}
				else{
					ValueContextForUpdate vc = pContext.getContextForNewOccurrence(targetTable);
					
					for(Path path:inputMap.keySet()){
						vc.setValue(inputMap.get(path), path);
					}

					pContext.doCreateOccurrence(vc, targetTable);

					
					
				}
				
				pContext.setTriggerActivation(true);
				pContext.setAllPrivileges(false);
				
				LOG.debug("Update Done!! ");
			}
			
		}catch(Exception e){
			LOG.error("SaveUtil: Error while execute of SaveUtil.", e);
		}
		
		
		
		

	}

}
