package com.citi.ebx.util;

import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class UpdateGOCWFDtlTable implements Procedure{
	
	Adaptation record ;
	Repository repo; 
	public String tablePath ;
	public String dataSpace ;
	public String dataSet ;
	//private String updatedTime = "./CHNG_DTTM";
	
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	public UpdateGOCWFDtlTable (Adaptation record,
			Repository repo, String tablePath , String dataSpace, String dataSet) {
		this.repo = repo;
		this.record = record;
		this.tablePath = tablePath;
		this.dataSpace = dataSpace;
		this.dataSet = dataSet;
	}
	
	
	
	
	public void execute(ProcedureContext pContext) throws Exception {

		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
				dataSpace, dataSet);
		
		updateGOCdata(pContext);
/**		
		ValueContextForUpdate valueContext = null;
 		AdaptationTable targetTable = metadataDataSet
				.getTable(getPath(tablePath));
		Adaptation targetRecord = null;

		final String gocPK = record.get(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK)
				.toString();

		final String pred1 = GOCWFPaths._C_GOC_WF_DTL._GOC_PK.format()
				+ " = \"" + gocPK + "\"";

		String gocUpdateDate = null;
		final SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss");

		try {
			gocUpdateDate = dateFormat.format(record.getDate(Path
					.parse(updatedTime)));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOG.error("Repor end Date is in wrong format for report update time");
		}
		final String pred2 = "date-greater-than("
				+ Path.parse(updatedTime).format() + ",'" + gocUpdateDate
				+ "')";
		final String predicate = pred1 + " and " + pred2;

		LOG.info("UpdateGOCWFDtlTable ::  execute procedure predicate value : "
				+ predicate);
		RequestResult reqRes = targetTable.createRequestResult(predicate);
		if (null != reqRes) {
			targetRecord = reqRes.nextAdaptation();
		}

		if (targetRecord != null) {

			valueContext = pContext
					.getContext(targetRecord.getAdaptationName());
			valueContext
					.setValue(
							record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC),
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC);
			valueContext
					.setValue(
							record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD),
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD);
			valueContext
					.setValue(
							record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_COMMENT),
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_COMMENT);

			targetRecord = pContext.doModifyContent(targetRecord, valueContext);

		}
**/
	}
	

	
private void updateGOCdata(ProcedureContext pContext) throws OperationException {
	
	
	Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,dataSpace, dataSet);
	AdaptationTable targetTable = metadataDataSet.getTable(GOCWFPaths._C_GOC_WF_REPORTING.getPathInSchema());
	 String childDataSpace = record.getHome().getKey().getName();
	 LOG.info("UpdateGOCWFDtlTable ::  childDataSpace : " +childDataSpace);
	 Adaptation targetRecord =  getHistoryRecord(childDataSpace, targetTable);
	 ValueContextForUpdate valueContext =null;
	 		if(null!= targetRecord){
			final String gocPK = record
					.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK)
					.toString();
			final String wfID = targetRecord.get(
					GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_ID).toString();
			LOG.info("UpdateGOCWFDtlTable--> updateGOCdata-->  wfID :" +wfID +" gocPK : "+gocPK);
			
			AdaptationTable targetTableGOCDtl = metadataDataSet
					.getTable(GOCWFPaths._C_GOC_WF_DTL.getPathInSchema());
			final PrimaryKey pk = targetTableGOCDtl
					.computePrimaryKey(new String[] { wfID, gocPK });

			final Adaptation gocWfDetailRecord = targetTableGOCDtl
					.lookupAdaptationByPrimaryKey(pk);
			if (gocWfDetailRecord != null) {

				valueContext = pContext.getContext(gocWfDetailRecord
						.getAdaptationName());
				valueContext
						.setValue(
								record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC),
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC);
				valueContext
						.setValue(
								record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD),
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD);
				valueContext
						.setValue(
								record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_COMMENT),
								GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_COMMENT);

				 pContext.doModifyContent(gocWfDetailRecord,
						valueContext);
			}
		
	}
		
	}


protected Adaptation getHistoryRecord(final Object childataspace, final AdaptationTable targetTable){
	
	Adaptation targetRecord = null;
	
	
	final String predicate = 	GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID.format()  + " = \"" + childataspace + "\"";
	
	RequestResult reqRes = targetTable.createRequestResult(predicate);
	if (null != reqRes) {
		
		targetRecord = reqRes.nextAdaptation();
		reqRes.close();
		
	}

	return targetRecord;
}

private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}

}
