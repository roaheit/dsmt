package com.citi.ebx.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.SchemaTypeName;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.ReadOnlyProcedure;
import com.orchestranetworks.service.Session;

public class Utils {
	/**
	 * Returns whether 2 objects are equal, where both being null also means they're equal.
	 * This is the same behavior you get with Apache ObjectUtils class.
	 * 
	 * @param o1 the first object
	 * @param o2 the second object
	 * @return whether they're equal
	 */
	public static boolean objectsEqual(final Object o1, final Object o2) {
		if (o1 == null) {
			return o2 == null;
		}
		if (o2 == null) {
			return false;
		}
		return o1.equals(o2);
	}
	
	public static final String getValueString(final Object value, final SchemaNode node) {
		if (value == null) {
			return "null";
		}
		final SchemaTypeName type = node.getXsTypeName();
		if (SchemaTypeName.XS_DATE.equals(type)) {
			final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			return dateFormat.format((Date) value);
		}
		if (SchemaTypeName.XS_DATETIME.equals(type)) {
			final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-ddTHH:mm:ss");
			return dateFormat.format((Date) value);
		}
		if (SchemaTypeName.XS_TIME.equals(type)) {
			final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
			return dateFormat.format((Date) value);
		}
		if (SchemaTypeName.XS_BOOLEAN.equals(type)
				|| SchemaTypeName.XS_INTEGER.equals(type)
				|| SchemaTypeName.XS_INT.equals(type)
				|| SchemaTypeName.XS_DECIMAL.equals(type)) {
			return value.toString();
		}
		return "\"" + value.toString() + "\"";
	}
	
	public static synchronized Adaptation getLinkedRecord(final Adaptation record, final Path fkPath) {
		return record.getSchemaNode().getNode(fkPath)
				.getFacetOnTableReference().getLinkedRecord(record);
	}
	
	public static Adaptation getLinkedRecord(final ValueContext vc, final Path fkPath) {
		return vc.getNode().getNode(fkPath)
				.getFacetOnTableReference().getLinkedRecord(vc);
	}
	
	public static void executeProcedure(final Procedure proc, final Session session,
			final AdaptationHome dataSpace) throws OperationException {
		final ProgrammaticService svc = ProgrammaticService.createForSession(session, dataSpace);
		final ProcedureResult procResult = svc.execute(proc);
		if (procResult.getException() != null) {
			throw procResult.getException();
		}
	}
	
	
	public static void executeReadOnlyProcedure(final ReadOnlyProcedure proc, final Session session,
			final AdaptationHome dataSpace) throws OperationException {
		final ProgrammaticService svc = ProgrammaticService.createForSession(session, dataSpace);
		final ProcedureResult procResult = svc.execute(proc);
		if (procResult.getException() != null) {
			throw procResult.getException();
		}
	}
	
	private Utils() {
	}
}
