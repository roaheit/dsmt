package com.citi.ebx.dsmt.constraint;

import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.Constraint;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;

public class DSMTOUBUFilterConstraint implements Constraint{

	@Override
	public void checkOccurrence(Object value, ValueContextForValidation context)
			throws InvalidSchemaException {
	
		final StringBuffer message = new StringBuffer();
		
		AdaptationTable table = context.getAdaptationTable();
		Adaptation record = table.lookupAdaptationByPrimaryKey(PrimaryKey.parseString((String) value));
		if (record == null)
		{
			return;
		}
		 		
		boolean isParentRecordActiveOrBothInactive = isParentRecordActiveOrBothInactive(record, context);
		
	
		
		
		
		if(! isParentRecordActiveOrBothInactive){
			message.append("The Parent record is inactive.");
			
		}
		
		if(!isParentRecordActiveOrBothInactive){
			context.addInfo(String.valueOf(message));
		}
		
		
	}
	



	
	

	private boolean isParentRecordActiveOrBothInactive(Adaptation parentRecord, ValueContext context){
		
		final String currentRecordStatus = (String) context.getValue(getParent(statusField));	
		final String parentRecordStatus =  parentRecord.getString(getPath(statusField));
		
		if(null == currentRecordStatus || null == parentRecordStatus){ // THIS SCENARIO SHOULD NOT HAPPEN BUT JUST IN CASE
			return true;
		}
	
		final boolean isParentRecordActiveOrBothInactive = DSMTConstants.ACTIVE.equalsIgnoreCase(parentRecordStatus) || ( DSMTConstants.INACTIVE.equalsIgnoreCase(currentRecordStatus) && DSMTConstants.INACTIVE.equalsIgnoreCase(parentRecordStatus));
		return isParentRecordActiveOrBothInactive;
		
		
	}
	
	
	
	
	
	private String statusField = "./EFF_STATUS";
	
	
	
	
	private Path getParent(final String fieldName) {
		
		return Path.parse(DSMTConstants.DOT + fieldName);
	}


	private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}



	


	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		
		return null;
	}



	@Override
	public void setup(ConstraintContext arg0) {
		
		
	}



	

	

}

