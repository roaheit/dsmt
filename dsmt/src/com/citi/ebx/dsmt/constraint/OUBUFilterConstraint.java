package com.citi.ebx.dsmt.constraint;

import java.util.Date;
import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.Constraint;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;

public class OUBUFilterConstraint  implements Constraint{

	@Override
	public void checkOccurrence(Object value, ValueContextForValidation context)
			throws InvalidSchemaException {
	
		final StringBuffer message = new StringBuffer();
		
		AdaptationTable table = context.getAdaptationTable();
		Adaptation record = table.lookupAdaptationByPrimaryKey(PrimaryKey.parseString((String) value));
		if (record == null)
		{
			return;
		}
		boolean isParentEffectiveOrBothEndDated = isParentEffectiveOrBothEndDated(record, context); // logic to be set up  isNodeFlagSetAsParent && isParentRecordActiveOrBothInactive && isChildEndDatedOrBothEffective ; 		
		boolean isParentRecordActiveOrBothInactive = isParentRecordActiveOrBothInactive(record, context);
		
		
		final Date currentRecordEndDate = (Date) context.getValue(getParent(endDateField));	
		boolean isCurrentRecordEndDated = lastEndDate.after(currentRecordEndDate);
		if(isCurrentRecordEndDated){
			return;
		}
		
		
		if(! isParentEffectiveOrBothEndDated){
			message.append("The Parent has been end dated with a non-end dated child. ");
		}
		
		
		
		if(! isParentRecordActiveOrBothInactive){
			message.append("The Parent record is inactive.");
			
		}
		
		if(! isParentEffectiveOrBothEndDated || !isParentRecordActiveOrBothInactive){
			context.addInfo(String.valueOf(message));
		}
		
		
	}
	



	private boolean isParentEffectiveOrBothEndDated(Adaptation parentRecord, ValueContext context){
		
		
		
		final Date currentRecordEndDate = (Date) context.getValue(getParent(endDateField));	
		final Date parentRecordEndDate =  parentRecord.getDate(getPath(endDateField));
		
		if(null == currentRecordEndDate || null == parentRecordEndDate){ // THIS SCENARIO SHOULD NOT HAPPEN BUT JUST IN CASE
			return true;
		}
		
		// CHECK if the parent record end date is 12-31-9999 (NOT END DATED) or both the records are before 12-31-9999 (END DATED)
		final boolean isParentEffectiveOrBothEndDated =  (( null == parentRecordEndDate ) || lastEndDate.equals(parentRecordEndDate)) 
											|| 
										 ( lastEndDate.after(parentRecordEndDate) && lastEndDate.after(currentRecordEndDate));
		return isParentEffectiveOrBothEndDated;
		
		
	}
	

	private boolean isParentRecordActiveOrBothInactive(Adaptation parentRecord, ValueContext context){
		
		final String currentRecordStatus = (String) context.getValue(getParent(statusField));	
		final String parentRecordStatus =  parentRecord.getString(getPath(statusField));
		
		if(null == currentRecordStatus || null == parentRecordStatus){ // THIS SCENARIO SHOULD NOT HAPPEN BUT JUST IN CASE
			return true;
		}
	
		final boolean isParentRecordActiveOrBothInactive = DSMTConstants.ACTIVE.equalsIgnoreCase(parentRecordStatus) || ( DSMTConstants.INACTIVE.equalsIgnoreCase(currentRecordStatus) && DSMTConstants.INACTIVE.equalsIgnoreCase(parentRecordStatus));
		return isParentRecordActiveOrBothInactive;
		
		
	}
	
	
	
	
	private String endDateField = "./ENDDT";
	private String statusField = "./EFF_STATUS";
	
	private static Date lastEndDate = new Date(253402232400000L);
	
	
	private Path getParent(final String fieldName) {
		
		return Path.parse(DSMTConstants.DOT + fieldName);
	}


	private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}



	


	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void setup(ConstraintContext arg0) {
		// TODO Auto-generated method stub
		
	}



	

	

}
