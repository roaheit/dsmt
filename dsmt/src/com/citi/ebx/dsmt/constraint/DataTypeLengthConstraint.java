package com.citi.ebx.dsmt.constraint;

import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.service.LoggingCategory;

public class DataTypeLengthConstraint implements ConstraintOnTableWithRecordLevelCheck{

	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	@Override
	public void checkTable(ValueContextForValidationOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
		
		final ValueContext value = context.getRecord();
		context.removeRecordFromMessages(value);
		
		String DATA_TYPE =(String) value.getValue(DSMTConstants.DATA_TYPE);
		String PL_FLAG =(String) value.getValue(DSMTConstants.PL_FLAG);
		LOG.info("DATA_TYPE == "+DATA_TYPE + "PL_FLAG ==" +PL_FLAG);
		
		if("L".equalsIgnoreCase(PL_FLAG) && DATA_TYPE.length() >2){
			context.addMessage(UserMessage
					.createError("Data type length for leaf record can not be more than 2 char"));
		}
	}

}
