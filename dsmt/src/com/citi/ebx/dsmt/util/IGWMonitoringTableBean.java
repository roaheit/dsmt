package com.citi.ebx.dsmt.util;

import java.util.ArrayList;
import java.util.Date;

import com.citi.ebx.workflow.vo.DsmtGocVO;

public class IGWMonitoringTableBean {
	
	private String requestID;
	private String requestType;
	private String childDataSpaceID;
	private Date requestDate;
	private String requestBy;
	private String requestStatus;
	private String gocRecordCount;
	private ArrayList<DsmtGocVO> dsmtGocVOs;
	private String requestorComment;
	private String operation;
	
	private String totalMod;
	private String updatedGOCCount;
	private String newNonGOCCount;
	private String updatedNonGOCCount;
	private String maintenanceType;
	private String functionalID;
	
	
	public String getFunctionalID() {
		return functionalID;
	}
	public void setFunctionalID(String functionalID) {
		this.functionalID = functionalID;
	}
	public String getTotalMod() {
		return totalMod;
	}
	public void setTotalMod(String totalMod) {
		this.totalMod = totalMod;
	}
	public String getUpdatedGOCCount() {
		return updatedGOCCount;
	}
	public void setUpdatedGOCCount(String updatedGOCCount) {
		this.updatedGOCCount = updatedGOCCount;
	}
	public String getNewNonGOCCount() {
		return newNonGOCCount;
	}
	public void setNewNonGOCCount(String newNonGOCCount) {
		this.newNonGOCCount = newNonGOCCount;
	}
	public String getUpdatedNonGOCCount() {
		return updatedNonGOCCount;
	}
	public void setUpdatedNonGOCCount(String updatedNonGOCCount) {
		this.updatedNonGOCCount = updatedNonGOCCount;
	}
	/**
	 * @return the requestID
	 */
	public String getRequestID() {
		return requestID;
	}
	/**
	 * @param requestID the requestID to set
	 */
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}
	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	/**
	 * @return the childDataSpaceID
	 */
	public String getChildDataSpaceID() {
		return childDataSpaceID;
	}
	/**
	 * @param childDataSpaceID the childDataSpaceID to set
	 */
	public void setChildDataSpaceID(String childDataSpaceID) {
		this.childDataSpaceID = childDataSpaceID;
	}
	/**
	 * @return the requestDate
	 */
	public Date getRequestDate() {
		return requestDate;
	}
	/**
	 * @param requestDate the requestDate to set
	 */
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	/**
	 * @return the requestBy
	 */
	public String getRequestBy() {
		return requestBy;
	}
	/**
	 * @param requestBy the requestBy to set
	 */
	public void setRequestBy(String requestBy) {
		this.requestBy = requestBy;
	}
	/**
	 * @return the requestStatus
	 */
	public String getRequestStatus() {
		return requestStatus;
	}
	/**
	 * @param requestStatus the requestStatus to set
	 */
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	/**
	 * @return the gocRecordCount
	 */
	public String getGocRecordCount() {
		return gocRecordCount;
	}
	/**
	 * @param gocRecordCount the gocRecordCount to set
	 */
	public void setGocRecordCount(String gocRecordCount) {
		this.gocRecordCount = gocRecordCount;
	}
	/**
	 * @return the dsmtGocVOs
	 */
	public ArrayList<DsmtGocVO> getDsmtGocVOs() {
		return dsmtGocVOs;
	}
	/**
	 * @param dsmtGocVOs the dsmtGocVOs to set
	 */
	public void setDsmtGocVOs(ArrayList<DsmtGocVO> dsmtGocVOs) {
		this.dsmtGocVOs = dsmtGocVOs;
	}
	/**
	 * @return the requestorComment
	 */
	public String getRequestorComment() {
		return requestorComment;
	}
	/**
	 * @param requestorComment the requestorComment to set
	 */
	public void setRequestorComment(String requestorComment) {
		this.requestorComment = requestorComment;
	}
	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	
	public String getMaintenanceType() {
		return maintenanceType;
	}
	public void setMaintenanceType(String maintenanceType) {
		this.maintenanceType = maintenanceType;
	}
	@Override
	public String toString() {
		return "requestID:"+requestID +"requestType:"+requestType+"childDataSpaceID:"+childDataSpaceID+"requestDate:"+requestDate+"requestBy:"+requestBy+
			"requestStatus:"+requestStatus+	"gocRecordCount:"+gocRecordCount+"requestorComment:"+requestorComment+"operation:"+operation;
		
	}
	
	
}
