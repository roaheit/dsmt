package com.citi.ebx.dsmt.util;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.orchestranetworks.ps.util.DSMTPropertyHelper;

public class MergeReadme {

	private static final Logger logger = Logger.getLogger("com.citi.ebx.dsmt.util.MergeReadMe");
	
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();

	private static final String DOT = ".";

	
	public void runMergeReadMe(){
		
		final String env = propertyHelper.getProp("dsmt.environment");
		
		new MergeReadme().mergeReadMeFiles
			(	 		bundle.getString(env + DOT +"dsmt1.readme.file.path"), 
						bundle.getString(env + DOT + "dsmt2.readme.file.path"),
						bundle.getString(env + DOT + "dsmt.readme.output.file.path"), 
						bundle.getString(env + DOT + "dsmt.readme2.output.file.path"));
	}
	
	protected  void mergeReadMeFiles(final String dsmt1ReadMePath, final String dsmt2ReadMePath, final String dsmtReadMeOutputPath, final String dsmtReadMe2OutputPath) {
		
		logger.log(Level.INFO, "Merge readme started..");
		logger.log(Level.INFO, "DSMT2 Readme = " + dsmt2ReadMePath);
		
		try {
			File dsmt1ReadMe = new File(dsmt1ReadMePath);
			File dsmt2ReadMe = new File(dsmt2ReadMePath);
			
			logger.log(Level.INFO, "DSMT1 Readme is being read = " + dsmt1ReadMePath);		
			List<String> readMeList1 = null;
			Integer readMe1Size = 0;
			Integer readMe2Size = 0;
			
			try {
				readMeList1 = getReadMeList (new Scanner(dsmt1ReadMe));
				readMe1Size = readMeList1.size() - 10;
			} catch (Exception e) {
				logger.log(Level.SEVERE,  "DSMT1 read me file not found at " + dsmt1ReadMePath + " Program will now create only readme and readme2 file based on DSMT2 readme only.");
			}
			logger.log(Level.INFO, "DSMT2 Readme is being read = " + dsmt2ReadMePath);		
			
			List<String> readMeList2 = getReadMeList (new Scanner(dsmt2ReadMe));

			/* Set header size if dsmt1 readme is found */
			if(null != readMeList1 && 0 < readMeList1.size()){
				readMe2Size = readMeList2.size() - 11;
			}
			else { /** readme size to be same as the DSMT2 readme if no DSMT1 readme file is found */
				readMe2Size = readMeList2.size();
			}
			
			logger.log(Level.INFO,  "Total rows in readme file = " + readMe1Size);
			
			generateOutputFiles( readMeList2, readMe2Size, dsmtReadMeOutputPath, dsmtReadMe2OutputPath);
			// generateOutputFile2(readMeList1, readMeList2, readMe1Size, readMe2Size, dsmtReadMe2OutputPath );
			
			logger.log(Level.INFO, "DSMT Merged Readme 1 file path = " + dsmtReadMeOutputPath);
			logger.log(Level.INFO, "DSMT Merged Readme 2 file path = " + dsmtReadMe2OutputPath);
			logger.log(Level.INFO, "Merge readme finished successfully..");
			
		} catch (Exception e) {
			
			System.err.println(e);
			logger.log(Level.INFO, "Merge readme terminated with following exception .." + e + " Exception message " + e.getMessage());
			e.printStackTrace();
			
		}
		

	}
	
	
	protected  void generateReadMeFiles(final String dsmt2ReadMePath, final String dsmtReadMeOutputPath, final String dsmtReadMe2OutputPath) {
		
		logger.log(Level.INFO, "Merge readme started..");
		logger.log(Level.INFO, "DSMT2 Readme = " + dsmt2ReadMePath);
		
		try {
	//		File dsmt1ReadMe = new File(dsmt1ReadMePath);
			File dsmt2ReadMe = new File(dsmt2ReadMePath);
			
	//		logger.log(Level.INFO, "DSMT1 Readme is being read = " + dsmt1ReadMePath);		
	//		List<String> readMeList1 = null;
	//		Integer readMe1Size = 0;
			Integer readMe2Size = 0;
			
	/*		try {
				readMeList1 = getReadMeList (new Scanner(dsmt1ReadMe));
				readMe1Size = readMeList1.size() - 10;
			} catch (Exception e) {
				logger.log(Level.SEVERE,  "DSMT1 read me file not found at " + dsmt1ReadMePath + " Program will now create only readme and readme2 file based on DSMT2 readme only.");
			}
			logger.log(Level.INFO, "DSMT2 Readme is being read = " + dsmt2ReadMePath);		
	*/		
			List<String> readMeList2 = getReadMeList (new Scanner(dsmt2ReadMe));

			/* Set header size if dsmt1 readme is found */
			readMe2Size = readMeList2.size();
			
			
			logger.log(Level.INFO,  "Total rows in readme file = " + readMe2Size);
			
			generateOutputFiles(readMeList2, readMe2Size, dsmtReadMeOutputPath, dsmtReadMe2OutputPath);
			// generateOutputFile2(readMeList1, readMeList2, readMe1Size, readMe2Size, dsmtReadMe2OutputPath );
			
			logger.log(Level.INFO, "DSMT Merged Readme 1 file path = " + dsmtReadMeOutputPath);
			logger.log(Level.INFO, "DSMT Merged Readme 2 file path = " + dsmtReadMe2OutputPath);
			logger.log(Level.INFO, "Merge readme finished successfully..");
			
		} catch (Exception e) {
			
			System.err.println(e);
			logger.log(Level.INFO, "Merge readme terminated with following exception .." + e + " Exception message " + e.getMessage());
			e.printStackTrace();
			
		}
		

	}
	
	
	private void generateOutputFiles(List<String> readMeList2, Integer readMe2Size, String dsmtReadMeOutputPath, String dsmtReadMe2OutputPath) throws Exception {

		logger.log(Level.INFO, "Readme 1 file generation started...");
		
		File output1 = new File(dsmtReadMeOutputPath);
		File output2 = new File(dsmtReadMe2OutputPath);
	
		output1.setReadable(true, false);
		output1.setWritable(true, true);
		
		output2.setReadable(true, false);
		output2.setWritable(true, true);
		
	
		PrintWriter printer1 = new PrintWriter(output1);
		PrintWriter printer2 = new PrintWriter(output2);
		
		
		// Print readMe file 2 first
		int lineCount = 0;
		for (String string : readMeList2) {
		
			logger.log(Level.INFO, string + NEW_LINE);
			printer1.write(string + NEW_LINE);
			printer2.write(removeFileExtension(string) + NEW_LINE);
			lineCount ++;
			
		}
		
		printer1.flush();
		printer2.flush();
		
		printer1.close();
		printer2.close();
		
		logger.log(Level.INFO, "Readme file generation ended...");
		
	}
	
/*	private void generateOutputFiles(List<String> readMeList1,	List<String> readMeList2, Integer readMe1Size, Integer readMe2Size, String dsmtReadMeOutputPath, String dsmtReadMe2OutputPath) throws Exception {

		final String env =  propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
		
		logger.log(Level.INFO, "Readme 1 file generation started...");
		
		File output1 = new File(dsmtReadMeOutputPath);
		File output2 = new File(dsmtReadMe2OutputPath);
	
		output1.setReadable(true, false);
		output1.setWritable(true, true);
		
		output2.setReadable(true, false);
		output2.setWritable(true, true);
		
		if(null != readMeList1){
		
			*//** Start - Create backup of DSMT1 README file *//*
			File backupDSMT1Readme = new File(bundle.getString(env + DOT + "dsmt1.readme.backup.path"));
			PrintWriter backUpPrinter = new PrintWriter(backupDSMT1Readme);
			
			for (String string : readMeList1) {
				backUpPrinter.write(string + NEW_LINE);
			}
			
			backUpPrinter.flush();
			backUpPrinter.close();
			logger.log(Level.INFO, "Backup created for DSMT1 read me file at location = " + backupDSMT1Readme);
			*//** End  - backup of DSMT1 README file *//*
		}
		
		PrintWriter printer1 = new PrintWriter(output1);
		PrintWriter printer2 = new PrintWriter(output2);
		
		if(null != readMeList1){
			readMeList2.addAll(readMeList1);
		}
		
		// Print readMe file 2 first
		int lineCount = 0;
		for (String string : readMeList2) {
		
			if(lineCount == readMe2Size + readMe1Size + 20 ){
				
				String updateString = replaceWordFromSring(string, String.valueOf(readMe1Size + readMe2Size), 1);
				logger.log(Level.INFO, updateString);
				printer1.write(updateString + NEW_LINE);
				printer2.write(removeFileExtension(updateString) + NEW_LINE);
				
			}
			else if(lineCount < readMe2Size + 8 || lineCount > readMe2Size + 18){
		
				logger.log(Level.INFO, string + NEW_LINE);
				printer1.write(string + NEW_LINE);
				printer2.write(removeFileExtension(string) + NEW_LINE);
			}
			
			lineCount ++;
			
		}
		
		printer1.flush();
		printer2.flush();
		
		printer1.close();
		printer2.close();
		
		logger.log(Level.INFO, "Readme 1 file generation ended...");
		
	}
	*/
	private String removeFileExtension(String updateString) {
	
		if(updateString.contains(FILE_EXTN)){
			updateString = updateString.replace(FILE_EXTN, REPLACE_STRING);
	//		System.out.println(updateString);
		}
		return updateString;
	}
	
	/**
	 * replace dsmt2 number of rows with total number of rows 
	 * @param string
	 * @param newWord
	 * @param wordLocation
	 * @return String with corrected number of rows
	 */
	private String replaceWordFromSring(String string, String newWord, int wordLocation) {
	
		int wordCount = 1;
		final StringTokenizer st = new StringTokenizer(string);
		while (st.hasMoreTokens()) {
			
			/** replace dsmt2 length with total length of the file */
			if(wordCount == wordLocation){
				
				String wordToBeReplaced = st.nextToken();
				string = string.replaceAll(wordToBeReplaced, newWord);
				
				logger.log(Level.INFO, string);
				return string;
			}
			else{
				wordCount ++;
			}
		}
		
		return string;
	}

	/** 
	 * Read file and return a List of Strings
	 * @param scanner
	 * @return
	 */
	private List<String>  getReadMeList(Scanner scanner){
		
		final List<String>readMeList = new ArrayList<String>();
		
		int lineNumber = 0;
		
		while (scanner.hasNextLine()) {
			try {
				String string = (String) scanner.nextLine();
				lineNumber ++;
				readMeList.add(string);
			
			} catch (NoSuchElementException nse) {
			 // do nothing
			}
		}
		return readMeList;
		
	}
	
	private static final String NEW_LINE = "\n";
	private static final String FILE_EXTN = ".dat";
	private static final String REPLACE_STRING = "    ";
 
	
	public static void main(String[] args) {

			final String env = "local";
			new MergeReadme().mergeReadMeFiles(bundle.getString(env + DOT +"dsmt1.readme.file.path"), 
					bundle.getString(env + DOT + "dsmt2.readme.file.path"), bundle.getString(env + DOT + "dsmt.readme.output.file.path"), bundle.getString(env + DOT + "dsmt.readme2.output.file.path"));

	}
	
	private static final ResourceBundle bundle = ResourceBundle.getBundle("com.citi.ebx.dsmt.util.ApplicationResources");

}
