package com.citi.ebx.dsmt.util;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.ResourceBundle;

import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;

public class DSMTNotificationService {

	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	private static final String FILE_EXTENSION = ".txt";
	static HashMap<String, String> maps = new HashMap<String, String>();

	
	private static final ResourceBundle bundle = ResourceBundle.getBundle("com.citi.ebx.dsmt.util.NotificationResources");
	
	public static void main(String[] args) {
		
		
		try {
			LOG.info (DSMTNotificationService.notifyEmailID("rohit4.kumar@citi.com", "DSMT2 Notification", "This is a test message.", true));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private static String getFileName(){
		//date format changed for DSMT Approval workflow mail notification 
		final SimpleDateFormat sdf =  new SimpleDateFormat("yyyyMMddHHmmssSSSSSSS");
		return "DSMT2_" + sdf.format(Calendar.getInstance().getTime()) + FILE_EXTENSION;
	}
	
	/**
	 * 
	 * @param emailID - email id of the person to whom notification has to be sent.
	 * @param subject - subject of the email
	 * @param message - The message content that has to be communicated.
	 * @param dsmtLinkToBeShown - trailer row 
	 * @return - filename that is created on the directory.
	 * @throws Exception
	 */
	public static String notifyEmailID(final String soeID, final String subject, final String message, final boolean dsmtLinkToBeShown){
		
		LOG.info(message);
		
		PrintWriter printer = null;
		final String fileName = getFileName();
		
		try{
			
			final String env = propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT); // "local" ; 
			
			printer = new PrintWriter(bundle.getString(env + DSMTConstants.DOT	+ "output.directory") + fileName );
			
			printer.write(soeID + DSMTConstants.NEXT_LINE);
			if(! "prod".equalsIgnoreCase(env) && null != env){
				printer.write("[" + env.toUpperCase() + "] " + subject + DSMTConstants.NEXT_LINE);
			}else{
				printer.write(subject + DSMTConstants.NEXT_LINE);
			}
			
				printer.write(message + DSMTConstants.NEXT_LINE);
			
			if(dsmtLinkToBeShown){
			
				printer.write(DSMTConstants.NEXT_LINE + "Please login to DSMT2 application and go to Data Workflows section to complete the task." );
			}
			
			printer.flush();
			printer.close();
						
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			if(null != printer){
				printer.close();
				printer.flush();
				printer = null;
			}
		}
			
		return fileName;
	}
	
	
	/**
	 * 
	 * @param distributionList
	 * @param subject
	 * @param messageContent
	 * @return
	 */
	public static String notifyEmailID(final String distributionList, final String subject, final String messageContent){
		
		LOG.info(messageContent);
		
		PrintWriter printer = null;
		final String fileName = getFileName();
		
		try{
			
			final String env = propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT); // "local" ; 
			
			printer = new PrintWriter(bundle.getString(env + DSMTConstants.DOT	+ "output.directory") + fileName );
			
			printer.write(distributionList + DSMTConstants.NEXT_LINE);
			if(! "prod".equalsIgnoreCase(env) && null != env){
				printer.write("[" + env.toUpperCase() + "] " + subject + DSMTConstants.NEXT_LINE);
			}else{
				printer.write(subject + DSMTConstants.NEXT_LINE);
			}
			
				printer.write(messageContent + DSMTConstants.NEXT_LINE);
			
			
			
			printer.flush();
			printer.close();
						
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			if(null != printer){
				printer.close();
				printer.flush();
				printer = null;
			}
		}
			
		return fileName;
	}
	
	/**
	 * 
	 * @param emailID - email id of the person to whom notification has to be sent.
	 * @param subject - subject of the email
	 * @param message - The message content that has to be communicated.
	 * @param dsmtLinkToBeShown - trailer row 
	 * @param map - value of map for Approval WF
	 * @return - filename that is created on the directory.
	 * @throws Exception
	 */
	public static String notifyEmailID(final String soeID, final String subject, final String message, final boolean dsmtLinkToBeShown, HashMap<String, String> map){
		
		LOG.info(message);
		
		PrintWriter printer = null;
		final String fileName = getFileName();
		
		try{
			
			final String env = propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT); // "local" ; 
			printer = new PrintWriter(bundle.getString(env + DSMTConstants.DOT	+ "output.directory") + fileName );

			
			printer.write(soeID + DSMTConstants.NEXT_LINE);
			if(! "prod".equalsIgnoreCase(env) && null != env){
				printer.write("[" + env.toUpperCase() + "] " + subject + DSMTConstants.NEXT_LINE);
			}else{
				printer.write(subject + DSMTConstants.NEXT_LINE);
			}
				printer.write("Request ID: " +map.get("RequestID") + DSMTConstants.NEXT_LINE);
				printer.write("Table Name: " +map.get("TableName") + DSMTConstants.NEXT_LINE);
				printer.write("Requestor: " +map.get("Requestor") + DSMTConstants.NEXT_LINE);
				printer.write(message + DSMTConstants.NEXT_LINE);
			
			if(dsmtLinkToBeShown){
			
				printer.write(DSMTConstants.NEXT_LINE + "Please login to DSMT2 application and go to Data Workflows section to complete the task." );
			}
			
			printer.flush();
			printer.close();
						
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			if(null != printer){
				printer.close();
				printer.flush();
				printer = null;
			}
		}
			
		return fileName;
	}
	
	
	
}
