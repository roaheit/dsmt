package com.citi.ebx.dsmt.util;

import java.util.ResourceBundle;

import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.schema.Path;

public interface DSMTConstants {
	
	
	public static final ResourceBundle bundle = ResourceBundle.getBundle("com.citi.ebx.dsmt.util.ApplicationResources");
	public static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	
	public Path creationUserFieldPath = Path.parse("./CREATED_OPERID");
	public Path creationDateFieldPath = Path.parse("./ORIG_DTTM");
	public Path lastUpdateUserFieldPath = Path.parse("./LASTUPDOPRID");
	public Path lastUpdateDateFieldPath = Path.parse("./CHNG_DTTM");
	public Path effDateFieldPath = Path.parse("./EFFDT");
	public Path endDateFieldPath = Path.parse("./ENDDT");
	public Path statusFieldPath = Path.parse("./EFF_STATUS");
	public Path setId=Path.parse("./SETID");
	public Path requestedByFieldPath = Path.parse("./Requested_by");
	public Path requestedDateFieldPath = Path.parse("./Requested_Date");
	
	public static final String DIRECTUPLOAD = "DirectUpload";
	
	
	public static final String ADMIN_USER = "admin";
	public static final String ACTIVE = "A";
	public static final String INACTIVE = "I";
	
	public static final String PARENT = "P";
	public static final String LEAF = "L";
	
	public static final String EBX_ACCOUNT_WF_USER = "CWMACCT";
	
	public static final String DSMT_ENVIRONMENT ="dsmt.environment";
	public static final String BLANK_STRING = "";
	public static final String SPACE = " ";
	
	public static final String EQUALS = "=";
	public static final String UNDERSCORE = "_";
	public static final String DOT = ".";
	public static final String FORWARD_SLASH = "/";
	public static final String NEXT_LINE =  "\n";
	
	public static final String NA=  "N/A";

	public static final String DSMT2_WORKFLOW_TYPE = "DSMT2Workflow";
	public static final String GOC_WORKFLOW_TYPE = "GOC";
	public static final String DSMT_WORKFLOW_MAINTENANCE_ROLE = "DSMT_WORKFLOW_MAINTENANCE_ROLE";
	public static final String WORKFLOW_ADMIN_DISTRIBUTION_LIST = "workflow.admin.distribution.list";
	public static final String LATAM_WORKFLOW_ADMIN_DISTRIBUTION_LIST = "latam.workflow.admin.distribution.list";
	public static final String ROSETTA_WORKFLOW_ADMIN_DISTRIBUTION_LIST = "rosetta.workflow.admin.distribution.list";
	public static final String DEFAULT_WORKFLOW_ADMIN_DISTRIBUTION_LIST = "default.workflow.admin.distribution.list";
	public static final String CWM_TERMINATOR="cwm.termination.distribution.list";
	public static final String PROJECT_NOTIFICATION="project.import.notification.distribution.list";
	public static final String CGM_PROJECT_NOTIFICATION="project.cgm.import.notification.distribution.list";
	public static final String IMS_PROJECT_NOTIFICATION="project.ims.import.notification.distribution.list";
	public static final String HARP = "HARP";
	public static final String DAILY = "DAILY";
	public static final String WEEKLY = "WEEKLY";
	public static final String MONTHLY = "MONTHLY";
	
	public static final String BULKREPORTS = "BULKREPORTS";
	public static final String GOC = "GOC";
	public static final String COSTCODE = "COSTCODE";
	
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	public static final String MAX_END_DATE = "9999-12-31";
	public static final String DAY_START_TIME_STAMP = " 00:00:00";
	public static final String DAY_END_TIME_STAMP = " 23:59:59";
	public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	public static final String DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	
	public static final String BD2REPORT = "BD2";
	public static final String BD3REPORT = "BD3";
	public static final String BD3REPORTEC = "BD+3";
	public static final String BD2REPORTEC = "BD-2";
	public static final String ACCTREPORTEC = "ACCT";
	public static final String OTHREPORTEC = "OTH";
	public static final String COMMA = ",";
	
	public static final String ERROR = "error";
	public static final String WARNING = "warning";
	public static final String INFO = "info";
	
	public static final String MAINTENANCEORDER = "GL_RE_P2P";
	public static final String GLSYSTEM = "GL";
	public static final String REPSYSTEM = "RE";
	public static final String P2PSYSTEM = "P2P";
	public static final String FMSREPORTHEADER = "HDR FMRCLST ";
	public static final String FMSREPORTRAILER = "TRL FMRCLST ";
	public static final String FMSREPORTNAME = "FMS_";
	public static final String FMSREPORTDAT = "_DSMT2_costcode.dat";
	public static final String FMSREPORTCSV = "_DSMT2_costcode.csv";
	 
	public static final String BULKREPORTS_HEADERTRAILER="BULKREPORTS_HEADERTRAILER";
	public static final String TRAILER="T";
	public static final String PIPE_SEP="|";
	public static final String HEADER="H";
	public static final String DELETED_RECORD_PARAM = "deleted.record.param";
	public static final String ARCHIVE_DATA_SPACE_NAME = "DSMTArchive";
	public static final String ARCHIVE_DATA_SET_NAME = "DSMT2Hier";
	
	public static final String FUNCTIONAL="functional";
	
	public static final String BS_OB_INDICATOR="BS_OB_INDICATOR";
	public static final String Flip_Sign="Flip_Sign";
	public static final String BALANCE_FORWARD_INDICATOR="BALANCE_FORWARD_INDICATOR";
	public static final String REVALUATION_FLAG ="REVALUATION_FLAG";
	
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	
	public static final Path REPORT_NAME = Path.parse("./NAME");
	public static final Path REPORT_LOCATION = Path.parse("./LOCATION");
	public static final Path REPORT_ROW_COUNT = Path.parse("./ROW_COUNT");
	public static final Path REPORT_STATUS = Path.parse("./STATUS");
	public static final Path REPORT_TIMESTAMP = Path.parse("./TIMESTAMP");
	public static final Path REPORT_LASTUPDOPRID = Path.parse("./LASTUPDOPRID");
	public static final String DATASPACE_DSMT2Hier= "DSMT2Hier";
	public static final String DATASET_DSMT2Hier= "DSMT2Hier";
	public static final String DATASPACE_DSMT2= "DSMT2";
	//TODO

	public static final String DATASET_NEW_DSMT2="NEW_DSMT2";//DEV
	public static final String DATASET_NEW_DSMT2_UAT="OLD_DSMT2";//UAT

	//public static final String DATASET_NEW_DSMT2="OLD_DSMT2";//UAT

	//public static final String DATASET_NEW_DSMT2="NEW_DSMT2";//DEV 

	public static final String DATASET_NEW_DSMT2_PROD="DSMT2";//PROD
	//public static final String DATASET_NEW_DSMT2="DSMT2";//PROD

	public static final String DATASPACE_LA="LatinAmerica";
	public static final String DATASET_LA="LatinAmericaV1";//DEV
	public static final String DATASET_LA_UAT="LatinAmerica";//UAT
	public static final String DATASET_LA_PROD="LatinAmerica";//PROD
	
	
	
	public static final Path LATAM_REPORT_AUDIT_PATH=Path.parse("/root/DSMT_REPORT_AUDIT");
	public static final Path LATAM_CATALOG_TABLE_PATH=Path.parse("/root/DSMT_Catalog");	
	
	public static final Path REPORT_AUDIT_PATH=Path.parse("/root/GLOBAL_STD/DSMT_REPORT_AUDIT");
	public static final Path CATALOG_TABLE_PATH=Path.parse("/root/GLOBAL_STD/DSMT_Catalog");
	public static final Path CATALOG_TABLE_FILENAME=Path.parse("./C_OB_FILENAME");
	public static final Path CATALOG_TABLE_COUNT=Path.parse("./C_REC_CNT");
	public static final Path CATALOG_TABLE_PREV_COUNT=Path.parse("./C_PREC_CNT");
	public static final Path CATALOG_TABLE_VARIANCE=Path.parse("./C_VARIANCE");
	public static final String GOC_WF_DATASPACE="GOC_WF";
	public static final String GOC_WF_DATASET="GOC_WF";
	public static final String DSMT_WF_DATASPACE="DSMT_WF";
	public static final String DSMT_WF_DATASET="DSMT_WF";
	public static final Path DSMT_REPORTING_TABLE_PATH=Path.parse("/root/C_DSMT_WF_REPORTING");
	public static final Path GOC_REPORTING_REQ_TYPE=Path.parse("./REQUEST_TYPE");
	public static final Path GOC_REPORTING_REQ_DT=Path.parse("./REQ_DT");
	public static final Path GOC_REPORTING_TABLE_PATH=Path.parse("/root/C_GOC_WF_REPORTING");
	public static final Path GOC_REPORTING_DATASPACE_ID=Path.parse("./CHILD_DATASPACE_ID");
	public static final String INSERT="Insert";
	public static final String UPDATE="Update";
	public static final String REJECT="Reject";
	public static final String SUBMIT="Submit";
	public static final String DSMT_CONTENT_MANAGMENT="DSMT_CONTENT_MANAGMENT";
    public static final Path MRL_Account_Code =Path.parse("./MRL_Account_Code");
	public static final Path DATA_SET =Path.parse("./DATASET_NAME");
	public static final Path EXCHANGE_LISTED =Path.parse("./EXCHANGE_LISTED");
	public static final Path EXCHANGE =Path.parse("./EXCHANGES");
	public static final Path tableId =Path.parse("./TABLE_NAME");
	
	//R3ManualTagging
	public static final Path CBL_CD =Path.parse("./CBL_CD");
	public static final Path CO_CD =Path.parse("./CO_CD");
	public static final Path MLE_CD =Path.parse("./MLE_CD");
	public static final Path GF_CD =Path.parse("./GF_CD");
	
	public static final Path RECORD_COUNT = Path.parse("./REC_COUNT");
	
	
	public static final String DATASET_ROSETTA="LatinAmerica";//PROD
	public static final String DATASET_R3MANUAL="R3ManualTagging";
	
	public static final String DATASPACE_DSMT2_WF ="DSMT_WF";//PROD
	public static final String DATASET_DSMT2_WF= "DSMT_WF";
	
	public static final Path ADDITION = Path.parse("./ADDITION");
	public static final Path UPDATION = Path.parse("./UPDATION");
	public static final Path REACTIVATION = Path.parse("./REACTIVATION");
	public static final Path DEACTIVATION = Path.parse("./DEACTIVATION");
		
	public static final String ACTION_NO_CHANGE = "N";
	
	public static final String COMPLETED ="Completed";
	
	public static final String PENDING = "Pending";
	public static final String PENDING_STATUS = "Pending for Merge";
	
	public static final String REQUEST_STATUS = "Pending for approval with Approver Level: ";
	
	public Path count = null;

	// OOS
	public static final Path ORG_CHAIN =Path.parse("./ORG_CHAIN");
	public static final Path MS_ID =Path.parse("./MS_ID");
	public static final Path MG_ID =Path.parse("./MG_ID");
	public static final Path LEID =Path.parse("./LEID");
	public static final Path OOS_ID =Path.parse("./OOS_ID");
	public static final Path OOS_GROUP_ID =Path.parse("./OOS_GROUP_ID");
	
	
	
	public static final String AR_ADDITION = "A";
	public static final String AR_UPDATION = "U";
	public static final String AR_REACTIVATION = "R";
	public static final String AR_DEACTIVATION = "D";
	public static final String AR_NO_CHANGE = "N";
	public static final String ASSIGN = "Assign";
	public static final String DEFAULT_APPROVER = "DEFAULT";
	public static final int CONSTANT_LENGTH = 7;
	public static final String REQ_REJECT="Rejected";
	
	// DATA Type
	
	public static final Path DATA_TYPE =Path.parse("./DATA_TYPE");
	public static final Path PL_FLAG =Path.parse("./PL_FLAG");
	
	public static final String MSREPORT="MSREPORT";
	public static final String ACCOUNTING_MTX_WORKFLOW_TYPE = "AccountingWorkflow";
	
	public final String ACTIVE_ERROR_MESG = "Record is Active.";
	public final String CHILD_RECORD_ACTIVE_ERROR_MESG = "Selected Record has active child Record(s).";
	public final String GOC_REFERENCE = "Selected Record is used in GOC.";
}



