package com.citi.ebx.dsmt.util;

import java.rmi.RemoteException;

import CCS.RequestWSPortTypeProxy;

import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;

public class BPMWSUtil {
	
	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	
	/*public static void main(String[] args) {
		
		System.out.println(new BPMWSUtil().getRequestIDFromBPM("nc72931"));
	}*/
	
	public String getRequestIDFromBPM(String soeID) throws Exception{

		String response = null;
		String requestID = null;
		
		try {
			final RequestWSPortTypeProxy proxy = new RequestWSPortTypeProxy();
			
			String requestIDWSEndPoint = propertyHelper.getProp("bpm.requestid.webservice.url"); // "http://ccgfibpm10d.nam.nsroot.net:30120/teamworks/webservices/CCS/RequestWS.tws";
			LOG.info("bpm.requestid.webservice.url = " + requestIDWSEndPoint);
			LOG.info("bpm.dsmt.project.id = " + propertyHelper.getProp("bpm.dsmt.project.id"));
		
			proxy.setEndpoint(requestIDWSEndPoint);
			LOG.info("soeID"+soeID);
			
			response = proxy.create(18,  soeID);
			
			LOG.info("Request ID received from BPM : " + response);
			
			int startIndex = response.indexOf("CDATA[");
			
			int endIndex = response.indexOf("]]");
			
			requestID = response.substring(startIndex+6, endIndex);
			
		} catch (RemoteException e) {
			LOG.error("Remote Exception caught while communicating with CWM - " + e.getMessage());
			e.printStackTrace();
			throw new Exception("Remote Exception caught while communicating with CWM - " + e + " Please try after some time when Citi Workflow Management is up.");
		}
		catch (Exception e) {
			LOG.error("Exception caught" + e.getMessage());
			e.printStackTrace();
			throw new Exception("Exception caught while communicating with CWM - " + e + " Please try after some time when Citi Workflow Management is up.");
		}
		
		return requestID;
		
	}
}
