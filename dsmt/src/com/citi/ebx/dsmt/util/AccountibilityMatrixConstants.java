package com.citi.ebx.dsmt.util;

import java.util.ResourceBundle;

import com.orchestranetworks.ps.util.DSMTPropertyHelper;

public interface AccountibilityMatrixConstants {
	
	
	public static final ResourceBundle bundle = ResourceBundle.getBundle("com.citi.ebx.dsmt.util.ApplicationResources");
	public static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	
	public static final String DATASPACE_Accounting_Matrix= "Accounting_Matrix";
	public static final String DATASET_Accounting_Matrix= "Accounting_Matrix";
	public static final String Accountability_Matrix_Admin="Accounting_Matrix_Admin";
	public static final String Accountability_Matrix_ReadOnly="Accounting_Matrix_ReadOnly";
	public static final String ACCT_MATRIX_DL="cwm.termination.distribution.list";
	public static final String LEM_TYPE_LEM="LEM";
	public static final String ACTIVESTATUS="A";
	public static final String SEPARATOR="/";
	public static final String ACCT_MATRIX="Accounting_Matrix";
	public static final String GOC = "GOC";
	public static final String goc_desc = "goc_desc";
	public static final String msID = "MSID";
	public static final String mgID = "MGID";
	public static final String lvid = "LVID";
	public static final String func = "FUNC";
	public static final String BU = "BU";
	public static final String OU = "OU";	
	public static final String Region = "Region";
	public static final String DELEGATE = "Delegate";
	
}



