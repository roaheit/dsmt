

package com.citi.ebx.dsmt.util;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.citi.ebx.util.RosettaReportingPeriod;
import com.citi.ebx.util.RosettaUtil;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;


public class RosettaHeaderTrailerGenerator {

	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	private static final String UTF_8 = "UTF-8";

	private static final ResourceBundle bundle = ResourceBundle.getBundle("com.citi.ebx.dsmt.util.RosettaResources");
	
	private static final Logger htlogger = Logger
			.getLogger("com.citi.ebx.dsmt.util.RosettaHeaderTrailerGenerator");
	private static final String DOT = ".";
	RosettaUtil util;
	Repository repository;
	String dataSpace="Rosetta";
	String dataSet="Rosetta";
	String EFF_DATE;
	
	
	/**
	 * @return the repository
	 */
	public Repository getRepository() {
		return repository;
	}



	/**
	 * @param repository the repository to set
	 */
	public void setRepository(Repository repository) {
		this.repository = repository;
	}



	/**
	 * @return the dataSpace
	 */
	public String getDataSpace() {
		return dataSpace;
	}



	/**
	 * @param dataSpace the dataSpace to set
	 */
	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}



	/**
	 * @return the dataSet
	 */
	public String getDataSet() {
		return dataSet;
	}



	/**
	 * @param dataSet the dataSet to set
	 */
	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}



	/**
	 * @return the eFF_DATE
	 */
	public String getEFF_DATE() {
		return EFF_DATE;
	}



	/**
	 * @param eFF_DATE the eFF_DATE to set
	 */
	public void setEFF_DATE(String eFF_DATE) {
		EFF_DATE = eFF_DATE;
	}





	/**
	 * @author Pratik Gaikwad / Suman Yelluri
	 * @param args
	 */
	/*public static void main(String[] args) {
		
	
		new LatamHeaderTrailerGenerator().runHeaderTrailerGenerator();
		
	}*/
	
	public void runHeaderTrailerGenerator(DSMTExportBean dsmtExportBean, Repository repo){
		
		final String env = propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
		
		RosettaUtil util = new RosettaUtil();
		//util.getDateValue(repo);
		
		List<RosettaReportingPeriod> reportPeriod = new ArrayList<RosettaReportingPeriod>();
		reportPeriod = util.getDateValue(repo);
		
		htlogger.info( "Rosetta HT Generator started..");
		try {

			File input = new File(bundle.getString(env + DOT + "header.trailer.metadata.path"));
			int tokenNumber = 0;
			StringTokenizer st = null;
			List<String> tableLabel = new ArrayList<String>();
	//		List<String> outputName = new ArrayList<String>();
			List<String> headerOutputName = new ArrayList<String>();
			Scanner sc = new Scanner(input);
			int lineNumber = 0;
			List<String> filesToAddHeader = new ArrayList<String>();
			final  List<String> encodingList = new ArrayList<String>();

			while (sc.hasNextLine()) {
				String s = sc.nextLine();
				st = new StringTokenizer(s, ",");
				while (st.hasMoreTokens()) {
					tokenNumber++;
					String temp = st.nextToken();
					if (tokenNumber == 1) {
						filesToAddHeader.add(temp);
						
					}
					if (tokenNumber == 2) {
						tableLabel.add(temp);
						
					}
					if (tokenNumber == 3) {
						headerOutputName.add(temp);
					   
					}
					if (tokenNumber == 4) {
						encodingList.add(temp);
						
					}
					
				}
				tokenNumber = 0;
			}
			List<String> filnamn = new ArrayList<String>();
			File folder = new File(bundle.getString(env + DOT + "input.directory"));
			File[] listOfFiles = folder.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {

					filnamn.add(listOfFiles[i].getName());

				}
			}
			
			int i = 0, j = 0;
			
			for (i = 0; i < listOfFiles.length; i++) {
				for (j = 0; j < filesToAddHeader.size(); j++) {
					if (listOfFiles[i].getName().equalsIgnoreCase(
							filesToAddHeader.get(j))) {
						dsmtExportBean.getFileNamesAndCount().remove(filesToAddHeader.get(j));

						String sep = "|";
						String addChar = "L";
						String header = "H";
						String trailer = "T";
						
						Date reportDate = null ;
						Date efftDate  = null;
						
						Date currentDate = Calendar.getInstance().getTime();
						if("ManualTagging".equalsIgnoreCase(tableLabel.get(j)) ){
							for(RosettaReportingPeriod rptPrd : reportPeriod){
								if("M".equalsIgnoreCase(rptPrd.getReportingTable())){
									efftDate = rptPrd.getReprtingDate();
									reportDate = rptPrd.getReportingCOBDate();
								}
							}
						}else if ("UserPlan".equalsIgnoreCase(tableLabel.get(j))){
							for(RosettaReportingPeriod rptPrd : reportPeriod){
								if("U".equalsIgnoreCase(rptPrd.getReportingTable())){
									efftDate = rptPrd.getReprtingDate();
									reportDate = rptPrd.getReportingCOBDate();
								}
							}
						}else if("Reference_Files".equalsIgnoreCase(tableLabel.get(j))){
							for(RosettaReportingPeriod rptPrd : reportPeriod){
								if("R".equalsIgnoreCase(rptPrd.getReportingTable())){
									efftDate = rptPrd.getReprtingDate();
									reportDate = rptPrd.getReportingCOBDate();
								}
							}
						}
						
						
						
						if( reportDate == null){
							reportDate = currentDate;
						}

						if( efftDate == null){
						//changing below logic to handle nullpointer exception
						//	reportDate = currentDate;
							efftDate = currentDate;
						}
						
						File inputFile = new File(bundle.getString(env + DOT + "input.directory")
								+ filesToAddHeader.get(j));
						
						sc = new Scanner(inputFile);
						if (!UTF_8.equalsIgnoreCase(encodingList.get(j))) {
							sc = new Scanner(inputFile, encodingList.get(j));
						}
						
						
						File output = new File(bundle.getString(env + DOT + "output.directory")
								+ headerOutputName.get(j));
						PrintWriter printer = null;
						if (!UTF_8.equalsIgnoreCase(encodingList.get(j))) {
							htlogger.log(Level.INFO, "Charset being used for "
									+ headerOutputName.get(j) + " is :"
									+ encodingList.get(j));
							printer = new PrintWriter(output,
									encodingList.get(j));
						} else {
							printer = new PrintWriter(output);
						}

						
						printer.write(header + sep + tableLabel.get(j) + sep 
								+headerOutputName.get(j)+sep
								+ new SimpleDateFormat(
										"MM/dd/yyyy HH:mm:ss").format(Calendar
												.getInstance().getTime())+sep
								+new SimpleDateFormat(
								"MM/dd/yyyy").format(efftDate) + sep 
								+ new SimpleDateFormat(
										"MM/dd/yyyy").format(reportDate) );
						
						while (sc.hasNextLine()) {
							String s = sc.nextLine();
							printer.write("\n" + addChar + "" + sep + "" + s + "" );
				//			System.out.println(addChar + "" + sep + "" + s);
							lineNumber++;
						}
						printer.write("\n" + trailer + sep + lineNumber + "\n");
						dsmtExportBean.getFileNamesAndCount().put(output.getName(),lineNumber);
						lineNumber = 0;
						printer.flush();
						
						printer.close();
						
					}

				}
			}

			htlogger.info( "Rosetta HT Generator finished..");
			
		} catch (Exception e) {
			System.err.println("Exception Caught in HeaderTrailerGeneration: " + e);
			e.printStackTrace();
		} finally {

		}

	}
}
