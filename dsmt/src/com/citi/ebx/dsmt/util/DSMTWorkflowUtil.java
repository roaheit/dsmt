
package com.citi.ebx.dsmt.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DSMTWorkflowUtil {

	private static final Logger logger = Logger.getLogger("com.citi.ebx.dsmt.util.WorkfowUtil");
	
	public String generateWorkflowID(String dataSpace){
		
		logger.log(Level.INFO, "dataspace is = " + dataSpace);
		String workflowID =  String.valueOf(convertDataSpaceToRequestID(dataSpace));
		logger.log(Level.INFO,  "workflow ID = " + workflowID);
		return  workflowID;
		
	}
	
	private Long convertDataSpaceToRequestID(String dataspaceID){
	
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS");
		Date date = null;
		try {
		
			date = (Date)formatter.parse(dataspaceID);
			return date.getTime();
		
		} catch (ParseException e) {
			logger.log(Level.SEVERE, "parse exception while parsing dataspace id = " + dataspaceID);
		}
		
		return null;
		
	}
	
	public static void main(String[] args) {
		
		String dataSpaceID="2013-08-01T10:47:01.510";
		String workflowID = new DSMTWorkflowUtil().generateWorkflowID(dataSpaceID);
//		workflowID = workflowID.substring(workflowID.length()-9, workflowID.length());
		logger.log(Level.INFO, workflowID);
		
	//	System.out.println(Integer.parseInt("133536100"));
		// 1299776070207
	}
	
}
