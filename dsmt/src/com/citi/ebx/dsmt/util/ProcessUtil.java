package com.citi.ebx.dsmt.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProcessUtil {

	private static final Logger processUtilLogger = Logger.getLogger("com.citi.ebx.dsmt.util.ProcessUtil");

	
	public static void main(String[] args) {
		
		new ProcessUtil().executeBashScript("C:/Windows/System32/notepad.exe");
	}
	
	public  void executeBashScript(String shellScriptPath) {
     
		processUtilLogger.log(Level.INFO, "now executing script... " + shellScriptPath);
		try {
        	
        	
				Process proc = Runtime.getRuntime().exec(shellScriptPath);
				BufferedReader read = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				
            try {
                proc.waitFor();
            } catch (InterruptedException e) {
            	processUtilLogger.log(Level.SEVERE, "InterruptedException  = " + e + ", Exception message" + e.getMessage());
            }
            while (read.ready()) {
            	processUtilLogger.log(Level.SEVERE, read.readLine());
             }
            
            processUtilLogger.log(Level.INFO, "finished executing script... " + shellScriptPath);
            
        } catch (IOException e) {
        	processUtilLogger.log(Level.SEVERE, "IOException  = " + e + ", Exception message" + e.getMessage());
        }
        
    }
	
}
