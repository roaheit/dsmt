package com.citi.ebx.dsmt.util;

import java.util.ArrayList;
import java.util.Date;

import com.citi.ebx.workflow.vo.DsmtGocVO;

public class ApprovalMonitoringTableBean {
	
	private int requestID;
	private String requestType;
	private String childDataSpaceID;
	private Date requestDate;
	private String requestBy;
	private String requestStatus;
	private String gocRecordCount;
	private ArrayList<DsmtGocVO> dsmtGocVOs;
	private String requestorComment;
	private String operation;
	private String dataSet;
	private String tableUpdated;
	private String recordCount;
	private int addition;
	private int updation;
	private int reactivation;
	private int deactivation;
	private int ApproverLevel;
	private String approver;
	private boolean check=false;
	private String allocatedApprover;
	private String tablePath;
	
	
	
	public String getTablePath() {
		return tablePath;
	}
	public void setTablePath(String tablePath) {
		this.tablePath = tablePath;
	}
	public String getAllocatedApprover() {
		return allocatedApprover;
	}
	public void setAllocatedApprover(String allocatedApprover) {
		this.allocatedApprover = allocatedApprover;
	}

	public boolean isCheck() {
		return check;
	}
	public void setCheck(boolean check) {
		this.check = check;
	}
	public int getApproverLevel() {
		return ApproverLevel;
	}
	public void setApproverLevel(int ApproverLevel) {
		this.ApproverLevel = ApproverLevel;
	}
	public String getApprover() {
		return approver;
	}
	public void setApprover(String approver) {
		this.approver = approver;
	}
	public int getAddition() {
		return addition;
	}
	public void setAddition(int addition) {
		this.addition = addition;
	}
	public int getUpdation() {
		return updation;
	}
	public void setUpdation(int updation) {
		this.updation = updation;
	}
	public int getReactivation() {
		return reactivation;
	}
	public void setReactivation(int reactivation) {
		this.reactivation = reactivation;
	}
	public int getDeactivation() {
		return deactivation;
	}
	public void setDeactivation(int deactivation) {
		this.deactivation = deactivation;
	}
	/**
	 * @return the recordCount
	 */
	public String getRecordCount() {
		return recordCount;
	}
	/**
	 * @param recordCount the recordCount to set
	 */
	public void setRecordCount(String recordCount) {
		this.recordCount = recordCount;
	}
	/**
	 * @return the requestID
	 */
	public int getRequestID() {
		return requestID;
	}
	/**
	 * @param requestID the requestID to set
	 */
	public void setRequestID(int requestID) {
		this.requestID = requestID;
	}
	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}
	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	/**
	 * @return the childDataSpaceID
	 */
	public String getChildDataSpaceID() {
		return childDataSpaceID;
	}
	/**
	 * @param childDataSpaceID the childDataSpaceID to set
	 */
	public void setChildDataSpaceID(String childDataSpaceID) {
		this.childDataSpaceID = childDataSpaceID;
	}
	/**
	 * @return the requestDate
	 */
	public Date getRequestDate() {
		return requestDate;
	}
	/**
	 * @param requestDate the requestDate to set
	 */
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	/**
	 * @return the requestBy
	 */
	public String getRequestBy() {
		return requestBy;
	}
	/**
	 * @param requestBy the requestBy to set
	 */
	public void setRequestBy(String requestBy) {
		this.requestBy = requestBy;
	}
	/**
	 * @return the requestStatus
	 */
	public String getRequestStatus() {
		return requestStatus;
	}
	/**
	 * @param requestStatus the requestStatus to set
	 */
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	/**
	 * @return the gocRecordCount
	 */
	public String getGocRecordCount() {
		return gocRecordCount;
	}
	/**
	 * @param gocRecordCount the gocRecordCount to set
	 */
	public void setGocRecordCount(String gocRecordCount) {
		this.gocRecordCount = gocRecordCount;
	}
	/**
	 * @return the dsmtGocVOs
	 */
	public ArrayList<DsmtGocVO> getDsmtGocVOs() {
		return dsmtGocVOs;
	}
	/**
	 * @param dsmtGocVOs the dsmtGocVOs to set
	 */
	public void setDsmtGocVOs(ArrayList<DsmtGocVO> dsmtGocVOs) {
		this.dsmtGocVOs = dsmtGocVOs;
	}
	/**
	 * @return the requestorComment
	 */
	public String getRequestorComment() {
		return requestorComment;
	}
	/**
	 * @param requestorComment the requestorComment to set
	 */
	public void setRequestorComment(String requestorComment) {
		this.requestorComment = requestorComment;
	}
	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	public String getDataSet() {
		return dataSet;
	}
	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}
	
	/**
	 * @return the tableUpdated
	 */
	public String getTableUpdated() {
		return tableUpdated;
	}
	/**
	 * @param tableUpdated the tableUpdated to set
	 */
	public void setTableUpdated(String tableUpdated) {
		this.tableUpdated = tableUpdated;
	}
	@Override
	public String toString() {
		return "requestID:"+requestID +"requestType:"+requestType+"childDataSpaceID:"+childDataSpaceID+"requestDate:"+requestDate+"requestBy:"+requestBy+
			"requestStatus:"+requestStatus+	"gocRecordCount:"+gocRecordCount+"requestorComment:"+requestorComment+"operation:"+operation;
		
	}
	
	
}
