package com.citi.ebx.dsmt.util;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HeaderTailerGenerator {

	private static final String UTF_8 = "UTF-8";

	
		
	private static final Logger htlogger = Logger
			.getLogger("com.citi.ebx.dsmt.util.HeaderTrailerGenerator");

	
	private static final String sep = "|";
	static String addChar = "L";
	private static final String header = "H";
	private static final String trailer = "T";
	
	/**
	 * @author Pratik Gaikwad / Suman Yelluri
	 * @param args
	 */

	public void runHeaderTrailerGenerator(DSMTExportBean dsmtExportBean) {

		final String env = DSMTConstants.propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT); // "local" ; 
		
		String outputFilePath = dsmtExportBean.getOutputFilePath();
		final List<String> exportedFileList = dsmtExportBean.getFileNames();
		
		htlogger.log(Level.INFO, "HT Generator started.. for export config " + dsmtExportBean.getConfigID());
		try {

			// File input = new File(bundle.getString(env + DOT +
			// "header.trailer.metadata.path"));
			final File input = new File(DSMTConstants.bundle.getString(env + DOT
					+ "header.trailer.metadata.path"));

			int tokenNumber = 0;
			StringTokenizer st = null;
			final  List<String> tableLabel = new ArrayList<String>();
			final  List<String> outputName = new ArrayList<String>();
			final  List<String> headerOutputName = new ArrayList<String>();
			Scanner sc = new Scanner(input);
			int lineNumber = 0;
			final List<String> filesToAddHeader = new ArrayList<String>();
			final  List<String> encodingList = new ArrayList<String>();

			while (sc.hasNextLine()) {
				String s = sc.nextLine();
				st = new StringTokenizer(s, ",");
				while (st.hasMoreTokens()) {
					tokenNumber++;
					String temp = st.nextToken();
					if (tokenNumber == 1) {
						filesToAddHeader.add(temp);
						// System.out.println("filesToAddHeader : " + temp);
					}
					if (tokenNumber == 2) {
						outputName.add(temp);
						// System.out.println("outputName: " + temp);
					}
					if (tokenNumber == 3) {
						tableLabel.add(temp);
						// System.out.println("table Label: " + temp);
					}
					if (tokenNumber == 4) {
						headerOutputName.add(temp);
						// System.out.println("headerOutputName: " + temp);
					}
					if (tokenNumber == 5) {
						encodingList.add(temp);
						// System.out.println("headerOutputName: " + temp);
					}
				}
				tokenNumber = 0;
			}
			final List<String> filnamn = new ArrayList<String>();
			// final File folder = new File(bundle.getString(env + DOT	+ "input.directory"));
			final File folder = new File(outputFilePath); // this is the same path from where files need to be picked
			
			final File[] listOfFiles = folder.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {

					filnamn.add(listOfFiles[i].getName());
					// System.out.println(listOfFiles[i].getName());

				}
			}
			int i = 0, j = 0;

			// System.out.println("listOfFiles LENGTH = " + listOfFiles.length);
			// System.out.println("filesToAddHeader SIZE = " +
			// filesToAddHeader.size());

			for (i = 0; i < listOfFiles.length; i++) {
				for (j = 0; j < filesToAddHeader.size(); j++) {

					/*
					 * System.out.println("listOfFiles[" + i + "].getName() = "
					 * + listOfFiles[i].getName());
					 * System.out.println("filesToAddHeader.get(" + j + ") = " +
					 * filesToAddHeader.get(j));
					 */

					String currentFileName = filesToAddHeader.get(j);
					if (listOfFiles[i].getName().equalsIgnoreCase(filesToAddHeader.get(j)) && exportedFileList.contains(currentFileName)) {
						dsmtExportBean.getFileNamesAndCount().remove(currentFileName);

						

						final String timeStamp = new SimpleDateFormat(
								"MM/dd/yyyy HH:mm:ss").format(Calendar
								.getInstance().getTime());

						
						
						// final File inputFile = new File(bundle.getString(env + DOT	+ "input.directory")+ filesToAddHeader.get(j));
						
						if(! outputFilePath.endsWith(DSMTConstants.FORWARD_SLASH)){
							outputFilePath = outputFilePath + DSMTConstants.FORWARD_SLASH;
						}
						final File inputFile = new File( outputFilePath + filesToAddHeader.get(j));
						
						sc = new Scanner(inputFile);
						if (!UTF_8.equalsIgnoreCase(encodingList.get(j))) {
							sc = new Scanner(inputFile, encodingList.get(j));
						}
						// System.out.println(filesToAddHeader.get(j));
						// final File output = new File(bundle.getString(env + DOT	+ "output.directory") + tableLabel.get(j));
						final File output = new File(outputFilePath + tableLabel.get(j));
						
						output.setReadable(true, false);
						output.setWritable(true, true);

						PrintWriter printer = null;
						if (!UTF_8.equalsIgnoreCase(encodingList.get(j))) {
							htlogger.log(Level.INFO, "Charset being used for "
									+ outputName.get(j) + " is :"
									+ encodingList.get(j));
							printer = new PrintWriter(output,
									encodingList.get(j));
						} else {
							printer = new PrintWriter(output);
						}

						printer.write(header + sep + "" + outputName.get(j)
								+ "" + sep + "" + timeStamp + "" + sep + ""
								+ headerOutputName.get(j));
						while (sc.hasNextLine()) {
							String string = sc.nextLine();

							String inputFileName = filesToAddHeader.get(j);
							
							if (isParentNodeToBeCorrected(inputFileName)) {
								
								// htlogger.log(Level.INFO, "inputFileName = " + inputFileName);
								final String hierarchyRootOld = DSMTConstants.bundle.getString(inputFileName);
								
								if(string.contains(hierarchyRootOld)){
									string = modifyParentOfHiearchyRoot(string);
								}
								// System.out.println(string);
							}
							else if (isManSegOrManGeoOrFrsFile(inputFileName)){
								
								String startingKeyword = DSMTConstants.bundle.getString(inputFileName);
								if(string.startsWith(startingKeyword)){
									string = modifyString(string, 0, 1, 2, 3);
								}
							}else if (isAccountFiles(inputFileName)){
								
								String startingKeyword = DSMTConstants.bundle.getString(inputFileName);
								if(string.startsWith(startingKeyword)){
									string = modifyString(string,2, 6, 1, 5);
								}
							}else if(isOutBoundsNotContainingNulls(inputFileName)){
								string =  replaceNullWithEmptyString(string);
							}
							else if(isAccountsCVAFiles(inputFileName)){
								if(string.contains("400001"))
								string = modifyStringValue(string);
							}

							if("DSMTN_LV_RELATIONSHIP_bef_2.dat".equalsIgnoreCase(inputFileName)){
								addChar = "B";
							}
							
							if(isDataTypeFile(inputFileName)){
								final String hierarchyRootOld = DSMTConstants.bundle.getString(inputFileName);
								
								if(string.contains(hierarchyRootOld)){
									string = modifyParentofDataType(string);
								}
							}
							printer.write("\n" + addChar + "" + sep + ""
									+ string);
							// System.out.println(addChar + "" + sep + "" + s);
							lineNumber++;
						}

						printer.write("\n" + trailer + sep + lineNumber + "\n");
						dsmtExportBean.getFileNamesAndCount().put(output.getName(),lineNumber);
						
						lineNumber = 0;
						printer.flush();

						printer.close();

					}

				}
			}

			htlogger.log(Level.INFO, "HT Generator finished..  for export config " + dsmtExportBean.getConfigID());

		} catch (Exception e) {
			System.err.println("Exception Caught in HeaderTrailerGeneration: "
					+ e);
			htlogger.log(
					Level.SEVERE,
					"HT Generator TERMINATED  for export config " + dsmtExportBean.getConfigID() + " with exception message.."
							+ e.getMessage());
			e.printStackTrace();
		} finally {
				
			
		}
	}


	public static void main(String[] args) {
	
	/*String string= "PMF01|400010|NETINTERES|Net Interest Revenue (L2)|A|2015-05-01|400001|Revenues (L1)|6|P|PM18880|2015-05-21T12:39:12.000|PM18880|2015-07-01T11:27:18.758";
		String inputFileName = "DSMT_ACCT_PMF_CVA_bef_2.dat";
		HeaderTailerGenerator htgen = new HeaderTailerGenerator();

			
		if ("DSMT_ACCT_PMF_CVA_bef_2.dat".equalsIgnoreCase(inputFileName)){
			String acctType = "400001";
			int x=0;
			if(string.contains(acctType)){
				string = htgen.modifyStringValue(string);
				
			}
		}		
		System.out.println(string);*/
		System.out.println(new HeaderTailerGenerator().isOutBoundsNotContainingNulls("DSMT_GOC_DEFN_bef.txt"));
	}
	
	private boolean isOutBoundsNotContainingNulls(final String inputFileName){
		
		List<String> outBoundsNotContainingNullsList = new ArrayList<String>();
		outBoundsNotContainingNullsList.add("DSMT_GOC_DEFN_bef_2.txt");
		outBoundsNotContainingNullsList.add("DSMT_GOC_P2P_bef_2.txt");
		outBoundsNotContainingNullsList.add("DSMT_ACCOUNT_bef_2.dat");
		outBoundsNotContainingNullsList.add("DSMT_ACCT_bef_2.dat");
		outBoundsNotContainingNullsList.add("DSMT_STDGL_ACCOUNT_bef_2.txt");
		
		
		return outBoundsNotContainingNullsList.contains(inputFileName);
	}
	
	
	
	private String replaceNullWithEmptyString(String string) {
		
		return string.replace("||", "| |");
	}

	private boolean isManSegOrManGeoOrFrsFile(String inputFileName) {

		
		boolean isManSegOrManGeoOrFrsFile = false;
		
		if  ( 		"DSMT_MAN_SEG_HIER_bef_2.dat".equalsIgnoreCase(inputFileName)
				||  "DSMT_MAN_GEO_HIER_bef_2.dat".equalsIgnoreCase(inputFileName)
				||  "DSMT_FRS_BU_HIERARCHY_bef_2.dat".equalsIgnoreCase(inputFileName)
				||  "DSMT_FRS_OU_HY_REF_bef_2.dat".equalsIgnoreCase(inputFileName)
				||  "DSMT_FRS_OU_HIERARCHY_bef_2.dat".equalsIgnoreCase(inputFileName)
				||  "HARP_DSMT_FRS_BU_HIERARCHY_bef_2.dat".equalsIgnoreCase(inputFileName)
			) 
		{
			isManSegOrManGeoOrFrsFile = true;
		}

		return isManSegOrManGeoOrFrsFile;
		
	}
	
	private boolean isAccountFiles(String inputFileName) {

		
		boolean isAccountFiles = false;
		
		if  ( 		"DSMT_ACCT_HY_FDL_bef_2.txt".equalsIgnoreCase(inputFileName)
				||  "DSMT_ACCT_HY_MRL_bef_2.txt".equalsIgnoreCase(inputFileName)
				||  "DSMT_ACCT_HY_PMF_bef_2.txt".equalsIgnoreCase(inputFileName)
				||  "DSMT_ACCT_HY_PMF_FRS_bef_2.txt".equalsIgnoreCase(inputFileName)				
			) 
		{
			isAccountFiles = true;
		}

		return isAccountFiles;
		
	}
private boolean isAccountsCVAFiles(String inputFileName) {

		
		boolean isAccountFiles = false;
		
		if  ( 		"DSMT_ACCT_HY_PMF_CVA_bef_2.txt".equalsIgnoreCase(inputFileName)
				||  "DSMT_ACCT_PMF_CVA_bef_2.dat".equalsIgnoreCase(inputFileName)				
			) 
		{
			isAccountFiles = true;
		}

		return isAccountFiles;
		
	}


private boolean isDataTypeFile(String inputFileName) {

	
	boolean isDataTypeFile = false;
	
	if  ( 	 "DSMT_DATA_TYPE_HY_bef_2.txt".equalsIgnoreCase(inputFileName)
			|| "DSMT_DATA_TYPE_bef.txt".equalsIgnoreCase(inputFileName)
			|| "DSMT_DATA_TYPE_HIER_bef_2.txt".equalsIgnoreCase(inputFileName)				
		) 
	{
		isDataTypeFile = true;
	}

	return isDataTypeFile;
	
}

	/*public static void main(String[] args) {

		
			
	//	String outputFilePath = "/home/rs74652/EBXHome/ImportFiles/Phase3/Cycle13";
		
	//	System.out.println(outputFilePath);
		//  new HeaderTailerGenerator().runHeaderTrailerGenerator();
		HeaderTailerGenerator htgen = new HeaderTailerGenerator();
		
		System.out.println(htgen.modifyParentOfHiearchyRoot("PMF01||1100|2013-06-01|A||TOTAL_CUSTOMER|1|CW77508|2009-04-07-11.54.47.000000|CW77508|2009-04-07-11.56.09.000000"));
		System.out.println(htgen.modifyParentOfHiearchyRoot("PMF01||1|2014-07-01|A||Total Citi [L1]|1|JD90410|2014-07-03-05.59.52.000000|JD90410|2014-07-03-05.59.52.000000"));
	//	htgen.modifyManGeoOrManSegHiearchy("1|Total Citi [L1]|||1|2013-12-01|A|");
	}*/

	private String modifyParentOfHiearchyRoot(String string) {

		htlogger.log(Level.INFO,
				"Now replacing the string for hiearchy root requirement- "
						+ string);

		/** set keyword for handling null field */
		final String updatedString = string.replace("||", "|" + KEYWORD + "|");
		
		String returnString = "";
		final StringTokenizer stringTokenizer = new StringTokenizer(updatedString,
				DELIMITER);

		String currentToken = "";
		while (stringTokenizer.hasMoreElements()) {

			currentToken = (String) stringTokenizer.nextToken();

			if (KEYWORD.equalsIgnoreCase(currentToken)) {

				String currentAndNextToken = stringTokenizer.nextToken();
				currentToken = currentAndNextToken + DELIMITER
						+ currentAndNextToken;
			}

			if (stringTokenizer.hasMoreTokens()) {
				returnString = returnString + currentToken + DELIMITER;
			} else {
				returnString = returnString + currentToken;
			}

		}

		htlogger.log(Level.INFO,
				"Corrected the string based on hierarchy root requirement- "
						+ returnString);

		return returnString;

	}


	/**
	 * corrects an input string by copying the string at emptyCodeIndex with rootCodeIndex and emptyDescIndex with rootDescIndex.
	 * 
	 * @param inputString 		"1|Total Citi [L1]|||1|2013-12-01|A|"
	 * @param rootCodeIndex		0
	 * @param rootDescIndex		1
	 * @param emptyCodeIndex	2
	 * @param emptyDescIndex	3
	 * @return					"1|Total Citi [L1]|1|Total Citi [L1]|1|2013-12-01|A"
	 */
	private String modifyString(final String inputString, final int rootCodeIndex, final int rootDescIndex, final int emptyCodeIndex, final int emptyDescIndex){
		
		final String[] array = inputString.split("[|]");
		
		array[emptyCodeIndex] = array[rootCodeIndex];
		array[emptyDescIndex] = array[rootDescIndex];
		
	//	System.out.println(array);
		
		final StringBuffer sb = new StringBuffer();
		
		for (String string : array) {
			
			sb.append(string).append(DELIMITER);
		}
		
		final String returnString = sb.toString();
	
		return returnString.substring(0, returnString.length() - 1);
		
	}
	
private String modifyStringValue(final String inputString){
		
		
		
		String updatedValue = inputString.replace("Revenues (L1)", "Revenues Excluding CVA/FVA").replace("REVENUESL1", "REVXCVAFVA") ;
		
		
	
		return updatedValue;
		
	}
	
	
	private boolean isParentNodeToBeCorrected(String inputFileName) {

		boolean isParentNodeToBeCorrected = false;

		if ("DSMT_CUST_SEG_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_CHANNEL_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_PROD_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_FUNC_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_PROJ_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_MAN_GEO_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_MAN_SEG_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_FRS_OU_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_FRS_BU_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_CUST_SEG_HY_W_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_CHANNEL_HY_W_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_LV_HIER_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_LVID_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_PROD_ESP_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "HARP_DSMT_LVID_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "HARP_DSMT_FUNC_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "HARP_DSMT_FRS_BU_HY_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "HARP_DSMT_LV_HIER_bef_2.dat".equalsIgnoreCase(inputFileName)
				|| "DSMT_ALT_FUNC_HY_bef_2.txt".equalsIgnoreCase(inputFileName)
				
		) {
			isParentNodeToBeCorrected = true;
		}

		return isParentNodeToBeCorrected;
	}

	private static final String DOT = ".";

//	private static final String HIERARCHY_ROOT_OLD = "PMF01||1|";
	// private static final String HIERARCHY_ROOT_NEW = "PMF01|1|1|";

	private static final String KEYWORD = "_SPACE_";
	private static final String DELIMITER = "|";
	
	private String modifyParentofDataType(String row){
		String updatedString = row.replace("||", "|ALLDATATYPES|");
		updatedString = updatedString.replace("||", "|All Data Types|");
		
		return updatedString;
	}
	
}
