

package com.citi.ebx.dsmt.util;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.orchestranetworks.ps.util.DSMTPropertyHelper;


public class LatamHeaderTrailerGenerator {

	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();

	private static final ResourceBundle bundle = ResourceBundle.getBundle("com.citi.ebx.dsmt.util.LatamResources");
	private static final Logger htlogger = Logger.getLogger("com.citi.ebx.dsmt.util.LatamHeaderTailerGenerator");
	private static final String DOT = ".";
	
	/**
	 * @author Pratik Gaikwad / Suman Yelluri
	 * @param args
	 */
	/*public static void main(String[] args) {
		
	
		new LatamHeaderTrailerGenerator().runHeaderTrailerGenerator();
		
	}*/
	
	public void runHeaderTrailerGenerator(DSMTExportBean dsmtExportBean){
		
		final String env = propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
		
		
		htlogger.log(Level.INFO, "LATAM HT Generator started..");
		try {

			File input = new File(bundle.getString(env + DOT + "header.trailer.metadata.path"));
			int tokenNumber = 0;
			StringTokenizer st = null;
			List<String> tableLabel = new ArrayList<String>();
	//		List<String> outputName = new ArrayList<String>();
			List<String> headerOutputName = new ArrayList<String>();
			Scanner sc = new Scanner(input);
			int lineNumber = 0;
			List<String> filesToAddHeader = new ArrayList<String>();

			while (sc.hasNextLine()) {
				String s = sc.nextLine();
				st = new StringTokenizer(s, ",");
				while (st.hasMoreTokens()) {
					tokenNumber++;
					String temp = st.nextToken();
					if (tokenNumber == 1) {
						filesToAddHeader.add(temp);
						//System.out.println("filesToAddHeader : " + temp);
					}
					if (tokenNumber == 2) {
						tableLabel.add(temp);
						// System.out.println("table Label: " + temp);
					}
					if (tokenNumber == 3) {
						headerOutputName.add(temp);
					    // System.out.println("headerOutputName: " + temp);
					}
				}
				tokenNumber = 0;
			}
			List<String> filnamn = new ArrayList<String>();
			File folder = new File(bundle.getString(env + DOT + "input.directory"));
			File[] listOfFiles = folder.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {

					filnamn.add(listOfFiles[i].getName());
					// System.out.println(listOfFiles[i].getName());

				}
			}
			int i = 0, j = 0;

	//		System.out.println("listOfFiles = " + listOfFiles.length);
	//		System.out.println("filesToAddHeader = " + filesToAddHeader.size());
			
			for (i = 0; i < listOfFiles.length; i++) {
				for (j = 0; j < filesToAddHeader.size(); j++) {
					
				/*	System.out.println("listOfFiles[" + i + "].getName() = " +  listOfFiles[i].getName());
					System.out.println("filesToAddHeader.get(" + j + ") = " +  filesToAddHeader.get(j));
				*/	
					
					if (listOfFiles[i].getName().equalsIgnoreCase(
							filesToAddHeader.get(j))) {
						dsmtExportBean.getFileNamesAndCount().remove(filesToAddHeader.get(j));

						
						String sep = "|";
						String addChar = "10";
						String header = "00";
						String trailer = "99";
						// String time;
						
						String timeStamp = new SimpleDateFormat(
								"MM/dd/yyyy hh:mm:ss.mmmmmm aa").format(Calendar
								.getInstance().getTime());

						File inputFile = new File(bundle.getString(env + DOT + "input.directory")
								+ filesToAddHeader.get(j));
						
						sc = new Scanner(inputFile);
						File output = new File(bundle.getString(env + DOT + "output.directory")
								+ headerOutputName.get(j));
						PrintWriter printer = new PrintWriter(output);
						
						printer.write(header + sep + "" + tableLabel.get(j)  + "" + sep
								+ "" + timeStamp + sep);
						
						while (sc.hasNextLine()) {
							String s = sc.nextLine();
							printer.write("\n" + addChar + "" + sep + "" + s + "" + sep);
				//			System.out.println(addChar + "" + sep + "" + s);
							lineNumber++;
						}
						lineNumber=lineNumber+2;
						printer.write("\n" + trailer + sep + lineNumber + sep + "\n");
						dsmtExportBean.getFileNamesAndCount().put(output.getName(),lineNumber);
						lineNumber = 0;
						printer.flush();
						
						printer.close();
						
					}

				}
			}

			htlogger.log(Level.INFO, "LATAM HT Generator finished..");
			
		} catch (Exception e) {
			System.err.println("Exception Caught in HeaderTrailerGeneration: " + e);
			e.printStackTrace();
		} finally {

		}
	}
}
