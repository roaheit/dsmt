package com.citi.ebx.dsmt.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.Request;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class HierarchyUtil {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private static final Path PARENT_FK_PATH = Path.parse("./PARENT_ID");
	
	public HierarchyUtil() {
		super();
		
	}
	
	/**
	 * This stores all the records from the table for faster lookup.
	 * Key = the primary key of the record, Value = the record itself.
	 */
	public Map<PrimaryKey, Adaptation> allRecordsMap;
	
	public Map<PrimaryKey, Adaptation> getAllRecordsMap() {
		return allRecordsMap;
	}
	public void setAllRecordsMap(Map<PrimaryKey, Adaptation> allRecordsMap) {
		this.allRecordsMap = allRecordsMap;
	}
	/**
	 * This is a list of only the leaf records of the table, each one containing
	 * its primary key and level within the hierarchy (1 being the root).
	 */
	protected List<CachedLeafInfo> leaves;

	public void initLeaves() {
		LOG.debug("ProdTypeFlatTableExporter: initLeaves");
		leaves = new ArrayList<CachedLeafInfo>();
		final Set<PrimaryKey> allPKs = allRecordsMap.keySet();
		// Loop over each primary key and if it's a leaf,
		// add a new CachedLeafInfo to the leaves list
		for (PrimaryKey pk: allPKs) {
			final Adaptation adaptation = allRecordsMap.get(pk);
			if (isLeaf(adaptation)) {
				final int level = getLevel(adaptation);
				final CachedLeafInfo cachedLeafInfo = new CachedLeafInfo();
				cachedLeafInfo.pk = pk;
				cachedLeafInfo.level = level;
				leaves.add(cachedLeafInfo);
			}
		}
		// Sort the list of leaves, using the CachedLeafInfo's comparison method.
		Collections.sort(leaves);
	}
	
	// Determines if a record is a leaf (no other record has it as its parent)
	public boolean isLeaf(final Adaptation adaptation) {
		final String pk = adaptation.getOccurrencePrimaryKey().format();
		final Set<PrimaryKey> allPKs = allRecordsMap.keySet();
		// Loop through all primary keys
		for (PrimaryKey otherPK: allPKs) {
			final Adaptation otherAdaptation = allRecordsMap.get(otherPK);
			final String parentFK = otherAdaptation.getString(PARENT_FK_PATH);
			// If this other record has the specified record as its parent then
			// the specified record isn't a leaf so return false
			if (parentFK != null && pk.equals(parentFK)) {
				return false;
			}
		}
		// We looped through all the records and didn't find any that had
		// the specified record as its parent, so it's a leaf.
		return true;
	}
	
	// Get the level in the hierarchy for a record (with root being level 1)
	public int getLevel(final Adaptation adaptation) {
		int level = 1;
		// Start with the parent of the specified record and loop up through the hierarchy
		// until there is no parent
		for (String pk = adaptation.getString(PARENT_FK_PATH);
				pk != null && allRecordsMap.containsKey(PrimaryKey.parseString(pk));) {
			level++;
			final PrimaryKey parentPK = PrimaryKey.parseString(pk);
			final Adaptation parent = allRecordsMap.get(parentPK);
			pk = parent.getString(PARENT_FK_PATH);
		}
		return level;
	}
	
	public void initAllRecordsMap(final AdaptationTable table, final AdaptationFilter filter) {
		if (LOG.isDebug()) {
			LOG.debug("ProdTypeFlatTableExporter: initAllRecordsMap");
			LOG.debug("ProdTypeFlatTableExporter: table = " + table.getTablePath().format());
		}
		allRecordsMap = new HashMap<PrimaryKey, Adaptation>();
		// If you specify no predicate, it will request every record in the table
		// (that meets any filter you set)
		final Request request = table.createRequest();
		if (filter != null) {
			request.setSpecificFilter(filter);
		}
		final RequestResult reqRes = request.execute();
		try {
			// Loop through all the results and put each into the map
			for (Adaptation adaptation; (adaptation = reqRes.nextAdaptation()) != null;) {
				allRecordsMap.put(adaptation.getOccurrencePrimaryKey(), adaptation);
			}
		} finally {
			reqRes.close();
		}
	}
	/**
	 * A class that caches info about the leaf nodes
	 */
	public class CachedLeafInfo implements Comparable<CachedLeafInfo> {
		/**
		 * The primary key of the leaf record
		 */
		private PrimaryKey pk;
		
		public PrimaryKey getPk() {
			return pk;
		}

		public void setPk(PrimaryKey pk) {
			this.pk = pk;
		}

		public int getLevel() {
			return level;
		}

		public void setLevel(int level) {
			this.level = level;
		}

		/**
		 * The level of the leaf record in the hierarchy (with root level being 1)
		 */
		private int level;
		
		/**
		 * Overridden so that they can be ordered according to primary key
		 */
		@Override
		public int compareTo(CachedLeafInfo obj) {
			return pk.format().compareTo(obj.pk.format());
		}
	}

}
