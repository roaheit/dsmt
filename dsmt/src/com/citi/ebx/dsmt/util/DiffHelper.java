package com.citi.ebx.dsmt.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

public class DiffHelper {

	public static void diff(String folderPath, String fileName, String targetFolder, String targetfile) {

		File folder = new File(folderPath);

		if (folder.isDirectory()) {
			String fullFileName = folderPath + File.separator + fileName;
			File newFile = new File(fullFileName);
			if (newFile.exists()) {
				if(!new File(targetFolder).exists()){
					new File(targetFolder).mkdir();
				}
				File copiedFile = new File(targetFolder + File.separator + fileName);
				try {
					FileUtils.moveFile(newFile, copiedFile);
					//System.out.println("Moved file:" + fullFileName + " to "+targetFolder+" folder");
				} catch (Exception e) {
					e.printStackTrace();
				}

				String ext = fileName.substring(fileName.lastIndexOf("."), fileName.length());
				String onlyFileName = fileName.substring(0, fileName.lastIndexOf("."));
				String baseFileName = targetFolder + File.separator + onlyFileName + "_b" + ext;
				File baseFile = new File(baseFileName);
				if (copiedFile.exists() && baseFile.exists()) {
					HashSet<String> hashSetOne = new HashSet<String>();

					Map<String, String> hashUtil = new HashMap<String, String>();
					try {
						LineIterator itr1 = FileUtils.lineIterator(baseFile);
						LineIterator itr2 = FileUtils.lineIterator(copiedFile);
						while (itr1.hasNext()) {
							hashSetOne.add(createHashkey((String)itr1.next()));

						}
						LineIterator.closeQuietly(itr1);
						while (itr2.hasNext()) {
							String line = (String)itr2.next();
							String hashKey = createHashkey(line);

							hashUtil.put(hashKey, line);

						}
						LineIterator.closeQuietly(itr2);
						hashUtil.keySet().removeAll(hashSetOne);

						File diffFile = new File(targetfile);
						if(diffFile.exists()){
							diffFile.delete();
						}
						

						FileUtils.writeLines(diffFile, hashUtil.values());
						//System.out.println("Successfuly created file:"+diffFile.getCanonicalPath()+" with size:"+hashUtil.size());

						String archiveFolderName = targetFolder + "/archive";
						File archiveFolder = new File(archiveFolderName);
						String date = new SimpleDateFormat("MM_dd_yyyy").format(new Date(System.currentTimeMillis()));
						if (archiveFolder.exists()) {

							String newbaseFileName = archiveFolderName + File.separator + onlyFileName + "_" + date + ext;
							File newBaseFile = new File(newbaseFileName);
							if (newBaseFile.exists()) {
								newBaseFile.delete();
								try {
									FileUtils.moveFile(baseFile, newBaseFile);
									//System.out.println("Moved file:" + onlyFileName + "_b" + ext + " to archive folder");
								} catch (Exception e) {
									e.printStackTrace();
								}

							}

							copiedFile.renameTo(baseFile);
							//System.out.println("Renamed " + fileName + " to " + onlyFileName + "_b" + ext);

						} else {
							archiveFolder.mkdir();

							String newbaseFileName = archiveFolderName + File.separator + onlyFileName + "_" + date + ext;
							File newBaseFile = new File(newbaseFileName);
							if (newBaseFile.exists()) {
								newBaseFile.delete();
								
							}
								try {
									FileUtils.moveFile(baseFile, newBaseFile);
								} catch (Exception e) {
									e.printStackTrace();
								}

							

							copiedFile.renameTo(baseFile);

						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else{
					if(copiedFile.exists()&& ! baseFile.exists()){
						copiedFile.renameTo(baseFile);
						try {
							if(new File(targetfile).exists()){
								new File(targetfile).delete();
							}
							FileUtils.copyFile(baseFile,new File(targetfile));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}

	}

	private static String createHashkey(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(text.getBytes("UTF-8"));

		return new String(hash, "UTF-8");

	}

	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		//diff(args[0], args[1], args[2], args[3]);
		//diff(String folderPath, String fileName, String targetFolder, String targetfile)
		// C:/vikash/office/Current_UAT
		diff("C:/vikash/office/","C_DSMT_MANAGER.txt","C:/vikash/office/Current_UAT","C:/vikash/office/Current_UAT/C_DSMT_MANAGER_Diff.txt");
		//System.out.println("Total time in Seconds:" + ((System.currentTimeMillis() - startTime) / 1000.00));

	}

}
