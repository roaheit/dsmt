/**
 * 
 */
package com.citi.ebx.dsmt.util;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.citi.ebx.dsmt.jdbc.connector.SQLConnectReportUtil;
import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;

/**
 * @author rk00242
 * 
 */
public class SQLReportGeneratorUtil {
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	final String env = propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
	
    protected String sqlID ;
    protected String exportFileDirectoryPath;
    protected String exportFileName;
    protected static ArrayList<String> sqlNames  ;
    protected static ArrayList<String> reportNames ;


	protected static Connection dbConnection = null;

	public void generateReport(final Connection conection, String outputFilePath,String reportType) {
		try {
			dbConnection = conection;
			Map<String, Integer> sqlReportMap = null;
			/**
			 * get default output file path if value is not passed in as a
			 * parameter
			 */
			if (null == outputFilePath) {
				outputFilePath = SQLReportConnectionUtils.bundle.getString(env
						+ DSMTConstants.DOT + "ExportFileDirectory");
			}
			SQLConnectReportUtil connectReportUtil = new SQLConnectReportUtil();
			connectReportUtil.setReportType(reportType);
			sqlReportMap = connectReportUtil.generateReport(dbConnection, null);
			List<String> sqlreportList = new ArrayList<String>(sqlReportMap.keySet());
			DSMTExportBean dsmtExportBean = new DSMTExportBean();
			dsmtExportBean.getFileNamesAndCount().putAll(sqlReportMap);
			 dsmtExportBean.setFileNames(sqlreportList);
			 dsmtExportBean.setOutputFilePath(outputFilePath);
			 LOG.info("htBean = " +  dsmtExportBean);	
			new HeaderTailerGenerator().runHeaderTrailerGenerator(dsmtExportBean);
			LOG.info("HT  generated successfully !!!  for  reports :  "+sqlreportList);
		} catch (Exception e) {
			LOG.error("Error inside method sqlReport-->generateReport  ->"
					+ e + " , message " + e.getMessage());
		}

	}
}
