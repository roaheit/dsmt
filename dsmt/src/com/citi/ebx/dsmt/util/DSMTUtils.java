package com.citi.ebx.dsmt.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;


public class DSMTUtils {
	
	   /**
   	 * Util  method to Check for Null or Empty or White Space in string
   	 * @param  String
   	 * @throws 
   	 * @return boolean(retrun true if string is null or empty or have white space 
   	 */   
    
	public static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	/**
	 * returns true if String is not null or empty.
	 * @param value
	 * @return
	 */
	public static boolean  isStringNotEmptyOrNull(final String value)
	{
		//return value != null && !value.isEmpty() && !value.trim().isEmpty();
		return value != null && !value.isEmpty();
		
	}

	
	public static String getSubject (final String userName, final String requestID){
		return "DSMT2 Notification : GOC Workflow Request ID - " + requestID;
	}

	
	public static String treeEffectiveDate(){
		
		final SimpleDateFormat sdf = new SimpleDateFormat(DSMTConstants.DEFAULT_DATE_FORMAT);
		final String environment =  propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
		
		String treeDate = "2014-01-01";  // assign value for testing
		
			final Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			treeDate= sdf.format(calendar.getTime());
		
		return treeDate;
		 
	}
	
	
	public static void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
	throws OperationException {

		/** Get user ID from current user session */
		String userID = context.getSession().getUserReference().getUserId();
		String environment = new DSMTPropertyHelper().getProp(DSMTConstants.DSMT_ENVIRONMENT);
		boolean isTestEnvironment = ! "prod".equalsIgnoreCase(environment);
		
		/** allow only admin to delete a record in Active status, only allow rs74652 to test on dev, sit and uat but not on Prod */
		final boolean isAdministrator = DSMTConstants.ADMIN_USER.equalsIgnoreCase(userID) || (isTestEnvironment && getAdminUserList().contains(userID) );
		
		try {
			String status = context.getAdaptationOccurrence().getString(DSMTConstants.statusFieldPath);

			if (DSMTConstants.ACTIVE.equalsIgnoreCase(status) && ! isAdministrator ) {
				throw OperationException.createError("Records with active status cannot be deleted");
				}
		} catch (PathAccessException e) {
			LOG.info("Delete will be allowed. " + DSMTConstants.statusFieldPath + " not found in the table." + e);
		}
	
	}
	
	
	
	
	public static Date previousMonthEffectiveDate(){
		
		final Calendar cal = Calendar.getInstance();
		
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
		 
	}
	
	/**
	 * format date in a specified format - eg "yyyy-MM-dd";
	 * @param format = "yyyy-MM-dd"
	 * @return formatted date
	 */
	public static String formatDate(final Date date, final String format){
		
		final SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
		 
	}

	
	public static Date currentMonthEffectiveDate(){
		
		final Calendar cal = Calendar.getInstance();
		
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
		 
	}
	
	
	private static List<String> getAdminUserList(){
		
		final List<String> adminUserList = new ArrayList<String>();
		adminUserList.add("rs74652");
		adminUserList.add("nc72931");
		adminUserList.add("fv66203");
	
		return adminUserList;
	}
	
	
	public static String getTodaysDate(final String pattern){
		
		final Calendar calendar = Calendar.getInstance();
		calendar.getTime();
	
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(calendar.getTime());
		
	}
	
	public static String getYesterdaysDate(final String pattern){
		
		final Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(calendar.getTime());
		
	}

	public static void main(String[] args) {
		
		System.out.println(formatDate(new Date(), "yyyy-MM-dd"));
		
		
	}
	
	
	
	
}
