package com.citi.ebx.dsmt.acctMatrix;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orchestranetworks.ps.service.LaunchWorkflowServlet;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.Session;

public class ControllerIDWorkflowServlet extends LaunchWorkflowServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String WORKFLOW_PUBLICATION = "ControllerIDPopulationWorkflow";
	
	public static final String PARAM_CONTROLLER_ID = "controllerID";
	private static final String PARAM_MSHIER_TABLE_PATH = "msHiertablePath";
	private static final String PARAM_MANAGED_SEG = "manSeg";
	private static final String PARAM_LVID = "lvID";
	private static final String PARAM_CONTROLLER_DETAILS_TABLE = "controllerDetailsTablePath";
	public static final String PARAM_MANAGED_GEOGRAPHY = "managedGeo";
	private static final String PARAM_ACTION = "action";
	public static final String PARAM_COMMENTS = "comments";
	

	
	public ControllerIDWorkflowServlet(){
		workflowPublication = WORKFLOW_PUBLICATION;
		redirectToUserTask = false;
	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		final ServiceContext sContext = ServiceContext
				.getServiceContext(req);
		
		Session session = sContext.getSession();
		
		String controllerID = req.getParameter(PARAM_CONTROLLER_ID);
		String action = req.getParameter("action");
		String managedGeo = req.getParameter(PARAM_MANAGED_GEOGRAPHY);
		LOG.info("action :"+ action);
		String msHiertablePath = (String) session.getAttribute("msHiertablePath");
		String manSeg = req.getParameter("manSeg");
		String lvID = req.getParameter("lvID");
		String comments = req.getParameter(PARAM_COMMENTS);
		
		Map<String, String> tableMap = new HashMap<String, String>();
		tableMap.put("/root/ACC_MAT/ACC_REG/ACC_MS_Hier_NAM", "/root/ACC_MAT/ACC_REG/Controller_Details_NAM");
		tableMap.put("/root/ACC_MAT/ACC_REG/ACC_MS_Hier_ASIA", "/root/ACC_MAT/ACC_REG/Controller_Details_ASIA");
		tableMap.put("/root/ACC_MAT/ACC_REG/ACC_MS_Hier_EMEA", "/root/ACC_MAT/ACC_REG/Controller_Details_EMEA");
		tableMap.put("/root/ACC_MAT/ACC_REG/ACC_MS_Hier_LATAM", "/root/ACC_MAT/ACC_REG/Controller_Details_LATAM");
		
		String controllerDetailsTablePath = tableMap.get(msHiertablePath);
		
		
		inputParameters = new HashMap<String, String>();
		inputParameters.put(PARAM_CONTROLLER_ID, controllerID);
		inputParameters.put(PARAM_LVID, lvID);
		inputParameters.put(PARAM_MANAGED_SEG, manSeg);
		inputParameters.put(PARAM_MSHIER_TABLE_PATH, msHiertablePath);
		inputParameters.put(PARAM_CONTROLLER_DETAILS_TABLE, controllerDetailsTablePath);
		inputParameters.put(PARAM_MANAGED_GEOGRAPHY, managedGeo);
		inputParameters.put(PARAM_ACTION, action);
		inputParameters.put(PARAM_COMMENTS, comments);
		
		
		LOG.info("controllerID : " +controllerID);
		LOG.info("msHiertablePath : " +msHiertablePath);
		LOG.info("manSeg : " +manSeg);
		LOG.info("lvID : " +lvID);
		LOG.info("controllerDetailsTablePath : " +controllerDetailsTablePath);
		
		
		super.service(req, res);
	}
}
