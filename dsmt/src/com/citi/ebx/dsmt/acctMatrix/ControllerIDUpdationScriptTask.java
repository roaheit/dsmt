/**
 * 
 */
package com.citi.ebx.dsmt.acctMatrix;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

/**
 * @author rk00242
 *
 */
public class ControllerIDUpdationScriptTask extends ScriptTaskBean {
	
	private String manSeg;
	private String lvID;
	private String controllerList;
	private String controllerID;
	private String controllerDetailsTablePath;
	private String msHiertablePath;
	private String dataSpace;
	private String comments;
	private String managedGeo;
	private String action;
	private boolean actionVal;

	/* (non-Javadoc)
	 * @see com.orchestranetworks.workflow.ScriptTaskBean#executeScript(com.orchestranetworks.workflow.ScriptTaskBeanContext)
	 */
	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		// TODO Auto-generated method stub
		Repository repo = context.getRepository();
		Session session = context.getSession();
		Adaptation metadataDataSet = null;
		
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(repo, dataSpace,AccountibilityMatrixConstants.DATASET_Accounting_Matrix);
			AdaptationTable msHierTable = metadataDataSet.getTable(Path.parse(msHiertablePath));
			
			AdaptationTable controllerDetailstable = metadataDataSet.getTable(Path.parse(controllerDetailsTablePath));
			
			String [] pPkey = new String[] {manSeg , lvID};
			
			PrimaryKey key = msHierTable.computePrimaryKey(pPkey);
			if(action.equalsIgnoreCase("U")){
				actionVal = true;
			}else{
				actionVal= false;
			}
			
			
			ControllerDetailsUpdationProcedure proc = new ControllerDetailsUpdationProcedure(key.format(), controllerID, controllerDetailstable,managedGeo, comments, actionVal);
			Utils.executeProcedure(proc, session, context.getRepository().lookupHome(
					HomeKey.forBranchName(dataSpace)));
			
			getChildRecord(key.format(), controllerID, controllerDetailstable,session,context,msHierTable);
			
			
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	
	public void getChildRecord(String msID, String controllerID,
			AdaptationTable controllerDetailstable,Session session, ScriptTaskBeanContext context,AdaptationTable msHierTable)
			throws OperationException {

		
		final String predicate = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Parent_MS_ID
				.format() + "='" + msID + "'";
		RequestResult rs = msHierTable.createRequestResult(predicate);	
		PrimaryKey key = null;

		if (rs.getSize() > 0) {
			for (Adaptation record; (record = rs.nextAdaptation()) != null;) {
					key = record.getOccurrencePrimaryKey();

				ControllerDetailsUpdationProcedure proc = new ControllerDetailsUpdationProcedure(key.format(), controllerID, controllerDetailstable,managedGeo,comments,actionVal);
				Utils.executeProcedure(proc, session, context.getRepository().lookupHome(
						HomeKey.forBranchName(dataSpace)));
				
					getChildRecord(key.format(), controllerID, controllerDetailstable,session,context,msHierTable);
				}
				
			}
		
		rs.close();
	}
	

	/**
	 * @return the manSeg
	 */
	public String getManSeg() {
		return manSeg;
	}

	/**
	 * @param manSeg the manSeg to set
	 */
	public void setManSeg(String manSeg) {
		this.manSeg = manSeg;
	}

	/**
	 * @return the lvID
	 */
	public String getLvID() {
		return lvID;
	}

	/**
	 * @param lvID the lvID to set
	 */
	public void setLvID(String lvID) {
		this.lvID = lvID;
	}

	/**
	 * @return the controllerList
	 */
	public String getControllerList() {
		return controllerList;
	}

	/**
	 * @param controllerList the controllerList to set
	 */
	public void setControllerList(String controllerList) {
		this.controllerList = controllerList;
	}

	/**
	 * @return the controllerID
	 */
	public String getControllerID() {
		return controllerID;
	}

	/**
	 * @param controllerID the controllerID to set
	 */
	public void setControllerID(String controllerID) {
		this.controllerID = controllerID;
	}

	/**
	 * @return the controllerDetailsTablePath
	 */
	public String getControllerDetailsTablePath() {
		return controllerDetailsTablePath;
	}

	/**
	 * @param controllerDetailsTablePath the controllerDetailsTablePath to set
	 */
	public void setControllerDetailsTablePath(String controllerDetailsTablePath) {
		this.controllerDetailsTablePath = controllerDetailsTablePath;
	}

	/**
	 * @return the msHiertablePath
	 */
	public String getMsHiertablePath() {
		return msHiertablePath;
	}

	/**
	 * @param msHiertablePath the msHiertablePath to set
	 */
	public void setMsHiertablePath(String msHiertablePath) {
		this.msHiertablePath = msHiertablePath;
	}


	/**
	 * @return the dataSpace
	 */
	public String getDataSpace() {
		return dataSpace;
	}


	/**
	 * @param dataSpace the dataSpace to set
	 */
	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}


	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}


	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}


	/**
	 * @return the managedGeo
	 */
	public String getManagedGeo() {
		return managedGeo;
	}


	/**
	 * @param managedGeo the managedGeo to set
	 */
	public void setManagedGeo(String managedGeo) {
		this.managedGeo = managedGeo;
	}


	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}


	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	
	
}
