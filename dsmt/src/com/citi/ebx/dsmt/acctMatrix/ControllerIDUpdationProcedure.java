/**
 * 
 */
package com.citi.ebx.dsmt.acctMatrix;

import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;


/**
 * @author am05884
 *
 */
public class ControllerIDUpdationProcedure implements Procedure {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private String manSegID;
	private String controllerID;
	private AdaptationTable controllerIDTablePath;
	private String lvid;
		
	public ControllerIDUpdationProcedure(String manSegID,String controllerID,AdaptationTable controllerIDTablePath,String lvid){
		super();
		this.manSegID = manSegID;
		this.controllerID= controllerID;
		this.controllerIDTablePath= controllerIDTablePath;
		this.lvid=lvid;
		
	}

	@Override
	public void execute(ProcedureContext pContext) throws Exception {
		// TODO Auto-generated method stub
		
		modifyRecord(pContext, controllerIDTablePath, controllerID, manSegID, lvid);
		
	}
	
	public void modifyRecord(ProcedureContext pContext, AdaptationTable table,
			String controllerID, String manSegID,String lvid) throws OperationException {
		LOG.info("modifying Controllers"+controllerID);
		ValueContextForUpdate valueContext;
		final String controllerIDPredicate = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_MAN_SEG_ID
				.format() + "='" + manSegID
				+ "' and "
				+ AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_LVID
				.format()
				+ "='"
				+ lvid
				+ "'";
		RequestResult res=table.createRequestResult(controllerIDPredicate);
		pContext.setAllPrivileges(true);
		if(res.getSize()>0)
		{
			Adaptation controllerRecord=res.nextAdaptation();
			valueContext=pContext.getContext(controllerRecord.getAdaptationName());
			valueContext
			.setValue(
					controllerID,
				AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID);
			pContext.doModifyContent(controllerRecord, valueContext);
			pContext.setAllPrivileges(false);
		}	
	}


	public String getManSegID() {
		return manSegID;
	}

	public void setManSegID(String manSegID) {
		this.manSegID = manSegID;
	}

	public String getControllerID() {
		return controllerID;
	}

	public void setControllerID(String controllerID) {
		this.controllerID = controllerID;
	}


	public AdaptationTable getControllerIDTablePath() {
		return controllerIDTablePath;
	}


	public void setControllerDetailsTablePath(AdaptationTable controllerDetailsTablePath) {
		this.controllerIDTablePath = controllerDetailsTablePath;
	}

	
	public String getlvid() {
		return lvid;
	}

		public void setlvid(String lvid) {
		this.lvid = lvid;
	}

}
