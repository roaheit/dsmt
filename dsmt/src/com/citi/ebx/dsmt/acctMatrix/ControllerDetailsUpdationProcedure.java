/**
 * 
 */
package com.citi.ebx.dsmt.acctMatrix;

import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;


/**
 * @author rk00242
 *
 */
public class ControllerDetailsUpdationProcedure implements Procedure {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private String manSegID;
	private String controllerID;
	private AdaptationTable controllerDetailsTablePath;
	private String comments;
	private String managedGeo;
	private boolean action;
	
	public ControllerDetailsUpdationProcedure(String manSegID,String controllerID,AdaptationTable controllerDetailsTablePath,String comments, String managedGeo,boolean action){
		super();
		this.manSegID = manSegID;
		this.controllerID= controllerID;
		this.controllerDetailsTablePath= controllerDetailsTablePath;
		this.comments=comments;
		this.managedGeo = managedGeo;
		this.action = action;
	}

	@Override
	public void execute(ProcedureContext pContext) throws Exception {
		// TODO Auto-generated method stub
		if(action)
		insertRecord(pContext, controllerDetailsTablePath, controllerID, manSegID, managedGeo, comments);
		else{
			deleteRecord(pContext, controllerDetailsTablePath, manSegID,controllerID);
		}
	}
	
	public void insertRecord(ProcedureContext pContext, AdaptationTable table,
			String controllerID, String manSegID,String managedGeo, String comments) throws OperationException {
		
		ValueContextForUpdate valueContext;
		valueContext = pContext.getContextForNewOccurrence(table);

		valueContext
				.setValue(
						controllerID,
					AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID);
				valueContext
					.setValue(
					manSegID,
					AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID);
				valueContext.setValue(managedGeo, AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Region);
				valueContext.setValue(comments , AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Comments);
				pContext.doCreateOccurrence(valueContext, table);
		}

	
	public void deleteRecord(ProcedureContext pContext, AdaptationTable table,
			String manSegID,String controllerID) throws OperationException {
		pContext.setAllPrivileges(true);
		final String predicate = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_MAN_SEG_ID
				.format() + " = '" + manSegID + "' and " +  AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID.format()
				+ "='" + controllerID + "'";
		LOG.info("table--" + table + "manSegID" + manSegID + "---predicate"
				+ predicate);
		RequestResult res = table.createRequestResult(predicate);
		if (res.getSize() > 0) {
			LOG.info("ResultSet is greater than 0");
			for (Adaptation Record; (Record = res.nextAdaptation()) != null;) {
				LOG.info("Record-------------------------" + Record);
				pContext.doDelete(Record.getAdaptationName(), false);
			}
		}
		res.close();
		pContext.setAllPrivileges(false);
	}

	/**
	 * @return the manSegID
	 */
	public String getManSegID() {
		return manSegID;
	}

	/**
	 * @param manSegID the manSegID to set
	 */
	public void setManSegID(String manSegID) {
		this.manSegID = manSegID;
	}

	/**
	 * @return the controllerID
	 */
	public String getControllerID() {
		return controllerID;
	}

	/**
	 * @param controllerID the controllerID to set
	 */
	public void setControllerID(String controllerID) {
		this.controllerID = controllerID;
	}

	/**
	 * @return the controllerDetailsTablePath
	 */
	public AdaptationTable getControllerDetailsTablePath() {
		return controllerDetailsTablePath;
	}

	/**
	 * @param controllerDetailsTablePath the controllerDetailsTablePath to set
	 */
	public void setControllerDetailsTablePath(AdaptationTable controllerDetailsTablePath) {
		this.controllerDetailsTablePath = controllerDetailsTablePath;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the managedGeo
	 */
	public String getManagedGeo() {
		return managedGeo;
	}

	/**
	 * @param managedGeo the managedGeo to set
	 */
	public void setManagedGeo(String managedGeo) {
		this.managedGeo = managedGeo;
	}

	/**
	 * @return the action
	 */
	public boolean isAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(boolean action) {
		this.action = action;
	}

	
	
}
