/**
 * 
 */
package com.citi.ebx.dsmt.acctMatrix;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.goc.path.DSMT2Paths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.importer.Constants;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.Session;

/**
 * @author rk00242
 *
 */
public class AcctMatrixUpdateControllerDetails extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final String env = DSMTConstants.propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
	
	
	protected BufferedWriter out;
	protected String servlet = "";
	private String controllerList="";
	private String manSeg;
	private String lvID;
	private String controllerID;
	private String msHiertablePath;
	private String action;
	private String managedGeo;
	private String comments="";
	private String tableID;
	Session session;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		
		final ServiceContext sContext = ServiceContext
				.getServiceContext(req);
		Repository repo = sContext.getCurrentHome().getRepository();


		Adaptation container = sContext.getCurrentAdaptation();
		final AdaptationHome dataSpace = container.getHome();
			session = sContext.getSession();

		List<Adaptation> selectedRecord = sContext.getSelectedOccurrences();
		
		for (int i = 0; i < selectedRecord.size(); i++) {
			final Adaptation record = (Adaptation) selectedRecord.get(i);
			final AdaptationTable table = record.getContainerTable();
			
			final String tablePath = sContext.getCurrentNode().getPathInSchema().format();
			
			String[] tableSplit=tablePath.split("/");
			 tableID = tableSplit[tableSplit.length-1];	
			
			msHiertablePath = table.getTablePath().format().toString();
			session.setAttribute("msHiertablePath", msHiertablePath);
			
			manSeg = record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_MAN_SEG_ID);
			lvID = record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_LVID);
			controllerList = record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID);

		}
		
		
		out = new BufferedWriter(res.getWriter());
		try {
			writeForm(sContext);
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.flush();
	}
	
	protected void writeForm(ServiceContext sContext) throws IOException, OperationException {
		writeln("<form id=\"form\" action=\"" + sContext.getURLForIncludingResource(servlet)
				+ "\" method=\"post\" autocomplete=\"off\">");
		writeFields(sContext);
		writeSubmitButton(sContext);
		writeln("</form>");
	}
	
	protected void writeFields(ServiceContext sContext) throws IOException, OperationException {
		writeln("  <table>");
		writeln("    <tbody>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + "Managed Segment"
				+ "\">Managed Segment</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + manSeg
				+ "\" type=\"text\" id=\"" + "manSeg"
				+ "\" name=\"" + "manSeg" + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + "LVID"
				+ "\">LVID</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + lvID
				+ "\" type=\"text\" id=\"" + "lvID"
				+ "\" name=\"" + "lvID" + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + "Controller List"
				+ "\">Controller List</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + controllerList
				+ "\" type=\"text\" id=\"" + "controllerList"
				+ "\" name=\"" + "controllerList" + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + ControllerIDWorkflowServlet.PARAM_CONTROLLER_ID
				+ "\">Controller ID</label>");
		writeln("        </td>");
		
		writeln("        <td class=\"ebx_Input\">");
		writeln(" <select class=\"ebx_Input\" id=\""+ControllerIDWorkflowServlet.PARAM_CONTROLLER_ID+"\""
				+"name=\""+ControllerIDWorkflowServlet.PARAM_CONTROLLER_ID+"\">"
				);
		 
		writeln(getControllers(sContext));
		
		writeln(" </select>");

		writeln("        </td>");
		writeln("      </tr>");
		
		
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + ControllerIDWorkflowServlet.PARAM_MANAGED_GEOGRAPHY
				+ "\">Managed Geography</label>");
		writeln("        </td>");
		
		writeln("        <td class=\"ebx_Input\">");
		writeln(" <select class=\"ebx_Input\" id=\""+ControllerIDWorkflowServlet.PARAM_MANAGED_GEOGRAPHY+"\""
				+"name=\""+ControllerIDWorkflowServlet.PARAM_MANAGED_GEOGRAPHY+"\">"
				);
		 
		writeln(getGeography(sContext));
		
		writeln(" </select>");

		writeln("        </td>");
		writeln("      </tr>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + ControllerIDWorkflowServlet.PARAM_COMMENTS
				+ "\">Comments</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input type=\"text\" id=\"" + ControllerIDWorkflowServlet.PARAM_COMMENTS
				+ "\" name=\"" + ControllerIDWorkflowServlet.PARAM_COMMENTS + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"Action" 
				+ "\">Action</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"U\" type=\"radio\" checked=\"checked\" id=\""+action
				+ "\" name=\"action\"/>");
		writeln("Update");
	
		writeln("        <input value=\"D\" type=\"radio\"  id=\""+action
				+ "\" name=\"action\"/>");
		writeln("Delete");
		writeln("        </td>");
		writeln("      </tr>");
		writeln("    </tbody>");
		writeln("  </table>");
	}
	
	protected void writeSubmitButton(ServiceContext sContext) throws IOException {
		writeln("  <button type=\"submit\" class=\"ebx_Button\">Update Controller List</button>");
	}
	
	protected void write(final String str) throws IOException {
		out.write(str);
	}
	
	protected void writeln(final String str) throws IOException {
		write(str);
		out.newLine();
	}
	
	 protected String getControllers(ServiceContext sContext) throws OperationException{
	    	Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(sContext.getCurrentHome().getRepository(), AccountibilityMatrixConstants.DATASPACE_Accounting_Matrix, AccountibilityMatrixConstants.DATASET_Accounting_Matrix);
			final AdaptationTable targetTable = metadataDataSet.getTable(AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER.getPathInSchema());
			String predicate = AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._C_GDW_EMP_STATUS.format() + " ='A' ";
			
			RequestResult res = targetTable.createRequestResult(predicate);
			int count=0;
			Adaptation row;
			StringBuffer sb = new StringBuffer();

			while(count < res.getSize()){
				row= res.nextAdaptation();
				sb.append("<option value=\"");
				sb.append(row.getString(AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._MANAGER_ID));
				sb.append("\">");
				sb.append(row.getString(AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._MANAGER_ID) + " - " + row.getString(AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._C_GDW_EMP_NAME));
				sb.append("</option>");
				
				count++;
			}
			return sb.toString();
	    }
	 
	 protected String getGeography(ServiceContext sContext) throws OperationException{
		 String dataSet;
		 if(env.equalsIgnoreCase("uat")){
			 dataSet=DSMTConstants.DATASET_NEW_DSMT2_UAT;
			}
			else if(env.equalsIgnoreCase("development")){
				dataSet=DSMTConstants.DATASET_NEW_DSMT2;
			}
			else{
				dataSet=DSMTConstants.DATASET_NEW_DSMT2_PROD;
			}
		 Path globalCountryTable = DSMT2Paths._GLOBAL_STD_ENT_STD_C_GBL_CNTRY.getPathInSchema();
		 
		 Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(sContext.getCurrentHome().getRepository(),DSMTConstants.DATASPACE_DSMT2, dataSet);
			AdaptationTable targetTable = metadataDataSet.getTable(globalCountryTable);
			
			String region = getRegion();
			
			final String pred1 = DSMT2Paths._GLOBAL_STD_ENT_STD_C_GBL_CNTRY._COUNTRY_REGION.format() + "= '" + region + "'";
			

			final String predicate = pred1  ;

		
			RequestResult res = targetTable.createRequestResult(predicate);
			int count=0;
			Adaptation row;
			StringBuffer sb = new StringBuffer();

			while(count < res.getSize()){
				row= res.nextAdaptation();
				sb.append("<option value=\"");
				sb.append(row.getString(DSMT2Paths._GLOBAL_STD_ENT_STD_C_GBL_CNTRY._DESCR));
				sb.append("\">");
				sb.append(row.getString(DSMT2Paths._GLOBAL_STD_ENT_STD_C_GBL_CNTRY._DESCR));
				sb.append("</option>");
				
				count++;
			}
			return sb.toString();
	    }
	 
	 private String getRegion(){
		 String region = null;
		 
		 	String[] tableSplit=tableID.split("_");
			String table = tableSplit[tableSplit.length-1];	
		 
		 if(tableID.contains("ASIA")){
			 region = "APAC";
		 }else{
			 region = table;
		 }
		 
		 
		 
		 return region;
	 }

	/**
	 * @return the servlet
	 */
	public String getServlet() {
		return servlet;
	}

	/**
	 * @param servlet the servlet to set
	 */
	public void setServlet(String servlet) {
		this.servlet = servlet;
	}

	/**
	 * @return the manSeg
	 */
	public String getManSeg() {
		return manSeg;
	}

	/**
	 * @param manSeg the manSeg to set
	 */
	public void setManSeg(String manSeg) {
		this.manSeg = manSeg;
	}

	/**
	 * @return the lvID
	 */
	public String getLvID() {
		return lvID;
	}

	/**
	 * @param lvID the lvID to set
	 */
	public void setLvID(String lvID) {
		this.lvID = lvID;
	}

	/**
	 * @return the controllerList
	 */
	public String getControllerList() {
		return controllerList;
	}

	/**
	 * @param controllerList the controllerList to set
	 */
	public void setControllerList(String controllerList) {
		this.controllerList = controllerList;
	}

	/**
	 * @return the controllerID
	 */
	public String getControllerID() {
		return controllerID;
	}

	/**
	 * @param controllerID the controllerID to set
	 */
	public void setControllerID(String controllerID) {
		this.controllerID = controllerID;
	}

	/**
	 * @return the msHiertablePath
	 */
	public String getMsHiertablePath() {
		return msHiertablePath;
	}

	/**
	 * @param msHiertablePath the msHiertablePath to set
	 */
	public void setMsHiertablePath(String msHiertablePath) {
		this.msHiertablePath = msHiertablePath;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the managedGeo
	 */
	public String getManagedGeo() {
		return managedGeo;
	}

	/**
	 * @param managedGeo the managedGeo to set
	 */
	public void setManagedGeo(String managedGeo) {
		this.managedGeo = managedGeo;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}


}
