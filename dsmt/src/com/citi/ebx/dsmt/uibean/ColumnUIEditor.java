package com.citi.ebx.dsmt.uibean;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.ui.UIBeanEditor;
import com.orchestranetworks.ui.UIResponseContext;

public class ColumnUIEditor extends UIBeanEditor {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	private String htmlColor = "red";

	@Override
	public void addForDisplay(UIResponseContext context) {
		context.addUIBestMatchingDisplay(Path.SELF, "");
	}

	@Override
	public void addForDisplayInTable(UIResponseContext context) {

		String htmlAttributes = "style=\"color:" + htmlColor + "\"";
		context.addUIBestMatchingDisplay(Path.SELF, htmlAttributes);
	}

	@Override
	public void addForEdit(UIResponseContext context) {
		context.addUIBestMatchingEditor(Path.SELF, "");
	}

	public void setHtmlColor(String htmlColor) {
		this.htmlColor = htmlColor;
	}

	public String getHtmlColor() {
		return htmlColor;
	}


}
