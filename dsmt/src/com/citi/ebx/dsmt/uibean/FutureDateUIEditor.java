package com.citi.ebx.dsmt.uibean;

import java.util.Calendar;
import java.util.Date;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.ui.UIBeanEditor;
import com.orchestranetworks.ui.UIResponseContext;

public class FutureDateUIEditor extends UIBeanEditor {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	
	@Override
	public void addForDisplay(UIResponseContext context) {
		context.addUIBestMatchingDisplay(Path.SELF, "");
	}

	@Override
	public void addForDisplayInTable(UIResponseContext context) {
		
		Date effectiveDate = (Date)context.getValue(Path.SELF);
        boolean isFuture = isFutureEffectiveDate(effectiveDate); 
	        
		String htmlAttributes = isFuture ? "style=\"color:red\"" : "";
		context.addUIBestMatchingDisplay(Path.SELF, htmlAttributes);
	}

	@Override
	public void addForEdit(UIResponseContext context) {
		context.addUIBestMatchingEditor(Path.SELF, "");
	}
	
	private boolean isFutureEffectiveDate(Date effectiveDate){
		
		boolean isFutureEffectiveDate = false;
		Date currentDate = Calendar.getInstance().getTime();
		// final Date effectiveDate = adaptation.getDate(Path.parse(effectiveDateField));
		
		isFutureEffectiveDate = currentDate.before(effectiveDate);
		
		return isFutureEffectiveDate;
	}
}
