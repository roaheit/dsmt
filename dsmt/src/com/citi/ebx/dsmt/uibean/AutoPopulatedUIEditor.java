package com.citi.ebx.dsmt.uibean;

import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.ui.UIBeanEditor;
import com.orchestranetworks.ui.UIResponseContext;

public class AutoPopulatedUIEditor extends UIBeanEditor {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	
	@Override
	public void addForDisplay(UIResponseContext context) {
		context.addUIBestMatchingDisplay(Path.SELF, "");
	}

	@Override
	public void addForDisplayInTable(UIResponseContext context) {
		
		boolean isFuture ;
		ValueContext vc = context.getValueContext();
		AdaptationTable table = vc.getAdaptationTable();
		PrimaryKey pk = table.computePrimaryKey(vc);
		Adaptation record = table.lookupAdaptationByPrimaryKey(pk);
		String _Auto_Populated = record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Auto_Populated);
		LOG.debug("_Auto_Populated value is " +_Auto_Populated);
		//String _Auto_Populated =(null==vc.getValue(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Auto_Populated)? null:
		//			(String) vc.getValue(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Auto_Populated));
        //String 	_Auto_Populated = 	(String)context.getValue(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Auto_Populated); 
        if(null != _Auto_Populated && _Auto_Populated.equalsIgnoreCase("Y")){
        	isFuture=true;
        }else{
        	isFuture=false;
        }
	        
		String htmlAttributes = isFuture ? "style=\"color:red\"" : "";
		context.addUIBestMatchingDisplay(Path.SELF, htmlAttributes);
	}

	@Override
	public void addForEdit(UIResponseContext context) {
		context.addUIBestMatchingEditor(Path.SELF, "");
	}
	
	
}
