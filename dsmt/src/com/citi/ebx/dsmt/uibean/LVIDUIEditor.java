package com.citi.ebx.dsmt.uibean;

import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.workflow.ImportLVIDData;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.Nomenclature;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryHandler;
import com.orchestranetworks.ui.UIBeanEditor;
import com.orchestranetworks.ui.UIContext;
import com.orchestranetworks.ui.UIRequestContext;
import com.orchestranetworks.ui.UIResponseContext;
import com.orchestranetworks.ui.form.widget.UIDropDownList;

public class LVIDUIEditor extends UIBeanEditor {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	public static final Path LVID_LEMTable= AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping.getPathInSchema();
	public static final Path NAMTable = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM.getPathInSchema();
	public static final Path LATAMTable = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_LATAM.getPathInSchema();
	public static final Path EMEATable = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_EMEA.getPathInSchema();
	public static final Path ASIATable = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_ASIA.getPathInSchema();
	public static final Path LV_ID= AccountibilityMatrixPaths._LV_ID;
	boolean valid=true;
	DirectoryHandler directory;
	UserReference userID;
	String User;
	@SuppressWarnings("deprecation")
	@Override
	public void addForDisplay(UIResponseContext context) {
		context.addUIDropDownBox(LV_ID, "");
	}

	@Override
	public void addForNull(UIResponseContext context)
	{
		addForEdit(context);
	}

	@Override
	public void addForDisplayInTable(UIResponseContext context) {
		
	}
	
	@Override
	public void addForEdit(UIResponseContext ctx) {
		Repository repo = ctx.getValueContext().getAdaptationInstance().getHome().getRepository();

		User=ctx.getSession().getUserReference().getUserId().toString();
		userID = ctx.getSession().getUserReference();
		directory = ctx.getSession().getDirectory();
		UIDropDownList uid=ctx.newDropDownList(LV_ID);
		uid.setAjaxValueSynchEnabled(true);
		uid.setSpecificNomenclature(createNomenclature(ctx, ctx.getLocale()));
		ctx.addWidget(uid);
		

}
private Nomenclature createNomenclature(UIResponseContext ctx, Locale locale)
	{
		Adaptation dataSet=ctx.getValueContext().getAdaptationInstance();
		AdaptationTable lvidLemTable=dataSet.getTable(LVID_LEMTable);
		final String RegionPredicate = "osd:contains-case-insensitive("
						+ AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID
						.format()
						+ ",'"
							+ User.toUpperCase()
						+ "')";
		final String lvidLEMPredicate=AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM.format()
				+" = '"
				+User.toUpperCase()
				+"'";
		
		Nomenclature nomenclature = new Nomenclature();

		Set<String> uniqueLVID = new LinkedHashSet<String>();
		
		if(directory.isUserInRole(
						userID,
						Role.forSpecificRole(AccountibilityMatrixConstants.Accountability_Matrix_Admin))){
				LOG.debug("LVIDUIEditor--> User role is Accounting_Matrix_Admin ");
				RequestResult reqRes = lvidLemTable.createRequestResult(null);
				for (Adaptation record; (record = reqRes.nextAdaptation()) != null;)
				{	
					String LVID=record.getString(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID);
					String descr=lvidDescription(lvidLemTable,LVID);
					String finalStrng=LVID+"-"+descr;
					uniqueLVID.add(finalStrng);
				}	
				reqRes.close();
			}
		else if(directory.isUserInRole(
				userID,
				Role.forSpecificRole(AccountibilityMatrixConstants.Accountability_Matrix_ReadOnly))){
			LOG.debug("LVIDUIEditor--> User role is Accounting_Matrix_ReadOnly ");
			
			RequestResult reqRes = lvidLemTable.createRequestResult(null);
			for (Adaptation record; (record = reqRes.nextAdaptation()) != null;)
			{	
				String LVID=record.getString(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID);
				String descr=lvidDescription(lvidLemTable,LVID);
				String finalStrng=LVID+"-"+descr;
				uniqueLVID.add(finalStrng);
			}
			reqRes.close();
		}
			
		else{
			AdaptationTable namTable=dataSet.getTable(NAMTable);
			AdaptationTable asiaTable=dataSet.getTable(ASIATable);
			AdaptationTable latamTable=dataSet.getTable(LATAMTable);
			AdaptationTable emeaTable=dataSet.getTable(EMEATable);
			RequestResult namres=namTable.createRequestResult(RegionPredicate);
			RequestResult asiares=asiaTable.createRequestResult(RegionPredicate);
			RequestResult latamres=latamTable.createRequestResult(RegionPredicate);
			RequestResult emeares=emeaTable.createRequestResult(RegionPredicate);
			RequestResult reqRes1 = lvidLemTable.createRequestResult(lvidLEMPredicate);
			
			if(reqRes1.getSize()>0) {
				for (Adaptation record; (record = reqRes1.nextAdaptation()) != null;)
				{	
					String LVID=record.getString(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID);
					LOG.debug("LVID for LEM ---------,,,,,,,,,,,,,,,,,,,,"+LVID);
					String descr=lvidDescription(lvidLemTable,LVID);
					String finalStrng=LVID+"-"+descr;
					uniqueLVID.add(finalStrng);
					
				}
				reqRes1.close();		
			} 
			
			if(namres.getSize()>0)
			{
				for (Adaptation record; (record = namres.nextAdaptation()) != null;)
				{	
					String LVID=record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_LVID);
					String descr=lvidDescription(lvidLemTable,LVID);
					String finalStrng=LVID+"-"+descr;
					uniqueLVID.add(finalStrng);
					
				}
			}
			namres.close();
			if(asiares.getSize()>0)
			{
				for (Adaptation record; (record = asiares.nextAdaptation()) != null;)
				{	
					String LVID=record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_LVID);
					String descr=lvidDescription(lvidLemTable,LVID);
					String finalStrng=LVID+"-"+descr;
					uniqueLVID.add(finalStrng);
					
				}
			}
			asiares.close();
			if(latamres.getSize()>0)
			{
				for (Adaptation record; (record = latamres.nextAdaptation()) != null;)
				{	
					String LVID=record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_LVID);
					String descr=lvidDescription(lvidLemTable,LVID);
					String finalStrng=LVID+"-"+descr;
					uniqueLVID.add(finalStrng);
					
				}
			}
			latamres.close();
			if(emeares.getSize()>0)
			{
				for (Adaptation record; (record = emeares.nextAdaptation()) != null;)
				{	
					String LVID=record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_LVID);
					String descr=lvidDescription(lvidLemTable,LVID);
					String finalStrng=LVID+"-"+descr;
					uniqueLVID.add(finalStrng);
					
				}
			}
			emeares.close();
		}
		for(String lvidValue : uniqueLVID){
			nomenclature.addItemValue(lvidValue,lvidValue);
		}
		return nomenclature;
	}
		
	public String lvidDescription(AdaptationTable lvidlemTable,String lvid){
		
		String lvidDescription="";
		final String lvidPredicate=AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID.format()
				+" = '"
				+lvid.toUpperCase()
				+"'";
		RequestResult reqRes = lvidlemTable.createRequestResult(lvidPredicate);
		if(reqRes.getSize()>0){
		Adaptation record=reqRes.nextAdaptation();
		lvidDescription=(null==record.get(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LV_NAME))?"":record.get(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LV_NAME).toString();
		}
		else{
			lvidDescription="";
		}
		return lvidDescription;
	}
	@Override
    public void validateInput(UIRequestContext ctx) {
		String webName = ctx.getWebName();
		String LVID = ctx.getOptionalRequestParameterValue(webName);
		String[] LVIDParts = LVID.split("-");
		LVID = LVIDParts[0]; 
		setSessionVar(ctx, LVID);
		Repository repo = ctx.getValueContext().getAdaptationInstance().getHome().getRepository();
		
		try {
				ImportLVIDData.importData(repo,ctx.getSession(),LVID);
			} catch (OperationException e) {
				// TODO Auto-generated catch block
				LOG.debug("LVIDUIEditor: Exception in importing data "+e.getMessage());
			}
		
	}
	private void setSessionVar(UIContext context, String LVID){
		context.getSession().setAttribute("LVID", LVID);
	}

}