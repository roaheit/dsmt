package com.citi.ebx.dsmt.uibean;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.ui.UILabelRendererForHierarchy;
import com.orchestranetworks.ui.UILabelRendererForHierarchyContext;

public class FutureDateNodeRenderer implements UILabelRendererForHierarchy {
	
	private String effectiveDateField = "./EFFDT";
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	@Override
	public void displayLabel(UILabelRendererForHierarchyContext context) {
		
        String defaultText = context.getLabelFromDefaultPattern();
        Adaptation adaptation = context.getOccurrence();
        boolean isFuture = isFutureEffectiveDate(adaptation); 
        if (isFuture) {
                        context.add("<font color=\"red\">" + defaultText );
        } else {
                        context.add(defaultText);
        }


	}
	
	private boolean isFutureEffectiveDate(Adaptation adaptation){
		
		boolean isFutureEffectiveDate = false;
		Date currentDate = Calendar.getInstance().getTime();
		final Date effectiveDate = adaptation.getDate(Path.parse(effectiveDateField));
		
		isFutureEffectiveDate = currentDate.before(effectiveDate);
		if(isFutureEffectiveDate){	
			
			try {
				LOG.info("This date is in future and cant be sent to export = " + effectiveDate + " for " + adaptation.getContainerTable().getTableNode().getLabel(Locale.getDefault()) + ":" + adaptation.getLabelOrName(Locale.getDefault()));
			} catch (Exception e) {
				LOG.error(e.getMessage());
			}
		}
		return isFutureEffectiveDate;
	}

}
