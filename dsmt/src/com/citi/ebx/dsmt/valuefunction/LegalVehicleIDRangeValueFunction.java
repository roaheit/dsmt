package com.citi.ebx.dsmt.valuefunction;


/**
 * Returns the range associated with the legal vehicle's ID
 */
public class LegalVehicleIDRangeValueFunction {
	/*

implements ValueFunction {
	private static final Path RANGE_TABLE_PATH =
			DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_LegalVehicleIDRange.getPathInSchema();
	private static final Path RANGE_START_PATH =
			DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_LegalVehicleIDRange._Start;
	private static final Path RANGE_END_PATH =
			DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_LegalVehicleIDRange._End;
	
	private static final int NUM_DIGITS = 5;
	
	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	@Override
	public Object getValue(Adaptation adaptation) {
		final String lvid = adaptation.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_C_DSMT_LVID_TBL._C_DSMT_LVID);
		try {
			/** Check if lvid is an integer or not *
			Integer.parseInt(lvid);
		} catch (NumberFormatException ex) {
			LOG.error("Legal Vehicle ID is not an integer.", ex);
			return null;
		}
		final String predicate = RANGE_START_PATH.format() + "<= " + lvid
				+ " and " + RANGE_END_PATH.format() + ">= " + lvid;
		final AdaptationTable rangeTable = adaptation.getContainer().getTable(RANGE_TABLE_PATH);
		final RequestResult reqRes = rangeTable.createRequestResult(predicate);
		final Adaptation range;
		try {
			range = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (range == null) {
			return null;
		}
		
		final int startInt = range.get_int(RANGE_START_PATH);
		final int endInt = range.get_int(RANGE_END_PATH);
		
		final NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMinimumIntegerDigits(NUM_DIGITS);
		return numberFormat.format(startInt) + " - " + numberFormat.format(endInt);
	}

	@Override
	public void setup(ValueFunctionContext context) {
	}
*/
}
