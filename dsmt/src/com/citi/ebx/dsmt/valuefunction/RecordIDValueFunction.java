package com.citi.ebx.dsmt.valuefunction;

import com.citi.ebx.goc.path.RosettaPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class RecordIDValueFunction implements ValueFunction {
	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	@Override
	public Object getValue(Adaptation adaptation) {
		String recordID="";
		String geid= getGEID(adaptation);
		String plan_id=getPLANID(adaptation);
		
		recordID= geid + plan_id;
		
		
		

		return recordID;
	}

	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub
		
	}

	private static String getGEID(final Adaptation Record) {
		
		//LOG.info("Inside getGEID"+RosettaPaths._MANUAL_TAGGING_KEY_MGT_EMP._GEID);
		final String geidRecord = Record.getString(RosettaPaths._MANUAL_TAGGING_KEY_MGT_EMP._GEID);
		
		if (geidRecord == null) {
			LOG.info("Grid Record is Null");
			LOG.error("No GEID found for record " + Record.getOccurrencePrimaryKey().format());
			return "";
		}
		//LOG.info(geidRecord);
		return geidRecord;
	}
	
	private static String getPLANID(final Adaptation Record)
	{
		final String planID= Record.getString(RosettaPaths._MANUAL_TAGGING_KEY_MGT_EMP._PLAN_ID);
		if (planID == null) {
			LOG.error("No planID found for record " + Record.getOccurrencePrimaryKey().format());
			return "";
		}
		return planID;
	}

}
