package com.citi.ebx.dsmt.valuefunction;

import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class GoctoSidValueFunction implements ValueFunction{

	
	private String sourceTablePath;		//"/root/GLOBAL_STD/ENT_STD/F_BAL_TYPE/C_DSMT_ACC_CTGR");
	private String sourceColumnName;	
	private String fkColumnPath;
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	
	@Override
	public Object getValue(Adaptation record) {
		// Get the foreign key node
		String predicateValue = record.getString(getPath(fkColumnPath));
		
		String sidValue ;
		String predicateString =  GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK.format() + "=\"" + predicateValue + "\"";
		if(null!=predicateValue){
			AdaptationTable table = record.getContainer().getTable(getPath(sourceTablePath));
			RequestResult reqRes = table.createRequestResult(predicateString);
			try{
					Adaptation gocRec = reqRes.nextAdaptation();
					final Adaptation sidRecord = Utils.getLinkedRecord(gocRec,getPath(sourceColumnName));
						if (sidRecord != null) {				
								sidValue =(String) sidRecord.getString(getPath(sourceColumnName));
								LOG.info("Value found for gocfk "+predicateValue + " fk value is : "+ sidValue);
								return sidValue;
						}
			}		
			catch (Exception e) {
				LOG.error("No reference found for GOC FK " + predicateValue + " in " + sourceTablePath  + " Exception = " + e.getMessage() );
				return null;	
			}
			finally {
				reqRes.close();
			}
			
		}
		else{ return null;}
		
		return null;
	}
	
	
	public void setup(ValueFunctionContext arg0) {
		// Not needed

	}
	
private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}

	/**
	 * @return the sourceTableXPath
	 */
	public String getSourceTablePath() {
		return sourceTablePath;
	}

	/**
	 * @param sourceTableXPath the sourceTableXPath to set
	 */
	public void setSourceTablePath(String sourceTablePath) {
		this.sourceTablePath = sourceTablePath;
	}

	/**
	 * @return the sourceColumnName
	 */
	public String getSourceColumnName() {
		return sourceColumnName;
	}

	/**
	 * @param sourceColumnName the sourceColumnName to set
	 */
	public void setSourceColumnName(String sourceColumnName) {
		this.sourceColumnName = sourceColumnName;
	}

	/**
	 * @return the fkColumnPath
	 */
	public String getFkColumnPath() {
		return fkColumnPath;
	}

	/**
	 * @param fkColumnPath the fkColumnPath to set
	 */
	public void setFkColumnPath(String fkColumnPath) {
		this.fkColumnPath = fkColumnPath;
	}
}
