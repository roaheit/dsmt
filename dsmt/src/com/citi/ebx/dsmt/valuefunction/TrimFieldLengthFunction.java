package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class TrimFieldLengthFunction implements ValueFunction {
	
	private String inputFieldID;
	private Integer trimLength = 1; 
	
	public String getInputFieldID() {
		return inputFieldID;
	}

	public void setInputFieldID(String inputFieldID) {
		this.inputFieldID = inputFieldID;
	}

	public Integer getTrimLength() {
		return trimLength;
	}

	public void setTrimLength(Integer trimLength) {
		this.trimLength = trimLength;
	}

	@Override
	public Object getValue(Adaptation adaptation) {	
		
		String naicsCode = adaptation.getString(Path.parse(inputFieldID));
		String naicsSubstr = null;
		
		if(null != naicsCode){
			naicsSubstr = naicsCode.substring(0, naicsCode.length() - trimLength);	
			// System.out.println(naicsSubstr);
		}
		
		return naicsSubstr;
	}

	@Override
	public void setup(ValueFunctionContext context) {
		// no nothing
	}

	
	
	
}
