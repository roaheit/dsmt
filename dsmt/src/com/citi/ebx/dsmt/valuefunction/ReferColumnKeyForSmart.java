package com.citi.ebx.dsmt.valuefunction;

import org.apache.commons.lang.StringUtils;

import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPartnerSystemPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.schema.SchemaNode;

public class ReferColumnKeyForSmart  implements ValueFunction {
	private Path sourceColumnPath_1;
	private Path sourceColumnPath_2;
	private Path targetColumn;
	private Path tablePath_1;
	private String dataSpace;	
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Object getValue(Adaptation record) {
		// Get the foreign key node
		
		Path fkColumnPath = Path.parse("./C_GOC_FK");
			final SchemaNode fkNode = record.getSchemaNode().getNode(fkColumnPath);
			final Adaptation gocLinkedRecord = fkNode.getFacetOnTableReference().getLinkedRecord(record);
			
			if (gocLinkedRecord != null) {
				
				// method starts to get if it's for LATM /cafis 
				if(record.getContainerTable().toString().contains("C_PTNR_SMART_LATAM")){
					LOG.debug(" ReferColumnKeyForSmart-->getValue  table name is  LAtM");
				
				final Adaptation Sid = Utils.getLinkedRecord(gocLinkedRecord,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
				final String currentSid= Sid.getString(
						DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID);	

				LOG.debug("currentSid"+currentSid.toString());	
					LOG.debug(" ReferColumnKeyForSmart-->getValue  currentSid value is " + currentSid);
				// get Region value from SID and then apply validation
				final Adaptation metadataDataSet;
				try {
					metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(record.getHome().getRepository());
				} catch (OperationException ex) {
					LOG.error("Error looking up metadata data set", ex);
					return null;
				}
				
				
				final AdaptationTable sidEnhancementTable = metadataDataSet.getTable(GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
				
				final PrimaryKey sidEnhancementTablePK = sidEnhancementTable.computePrimaryKey(new Object[] {currentSid});
				
				final Adaptation sidEnhancementTableRecord = sidEnhancementTable.lookupAdaptationByPrimaryKey(sidEnhancementTablePK);
				
				String region=sidEnhancementTableRecord.get(GOCWFPaths._C_DSMT_SID_ENH._Region).toString();
				LOG.debug(" LatamSmartRecordConstraint-->checkRecord  region value is " + region);
				if("LATAM".equalsIgnoreCase(region)){
					tablePath_1=GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU.getPathInSchema();
					}
					if("CAFIS".equalsIgnoreCase(region)){
						tablePath_1= GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU.getPathInSchema();
					}
					
				}
				
					//	method ends to get if it's for LATM /cafis 
					
				String targetRecord  = getTargetRecord(record.getHome().getRepository(), record );
				return targetRecord;
			}
			return null;
			
		}
/*
	private static String getGOCLinkedRecord(final Adaptation gocRecord,Path sourceColumnPath_1) {

		final Adaptation gocLinkedRecord = Utils.getLinkedRecord(gocRecord,
				sourceColumnPath_1);
		if (gocLinkedRecord == null) {
			LOG.error("No data found for"+sourceColumnPath_1+" record " + gocRecord.getOccurrencePrimaryKey().format());
			return null;
		}

		return gocLinkedRecord.getString(
				sourceColumnPath_1);
	}
	
*/	
	public  String getTargetRecord(Repository repo,Adaptation record)
	{
		
		final Adaptation metadataDataSet ;
		String targetColumnValue=null;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(repo, dataSpace, dataSpace);
			final AdaptationTable ccTable =	metadataDataSet.getTable(
					tablePath_1);
			
			final String condi_Vehicle =record.getString(sourceColumnPath_1);
			
			final String predicate = sourceColumnPath_2.format()
					+ "=\"" +condi_Vehicle+ "\"";
			
			
			if( null== condi_Vehicle ){
				return null;
				
			}
			final RequestResult reqRes = ccTable.createRequestResult(predicate);
			final Adaptation ccRecord;
			try {
				if(reqRes.isSizeGreaterOrEqual(2)){
					return "DSMT_SMT_SID_BU Table has duplicate value for condi_Vehicle "+ condi_Vehicle;
				}
				ccRecord = reqRes.nextAdaptation();
			} finally {
				reqRes.close();
			}
			
			if (ccRecord == null) {
				LOG.debug("No record found for predicate " + predicate
						+ " in table " + ccTable.getTablePath().format());
				return null;
			}
			else{
			
			targetColumnValue = ""+ccRecord.get_int(targetColumn);
			}
		} catch (OperationException ex) {
			
		}
		
		return targetColumnValue;
		
	} 
	/**
	 * @return the sourceColumnPath_1
	 */
	public Path getSourceColumnPath_1() {
		return sourceColumnPath_1;
	}
	/**
	 * @param sourceColumnPath_1 the sourceColumnPath_1 to set
	 */
	public void setSourceColumnPath_1(Path sourceColumnPath_1) {
		this.sourceColumnPath_1 = sourceColumnPath_1;
	}
	/**
	 * @return the sourceColumnPath_2
	 */
	public Path getSourceColumnPath_2() {
		return sourceColumnPath_2;
	}
	/**
	 * @param sourceColumnPath_2 the sourceColumnPath_2 to set
	 */
	public void setSourceColumnPath_2(Path sourceColumnPath_2) {
		this.sourceColumnPath_2 = sourceColumnPath_2;
	}
	
	/**
	 * @return the sourceColumnPath_2
	 */
	
	
	/**
	 * @return the dataSpace
	 */
	public String getDataSpace() {
		return dataSpace;
	}
	/**
	 * @param dataSpace the dataSpace to set
	 */
	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}
	/**
	 * @return the tablePath_1
	 */
	public Path getTablePath_1() {
		return tablePath_1;
	}
	/**
	 * @param tablePath_1 the tablePath_1 to set
	 */
	public void setTablePath_1(Path tablePath_1) {
		this.tablePath_1 = tablePath_1;
	}
	/**
	 * @return the targetColumn
	 */
	public Path getTargetColumn() {
		return targetColumn;
	}
	/**
	 * @param targetColumn the targetColumn to set
	 */
	public void setTargetColumn(Path targetColumn) {
		this.targetColumn = targetColumn;
	}
	
	public static void main(String[] args) {
		String temp="12";
		System.out.println("temp >> "+StringUtils.isEmpty(temp));
		System.out.println("temp >> "+StringUtils.isBlank(temp));
	}
}
