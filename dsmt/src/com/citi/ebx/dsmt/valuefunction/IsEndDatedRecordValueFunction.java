package com.citi.ebx.dsmt.valuefunction;

import java.util.Date;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class IsEndDatedRecordValueFunction implements ValueFunction {

	private static final String ENDDT = "./ENDDT";
	private static Date lastEndDate = new Date(253402232400000L);
	
	@Override
	public Object getValue(final Adaptation record) {
		
		final Date endDate = record.getDate(Path.parse(ENDDT));
		return endDate.before(lastEndDate);
	}

	@Override
	public void setup(final ValueFunctionContext context) {
		
		
	}
}
