package com.citi.ebx.dsmt.valuefunction;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class GenerateSelfReferencingFKValueFunction implements ValueFunction {

	private static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final String NULL = "null";

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private String foreignTableXPath;		//	"/root/GLOBAL_STD/OFD_STD/OU_STD/C_DSMT_FRS_OU_H
	private String sourceColumnName;		// ./C_DSMT_FRS_O_PARNT
	
	private String foreignSetIDColumn;	//./SETID
	private String foreignCodeColumn;		// ./C_DSMT_FRS_O_CHILD
	private String foreignEffDateColumn;	// ./EFFDT
	
	private static final Path endDateFieldPath = Path.parse("./ENDDT");
	
	@Override
	public Object getValue(Adaptation record) {

		try {
				
				Path foreignTablePath = Path.parse(foreignTableXPath); 
				Path sourceColumn = Path.parse(sourceColumnName);					
				Path foreignColumn = Path.parse(foreignCodeColumn);					
				
				
				Object predicateValueObject = record.get(sourceColumn);
				String sourceValue = null;
				
					if(predicateValueObject instanceof String){
						sourceValue = record.getString(sourceColumn);
					}
					else if (predicateValueObject instanceof BigDecimal){
						sourceValue = String.valueOf((BigDecimal)record.get(sourceColumn));
					}
				
					// set foreign key to null for root node
					if(null==sourceValue){
						return null;
					}
						
					// final String statusPredicate = status.format() + " = 'A'";
					final String endDatePredicate = "date-equal(" + endDateFieldPath.format() + ",'9999-12-31') or osd:is-null(" + endDateFieldPath.format() + ")";
	
					String predicateString = foreignColumn.format() + "= '" + sourceValue + "' and " + "(" + endDatePredicate + ")" ; 
					
					// + "' and (./ENDDT = '9999-12-31' OR ./ENDDT IS NULL) ";
					LOG.debug("predicate String = " + predicateString );
					AdaptationTable table = record.getContainer().getTable(foreignTablePath);
					RequestResult reqRes = table.createRequestResult(predicateString);
				
			Adaptation rec = reqRes.nextAdaptation();
			try {
				
				LOG.debug("record " + rec);
				
				Path foreignCodeCol = Path.parse(foreignCodeColumn);	
				Path foreignEffDateCol = Path.parse(foreignEffDateColumn);
				
				Path foreignSetIDCol  = null;
				if(null != foreignSetIDColumn){
					foreignSetIDCol  = Path.parse(foreignSetIDColumn);
				}
			
				String foreignCodeColString = rec.getString(Path.SELF.add(foreignCodeCol));
				
				Date effectiveDate = rec.getDate(Path.SELF.add(foreignEffDateCol));
				String foreignEffDateColString = getDateInFormat(effectiveDate, DATE_FORMAT);
				
				String foreignSetIDColString = null;
				if(null != foreignSetIDCol){
					foreignSetIDColString = rec.getString(Path.SELF.add(foreignSetIDCol));
				}
				
				String foreignKey = null;
				
				if(null != foreignSetIDColString){
					foreignKey = foreignKey + foreignSetIDColString + DELIMITER;
				}
				if(null != foreignCodeColString){
					foreignKey = foreignKey + foreignCodeColString + DELIMITER;
				}
				if(null != foreignEffDateColString){
					foreignKey = foreignKey + foreignEffDateColString + DELIMITER;
				}
				
				foreignKey = removeNulls(foreignKey);
				
				
				LOG.debug("foreign key is " + foreignKey + " for " + sourceColumnName + " = " + sourceValue);
				
				return String.valueOf(foreignKey);
			} catch (Exception e) {
				LOG.error("Exception = " + e.getMessage() );
				return null;	
			}
			finally {
				reqRes.close();
			}
		} 
		catch (Exception e) {
			LOG.error("Exception " +  e.getMessage() );
			return null;	
		}
		
	}

	private String getDateInFormat(Date date, String format){
		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	private String removeNulls(String value) {
		
		String foreignKey = null;
		if (null != value && value.contains(NULL)){
			foreignKey = value.replaceAll(NULL, "");
		}
		else 
			foreignKey = value;
		
		if(null != foreignKey && foreignKey.endsWith(DELIMITER)){
			foreignKey = foreignKey.substring(0, foreignKey.length() - DELIMITER.length());
		}
		
		return foreignKey;
	}

	
	public String getForeignTableXPath() {
		return foreignTableXPath;
	}

	public void setForeignTableXPath(String foreignTableXPath) {
		this.foreignTableXPath = foreignTableXPath;
	}

	

	public Path getEndDateFieldPath() {
		return endDateFieldPath;
	}

	public String getSourceColumnName() {
		return sourceColumnName;
	}



	public void setSourceColumnName(String sourceColumnName) {
		this.sourceColumnName = sourceColumnName;
	}


	
	
	@Override
	public void setup(ValueFunctionContext arg0) {
	
	}
	
	
	public String getForeignSetIDColumn() {
		return foreignSetIDColumn;
	}

	public void setForeignSetIDColumn(String foreignSetIDColumn) {
		this.foreignSetIDColumn = foreignSetIDColumn;
	}

	public String getForeignCodeColumn() {
		return foreignCodeColumn;
	}

	public void setForeignCodeColumn(String foreignCodeColumn) {
		this.foreignCodeColumn = foreignCodeColumn;
	}

	public String getForeignEffDateColumn() {
		return foreignEffDateColumn;
	}

	public void setForeignEffDateColumn(String foreignEffDateColumn) {
		this.foreignEffDateColumn = foreignEffDateColumn;
	}

	public static void main(String[] args) {
		
		// System.out.println(new GenerateForeignKeyValueFunction().getDateInFormat(new Date(), "yyyy-MM-dd"));
		
		System.out.println(new GenerateSelfReferencingFKValueFunction().removeNulls("0000000001|2006-01-01"));
	}

	private static final String DELIMITER = "|";

	
}
