package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class ExtendedSubstringValueFunction implements ValueFunction {

	private String inputFieldID1;
	private Integer substringLength = 255;
	private String inputFieldID2; // "./DESCR"

	public String getInputFieldID1() {
		return inputFieldID1;
	}

	public void setInputFieldID1(String inputFieldID1) {
		this.inputFieldID1 = inputFieldID1;
	}

	public String getInputFieldID2() {
		return inputFieldID2;
	}

	public void setInputFieldID2(String inputFieldID2) {
		this.inputFieldID2 = inputFieldID2;
	}

	public Integer getSubstringLength() {
		return substringLength;
	}

	public void setSubstringLength(Integer substringLength) {
		this.substringLength = substringLength;
	}

	@Override
	public Object getValue(Adaptation adaptation) {

		String substring = null;
		String desc = "N/A";

		try {

			Path inputFieldPath = Path.parse(inputFieldID1);
			String inputField1 = adaptation.getString(inputFieldPath);
			Path descrFieldPath = Path.parse(inputFieldID2);
			String inputField2 = adaptation.getString(descrFieldPath);

			if ((inputField2 != null && inputField2.equalsIgnoreCase(desc))) {

				if (null != inputFieldID1) {
					inputField1 = adaptation.getString(Path
							.parse(inputFieldID1));
				}

				if (null != inputField1
						&& inputField1.length() > substringLength) {

					substring = (inputField1.substring(0, substringLength))
							.trim();
				} else if (null != inputField1) {

					substring = inputField1.trim();
				}
				// System.out.println(naicsSubstr);
			} else {
				if (null != inputField2) {
					substring = adaptation.getString(Path.parse(inputFieldID2));
					
				}
			}
		} catch (Exception e) {

			LOG.debug("Exception found while updating field " + inputFieldID1
					+ ", Exception message = " + e.getMessage());
		}
		return substring;
	}

	@Override
	public void setup(ValueFunctionContext arg0) {
		// Not needed

	}

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	

}
