package com.citi.ebx.dsmt.valuefunction;

import com.citi.ebx.goc.path.GOCPartnerSystemPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
/**
 * Looks up values from a tables linked to one another via foreign keys 
 */


public class ReferColumnForSmart implements ValueFunction {
	private Path sourceColumnPath_1;
	private Path fkColumnPath;
	private Path sourceColumnPath_2;
	private Path sourceColumnPath_3;
	private Path tablePath;
	
//	ProcedureContext pContext;

	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	
	public String getSourceColumnPath_2() {
		return sourceColumnPath_2.format();
	}

	public void setSourceColumnPath_2(Path sourceColumnPath_2) {
		this.sourceColumnPath_2 = sourceColumnPath_2;
	}

	/**
	 * Get the relative path of the source column that you're pulling
	 * from the other table (i.e. "./GL_CLA").
	 * 
	 * @return the path of the source column
	 */
	public String getSourceColumnPath_1() {
		return sourceColumnPath_1.format();
	}

	/**
	 * Set the relative path of the source column that you're pulling
	 * from the other table (i.e. "./GL_CLA").
	 * 
	 * @param sourceColumnPath the path of the source column
	 */
	public void setSourceColumnPath_1(String sourceColumnPath_1) {
		this.sourceColumnPath_1 = Path.parse(sourceColumnPath_1);
	}

	/**
	 * Get the relative path of the foreign key column (i.e. "./LC_GL_ACCOUNT").
	 * 
	 * @return the path of the foreign key column
	 */
	public String getFkColumnPath() {
		return fkColumnPath.format();
	}

	/**
	 * Set the relative path of the foreign key column (i.e. "./LC_GL_ACCOUNT").
	 * 
	 * @param fkColumnPath the path of the foreign key column
	 */
	public void setFkColumnPath(String fkColumnPath) {
		this.fkColumnPath = Path.parse(fkColumnPath);
	}

	@Override
	public Object getValue(Adaptation record) {
		// Get the foreign key node
		final SchemaNode fkNode = record.getSchemaNode().getNode(fkColumnPath);
		// Follow the foreign key to get the record that it's pointing to
		final Adaptation linkedRecord = fkNode.getFacetOnTableReference().getLinkedRecord(record); 
				
//		final AdaptationTable sidEnhancementTable = metadataDataSet.getTable(GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
		if (linkedRecord != null) {
			// Get the value from the linked record 
			String sid = getSID(linkedRecord,sourceColumnPath_1);
			String frsbu = getBU(linkedRecord,sourceColumnPath_2);	
	        
			final String lcc = linkedRecord.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD);
		    String country=null;
		    String branch=null;
		    String predicateFlag=null;
		    if(tablePath.format().toString().equals("/root/SMART_REFERENCE_DATA/ASIA/DSMT_SMT_SID_BU")){
			country = record.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_ASIA._Country);
			branch = record.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_ASIA._Branch);
			predicateFlag="ASIA";
			
			
			if(country == null && branch==null)
				return "";
			}
			
			if(tablePath.format().toString().equals("/root/SMART_REFERENCE_DATA/JAPAN/DSMT_SMT_SID_BU")){
				country = record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_JAPAN._Country);
			branch = record.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_JAPAN._Branch);
			predicateFlag="JAPAN";
			
			if(country == null && branch==null)
				return "";
			}
			
			if(tablePath.format().toString().equals("/root/SMART_REFERENCE_DATA/NORTH_AMERICA/DSMT_SMT_SID_BU")){
				country = record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM._Country);
			branch = record.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM._Branch);
			predicateFlag="NAM";
			if(country == null && branch==null)
				return "";
			
			}
			
			if(tablePath.format().toString().equals("/root/SMART_REFERENCE_DATA/CEECA/DSMT_SMT_SID_BU")){
				country = record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_CEECA._Country);
			branch = record.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_CEECA._Branch);
			predicateFlag="CEECA";
			if(country == null && branch==null)
				return "";
			
			}
			
			if(tablePath.format().toString().equals("/root/SMART_REFERENCE_DATA/EUROPE/DSMT_SMT_SID_BU")){
				country = record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_WEUROPE._Country);
			branch = record.getString(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_WEUROPE._Branch);
			predicateFlag="EUROPE";
			
			if(country == null && branch==null)
				return "";
			}
			
			if(tablePath.format().toString().equals("/root/SMART_REFERENCE_DATA/LATIN_AMERICA/DSMT_SMT_SID_BU")){
				final Adaptation metadataDataSet;
				try {
					country = record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._Country);
				    branch = record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM._Branch);

					metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(record.getHome().getRepository());
					final AdaptationTable sidEnhancementTable = metadataDataSet.getTable(GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
					
					final PrimaryKey sidEnhancementTablePK = sidEnhancementTable.computePrimaryKey(new Object[] {sid});
					
					final Adaptation sidEnhancementTableRecord = sidEnhancementTable.lookupAdaptationByPrimaryKey(sidEnhancementTablePK);
					String region=sidEnhancementTableRecord.get(GOCWFPaths._C_DSMT_SID_ENH._Region).toString();
					LOG.debug(" LatamSmartRecordConstraint-->checkRecord  region value is " + region);
					if("LATAM".equalsIgnoreCase(region)){
						predicateFlag="LATAM";
					}
						if("CAFIS".equalsIgnoreCase(region)){
							predicateFlag="CAFIS";	
						}
					
					
				} catch (OperationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
							    
			    
			    if(country == null && branch==null)
					return "";
			}
			String OUCPrefix  = getOUCPrefix(record.getHome().getRepository(),sid,frsbu,country,branch,sourceColumnPath_3,tablePath,predicateFlag);
			
			if(OUCPrefix==null){
				return "";
			}else{
					if(null!=lcc){
						return OUCPrefix+lcc;
					}else{
						return OUCPrefix;
					}
			}
			
		}
		return null;
	}

	private String getCountry() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setSourceColumnPath_1(Path sourceColumnPath_1) {
		this.sourceColumnPath_1 = sourceColumnPath_1;
	}

	private static String getSID(final Adaptation gocRecord,Path sourceColumnPath_1) {

		final Adaptation sidRecord = Utils.getLinkedRecord(gocRecord,
				sourceColumnPath_1);
		if (sidRecord == null) {
			LOG.error("No SID found for record " + gocRecord.getOccurrencePrimaryKey().format());
			return null;
		}

		return sidRecord.getString(
				sourceColumnPath_1);
	}
	
	private static String getBU(final Adaptation gocRecord,Path sourceColumnPath_2) {

		final Adaptation sidRecord = Utils.getLinkedRecord(gocRecord,
				sourceColumnPath_2);
		if (sidRecord == null) {
			LOG.error("No SID found for record " + gocRecord.getOccurrencePrimaryKey().format());
			return null;
		}

		return sidRecord.getString(
				sourceColumnPath_2);
	}
	private static String getOUCPrefixID(final Adaptation metadataDataSet, 
			final String sid, String frsbu ,String country,String branch, Path sourceColumnPath_3,Path tablePath , String predicateFlag) {
			
		
//		final AdaptationTable ccTable =	metadataDataSet.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU.getPathInSchema());
		AdaptationTable ccTable =	metadataDataSet.getTable(tablePath);
		if(predicateFlag.equalsIgnoreCase("CAFIS")){
		ccTable =	metadataDataSet.getTable(GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU.getPathInSchema());
		}
		String predicate=null;
		if(predicateFlag.equals("ASIA")){
		predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SID_BU._SID.format()
					+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
		if(branch==null || branch ==""){
		predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SID_BU._SID.format()
		+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\"";
		if(country==null || country==""){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";	
		}
		if((country==null || country=="")&&(branch==null||branch=="")){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_ASIA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\"";	
		}
		}
		} 
		
		if(predicateFlag.equals("EUROPE")){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_EUROPE_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_EUROPE_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_EUROPE_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_EUROPE_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
			if(branch==null || branch==""){
				predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_EUROPE_DSMT_SMT_SID_BU._SID.format()
				+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_EUROPE_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_EUROPE_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\""; 	
			}
		if(country==null || country==""){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_EUROPE_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_EUROPE_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_EUROPE_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";	
		}
		if((country==null || country=="")&&(branch==null&&branch=="")){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_EUROPE_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_EUROPE_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\"";	
		}
		} 
		
		if(predicateFlag.equals("CEECA")){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_CEECA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_CEECA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_CEECA_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_CEECA_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
			 
		if(branch==null||branch==""){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_CEECA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_CEECA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_CEECA_DSMT_SMT_SID_BU._Country.format() + "=\"" + country + "\"";
		}
		if(country==null || country==""){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_CEECA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_CEECA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_CEECA_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
		}
		if((country==null || country=="")&&(branch==null || branch=="")){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_CEECA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_CEECA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\"";
		}
		}
		if(predicateFlag.equals("JAPAN")){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_JAPAN_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_JAPAN_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_JAPAN_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_JAPAN_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
		if(branch==null||branch==""){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_JAPAN_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_JAPAN_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_JAPAN_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\"";
		}
		if(country==null||country==""){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_JAPAN_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_JAPAN_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_JAPAN_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
		}
		}
		
		if(predicateFlag.equals("NAM")){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
		if(branch==null||branch==""){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\"";
		}
		if(country==null||country==""){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
		}
		if((country==null||country=="")&&(branch==null||branch=="")){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_NORTH_AMERICA_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
		}
		}
		
		if(predicateFlag.equals("LATAM")){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
		if(branch==null||branch==""){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\"";
		}
		if(country==null||country==""){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " +GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
		}
		if((country==null||country=="")&&(branch==null||branch=="")){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\"";
		}
		}
		if(predicateFlag.equals("CAFIS")){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
		if(branch==null||branch==""){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._Country.format() + "=\"" + country+"\"";
		}
		if(country==null||country==""){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\" and " +GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._Branch.format() + "=\"" + branch + "\"";
		}
		if((country==null||country=="")&&(branch==null||branch=="")){
			predicate = GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_CAFIS_DSMT_SMT_SID_BU._SID.format()
			+ "=\"" + sid + "\" and " + GOCPartnerSystemPaths._SMART_REFERENCE_DATA_LATIN_AMERICA_DSMT_SMT_SID_BU._BU.format() + "=\"" + frsbu+"\"";
		}
		}
		String response=null;
		LOG.debug(predicate);

		final RequestResult reqRes = ccTable.createRequestResult(predicate);
		final Adaptation ccRecord;
		try {
			ccRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (ccRecord == null) {
			LOG.debug("No record found for predicate " + predicate
					+ " in table " + ccTable.getTablePath().format());
			return null;
		}
		
			response = ccRecord.getString(sourceColumnPath_3);
		
		return response;
	}
	

	public static String getOUCPrefix(Repository repo,String sid,String frsbu ,String country,String branch, Path sourceColumnPath_3,Path tablePath, String predicateFlag)
	{
		
		final Adaptation metadataDataSet ;
		String OUCPrefix=null;
		try {
//			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(repo);
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSetGOCPartnerRefData(repo);
			OUCPrefix= getOUCPrefixID (metadataDataSet,sid,frsbu,country,branch,sourceColumnPath_3,tablePath,predicateFlag);
			
		} catch (OperationException ex) {
			
		}
		
		return OUCPrefix;
		
	} 
	
	
	
	
	

	

	@Override
	public void setup(ValueFunctionContext context) {
		// TODO Auto-generated method stub
		
	}

	

	/**
	 * @param sourceColumnPath_3 the sourceColumnPath_3 to set
	 */
	public void setSourceColumnPath_3(Path sourceColumnPath_3) {
		this.sourceColumnPath_3 = sourceColumnPath_3;
	}

	/**
	 * @return the sourceColumnPath_3
	 */
	public Path getSourceColumnPath_3() {
		return sourceColumnPath_3;
	}

	public Path getTablePath() {
		return tablePath;
	}

	public void setTablePath(Path tablePath) {
		this.tablePath = tablePath;
	}


}
