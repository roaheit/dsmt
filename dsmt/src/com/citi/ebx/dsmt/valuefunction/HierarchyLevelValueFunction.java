package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class HierarchyLevelValueFunction implements ValueFunction {
	private Path parentFKField = Path.parse("./Product_FK");
	private SchemaNode levelNode;
	
	public String getParentFKField() {
		return parentFKField.format();
	}

	public void setParentFKField(String parentFKField) {
		this.parentFKField = Path.parse(parentFKField);
	}

	@Override
	public Object getValue(Adaptation record) {
		String parentPK = record.getString(Path.SELF.add(parentFKField));
		if (parentPK == null) {
			return 1;
		}
		String primaryKey= record.getOccurrencePrimaryKey().format();
		if(primaryKey.equals(parentPK))
		{
			return 1;
		}
		
		AdaptationTable table = record.getContainerTable();
		
		Adaptation parentRecord = table.lookupAdaptationByPrimaryKey(
				PrimaryKey.parseString(parentPK));
		if (parentRecord == null) {
			return 1;
		}
		return parentRecord.get_int(levelNode.getPathInAdaptation()) + 1;
	}

	@Override
	public void setup(ValueFunctionContext context) {
		levelNode = context.getSchemaNode();
		if (parentFKField == null) {
			context.addError("Must specify parentFKField.");
		} else {
			SchemaNode parentFKNode = levelNode.getNode(Path.PARENT.add(parentFKField));
			if (parentFKNode == null) {
				context.addError(parentFKField.format() + " is not a node in the schema.");
			}
		}
	}
}
