package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class ParentNodeValueFunction implements ValueFunction {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path parentFKField = Path.parse("./Product_FK");
	private Path parentField = Path.parse("./PARENT_ID");
	private Path productCodeField = Path.parse("./C_DSMT_PRODUCT_TYP");
	private SchemaNode parentNode;

	public String getParentFKField() {
		return parentFKField.format();
	}

	public void setParentFKField(String parentFKField) {
		this.parentFKField = Path.parse(parentFKField);
	}

	@Override
	public Object getValue(Adaptation record) {
		String parentPK = record.getString(Path.SELF.add(parentFKField));
		if (parentPK == null) {
			return null;
		}
		AdaptationTable table = record.getContainerTable();
		Adaptation parentRecord = table.lookupAdaptationByPrimaryKey(PrimaryKey
				.parseString(parentPK));
		return parentRecord.createValueContext().getValue(productCodeField);
	}

	@Override
	public void setup(ValueFunctionContext context) {
		parentNode = context.getSchemaNode();
		if (parentField == null) {
			context.addError("Must specify parentField.");
		} else {
			SchemaNode parentNodeValue = parentNode.getNode(parentField);
			LOG.info("parentNodeValue" + parentNodeValue);
		}
	}
}
