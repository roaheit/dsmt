package com.citi.ebx.dsmt.valuefunction;

import java.util.ArrayList;
import java.util.List;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class AccountStandardGLTableValueFunction implements ValueFunction{

	
	
	private String sourceColumnName;
	private String destinatioColumn;	
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	
	@Override
	public Object getValue(Adaptation record) {
		// Get the foreign key node
		String predicateValue = record.getString(getPath(sourceColumnName));
		
		String returnValue=null ;
		if(destinatioColumn.equalsIgnoreCase(DSMTConstants.Flip_Sign)){
			
			if(getFlipSignListY().contains(predicateValue)){
				returnValue="Y";
			}
			else if(getFlipSignListN().contains(predicateValue)){
				returnValue="N";
			}
				
			
		}else if(destinatioColumn.equalsIgnoreCase(DSMTConstants.BALANCE_FORWARD_INDICATOR)){
			
			if(getBalanceForwardListY().contains(predicateValue)){
				returnValue="Y";
			}
			else if(getBalanceForwardListN().contains(predicateValue)){
				returnValue="N";
			}
			
			
		} else if(destinatioColumn.equalsIgnoreCase(DSMTConstants.BS_OB_INDICATOR)){
			if(getBSOSListBS().contains(predicateValue)){
				returnValue="BS";
			}
			else if(getBSOSListOS().contains(predicateValue)){
				returnValue="OB";
			}
			
		} 
		
		
		
		
		return returnValue;
	}
	
	
	
private static List<String> getFlipSignListY(){
		//Account type is L, R, U, X
		final List<String> flipSignListY = new ArrayList<String>();
		
		flipSignListY.add("K");
		flipSignListY.add("L");
		flipSignListY.add("R");
		flipSignListY.add("U");
		flipSignListY.add("X");	
		return flipSignListY;
	}
private static List<String> getFlipSignListN(){
	//Account type A, C, E, J, K, N, S, T, Y, P
	final List<String> flipSignListN = new ArrayList<String>();
	
	flipSignListN.add("A");
	flipSignListN.add("C");
	flipSignListN.add("E");
	flipSignListN.add("J");	
	//flipSignListN.add("K");	
	flipSignListN.add("N");	
	flipSignListN.add("S");	
	flipSignListN.add("T");	
	flipSignListN.add("Y");
	flipSignListN.add("P");
	flipSignListN.add("M");
	return flipSignListN;
}

private static List<String> getBalanceForwardListY(){
	//Account type A, L, U, J, K, S 
	final List<String> balanceForwardListY = new ArrayList<String>();
	balanceForwardListY.add("A");
	balanceForwardListY.add("L");	
	balanceForwardListY.add("U");
	balanceForwardListY.add("J");
	balanceForwardListY.add("K");
	balanceForwardListY.add("S");
	return balanceForwardListY;
}
private static List<String> getBalanceForwardListN(){
	//Account type C, E, M, N, P, R, T, X, Y
	final List<String> balanceForwardListN = new ArrayList<String>();			
	balanceForwardListN.add("C");
	balanceForwardListN.add("E");
	balanceForwardListN.add("M");
	balanceForwardListN.add("N");
	balanceForwardListN.add("P");	
	balanceForwardListN.add("R");
	balanceForwardListN.add("T");
	balanceForwardListN.add("X");
	balanceForwardListN.add("Y");
	return balanceForwardListN;
}

private static List<String> getBSOSListBS(){
	
	final List<String> bSOSListBS = new ArrayList<String>();
	//Account type A, L, U, R, E, T
	bSOSListBS.add("A");
	bSOSListBS.add("L");
	bSOSListBS.add("U");
	bSOSListBS.add("R");	
	bSOSListBS.add("E");	
	bSOSListBS.add("T");	
	return bSOSListBS;
}

private static List<String> getBSOSListOS(){

final List<String> bSOSListOS = new ArrayList<String>();

//Account type J, K, S, P, X, Y
bSOSListOS.add("J");	
bSOSListOS.add("K");	
bSOSListOS.add("S");	
bSOSListOS.add("P");
bSOSListOS.add("X");
bSOSListOS.add("Y");
return bSOSListOS;
}


	public void setup(ValueFunctionContext arg0) {
		// Not needed

	}
	
private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}

/**
 * @return the sourceColumnName
 */
public String getSourceColumnName() {
	return sourceColumnName;
}

/**
 * @param sourceColumnName the sourceColumnName to set
 */
public void setSourceColumnName(String sourceColumnName) {
	this.sourceColumnName = sourceColumnName;
}

/**
 * @return the destinatioColumn
 */
public String getDestinatioColumn() {
	return destinatioColumn;
}

/**
 * @param destinatioColumn the destinatioColumn to set
 */
public void setDestinatioColumn(String destinatioColumn) {
	this.destinatioColumn = destinatioColumn;
}

	

}
