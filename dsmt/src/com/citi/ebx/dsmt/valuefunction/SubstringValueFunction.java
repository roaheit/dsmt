package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class SubstringValueFunction implements ValueFunction {

	private String inputFieldID;
	private Integer substringLength = 255; 
	
	public String getInputFieldID() {
		return inputFieldID;
	}

	public void setInputFieldID(String inputFieldID) {
		this.inputFieldID = inputFieldID;
	}

	public Integer getSubstringLength() {
		return substringLength;
	}

	public void setSubstringLength(Integer substringLength) {
		this.substringLength = substringLength;
	}
	
	@Override
	public Object getValue(Adaptation adaptation) {
		
		String substring = null;
		try{
			Path inputFieldPath = Path.parse(inputFieldID);
			String inputField = adaptation.getString(inputFieldPath);
			
			if(null != inputFieldID){
				inputField = adaptation.getString(Path.parse(inputFieldID)); 
			}
			
			
			if(null != inputField && inputField.length() > substringLength){
				
				substring = (inputField.substring(0, substringLength)); // .trim();	
			}else if (null != inputField){
				
				substring = inputField; //trim();
			}
				// System.out.println(naicsSubstr);
		}catch (Exception e) {
			
			LOG.debug("Exception found while calculating substring for field "  +inputFieldID + ", Exception message = " +e.getMessage());
		}
		return substring;
	}

	/*
	public static void main(String[] args) {
		
		String str = "neelmani";
		System.out.println(str.substring(0, 5));
	}*/
	
	
	@Override
	public void setup(ValueFunctionContext arg0) {
		// Not needed

	}
	
	protected final LoggingCategory LOG = LoggingCategory.getKernel();
	

}
