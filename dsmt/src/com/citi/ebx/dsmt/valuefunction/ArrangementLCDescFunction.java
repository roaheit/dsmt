package com.citi.ebx.dsmt.valuefunction;

import java.math.BigDecimal;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class ArrangementLCDescFunction implements ValueFunction {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path arrangementLCStatusPath = Path.parse("/root/GLOBAL_STD/ENT_STD/F_ARNG_LIFE/C_DSMT_ARR_STA");
	private Path arrangementLCStatusCode = Path.parse("./C_DSMT_LIFE_STA_CD");
	private Path statusDesc = Path.parse("./C_DSMT_LONG_DESC");

	@Override
	public Object getValue(Adaptation record) {

		String statDesc = null;
		BigDecimal code = (BigDecimal)record.get(arrangementLCStatusCode);
		if (code == null) {
			return "Status Code is not found ";
		}
		
		String predicate = arrangementLCStatusCode.format() + "= '" + code + "'";
		AdaptationTable table = record.getContainer().getTable(arrangementLCStatusPath);
		RequestResult reqRes = table.createRequestResult(predicate);
		try {
			Adaptation rec = reqRes.nextAdaptation();
			
			try {
				statDesc = rec.getString(Path.SELF.add(statusDesc));
				return statDesc;
			} catch (Exception e) {
				LOG.error("No reference found for column " + arrangementLCStatusCode + " in " + arrangementLCStatusPath  + " Exception = " + e.getMessage() );
				return "Not Available";	
			}
			
		} finally {
			reqRes.close();
		}
	}

	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub

	}

}
