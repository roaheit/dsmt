package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class DomainNodeValueFunction implements ValueFunction{

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path domainFKField = Path.parse("./DOMAIN_FK");
	private Path parentField = Path.parse("./C_DSMT_DOMAIN_CD");
	private SchemaNode parentNode;

	public String getDomainFKField() {
		return domainFKField.format();
	}

	public void setDomainFKField(String domainFKField) {
		this.domainFKField = Path.parse(domainFKField);
	}

	@Override
	public Object getValue(Adaptation record) {
		String domainPK = record.getString(Path.SELF.add(domainFKField));
	//	LOG.info("domainPK = " + domainPK);
		if (domainPK == null) {
			return null;
		}
		String domainRecord = domainPK.substring(6,10);
		return domainRecord;
	}

	@Override
	public void setup(ValueFunctionContext context) {
		parentNode = context.getSchemaNode();
		if (parentField == null) {
			context.addError("Must specify parentField.");
		} else {
			SchemaNode parentNodeValue = parentNode.getNode(parentField);
			LOG.info("parentNodeValue" + parentNodeValue);
		}
	}

}
