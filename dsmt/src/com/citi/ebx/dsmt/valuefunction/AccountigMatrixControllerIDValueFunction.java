package com.citi.ebx.dsmt.valuefunction;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class AccountigMatrixControllerIDValueFunction implements ValueFunction  {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private String tableToUpdate;
	public String getTableToUpdate() {
		return tableToUpdate;
	}
	public void setTableToUpdate(String tableToUpdate) {
		this.tableToUpdate = tableToUpdate;
	}
	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Object getValue(Adaptation record) {
		
			Repository rep = record.getHome().getRepository();
			Adaptation metadataDataSet = null;
			try {
				metadataDataSet = EBXUtils.getMetadataDataSet(rep,
						AccountibilityMatrixConstants.DATASPACE_Accounting_Matrix,
						AccountibilityMatrixConstants.DATASET_Accounting_Matrix);
			} catch (OperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			final Path tableUpdate = Path.parse(tableToUpdate);
			LOG.info("Table to Update----"+tableToUpdate);
			AdaptationTable targetTable = metadataDataSet.getTable(tableUpdate);

			final String predicate=AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID
			.format() + "='" + record.getOccurrencePrimaryKey().format()+"'";
			RequestResult res=targetTable.createRequestResult(predicate);
			String controllerID="";
			if(res.getSize()>0)
			{
				for (Adaptation recordController; (recordController = res.nextAdaptation()) != null;){
					controllerID+=recordController.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID)+"/";
				}
				controllerID=controllerID.substring(0,controllerID.length()-1);
			}
			return controllerID;
		}
}
