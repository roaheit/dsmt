package com.citi.ebx.dsmt.valuefunction;

import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class ReferColumnKeyByDifferentDataSpace implements ValueFunction {
	private Path sourceColumnPath_1;
	private Path sourceColumnPath_2;
	private Path targetColumn;
	private Path fkColumnPath;
	private Path tablePath_1;
	private String dataSpace;
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Object getValue(Adaptation record) {
		// Get the foreign key node
		final SchemaNode fkNode = record.getSchemaNode().getNode(fkColumnPath);
		
		final Adaptation linkedRecord = fkNode.getFacetOnTableReference().getLinkedRecord(record);
		
		if (linkedRecord != null) {
			String gocLinkedRecord = getGOCLinkedRecord(linkedRecord,sourceColumnPath_1);
			String targetRecord  = getTargetRecord(record.getHome().getRepository(),gocLinkedRecord,sourceColumnPath_2,tablePath_1);
			return targetRecord;
		}
		return null;
	}
	
	private static String getGOCLinkedRecord(final Adaptation gocRecord,Path sourceColumnPath_1) {

		final Adaptation gocLinkedRecord = Utils.getLinkedRecord(gocRecord,
				sourceColumnPath_1);
		if (gocLinkedRecord == null) {
			LOG.error("No data found for"+sourceColumnPath_1+" record " + gocRecord.getOccurrencePrimaryKey().format());
			return null;
		}

		return gocLinkedRecord.getString(
				sourceColumnPath_1);
	}
	
	public  String getTargetRecord(Repository repo,String sourceValue,Path sourceColumnPath_2,Path tablePath_1)
	{
		
		final Adaptation metadataDataSet ;
		String targetColumnValue=null;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(repo, dataSpace, dataSpace);
			final AdaptationTable ccTable =	metadataDataSet.getTable(
					tablePath_1);
			final String predicate = sourceColumnPath_2.format()
					+ "=\"" + sourceValue + "\" " ;
			final RequestResult reqRes = ccTable.createRequestResult(predicate);
			final Adaptation ccRecord;
			try {
				ccRecord = reqRes.nextAdaptation();
			} finally {
				reqRes.close();
			}
			if (ccRecord == null) {
				LOG.debug("No record found for predicate " + predicate
						+ " in table " + ccTable.getTablePath().format());
				return null;
			}
			
			targetColumnValue = ccRecord.getString(targetColumn);
		} catch (OperationException ex) {
			
		}
		
		return targetColumnValue;
		
	} 
	/**
	 * @return the sourceColumnPath_1
	 */
	public Path getSourceColumnPath_1() {
		return sourceColumnPath_1;
	}
	/**
	 * @param sourceColumnPath_1 the sourceColumnPath_1 to set
	 */
	public void setSourceColumnPath_1(Path sourceColumnPath_1) {
		this.sourceColumnPath_1 = sourceColumnPath_1;
	}
	/**
	 * @return the fkColumnPath
	 */
	public Path getFkColumnPath() {
		return fkColumnPath;
	}
	/**
	 * @param fkColumnPath the fkColumnPath to set
	 */
	public void setFkColumnPath(Path fkColumnPath) {
		this.fkColumnPath = fkColumnPath;
	}
	/**
	 * @return the sourceColumnPath_2
	 */
	public Path getSourceColumnPath_2() {
		return sourceColumnPath_2;
	}
	/**
	 * @param sourceColumnPath_2 the sourceColumnPath_2 to set
	 */
	public void setSourceColumnPath_2(Path sourceColumnPath_2) {
		this.sourceColumnPath_2 = sourceColumnPath_2;
	}
	
	
	/**
	 * @return the dataSpace
	 */
	public String getDataSpace() {
		return dataSpace;
	}
	/**
	 * @param dataSpace the dataSpace to set
	 */
	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}
	/**
	 * @return the tablePath_1
	 */
	public Path getTablePath_1() {
		return tablePath_1;
	}
	/**
	 * @param tablePath_1 the tablePath_1 to set
	 */
	public void setTablePath_1(Path tablePath_1) {
		this.tablePath_1 = tablePath_1;
	}
	/**
	 * @return the targetColumn
	 */
	public Path getTargetColumn() {
		return targetColumn;
	}
	/**
	 * @param targetColumn the targetColumn to set
	 */
	public void setTargetColumn(Path targetColumn) {
		this.targetColumn = targetColumn;
	}
}
