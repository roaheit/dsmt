package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class CityStatusDescFunction implements ValueFunction {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path cityStatusPath = Path.parse("/root/GLOBAL_STD/ENT_STD/F_CITY_TO_CNTRY/C_DSMT_CITY_STA");
	private Path statusCodePath = Path.parse("./C_DSMT_CITY_STA_CD");
	private Path statusDesc = Path.parse("./C_DSMT_LONG_DESC");
	
	@Override
	public Object getValue(Adaptation record) {

		String code = record.getString(statusCodePath);
		if (code == null) {
			return "Status Code not found";
		}
		
		String predicate = statusCodePath.format() + "= '" + code + "'";
		AdaptationTable table = record.getContainer().getTable(cityStatusPath);
		RequestResult reqRes = table.createRequestResult(predicate);
		try {
			Adaptation rec = reqRes.nextAdaptation();
			try {
				String statDesc = rec.getString(Path.SELF.add(statusDesc));
				return statDesc;
			} catch (PathAccessException e) {
				LOG.error("No reference found for column " + statusCodePath + " in " + cityStatusPath  + " Exception = " + e.getMessage() );
				return "Not Available";	
			}
			
		} finally {
			reqRes.close();
		}

	}

	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub

	}

}
