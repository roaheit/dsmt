package com.citi.ebx.dsmt.valuefunction;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class HierarchyLeafFlagValueFunction implements ValueFunction {
	private static final String LEAF_VALUE = "L";
	private static final String PARENT_VALUE = "P";
	private Path endDateFieldPath = DSMTConstants.endDateFieldPath;
	
	private Path parentFKField = Path.parse("./Product_FK");
	private SchemaNode leafFlagNode;
	
	public String getParentFKField() {
		return parentFKField.format();
	}

	public void setParentFKField(String parentFKField) {
		this.parentFKField = Path.parse(parentFKField);
	}
	@Override
	public Object getValue(Adaptation record) {
		AdaptationTable table = record.getContainerTable();
		String recordPK = record.getOccurrencePrimaryKey().format();
		
		RequestResult reqRes = table.createRequestResult(Path.SELF.add(parentFKField).format() + "='" + recordPK + "' and date-equal(" + endDateFieldPath.format() + ", '9999-12-31')");
		Adaptation child = null;
		try {
			child = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		return child == null ? LEAF_VALUE : PARENT_VALUE;
	}

	@Override
	public void setup(ValueFunctionContext context) {
		leafFlagNode = context.getSchemaNode();
		if (parentFKField == null) {
			context.addError("Must specify parentFKField.");
		} else {
			SchemaNode parentFKNode = leafFlagNode.getNode(Path.PARENT.add(parentFKField));
			if (parentFKNode == null) {
				context.addError(parentFKField.format() + " is not a node in the schema.");
			}
		}
	}
}
