package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class GenericParentNodeValueFunction implements ValueFunction {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private String parentFKField;
	private Path CodeField;
	

	public String getParentFKField() {
		return parentFKField;
	}

	public void setParentFKField(String parentFKField) {
		this.parentFKField = parentFKField;
	}

	public String getCodeField() {
		return CodeField.format();
	}

	public void setCodeField(String CodeField) {
		this.CodeField = Path.parse(CodeField);
	}

	@Override
	public Object getValue(Adaptation record) {
		
		Object value = null;
		try {
			String parentPK = record.getString(Path.SELF.add(parentFKField));
			if (parentPK == null) {
				return null;
			}
			AdaptationTable table = record.getContainerTable();
			Adaptation parentRecord = table.lookupAdaptationByPrimaryKey(PrimaryKey
					.parseString(parentPK));
			if(null != parentRecord){
				value = parentRecord.createValueContext().getValue(CodeField);
			}
		} catch (Exception e) {
			LOG.error("Exception caught in GenericParentNodeValueFunction " + e);
		}
		return value;
	}

	@Override
	public void setup(ValueFunctionContext context) {
		//Do nothing
	}
}
