package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.schema.info.SchemaFacetTableRef;

/**
 * Looks up a value from a table that's linked to via a foreign key
 */
public class ReferForeignColumnViaFKFunction implements ValueFunction {
	private Path sourceColumnPath;
	private Path fkColumnPath;

	/**
	 * Get the relative path of the source column that you're pulling
	 * from the other table (i.e. "./GL_CLA").
	 * 
	 * @return the path of the source column
	 */
	public String getSourceColumnPath() {
		return sourceColumnPath.format();
	}

	/**
	 * Set the relative path of the source column that you're pulling
	 * from the other table (i.e. "./GL_CLA").
	 * 
	 * @param sourceColumnPath the path of the source column
	 */
	public void setSourceColumnPath(String sourceColumnPath) {
		this.sourceColumnPath = Path.parse(sourceColumnPath);
	}

	/**
	 * Get the relative path of the foreign key column (i.e. "./LC_GL_ACCOUNT").
	 * 
	 * @return the path of the foreign key column
	 */
	public String getFkColumnPath() {
		return fkColumnPath.format();
	}

	/**
	 * Set the relative path of the foreign key column (i.e. "./LC_GL_ACCOUNT").
	 * 
	 * @param fkColumnPath the path of the foreign key column
	 */
	public void setFkColumnPath(String fkColumnPath) {
		this.fkColumnPath = Path.parse(fkColumnPath);
	}

	@Override
	public Object getValue(Adaptation record) {
		// Get the foreign key node
		final SchemaNode fkNode = record.getSchemaNode().getNode(fkColumnPath);
		// Follow the foreign key to get the record that it's pointing to
		final Adaptation linkedRecord = fkNode.getFacetOnTableReference().getLinkedRecord(record);
		if (linkedRecord != null) {
			// Get the value from the linked record
			return linkedRecord.get(sourceColumnPath);
		}
		return null;
	}

	@Override
	public void setup(ValueFunctionContext context) {
		SchemaFacetTableRef fkTableRef = null;
		if (fkColumnPath == null) {
			context.addError("fkColumnPath parameter is required.");
		} else {
			final SchemaNode tableNode = context.getSchemaNode().getTableNode();
			// Get the node for the foreign column
			final SchemaNode fkColumnNode = tableNode
					.getTableOccurrenceRootNode().getNode(fkColumnPath);
			if (fkColumnNode == null) {
				context.addError(fkColumnPath.format() + " is not a node in the table "
						+ tableNode.getPathInSchema().format() + ".");
			} else {
				// Get the table ref facet, which indicates if it is a foreign key
				fkTableRef = fkColumnNode.getFacetOnTableReference();
				if (fkTableRef == null) {
					context.addError(fkColumnPath.format() + " in the table "
							+ tableNode.getPathInSchema().format() + " is not a foreign key.");
				}
			}
		}
			
		if (sourceColumnPath == null) {
			context.addError("sourceColumnPath parameter is required.");
		} else {
			if (fkTableRef != null) {
				// Get the table node of the table referenced by the foreign key
				final SchemaNode tableNode = fkTableRef.getTableNode();
				// Get the node for the source column in the foreign table
				final SchemaNode sourceColumnNode = tableNode
						.getTableOccurrenceRootNode().getNode(sourceColumnPath);
				if (sourceColumnNode == null) {
					context.addError(sourceColumnPath.format() + " is not a node in the table "
							+ tableNode.getPathInSchema().format() + ".");
				}
			}
		}
	}
}
