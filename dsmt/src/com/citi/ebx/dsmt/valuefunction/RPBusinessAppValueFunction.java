package com.citi.ebx.dsmt.valuefunction;

import com.citi.ebx.goc.path.RosettaPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class RPBusinessAppValueFunction  implements ValueFunction {

	@Override
	public Object getValue(Adaptation record) {
		String key = null;
		if(record.getContainerTable().getTablePath().compareTo(RosettaPaths._MANUAL_TAGGING_RP_BUS_APP_LE.getPathInSchema())==0){
			String csi = record.getString(RosettaPaths._MANUAL_TAGGING_RP_BUS_APP_LE._LINKED_CSI);
			String userEntity = record.getString(RosettaPaths._MANUAL_TAGGING_RP_BUS_APP_LE._USER_LEGAL_ENTITY);
			key = csi+userEntity;
		}else if(record.getContainerTable().getTablePath().compareTo(RosettaPaths._MANUAL_TAGGING_RP_BUS_APP_MS.getPathInSchema())==0){
			String csi = record.getString(RosettaPaths._MANUAL_TAGGING_RP_BUS_APP_MS._LINKED_CSI);
			String userMS = record.getString(RosettaPaths._MANUAL_TAGGING_RP_BUS_APP_MS._USER_MS);
			key = csi+userMS;
		}else if(record.getContainerTable().getTablePath().compareTo(RosettaPaths._MANUAL_TAGGING_RP_VAL_TRANS_NET_MS.getPathInSchema())==0){
			String csi = record.getString(RosettaPaths._MANUAL_TAGGING_RP_VAL_TRANS_NET_MS._FMI_ID);
			String userMS = record.getString(RosettaPaths._MANUAL_TAGGING_RP_VAL_TRANS_NET_MS._LNK_MS);
			key = csi+userMS;
		}else if(record.getContainerTable().getTablePath().compareTo(RosettaPaths._MANUAL_TAGGING_RP_VAL_TRANS_NET_LE.getPathInSchema())==0){
			String csi = record.getString(RosettaPaths._MANUAL_TAGGING_RP_VAL_TRANS_NET_LE._FMI_ID);
			String userMS = record.getString(RosettaPaths._MANUAL_TAGGING_RP_VAL_TRANS_NET_LE._LNK_LE);
			key = csi+userMS;
		}
		
		return key;
	}

	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub
		
	}

}
