/**
 * 
 */
package com.citi.ebx.dsmt.valuefunction;

import java.util.Arrays;
import java.util.List;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

/**
 * @author rk00242
 *
 */
public class BURestrictionValueFunction implements ValueFunction {
	

	/* (non-Javadoc)
	 * @see com.orchestranetworks.schema.ValueFunction#getValue(com.onwbp.adaptation.Adaptation)
	 */
	@Override
	public Object getValue(Adaptation adaptation) {
		
		List<String> attributeValues = Arrays.asList("LEADSPLIT", "SPLIT", "STANDALONE",
				"SHADOW");
		
		String attributeID = adaptation
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_ATTR_ID);
		String value = adaptation
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_ATT_VAL_ID);
		String status = adaptation
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._EFF_STATUS);

		if (status.equalsIgnoreCase(DSMTConstants.ACTIVE)
				&& ((attributeID.equalsIgnoreCase("CONDI_TYPE") && attributeValues
						.contains(value)) || (attributeID
						.equalsIgnoreCase("BU_TYPE") && !value
						.equalsIgnoreCase("AUTO-ELMN")))) {
			return "N";
		} else {
			return "Y";
		}
		
	}

	/* (non-Javadoc)
	 * @see com.orchestranetworks.schema.ValueFunction#setup(com.orchestranetworks.schema.ValueFunctionContext)
	 */
	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub

	}

}
