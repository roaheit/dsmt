package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class AccountCategoryDescFunction implements ValueFunction {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path accounCategoryPath = Path.parse("/root/GLOBAL_STD/ENT_STD/F_BAL_TYPE/C_DSMT_ACC_CTGR");
	private Path accountCategoryCode = Path.parse("./C_DSMT_ACC_CATGORY");
	private Path categoryDesc = Path.parse("./C_DSMT_LONG_DESC");

	@Override
	public Object getValue(Adaptation record) {

		String code = record.getString(accountCategoryCode);
		if (code == null) {
			return "Account category Code not found ";
		}
		
		String predicate = accountCategoryCode.format() + "= '" + code + "'";
		AdaptationTable table = record.getContainer().getTable(accounCategoryPath);
		RequestResult reqRes = table.createRequestResult(predicate);
		try {
			Adaptation rec = reqRes.nextAdaptation();
			try {
				String statDesc = rec.getString(Path.SELF.add(categoryDesc));
				return statDesc;
			} catch (Exception e) {
				LOG.error("No reference found for " + code + " in " + accounCategoryPath  + " Exception = " + e.getMessage() );
				return "Not Available";	
			}
		} finally {
			reqRes.close();
		}
	}

	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub

	}

}
