package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class ContainsChildrenValueFunction implements ValueFunction {
	
	
	private String parentFKField;
	
	
	
	public String getParentFKField() {
		return parentFKField;
	}

	public void setParentFKField(String parentFKField) {
		this.parentFKField = parentFKField;
	}
	
	@Override
	public Object getValue(Adaptation record) {
		String pk = record.getOccurrencePrimaryKey().format();
		AdaptationTable table = record.getContainerTable();
		RequestResult reqRes = table.createRequestResult(
				 Path.SELF.add(parentFKField).format() + "=\"" + pk + "\"");
		return ! reqRes.isEmpty();
	}

	@Override
	public void setup(ValueFunctionContext context) {
		
		if (parentFKField == null) {
			context.addError("Must specify parentFKField.");
		} else {
			SchemaNode parentFKNode = context.getSchemaNode().getNode(Path.PARENT.add(parentFKField));
			if (parentFKNode == null) {
				context.addError(parentFKField + " is not a node in the schema.");
			}
		}
	}
	

}
