package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class FamilyNodeValueFunction implements ValueFunction{

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path familyFKField = Path.parse("./FAMILY_FK");
	private Path parentField = Path.parse("./C_DSMT_FAMILY_CD");
	private SchemaNode parentNode;

	public String getFamilyFKField() {
		return familyFKField.format();
	}

	public void setFamilyFKField(String familyFKField) {
		this.familyFKField = Path.parse(familyFKField);
	}

	@Override
	public Object getValue(Adaptation record) {
		String familyPK = record.getString(Path.SELF.add(familyFKField));
		if (familyPK == null) {
			return null;
		}
		String domainRecord = familyPK.substring(11,15);
		return domainRecord;
	}

	@Override
	public void setup(ValueFunctionContext context) {
		parentNode = context.getSchemaNode();
		if (parentField == null) {
			context.addError("Must specify parentField.");
		} else {
			SchemaNode parentNodeValue = parentNode.getNode(parentField);
			LOG.info("parentNodeValue" + parentNodeValue);
		}
	}


}
