package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class ContainsActiveChildrenValueFunction implements ValueFunction {
	private static final String ACTIVE = "A";
	
	private String statusField = "./EFF_STATUS";
	private String parentFKField;
	private String endDateField = "./ENDDT";	
	public String getStatusField() {
		return statusField;
	}

	public void setStatusField(final String statusField) {
		this.statusField = statusField;
	}
	
	public String getParentFKField() {
		return parentFKField;
	}

	public void setParentFKField(String parentFKField) {
		this.parentFKField = parentFKField;
	}
	
	@Override
	public Object getValue(Adaptation record) {
		String pk = record.getOccurrencePrimaryKey().format();
		AdaptationTable table = record.getContainerTable();
		RequestResult reqRes = table.createRequestResult(
				Path.SELF.add(statusField).format() + "=\"" + ACTIVE + "\" and "+ endDatePredicate()+ " and "
				+ Path.SELF.add(parentFKField).format() + "=\"" + pk + "\"");
		return ! reqRes.isEmpty();
	}

	@Override
	public void setup(ValueFunctionContext context) {
		if (statusField == null) {
			context.addError("Must specify statusField.");
		} else {
			SchemaNode statusFieldNode = context.getSchemaNode().getNode(Path.PARENT.add(statusField));
			if (statusFieldNode == null) {
				context.addError(statusField + " is not a node in the schema.");
			}
		}
		if (parentFKField == null) {
			context.addError("Must specify parentFKField.");
		} else {
			SchemaNode parentFKNode = context.getSchemaNode().getNode(Path.PARENT.add(parentFKField));
			if (parentFKNode == null) {
				context.addError(parentFKField + " is not a node in the schema.");
			}
		}
	}
	
	
private String endDatePredicate(){
		
		return " ( osd:is-null(" + endDateField + ") or date-equal(" + endDateField + ", '9999-12-31') )";
	}
}
