package com.citi.ebx.dsmt.valuefunction;

import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
/**
 * Looks up values from a tables linked to one another via foreign keys 
 */


public class ReferColumnKeyGOCPartnerSystem implements ValueFunction {
	private Path sourceColumnPath_1;
	private Path fkColumnPath;
	private Path sourceColumnPath_2;
	private Path tablePath_1;
	private Path tablePath_2;
//	ProcedureContext pContext;

	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	
	public String getSourceColumnPath_2() {
		return sourceColumnPath_2.format();
	}

	public void setSourceColumnPath_2(Path sourceColumnPath_2) {
		this.sourceColumnPath_2 = sourceColumnPath_2;
	}

	/**
	 * Get the relative path of the source column that you're pulling
	 * from the other table (i.e. "./GL_CLA").
	 * 
	 * @return the path of the source column
	 */
	public String getSourceColumnPath_1() {
		return sourceColumnPath_1.format();
	}

	/**
	 * Set the relative path of the source column that you're pulling
	 * from the other table (i.e. "./GL_CLA").
	 * 
	 * @param sourceColumnPath the path of the source column
	 */
	public void setSourceColumnPath_1(String sourceColumnPath_1) {
		this.sourceColumnPath_1 = Path.parse(sourceColumnPath_1);
	}

	/**
	 * Get the relative path of the foreign key column (i.e. "./LC_GL_ACCOUNT").
	 * 
	 * @return the path of the foreign key column
	 */
	public String getFkColumnPath() {
		return fkColumnPath.format();
	}

	/**
	 * Set the relative path of the foreign key column (i.e. "./LC_GL_ACCOUNT").
	 * 
	 * @param fkColumnPath the path of the foreign key column
	 */
	public void setFkColumnPath(String fkColumnPath) {
		this.fkColumnPath = Path.parse(fkColumnPath);
	}

	@Override
	public Object getValue(Adaptation record) {
		// Get the foreign key node
		final SchemaNode fkNode = record.getSchemaNode().getNode(fkColumnPath);
		// Follow the foreign key to get the record that it's pointing to
		final Adaptation linkedRecord = fkNode.getFacetOnTableReference().getLinkedRecord(record);
		//System.out.println("*****************sourceColumnPath_2"+sourceColumnPath_2.format());
		if (linkedRecord != null) {
			// Get the value from the linked record 
			String sid = getSID(linkedRecord,tablePath_1,sourceColumnPath_1);
//			
		//	LOG.error ("SID Value  ->"+sid); 
			String GL  = getGLSystem(record.getHome().getRepository(),sid,sourceColumnPath_2,tablePath_2);
			return GL;
		}
		return null;
	}

	public void setSourceColumnPath_1(Path sourceColumnPath_1) {
		this.sourceColumnPath_1 = sourceColumnPath_1;
	}

	private static String getSID(final Adaptation gocRecord,Path tablePath_1,Path sourceColumnPath_1) {

		final Adaptation sidRecord = Utils.getLinkedRecord(gocRecord,
				sourceColumnPath_1);
		if (sidRecord == null) {
			LOG.error("No SID found for record " + gocRecord.getOccurrencePrimaryKey().format());
			return null;
		}

		return sidRecord.getString(
				sourceColumnPath_1);
	}
	
	private static String getGLSystemID(final Adaptation metadataDataSet, 
			final String sid,Path sourceColumnPath_2,Path tablePath_2) {
			

		final AdaptationTable ccTable =	metadataDataSet.getTable(
				tablePath_2);
		final String predicate = GOCWFPaths._C_DSMT_SID_ENH._SID.format()
					+ "=\"" + sid + "\" and " + GOCWFPaths._C_DSMT_SID_ENH._EFF_STATUS.format() + "=\"A\"";
		String response=null;
		LOG.debug(predicate);

		final RequestResult reqRes = ccTable.createRequestResult(predicate);
		final Adaptation ccRecord;
		try {
			ccRecord = reqRes.nextAdaptation();
		} finally {
			reqRes.close();
		}
		if (ccRecord == null) {
			LOG.debug("No record found for predicate " + predicate
					+ " in table " + ccTable.getTablePath().format());
			return null;
		}
		
			response = ccRecord.getString(sourceColumnPath_2);
		
		return response;
	}
	

	public static String getGLSystem(Repository repo,String sid,Path sourceColumnPath_2,Path tablePath_2)
	{
		
		final Adaptation metadataDataSet ;
		String GLSystem=null;
		try {
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(repo);
			GLSystem= getGLSystemID (metadataDataSet,sid,sourceColumnPath_2,tablePath_2);
			
		} catch (OperationException ex) {
			
		}
		
		return GLSystem;
		
	} 
	
	
	
	
	

	/**
	 * @param tablePath_1 the tablePath_1 to set
	 */
	public void setTablePath_1(Path tablePath_1) {
		this.tablePath_1 = tablePath_1;
	}

	/**
	 * @return the tablePath_1
	 */
	public Path getTablePath_1() {
		return tablePath_1;
	}

	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @param tablePath_2 the tablePath_2 to set
	 */
	public void setTablePath_2(Path tablePath_2) {
		this.tablePath_2 = tablePath_2;
	}

	/**
	 * @return the tablePath_2
	 */
	public Path getTablePath_2() {
		return tablePath_2;
	}
}
