package com.citi.ebx.dsmt.valuefunction;

import java.text.SimpleDateFormat;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class AccountHierarchyLeafFlagValueFunction implements ValueFunction {
	
	
	private String parentFKField;			// "./Parent_PMF_Account"; This should be the current table column name where current records PK is stored.

	private String childTableXPath =  null; // "/root/GLOBAL_STD/PMF_STD/F_ACC_STD/FDL_Account_Table";  This should be the XPath of the child table
	private String childTableFKField = null ; // "./Parent_PMF_Account";	This should be the child table column name where current records PK is stored. 
	
	private String effectiveStatusField = "./EFF_STATUS";
	private String endDateField = "./ENDDT";
		
	@Override
	public Object getValue(Adaptation record) {
		
		final AdaptationTable table = record.getContainerTable();
		
		final String recordPK = record.getOccurrencePrimaryKey().format();
		String predicate = Path.SELF.add(parentFKField).format() + "='" + recordPK + "' and "  + endDatePredicate();
		final RequestResult reqRes = table.createRequestResult(predicate);
	
	
		Adaptation child = null;
		try {
			child = reqRes.nextAdaptation();
		//	LOG.debug(String.valueOf(child) + " predicate = " + predicate);
		} finally {
			reqRes.close();
		}
		
		
		/** Logic to look up whether current record is a parent of an active non-enddated record in the foreign Table */
		if(null != childTableXPath && null != childTableFKField){
			
			final AdaptationTable childTable = table.getContainerAdaptation().getTable(Path.parse(childTableXPath));
			predicate = childTableFKField +  "='" + recordPK + "' and " + endDatePredicate();
			final RequestResult childTableResult = childTable.createRequestResult(predicate);
			
			Adaptation foreignChild = null;
			try{
				foreignChild = childTableResult.nextAdaptation();
//				LOG.debug(String.valueOf(foreignChild) + " predicate = " + predicate);
			}finally{
				childTableResult.close();
			}
		
			if(null != foreignChild && null == child){
				LOG.debug (recordPK + " is a parent of an active record in the table " + childTableXPath + " but a leaf in the current table. " );
			}
			
			return (child == null && foreignChild == null) ? LEAF_VALUE : PARENT_VALUE;
		}
		
		
		return child == null ? LEAF_VALUE : PARENT_VALUE;
	}

	protected String statusPredicate(){
		
		return effectiveStatusField + "='A'";
	}
	
	private String endDatePredicate(){
		
		return " ( osd:is-null(" + endDateField + ") or date-equal(" + endDateField + ", '9999-12-31') )";
	}

	@Override
	public void setup(ValueFunctionContext context) {
		
		node = context.getSchemaNode();
		if (parentFKField == null) {
			context.addError("Must specify parentFKField.");
		} else {
			SchemaNode parentFKNode = node.getNode(Path.PARENT.add(parentFKField));
			if (parentFKNode == null) {
				context.addError(parentFKField+ " is not a node in the schema.");
			}
		}
	
	}
	

	
	public String getChildTableXPath() {
		return childTableXPath;
	}

	public void setChildTableXPath(String childTableXPath) {
		this.childTableXPath = childTableXPath;
	}

	public String getChildTableFKField() {
		return childTableFKField;
	}

	public void setChildTableFKField(String childTableFKField) {
		this.childTableFKField = childTableFKField;
	}
	

	private SchemaNode node;
	
	public String getParentFKField() {
		return parentFKField;
	}

	public void setParentFKField(String parentFKField) {
		this.parentFKField = parentFKField;
	}
	
	final SimpleDateFormat dateFormat = new SimpleDateFormat(DSMTConstants.DEFAULT_DATE_FORMAT);

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private static final String LEAF_VALUE = "L";
	private static final String PARENT_VALUE = "P";

}
