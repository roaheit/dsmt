package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class SubStringValueFunctionbyLength implements ValueFunction {

	private String inputFieldID;
	private Integer substringStartPosition=0;
	private String outputFieldID; // "./DESCR"
	private Integer substringEndPosition = 250;

	
	
	
	@Override
	public Object getValue(Adaptation adaptation) {

		String substring = null;
		
		try {

			Path inputFieldPath = Path.parse(inputFieldID);
			String inputField1 = adaptation.getString(inputFieldPath);
			Path descrFieldPath = Path.parse(outputFieldID);
			
			if (descrFieldPath != null ) {

				if (null != inputFieldID) {
					inputField1 = adaptation.getString(Path
							.parse(inputFieldID));
					LOG.info( " inputField1 value "+ inputField1  +"descrFieldPath" + descrFieldPath);
				}
				int length= inputField1.length();
				int sunbStringlength= 0;
				
				if (null != inputField1
						&& ( length > substringStartPosition  )) {
					
					if( length < substringEndPosition ){
						sunbStringlength=length;
					}else {
						sunbStringlength= substringEndPosition;
					}
					
					
								
					LOG.info( " substringStartPosition value "+ substringStartPosition  +" substringEndPosition >>  " + substringEndPosition + " length  :: "+length + " sunbStringlength :: "+ sunbStringlength);
					substring = inputField1.substring(substringStartPosition, sunbStringlength);
				} 
				// System.out.println(naicsSubstr);
				
			} 
			
		} catch (Exception e) {
			
			e.printStackTrace();
			LOG.info("Exception found while updating field " + outputFieldID
					+ ", Exception message = " + e.getMessage());
			
		}
		return substring;
	}

	@Override
	public void setup(ValueFunctionContext arg0) {
		// Not needed

	}

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();




	public String getInputFieldID() {
		return inputFieldID;
	}

	public void setInputFieldID(String inputFieldID) {
		this.inputFieldID = inputFieldID;
	}

	public Integer getSubstringStartPosition() {
		return substringStartPosition;
	}

	public void setSubstringStartPosition(Integer substringStartPosition) {
		this.substringStartPosition = substringStartPosition;
	}

	public String getOutputFieldID() {
		return outputFieldID;
	}

	public void setOutputFieldID(String outputFieldID) {
		this.outputFieldID = outputFieldID;
	}

	public Integer getSubstringEndPosition() {
		return substringEndPosition;
	}

	public void setSubstringEndPosition(Integer substringEndPosition) {
		this.substringEndPosition = substringEndPosition;
	}

	

}
