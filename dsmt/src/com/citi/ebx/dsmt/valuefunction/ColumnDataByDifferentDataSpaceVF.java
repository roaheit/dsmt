package com.citi.ebx.dsmt.valuefunction;

import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class ColumnDataByDifferentDataSpaceVF implements ValueFunction {
	private Path fkColumnPath;// Foreign Key
	private Path foreginTablePath; // Other dataspace table path
	private String dataSpace;// dataspace name
	private Path DataFetcherColumn;// column to fetch data to this record
	private Path fkRefferedColumnPath; // column which is referred as foreign Key
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Object getValue(Adaptation record) {
		// Get the foreign key node
			String targetRecord  = getTargetRecord(record.getHome().getRepository(),record.getString(fkColumnPath),fkRefferedColumnPath,foreginTablePath);
			return targetRecord;
		}
		
	public  String getTargetRecord(Repository repo,String sourceValue,Path foreignColumnPath,Path targetTablePath)
	{
		
		final Adaptation metadataDataSet ;
		String targetColumnValue=null;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(repo, dataSpace, dataSpace);
			final AdaptationTable ccTable =	metadataDataSet.getTable(
					targetTablePath);
			final String predicate = foreignColumnPath.format()
					+ "=\"" + sourceValue + "\" " ;
			final RequestResult reqRes = ccTable.createRequestResult(predicate);
			
			final Adaptation ccRecord;
			try {
				ccRecord = reqRes.nextAdaptation();
			} finally {
				reqRes.close();
			}
			if (ccRecord == null) {
				LOG.debug("No record found for predicate " + predicate
						+ " in table " + ccTable.getTablePath().format());
				return null;
			}
			
			targetColumnValue = ccRecord.getString(DataFetcherColumn);
		} catch (OperationException ex) {
			
		}
		
		return targetColumnValue;
		
	}
	public Path getFkColumnPath() {
		return fkColumnPath;
	}
	public void setFkColumnPath(Path fkColumnPath) {
		this.fkColumnPath = fkColumnPath;
	}
	public Path getForeginTablePath() {
		return foreginTablePath;
	}
	public void setForeginTablePath(Path foreginTablePath) {
		this.foreginTablePath = foreginTablePath;
	}
	public String getDataSpace() {
		return dataSpace;
	}
	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}
	public Path getDataFetcherColumn() {
		return DataFetcherColumn;
	}
	public void setDataFetcherColumn(Path dataFetcherColumn) {
		DataFetcherColumn = dataFetcherColumn;
	}
	public Path getFkRefferedColumnPath() {
		return fkRefferedColumnPath;
	}
	public void setFkRefferedColumnPath(Path fkRefferedColumnPath) {
		this.fkRefferedColumnPath = fkRefferedColumnPath;
	}
}
