package com.citi.ebx.dsmt.valuefunction;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.LoggingCategory;

public class CountryNameFunction implements ValueFunction {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path dsmtCountryPath = Path.parse("/root/GLOBAL_STD/ENT_STD/C_DSMT_NEW_CNTRY");
	private Path countryCodePath = Path.parse("./COUNTRY_2CHAR");
	private Path countryDesc = Path.parse("./DESCR");

	@Override
	public Object getValue(Adaptation record) {
		
		String code = record.getString(countryCodePath);
		if (code == null) {
			return "Country Code is not found " ;
		}
		
		String predicate = countryCodePath.format() + "= '" + code + "'";
		AdaptationTable table = record.getContainer().getTable(dsmtCountryPath);
		RequestResult reqRes = table.createRequestResult(predicate);
		try {
			Adaptation rec = reqRes.nextAdaptation();
			String statDesc;
			try {
				statDesc = rec.getString(Path.SELF.add(countryDesc));
				return statDesc;
			} catch (Exception e) {
				LOG.error("No reference found for column " + countryCodePath + " in " + dsmtCountryPath  + " Exception = " + e.getMessage() );
				return "Not Available";	
			}
		
		
		} finally {
			reqRes.close();
		}
	}

	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub

	}

}