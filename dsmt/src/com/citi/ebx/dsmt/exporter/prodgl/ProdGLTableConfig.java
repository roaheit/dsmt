package com.citi.ebx.dsmt.exporter.prodgl;

import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;

/**
 * A table config for use with the ProdType table
 */
public class ProdGLTableConfig extends DataSetExportTableConfig {
	public enum ProdGLExportType {
		// Currently only one type but there will be more
		FLAT 
	}
	
	
	private static final String DEFAULT_TABLE_EXPORT_NAME = "PRODUCT";
	
	protected ProdGLExportType prodGLExportType;
	protected String tableExportName = DEFAULT_TABLE_EXPORT_NAME;

	/**
	 * Get the pmfProd type export type
	 * 
	 * @return the pmfProd type export type
	 */
	public ProdGLExportType getprodGLExportType() {
		return prodGLExportType;
	}

	/**
	 * Set the pmfProd type export type
	 * 
	 * @param ProdGLExportType the PmfProd type export type
	 */
	public void setPmfProdExportType(ProdGLExportType pmfProdExportType) {
		this.prodGLExportType = pmfProdExportType;
	}
	


	/**
	 * Get the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @return the name of the table
	 */
	public String getTableExportName() {
		return tableExportName;
	}

	/**
	 * Set the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @param tableExportName the name of the table
	 */
	public void setTableExportName(String tableExportName) {
		this.tableExportName = tableExportName;
	}
}
