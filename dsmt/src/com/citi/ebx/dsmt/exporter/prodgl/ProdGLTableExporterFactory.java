package com.citi.ebx.dsmt.exporter.prodgl;

import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporter;

/**
 * A factory for table exporters that handles pmfProdSeg type table exports
 */
public class ProdGLTableExporterFactory extends DefaultTableExporterFactory {
	/**
	 * Overridden in order to handle pmfProd type exports
	 */
	@Override
	public TableExporter getTableExporter(DataSetExportTableConfig tableConfig, AdaptationTable table) {
		if (tableConfig instanceof ProdGLTableConfig) {
			ProdGLTableConfig pmfProdTableConfig = (ProdGLTableConfig) tableConfig;
			switch (pmfProdTableConfig.getprodGLExportType()) {
			// Currently there is just the one type but this is where we'd handle other export types
			// for pmfProd type table
			case FLAT:
				LOG.info("PmfProdFlatTableExporter : " );
				return new ProdGLTableExporter();
			
			}
		}
		return super.getTableExporter(tableConfig, table);
	}
}
