package com.citi.ebx.dsmt.exporter;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTExportBean;
import com.citi.ebx.dsmt.util.LatamHeaderTrailerGenerator;
import com.citi.ebx.util.SaveUtil;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

/**
 * This extends DataSetExporter in order to implement some DSMT-specific exports
 */
public class LatamExporter extends DataSetExporter {
	
	private static final String DEFAULT_README_FILENAME = "LATAM_readme.txt";
	
	private String readmeFilename = DEFAULT_README_FILENAME;
	boolean mergeReadme = false;
	boolean runHTGenerator;
	boolean runSQLReports = false;

	
	
	/**
	 * @return the readmeFilename
	 */
	public String getReadmeFilename() {
		return readmeFilename;
	}

	/**
	 * @param readmeFilename the readmeFilename to set
	 */
	public void setReadmeFilename(String readmeFilename) {
		this.readmeFilename = readmeFilename;
	}

	public LatamExporter() {
		super();
	}
	
	public LatamExporter(final TableExporterFactory tableExporterFactory,final DataSetExportConfig config) {
		
		super(tableExporterFactory, config);
	}
	
	/** Constructor for Export Servlet */
	public LatamExporter(final TableExporterFactory tableExporterFactory,final DataSetExportConfig config, boolean mergeReadme, boolean runSQLReports, boolean runHT) {
		
		super(tableExporterFactory, config);
		this.mergeReadme = mergeReadme;
		this.runHTGenerator = runHT;
		this.runSQLReports = runSQLReports;
	}
	
	
	/**
	 * Overridden in order to produce some more complicated DSMT-specific exports, and to produce a readme file
	 */
	@Override
	public DSMTExportBean exportDataSet(Session session, Adaptation dataSet)
			throws IOException, OperationException {
		
		LOG.info("LatamExporter: exportDataSet started");
		DSMTExportBean dsmtExportBean = super.exportDataSet(session, dataSet);
		LOG.info("LatamExporter: exportDataSet finished");
		// Execute HT Generator
		new LatamHeaderTrailerGenerator().runHeaderTrailerGenerator(dsmtExportBean);
		final String userID = session.getUserReference().getUserId();
		
		for(String fileName:dsmtExportBean.getFileNamesAndCount().keySet()){
			Map<Path, Object> inputMap= new HashMap<Path, Object>();
			inputMap.put(DSMTConstants.REPORT_NAME, fileName);
			inputMap.put(DSMTConstants.REPORT_LOCATION, dsmtExportBean.getOutputFilePath());
			inputMap.put(DSMTConstants.REPORT_ROW_COUNT, dsmtExportBean.getFileNamesAndCount().get(fileName));
			inputMap.put(DSMTConstants.REPORT_STATUS, "Success");
			inputMap.put(DSMTConstants.REPORT_LASTUPDOPRID, userID);
			inputMap.put(DSMTConstants.REPORT_TIMESTAMP,Calendar.getInstance().getTime());		
			
			SaveUtil saveUtil = new SaveUtil(dataSet.getHome().getRepository(),DSMTConstants.DATASPACE_LA,DSMTConstants.DATASET_LA,DSMTConstants.LATAM_REPORT_AUDIT_PATH,inputMap,false );
			Utils.executeProcedure(saveUtil, session, dataSet.getHome().getRepository().lookupHome(
					HomeKey.forBranchName(DSMTConstants.DATASPACE_LA)));
			
			Map<Path, Object> updateMap= new HashMap<Path, Object>();
			updateMap.put(DSMTConstants.CATALOG_TABLE_FILENAME, fileName);
			updateMap.put(DSMTConstants.CATALOG_TABLE_COUNT, dsmtExportBean.getFileNamesAndCount().get(fileName));
			
			
			
			
			SaveUtil asaveUtil = new SaveUtil(dataSet.getHome().getRepository(),DSMTConstants.DATASPACE_LA,DSMTConstants.DATASET_LA,DSMTConstants.LATAM_CATALOG_TABLE_PATH,updateMap,true );
			Utils.executeProcedure(asaveUtil, session, dataSet.getHome().getRepository().lookupHome(
					HomeKey.forBranchName(DSMTConstants.DATASPACE_LA)));
		}
		LOG.info("LatamExporter: LatamHeaderTrailerGenerator finished");
		
		return dsmtExportBean;
	}
	
}
