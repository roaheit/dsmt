package com.citi.ebx.dsmt.exporter;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.citi.ebx.dsmt.exporter.readme.ReadmeConfig;
import com.citi.ebx.dsmt.exporter.readme.ReadmeConfigFactory;
import com.citi.ebx.dsmt.exporter.readme.ReadmeExporter;
import com.citi.ebx.dsmt.jdbc.connector.SQLConnectReportUtil;
import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.path.DSMTPaths;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTExportBean;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.dsmt.util.HeaderTailerGenerator;
import com.citi.ebx.dsmt.util.MergeReadme;
import com.citi.ebx.dsmt.util.RosettaHeaderTrailerGenerator;
import com.citi.ebx.util.SaveUtil;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

/**
 * This extends DataSetExporter in order to implement some DSMT-specific exports
 */
public class DSMTExporter extends DataSetExporter {
	private static final String DEFAULT_README_FILENAME = "DSMT2_readme.txt";
		
	private String readmeFilename = DEFAULT_README_FILENAME;
	boolean mergeReadme = false;
	boolean runHTGenerator;
	boolean runSQLReports = false;
	
	
	
	public DSMTExporter() {
		super();
	}
	
	/** Constructor for Export Servlet */
	public DSMTExporter(final TableExporterFactory tableExporterFactory,final DataSetExportConfig config, boolean mergeReadme, boolean runSQLReports, boolean runHT) {
		
		super(tableExporterFactory, config);
		this.mergeReadme = mergeReadme;
		this.runHTGenerator = runHT;
		this.runSQLReports = runSQLReports;
	}
	
	/** Constructor for Scheduled Task */
	public DSMTExporter(final TableExporterFactory tableExporterFactory,final DataSetExportConfig config, boolean mergeReadme, boolean runSQLReports) {
		
		super(tableExporterFactory, config);
		this.mergeReadme = mergeReadme;
		this.runHTGenerator = true;
		this.runSQLReports = runSQLReports;
	}

	/**
	 * Get the readme filename
	 * 
	 * @return the readme filename
	 */
	public String getReadmeFilename() {
		return readmeFilename;
	}

	/**
	 * Set the readme filename
	 * 
	 * @param readmeFilename the readme filename
	 */
	public void setReadmeFilename(String readmeFilename) {
		this.readmeFilename = readmeFilename;
	}

	/**
	 * Overridden in order to produce some more complicated DSMT-specific exports, and to produce a readme file
	 */
	@Override
	public DSMTExportBean exportDataSet(Session session, Adaptation dataSet)
			throws IOException, OperationException {
		
		
		LOG.info("DSMTExporter: exportDataSet");
		DSMTExportBean dsmtExportBean = super.exportDataSet(session, dataSet);
		
		LOG.info("dsmtExportBean = " +  dsmtExportBean);
	
		if(runSQLReports){
			
			Map<String, Integer> sqlReportMap;
			try {
				sqlReportMap = generateSQLReport(dsmtExportBean);
				List<String> sqlreportList = new ArrayList<String>(sqlReportMap.keySet());
				dsmtExportBean.getFileNames().addAll(sqlreportList);
				dsmtExportBean.getFileNamesAndCount().putAll(sqlReportMap);
				LOG.info("DSMTExporter :generateSQLReport file list after SQl call" +dsmtExportBean.getFileNames());
				session.setAttribute("sqlReportMap", sqlReportMap);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		exportReadme(session, dataSet);
		String configID = dsmtExportBean.getConfigID();
		// Execute HT Generator
		if(runHTGenerator){
			/*Rosetta HT FIle changes
			condition to check whether EC id is Rosetta*/
			if(configID.contains("Rosetta") || configID.contains("R3ManualTagging") || configID.contains("RosettaUP")){
				
				new RosettaHeaderTrailerGenerator().runHeaderTrailerGenerator(dsmtExportBean,dataSet.getHome().getRepository());
			}
			else{
			new HeaderTailerGenerator().runHeaderTrailerGenerator(dsmtExportBean);
			}
		}
		
		//Save the exportBean to new dataset
		final String userID = session.getUserReference().getUserId();
		
		
		
		
		for(String fileName:dsmtExportBean.getFileNamesAndCount().keySet()){
			Map<Path, Object> inputMap= new HashMap<Path, Object>();
			inputMap.put(DSMTConstants.REPORT_NAME, fileName);
			inputMap.put(DSMTConstants.REPORT_LOCATION, dsmtExportBean.getOutputFilePath());
			inputMap.put(DSMTConstants.REPORT_ROW_COUNT, dsmtExportBean.getFileNamesAndCount().get(fileName));
			inputMap.put(DSMTConstants.REPORT_STATUS, "Success");
			inputMap.put(DSMTConstants.REPORT_LASTUPDOPRID, userID);
			inputMap.put(DSMTConstants.REPORT_TIMESTAMP,Calendar.getInstance().getTime());		
			
			SaveUtil saveUtil = new SaveUtil(dataSet.getHome().getRepository(),DSMTConstants.DATASPACE_DSMT2,DSMTConstants.DATASET_NEW_DSMT2,DSMTConstants.REPORT_AUDIT_PATH,inputMap,false );
			Utils.executeProcedure(saveUtil, session, dataSet.getHome().getRepository().lookupHome(
					HomeKey.forBranchName(DSMTConstants.DATASPACE_DSMT2)));
			
			Map<Path, Object> updateMap= new HashMap<Path, Object>();
			updateMap.put(DSMTConstants.CATALOG_TABLE_FILENAME, fileName);
			updateMap.put(DSMTConstants.CATALOG_TABLE_COUNT, dsmtExportBean.getFileNamesAndCount().get(fileName));
			
			
			
			
			SaveUtil asaveUtil = new SaveUtil(dataSet.getHome().getRepository(),DSMTConstants.DATASPACE_DSMT2,DSMTConstants.DATASET_NEW_DSMT2,DSMTConstants.CATALOG_TABLE_PATH,updateMap,true );
			Utils.executeProcedure(asaveUtil, session, dataSet.getHome().getRepository().lookupHome(
					HomeKey.forBranchName(DSMTConstants.DATASPACE_DSMT2)));
		}
		
		LOG.info("user running the export " + userID );
		if(DSMTConstants.ADMIN_USER.equalsIgnoreCase(userID)){
			// DO nothing
		}else{
			// notify user who executed the export
			LOG.info("user " + userID + " is being notified that the export has completed. Export config - " +  dsmtExportBean.getConfigID());
			notifyUserRunningTheExport(userID, dsmtExportBean);
		}
		
		return dsmtExportBean;
	}
	
	private void notifyUserRunningTheExport(final String userID, final DSMTExportBean dsmtExportBean) {
		
		final String message = "Export of config id " +  dsmtExportBean.getConfigID() + " is complete. The generated file(s) are present at ";
		final String subject = "DSMT2 Notification";
		final String fileLocation = dsmtExportBean.getOutputFilePath();
		
		DSMTNotificationService.notifyEmailID(userID, subject, message  + fileLocation, false);
	}

	// Export the readme file
	private void exportReadme(final Session session, final Adaptation dataSet)
			throws IOException {
		AdaptationTable readmeTable = null;
		
		try {
			readmeTable = dataSet.getTable(DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM.getPathInSchema());
		} catch (PathAccessException e) {
			LOG.info("Readme generation will not occur for the current data set " + dataSet.getLabel(session.getLocale()));
		}
		
		LOG.info("mergeReadme value in DSMTExporter " + mergeReadme);
		
		if (null != readmeTable ){
			
			final File folder = config.getFolder();
			final File readmeFile = new File(folder, readmeFilename);
			
			readmeFile.setReadable(true, false);
			readmeFile.setWritable(true, true);
			
			
			// Create the config from the readme table records
			final ReadmeConfig readmeConfig = ReadmeConfigFactory.createFromEBXTable(
					readmeTable, readmeFile, session.getLocale());
			final ReadmeExporter readmeExporter = new ReadmeExporter(readmeConfig);
			// Export the readme file
			try {
				readmeExporter.exportReadme(dataSet, session);
			} catch (Exception e) {
				LOG.error("Exception while creating export for readme file");
				e.printStackTrace();
			}
			
			new MergeReadme().runMergeReadMe();
		}
			
		//Execute merge readme
	
	}
	
	public Map<String, Integer> generateSQLReport (DSMTExportBean dsmtExportBean) throws SQLException{
		
		LOG.info("Report generation started !! "); 
		Map<String, Integer> sqlReportMap = null;
		Connection connection =null;
		
		try{
			
			SQLConnectReportUtil connectReportUtil = new SQLConnectReportUtil();
			
			connection = SQLReportConnectionUtils.getConnection();
			String configID = dsmtExportBean.getConfigID();
			if(configID!=null)
			{
				if(configID.startsWith(DSMTConstants.BD3REPORTEC)){
					connectReportUtil.setReportType(DSMTConstants.BD3REPORT);
				}
				else if(configID.startsWith(DSMTConstants.BD2REPORTEC)){
					connectReportUtil.setReportType(DSMTConstants.BD2REPORT);
				}
				else if(configID.startsWith(DSMTConstants.ACCTREPORTEC)){
					connectReportUtil.setReportType(DSMTConstants.ACCTREPORTEC);
				}
				else if(configID.startsWith(DSMTConstants.OTHREPORTEC)){
					connectReportUtil.setReportType(DSMTConstants.OTHREPORTEC);
				}
			}
			sqlReportMap = connectReportUtil.generateReport(connection, dsmtExportBean.getOutputFilePath());
			
			connection.close();
			
			LOG.info("Report generated successfully !!!");
				
		}
		catch (SQLException e){
			
			LOG.error("Encountered SQLException ->  message = " + e.getMessage());
		}
		catch (Exception e){
			
			LOG.error("Report generation terminated with Exception -> " + e + ", message = " + e.getMessage());
		}
		finally{
			
			if(null != connection){
				connection.close();
				connection = null;
			}
		}
		
		return sqlReportMap;
	}
}
