package com.citi.ebx.dsmt.exporter.custseg;

import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporter;

/**
 * A factory for table exporters that handles custSeg type table exports
 */
public class CustSegTableExporterFactory extends DefaultTableExporterFactory {
	/**
	 * Overridden in order to handle custSeg type exports
	 */
	@Override
	public TableExporter getTableExporter(DataSetExportTableConfig tableConfig, AdaptationTable table) {
		if (tableConfig instanceof CustSegTableConfig) {
			CustSegTableConfig custSegTableConfig = (CustSegTableConfig) tableConfig;
			switch (custSegTableConfig.getCustSegExportType()) {
			// Currently there is just the one type but this is where we'd handle other export types
			// for custSeg type table
			case FLAT:
				LOG.info("CustSegFlatTableExporter : " );
				return new CustSegFlatTableExporter();
			}
		}
		return super.getTableExporter(tableConfig, table);
	}
}
