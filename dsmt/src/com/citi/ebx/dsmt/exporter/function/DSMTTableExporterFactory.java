package com.citi.ebx.dsmt.exporter.function;

import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporter;
/**
 * A factory for table exporters that handles DSMT-specific implementations
 */
public class DSMTTableExporterFactory extends DefaultTableExporterFactory {
	/**
	 * Overridden in order to handle function flat exports
	 */
	@Override
	public TableExporter getTableExporter(DataSetExportTableConfig tableConfig, AdaptationTable table) {
		if (tableConfig instanceof FunctionTableConfig) {
			FunctionTableConfig functionTableConfig = (FunctionTableConfig) tableConfig;
			switch (functionTableConfig.getFunctionExportType()) {
			// Currently there is just the one type but this is where we'd handle other export types
			// for function table
			case FLAT:
				return new FunctionFlatTableExporter();
			}
		}
		return super.getTableExporter(tableConfig, table);
	}
}
