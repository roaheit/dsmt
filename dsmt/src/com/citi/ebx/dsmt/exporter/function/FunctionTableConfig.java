package com.citi.ebx.dsmt.exporter.function;

import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;

/**
 * A table config for use with the function table
 */
public class FunctionTableConfig extends DataSetExportTableConfig {
	public enum FunctionExportType {
		// Currently only one type but there will be more
		FLAT
	}
	
	private static final int DEFAULT_NUM_OF_LEVELS = 15;
	private static final String DEFAULT_TABLE_EXPORT_NAME = "PMF_FUNCTION";
	
	protected FunctionExportType functionExportType;
	protected int numOfLevels = DEFAULT_NUM_OF_LEVELS;
	protected String tableExportName = DEFAULT_TABLE_EXPORT_NAME;

	/**
	 * Get the function export type
	 * 
	 * @return the function export type
	 */
	public FunctionExportType getFunctionExportType() {
		return functionExportType;
	}

	/**
	 * Set the function export type
	 * 
	 * @param FunctionExportType the Function export type
	 */
	public void setFunctionExportType(FunctionExportType functionExportType) {
		this.functionExportType = functionExportType;
	}
	
	/**
	 * Get the number of levels to output
	 * 
	 * @return the number of levels to output
	 */
	public int getNumOfLevels() {
		return numOfLevels;
	}

	/**
	 * Set the number of levels to output
	 * 
	 * @param numOfLevels the number of levels to output
	 */
	public void setNumOfLevels(int numOfLevels) {
		this.numOfLevels = numOfLevels;
	}

	/**
	 * Get the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @return the name of the table
	 */
	public String getTableExportName() {
		return tableExportName;
	}

	/**
	 * Set the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @param tableExportName the name of the table
	 */
	public void setTableExportName(String tableExportName) {
		this.tableExportName = tableExportName;
	}
}
