package com.citi.ebx.dsmt.exporter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import com.citi.ebx.dsmt.util.DSMTExportBean;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.Request;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.TableExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

/**
 * An exporter for the product table that outputs all leaves of the table along with a flattened
 * representation of their complete path to the root. There is a specific format this conforms
 * to. Refer to a sample output file.
 */
public class ProdTypeFlatTableExporter implements TableExporter {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private static final String HEADER_DATE_PATTERN = "MM/dd/yyyy HH:mm:ss";
	
    private static final Path PARENT_FK_PATH = Path.parse("./Product_FK");
    private static final Path PRODUCT_CODE_PATH = Path.parse("./C_DSMT_PRODUCT_TYP");
    
    private static final Path PRODUCT_PARENT_DESC = Path.parse("./C_DSMT_LONG_DESC");
    private static final Path PRODUCT_CHILD_DESC = Path.parse("./C_DSMT_LONG_DESC");
    private static final Path EFFECTIVE_DATE_PATH = Path.parse("./PRT_EFFDT");
	
	/**
	 * This stores all the records from the table for faster lookup.
	 * Key = the primary key of the record, Value = the record itself.
	 */
	protected Map<PrimaryKey, Adaptation> allRecordsMap;
	/**
	 * This is a list of only the leaf records of the table, each one containing
	 * its primary key and level within the hierarchy (1 being the root).
	 */
	protected List<CachedLeafInfo> leaves;

	@Override
	public void exportTable(Session session, AdaptationTable table,DSMTExportBean exportBean,
			OutputStream out, DataSetExportTableConfig tableConfig,
			TableExporterFactory tableExporterFactory)
			throws IOException, OperationException {
		LOG.info("ProdTypeFlatTableExporter: exportTable");
		LOG.info("ProdTypeFlatTableExporter: table = " + table.getTablePath().format());
		ProdTypeTableConfig prodTypeTableConfig = (ProdTypeTableConfig) tableConfig;
		int recordCount=initAllRecordsMap(table, prodTypeTableConfig.getFilter());
		exportBean.getFileNamesAndCount().put(tableConfig.getFileName(), recordCount);
		initLeaves();
		
		final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
		try {
			final String header = createHeader(prodTypeTableConfig);
			writer.write(header);
			writer.newLine();
			
			printEffectiveDate = DSMTUtils.treeEffectiveDate();
			// Loop through each leaf record
			for (CachedLeafInfo leaf: leaves) {
				final String rowStr = createLeafRow(leaf, prodTypeTableConfig);
				writer.write(rowStr);
				writer.newLine();
			}
			
			final String footer = createFooter(prodTypeTableConfig);
			writer.write(footer);
			writer.newLine();
		} finally {
			writer.close();
		}
	}
	
	/**
	 * Create the header row
	 * 
	 * @param tableConfig the table config
	 * @return the header row
	 */
	protected String createHeader(ProdTypeTableConfig tableConfig) {
		LOG.debug("ProdTypeFlatTableExporter: createHeader");
		final StringBuffer strBuff = new StringBuffer();
		strBuff.append("H");
		final char separator = tableConfig.getSeparator();
		strBuff.append(separator);
		strBuff.append(tableConfig.getTableExportName());
		strBuff.append(separator);
		final SimpleDateFormat dateFormat = new SimpleDateFormat(HEADER_DATE_PATTERN);
		strBuff.append(dateFormat.format(new Date()));
		strBuff.append(separator);
		strBuff.append(tableConfig.getFileName());
		return strBuff.toString();
	}
	
	/**
	 * Create the leaf row
	 * 
	 * @param leaf the leaf cached info
	 * @param tableConfig the table config
	 * @return the leaf row
	 * @throws OperationException if an error occurred creating the row
	 */
	protected String createLeafRow(CachedLeafInfo leaf, ProdTypeTableConfig tableConfig)
			throws OperationException {
		if (LOG.isDebug()) {
			LOG.debug("ProdTypeFlatTableExporter: createLeafRow");
			LOG.debug("ProdTypeFlatTableExporter: leaf.pk = " + leaf.pk.format());
			LOG.debug("ProdTypeFlatTableExporter: leaf.level = " + leaf.level);
		}
		final Adaptation record = allRecordsMap.get(leaf.pk);
		final int level = leaf.level;
		if (level > tableConfig.getNumOfLevels()) {
			throw OperationException.createError("Record " + leaf.pk.format() + " has level of " + level
					+ " which is greater than max level of " + tableConfig.getNumOfLevels());
		}
		
		// strBuff will contain everything before the root
		final StringBuffer strBuff = new StringBuffer();
		strBuff.append("L");
		final char separator = tableConfig.getSeparator();
		strBuff.append(separator);
		String productCode = record.getString(PRODUCT_CODE_PATH);
		strBuff.append(productCode);
		strBuff.append(separator);
		
		// final Date effDate = record.getDate(EFFECTIVE_DATE_PATH);
		Object effDate = record.get(EFFECTIVE_DATE_PATH);
		String effDateStr = "";
		
		/** Custom effective date print for Hierarchical Records */
		if(null != printEffDateField && printEffDateField.equalsIgnoreCase(String.valueOf(effDate))){
			effDate = printEffectiveDate;
		//	LOG.debug("replaced Print effective date column");
		}
		
		effDateStr = String.valueOf(effDate); //FormattedExporterUtils.getStringForType(SchemaTypeName.XS_DATE, tableConfig.getFormattedOptions().getPatterns(), effDate);
		
		strBuff.append(effDateStr);
		strBuff.append(separator);
		
		// strBuff2 will start with the leaf's code & desc
		final StringBuffer strBuff2 = new StringBuffer();
		strBuff2.append(productCode);
		strBuff2.append(separator);
		String productDesc = record.getString(PRODUCT_CHILD_DESC);
		strBuff2.append(productDesc);
		
		// Now we pad out strBuff2 at the end with blanks, if it's less levels than what was specified
		for (int i = level; i < tableConfig.getNumOfLevels(); i++) {
			strBuff2.append(separator);
			strBuff2.append(" ");
			strBuff2.append(separator);
			if (i < tableConfig.getNumOfLevels() - 1) {
				strBuff2.append(" ");
			}
		}
		
		// Now we loop up through the hierarchy towards the root, prepending each parent to strBuff2
		// (so after we're done, strBuff2 will begin with the root node).
		for (String pk = record.getString(PARENT_FK_PATH);
				pk != null && allRecordsMap.containsKey(PrimaryKey.parseString(pk));) {
			final PrimaryKey parentPK = PrimaryKey.parseString(pk);
			final Adaptation parent = allRecordsMap.get(parentPK);
			
			strBuff2.insert(0, separator);
			productDesc = parent.getString(PRODUCT_PARENT_DESC);
			strBuff2.insert(0, productDesc);
			strBuff2.insert(0, separator);
			productCode = parent.getString(PRODUCT_CODE_PATH);
			strBuff2.insert(0, productCode);
			
			pk = parent.getString(PARENT_FK_PATH);
		}
		// Now return the 2 buffers concatenated
		return strBuff.toString() + strBuff2.toString();
	}
	
	/**
	 * Create the footer
	 * 
	 * @param tableConfig the table config
	 * @return the footer
	 */
	protected String createFooter(DataSetExportTableConfig tableConfig) {
		LOG.debug("ProdTypeFlatTableExporter: createFooter");
		final StringBuffer strBuff = new StringBuffer();
		strBuff.append("T");
		strBuff.append(tableConfig.getSeparator());
		strBuff.append(leaves.size());
		return strBuff.toString();
	}
	
	private int initAllRecordsMap(final AdaptationTable table, final AdaptationFilter filter) {
		if (LOG.isDebug()) {
			LOG.debug("ProdTypeFlatTableExporter: initAllRecordsMap");
			LOG.debug("ProdTypeFlatTableExporter: table = " + table.getTablePath().format());
		}
		allRecordsMap = new HashMap<PrimaryKey, Adaptation>();
		// If you specify no predicate, it will request every record in the table
		// (that meets any filter you set)
		final Request request = table.createRequest();
		if (filter != null) {
			request.setSpecificFilter(filter);
		}
		final RequestResult reqRes = request.execute();
		int recordCount=reqRes.getSize();
		try {
			// Loop through all the results and put each into the map
			for (Adaptation adaptation; (adaptation = reqRes.nextAdaptation()) != null;) {
				allRecordsMap.put(adaptation.getOccurrencePrimaryKey(), adaptation);
			}
		} finally {
			reqRes.close();
		}
		return recordCount;
	}
	
	private void initLeaves() {
		LOG.debug("ProdTypeFlatTableExporter: initLeaves");
		leaves = new ArrayList<CachedLeafInfo>();
		final Set<PrimaryKey> allPKs = allRecordsMap.keySet();
		// Loop over each primary key and if it's a leaf,
		// add a new CachedLeafInfo to the leaves list
		for (PrimaryKey pk: allPKs) {
			final Adaptation adaptation = allRecordsMap.get(pk);
			if (isLeaf(adaptation)) {
				final int level = getLevel(adaptation);
				final CachedLeafInfo cachedLeafInfo = new CachedLeafInfo();
				cachedLeafInfo.pk = pk;
				cachedLeafInfo.level = level;
				leaves.add(cachedLeafInfo);
			}
		}
		// Sort the list of leaves, using the CachedLeafInfo's comparison method.
		Collections.sort(leaves);
	}
	
	// Determines if a record is a leaf (no other record has it as its parent)
	private boolean isLeaf(final Adaptation adaptation) {
		final String pk = adaptation.getOccurrencePrimaryKey().format();
		final Set<PrimaryKey> allPKs = allRecordsMap.keySet();
		// Loop through all primary keys
		for (PrimaryKey otherPK: allPKs) {
			final Adaptation otherAdaptation = allRecordsMap.get(otherPK);
			final String parentFK = otherAdaptation.getString(PARENT_FK_PATH);
			// If this other record has the specified record as its parent then
			// the specified record isn't a leaf so return false
			if (parentFK != null && pk.equals(parentFK)) {
				return false;
			}
		}
		// We looped through all the records and didn't find any that had
		// the specified record as its parent, so it's a leaf.
		return true;
	}
	
	// Get the level in the hierarchy for a record (with root being level 1)
	private int getLevel(final Adaptation adaptation) {
		int level = 1;
		// Start with the parent of the specified record and loop up through the hierarchy
		// until there is no parent
		for (String pk = adaptation.getString(PARENT_FK_PATH);
				pk != null && allRecordsMap.containsKey(PrimaryKey.parseString(pk));) {
			level++;
			final PrimaryKey parentPK = PrimaryKey.parseString(pk);
			final Adaptation parent = allRecordsMap.get(parentPK);
			pk = parent.getString(PARENT_FK_PATH);
		}
		return level;
	}
	
	/**
	 * A class that caches info about the leaf nodes
	 */
	protected class CachedLeafInfo implements Comparable<CachedLeafInfo> {
		/**
		 * The primary key of the leaf record
		 */
		protected PrimaryKey pk;
		/**
		 * The level of the leaf record in the hierarchy (with root level being 1)
		 */
		protected int level;
		
		/**
		 * Overridden so that they can be ordered according to primary key
		 */
		@Override
		public int compareTo(CachedLeafInfo obj) {
			return pk.format().compareTo(obj.pk.format());
		}
	}
	
	private final static String printEffDateField = ResourceBundle.getBundle("com.citi.ebx.dsmt.util.ApplicationResources").getString("hierarchy.print.effective.date.field");
	//private final static String printEffectiveDate = new DSMTPropertyHelper().getProp("hierarchy.print.effective.date");
	private String printEffectiveDate = null;
}
