package com.citi.ebx.dsmt.exporter.channel;

import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;

public class ChannelTableConfig extends DataSetExportTableConfig {
	public enum ChannelTExportType {
		// Currently only one type but there will be more
		FLAT
	}
	
	private static final int DEFAULT_NUM_OF_LEVELS = 16;
	private static final String DEFAULT_TABLE_EXPORT_NAME = "PROJECT";
	
	protected ChannelTExportType channelExportType;
	protected int numOfLevels = DEFAULT_NUM_OF_LEVELS;
	protected String tableExportName = DEFAULT_TABLE_EXPORT_NAME;



	public ChannelTExportType getChannelExportType() {
		return channelExportType;
	}

	public void setChannelExportType(ChannelTExportType channelExportType) {
		this.channelExportType = channelExportType;
	}

	/**
	 * Get the number of levels to output
	 * 
	 * @return the number of levels to output
	 */
	public int getNumOfLevels() {
		return numOfLevels;
	}

	/**
	 * Set the number of levels to output
	 * 
	 * @param numOfLevels the number of levels to output
	 */
	public void setNumOfLevels(int numOfLevels) {
		this.numOfLevels = numOfLevels;
	}

	/**
	 * Get the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @return the name of the table
	 */
	public String getTableExportName() {
		return tableExportName;
	}

	/**
	 * Set the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @param tableExportName the name of the table
	 */
	public void setTableExportName(String tableExportName) {
		this.tableExportName = tableExportName;
	}
}
