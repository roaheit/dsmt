package com.citi.ebx.dsmt.exporter.channel;


import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporter;

public class ChannelTableExporterFactory extends DefaultTableExporterFactory {
	/**
	 * Overridden in order to handle channelectDSMT type exports
	 */
	@Override
	public TableExporter getTableExporter(DataSetExportTableConfig tableConfig, AdaptationTable table) {
		if (tableConfig instanceof ChannelTableConfig) {
			ChannelTableConfig channelTableConfig = (ChannelTableConfig) tableConfig;
			switch (channelTableConfig.getChannelExportType()) {
			
			case FLAT:
				LOG.info("ProjectTableFlatExporter : " );
				return new ChannelTableFlatExporter();
			}
		}
		return super.getTableExporter(tableConfig, table);
	}
}
