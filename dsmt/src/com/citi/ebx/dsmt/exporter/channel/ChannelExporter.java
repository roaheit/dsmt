package com.citi.ebx.dsmt.exporter.channel;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import com.citi.ebx.dsmt.exporter.channel.ChannelTableConfig.ChannelTExportType;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTExportBean;
import com.citi.ebx.util.SaveUtil;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig.ColumnHeaderType;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig.ExportType;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.FormattedDataSetExportOptions;
import com.orchestranetworks.ps.exporter.FormattedDataSetExportOptions.PatternType;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.ps.filter.ActiveRecordFilter;
import com.orchestranetworks.ps.filter.CurrentRecordFilter;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

public class ChannelExporter extends DataSetExporter {

	private static final char SEPARATOR = '~';
	private static final String CHANNEL_HIERARCHY_FILENAME = "channel.dsmt.hierarchy.filename";
	private static final String CHANNEL_HIERARCHY_XPATH = "channel.dsmt.hierarchy.xpath";
	private static final String CHANNEL_FLAT_DATE_PATTERN = "yyyy-MM-dd";
	private ResourceBundle bundle = ResourceBundle.getBundle("com.citi.ebx.dsmt.util.ApplicationResources");
	

		
	public ChannelExporter() {
		super();
	}
	
	public ChannelExporter(final TableExporterFactory tableExporterFactory,
			final DataSetExportConfig config) {
		super(tableExporterFactory, config);
	}

	/**
	 * Overridden in order to produce the Channel type exports
	 */
	@Override
	public DSMTExportBean exportDataSet(Session session, Adaptation dataSet)
			throws IOException, OperationException {
		LOG.info("ChanneltDSMTExporter: exportDataSet");
		
		final DataSetExportTableConfig channelFlatTableConfig = createChannelFlatTableConfig();
		config.getTableConfigs().add(channelFlatTableConfig);
		
		DSMTExportBean dsmtExportBean =super.exportDataSet(session, dataSet);
		final String userID = session.getUserReference().getUserId();		
		
		
		for(String fileName:dsmtExportBean.getFileNamesAndCount().keySet()){
			Map<Path, Object> inputMap= new HashMap<Path, Object>();
			inputMap.put(DSMTConstants.REPORT_NAME, fileName);
			inputMap.put(DSMTConstants.REPORT_LOCATION, dsmtExportBean.getOutputFilePath());
			inputMap.put(DSMTConstants.REPORT_ROW_COUNT, dsmtExportBean.getFileNamesAndCount().get(fileName));
			inputMap.put(DSMTConstants.REPORT_STATUS, "Success");
			inputMap.put(DSMTConstants.REPORT_LASTUPDOPRID, userID);
			inputMap.put(DSMTConstants.REPORT_TIMESTAMP,Calendar.getInstance().getTime());		
			
			SaveUtil saveUtil = new SaveUtil(dataSet.getHome().getRepository(),DSMTConstants.DATASPACE_DSMT2,DSMTConstants.DATASET_NEW_DSMT2,DSMTConstants.REPORT_AUDIT_PATH,inputMap,false );
			Utils.executeProcedure(saveUtil, session, dataSet.getHome().getRepository().lookupHome(
					HomeKey.forBranchName(DSMTConstants.DATASPACE_DSMT2)));
			
			Map<Path, Object> updateMap= new HashMap<Path, Object>();
			updateMap.put(DSMTConstants.CATALOG_TABLE_FILENAME, fileName);
			updateMap.put(DSMTConstants.CATALOG_TABLE_COUNT, dsmtExportBean.getFileNamesAndCount().get(fileName));
			
			
			
			
			SaveUtil asaveUtil = new SaveUtil(dataSet.getHome().getRepository(),DSMTConstants.DATASPACE_DSMT2,DSMTConstants.DATASET_NEW_DSMT2,DSMTConstants.CATALOG_TABLE_PATH,updateMap,true );
			Utils.executeProcedure(asaveUtil, session, dataSet.getHome().getRepository().lookupHome(
					HomeKey.forBranchName(DSMTConstants.DATASPACE_DSMT2)));
		}
		
		return null;
	}
	
	// Create the table config for the channel type flat export
	// This config is not read from EBX, but is constructed here
	private DataSetExportTableConfig createChannelFlatTableConfig() {
		
		String channelDSMTTablePath = bundle.getString(CHANNEL_HIERARCHY_XPATH);
		LOG.info("channelDSMTTablePath" +channelDSMTTablePath);
		final ChannelTableConfig tableConfig = new ChannelTableConfig();
		tableConfig.setChannelExportType(ChannelTExportType.FLAT);
		tableConfig.setType(ExportType.FORMATTED);
		tableConfig.setTablePath(channelDSMTTablePath);
		tableConfig.setFileName( bundle.getString(CHANNEL_HIERARCHY_FILENAME));
		tableConfig.setColumnHeader(ColumnHeaderType.NONE);
		tableConfig.setSeparator(SEPARATOR);
		tableConfig.setFilter(new CurrentRecordFilter());
		final FormattedDataSetExportOptions formattedOptions = new FormattedDataSetExportOptions();
		final HashMap<PatternType, String> patterns = new HashMap<PatternType, String>();
		patterns.put(PatternType.DATE, CHANNEL_FLAT_DATE_PATTERN);
		formattedOptions.setPatterns(patterns);
		tableConfig.setFormattedOptions(formattedOptions);
		LOG.info("tableConfig" +tableConfig);
		return tableConfig;
	}
}

