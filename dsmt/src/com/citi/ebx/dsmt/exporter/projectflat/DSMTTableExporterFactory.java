package com.citi.ebx.dsmt.exporter.projectflat;

import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporter;
/**
 * A factory for table exporters that handles DSMT-specific implementations
 */
public class DSMTTableExporterFactory extends DefaultTableExporterFactory {
	/**
	 * Overridden in order to handle proj type exports
	 */
	@Override
	public TableExporter getTableExporter(DataSetExportTableConfig tableConfig, AdaptationTable table) {
		if (tableConfig instanceof ProjectDSMTTableConfig) {
			ProjectDSMTTableConfig projectTableConfig = (ProjectDSMTTableConfig) tableConfig;
			switch (projectTableConfig.getProjectExportType()) {
			
			case FLAT:
				return new ProjectDSMTTableFlatExporter();
			}
		}
		return super.getTableExporter(tableConfig, table);
	}
}
