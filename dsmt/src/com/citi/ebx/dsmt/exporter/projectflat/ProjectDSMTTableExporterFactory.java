package com.citi.ebx.dsmt.exporter.projectflat;


import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporter;

public class ProjectDSMTTableExporterFactory extends DefaultTableExporterFactory {
	/**
	 * Overridden in order to handle projectDSMT type exports
	 */
	@Override
	public TableExporter getTableExporter(DataSetExportTableConfig tableConfig, AdaptationTable table) {
		if (tableConfig instanceof ProjectDSMTTableConfig) {
			ProjectDSMTTableConfig projTableConfig = (ProjectDSMTTableConfig) tableConfig;
			switch (projTableConfig.getProjectExportType()) {
			
			case FLAT:
				LOG.info("ProjectTableFlatExporter : " );
				return new ProjectDSMTTableFlatExporter();
			}
		}
		return super.getTableExporter(tableConfig, table);
	}
}
