package com.citi.ebx.dsmt.exporter.task;

import com.citi.ebx.dsmt.util.BPMReqIDUtil;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class TestBMPScheduleTask extends ScheduledTask {
	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private String soeID;
	
	@Override
	public void execute(ScheduledExecutionContext context)
			throws OperationException, ScheduledTaskInterruption {
		
		
		try{
			BPMReqIDUtil bmpUtil = new BPMReqIDUtil();
			bmpUtil.getRequestIDFromBPM(soeID);
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	public String getSoeID() {
		return soeID;
	}

	public void setSoeID(String soeID) {
		this.soeID = soeID;
	}
}