package com.citi.ebx.dsmt.exporter.task;

import com.citi.ebx.dsmt.exporter.projectflat.ProjectDSMTExporter;
import com.citi.ebx.dsmt.exporter.projectflat.ProjectDSMTTableExporterFactory;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.ps.exporter.task.DataSetExporterScheduledTask;

/**
 * A DataSetExportScheduledTask that handles DSMT-specific exports
 */

public class ProjectDSMTExportScheduledTask extends DataSetExporterScheduledTask {
	@Override
	protected DataSetExporter createExporter(TableExporterFactory tableExporterFactory,
			DataSetExportConfig config) {
		return new ProjectDSMTExporter(tableExporterFactory, config);
	}

	@Override
	protected TableExporterFactory createTableExporterFactory() {
		return new ProjectDSMTTableExporterFactory();
	}
}

	
	

