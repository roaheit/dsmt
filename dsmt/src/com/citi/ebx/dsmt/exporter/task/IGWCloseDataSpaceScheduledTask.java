package com.citi.ebx.dsmt.exporter.task;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;



import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;

public class IGWCloseDataSpaceScheduledTask extends ScheduledTask{
	
	

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	@Override
	public void execute(ScheduledExecutionContext context) throws OperationException, ScheduledTaskInterruption {
		Session session = context.getSession();
		Repository rp = context.getRepository();
		try {
			closeChildDS(session, rp);
		} catch (Exception e) {
			LOG.error("Exception while deleting dataspaces: " + e.getMessage());
		}

	}

	private void closeChildDS(Session session, Repository rp)
			throws OperationException {

		LOG.debug("Inside execute of DeleteDataSpaceScheduleTask");
		Calendar cal = Calendar.getInstance();
		cal.add(cal.DATE, -1);

		final String endTimeString = DSMTUtils.formatDate(cal.getTime(),
				DSMTConstants.DEFAULT_DATETIME_FORMAT);
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(rp,
				DSMTConstants.GOC_WF_DATASPACE, DSMTConstants.GOC_WF_DATASET);

		AdaptationTable targetTable = metadataDataSet
				.getTable(GOCWFPaths._C_DSMT_INT_GOC_REPORT.getPathInSchema());
		String pred1 = GOCWFPaths._C_DSMT_INT_GOC_REPORT._REQUEST_STATUS.format()
				+ " = '"
				+ GOCConstants.SUBMITTED
				+ "' and (date-less-than("
				+ GOCWFPaths._C_DSMT_INT_GOC_REPORT._REQ_DT.format()
				+ ",'"
				+ endTimeString + "'))";

		RequestResult rs = targetTable.createRequestResult(pred1);
		

		try {
			for (Adaptation record; (record = rs.nextAdaptation()) != null;) {
				ArrayList<Adaptation> childDataSpacelist = new ArrayList<Adaptation>();
				String spaceID = record.getString(GOCWFPaths._C_DSMT_INT_GOC_REPORT._CHILD_DATASPACE_ID);
				LOG.debug("spaceID : " + spaceID);
				int reqId = record.get_int(GOCWFPaths._C_DSMT_INT_GOC_REPORT._REQUEST_ID);
				String pred2 = GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL._REQUEST_ID
						.format() + " = '" + reqId + "'";

				AdaptationTable IGW_DTL = metadataDataSet.getTable(GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL.getPathInSchema());
				RequestResult dtlRec = IGW_DTL.createRequestResult(pred2);
				boolean finalStatus = true;
				for (Adaptation detailRec; (detailRec = dtlRec.nextAdaptation()) != null;) {

					String WFReqID = detailRec.getString((GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL._WFREQ_ID));
					AdaptationTable GOCRepTable = metadataDataSet.getTable(DSMTConstants.GOC_REPORTING_TABLE_PATH);

					String GOCWFStatusPred = GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_ID.format() + " = '" + WFReqID + "'";
					RequestResult GOCWFRep = GOCRepTable.createRequestResult(GOCWFStatusPred);

					Adaptation GOCWFRepRec = GOCWFRep.nextAdaptation();
					String WFReqStatus = GOCWFRepRec.getString(GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
					if (WFReqStatus != null
							&& (WFReqStatus.contains("Request approved") || WFReqStatus.contains("Rejected at") || (WFReqStatus.contains("Deleted")))) {

					} else {
						finalStatus = false;
					}
				}
				if (finalStatus) {
					try {
						if(dtlRec.getSize() == 1){
							//for only one grid routing id in request
							childDataSpacelist.add(record);
						}else{
						childDataSpacelist.add(record);
						AdaptationHome home = rp.lookupHome(HomeKey
								.forBranchName(spaceID));
						rp.closeHome(home, session);
						rp.deleteHome(home, session);
						rp.getPurgeDelegate().markHomeForHistoryPurge(home,
								session);
						LOG.info("Dataspace  Closed " + spaceID);
						}
						 AdaptationHome dataSpaceRef = rp.lookupHome(HomeKey.forBranchName(DSMTConstants.GOC_WF_DATASPACE)); 
						 final UpdateRecordsProcedure updateProc = new UpdateRecordsProcedure(childDataSpacelist);
						 Utils.executeProcedure(updateProc, session, dataSpaceRef);

					} catch (Exception e) {

						LOG.error("Error occured while Closing  dataspace : "
								+ spaceID);
						e.printStackTrace();
					}
				}
			}

			 
		} catch (Exception e) {
			LOG.error("Exception while iterating over the resultset"
					+ e.getMessage());
			e.printStackTrace();
		} finally {
			rs.close();
		}
	}
	
	
	private class UpdateRecordsProcedure implements Procedure {
		private ArrayList<Adaptation> records;
		public UpdateRecordsProcedure(ArrayList<Adaptation> records) {
			this.records = records;
		}
		
		@Override
		public void execute(ProcedureContext pContext) throws Exception {

			pContext.setAllPrivileges(true);
			for (Adaptation record: records) {
				updateRecord(record, pContext);
			}
			pContext.setAllPrivileges(false);
		}

		private void updateRecord(final Adaptation record, final ProcedureContext pContext)
				throws OperationException {
			final ValueContextForUpdate vc = pContext.getContext(record.getAdaptationName());
			LOG.info("CloseDatasapceSchedule update record  : ");
			try{
				vc.setValue(GOCConstants.APPROVED, GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);
				pContext.doModifyContent(record, vc);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}