package com.citi.ebx.dsmt.exporter.task;

import java.sql.Connection;
import java.sql.SQLException;

import com.citi.ebx.dsmt.jdbc.connector.FMSReportUtil;
import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class FMSReportScheduledtask extends ScheduledTask{
	 protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	 protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	   
		
		
		protected String exportFileDirectoryPath;
		
		
		
		@Override
		public void execute(ScheduledExecutionContext arg0)
				throws OperationException, ScheduledTaskInterruption {
			
			generateSQLReport();
		
		}
		
		public void generateSQLReport (){
			
			LOG.info("Report generation started !! "); 
			Connection conn=null;
			
			try{
				
				FMSReportUtil fmsReportUtil = new FMSReportUtil();
				
				conn = SQLReportConnectionUtils.getConnection();
				fmsReportUtil.generateReport(conn, getExportFileDirectoryPath());
				LOG.info("Report  generated successfully !!!   ");	
				}
			catch (Exception e){
				
				LOG.error("Report generation terminated with Exception in FMSReportScheduledTask  -> " + e + ", message = " + e.getMessage());
			}
			finally{
				
				if(null != conn){
					try{
					conn.close();
					conn = null;
					}
					catch (SQLException SQ){
						LOG.error("Report generation terminated with Exception -> " + SQ + ", message = " + SQ.getMessage());
					}
				}
			}
			
			
		}
		
		
		
		



		public String getExportFileDirectoryPath() {
			return exportFileDirectoryPath;
		}

		public void setExportFileDirectoryPath(String exportFileDirectoryPath) {
			this.exportFileDirectoryPath = exportFileDirectoryPath;
		}

		
		
		
}
