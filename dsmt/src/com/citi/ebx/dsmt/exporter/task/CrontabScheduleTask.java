package com.citi.ebx.dsmt.exporter.task;

import java.io.IOException;

import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class CrontabScheduleTask extends ScheduledTask {

	private String scriptName;
	private String numberofArguments;
	private String argument1;
	private String argument2;
	private String argument3;
	private String argument4;
	private String argument5;
	public String getScriptName() {
		return scriptName;
	}
	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}
	public String getNumberofArguments() {
		return numberofArguments;
	}
	public void setNumberofArguments(String numberofArguments) {
		this.numberofArguments = numberofArguments;
	}
	public String getArgument1() {
		return argument1;
	}
	public void setArgument1(String argument1) {
		this.argument1 = argument1;
	}
	public String getArgument2() {
		return argument2;
	}
	public void setArgument2(String argument2) {
		this.argument2 = argument2;
	}
	public String getArgument3() {
		return argument3;
	}
	public void setArgument3(String argument3) {
		this.argument3 = argument3;
	}
	public String getArgument4() {
		return argument4;
	}
	public void setArgument4(String argument4) {
		this.argument4 = argument4;
	}
	public String getArgument5() {
		return argument5;
	}
	public void setArgument5(String argument5) {
		this.argument5 = argument5;
	}

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
		@Override
	public void execute(ScheduledExecutionContext context) throws OperationException, ScheduledTaskInterruption {
			String executionString=scriptName+" ";
			LOG.info("numberofArguments ::::"+numberofArguments);
			String[] argumentArray={argument1,argument2,argument3,argument4,argument5};
			LOG.info("Arguments Passed : "+argument1+" "+argument2+" "+argument3+" "+argument4+" "+argument5);
			String argumentsToPass="";
			if(Integer.parseInt(numberofArguments)!=0){
			for(int i=0;i<Integer.parseInt(numberofArguments);i++)
			{
				argumentsToPass+=argumentArray[i]+" ";
			}
			}
		executionString=executionString+argumentsToPass+">>"+scriptName+".log";
		LOG.info("executionString"+executionString);
		ProcessBuilder pb=new ProcessBuilder("ksh",executionString);
		try {
			Process p = pb.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // throws IOException
		//Runtime.getRuntime().exec(scriptName);	
		LOG.info("Script Execution Completed");
		}
}
