package com.citi.ebx.dsmt.exporter.task;

import java.sql.Connection;
import java.sql.SQLException;

import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.SQLReportGeneratorUtil;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class MSHierSQLReportScheduledTask extends ScheduledTask {
	 protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	 protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	   
		
		protected String sqlID;
		protected String exportFileDirectoryPath;
		protected String exportFileName;
		private String reportType;
		final String env =  propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
		
		@Override
		public void execute(ScheduledExecutionContext arg0)
				throws OperationException, ScheduledTaskInterruption {			
				generateSQLReport();
		
		}
		
		public void generateSQLReport (){
			
			LOG.info("Report generation started !! "); 
			Connection conn=null;
			
			try{
				
				SQLReportGeneratorUtil sqlReportUtil = new SQLReportGeneratorUtil(); 
				reportType = DSMTConstants.MSREPORT;
				conn = SQLReportConnectionUtils.getConnection();
				sqlReportUtil.generateReport(conn, getExportFileDirectoryPath(),reportType);
				LOG.info("Report  generated successfully !!!   ");	
				}
			catch (Exception e){
				
				LOG.error("Report generation terminated with Exception in MSHierSQLReportScheduledTask  -> " + e + ", message = " + e.getMessage());
			}
			finally{
				
				if(null != conn){
					try{
					conn.close();
					conn = null;
					}
					catch (SQLException SQ){
						LOG.error("Report generation terminated with Exception -> " + SQ + ", message = " + SQ.getMessage());
					}
				}
			}
			
			
		}

		public String getSqlID() {
			return sqlID;
		}



		public void setSqlID(String sqlID) {
			this.sqlID = sqlID;
		}



		public String getExportFileDirectoryPath() {
			return exportFileDirectoryPath;
		}

		public void setExportFileDirectoryPath(String exportFileDirectoryPath) {
			this.exportFileDirectoryPath = exportFileDirectoryPath;
		}

		public String getExportFileName() {
			return exportFileName;
		}

		public void setExportFileName(String exportFileName) {
			this.exportFileName = exportFileName;
		}
		
}
