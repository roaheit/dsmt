package com.citi.ebx.dsmt.exporter.task;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.citi.ebx.dsmt.jdbc.connector.SQLConnectReportUtil;
import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTExportBean;
import com.citi.ebx.dsmt.util.HeaderTailerGenerator;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

/**
 * A SQLExportScheduledTask that handles DSMT-specific SQL exports
 */
public class SQLExportScheduledTask extends ScheduledTask {

	   protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	   protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	   
	
	protected String sqlID;
	protected String exportFileDirectoryPath;
	protected String exportFileName;
	final String env =  propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
	
	@Override
	public void execute(ScheduledExecutionContext arg0)
			throws OperationException, ScheduledTaskInterruption {
		
		generateSQLReport();
	
	}
	
	public Map<String, Integer> generateSQLReport (){
		
		LOG.info("Report generation started !! "); 
		
		Map<String, Integer> sqlReportMap = null;
		Connection conn=null;
		try{
			
			SQLConnectReportUtil connectReportUtil = new SQLConnectReportUtil();
			
			conn = SQLReportConnectionUtils.getConnection();
			exportFileDirectoryPath = SQLReportConnectionUtils.bundle.getString(env + DSMTConstants.DOT +"ExportFileDirectory");
			if(null!=sqlID){
			connectReportUtil.setReportType(sqlID);
			}
			sqlReportMap = connectReportUtil.generateReport(conn, exportFileDirectoryPath);
			List<String> sqlreportList = new ArrayList<String>(sqlReportMap.keySet());
			DSMTExportBean dsmtExportBean = new DSMTExportBean();
			 dsmtExportBean.setFileNames(sqlreportList);
			 dsmtExportBean.setOutputFilePath(exportFileDirectoryPath);
			LOG.info("Report generated successfully !!!");
			
			new HeaderTailerGenerator().runHeaderTrailerGenerator(dsmtExportBean);
			LOG.info("HT  generated successfully !!!  for  reports :  "+sqlreportList);
				
			}
		catch (Exception e){
			
			LOG.error("Report generation terminated with Exception -> " + e + ", message = " + e.getMessage());
		}
		finally{
			
			if(null != conn){
				try{
				conn.close();
				conn = null;
				}
				catch (SQLException SQ){
					LOG.error("Report generation terminated with Exception -> " + SQ + ", message = " + SQ.getMessage());
				}
			}
		}
		
		return sqlReportMap;
	}
	
	
	
	public String getSqlID() {
		return sqlID;
	}



	public void setSqlID(String sqlID) {
		this.sqlID = sqlID;
	}



	public String getExportFileDirectoryPath() {
		return exportFileDirectoryPath;
	}

	public void setExportFileDirectoryPath(String exportFileDirectoryPath) {
		this.exportFileDirectoryPath = exportFileDirectoryPath;
	}

	public String getExportFileName() {
		return exportFileName;
	}

	public void setExportFileName(String exportFileName) {
		this.exportFileName = exportFileName;
	}
	
}
