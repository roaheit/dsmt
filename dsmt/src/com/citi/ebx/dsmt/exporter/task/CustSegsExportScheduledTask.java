package com.citi.ebx.dsmt.exporter.task;

import com.citi.ebx.dsmt.exporter.custsegs.CustSegsExporter;
import com.citi.ebx.dsmt.exporter.custsegs.CustSegsTableExporterFactory;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.ps.exporter.task.DataSetExporterScheduledTask;

public class CustSegsExportScheduledTask extends DataSetExporterScheduledTask {
	@Override
	protected DataSetExporter createExporter(TableExporterFactory tableExporterFactory,
			DataSetExportConfig config) {
		return new CustSegsExporter(tableExporterFactory, config);
	}

	@Override
	protected TableExporterFactory createTableExporterFactory() {
		return new CustSegsTableExporterFactory();
	}
}
