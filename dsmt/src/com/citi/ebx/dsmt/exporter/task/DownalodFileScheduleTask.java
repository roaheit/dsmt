package com.citi.ebx.dsmt.exporter.task;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class DownalodFileScheduleTask extends ScheduledTask {

	   protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	   protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	   
	
	
	protected String downloadtFileDirectoryPath;
	protected String downloadFileName;
	protected String urlLink;
	final String env =  propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
	
	@Override
	public void execute(ScheduledExecutionContext arg0)
			throws OperationException, ScheduledTaskInterruption {
		
		downloadFile();
	
	}
	
	public void downloadFile (){
		
		LOG.info("Download report started !! "); 
		String fileName = downloadtFileDirectoryPath+downloadFileName;
		
		try{
			URL link = new URL(urlLink); //The file that you want to download
			TrustManager[] trustAllCerts = new TrustManager[]{
				    new X509TrustManager() {public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				            return null;
				        }
				        public void checkClientTrusted(
				            java.security.cert.X509Certificate[] certs, String authType) {
				        }
				        public void checkServerTrusted(
				            java.security.cert.X509Certificate[] certs, String authType) {
				        }
				    }
				};
			
		 	//Code to download
			 LOG.info("set proxy !! "); 
			 SSLContext sc = SSLContext.getInstance("SSL");
			    sc.init(null, trustAllCerts, new java.security.SecureRandom());
			    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			    
			    System.setProperty("https.protocols", "TLSv1,SSLv3");
			 Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("webproxyp.wlb.nam.nsroot.net",80));
				HttpURLConnection httpConn = (HttpURLConnection) link.openConnection(proxy);
				 InputStream in = new BufferedInputStream(httpConn.getInputStream());
		 ByteArrayOutputStream out = new ByteArrayOutputStream();
		 byte[] buf = new byte[1024];
		 int n = 0;
		 while (-1!=(n=in.read(buf)))
		 {
		    out.write(buf, 0, n);
		 }
		 out.close();
		 in.close();
		 byte[] response = out.toByteArray();
		 LOG.info(" response :  " + response);
		 FileOutputStream fos = new FileOutputStream(fileName);
		 fos.write(response);
		 fos.close();
			
				
			}
		catch (Exception e){
			
			LOG.error("Report generation terminated with Exception -> " + e + ", message = " + e.getMessage());
		}
		
		
		
	}

	public String getDownloadtFileDirectoryPath() {
		return downloadtFileDirectoryPath;
	}

	public void setDownloadtFileDirectoryPath(String downloadtFileDirectoryPath) {
		this.downloadtFileDirectoryPath = downloadtFileDirectoryPath;
	}

	public String getDownloadFileName() {
		return downloadFileName;
	}

	public void setDownloadFileName(String downloadFileName) {
		this.downloadFileName = downloadFileName;
	}

	public String getUrlLink() {
		return urlLink;
	}

	public void setUrlLink(String urlLink) {
		this.urlLink = urlLink;
	}
	

	
}
