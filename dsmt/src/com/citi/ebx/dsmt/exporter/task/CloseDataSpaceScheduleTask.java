package com.citi.ebx.dsmt.exporter.task;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;

public class CloseDataSpaceScheduleTask extends ScheduledTask {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	protected String requestType;
	protected Date startTime;
	protected Date endTime;
	public String getBulkDeletePath() {
		return bulkDeletePath;
	}

	public void setBulkDeletePath(String bulkDeletePath) {
		this.bulkDeletePath = bulkDeletePath;
	}



	protected String bulkDeletePath;

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime; 
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}



	@Override
	public void execute(ScheduledExecutionContext context) throws OperationException, ScheduledTaskInterruption {
		Session session = context.getSession();
		Repository rp = context.getRepository();
		 ArrayList<String> dataSpaces = new ArrayList<String>();
		try {
			if (null == bulkDeletePath) {
				performStartEndDateDelete(session, rp);

			} else {
				dataSpaces=performBulkDelete(session, rp);
			}
			if(!(dataSpaces.isEmpty())){
				deleteDataspace(dataSpaces, rp, session);
			}
			
		} catch (Exception e) {
			LOG.error("Exception while deleting dataspaces: " + e.getMessage());
		}

	}

	private ArrayList<String> performBulkDelete(Session session, Repository rp) {

		
		
		File f= new File(bulkDeletePath);
		ArrayList<String> dataSpaces = new ArrayList<String>();
		try{
			LineIterator itr = FileUtils.lineIterator(f);
			
			while(itr.hasNext()){
				dataSpaces.add(itr.next().trim());
			}
			
		}catch (Exception e) {
			LOG.error("Unable read file at location:"+bulkDeletePath);
		}
		return dataSpaces;
		
		
	
		
	}

	


	private void performStartEndDateDelete(Session session,Repository rp) throws OperationException {

		
		
		LOG.debug("Inside execute of CloseDataSpaceScheduleTask --> performStartEndDateDelete");
		
		//DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	    Calendar cal = Calendar.getInstance();
	    cal.add(cal.DATE, -30);
		
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(rp, DSMTConstants.GOC_WF_DATASPACE,DSMTConstants.GOC_WF_DATASET);
		
		AdaptationTable targetTable = metadataDataSet.getTable(DSMTConstants.GOC_REPORTING_TABLE_PATH);
		
		String pred1= GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS.format() + " = '"
					+ GOCConstants.CREATED + "'";			
		final String endTimeString = DSMTUtils.formatDate(cal.getTime(), DSMTConstants.DEFAULT_DATETIME_FORMAT);
		
		String pred3= "(date-less-than(" + GOCWFPaths._C_GOC_WF_REPORTING._REQ_DT.format()
			+ ",'"+endTimeString+"'))";


			
		
		RequestResult rs = targetTable.createRequestResult(pred1 + " and " + pred3);
				
		 ArrayList<Adaptation> childDataSpacelist = new ArrayList<Adaptation>();

		try {

			for (Adaptation record; (record = rs.nextAdaptation()) != null;) {
				String spaceID = record.getString(GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID);
				LOG.debug ("spaceID  for Direct maintenance: "+ spaceID);
				
				try{
					childDataSpacelist.add(record);
					AdaptationHome home = rp.lookupHome(HomeKey.forBranchName(spaceID));
					rp.closeHome(home, session);
					rp.deleteHome(home, session);
					rp.getPurgeDelegate().markHomeForHistoryPurge(home,
							session);
					LOG.info("Dataspace  Closed for Direct maintenance:"+ spaceID );
					
				}catch(Exception e){
					
					LOG.error("Error occured while Closing  dataspace : "+ spaceID );
					e.printStackTrace();
				}

			}
			
			AdaptationTable IGWTargetTable = metadataDataSet.getTable(GOCWFPaths._C_DSMT_INT_GOC_REPORT.getPathInSchema());
			
			RequestResult IGWRs = IGWTargetTable.createRequestResult(pred1 + " and " + pred3);
					for (Adaptation record; (record = IGWRs.nextAdaptation()) != null;) {
						String spaceID = record.getString(GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID);
						LOG.debug("spaceID  from IWG table: "+ spaceID);
						
						try{
							childDataSpacelist.add(record);
							AdaptationHome home = rp.lookupHome(HomeKey.forBranchName(spaceID));
							rp.closeHome(home, session);
							rp.deleteHome(home, session);
							rp.getPurgeDelegate().markHomeForHistoryPurge(home,
									session);
							LOG.info("Dataspace  Closed  from IWG table :"+ spaceID );
							
						}catch(Exception e){
							
							LOG.error("Error occured while Closing  dataspace : "+ spaceID );
							e.printStackTrace();
						}
		
					}
			
			// update request status as  deleted
			AdaptationHome dataSpaceRef = rp.lookupHome(HomeKey.forBranchName(DSMTConstants.GOC_WF_DATASPACE));
			final UpdateRecordsProcedure updateProc = new UpdateRecordsProcedure(childDataSpacelist);
			Utils.executeProcedure(updateProc, session, dataSpaceRef);
		}
		catch(Exception e){
			
			LOG.error("Exception while iterating over the resultset"+ e.getMessage());
		}
			finally {
		
			rs.close();
			
		}
		
		
	
		
	}
	private void deleteDataspace(ArrayList<String> dataSpaces, Repository rp, Session session) {
		for (String dataspace : dataSpaces) {

			try {
				AdaptationHome home = rp.lookupHome(HomeKey.forBranchName(dataspace));
				rp.closeHome(home, session);
				rp.deleteHome(home, session);
				rp.getPurgeDelegate().markHomeForHistoryPurge(home,
						session);
			} catch (Exception e) {
				LOG.error("Unable to close/delete dataspace: " + dataspace + e.getMessage());

			}

			LOG.debug("Closed dataspace : " + dataspace);
		}

	}
	
	private class UpdateRecordsProcedure implements Procedure {
		private ArrayList<Adaptation> records;
		
		public UpdateRecordsProcedure(ArrayList<Adaptation> records) {
			this.records = records;
			
		}
		
		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			pContext.setAllPrivileges(true);
			
			
			
			for (Adaptation record: records) {
				
				updateRecord(record, pContext);
			}
			
			
			pContext.setAllPrivileges(false);
		}
		
		private void updateRecord(final Adaptation record, final ProcedureContext pContext)
				throws OperationException {
			final ValueContextForUpdate vc = pContext.getContext(record.getAdaptationName());
			LOG.info("CloseDatasapceSchedule update record  : ");
			try{
			vc.setValue(GOCConstants.DELETED, GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);

			pContext.doModifyContent(record, vc);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	

}
