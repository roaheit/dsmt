package com.citi.ebx.dsmt.exporter.task;

import com.citi.ebx.dsmt.exporter.DSMTExporter;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.ps.exporter.task.DataSetExporterScheduledTask;

/**
 * A DataSetExportScheduledTask that handles DSMT-specific exports
 */
public class DSMTExportScheduledTask extends DataSetExporterScheduledTask {
	@Override
	protected DataSetExporter createExporter(TableExporterFactory tableExporterFactory,
			DataSetExportConfig config) {
		
		boolean isMergeReadmeToBeDone = false ;// "true".equalsIgnoreCase(mergeReadme) ;
		boolean isSQLReportsToBeRun = "true".equalsIgnoreCase(runSQLReports) ;
		
		return new DSMTExporter(tableExporterFactory, config, isMergeReadmeToBeDone, isSQLReportsToBeRun);
	}
}
