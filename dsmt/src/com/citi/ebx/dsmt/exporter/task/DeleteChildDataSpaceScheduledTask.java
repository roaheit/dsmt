
package com.citi.ebx.dsmt.exporter.task;

/**
 * @author am05884
 * This class has two different methods which is called by the EBX Scheduler.
 * As per the configuration in scheduler one method is invoked to generate the list of child data sets to be purged
 * Next Method reads a file which is passed as argument and is responsible to purge that data set.
 * 
 * Methods :
 * 
 * listForPurge: 
 * arguments : Session and Repository 
 * Returns : File containing all the child data set to be purged which are older than 120 days and are not updated in last 30 days
 * 
 * PurgeEvent:
 * Arguments : Session, Repository and File path containing the list of child data sets to be purged
 * Returns : Reference to all the child dataset to be purged.
 *
 */
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;

public class DeleteChildDataSpaceScheduledTask extends ScheduledTask {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	protected String requestType;
	protected Date startTime;
	protected Date endTime;
	private String distributionList;
	final String newLine = System.getProperty("line.separator");
	protected String event_Scheuled;
	protected String purge_list_File;
	protected String location_file_purge;
	protected String Created_date_timeframe;
	protected String last_update_timeframe;

	public String getCreated_date_timeframe() {
		return Created_date_timeframe;
	}

	public void setCreated_date_timeframe(String created_date_timeframe) {
		Created_date_timeframe = created_date_timeframe;
	}

	public String getLast_update_timeframe() {
		return last_update_timeframe;
	}

	public void setLast_update_timeframe(String last_update_timeframe) {
		this.last_update_timeframe = last_update_timeframe;
	}


	File report_file = null;
	
	public String getLocation_file_purge() {
		return location_file_purge;
	}

	public void setLocation_file_purge(String location_file_purge) {
		this.location_file_purge = location_file_purge;
	}

	ArrayList<Adaptation> dataSpacesDeleted = new ArrayList<Adaptation>();
	DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	public String getEvent_Scheuled() {
		return event_Scheuled;
	}

	public void setEvent_Scheuled(String event_Scheuled) {
		this.event_Scheuled = event_Scheuled;
	}

	public String getPurge_list_File() {
		return purge_list_File;
	}

	public void setPurge_list_File(String purge_list_File) {
		this.purge_list_File = purge_list_File;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	@Override
	public void execute(ScheduledExecutionContext context)
			throws OperationException, ScheduledTaskInterruption {
		Session session = context.getSession();
		Repository rp = context.getRepository();
		ArrayList<String> dataSpaces = new ArrayList<String>();
		try {
			if (event_Scheuled.equals("Purge List Generation")) {
				listForPurge(session, rp);

			} else if (event_Scheuled.equals("Purge")) {
		
				dataSpaces = purgeEvent(session, rp);
			}

			if (!(dataSpaces.isEmpty())) {
				LOG.info("Delete Data Space invoked");
				deleteDataspace(dataSpaces, rp, session);
				cwmReportingRequestID();
			}

		} catch (Exception e) {
			LOG.error("Exception while deleting dataspaces: " + e.getMessage());
		}

	}

	private ArrayList<String> purgeEvent(Session session, Repository rp) {

		File f = new File(purge_list_File);
		ArrayList<String> dataSpaces = new ArrayList<String>();
		StringTokenizer tokenString;

		try {
			LineIterator itr = FileUtils.lineIterator(f);
			itr.next();
			while (itr.hasNext()) {
				tokenString = new StringTokenizer(itr.next().trim(), ";");
				// tokenString.nextToken();
				dataSpaces.add(tokenString.nextToken());
				LOG.info("DataSpace read from the Purge File");
			}

		} catch (Exception e) {
			LOG.error("Unable read file at location:" + purge_list_File);
		}
		return dataSpaces;

	}

	private void listForPurge(Session session, Repository rp)
			throws OperationException {
		report_file=new File(location_file_purge);
		int created_date_timeFrame;
		int Last_update_timeframe;
		if(Created_date_timeframe==null || Created_date_timeframe.trim().equals(""))
		{
			created_date_timeFrame=-120;
			
		}
		else
		{
		created_date_timeFrame=new Integer(Created_date_timeframe).intValue();
		created_date_timeFrame=-created_date_timeFrame;
		}
		if(last_update_timeframe==null || last_update_timeframe.trim().equals(""))
		{
			Last_update_timeframe=-30;
		}
		else
		{
			Last_update_timeframe=new Integer(last_update_timeframe).intValue();
			Last_update_timeframe=-Last_update_timeframe;	
		}
		if (report_file.exists()) {

			DateFormat df2 = new SimpleDateFormat("MMddyyyy");
			Calendar prevMonth = Calendar.getInstance();
			prevMonth.add(Calendar.MONTH, -1);
			StringBuffer previousrunbackup = new StringBuffer(df2.format(prevMonth.getTime()));
			StringTokenizer fileRenaming=new StringTokenizer(location_file_purge, ".");
			StringBuffer backupFileName = new StringBuffer(fileRenaming.nextToken()+"_"+previousrunbackup+ "." +fileRenaming.nextToken());
			File movedFile = new File(backupFileName.toString());
			report_file.renameTo(movedFile);

		}
		
		Calendar createdDateCalender = Calendar.getInstance();
		createdDateCalender.add(Calendar.DATE, created_date_timeFrame);
		final StringBuffer endTimeString = new StringBuffer(DSMTUtils.formatDate(createdDateCalender.getTime(),
				DSMTConstants.DEFAULT_DATETIME_FORMAT));
		
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(rp,
				DSMTConstants.GOC_WF_DATASPACE, DSMTConstants.GOC_WF_DATASET);

		AdaptationTable targetTable = metadataDataSet
				.getTable(DSMTConstants.GOC_REPORTING_TABLE_PATH);

		StringBuffer predicate1 = new StringBuffer( GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_TYPE
				.format() + " = '" + GOCConstants.WORKFLOW_PUBLICATION + "'");
		

		StringBuffer predicate2 = new StringBuffer( "(date-less-than("
				+ GOCWFPaths._C_GOC_WF_REPORTING._REQ_DT.format() + ",'"
				+ endTimeString + "'))");

		Calendar lastUpdatedCalender = Calendar.getInstance();
		lastUpdatedCalender.add(Calendar.DATE, Last_update_timeframe);

		final StringBuffer lastUpdatedTime = new StringBuffer(DSMTUtils.formatDate(lastUpdatedCalender.getTime(),
				DSMTConstants.DEFAULT_DATETIME_FORMAT));
		
		StringBuffer predicate4 = new StringBuffer("(date-less-than("
				+ GOCWFPaths._C_GOC_WF_REPORTING._CHNG_DTTM.format()
				+ ",'" + lastUpdatedTime + "'))");
	
		StringBuffer predicate5 = new StringBuffer("(contains("+GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS.format() + ",'Pending'))");
		StringBuffer predicate6 = new StringBuffer("(contains("+GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS.format() + ",'Send for Rework at'))");
		RequestResult rs = targetTable.createRequestResult(predicate1 + " and "
				+ predicate2 + " and " + predicate4 + " and (" + predicate5 + " or "+ predicate6 +")" );
		int recordCounter=0;
		try {
			
			StringBuffer fileHeader=new StringBuffer("ChildDataSpace;RequestID;Requested By;Request Status;Request Created Date;Last Upated Date");
			FileUtils.writeStringToFile(report_file, fileHeader + newLine, true);
			
			for (Adaptation record; (record = rs.nextAdaptation()) != null;) {
				StringBuffer requestedDate = new StringBuffer(df
						.format(record
								.getDate(GOCWFPaths._C_GOC_WF_REPORTING._REQ_DT)));
				String lastChange = df
						.format(record
								.getDate(GOCWFPaths._C_GOC_WF_REPORTING._CHNG_DTTM));

				StringBuffer purge_list =new StringBuffer( (record
						.getString(GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID))
						+ ";"
						+ (record.getString(GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_ID))
						+ ";"
						+ (record.getString(GOCWFPaths._C_GOC_WF_REPORTING._REQ_BY))
						+ ";"
						+ (record.getString(GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS))
						+ ";" + requestedDate + ";"+ lastChange);

				recordCounter++;
				FileUtils.writeStringToFile(report_file, purge_list	+ newLine, true);
				try {
						if(recordCounter>0){
							final String subject = "DSMT2 Workflow request purge file created";
							final String message = "GOC Workflow purge file created.Generated file is present at " + location_file_purge;
							
						LOG.info("Sending Email");
						distributionList = DSMTConstants.propertyHelper
								.getProp(DSMTConstants.WORKFLOW_ADMIN_DISTRIBUTION_LIST);
						DSMTNotificationService.notifyEmailID(distributionList,
								subject,
								message, false);
					}
				} catch (Exception e) {

					LOG.info("Exception Sending Purge List Email");
					e.printStackTrace();
				}

			}
		} catch (Exception e) {

			LOG.error("Exception while iterating over the resultset"
					+ e.getMessage());
		} finally {
			rs.close();
		}

	}

	private void deleteDataspace(ArrayList<String> dataSpaces, Repository rp,
			Session session) {

		for (String dataspace : dataSpaces) {

			try {
				LOG.info("Iterating over DataSpace inside Deletion");
				AdaptationHome home = rp.lookupHome(HomeKey
						.forBranchName(dataspace));
				
				StringBuffer deletionPredicate = new StringBuffer( GOCWFPaths._C_GOC_WF_REPORTING._CHILD_DATASPACE_ID
						.format() + " = '" + dataspace + "'");
				LOG.info("deletion predicate"+deletionPredicate);
				Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(rp,
						DSMTConstants.GOC_WF_DATASPACE,
						DSMTConstants.GOC_WF_DATASET);
				LOG.info("MetaDataSet Retrieved");
				AdaptationTable targetTable = metadataDataSet
						.getTable(DSMTConstants.GOC_REPORTING_TABLE_PATH);
				LOG.info("TargetTable Retrieved"+targetTable);
				RequestResult rs = targetTable
						.createRequestResult(deletionPredicate.toString());
				LOG.info("ResultSet"+rs.toString());
				try {

					for (Adaptation record; (record = rs.nextAdaptation()) != null;) {
						dataSpacesDeleted.add(record);
					}
					if (dataSpacesDeleted != null
							&& dataSpacesDeleted.size() > 0) {
						AdaptationHome dataSpaceRef = rp.lookupHome(HomeKey
								.forBranchName(DSMTConstants.GOC_WF_DATASPACE));
						final UpdateRecordsProcedure updateProc = new UpdateRecordsProcedure(
								dataSpacesDeleted);
						LOG.info("Calling Updadt Execute Procedure");
						Utils.executeProcedure(updateProc, session,
								dataSpaceRef);
						LOG.info("Execute Procedure Invoked");
					}
					LOG.info("Closing Home"+home+session);
					rp.closeHome(home, session);
					LOG.info("Deleting Home");
					rp.deleteHome(home, session);
				} catch (Exception e) {

					LOG.error("Exception while iterating over the resultset"
							+ e.getMessage());
				} finally {
					rs.close();
				}
			} catch (Exception e) {
				LOG.error("Unable to close/delete dataspace: " + dataspace
						+ e.getMessage());

			}

			LOG.debug("Closed dataspace : " + dataspace);
		}

	}

	public void cwmReportingRequestID()
	{
		File modified_purge_file=new File(purge_list_File);
		File cwm_reporting_file=new File("RequestID.csv");
		StringTokenizer tokenString;
		int requestID_counter=0;
		final String subject = "DSMT2 Workflow Request ID for termination";
		
		String message = "Please Terminate the request ID present in Attached file which has been deleted from DSMT2\n";
		
		if (cwm_reporting_file.exists()) {
			DateFormat df2 = new SimpleDateFormat("MMddyyyy");
			Calendar prevMonth = Calendar.getInstance();
			prevMonth.add(Calendar.MONTH, -1);
			StringBuffer previousrunbackup = new StringBuffer(df2.format(prevMonth.getTime()));
			StringTokenizer fileRenaming=new StringTokenizer(location_file_purge, ".");
			StringBuffer backupFileName = new StringBuffer(fileRenaming.nextToken()+"_"+previousrunbackup+ "." +fileRenaming.nextToken());
			File movedFile = new File(backupFileName.toString());
			cwm_reporting_file.renameTo(movedFile);

		}

		try {
			LineIterator itr = FileUtils.lineIterator(modified_purge_file);
			String request_ID = "";
			itr.next();
			while (itr.hasNext()) {
				requestID_counter++;
				tokenString = new StringTokenizer(itr.next().trim(), ";");
				tokenString.nextToken();
				request_ID+=tokenString.nextToken()+",";			
			}
			message+=request_ID;

		} catch (Exception e) {
			LOG.error("Unable read file at location:" + purge_list_File);
		}
		try {
			if(requestID_counter>0){
				
			LOG.info("Sending Email");
			distributionList = DSMTConstants.propertyHelper
					.getProp(DSMTConstants.CWM_TERMINATOR);
			
					DSMTNotificationService.notifyEmailID(distributionList,
					subject,
					message, false);
		
		}
	} catch (Exception e) {

		LOG.info("Exception Sending Purge List Email");
		e.printStackTrace();
	}


		
	}
	private class UpdateRecordsProcedure implements Procedure {
		private ArrayList<Adaptation> records;

		public UpdateRecordsProcedure(ArrayList<Adaptation> records) {
			this.records = records;

		}

		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			pContext.setAllPrivileges(true);

			for (Adaptation record : records) {

				updateRecord(record, pContext);
			}

			pContext.setAllPrivileges(false);
		}

		private void updateRecord(final Adaptation record,
				final ProcedureContext pContext) throws OperationException {
			final ValueContextForUpdate vc = pContext.getContext(record
					.getAdaptationName());
			try {
				vc.setValue("Deleted",
						GOCWFPaths._C_GOC_WF_REPORTING._REQUEST_STATUS);

				pContext.doModifyContent(record, vc);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
