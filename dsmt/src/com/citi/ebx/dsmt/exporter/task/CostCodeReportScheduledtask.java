package com.citi.ebx.dsmt.exporter.task;

import java.sql.Connection;
import java.sql.SQLException;

import com.citi.ebx.dsmt.jdbc.connector.CostCodeReportUtil;
import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class CostCodeReportScheduledtask extends ScheduledTask{
	 protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	 protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	   
		
		
		protected String reportType;
		protected String exportFileDirectoryPath;
		
		
		
		@Override
		public void execute(ScheduledExecutionContext arg0)
				throws OperationException, ScheduledTaskInterruption {
			
			generateSQLReport();
		
		}
		
		public void generateSQLReport (){
			
			LOG.info("CostCodeReportScheduledtask :--> Report generation started !! ");
			Connection conn=null;
			
			try{
				
				CostCodeReportUtil costCodeReportUtil = new CostCodeReportUtil();
				
				conn = SQLReportConnectionUtils.getConnection();
				costCodeReportUtil.setReportType(reportType);
				LOG.info("CostCodeReportScheduledtask :--> report type "+reportType); 
				
				//flexCubeReportUtil.setReportType(DSMTConstants.FLEXCUBEREPORTS);
				costCodeReportUtil.generateCostCodeReports(conn, getExportFileDirectoryPath());
				LOG.info("CostCodeReportScheduledtask : Report  generated successfully !!!   ");	
				}
			catch (Exception e){
				
				LOG.error("Report generation terminated with Exception in CostCodeReportScheduledtask  -> " + e + ", message = " + e.getMessage());
			}
			finally{
				
				if(null != conn){
					try{
					conn.close();
					conn = null;
					}
					catch (SQLException SQ){
						LOG.error("Report generation terminated with Exception -> " + SQ + ", message = " + SQ.getMessage());
					}
				}
			}
			
			
		}
		
		
		
		



		public String getExportFileDirectoryPath() {
			return exportFileDirectoryPath;
		}

		public void setExportFileDirectoryPath(String exportFileDirectoryPath) {
			this.exportFileDirectoryPath = exportFileDirectoryPath;
		}

		/**
		 * @return the reportType
		 */
		public String getReportType() {
			return reportType;
		}

		/**
		 * @param reportType the reportType to set
		 */
		public void setReportType(String reportType) {
			this.reportType = reportType;
		}

		
		
		
}
