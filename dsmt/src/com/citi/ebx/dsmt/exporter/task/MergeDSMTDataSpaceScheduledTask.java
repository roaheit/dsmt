/**
 * 
 */
package com.citi.ebx.dsmt.exporter.task;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.goc.path.DSMT2WFPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;

/**
 * @author rk00242
 *
 */
public class MergeDSMTDataSpaceScheduledTask extends ScheduledTask {

	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	private String dataSpace;
	private AdaptationHome dsmtChildDataSpace;
	private Adaptation record;
	private Adaptation dataSetRef;
	private String dataSet;
	String tablePath;
	int requestID;
	String tableName;
	
	
	public String getDataSet() {
		return dataSet;
	}



	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}



	public String getDataSpace() {
		return dataSpace;
	}



	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}
	
	/* (non-Javadoc)
	 * @see com.orchestranetworks.scheduler.ScheduledTask#execute(com.orchestranetworks.scheduler.ScheduledExecutionContext)
	 */
	@Override
	public void execute(ScheduledExecutionContext context)
			throws OperationException, ScheduledTaskInterruption {
		Repository repo = context.getRepository();
		Session session = context.getSession();
		try {
			if (validateBD (repo)){
			closeChildDS(session, repo,context);
			}
		} catch (Exception e) {
			LOG.error("Exception while Closing DataSpace: " + e.getMessage());
		}

	}
		
		
	private void closeChildDS(Session session, Repository repo,ScheduledExecutionContext context)
				throws OperationException {
		
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
				"DSMT_WF", "DSMT_WF");
		final AdaptationTable targetTable = metadataDataSet
				.getTable(getPath("/root/C_APP_WF_REPORTING"));
		
		
		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
				HomeKey.forBranchName("DSMT_WF"));
		if (dataSpaceRef == null) {
			throw OperationException.createError("Data space " + dataSpace + " can't be found.");
		}
		dataSetRef = dataSpaceRef.findAdaptationOrNull(
				AdaptationName.forName("DSMT_WF"));
		if (dataSetRef == null) {
			throw OperationException.createError("Data set " + dataSetRef + " can't be found in data space " + dataSpace + ".");
		}
		HashMap<String, String> map = new HashMap<String, String>();

		final String pred = DSMT2WFPaths._C_APP_WF_REPORTING._REQUEST_STATUS.format() + "= '" + DSMTConstants.PENDING_STATUS + "' " ;
		
		RequestResult req = targetTable.createRequestResult(pred);
		
		
		for (int i =0 ; i < req.getSize(); i++){
			record = req.nextAdaptation();
			String childDataSpace = record.getString(DSMT2WFPaths._C_APP_WF_REPORTING._CHILD_DATASPACE_ID);
			tablePath = record.getString(DSMT2WFPaths._C_APP_WF_REPORTING._Table_Path);
			requestID = record.get_int(DSMT2WFPaths._C_APP_WF_REPORTING._REQUEST_ID);
			String requestor = record.getString(DSMT2WFPaths._C_APP_WF_REPORTING._REQ_BY);
			tableName = record.getString(DSMT2WFPaths._C_APP_WF_REPORTING._TABLE_NAME);
			dsmtChildDataSpace = context.getRepository().lookupHome(
					HomeKey.forBranchName(childDataSpace));
			AdaptationHome  dsmtDataSpace = context.getRepository().lookupHome(
					HomeKey.forBranchName(dataSpace));
			
			Adaptation dsmtDataSet = dsmtChildDataSpace.findAdaptationOrNull(
					AdaptationName.forName(dataSet));
			if (dsmtDataSet == null) {
				throw OperationException.createError("Data set " + dsmtDataSet + " can't be found in data space " + dataSpace + ".");
			}
		
			final AdaptationTable updatedTable = dsmtDataSet.getTable(Path.parse(tablePath));
			LOG.info("checking table path "+updatedTable);
			

			String subject="(Simplified DSMT2 Workflow) Workflow Request ID "+requestID+ " is Completed.";
			String message="Workflow ID "+requestID+" for "+tableName +" is Approved"
					+ " and changed data is merged.";

			map.put("TableName", tableName);
			map.put("RequestID", String.valueOf(requestID));
			map.put("Requestor",requestor);
			
		// TODO Auto-generated method stub
		ProgrammaticService.createForSession(context.getSession(),
				dsmtDataSpace).execute(new Procedure() {

					@Override
					public void execute(final ProcedureContext pContext) 
							throws Exception {
												
						pContext.setAllPrivileges(true);
						pContext.doMergeToParent(dsmtChildDataSpace);
						pContext.setAllPrivileges(false);
					}
				});
		
		ProgrammaticService.createForSession(context.getSession(),
				dataSpaceRef).execute(new Procedure() {

					@Override
					public void execute(final ProcedureContext pContext) 
							throws Exception {
												
						pContext.setAllPrivileges(true);
						updateRequestStatus(record, pContext);
						pContext.setAllPrivileges(false);
					}
				});
		
		
		DSMTNotificationService.notifyEmailID(requestor,
				subject,
				message, false, map);
		
		
		
	}
	
	}
	private Path getPath(final String fieldName) {

		return Path.parse(fieldName);
	}




		public boolean validateBD(Repository repo){
			boolean check= false;
			try {
				
				Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
						"DSMT_WF", "DSMT_WF");
				final AdaptationTable targetTable = metadataDataSet
						.getTable(getPath("/root/C_DSMT_WF_CAL"));
				
				Date now = Calendar.getInstance().getTime();
				Calendar cal = Calendar.getInstance();
				cal.setTime(now);
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				month = month + 1;
				int date = cal.get(Calendar.DATE);
				
				System.out.println("month === " +month);

				final String pred = DSMT2WFPaths._C_DSMT_WF_CAL._MONTH.format() + "= '"
						+ month + "' and " + DSMT2WFPaths._C_DSMT_WF_CAL._YEAR.format()
						+ "= '" + year + "'";

				RequestResult reqRes = targetTable.createRequestResult(pred);
				Adaptation record = reqRes.nextAdaptation();
				if (record != null) {

					final int endDate = record
							.get_int(DSMT2WFPaths._C_DSMT_WF_CAL._REG_END_DATE);
					final int startDate = record
							.get_int(DSMT2WFPaths._C_DSMT_WF_CAL._REG_START_DATE);

					if (date >= startDate && date <= endDate) {
						LOG.info("Current Date is between Request Start and end Date, Proceeding for Data Space Merge scheduled task ");
						check= true;
					} else {
						
					}

				}else{
					check= true;
				}
			} catch (OperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return check;
			
		}
		//changing request status to Completed after merging DS
		private void updateRequestStatus(final Adaptation record, final ProcedureContext pContext)
				throws OperationException {
			final ValueContextForUpdate vc = pContext.getContext(record.getAdaptationName());

			vc.setValue(DSMTConstants.COMPLETED, DSMT2WFPaths._C_APP_WF_REPORTING._REQUEST_STATUS);
			
			pContext.doModifyContent(record, vc);
		}


}
