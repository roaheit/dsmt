package com.citi.ebx.dsmt.exporter.task;

import com.citi.ebx.dsmt.exporter.projectgl.ProjectGLExporter;
import com.citi.ebx.dsmt.exporter.projectgl.ProjectGLTableExporterFactory;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.ps.exporter.task.DataSetExporterScheduledTask;

public class ProjectGLExportScheduledTask extends DataSetExporterScheduledTask {
	@Override
	protected DataSetExporter createExporter(TableExporterFactory tableExporterFactory,
			DataSetExportConfig config) {
		return new ProjectGLExporter(tableExporterFactory, config);
	}

	@Override
	protected TableExporterFactory createTableExporterFactory() {
		return new ProjectGLTableExporterFactory();
	}
}
