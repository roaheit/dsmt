package com.citi.ebx.dsmt.exporter.task;

import com.citi.ebx.dsmt.exporter.channel.ChannelExporter;
import com.citi.ebx.dsmt.exporter.channel.ChannelTableExporterFactory;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.ps.exporter.task.DataSetExporterScheduledTask;

public class ChannelExportScheduledTask extends DataSetExporterScheduledTask {
	@Override
	protected DataSetExporter createExporter(TableExporterFactory tableExporterFactory,
			DataSetExportConfig config) {
		return new ChannelExporter(tableExporterFactory, config);
	}

	@Override
	protected TableExporterFactory createTableExporterFactory() {
		return new ChannelTableExporterFactory();
	}
}

	
	


