package com.citi.ebx.dsmt.exporter.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.task.GenerateSQLExportFileTask;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;

public class ACCTMTXSQLExportUtil {


	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	
	private String reportType = "SQL";
	
	final String env =  propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);

    protected String sql ;
  
    protected ArrayList<String> reportNames ;
    protected ArrayList<String> sqlNames  ;
    protected Map<String, Integer> readmeMap;
	
	
    public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
    
   
    
    public Map<String, Integer>  generateSQLReport (Map<String, String> sqlExportMap) throws SQLException
	{
    	
    	LOG.info("ACCTMTXSQLExportUtil: ..................start");
    	
		try {
			readmeMap= new java.util.HashMap<String, Integer>();
			
		
				LOG.info("sqlFilePath-->" + sqlExportMap.get(GenerateSQLExportFileTask.SQL_FILE_PATH));
				sql = getInputQuery(sqlExportMap.get(GenerateSQLExportFileTask.SQL_FILE_PATH));
				//exportFileDirectoryPath= exportFileDirectory;
				readmeMap = selectRecordsFromTable(sqlExportMap); 
				
			
		} catch (SQLException e) {
			LOG.error ("Error inside method  generateSQLReport  ->" + e + " , message " + e.getMessage()); 
		}
		
		LOG.info("ACCTMTXSQLExportUtil: ..................end");

		return readmeMap;
	}
    
    /**
   	 * Helper method to get the O/P from the file table
   	 * @param  
   	 * @throws SQLException
   	 */   
    
    
    
    
	public Map<String, Integer> selectRecordsFromTable(Map<String, String> sqlExportMap) throws SQLException {

		final Connection dbConnection = SQLReportConnectionUtils.getConnection();
		PreparedStatement preparedStatement = null;

		File file = null;
		FileOutputStream fos = null;

		try {
			// Creating prepare statement.
			
			preparedStatement = dbConnection.prepareStatement(sql);
			String LVID = sqlExportMap.get("lvid");
			
			preparedStatement.setString(1, LVID);
	
		
			long reportStartTime =  System.currentTimeMillis();
			ResultSet rs = preparedStatement.executeQuery();
			ResultSetMetaData rsMetaData = rs.getMetaData();
			int columncount = rsMetaData.getColumnCount();
			// to get record counts in report
			int recordcount = 0;

			String exportFileDirectoryPath = sqlExportMap.get(GenerateSQLExportFileTask.EXPORT_FILE_DIRECTORY);
			String fileNameToBeGenerated =  sqlExportMap.get(GenerateSQLExportFileTask.FILE_NAME_TO_BE_GENERATED);
			
			if (!exportFileDirectoryPath.endsWith("/")) {
				exportFileDirectoryPath = exportFileDirectoryPath + "/";
			}

			file = new File(exportFileDirectoryPath + fileNameToBeGenerated);

			file.setReadable(true, false);
			file.setWritable(true, true);

			fos = new FileOutputStream(file);
			
			final String header = sqlExportMap.get("headerRow");
			while (rs.next()) {

				StringBuffer reportfile = new StringBuffer();
			
				
				if(null != header && recordcount==0){
					reportfile.append(header);
					reportfile.append(DSMTConstants.NEXT_LINE);
				}
				
				recordcount++;
				for (int i = 0; i < columncount; i++) {
					
					
					String data = rs.getString(i + 1);
					if (DSMTUtils.isStringNotEmptyOrNull(data)) {
						reportfile.append(data);
					} else {
						reportfile.append("");
					}

				}

				reportfile.append(DSMTConstants.NEXT_LINE);
				byte[] contentInBytes = reportfile.toString().getBytes();
				fos.write(contentInBytes);
				
				
			}
			
			
			
			long reportEndTime =  System.currentTimeMillis();
			
			readmeMap.put(fileNameToBeGenerated, recordcount);
			LOG.info("Total record count in " + fileNameToBeGenerated + " is "
					+ recordcount + ". Report fetched and generated in " + ((reportEndTime-reportStartTime)/1000) + " seconds");
		} catch (SQLException e) {
			LOG.error(" Error  while excuting the query for report  >>>>"
					+ sqlExportMap.get(GenerateSQLExportFileTask.FILE_NAME_TO_BE_GENERATED) + ". Exception ->" + e + " , message "
					+ e.getMessage());

		} catch (FileNotFoundException e) {

			LOG.error(" Error  while creating  >>>>" + sqlExportMap.get(GenerateSQLExportFileTask.FILE_NAME_TO_BE_GENERATED)
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} catch (IOException e) {

			LOG.error("  Error  while writing  >>>>" + sqlExportMap.get(GenerateSQLExportFileTask.FILE_NAME_TO_BE_GENERATED)
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} finally {
			
			if (preparedStatement != null) {
				preparedStatement.close();
				LOG.info("preparedStatement closed successfully");
			}
			try {
				if (fos != null) {
					fos.close();
					LOG.info("file output stream closed successfully");
				}
				
			} catch (IOException e) {
				LOG.error("  Error  while closing  >>>>" + sqlExportMap.get(GenerateSQLExportFileTask.FILE_NAME_TO_BE_GENERATED)+ ". Exception ->" + e + " , message " + e.getMessage());
			}
			sql = DSMTConstants.BLANK_STRING;
			
			
				if (dbConnection != null) {
					dbConnection.close();
					LOG.info("dbConnection closed successfully");
					
				}
				
			
		}
		
		return readmeMap;
	}  
    
    
	
	
    /**
   	 * Helper method to read Sql from file
   	 * @param  sqlFileDirectory , sqlFile
   	 * @throws 
   	 */       
    
	public String getInputQuery(String sqlFile) {
		LOG.info("getInputQuery  " + sqlFile);
		StringBuffer inputquery = new StringBuffer();
		BufferedReader br = null;
		String line = null;
		try {
			br = new BufferedReader(new FileReader(sqlFile));
			while ((line = br.readLine()) != null) {

				inputquery.append(line);
				inputquery.append(DSMTConstants.NEXT_LINE);

			}

		} catch (FileNotFoundException e) {

			LOG.error("Unable to find SQl file  >>>>" + sqlFile
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} catch (IOException e) {

			LOG.error("Unable to read SQl file " + sqlFile + ". Exception ->"
					+ e + " , message " + e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return inputquery.toString();
	}
    
    
   /**
   	 * Helper method to get SQl file name and corresponding Report name
   	 * @param  
   	 * @throws 
   	 */        
    

	



			
	 public String getReportType() {
			return reportType;
		}



	
		
}
