package com.citi.ebx.dsmt.exporter.pmfprod;

import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporter;
/**
 * A factory for table exporters that handles DSMT-specific implementations
 */
public class DSMTTableExporterFactory extends DefaultTableExporterFactory {
	/**
	 * Overridden in order to handle prod exports
	 */
	@Override
	public TableExporter getTableExporter(DataSetExportTableConfig tableConfig, AdaptationTable table) {
		if (tableConfig instanceof PmfProdTableConfig) {
			PmfProdTableConfig pmfProdTableConfig = (PmfProdTableConfig) tableConfig;
			switch (pmfProdTableConfig.getPmfProdExportType()) {
			// Currently there is just the one type but this is where we'd handle other export types
			// for prod type table
			case FLAT:
				return new PmfProdFlatTableExporter();
			}
		}
		return super.getTableExporter(tableConfig, table);
	}
}
