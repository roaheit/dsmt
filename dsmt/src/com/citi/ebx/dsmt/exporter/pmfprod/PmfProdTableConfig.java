package com.citi.ebx.dsmt.exporter.pmfprod;

import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;

/**
 * A table config for use with the ProdType table
 */
public class PmfProdTableConfig extends DataSetExportTableConfig {
	public enum PmfProdExportType {
		// Currently only one type but there will be more
		FLAT ,FLAT1
	}
	
	private static final int DEFAULT_NUM_OF_LEVELS = 16;
	private static final String DEFAULT_TABLE_EXPORT_NAME = "PMF_PRODUCT";
	
	protected PmfProdExportType pmfProdExportType;
	protected int numOfLevels = DEFAULT_NUM_OF_LEVELS;
	protected String tableExportName = DEFAULT_TABLE_EXPORT_NAME;

	/**
	 * Get the pmfProd type export type
	 * 
	 * @return the pmfProd type export type
	 */
	public PmfProdExportType getPmfProdExportType() {
		return pmfProdExportType;
	}

	/**
	 * Set the pmfProd type export type
	 * 
	 * @param PmfProdExportType the PmfProd type export type
	 */
	public void setPmfProdExportType(PmfProdExportType pmfProdExportType) {
		this.pmfProdExportType = pmfProdExportType;
	}
	
	/**
	 * Get the number of levels to output
	 * 
	 * @return the number of levels to output
	 */
	public int getNumOfLevels() {
		return numOfLevels;
	}

	/**
	 * Set the number of levels to output
	 * 
	 * @param numOfLevels the number of levels to output
	 */
	public void setNumOfLevels(int numOfLevels) {
		this.numOfLevels = numOfLevels;
	}

	/**
	 * Get the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @return the name of the table
	 */
	public String getTableExportName() {
		return tableExportName;
	}

	/**
	 * Set the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @param tableExportName the name of the table
	 */
	public void setTableExportName(String tableExportName) {
		this.tableExportName = tableExportName;
	}
}
