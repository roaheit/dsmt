package com.citi.ebx.dsmt.exporter.pmfprod;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.citi.ebx.dsmt.util.DSMTExportBean;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.Request;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.TableExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

/**
 * An exporter for the PMF Product table that outputs all leaves of the table along with a flattened
 * representation of their complete path to the root. There is a specific format this conforms
 * to. Refer to a sample output file.
 */
public class PmfProdFlatTableExporter1 implements TableExporter {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private static final Path PARENT_FK_PATH = Path.parse("./Parent_FK");
    private static final Path PMF_PRODUCT_PATH = Path.parse("./C_DSMT_PRODUCT");
    private static final Path PMF_PRODUCT_DESC = Path.parse("./C_DSMT_DESCR120");
    private static final Path PMF_PRODUCT_EFF_STAT = Path.parse("./EFF_STATUS");
	
	/**
	 * This stores all the records from the table for faster lookup.
	 * Key = the primary key of the record, Value = the record itself.
	 */
	protected Map<PrimaryKey, Adaptation> allRecordsMap;
	/**
	 * This is a list of only the leaf records of the table, each one containing
	 * its primary key and level within the hierarchy (1 being the root).
	 */
	protected List<CachedLeafInfo> leaves;
	Calendar cal ;
	

	@Override
	public void exportTable(Session session, AdaptationTable table,DSMTExportBean exportBean,
			OutputStream out, DataSetExportTableConfig tableConfig,
			TableExporterFactory tableExporterFactory)
			throws IOException, OperationException {
		LOG.info("PmfProdFlatTableExporter1: exportTable");
		LOG.info("PmfProdFlatTableExporter1: table = " + table.getTablePath().format());
		PmfProdTableConfig pmfProdTableConfig = (PmfProdTableConfig) tableConfig;
		initAllRecordsMap(table, pmfProdTableConfig.getFilter());
		initLeaves();
		
			int recordCount=leaves.size();
		
		exportBean.getFileNamesAndCount().put(tableConfig.getFileName(), recordCount);
		
		final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
		try {
			// Loop through each leaf record
			for (CachedLeafInfo leaf: leaves) {
				final String rowStr = createLeafRow(leaf, pmfProdTableConfig);
				writer.write(rowStr);
				writer.newLine();
			}
		
		} finally {
			writer.close();
		}
	}
	
	/**
	 * Create the leaf row
	 * 
	 * @param leaf the leaf cached info
	 * @param tableConfig the table config
	 * @return the leaf row
	 * @throws OperationException if an error occurred creating the row
	 */
	protected String createLeafRow(CachedLeafInfo leaf, PmfProdTableConfig tableConfig)
			throws OperationException {
		if (LOG.isDebug()) {
			LOG.debug("PmfProdFlatTableExporter: createLeafRow");
			LOG.debug("PmfProdFlatTableExporter: leaf.pk = " + leaf.pk.format());
			LOG.debug("PmfProdFlatTableExporter: leaf.level = " + leaf.level);
		}
		final Adaptation record = allRecordsMap.get(leaf.pk);
		final int level = leaf.level;
		if (level > tableConfig.getNumOfLevels()) {
			throw OperationException.createError("Record " + leaf.pk.format() + " has level of " + level
					+ " which is greater than max level of " + tableConfig.getNumOfLevels());
		}
		
		// strBuff will contain everything before the root
		final StringBuffer strBuff = new StringBuffer();
		final char separator = tableConfig.getSeparator();
		String pmfProdCode = record.getString(PMF_PRODUCT_PATH);
		strBuff.append(pmfProdCode);
		strBuff.append(separator);
		String pmfProdDesc = record.getString(PMF_PRODUCT_DESC);
		strBuff.append(pmfProdDesc);
		strBuff.append(separator);
		String effectiveStatus=record.getString(PMF_PRODUCT_EFF_STAT);
		strBuff.append(effectiveStatus);
		strBuff.append(separator);
		final StringBuffer strBuff2 = new StringBuffer();
		// Now we pad out strBuff2 at the end with blanks, if it's less levels than what was specified
		for (int i = level; i < tableConfig.getNumOfLevels(); i++) {
			strBuff2.append(separator);
			strBuff2.append("");
			strBuff2.append(separator);
			if (i < tableConfig.getNumOfLevels() - 1) {
				strBuff2.append("");
				}
			
		}
		
		// Now we loop up through the hierarchy towards the root, prepending each parent to strBuff2
		// (so after we're done, strBuff2 will begin with the root node).
		for (String pk = record.getString(PARENT_FK_PATH);
				pk != null && allRecordsMap.containsKey(PrimaryKey.parseString(pk));) {
			final PrimaryKey parentPK = PrimaryKey.parseString(pk);
			final Adaptation parent = allRecordsMap.get(parentPK);
			
			strBuff2.insert(0, separator);
			pmfProdDesc = parent.getString(PMF_PRODUCT_DESC);
			strBuff2.insert(0, pmfProdDesc);
			strBuff2.insert(0, separator);
			pmfProdCode = parent.getString(PMF_PRODUCT_PATH);
			strBuff2.insert(0, pmfProdCode);
			pk = parent.getString(PARENT_FK_PATH);
		}
		//Removing an extra separator
		strBuff2.deleteCharAt(strBuff2.length()-1);
		// Now return the 2 buffers concatenated
		return strBuff.toString() + strBuff2.toString();
	}
	
	private int initAllRecordsMap(final AdaptationTable table, final AdaptationFilter filter) {
		if (LOG.isDebug()) {
			LOG.debug("PmfProdFlatTableExporter: initAllRecordsMap");
			LOG.debug("PmfProdFlatTableExporter: table = " + table.getTablePath().format());
		}
		allRecordsMap = new HashMap<PrimaryKey, Adaptation>();
		// If you specify no predicate, it will request every record in the table
		// (that meets any filter you set)
		final Request request = table.createRequest();
		if (filter != null) {
			request.setSpecificFilter(filter);
		}
		final RequestResult reqRes = request.execute();
		int recordCount= reqRes.getSize();
		try {
			// Loop through all the results and put each into the map
			for (Adaptation adaptation; (adaptation = reqRes.nextAdaptation()) != null;) {
				allRecordsMap.put(adaptation.getOccurrencePrimaryKey(), adaptation);
			}
		} finally {
			reqRes.close();
		}
		return recordCount;
	}
	
	private void initLeaves() {
		LOG.debug("PmfProdFlatTableExporter1: initLeaves");
		leaves = new ArrayList<CachedLeafInfo>();
		final Set<PrimaryKey> allPKs = allRecordsMap.keySet();
		// Loop over each primary key and if it's a leaf,
		// add a new CachedLeafInfo to the leaves list
		for (PrimaryKey pk: allPKs) {
			final Adaptation adaptation = allRecordsMap.get(pk);
			if (isLeaf(adaptation)) {
				final int level = getLevel(adaptation);
				final CachedLeafInfo cachedLeafInfo = new CachedLeafInfo();
				cachedLeafInfo.pk = pk;
				cachedLeafInfo.level = level;
				leaves.add(cachedLeafInfo);
			}
		}
		// Sort the list of leaves, using the CachedLeafInfo's comparison method.
		Collections.sort(leaves);
	}
	
	// Determines if a record is a leaf (no other record has it as its parent)
	private boolean isLeaf(final Adaptation adaptation) {
		/**
		final String pk = adaptation.getOccurrencePrimaryKey().format();
		final Set<PrimaryKey> allPKs = allRecordsMap.keySet();
		// Loop through all primary keys
		
		for (PrimaryKey otherPK: allPKs) {
			final Adaptation otherAdaptation = allRecordsMap.get(otherPK);
			final String parentFK = otherAdaptation.getString(PARENT_FK_PATH);
			// If this other record has the specified record as its parent then
			// the specified record isn't a leaf so return false
			if (parentFK != null && pk.equals(parentFK)) {
				return false;
			}
		}
		**/
		String leafvalue = adaptation.getString(DSMT2HierarchyPaths._GLOBAL_STD_PMF_STD_F_PMF_PROD_STD_Product_New._PL_FLAG);
		if(leafvalue==null||leafvalue.equalsIgnoreCase("P"))
		return false;
		// We looped through all the records and didn't find any that had
		// the specified record as its parent, so it's a leaf.
		return true;
	}
	
	// Get the level in the hierarchy for a record (with root being level 1)
	private int getLevel(final Adaptation adaptation) {
		int level = 1;
		// Start with the parent of the specified record and loop up through the hierarchy
		// until there is no parent
		/**
		for (String pk = adaptation.getString(PARENT_FK_PATH);
				pk != null && allRecordsMap.containsKey(PrimaryKey.parseString(pk));) {
			level++;
			final PrimaryKey parentPK = PrimaryKey.parseString(pk);
			final Adaptation parent = allRecordsMap.get(parentPK);
			pk = parent.getString(PARENT_FK_PATH);
		}
		**/
		level= adaptation.get_int(DSMT2HierarchyPaths._GLOBAL_STD_PMF_STD_F_PMF_PROD_STD_Product_New._LEVEL);
		return level;
	}
	
	/**
	 * A class that caches info about the leaf nodes
	 */
	protected class CachedLeafInfo implements Comparable<CachedLeafInfo> {
		/**
		 * The primary key of the leaf record
		 */
		protected PrimaryKey pk;
		/**
		 * The level of the leaf record in the hierarchy (with root level being 1)
		 */
		protected int level;
		
		/**
		 * Overridden so that they can be ordered according to primary key
		 */
		@Override
		public int compareTo(CachedLeafInfo obj) {
			return pk.format().compareTo(obj.pk.format());
		}
	}
	
}
