package com.citi.ebx.dsmt.exporter.pmfprod;

import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporter;

/**
 * A factory for table exporters that handles pmfProdSeg type table exports
 */
public class PmfProdTableExporterFactory extends DefaultTableExporterFactory {
	/**
	 * Overridden in order to handle pmfProd type exports
	 */
	@Override
	public TableExporter getTableExporter(DataSetExportTableConfig tableConfig, AdaptationTable table) {
		if (tableConfig instanceof PmfProdTableConfig) {
			PmfProdTableConfig pmfProdTableConfig = (PmfProdTableConfig) tableConfig;
			switch (pmfProdTableConfig.getPmfProdExportType()) {
			// Currently there is just the one type but this is where we'd handle other export types
			// for pmfProd type table
			case FLAT:
				LOG.info("PmfProdFlatTableExporter : " );
				return new PmfProdFlatTableExporter();
			case FLAT1:
				LOG.info("PmfProdFlatTableExporter1 : " );
				return new PmfProdFlatTableExporter1();
			}
		}
		return super.getTableExporter(tableConfig, table);
	}
}
