package com.citi.ebx.dsmt.exporter.custsegs;

import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;

public class CustSegsTableConfig  extends DataSetExportTableConfig{
	public enum CustSegExportType {
		// Currently only one type but there will be more
		FLAT
	}
	
	private static final int DEFAULT_NUM_OF_LEVELS = 16;
	private static final String DEFAULT_TABLE_EXPORT_NAME = "CUSTOMER_SEG";
	
	protected CustSegExportType custSegExportType;
	protected int numOfLevels = DEFAULT_NUM_OF_LEVELS;
	protected String tableExportName = DEFAULT_TABLE_EXPORT_NAME;

	/**
	 * Get the CustSeg type export type
	 * 
	 * @return the CustSeg type export type
	 */
	public CustSegExportType getCustSegExportType() {
		return custSegExportType;
	}

	/**
	 * Set the CustSeg type export type
	 * 
	 * @param CustSegExportType the CustSeg type export type
	 */
	public void setCustSegExportType(CustSegExportType custSegExportType) {
		this.custSegExportType = custSegExportType;
	}
	
	/**
	 * Get the number of levels to output
	 * 
	 * @return the number of levels to output
	 */
	public int getNumOfLevels() {
		return numOfLevels;
	}

	/**
	 * Set the number of levels to output
	 * 
	 * @param numOfLevels the number of levels to output
	 */
	public void setNumOfLevels(int numOfLevels) {
		this.numOfLevels = numOfLevels;
	}

	/**
	 * Get the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @return the name of the table
	 */
	public String getTableExportName() {
		return tableExportName;
	}

	/**
	 * Set the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @param tableExportName the name of the table
	 */
	public void setTableExportName(String tableExportName) {
		this.tableExportName = tableExportName;
	}
}
