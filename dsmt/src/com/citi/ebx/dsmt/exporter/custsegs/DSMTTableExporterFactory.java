package com.citi.ebx.dsmt.exporter.custsegs;

import com.citi.ebx.dsmt.exporter.custseg.CustSegFlatTableExporter;
import com.citi.ebx.dsmt.exporter.custseg.CustSegTableConfig;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporter;

public class DSMTTableExporterFactory extends DefaultTableExporterFactory{
	/**
	 * Overridden in order to handle prod type exports
	 */
	@Override
	public TableExporter getTableExporter(DataSetExportTableConfig tableConfig, AdaptationTable table) {
		if (tableConfig instanceof CustSegTableConfig) {
			CustSegTableConfig custSegTableConfig = (CustSegTableConfig) tableConfig;
			switch (custSegTableConfig.getCustSegExportType()) {
			// Currently there is just the one type but this is where we'd handle other export types
			// for prod type table
			case FLAT:
				return new CustSegFlatTableExporter();
			}
		}
		return super.getTableExporter(tableConfig, table);
	}
}
