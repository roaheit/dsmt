package com.citi.ebx.dsmt.exporter.custsegs;

import com.citi.ebx.dsmt.exporter.custsegs.CustSegsFlatTableExporter;
import com.citi.ebx.dsmt.exporter.custsegs.CustSegsTableConfig;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporter;

public class CustSegsTableExporterFactory extends DefaultTableExporterFactory{
	/**
	 * Overridden in order to handle custSeg type exports
	 */
	@Override
	public TableExporter getTableExporter(DataSetExportTableConfig tableConfig, AdaptationTable table) {
		if (tableConfig instanceof CustSegsTableConfig) {
			CustSegsTableConfig custSegTableConfig = (CustSegsTableConfig) tableConfig;
			switch (custSegTableConfig.getCustSegExportType()) {
			// Currently there is just the one type but this is where we'd handle other export types
			// for custSeg type table
			case FLAT:
				LOG.info("CustSegFlatTableExporter : " );
				return new CustSegsFlatTableExporter();
			}
		}
		return super.getTableExporter(tableConfig, table);
	}
}
