package com.citi.ebx.dsmt.exporter;

import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;

/**
 * A table config for use with the ProdType table
 */
public class ProdTypeTableConfig extends DataSetExportTableConfig {
	public enum ProdTypeExportType {
		// Currently only one type but there will be more
		FLAT
	}
	
	private static final int DEFAULT_NUM_OF_LEVELS = 10;
	private static final String DEFAULT_TABLE_EXPORT_NAME = "PRODUCT_TYPE";
	
	protected ProdTypeExportType prodTypeExportType;
	protected int numOfLevels = DEFAULT_NUM_OF_LEVELS;
	protected String tableExportName = DEFAULT_TABLE_EXPORT_NAME;

	/**
	 * Get the prod type export type
	 * 
	 * @return the prod type export type
	 */
	public ProdTypeExportType getProdTypeExportType() {
		return prodTypeExportType;
	}

	/**
	 * Set the prod type export type
	 * 
	 * @param prodTypeExportType the prod type export type
	 */
	public void setProdTypeExportType(ProdTypeExportType prodTypeExportType) {
		this.prodTypeExportType = prodTypeExportType;
	}
	
	/**
	 * Get the number of levels to output
	 * 
	 * @return the number of levels to output
	 */
	public int getNumOfLevels() {
		return numOfLevels;
	}

	/**
	 * Set the number of levels to output
	 * 
	 * @param numOfLevels the number of levels to output
	 */
	public void setNumOfLevels(int numOfLevels) {
		this.numOfLevels = numOfLevels;
	}

	/**
	 * Get the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @return the name of the table
	 */
	public String getTableExportName() {
		return tableExportName;
	}

	/**
	 * Set the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @param tableExportName the name of the table
	 */
	public void setTableExportName(String tableExportName) {
		this.tableExportName = tableExportName;
	}
}
