package com.citi.ebx.dsmt.exporter.projectgl;

import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;

/**
 * A table config for use with the PmfProj table
 */
public class ProjectGLTableConfig extends DataSetExportTableConfig {
	public enum ProjGLExportType {
		// Currently only one type but there will be more
		FLAT 
	}
	
	
	private static final String DEFAULT_TABLE_EXPORT_NAME = "PROJECT";
	
	protected ProjGLExportType projGLExportType;
	protected String tableExportName = DEFAULT_TABLE_EXPORT_NAME;

	/**
	 * Get the PmfProj type export type
	 * 
	 * @return the PmfProj type export type
	 */
	public ProjGLExportType getprojGLExportType() {
		return projGLExportType;
	}

	/**
	 * Set the PmfProj type export type
	 * 
	 * @param ProdGLExportType the PmfProj type export type
	 */
	public void setPmfProjExportType(ProjGLExportType pmfProjExportType) {
		this.projGLExportType = pmfProjExportType;
	}
	


	/**
	 * Get the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @return the name of the table
	 */
	public String getTableExportName() {
		return tableExportName;
	}

	/**
	 * Set the name of the table. This is how it should show in the export, which may not
	 * correspond to its ID or label in EBX.
	 * 
	 * @param tableExportName the name of the table
	 */
	public void setTableExportName(String tableExportName) {
		this.tableExportName = tableExportName;
	}
}
