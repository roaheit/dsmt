package com.citi.ebx.dsmt.exporter.projectgl;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTExportBean;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationFilter;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.Request;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.TableExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

/**
 * An exporter for the PMF Project table that outputs all leaves of the table 
 * to. Refer to a sample output file.
 */
public class ProjectGLTableExporter implements TableExporter {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
		private static final Path NODE_FLAG = Path.parse("./nodeFlag");
    private static final Path SETID = Path.parse("./SETID");
    private static final Path PMF_PROJECT_PATH = Path.parse("./C_DSMT_PROJECT");
    private static final Path EFFDT = Path.parse("./EFFDT");
    private static final Path EFF_STATUS = Path.parse("./EFF_STATUS");
    private static final Path PROJECT_TYPE = Path.parse("./PROJECT_TYPE");
    private static final Path DESCR = Path.parse("./DESCR"); 
    private static final Path PROJECT_MANAGER = Path.parse("./PROJECT_MANAGER");
    private static final Path START_DT = Path.parse("./START_DT");
    private static final Path END_DT = Path.parse("./END_DT");
    private static final Path CREATED_OPERID = Path.parse("./CREATED_OPERID");
    private static final Path ORIG_DTTM = Path.parse("./ORIG_DTTM");
    private static final Path LASTUPDOPRID = Path.parse("./LASTUPDOPRID");
    private static final Path CHNG_DTTM = Path.parse("./CHNG_DTTM");
    private static final String HEADER_DATE_PATTERN = "MM/dd/yyyy HH:mm:ss";
    
	
	/**
	 * This stores all the records from the table for faster lookup.
	 * Key = the primary key of the record, Value = the record itself.
	 */
	protected Map<PrimaryKey, Adaptation> allRecordsMap;
	/**
	 * This is a list of only the leaf records of the table, each one containing
	 * its primary key and level within the hierarchy (1 being the root).
	 */
	protected List<String> parents  = new ArrayList<String>();;

	@Override
	public void exportTable(Session session, AdaptationTable table,DSMTExportBean exportBean,
			OutputStream out, DataSetExportTableConfig tableConfig,
			TableExporterFactory tableExporterFactory)
			throws IOException, OperationException {
		LOG.info("ProjectGLTableExporter: exportTable");
		LOG.info("ProjectGLTableExporter: table = " + table.getTablePath().format());
		ProjectGLTableConfig pojectGLTableConfig = (ProjectGLTableConfig) tableConfig;
		initAllRecordsMap(table, pojectGLTableConfig.getFilter());
		
		//initParent();
		
		final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
		try {
			final String header = createHeader(pojectGLTableConfig);
			writer.write(header);
			writer.newLine();
			int fileSize=0;
			// Loop through each  record
			for (PrimaryKey  key : allRecordsMap.keySet()) {
					Adaptation tempRecord = allRecordsMap.get(key);
					 String project = tempRecord.getString(PMF_PROJECT_PATH);
					 if(!parents.contains(project)){
							
							final String rowStr = crerateFile(tempRecord, pojectGLTableConfig);
							writer.write(rowStr);
							writer.newLine();
							fileSize++;
						 
					 }
			}
			final String footer = createFooter(pojectGLTableConfig, fileSize);
			writer.write(footer);
			writer.newLine();
			exportBean.getFileNamesAndCount().put(tableConfig.getFileName(), fileSize);
		
		} finally {
			writer.close();
		}
	}
	
	
	protected String crerateFile(Adaptation record ,DataSetExportTableConfig tableConfig) {
		// TODO Auto-generated method stub
		final StringBuffer strBuff = new StringBuffer();
		final char separator = tableConfig.getSeparator();
		strBuff.append("L");
		strBuff.append(separator);
		strBuff.append(record.getString(SETID));
		strBuff.append(separator);
		strBuff.append(record.getString(PMF_PROJECT_PATH));
		strBuff.append(separator);
		strBuff.append(formateDate(record.getDate(EFFDT)));
		strBuff.append(separator);
		strBuff.append(record.getString(EFF_STATUS));
		strBuff.append(separator);
		strBuff.append(record.getString(PROJECT_TYPE));
		strBuff.append(separator);
		if(record.getString(DESCR)!=null){
		strBuff.append(record.getString(DESCR));
		}else{
			strBuff.append(DSMTConstants.SPACE);;
		}
				
		strBuff.append(separator);
		if(record.getString(PROJECT_MANAGER)!=null){
			strBuff.append(record.getString(PROJECT_MANAGER));
		}else{
			strBuff.append(DSMTConstants.SPACE);
		}		
		strBuff.append(separator);
		strBuff.append(formateDate(record.getDate(START_DT)));
		strBuff.append(separator);
		strBuff.append(formateDate(record.getDate(END_DT)));
		strBuff.append(separator);
		
		if(record.getString(CREATED_OPERID)!=null){
		strBuff.append(record.getString(CREATED_OPERID));
		}else{
			strBuff.append(DSMTConstants.SPACE);
		}
		
		strBuff.append(separator);
		strBuff.append(formateDateWithTime(record.getDate(ORIG_DTTM)));
		strBuff.append(separator);
		if(record.getString(LASTUPDOPRID)!=null){
		strBuff.append(record.getString(LASTUPDOPRID));
		}else{
			strBuff.append(DSMTConstants.SPACE);
		}
		strBuff.append(separator);
		strBuff.append(formateDateWithTime(record.getDate(CHNG_DTTM)));
		
		
		return strBuff.toString() ;
	}

	private int initAllRecordsMap(final AdaptationTable table, final AdaptationFilter filter) {
		if (LOG.isDebug()) {
			LOG.debug("ProjectGLTableExporter: initAllRecordsMap");
					}
		allRecordsMap = new LinkedHashMap<PrimaryKey, Adaptation>();
		// If you specify no predicate, it will request every record in the table
		// (that meets any filter you set)
		final Request request = table.createRequest();
		if (filter != null) {
			request.setSpecificFilter(filter);
		}
		final RequestResult reqRes = request.execute();
		int recordCount=reqRes.getSize();
		try {
			// Loop through all the results and put each into the map
			for (Adaptation adaptation; (adaptation = reqRes.nextAdaptation()) != null;) {
				initParent(adaptation);
				allRecordsMap.put(adaptation.getOccurrencePrimaryKey(), adaptation);
			}
		} finally {
			reqRes.close();
		}
		return recordCount;
	}
	
	private void initParent(Adaptation currentRecord) {
		LOG.debug("ProjectGLTableExporter: initParent");
		
		// Loop over each primary key and if it's a Parent ,
		// add a new CachedparentInfo to the parents list
		if (isParent(currentRecord)) {
				final String project = currentRecord.getString(PMF_PROJECT_PATH);
				parents.add(project);
			}
		
	}
	
	// Determines if a record is a leaf (no other record has it as its parent)
	private boolean isParent(final Adaptation adaptation) {
		
		// Loop through all primary keys
			final String nodeFlag = adaptation.getString(NODE_FLAG);
			// If this other record have node flag as L means its leaf then
			// the specified record is a leaf so return false
			if (nodeFlag.equalsIgnoreCase("L")) {
				return false;
			}
		
		// We looped through all the records and didn't find any that had
		// the specified record as its parent, so it's a leaf.
		return true;
	}
	
	protected String formateDateWithTime(Date inputdate){
		String date ="";
		if(inputdate!=null){
		
			SimpleDateFormat sfd= new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss.SSSSSS");
			date = sfd.format(inputdate);
			
		}
		return  date;
	}
	
	
	protected String formateDate(Date inputdate){
		String date ="";
		if(inputdate!=null){
			
			SimpleDateFormat sfd= new SimpleDateFormat("yyyy-MM-dd");
			date = sfd.format(inputdate);
			
		}
		return  date;
	}
	
	/**
	 * Create the header row
	 * 
	 * @param tableConfig the table config
	 * @return the header row
	 */
	protected String createHeader(ProjectGLTableConfig tableConfig) {
		LOG.debug("ProjectGlTableExporter: createHeader");
		final StringBuffer strBuff = new StringBuffer();
		strBuff.append("H");
		final char separator = tableConfig.getSeparator();
		strBuff.append(separator);
		strBuff.append(tableConfig.getTableExportName());
		strBuff.append(separator);
		final SimpleDateFormat dateFormat = new SimpleDateFormat(HEADER_DATE_PATTERN);
		strBuff.append(dateFormat.format(new Date()));
		strBuff.append(separator);
		strBuff.append(tableConfig.getFileName().replace("_2", ""));
		return strBuff.toString();
	}
	
	/**
	 * Create the footer
	 * 
	 * @param tableConfig the table config
	 * @return the footer
	 */
	protected String createFooter(DataSetExportTableConfig tableConfig, int size) {
		LOG.debug("ProjectGlTableExporter: createFooter");
		final StringBuffer strBuff = new StringBuffer();
		strBuff.append("T");
		strBuff.append(tableConfig.getSeparator());
		strBuff.append(size);
		return strBuff.toString();
	}
	
	
	
}
