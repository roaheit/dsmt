package com.citi.ebx.dsmt.exporter.projectgl;

import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.ps.exporter.DataSetExportTableConfig;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporter;

/**
 * A factory for table exporters that handles PmfProjSeg type table exports
 */
public class ProjectGLTableExporterFactory extends DefaultTableExporterFactory {
	/**
	 * Overridden in order to handle PmfProj type exports
	 */
	@Override
	public TableExporter getTableExporter(DataSetExportTableConfig tableConfig, AdaptationTable table) {
		if (tableConfig instanceof ProjectGLTableConfig) {
			ProjectGLTableConfig pmfProdTableConfig = (ProjectGLTableConfig) tableConfig;
			switch (pmfProdTableConfig.getprojGLExportType()) {
			// Currently there is just the one type but this is where we'd handle other export types
			// for PmfProj type table
			case FLAT:
				LOG.info("PmfProdFlatTableExporter : " );
				return new ProjectGLTableExporter();
			
			}
		}
		return super.getTableExporter(tableConfig, table);
	}
}
