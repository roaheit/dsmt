package com.citi.ebx.dsmt.exporter.readme;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Encapsulates configuration details for the readme exporter
 */
public class ReadmeConfig {
	private File file;
	private String dataSet;
	private Date dateTime;
	private List<ReadmeEntry> entries;
	
	/**
	 * Get the file to export to
	 * 
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * Set the file to export to
	 * 
	 * @param file the file
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * Get the ID of the data set to generate the readme for
	 * 
	 * @return the ID of the data set
	 */
	public String getDataSet() {
		return dataSet;
	}
	
	/**
	 * Set the ID of the data set to generate the readme for
	 * 
	 * @param dataSet the ID of the data set
	 */
	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}
	
	/**
	 * Get the date & time of the readme generation
	 * 
	 * @return the date & time of the readme generation
	 */
	public Date getDateTime() {
		return dateTime;
	}
	
	/**
	 * Set the date & time of the readme generation
	 * 
	 * @param dateTime the date & time of the readme generation
	 */
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * Get the entries of the readme
	 * 
	 * @return the entries
	 */
	public List<ReadmeEntry> getEntries() {
		return entries;
	}

	/**
	 * Set the entries of the readme
	 * 
	 * @param entries the entries
	 */
	public void setEntries(List<ReadmeEntry> entries) {
		this.entries = entries;
	}
}
