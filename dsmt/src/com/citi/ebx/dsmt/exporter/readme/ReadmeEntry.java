package com.citi.ebx.dsmt.exporter.readme;

import java.util.Date;

import com.orchestranetworks.schema.Path;

/**
 * Encapsulates configuration details for an entry in the readme file
 */
public class ReadmeEntry {
	private Path tablePath;
	private String exportName;
	private String exportDescription;
	private String frequency;
	private String filter;
	private Path lastUpdatePath;
	private Date lastDeletion;
	private String exportFilename;
	private String reportType = "EBX";
	
	/**
	 * Get the table path
	 * 
	 * @return the table path
	 */
	public Path getTablePath() {
		return tablePath;
	}
	
	/**
	 * Set the table path
	 * 
	 * @param tablePath the table path
	 */
	public void setTablePath(Path tablePath) {
		this.tablePath = tablePath;
	}
	
	/**
	 * Get the export name. This is the way the table name will appear in the file, which isn't necessarily the same as the table's label.
	 * Also, a table can appear twice with different export names.
	 * 
	 * @return the export name
	 */
	public String getExportName() {
		return exportName;
	}
	
	/**
	 * Set the export name. This is the way the table name will appear in the file, which isn't necessarily the same as the table's label.
	 * Also, a table can appear twice with different export names.
	 * 
	 * @param exportName the export name
	 */
	public void setExportName(String exportName) {
		this.exportName = exportName;
	}
	
	/**
	 * Get the export description. This is further text describing the export.
	 * 
	 * @return the export description
	 */
	public String getExportDescription() {
		return exportDescription;
	}
	
	/**
	 * Set the export description. This is further text describing the export.
	 * 
	 * @param exportDescription the export description
	 */
	public void setExportDescription(String exportDescription) {
		this.exportDescription = exportDescription;
	}
	
	/**
	 * Get the frequency code
	 * 
	 * @return the frequency code
	 */
	public String getFrequency() {
		return frequency;
	}
	
	/**
	 * Set the frequency code
	 * 
	 * @param frequency the frequency code
	 */
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	
	/**
	 * Get the path to the last update dateTime field for the table
	 * 
	 * @return the path to the last update dateTime field
	 */
	public Path getLastUpdatePath() {
		return lastUpdatePath;
	}

	/**
	 * Set the path to the last update dateTime field for the table
	 * 
	 * @param lastUpdatePath the path to the last update dateTime field
	 */
	public void setLastUpdatePath(Path lastUpdatePath) {
		this.lastUpdatePath = lastUpdatePath;
	}

	/**
	 * Get the date that the table was last deleted from
	 * 
	 * @return the date that the table was last deleted from
	 */
	public Date getLastDeletion() {
		return lastDeletion;
	}
	
	/**
	 * Set the date that the table was last deleted from
	 * 
	 * @param lastDeletion the date that the table was last deleted from
	 */
	public void setLastDeletion(Date lastDeletion) {
		this.lastDeletion = lastDeletion;
	}
	
	/**
	 * Get the filename of the table's export file
	 * 
	 * @return the filename of the table's export file
	 */
	public String getExportFilename() {
		return exportFilename;
	}
	
	/**
	 * Set the filename of the table's export file
	 * 
	 * @param exportFilename the filename of hte table's export file
	 */
	public void setExportFilename(String exportFilename) {
		this.exportFilename = exportFilename;
	}
	
	
	public String toString(){
		
		return "{tablePath = " + tablePath + 
		",exportName = " + exportName +
		",exportDescription = " + exportDescription +
		",reportType = " + reportType +
		",exportFilename = " + exportFilename + "}"
		;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getFilter() {
		return filter;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getReportType() {
		return reportType;
	}
}
