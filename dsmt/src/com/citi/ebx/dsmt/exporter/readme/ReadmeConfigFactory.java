package com.citi.ebx.dsmt.exporter.readme;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.citi.ebx.dsmt.path.DSMTPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.adaptation.RequestSortCriteria;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

/**
 * Factory for creating a a ReadmeConfig
 */
public abstract class ReadmeConfigFactory {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	/**
	 * Create the ReadmeConfig from a record in a readme table in EBX
	 * 
	 * @param readmeTable the readme table
	 * @param readmeFile the readme file
	 * @param locale the locale
	 * @return the readme config
	 */
	public static ReadmeConfig createFromEBXTable(final AdaptationTable readmeTable, final File readmeFile, final Locale locale) {
		LOG.debug("ReadmeConfigFactory: createFromEBXTable");
		LOG.debug("ReadmeConfigFactory: readmeTable = " + readmeTable.getTablePath().format());
		LOG.debug("ReadmeConfigFactory: readmeFile = " + readmeFile.getAbsolutePath());
		final ReadmeConfig config = new ReadmeConfig();

		// Set the date & time to now
		config.setDateTime(new Date());	
		config.setFile(readmeFile);
		final Adaptation dataSet = readmeTable.getContainerAdaptation();
		final String dataSetLabel = dataSet.getLabel(locale);
		// Use the label of the data set, or if none was set use the ID
		config.setDataSet(dataSetLabel == null
				? dataSet.getAdaptationName().getStringName() : dataSetLabel);
		
		final List<ReadmeEntry> entries = createEntriesFromEBXTable(readmeTable);
		config.setEntries(entries);
		
		return config;
	}
	
	private static List<ReadmeEntry> createEntriesFromEBXTable(final AdaptationTable readmeTable) {
		LOG.debug("ReadmeConfigFactory: createEntriesFromEBXTable");
		final ArrayList<ReadmeEntry> entries = new ArrayList<ReadmeEntry>();
		
		// Sort by the export name column
		final RequestSortCriteria sortCriteria = new RequestSortCriteria();
		sortCriteria.add(DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM._C_RM_TABLE_NAME);
		RequestResult reqRes = readmeTable.createRequestResult(null, sortCriteria);
		
		for (Adaptation adaptation; (adaptation = reqRes.nextAdaptation()) != null;) {
			entries.add(createEntryFromAdaptation(adaptation));
		}
		
		return entries;
	}
	
	private static ReadmeEntry createEntryFromAdaptation(final Adaptation adaptation) {
		if (LOG.isDebug()) {
			LOG.debug("ReadmeConfigFactory: createEntryFromAdaptation");
			LOG.debug("ReadmeConfigFactory: adaptation = " + adaptation.getOccurrencePrimaryKey().format());
		}
		final ReadmeEntry entry = new ReadmeEntry();
		entry.setTablePath(Path.parse(adaptation.getString(DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM._C_GTD_TABLE_NAME)));
		entry.setExportName(adaptation.getString(DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM._C_RM_TABLE_NAME));
		entry.setExportDescription(adaptation.getString(DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM._C_RM_TABLE_BRIEF));
		entry.setFrequency(adaptation.getString(DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM._C_RM_UPDATE_CODE));
		
		entry.setFilter(adaptation.getString(DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM._C_FILTER_TYPE));
		entry.setReportType(adaptation.getString(DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM._C_REPORT_TYPE));
		
		// This assumes all tables have the same relative path for this field as this one
		entry.setLastUpdatePath(DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM._CHNG_DTTM);
		entry.setLastDeletion(adaptation.getDate(DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM._TrgLastDelDt));
		entry.setExportFilename(adaptation.getString(DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM._C_RM_FILE_NAME));
		return entry;
	}
}
