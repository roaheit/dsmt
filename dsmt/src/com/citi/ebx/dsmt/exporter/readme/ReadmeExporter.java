package com.citi.ebx.dsmt.exporter.readme;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.Request;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.adaptation.RequestSortCriteria;
import com.orchestranetworks.ps.filter.ActiveRecordFilter;
import com.orchestranetworks.ps.filter.CurrentRecordFilter;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.Session;

/**
 * Exports a readme file for a data set
 */
public class ReadmeExporter {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private static final String HEADER_DATE_TIME_FORMAT = "dd MMM yyyy hh:mma";
	private static final char SEPARATOR_LINE_CHAR = '-';
	private static final String LAST_CHANGED_DATE_FORMAT = "MMM dd yyyy";
	
	private static final String COLUMN_EXPORT_NAME_STR = "NAME";
	private static final String COLUMN_RECORDS_STR = "RECS";
	private static final String COLUMN_LAST_CHANGED_DATE_STR = "AS_OF_DATE";
	private static final String COLUMN_FREQUENCY_STR = "C";
	private static final String COLUMN_EXPORT_DESCRIPTION_STR = "DESCR";
	private static final String COLUMN_EXPORT_FILE_STR = "FILE";
	
	private static final int COLUMN_EXPORT_NAME_LEN = 21;
	private static final int COLUMN_RECORDS_LEN = 7;
	private static final int COLUMN_LAST_CHANGED_DATE_LEN = 11;
	private static final int COLUMN_FREQUENCY_LEN = 1;
	private static final int COLUMN_EXPORT_DESCRIPTION_LEN = 20;
	private static final int COLUMN_EXPORT_FILE_LEN = 31;
	
	protected ReadmeConfig config;
	
	/**
	 * Create the readme exporter
	 */
	public ReadmeExporter() {
	}
	
	/**
	 * Create the readme exporter with a config
	 * 
	 * @param config the config
	 */
	public ReadmeExporter(final ReadmeConfig config) {
		this.config = config;
	}
	
	/**
	 * Get the config
	 * 
	 * @return the config
	 */
	public ReadmeConfig getConfig() {
		return config;
	}

	/**
	 * Set the config
	 * 
	 * @param config the config
	 */
	public void setConfig(ReadmeConfig config) {
		this.config = config;
	}

	/**
	 * Export the readme file
	 * 
	 * @param dataSet the data set to export it for
	 * @throws IOException if there's an exception writing the output
	 */
	public void exportReadme(Adaptation dataSet, Session session) throws IOException {
		LOG.info("ReadmeExporter: exportReadme");
		LOG.info("ReadmeExporter: dataSet = " + dataSet.getAdaptationName().getStringName());
		
		FileWriter fileWriter = new FileWriter(config.getFile());
		final BufferedWriter writer = new BufferedWriter(fileWriter);
		try {
			writeReadmeHeader(writer);
			writeTableHeader(writer);
			writeEntries(writer, dataSet, session);
			writeReadmeFooter(writer);
		} finally {
			writer.close();
		}
	}
	
	/**
	 * Write the header (the section before the table)
	 * 
	 * @param writer the writer
	 * @throws IOException if there's an exception writing the output
	 */
	protected void writeReadmeHeader(BufferedWriter writer) throws IOException {
		LOG.info("ReadmeExporter: writeReadmeHeader");
		final String headerDesc = createReadmeHeaderDescription();
		final String headerDateTime = createReadmeHeaderDateTime();
		
		writer.newLine();
		writer.newLine();
		
		writer.write(createSeparatorLine(headerDesc.length()));
		writer.write(' ');
		writer.write(createSeparatorLine(headerDateTime.length()));
		writer.newLine();
		
		writer.write(headerDesc);
		writer.write(' ');
		writer.write(headerDateTime);
		writer.newLine();
		
		writer.newLine();
		writer.newLine();
	}
	
	/**
	 * Create the header description
	 * 
	 * @return the header description
	 */
	protected String createReadmeHeaderDescription() {
		LOG.info("ReadmeExporter: createReadmeHeaderDescription");
	
		//	return "Listing of " + config.getDataSet() + " tables as of";
		return bundle.getString("readme.header.text");
	}
	
	/**
	 * Create the header date & time string
	 * 
	 * @return the header date & time string
	 */
	protected String createReadmeHeaderDateTime() {
		LOG.info("ReadmeExporter: createReadmeHeaderDateTime");
		final SimpleDateFormat dateFormat = new SimpleDateFormat(HEADER_DATE_TIME_FORMAT);
		final String dateTimeStr = dateFormat.format(config.getDateTime());
		return dateTimeStr.substring(0, dateTimeStr.length() - 2)
				+ dateTimeStr.substring(dateTimeStr.length() - 2).toLowerCase();
	}
	
	/**
	 * Write the table header out
	 * 
	 * @param writer the writer
	 * @throws IOException if there's an exception writing the output
	 */
	protected void writeTableHeader(BufferedWriter writer) throws IOException {
		LOG.info("ReadmeExporter: writeTableHeader");
		// Set up an array of column header info
		final ReadmeTableColumnConfig[] tableColumnConfigs = {
				new ReadmeTableColumnConfig(COLUMN_EXPORT_NAME_STR, COLUMN_EXPORT_NAME_LEN),
				new ReadmeTableColumnConfig(COLUMN_RECORDS_STR, COLUMN_RECORDS_LEN),
				new ReadmeTableColumnConfig(COLUMN_LAST_CHANGED_DATE_STR, COLUMN_LAST_CHANGED_DATE_LEN),
				new ReadmeTableColumnConfig(COLUMN_FREQUENCY_STR, COLUMN_FREQUENCY_LEN),
				new ReadmeTableColumnConfig(COLUMN_EXPORT_DESCRIPTION_STR, COLUMN_EXPORT_DESCRIPTION_LEN),
				new ReadmeTableColumnConfig(COLUMN_EXPORT_FILE_STR, COLUMN_EXPORT_FILE_LEN)
		};
		
		for (int i = 0; i < tableColumnConfigs.length; i++) {
			final ReadmeTableColumnConfig tableColumnConfig = tableColumnConfigs[i];
			if (i < tableColumnConfigs.length - 1) {
				// Each column except the last one gets padded out, or cut down, to a static length
				writer.write(getStaticLengthString(tableColumnConfig.name, tableColumnConfig.length));
				writer.write(' ');
			} else {
				// The last column doesn't get padded out, so it will be either the length of the text
				// or the max length for the column, whichever is shorter
				writer.write(getMaxLengthString(tableColumnConfig.name, tableColumnConfig.length));
			}
		}
		writer.newLine();
		// Create the lines separating the table header from the table values
		for (int i = 0; i < tableColumnConfigs.length; i++) {
			final ReadmeTableColumnConfig tableColumnConfig = tableColumnConfigs[i];
			final String sepLine = createSeparatorLine(tableColumnConfig.length);
			writer.write(sepLine);
			if (i < tableColumnConfigs.length - 1) {
				writer.write(' ');
			}
		}
		writer.newLine();
	}
	
	/**
	 * Write the readme table entries
	 * 
	 * @param writer the writer
	 * @param dataSet the data set
	 * @throws IOException if there's an exception writing the output
	 */
	protected void writeEntries(BufferedWriter writer, Adaptation dataSet, Session session) throws IOException {
		LOG.info("ReadmeExporter: writeEntries");
		for (ReadmeEntry entry: config.getEntries()) {
			writer.write(getEntryLine(entry, dataSet, session));
			writer.newLine();
		}
	}
	
	/**
	 * Get the line to output for a readme entry
	 * 
	 * @param entry the readme entry
	 * @param dataSet the data set
	 * @return the line to output
	 */
	protected String getEntryLine(ReadmeEntry entry, Adaptation dataSet, Session session) {
		LOG.debug("ReadmeExporter: getEntryLine");
		final StringBuffer strBuff = new StringBuffer();
		
		strBuff.append(' ');
		// Starts with a space so subtract 1
		strBuff.append(getStaticLengthString(entry.getExportName(), COLUMN_EXPORT_NAME_LEN - 1));
		
		strBuff.append(' ');
		final AdaptationTable table = dataSet.getTable(entry.getTablePath());
		// Run a request against the table with no predicate to get all records,
		// and sort them in descending order by last update dateTime.
		RequestSortCriteria sortCriteria = new RequestSortCriteria();
		sortCriteria.add(entry.getLastUpdatePath(), false);
	
		// final RequestResult reqRes = table.createRequestResult(null, sortCriteria);
		
		final Request req = table.createRequest();
		req.setSortCriteria(sortCriteria);
		req.setSpecificFilter(new CurrentRecordFilter());
		
		int recordCount = 0;
		RequestResult reqRes = null;
		
		final String filter = entry.getFilter();
		if("ActiveRecordFilter".equalsIgnoreCase(filter)){
			req.setSpecificFilter(new ActiveRecordFilter());
			LOG.info("Active Record filter set for Readme entry - " + entry);
		}
		
		reqRes = req.execute();
		
		if("SQL".equalsIgnoreCase(entry.getReportType())){
			
			LOG.info("SQL export type found for " + entry);
			
			@SuppressWarnings("unchecked")
			Map<String, Integer> sqlReportMap = (HashMap<String, Integer> ) session.getAttribute("sqlReportMap");
			try {
				recordCount = sqlReportMap.get(entry.getExportFilename());
			} catch (Exception e) {
				LOG.error(entry.getExportFilename() + " not found in " + sqlReportMap + " for GFDSS Catalogue entry -> " + entry);
			}
			
		}
		else{
			
		
			// The record count will be the size of the result set
			recordCount = reqRes.getSize();
		}
		strBuff.append(getStaticLengthString(String.valueOf(recordCount), COLUMN_RECORDS_LEN));
		
		strBuff.append(' ');
		final Date lastDeletion = entry.getLastDeletion();
		final Date lastUpdate;
		// If there are no records for the table, there is no last update dateTime
		if (recordCount == 0) {
			lastUpdate = null;
		// Else the most recently updated row will be the first record in the result set
		// (since it's sorted by lastUpdateDateTime in descending order)
		} else {
			try {
				final Adaptation lastUpdatedRecord = reqRes.nextAdaptation();
				lastUpdate = lastUpdatedRecord.getDate(entry.getLastUpdatePath());
			} finally {
				// After getting the first record, we don't need the result set any more
				// so it can be closed
				
				if(null != reqRes){
					reqRes.close();
				}
			}
		}
		final Date dateToExport;
		// If there's no deletion date for the table then use the last update dateTime
		if (lastDeletion == null) {
			dateToExport = lastUpdate;
		// Else we have to see which is more recent
		} else {
			// If there's no last update dateTime then use the last deletion date
			if (lastUpdate == null) {
				dateToExport = lastDeletion;
			// Else use whichever is more recent
			} else {
				dateToExport = lastDeletion.after(lastUpdate) ? lastDeletion : lastUpdate;
			}
		}
		// If there was no last update dateTime or last deletion date, then simply show blanks
		if (dateToExport == null) {
			strBuff.append(getStaticLengthString("", COLUMN_LAST_CHANGED_DATE_LEN));
		// Else format the date for display
		} else {
			final SimpleDateFormat dateFormat = new SimpleDateFormat(LAST_CHANGED_DATE_FORMAT);
			strBuff.append(dateFormat.format(dateToExport));
		}
		
		strBuff.append(' ');
		strBuff.append(entry.getFrequency());
		
		strBuff.append(' ');
		strBuff.append(getStaticLengthString(entry.getExportDescription(), COLUMN_EXPORT_DESCRIPTION_LEN));
		
		strBuff.append(' ');
		// It's padded out to 1 less than the ----- separator
		strBuff.append(getStaticLengthString(entry.getExportFilename(), COLUMN_EXPORT_FILE_LEN - 1));
		
		return strBuff.toString();
	}
	
	/**
	 * Write the footer (the section after the table)
	 * 
	 * @param writer the writer
	 * @throws IOException if there's an exception writing the output
	 */
	protected void writeReadmeFooter(BufferedWriter writer) throws IOException {
		LOG.info("ReadmeExporter: writeReadmeFooter");
		writer.newLine();
		writer.write(String.valueOf(config.getEntries().size()));
		writer.write(" rows selected.");
		writer.newLine();
		writer.newLine();
	}
	
	/**
	 * Create a line used as a separation in the readme
	 * 
	 * @param length the length of the line
	 * @return the separator line
	 */
	protected static String createSeparatorLine(final int length) {
		final char[] chars = new char[length];
		Arrays.fill(chars, SEPARATOR_LINE_CHAR);
		return new String(chars);
	}
	
	// Either pad or trim the given string to the specified length
	private static String getStaticLengthString(final String str, final int length) {
		final int strLen = str.length();
		if (length == strLen) {
			return str;
		}
		if (length < strLen) {
			return str.substring(0, length);
		}
		final char[] chars = new char[length - strLen];
		Arrays.fill(chars, ' ');
		return str + new String(chars);
	}
	
	// If the given string is longer than the specified length, trim it to that length.
	// Otherwise, simply return the string.
	private static String getMaxLengthString(final String str, final int length) {
		final int strLen = str.length();
		if (length >= strLen) {
			return str;
		}
		return str.substring(0, length);
	}
	
	/**
	 * Stores info about a table column for use with the readme exporter
	 */
	protected static class ReadmeTableColumnConfig {
		/**
		 * Create the readme table column config
		 * 
		 * @param name the name of the column
		 * @param length the length of the column
		 */
		protected ReadmeTableColumnConfig(final String name, final int length) {
			this.name = name;
			this.length = length;
		}
		
		/**
		 * The name of the column
		 */
		protected String name;
		
		/**
		 * The length of the column
		 */
		protected int length;
	}

	private static final ResourceBundle bundle = ResourceBundle.getBundle("com.citi.ebx.dsmt.util.ApplicationResources");
}
