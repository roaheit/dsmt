package com.citi.ebx.dsmt.exporter.sql;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExportConfigFactory;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ServiceContext;
import com.orchestranetworks.service.Session;

/**
 * A servlet for invoking the data set export. It is not required for invoking the exporter, but is
 * one way in which it can be invoked. It is required if you wish to invoke it via the GUI services menu.
 * 
 */
public class SQLExportServlet extends HttpServlet {
	// These are the parameters for the servlet

	public static final String PARAM_SQL_EXPORT_CONFIG_ID = "sqlExportConfigID";
	public static final String PARAM_SQL_MAPPING_FULL_PATH = "sqlMappingFullPath";
	public static final String PARAM_EXPORT_FILE_FULL_PATH ="exportFileDirectory";

	

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	

	private String sqlExportConfigID;
	private String sqlMappingFullPath;
	private String exportFileDirectory;

	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		LOG.info("SQLExportServlet: service");
		final PrintWriter out = res.getWriter();
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		final Adaptation dataSet = sContext.getCurrentAdaptation();

		sqlExportConfigID =  req.getParameter(PARAM_SQL_EXPORT_CONFIG_ID);
		sqlMappingFullPath =  req.getParameter(PARAM_SQL_MAPPING_FULL_PATH);
		exportFileDirectory = req.getParameter(PARAM_EXPORT_FILE_FULL_PATH);
		
		try {
			export(sContext.getSession(), sContext.getCurrentHome().getRepository(), dataSet);
		} catch (final OperationException ex) {
			throw new ServletException(ex);
		}
		
		writeRedirectionOnEnding(out, sContext);
	}
	
	protected void export(Session session, Repository repo, Adaptation dataSet)
			throws IOException, OperationException {
		LOG.info("SQLExportServlet: export");
		SQLReportExport export = new SQLReportExport();
		export.runSQLReportGeneration(sqlExportConfigID,sqlMappingFullPath,exportFileDirectory);
	}
	
	
	protected void writeRedirectionOnEnding(PrintWriter out, ServiceContext sContext) {
		LOG.info("SQLExportServlet: writeRedirectionOnEnding");
		LOG.debug(" sContext.getURLForEndingService()  " +  sContext.getURLForEndingService() );
		out.print("<script>window.location.href='" + sContext.getURLForEndingService() + "';</script>");
	}
}
