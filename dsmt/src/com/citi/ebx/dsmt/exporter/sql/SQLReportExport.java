package com.citi.ebx.dsmt.exporter.sql;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.citi.ebx.dsmt.jdbc.connector.SQLConnectReportUtil;
import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTExportBean;
import com.citi.ebx.dsmt.util.HeaderTailerGenerator;
import com.citi.ebx.util.SaveUtil;
import com.citi.ebx.util.Utils;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

/**
 * 
 * @author nc72931
 *
 */
public class SQLReportExport{

	
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	   
	
	final String env =  propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);

	public static final String FILE_NAME_TO_BE_GENERATED = "fileNameToBeGenerated";
	public static final String EXPORT_FILE_DIRECTORY = "exportFileDirectory";
	public static final String SQL_FILE_PATH = "sqlFilePath";
	
	private String sqlFilePath = "/citi/goc/abc.sql";
	private String exportFileDirectory = "/home/cloudusr/EBXHome/importFiles";
	private String fileNameToBeGenerated = "tempFile.txt";
	private String headerRow = null;
	
	private String recordCount = "0" ;
	
	private String errorMessage;
	private String errorDetails;
	
	protected String exportFileDirectoryPath;
	
	public void runSQLReportGeneration(String sqlList,String sqlMappingFilePath, String exportFileDirectory){
		
		generateSQLReport(exportFileDirectory, sqlFilePath, sqlList, sqlMappingFilePath );
		
	}

	

	public Map<String, Integer> generateSQLReport (String exportFileDirectory, String sqlFilePath, String sqlList, String mappingFilePath){


		
		LOG.info("Report generation started !! "); 
		
		Map<String, Integer> sqlReportMap = null;
		Connection conn=null;
		
		try{
			
			SQLReportExportUtil connectReportUtil = new SQLReportExportUtil();
			connectReportUtil.setSqlNames(sqlList);
			connectReportUtil.setMappingFilePath(mappingFilePath);
			conn = SQLReportConnectionUtils.getConnection();
			
			if(null!=exportFileDirectory){
				exportFileDirectoryPath=exportFileDirectory;
			}

						
			sqlReportMap = connectReportUtil.generateReport(conn, exportFileDirectoryPath);
			List<String> sqlreportList = new ArrayList<String>(sqlReportMap.keySet());
			DSMTExportBean dsmtExportBean = new DSMTExportBean();
			dsmtExportBean.getFileNamesAndCount().putAll(sqlReportMap);
			 dsmtExportBean.setFileNames(sqlreportList);
			 dsmtExportBean.setOutputFilePath(exportFileDirectoryPath);
			 LOG.info("htBean = " +  dsmtExportBean);	
		//	new HeaderTailerGenerator().runHeaderTrailerGenerator(dsmtExportBean);
			LOG.info("HT  generated successfully !!!  for  reports :  "+sqlreportList);

			}
		catch (final Exception e){
			
			LOG.error("Report generation terminated with Exception in SQLReportExport  -> " + e + ", message = " + e.getMessage());
		}
		
		return sqlReportMap;
	}
	
	
	public String getFileNameToBeGenerated() {
		return fileNameToBeGenerated;
	}

	public void setFileNameToBeGenerated(String fileNameToBeGenerated) {
		this.fileNameToBeGenerated = fileNameToBeGenerated;
	}


	public String getRecordCount() {
		return recordCount;
	}


	public void setRecordCount(String recordCount) {
		this.recordCount = recordCount;
	}

	

	public String getSqlFilePath() {
		return sqlFilePath;
	}

	public void setSqlFilePath(String sqlFilePath) {
		this.sqlFilePath = sqlFilePath;
	}

	public String getExportFileDirectory() {
		return exportFileDirectory;
	}

	public void setExportFileDirectory(String exportFileDirectory) {
		this.exportFileDirectory = exportFileDirectory;
	}

	public String getHeaderRow() {
		return headerRow;
	}

	public void setHeaderRow(String headerRow) {
		this.headerRow = headerRow;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	

}
