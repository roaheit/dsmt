package com.citi.ebx.dsmt.exporter.sql;

import java.io.BufferedWriter;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.ServiceContext;

/**
 * A servlet that presents a form to the user, allowing them to specify the data space and data set
 * of the export config records, and the ID of the export config to execute. Upon submitting the form,
 * the DataSetExportServlet is invoked. This is not required to execute the exporter, but it is required
 * if you wish to allow the user to invoke it via the services menu and you wish them to be able to
 * choose the export config rather than have it hardcoded by the jsp.
 * 
 */
public class SQLExportForm extends HttpServlet {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private static final String DEFAULT_SERVLET = "/SQLExport";
	private static final String DEFAULT_SQL_EXPORT_CONFIG_ID = "";
	private static final String DEFAULT_SQL_MAPPING_FULL_PATH = "";
//	protected String mergeReadme = "true";
	
	private static final long serialVersionUID = 1L;
	
	protected BufferedWriter out;
	

	protected String servlet = DEFAULT_SERVLET;
	protected String sqlExportConfigID = DEFAULT_SQL_EXPORT_CONFIG_ID;
	protected String sqlMappingFullPath = DEFAULT_SQL_MAPPING_FULL_PATH;
	
	/**
	 * Get the string identifying the servlet to invoke upon submit. This needs to match the name configured
	 * in the application server config (i.e. server.xml). For example, "/DataSetExport".
	 * 
	 * @return the servlet
	 */
	public String getServlet() {
		return servlet;
	}

	/**
	 * Set the string identifying the servlet to invoke upon submit. This needs to match the name configured
	 * in the application server config (i.e. server.xml). For example, "/DataSetExport".
	 * 
	 * @param servlet the servlet
	 */
	public void setServlet(String servlet) {
		this.servlet = servlet;
	}

	/**
	 * @return the sqlExportConfigID
	 */
	public String getSqlExportConfigID() {
		return sqlExportConfigID;
	}

	/**
	 * @param sqlExportConfigID the sqlExportConfigID to set
	 */
	public void setSqlExportConfigID(String sqlExportConfigID) {
		this.sqlExportConfigID = sqlExportConfigID;
	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		LOG.debug("DataSetExportForm: service");
		out = new BufferedWriter(res.getWriter());
		final ServiceContext sContext = ServiceContext.getServiceContext(req);
		writeForm(sContext);
		out.flush();
	}
	
	protected void writeForm(ServiceContext sContext) throws IOException {
		LOG.debug("DataSetExportForm: writeForm");
		writeln("<form id=\"form\" action=\"" + sContext.getURLForIncludingResource(servlet)
				+ "\" method=\"post\" autocomplete=\"off\">");
		writeFields(sContext);
		writeSubmitButton(sContext);
		writeln("</form>");
	}
	
	protected void writeFields(ServiceContext sContext) throws IOException {
		// Much of this HTML is replicating what gets produced by EBX to make it look
		// similar to the general look of EBX. However, It is not guaranteed that EBX
		// will continue to use these same css tags, etc in future versions.
		LOG.debug("DataSetExportForm: writeFields");
		writeln("  <table>");
		writeln("    <tbody>");
		
		
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + SQLExportServlet.PARAM_SQL_EXPORT_CONFIG_ID
				+ "\">SQL Config ID</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + sqlExportConfigID
				+ "\" type=\"text\" id=\"" +SQLExportServlet.PARAM_SQL_EXPORT_CONFIG_ID
				+ "\" name=\"" + SQLExportServlet.PARAM_SQL_EXPORT_CONFIG_ID + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");
		
		writeln("      <tr class=\"ebx_Field\">");
		writeln("        <td class=\"ebx_Label\">");
		writeln("          <label for=\"" + SQLExportServlet.PARAM_SQL_MAPPING_FULL_PATH
				+ "\">SQL Mapping Full Path</label>");
		writeln("        </td>");
		writeln("        <td class=\"ebx_Input\">");
		writeln("          <input value=\"" + sqlMappingFullPath
				+ "\" type=\"text\" id=\"" +SQLExportServlet.PARAM_SQL_MAPPING_FULL_PATH
				+ "\" name=\"" + SQLExportServlet.PARAM_SQL_MAPPING_FULL_PATH + "\"/>");
		writeln("        </td>");
		writeln("      </tr>");

	
		writeln("    </tbody>");
		writeln("  </table>");
	}
	
	protected void writeSubmitButton(ServiceContext sContext) throws IOException {
		LOG.debug("DataSetExportForm: writeSubmitButton");
		writeln("  <button type=\"submit\" class=\"ebx_Button\">Export</button>");
	}
	
	protected void write(final String str) throws IOException {
		out.write(str);
	}
	
	protected void writeln(final String str) throws IOException {
		write(str);
		out.newLine();
	}
}
