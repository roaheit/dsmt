package com.citi.ebx.dsmt.exporter.sql;

import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class MultipleSQLReportScheduledTask extends ScheduledTask {
	 protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	 protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	   
		
	 	protected String sqlExportConfigID;
	 	protected String sqlMappingFullPath;
	 	protected String exportFileDirectory;
		
		
		@Override
		public void execute(ScheduledExecutionContext arg0)
				throws OperationException, ScheduledTaskInterruption {
			
			LOG.info("MultipleSQLReportScheduledTask : Report generation started !! "); 
			
			SQLReportExport export = new SQLReportExport();
			export.runSQLReportGeneration(sqlExportConfigID, sqlMappingFullPath, exportFileDirectory);
			
			LOG.info("MultipleSQLReportScheduledTask : Report generation end !! "); 
		}
		

		/**
		 * @return the sqlExportConfigID
		 */
		public String getSqlExportConfigID() {
			return sqlExportConfigID;
		}

		/**
		 * @param sqlExportConfigID the sqlExportConfigID to set
		 */
		public void setSqlExportConfigID(String sqlExportConfigID) {
			this.sqlExportConfigID = sqlExportConfigID;
		}

		/**
		 * @return the sqlMappingFullPath
		 */
		public String getSqlMappingFullPath() {
			return sqlMappingFullPath;
		}

		/**
		 * @param sqlMappingFullPath the sqlMappingFullPath to set
		 */
		public void setSqlMappingFullPath(String sqlMappingFullPath) {
			this.sqlMappingFullPath = sqlMappingFullPath;
		}


		public String getExportFileDirectory() {
			return exportFileDirectory;
		}


		public void setExportFileDirectory(String exportFileDirectory) {
			this.exportFileDirectory = exportFileDirectory;
		}


		
		
}
