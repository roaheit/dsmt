package com.citi.ebx.dsmt.exporter.sql;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.task.GenerateSQLExportFileTask;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;

public class SQLReportExportUtil {


	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	
	private String reportType = "SQL";
	
	final String env =  propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
	private static final String  EFFECTIVE_DATE_COLUMN="PRT_EFFDT";


    protected String sql ;
    protected static Connection dbConnection = null;
  
    protected ArrayList<String> reportNames ;
    protected ArrayList<String> sqlReportNames ;
    protected String sqlNames  ;
    protected Map<String, Integer> readmeMap;
    
    protected String sqlID ;
    protected String exportFileDirectoryPath;
    protected String exportFileName;
    private boolean customHeaderTrailer=false;
	private  static String printEffectiveDate ;
	protected String mappingFilePath;
	
	


	/**
	 * @param mappingFilePath the mappingFilePath to set
	 */
	public void setMappingFilePath(String mappingFilePath) {
		this.mappingFilePath = mappingFilePath;
	}



	public void setReportType(String reportType) {
		this.reportType = reportType;
	}



	/**
	 * @param sqlNames the sqlNames to set
	 */
	public void setSqlNames(String sqlNames) {
		this.sqlNames = sqlNames;
	}
	
	public boolean isCustomHeaderTrailer() {
		return customHeaderTrailer;
	}



	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
    
   

	public Map<String, Integer> generateReport (final Connection conection, String outputFilePath) {
    	
    	
    	try{
    		 dbConnection=conection;
    		 // methos called to get the sql file name and there mapping report name from specific file
    //		 getSQlReportMappingRelative();
			//method call to generate currency reports 
    		 printEffectiveDate = getEffectiveDateForReport();
    		 
    		 /** get default output file path if value is not passed in as a parameter */
    		 if(null == outputFilePath){
    			 
    			 outputFilePath = SQLReportConnectionUtils.bundle.getString(env + DSMTConstants.DOT +"ExportFileDirectory");
    		 }
    		 
    		 generateCurrencyRpt(SQLReportConnectionUtils.bundle.getString(env + DSMTConstants.DOT + "SqlFileDirectory"), outputFilePath );
			 
    	}
    	catch(SQLException e)
    	{
    		LOG.error ("Error inside method  generateReport  ->" + e + " , message " + e.getMessage()); 
    	}
    	LOG.info("Readme map file in SQlConnect Util file "+ readmeMap);
    	return readmeMap;
	}
    
    /**
   	 * Helper method to get the O/P from the file table
   	 * @param  
   	 * @throws SQLException
   	 */   
	 public void generateCurrencyRpt (final String sqlFileDirectory, final String exportFileDirectory) throws SQLException
		{
	    	
			int reportcount=0;
			try {
				readmeMap= new java.util.HashMap<String, Integer>();
				
				String[] sqlListNames = sqlNames.split("\\,");
				
				for (String sqlfilename : sqlListNames) {
					// get the sql query from the given file location
					LOG.info("sqlfilename-->"+sqlfilename);
					sqlID = getInputQuery(sqlFileDirectory, sqlfilename);
					exportFileDirectoryPath= exportFileDirectory;
					getSQlReportMappingRelative(sqlfilename,mappingFilePath);
					LOG.info("reportcount-->"+reportcount);
					exportFileName = reportNames.get(reportcount);
					//This method will generate execute the query and write each row in given file
					selectRecordsFromTable(); 
				//	reportcount++;
					exportFileName=null;
				}
				
			} catch (SQLException e) {
				LOG.error ("Error inside method  generateCurrencyRpt  ->" + e + " , message " + e.getMessage()); 
			}
			finally{
				if (dbConnection != null) {
					dbConnection.close();
				}
				
			}
			  
			
		}
    
	    /**
	   	 * Helper method to get the O/P from the file table
	   	 * @param  
	   	 * @throws SQLException
	   	 */   
	    
	    
	    
	    private  void selectRecordsFromTable() throws SQLException {
			 
			
			PreparedStatement preparedStatement = null;
			
			File file = null;
			FileOutputStream fos = null;
			
					 		 
			try {
				LOG.info(" ** SQl Connect report started for report name  = " + exportFileName + " for report type "+ getReportType()+isCustomHeaderTrailer()); 
				// Creating prepare statement.
				preparedStatement = dbConnection.prepareStatement(sqlID);
				// execute select SQL statement
				ResultSet rs = preparedStatement.executeQuery();
				ResultSetMetaData rsMetaData = rs.getMetaData();
				// get the count of column to generate the report.
				int columncount = rsMetaData.getColumnCount();
				// to get record counts in report
				int recordcount=0;
				
				if(! exportFileDirectoryPath.endsWith("/")){
					exportFileDirectoryPath = exportFileDirectoryPath + "/";
				}
				file = new File(exportFileDirectoryPath+exportFileName);
				
				file.setReadable(true, false);
				file.setWritable(true, true);
				
				fos = new FileOutputStream(file);
				if(isCustomHeaderTrailer()){
				int subsStrIndex=exportFileName.indexOf(".");
				StringBuffer header=new StringBuffer(generateHeader(exportFileName.substring(0,subsStrIndex),exportFileName)+DSMTConstants.NEXT_LINE);
				fos.write(header.toString().getBytes());
				}
				
				
				while (rs.next()) {

					StringBuffer reportfile= new StringBuffer();
					recordcount++;
						for(int i=0 ; i<columncount; i++ )
						{
								String data= rs.getString(i+1);
								
								if(DSMTUtils.isStringNotEmptyOrNull(data))
								{
									if(data.equals(EFFECTIVE_DATE_COLUMN))data=printEffectiveDate;
									reportfile.append(data);
								}
								else
								{
									reportfile.append(DSMTConstants.BLANK_STRING);
								}
							
							
						}
					
					reportfile.append(DSMTConstants.NEXT_LINE);
					byte[] contentInBytes = reportfile.toString().getBytes();
					fos.write(contentInBytes);
					
				}
				readmeMap.put(exportFileName,recordcount);
				//T|699263
				if(isCustomHeaderTrailer()){
					String trailer=DSMTConstants.TRAILER+DSMTConstants.PIPE_SEP+ recordcount + DSMTConstants.NEXT_LINE;
					fos.write(trailer.getBytes());
				}
				LOG.info("Total record count in " + exportFileName +" is "+ recordcount);
				
	 
			} catch (SQLException e) {
				LOG.error (" Error  while excuting the query for report  >>>>" + exportFileName + ". Exception ->" + e + " , message " + e.getMessage() );
				
	 
			}
			 catch (FileNotFoundException e) {
				 
				 LOG.error (" Error  while creating  >>>>" + exportFileName + ". Exception ->" + e + " , message " + e.getMessage() );
		 
				} 
			catch (IOException e) {
				 
				 LOG.error ("  Error  while writing  >>>>" + exportFileName + ". Exception ->" + e + " , message " + e.getMessage() );
	 
			}
			finally {
	 
				if (preparedStatement != null) {
					preparedStatement.close();
				}
	 
				try {
					if (fos != null) {
						fos.close();
					}
					
					} catch (IOException e) {
						LOG.error ("  Error  while closing  >>>>" + exportFileName + ". Exception ->" + e + " , message " + e.getMessage() );
					
				}
			
				sqlID = DSMTConstants.BLANK_STRING;
	 
			}
			
	 
		}  
	    
    /**
   	 * Helper method to read Sql from file
   	 * @param  sqlFileDirectory , sqlFile
   	 * @throws 
   	 */       
    
    protected static String getInputQuery(String sqlFileDirectory,String sqlFile) {
		LOG.info("getInputQuery  "+sqlFileDirectory +"    "+sqlFile);
		StringBuffer inputquery = new StringBuffer();
		BufferedReader br = null;
		String line=null;
		try {	
				br = new BufferedReader(new FileReader(sqlFileDirectory+sqlFile));
				while ((line = br.readLine()) != null) {
				
					inputquery.append(line);
					inputquery.append(DSMTConstants.NEXT_LINE);
				
				}
			
		} catch (FileNotFoundException e) {
			 
			LOG.error ("Unable to find SQl file  >>>>" + sqlFile + ". Exception ->" + e + " , message " + e.getMessage() );
	 
			} 
		catch (IOException e) {
			 
			LOG.error ("Unable to read SQl file " + sqlFile + ". Exception ->" + e + " , message " + e.getMessage());
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return inputquery.toString();
	}
    
    
   /**
   	 * Helper method to get SQl file name and corresponding Report name
   	 * @param  
   	 * @throws 
   	 */        
    

	



			
	 public String getReportType() {
			return reportType;
		}
	 
	
	    
	    /**
	   	 * Helper method to get SQl file name and corresponding Report name
	   	 * @param  
	   	 * @throws 
	   	 */        
	    

	public void getSQlReportMappingRelative(String fileName, String mappingFilePath)
	{

				BufferedReader br = null;
				reportNames= new ArrayList<String>();
				sqlReportNames= new ArrayList<String>();
				int count_of_sql=0;
				try {
				
						String line = DSMTConstants.BLANK_STRING;
						String csvSplitBy = DSMTConstants.EQUALS;
						LOG.info("getSQlReportMappingRelative--->"+getReportType()+env);
						if(null!=mappingFilePath){
							
							br = new BufferedReader(new FileReader(mappingFilePath));
						}else{
							br = new BufferedReader(new FileReader(SQLReportConnectionUtils.bundle.getString(env + DSMTConstants.DOT + "SqlMAppingFile")));	
						}
						

									while ((line = br.readLine()) != null) {
										
										if(line.contains(fileName)){
				
										String[] values = line.split(csvSplitBy);
										sqlReportNames.add(count_of_sql, values[0]);
								        reportNames.add(count_of_sql, values[1]);
										count_of_sql++;
											}
							
										}
						
				
				} catch (FileNotFoundException e) {
					 
					LOG.error ("Unable to find SQl property file  >>>> for report Type "+getReportType() +" : "+ e.getMessage() );
			 
					} 
				catch (IOException e) {
					 
					LOG.error ("Unable to read SQl property file " + e.getMessage());
				}
				finally {
					if (br != null) {
						try {
							br.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				LOG.info("NO of report ->> " +count_of_sql);
				}


	public String getEffectiveDateForReport(){
		
		if(null!=getReportType()){
			 if(getReportType().equals(DSMTConstants.BD3REPORT)){
				return DSMTUtils.formatDate(DSMTUtils.previousMonthEffectiveDate(), DSMTConstants.DEFAULT_DATE_FORMAT);	
			}
			return DSMTUtils.treeEffectiveDate();
		}else{
			return DSMTUtils.treeEffectiveDate();
		}
	}
	
	private String generateHeader(String outBoundName,String fileName){
		
		final String timeStamp = new SimpleDateFormat(
				"MM/dd/yyyy HH:mm:ss").format(Calendar
				.getInstance().getTime());
		
		String headerStr =DSMTConstants.HEADER + DSMTConstants.PIPE_SEP + DSMTConstants.BLANK_STRING + outBoundName
		+ DSMTConstants.BLANK_STRING + DSMTConstants.PIPE_SEP + DSMTConstants.BLANK_STRING + timeStamp + DSMTConstants.BLANK_STRING + DSMTConstants.PIPE_SEP + DSMTConstants.BLANK_STRING
		+ fileName;
		
		return headerStr;
		
	}
	
		
}
