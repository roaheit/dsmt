package com.citi.ebx.dsmt.exporter.servlet;

import com.citi.ebx.dsmt.exporter.custseg.CustSegExporter;
import com.citi.ebx.dsmt.exporter.custseg.CustSegTableExporterFactory;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.ps.exporter.servlet.DataSetExportServlet;

/**
 * A DataSetExportServlet that handles DSMT-specific exports
 */
public class CustSegExportServlet extends DataSetExportServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected DataSetExporter createExporter(TableExporterFactory tableExporterFactory,
			DataSetExportConfig config) {
		return new CustSegExporter(tableExporterFactory, config);
	}

	@Override
	protected TableExporterFactory createTableExporterFactory() {
		LOG.info("EXPORTER FACTORY");
		return new CustSegTableExporterFactory();
	}
}
