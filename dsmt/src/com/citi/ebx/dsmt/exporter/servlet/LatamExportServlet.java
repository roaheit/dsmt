package com.citi.ebx.dsmt.exporter.servlet;


import com.citi.ebx.dsmt.exporter.LatamExporter;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.ps.exporter.servlet.DataSetLatamExportServlet;

/**
 * A DataSetExportServlet that handles DSMT-specific exports
 */
public class LatamExportServlet extends DataSetLatamExportServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected LatamExporter createExporter(TableExporterFactory tableExporterFactory,
			DataSetExportConfig config) {
		return new LatamExporter(tableExporterFactory, config, mergeReadme, runSQLReports , runHTGenerator);
	}
}
