package com.citi.ebx.dsmt.exporter.servlet;

import com.citi.ebx.dsmt.exporter.ProdTypeExporter;
import com.citi.ebx.dsmt.exporter.ProdTypeTableExporterFactory;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.ps.exporter.servlet.DataSetExportServlet;

/**
 * A DataSetExportServlet that handles DSMT-specific exports
 */
public class ProdTypeExportServlet extends DataSetExportServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected DataSetExporter createExporter(TableExporterFactory tableExporterFactory,
			DataSetExportConfig config) {
		return new ProdTypeExporter(tableExporterFactory, config);
	}

	@Override
	protected TableExporterFactory createTableExporterFactory() {
		return new ProdTypeTableExporterFactory();
	}
}
