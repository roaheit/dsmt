package com.citi.ebx.dsmt.exporter.servlet;


import com.citi.ebx.dsmt.exporter.DSMTExporter;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.ps.exporter.servlet.DataSetExportServlet;

/**
 * A DataSetExportServlet that handles DSMT-specific exports
 */
public class DSMTExportServlet extends DataSetExportServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected DataSetExporter createExporter(TableExporterFactory tableExporterFactory,
			DataSetExportConfig config) {
		return new DSMTExporter(tableExporterFactory, config, mergeReadme, runSQLReports , runHTGenerator);
	}
}
