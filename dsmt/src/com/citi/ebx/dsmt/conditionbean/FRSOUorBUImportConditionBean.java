package com.citi.ebx.dsmt.conditionbean;

import com.citi.ebx.dsmt.util.DSMTUtils;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

public class FRSOUorBUImportConditionBean extends ConditionBean {

	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private final String C_DSMT_FRS_OU = "C_DSMT_FRS_OU";
	private final String C_DSMT_FRS_BU = "C_DSMT_FRS_BU";
	
	private String importConfigID;
	private String stagingConfigID;
	private String disableTriggers;
	
	private boolean writeErrorToContext = true;
	private String errorMessage;
	private String errorDetails;
	
	
	@Override
	public boolean evaluateCondition(ConditionBeanContext arg0)
			throws OperationException {
	
		boolean isFRSOUorBUImportConfig = false;
		if(DSMTUtils.isStringNotEmptyOrNull(importConfigID)){
			isFRSOUorBUImportConfig = importConfigID.equalsIgnoreCase(C_DSMT_FRS_OU) || importConfigID.equalsIgnoreCase(C_DSMT_FRS_BU);
		
			if (isFRSOUorBUImportConfig) {
				disableTriggers = "false";
				stagingConfigID = importConfigID.concat("_STG");
				
			}
		}
		LOG.info("isFRSOUorBUImportConfig = " + isFRSOUorBUImportConfig + ", disableTriggers = " +  disableTriggers + ", stagingConfig ID = " + stagingConfigID);
		return isFRSOUorBUImportConfig;
		
	}
	
	
	public String getImportConfigID() {
		return importConfigID;
	}
	public void setImportConfigID(String importConfigID) {
		this.importConfigID = importConfigID;
	}
	
	public String getDisableTriggers() {
		return disableTriggers;
	}
	
	public void setDisableTriggers(String disableTriggers) {
		this.disableTriggers = disableTriggers;
	}
	public boolean isWriteErrorToContext() {
		return writeErrorToContext;
	}
	public void setWriteErrorToContext(boolean writeErrorToContext) {
		this.writeErrorToContext = writeErrorToContext;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getErrorDetails() {
		return errorDetails;
	}
	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}





	public void setStagingConfigID(String stagingConfigID) {
		this.stagingConfigID = stagingConfigID;
	}





	public String getStagingConfigID() {
		return stagingConfigID;
	}
	

	
}
