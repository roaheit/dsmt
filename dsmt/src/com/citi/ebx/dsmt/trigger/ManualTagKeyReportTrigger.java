package com.citi.ebx.dsmt.trigger;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.ValueContextForUpdate;

public class ManualTagKeyReportTrigger extends AuditColumnsTrigger{

	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path userPlanTablePath = Path.parse("/root/ENTITLEMENTS/USER_PLANS");
	private Path SoeID = Path.parse("./SOEID");
	private static final Path PLAN_ID_PATH = Path.parse("./PLAN_ID");
	
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException{
		
	final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();

	Session session = context.getSession();
	UserReference user=session.getUserReference();
	Role role=Role.forSpecificRole("Rosetta_Plan_Owner");
	String Soeid = session.getUserReference().getUserId();
	
	List<String> plans = new ArrayList<String>();
		
		Repository repo = context.getAdaptationHome().getRepository();
		Adaptation metadataDataSet;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(repo,"Rosetta", "Rosetta");
			final AdaptationTable targetTable = metadataDataSet.getTable(userPlanTablePath);
			
			String predicate = SoeID.format() + "= \"" + Soeid+"\"";
			
			RequestResult reqRes = targetTable.createRequestResult(predicate);
			for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
				String planID =  record.getString(Path.SELF.add(PLAN_ID_PATH));
				LOG.info("Plan  ID ::  "+ planID);
				plans.add(planID);
			}
		} catch (OperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		
	
		final List<String> cblCode = (List<String>) vc.getValue(DSMTConstants.CBL_CD);
		final List<String> coCode = (List<String>) vc.getValue(DSMTConstants.CO_CD);
		
		final List<String> mleCode = (List<String>) vc.getValue(DSMTConstants.MLE_CD);
		final List<String> gfCode = (List<String>) vc.getValue(DSMTConstants.GF_CD);
		
		if(session.getDirectory().isUserInRole(user, role)){
	
			if(cblCode.size() < 1){
				List<String> dCBL = new ArrayList<String>();
				dCBL.add("NO_CBL");
				vc.setValue(dCBL, DSMTConstants.CBL_CD);
			}else if(!cblCode.contains("NO_CBL") && (!plans.containsAll(cblCode))){
				throw OperationException.createError(
						"You are not allowed to created record for one of the Plan " + cblCode.toString());
			}
			if(coCode.size()<1){
				List<String> dCO = new ArrayList<String>();
				dCO.add("NO_CO");
				vc.setValue(dCO, DSMTConstants.CO_CD);
			}else if(!coCode.contains("NO_CO") && (!plans.containsAll(coCode))){
				throw OperationException.createError(
						"You are not allowed to created record for one of the Plan " + coCode.toString());
			}
			
			if(mleCode.size()<1){
				List<String> dMLE = new ArrayList<String>();
				dMLE.add("NO_MLE");
				vc.setValue(dMLE, DSMTConstants.MLE_CD);
			}else if(!mleCode.contains("NO_MLE") && (!plans.containsAll(mleCode))){
				throw OperationException.createError(
						"You are not allowed to created record for one of the Plan " + mleCode.toString());
			}
			if(gfCode.size()<1){
				List<String> dGF = new ArrayList<String>();
				dGF.add("NO_GF");
				vc.setValue(dGF, DSMTConstants.GF_CD);
			}else if(!gfCode.contains("NO_GF") && (!plans.containsAll(gfCode))){
				throw OperationException.createError(
						"You are not allowed to created record for one of the Plan " + gfCode.toString());
			}
		
		}else{
			if(cblCode.size() < 1){
				List<String> dCBL = new ArrayList<String>();
				dCBL.add("NO_CBL");
				vc.setValue(dCBL, DSMTConstants.CBL_CD);
			}
			if(coCode.size()<1){
				List<String> dCO = new ArrayList<String>();
				dCO.add("NO_CO");
				vc.setValue(dCO, DSMTConstants.CO_CD);
			}
			
			if(mleCode.size()<1){
				List<String> dMLE = new ArrayList<String>();
				dMLE.add("NO_MLE");
				vc.setValue(dMLE, DSMTConstants.MLE_CD);
			}
			if(gfCode.size()<1){
				List<String> dGF = new ArrayList<String>();
				dGF.add("NO_GF");
				vc.setValue(dGF, DSMTConstants.GF_CD);
			}
		}
		super.handleBeforeCreate(context);
		
	}
	
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		
		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();

		Session session = context.getSession();
		UserReference user=session.getUserReference();
		Role role=Role.forSpecificRole("Rosetta_Plan_Owner");
		String Soeid = session.getUserReference().getUserId();
		
		List<String> plans = new ArrayList<String>();
			
			Repository repo = context.getAdaptationHome().getRepository();
			Adaptation metadataDataSet;
			try {
				metadataDataSet = EBXUtils.getMetadataDataSet(repo,"Rosetta", "Rosetta");
				final AdaptationTable targetTable = metadataDataSet.getTable(userPlanTablePath);
				
				String predicate = SoeID.format() + "= \"" + Soeid+"\"";
				
				RequestResult reqRes = targetTable.createRequestResult(predicate);
				for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
					String planID =  record.getString(Path.SELF.add(PLAN_ID_PATH));
					LOG.info("Plan  ID ::  "+ planID);
					plans.add(planID);
				}
			} catch (OperationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			
		
			final List<String> cblCode = (List<String>) vc.getValue(DSMTConstants.CBL_CD);
			final List<String> coCode = (List<String>) vc.getValue(DSMTConstants.CO_CD);
			
			final List<String> mleCode = (List<String>) vc.getValue(DSMTConstants.MLE_CD);
			final List<String> gfCode = (List<String>) vc.getValue(DSMTConstants.GF_CD);
			
			if(session.getDirectory().isUserInRole(user, role)){
		
				if(cblCode.size() < 1){
					List<String> dCBL = new ArrayList<String>();
					dCBL.add("NO_CBL");
					vc.setValue(dCBL, DSMTConstants.CBL_CD);
				}else if(!cblCode.contains("NO_CBL") && (!plans.containsAll(cblCode))){
					throw OperationException.createError(
							"You are not allowed to created record for one of the Plan " + cblCode.toString());
				}
				if(coCode.size()<1){
					List<String> dCO = new ArrayList<String>();
					dCO.add("NO_CO");
					vc.setValue(dCO, DSMTConstants.CO_CD);
				}else if(!coCode.contains("NO_CO") && (!plans.containsAll(coCode))){
					throw OperationException.createError(
							"You are not allowed to created record for one of the Plan " + coCode.toString());
				}
				
				if(mleCode.size()<1){
					List<String> dMLE = new ArrayList<String>();
					dMLE.add("NO_MLE");
					vc.setValue(dMLE, DSMTConstants.MLE_CD);
				}else if(!mleCode.contains("NO_MLE") && (!plans.containsAll(mleCode))){
					throw OperationException.createError(
							"You are not allowed to created record for one of the Plan " + mleCode.toString());
				}
				if(gfCode.size()<1){
					List<String> dGF = new ArrayList<String>();
					dGF.add("NO_GF");
					vc.setValue(dGF, DSMTConstants.GF_CD);
				}else if(!gfCode.contains("NO_GF") && (!plans.containsAll(gfCode))){
					throw OperationException.createError(
							"You are not allowed to created record for one of the Plan " + gfCode.toString());
				}
			
			}else{
				if(cblCode.size() < 1){
					List<String> dCBL = new ArrayList<String>();
					dCBL.add("NO_CBL");
					vc.setValue(dCBL, DSMTConstants.CBL_CD);
				}
				if(coCode.size()<1){
					List<String> dCO = new ArrayList<String>();
					dCO.add("NO_CO");
					vc.setValue(dCO, DSMTConstants.CO_CD);
				}
				
				if(mleCode.size()<1){
					List<String> dMLE = new ArrayList<String>();
					dMLE.add("NO_MLE");
					vc.setValue(dMLE, DSMTConstants.MLE_CD);
				}
				if(gfCode.size()<1){
					List<String> dGF = new ArrayList<String>();
					dGF.add("NO_GF");
					vc.setValue(dGF, DSMTConstants.GF_CD);
				}
			}
			super.handleBeforeModify(context);
	}
}
