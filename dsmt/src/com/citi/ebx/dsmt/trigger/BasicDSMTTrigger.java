package com.citi.ebx.dsmt.trigger;

import java.util.Calendar;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.onwbp.adaptation.UnavailableContentError;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class BasicDSMTTrigger extends AuditColumnsTrigger {
	private Path effDateFieldPath = DSMTConstants.effDateFieldPath;

	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context) throws OperationException {

		DSMTUtils.handleBeforeDelete(context);

	}

	public void handleNewContext(NewTransientOccurrenceContext context) {

		try {
			Date date = new Date();
			final Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = 01;
			cal.set(Calendar.MONTH, month);
			cal.set(Calendar.DAY_OF_MONTH, day);
			cal.set(Calendar.YEAR, year);
			context.getOccurrenceContextForUpdate().setValue(cal.getTime(), effDateFieldPath);

			super.handleNewContext(context);

		} catch (UnavailableContentError e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		}

		catch (PathAccessException e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		}
	}

	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException {

		context.setAllPrivileges();

		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();

		validateEffectiveDate(vc);

		super.handleBeforeCreate(context);
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context) throws OperationException {
		context.setAllPrivileges();

		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		validateEffectiveDate(vc);
		super.handleBeforeModify(context);
	}

	private void validateEffectiveDate(ValueContextForUpdate vc) {
		Date userDate = (Date) vc.getValue(effDateFieldPath);
		// Today's date
		Date date = new Date();
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = 01;
		// UserInput date
		final Calendar userCal = Calendar.getInstance();
		userCal.setTime(userDate);
		int uYear = userCal.get(Calendar.YEAR);
		int uMonth = userCal.get(Calendar.MONTH);
		boolean setThisMonthDate = true;
		/*
		 * in case the user input date is in future make it the first day of
		 * that month if its in the past make it to first day of the current
		 * month
		 */
		if (uYear > year) {

			setThisMonthDate = false;

		} else if (uYear == year) {
			if (uMonth > month) {
				setThisMonthDate = false;
			}
		}
		if (setThisMonthDate) {

			cal.set(Calendar.DAY_OF_MONTH, day);
			vc.setValue(cal.getTime(), effDateFieldPath);
		} else {
			userCal.set(Calendar.DAY_OF_MONTH, day);
			vc.setValue(userCal.getTime(), effDateFieldPath);
		}

	}

}