package com.citi.ebx.dsmt.trigger;

import java.util.Calendar;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.adaptation.RequestSortCriteria;
import com.onwbp.adaptation.UnavailableContentError;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class EndDateTrigger extends TableTrigger {

	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	private Path effDateFieldPath = DSMTConstants.effDateFieldPath;
	private Path endDateFieldPath = DSMTConstants.endDateFieldPath;
	private Path statusFieldPath = DSMTConstants.statusFieldPath;

	private RequestSortCriteria sortByEffDate = new RequestSortCriteria();

	
	public String getEffDateFieldPath() {
		return this.effDateFieldPath.format();
	}

	public String getStatusFieldPath() {
		return this.statusFieldPath.format();
	}

	public void setEffDateFieldPath(String effDateFieldPath) {
		this.effDateFieldPath = Path.parse(effDateFieldPath);
		this.sortByEffDate = new RequestSortCriteria();
		this.sortByEffDate.add(this.effDateFieldPath, false);
	}

	public String getEndDateFieldPath() {
		return this.endDateFieldPath.format();
	}

	public void setEndDateFieldPath(String endDateFieldPath) {
		this.endDateFieldPath = Path.parse(endDateFieldPath);
	}

	@Override
	public void setup(TriggerSetupContext context) {
		SchemaNode node = context.getSchemaNode();
	
		if (!node.isTableNode() && !node.isTableOccurrenceNode())
			context.addError("Table trigger " + this.getClass().getName()
					+ " applied to non table.");
		if (node.getNode(effDateFieldPath) == null)
			context.addError("Effective date path " + this.effDateFieldPath
					+ " not found on table.");
		if (node.getNode(endDateFieldPath) == null)
			context.addError("End date path " + this.endDateFieldPath
					+ " not found on table.");

		sortByEffDate = new RequestSortCriteria();
		sortByEffDate.add(this.effDateFieldPath, false);
	}

	// There is no static way to initialize to 12-31-9999, use long value
	private static Date lastEndDate = new Date(253402232400000L);

	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		final Adaptation newRec = context.getAdaptationOccurrence();
		final AdaptationTable aTab = newRec.getContainerTable();

		final ProcedureContext pContext = context.getProcedureContext();
		final ValueContextForUpdate vc = pContext.getContext(newRec
				.getAdaptationName());

		Adaptation previous = null;
		Date newRecEnd = lastEndDate;
		final Date newRecEff = newRec.getDate(effDateFieldPath);

		// Search for record we occur after
		try {
			// String searchPred = "date-less-than("+ effDateFieldPath.format()
			// + ", " + vc.getValue(effDateFieldPath) + ")";
			String searchPred = null;
			for (Path p : aTab.getPrimaryKeySpec()) {
				String path = "." + p.format();
				// Copy vc primary key fields except effective date
				if (path.equals(effDateFieldPath.format()))
					continue;
				// Note this does not handle values with '
				if (searchPred == null)
					searchPred = "(" + path + " = '" + vc.getValue(p) + "')";
				else
					searchPred = searchPred.concat(" and (" + path + " = '"
							+ vc.getValue(p) + "')");
			}
			RequestResult res = aTab.createRequestResult(searchPred,
					sortByEffDate);
			Adaptation rec = res.nextAdaptation();

			// Scan for records that take place after the new record
			while (rec != null) {
				Date recStart = rec.getDate(effDateFieldPath);
				if (recStart.after(newRecEff))
					// Rec starts after newRec, adjust endDate
					newRecEnd = recStart;
				else if (recStart.before(newRecEff)) {
					// Rec starts before newRec
					break;
				}
				rec = res.nextAdaptation();
			}
			previous = rec;

		} catch (Exception e) {
			// Previous not found
			LOG.error(e.getMessage());
			// System.out.println("No others found.");
		}

		pContext.setAllPrivileges(true);
		vc.setValue(newRecEnd, endDateFieldPath);
		pContext.doModifyContent(newRec, vc);

		// Update existing record to end when this record begins
		if (previous != null) {
			final ValueContextForUpdate vcPrev = pContext.getContext(previous
					.getAdaptationName());
			vcPrev.setValue(vc.getValue(effDateFieldPath), endDateFieldPath);
			pContext.doModifyContent(previous, vcPrev);
		}
		pContext.setAllPrivileges(false);

		super.handleAfterCreate(context);
	}

	public void handleNewContext(NewTransientOccurrenceContext context) {

		try {
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = 01;
			cal.set(Calendar.MONTH, month);
			cal.set(Calendar.DAY_OF_MONTH, day);
			cal.set(Calendar.YEAR, year);
			context.getOccurrenceContextForUpdate().setValue(cal.getTime(),
					effDateFieldPath);

			super.handleNewContext(context);

		} catch (UnavailableContentError e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		}

		catch (PathAccessException e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		}
	}

	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
	
		DSMTUtils.handleBeforeDelete(context);
	}
}