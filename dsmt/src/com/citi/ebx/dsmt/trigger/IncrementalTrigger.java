package com.citi.ebx.dsmt.trigger;

import com.citi.ebx.goc.trigger.GOCSubtableTrigger;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class IncrementalTrigger extends GOCSubtableTrigger {
	
	private Path actionRequried = Path.parse("./ACTION_REQ");
	private Path statusFieldPath = Path.parse("./EFF_STATUS");
	
	

	@Override
	public void setup(TriggerSetupContext context) {
		SchemaNode node = context.getSchemaNode();
		if (node.getNode(actionRequried) == null)
			context.addError("Creation user field path "
					+ this.actionRequried + " not found on table.");
		
		super.setup(context);
	}
	
	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		
		final Adaptation newRec = context.getAdaptationOccurrence();
		
		final ProcedureContext pContext = context.getProcedureContext();
		final ValueContextForUpdate vc = pContext.getContext(newRec
				.getAdaptationName());
		vc.setValue("A", actionRequried);
		pContext.doModifyContent(newRec, vc);
		 super.handleAfterCreate(context);
	}
	
	
	
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		final ValueContextForUpdate vc = context
				.getOccurrenceContextForUpdate();
		
		
		 if(null==context.getChanges().getChange(statusFieldPath)){
			 vc.setValue("U", actionRequried);
		 }
		 else{
				 String originalValue =(String) context.getChanges().getChange(statusFieldPath).getValueBefore();
				 String changedValue =(String) context.getChanges().getChange(statusFieldPath).getValueAfter();
				 
				 if(originalValue.equals(changedValue)){
					 vc.setValue("U", actionRequried);
				 }
				 else if(changedValue.equals("I")){
				 vc.setValue("D", actionRequried);
				 }else{
					 vc.setValue("U", actionRequried); 
				 }
			 
		 }
		
		super.handleBeforeModify(context);
		
		
	}
	/*
	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context)
			throws OperationException {
		final Adaptation newRec = context.getAdaptationOccurrence();
		
		final ProcedureContext pContext = context.getProcedureContext();
		final ValueContextForUpdate vc = pContext.getContext(newRec
				.getAdaptationName());
		System.out.println("start of IncrementalTrigger-->handleAfterModify ");
		 if(null==context.getChanges().getChange(statusFieldPath)){
			 vc.setValue("C", actionRequried);
		 }
		 else{
				 String originalValue =(String) context.getChanges().getChange(statusFieldPath).getValueBefore();
				 String changedValue =(String) context.getChanges().getChange(statusFieldPath).getValueAfter();
				 System.out.println("inside handleAfterModify originalValue"+originalValue + " chnagevalue "+ originalValue);
				 if(originalValue.equals(changedValue)){
					 vc.setValue("C", actionRequried);
				 }
				 else if(changedValue.equals("I")){
				 vc.setValue("D", actionRequried);
				 }else{
					 vc.setValue("C", actionRequried); 
				 }			 
		 }
		 
		 pContext.doModifyContent(newRec,  vc);
		 System.out.println("end of IncrementalTrigger-->handleAfterModify");
		 super.handleAfterModify(context);;
		 
	}
*/
}
