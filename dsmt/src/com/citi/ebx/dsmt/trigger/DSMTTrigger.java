package com.citi.ebx.dsmt.trigger;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class DSMTTrigger extends BasicDSMTTrigger {
	
	private Path setId= DSMTConstants.setId;
	

	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException {
		
		context.setAllPrivileges();
		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		String uSetId=(String)vc.getValue(setId);
		if(null==uSetId ||"".equals(uSetId)){
			vc.setValue("PMF01", setId);
		}
		
		super.handleBeforeCreate(context);
	}
	
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context) throws OperationException {
		context.setAllPrivileges();

		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		String uSetId=(String)vc.getValue(setId);
		if(null==uSetId ||"".equals(uSetId)){
			vc.setValue("PMF01", setId);
		}
		
		super.handleBeforeModify(context);
	}

}
