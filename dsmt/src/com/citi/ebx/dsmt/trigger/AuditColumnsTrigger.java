package com.citi.ebx.dsmt.trigger;

import java.util.Calendar;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class AuditColumnsTrigger extends TableTrigger {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	private String enableDeletionCheck = "N";
	
	private Path creationUserFieldPath = DSMTConstants.creationUserFieldPath;
	private Path creationDateFieldPath = DSMTConstants.creationDateFieldPath;
	private Path lastUpdateUserFieldPath = DSMTConstants.lastUpdateUserFieldPath;
	private Path lastUpdateDateFieldPath = DSMTConstants.lastUpdateDateFieldPath;
	// private Path statusFieldPath = DSMTConstants.statusFieldPath;
	
	public String getCreationUserFieldPath() {
		return this.creationUserFieldPath.format();
	}

	public void setCreationUserFieldPath(String creationUserFieldPath) {
		this.creationUserFieldPath = Path.parse(creationUserFieldPath);
	}

	public String getCreationDateFieldPath() {
		return creationDateFieldPath.format();
	}

	public void setCreationDateFieldPath(String creationDateFieldPath) {
		this.creationDateFieldPath = Path.parse(creationDateFieldPath);
	}

	public String getLastUpdateUserFieldPath() {
		return lastUpdateUserFieldPath.format();
	}

	public void setLastUpdateUserFieldPath(String lastUpdateUserFieldPath) {
		this.lastUpdateUserFieldPath = Path.parse(lastUpdateUserFieldPath);
	}

	public String getLastUpdateDateFieldPath() {
		return lastUpdateDateFieldPath.format();
	}

	public void setLastUpdateDateFieldPath(String lastUpdateDateFieldPath) {
		this.lastUpdateDateFieldPath = Path.parse(lastUpdateDateFieldPath);
	}


	@Override
	public void setup(TriggerSetupContext context) {
		SchemaNode node = context.getSchemaNode();
		if (!node.isTableNode() && !node.isTableOccurrenceNode())
			context.addError("Table trigger " + this.getClass().getName()
					+ " applied to non table.");
		
		if (node.getNode(creationUserFieldPath) == null)
			context.addError("Creation user field path " + this.creationUserFieldPath + " not found on table.");
		if (node.getNode(lastUpdateUserFieldPath) == null)
			context.addError("Last update field path " + this.lastUpdateUserFieldPath + " not found on table.");
		
		if (node.getNode(creationDateFieldPath) == null)
			context.addError("Creation Date field path " + this.creationDateFieldPath + " not found on table.");
		if (node.getNode(lastUpdateDateFieldPath) == null)
			context.addError("Last update date field path " + this.lastUpdateDateFieldPath + " not found on table.");
		
	}

	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		// System.out.println("ENTERED handlebeforeCreate");
		context.setAllPrivileges();

		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		String userID = context.getSession().getUserReference().getUserId();

		/** Capitalize the userID */
		if (null != userID) {
			userID = userID.toUpperCase();
		}

		vc.setValue(userID, creationUserFieldPath);
		vc.setValue(userID, lastUpdateUserFieldPath);
		Date now = Calendar.getInstance().getTime();
		vc.setValue(now, creationDateFieldPath);
		vc.setValue(now, lastUpdateDateFieldPath);

		
		super.handleBeforeCreate(context);
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		context.setAllPrivileges();

		final ValueContextForUpdate vc = context
				.getOccurrenceContextForUpdate();

		// checkIfParentIsALeaf(vc);

		String userID = context.getSession().getUserReference().getUserId();

		/** Capitalize the userID */
		if (null != userID) {
			userID = userID.toUpperCase();
		}

		vc.setValue(userID, lastUpdateUserFieldPath);
		vc.setValue(new Date(), lastUpdateDateFieldPath);

		super.handleBeforeModify(context);
	}

	
	
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
	throws OperationException {

		if("Y".equalsIgnoreCase(enableDeletionCheck)){
			
			DSMTUtils.handleBeforeDelete(context);
		}
	}
	
	public void setEnableDeletionCheck(String enableDeletionCheck) {
		this.enableDeletionCheck = enableDeletionCheck;
	}

	public String getEnableDeletionCheck() {
		return enableDeletionCheck;
	}


	

}