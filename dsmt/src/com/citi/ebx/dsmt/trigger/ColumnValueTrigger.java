package com.citi.ebx.dsmt.trigger;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class ColumnValueTrigger extends AuditColumnsTrigger {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	public String getColumnID() {
		return columnID;
	}


	public void setColumnID(String columnID) {
		this.columnID = columnID;
	}


	public String getAllowedValueForDelete() {
		return allowedValueForDelete;
	}


	public void setAllowedValueForDelete(String allowedValueForDelete) {
		this.allowedValueForDelete = allowedValueForDelete;
	}


	private String columnID = "./EFF_STATUS";
	private String allowedValueForDelete = "A" ;
	 
	public void setup(TriggerSetupContext context) {
		
		SchemaNode node = context.getSchemaNode();
		if (!node.isTableNode() && !node.isTableOccurrenceNode())
			context.addError("Table trigger " + this.getClass().getName()
					+ " applied to non table.");
	}
	
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
	
			/** Get user ID from current user session */
			String userID = context.getSession().getUserReference().getUserId();
			
			boolean isTestEnvironment = ! "prod".equalsIgnoreCase(environment);
			
			/** allow only admin to delete a record in Active status, only allow rs74652 to test on dev, sit and uat but not on Prod */
			final boolean isAdministrator = DSMTConstants.ADMIN_USER.equalsIgnoreCase(userID) || (isTestEnvironment && "rs74652".equalsIgnoreCase(userID));
			 
			
			final Path column = Path.parse(columnID);
			
			final String columnValue = context.getAdaptationOccurrence().getString(
					column);
		
			
			final String label = columnID.replace("./", "");
			//TODO find the label of the column
			
			/** if the value is not allowed and you are not an administrator then throw an error.*/
			if (! allowedValueForDelete.equalsIgnoreCase(columnValue) && ! isAdministrator ) {
				throw OperationException.createError("Column :" + label  + " with value other than " + allowedValueForDelete + " cannot be deleted");
				
				
				}
	
	}

	private static final String environment = new DSMTPropertyHelper().getProp(DSMTConstants.DSMT_ENVIRONMENT);
	
}