package com.citi.ebx.dsmt.trigger;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.ValueContextForUpdate;

public class ManualTaggingTrigger extends HierarchyTrigger{
	
	private static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path userPlanTablePath = Path.parse("/root/ENTITLEMENTS/USER_PLANS");
	private Path SoeID = Path.parse("./SOEID");
	private static final Path PLAN_ID_PATH = Path.parse("./PLAN_ID");
	
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException{
	
			
		
	final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
	Session session = context.getSession();
	UserReference user=session.getUserReference();
	Role role=Role.forSpecificRole("Rosetta_Plan_Owner");
	String Soeid = session.getUserReference().getUserId();
	
	final String cblCode = (String) vc.getValue(DSMTConstants.CBL_CD);
	final String coCode = (String) vc.getValue(DSMTConstants.CO_CD);
	final String mleCode = (String) vc.getValue(DSMTConstants.MLE_CD);
	final String gfCode = (String) vc.getValue(DSMTConstants.GF_CD);
	
	
	List<String> plans = new ArrayList<String>();
	
	Repository repo = context.getAdaptationHome().getRepository();
	Adaptation metadataDataSet;
	try {
		metadataDataSet = EBXUtils.getMetadataDataSet(repo,"Rosetta", "Rosetta");
		final AdaptationTable targetTable = metadataDataSet.getTable(userPlanTablePath);
		
		String predicate = SoeID.format() + "= \"" + Soeid+"\"";
		
		RequestResult reqRes = targetTable.createRequestResult(predicate);
		for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
			String planID =  record.getString(Path.SELF.add(PLAN_ID_PATH));
			LOG.info("Plan  ID ::  "+ planID);
			plans.add(planID);
		}
	} catch (OperationException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	SchemaNode cblNode = vc.getNode(DSMTConstants.CBL_CD);
	SchemaNode coNode = vc.getNode(DSMTConstants.CO_CD);
	SchemaNode mleNode = vc.getNode(DSMTConstants.MLE_CD);
	SchemaNode gfNode = vc.getNode(DSMTConstants.GF_CD);
		if(cblNode == null){
			LOG.info("cbl node not present");
		}
		if(coNode == null){
			LOG.info("co node not present");
		}
		if(mleNode == null){
			LOG.info("mle node not present");
		}
		if(gfNode == null){
			LOG.info("gf node not present");
		}
		
		if(session.getDirectory().isUserInRole(user, role)){
		
			if(StringUtils.isBlank(cblCode)){
				vc.setValue("NO_CBL", DSMTConstants.CBL_CD);
			}else if(!"NO_CBL".equalsIgnoreCase(cblCode) && (! plans.contains(cblCode)) ) {
				throw OperationException.createError(
						"You are not allowed to created record for Plan " + cblCode);
			}
			if(StringUtils.isBlank(coCode)){
				vc.setValue("NO_CO", DSMTConstants.CO_CD);
			}else if(!"NO_CO".equalsIgnoreCase(coCode)&& (! plans.contains(coCode))){
				throw OperationException.createError(
						"You are not allowed to created record for Plan " + coCode);
			}
			if(StringUtils.isBlank(mleCode)){
				vc.setValue("NO_MLE", DSMTConstants.MLE_CD);
			}else if(!"NO_MLE".equalsIgnoreCase(mleCode) && (! plans.contains(mleCode))){
				throw OperationException.createError(
						"You are not allowed to created record for Plan " + mleCode);
			}
			if(StringUtils.isBlank(gfCode)){
				vc.setValue("NO_GF", DSMTConstants.GF_CD);
			}else if(!"NO_GF".equalsIgnoreCase(gfCode) && (! plans.contains(gfCode))){
				throw OperationException.createError(
						"You are not allowed to created record for Plan " + gfCode);
			}
		}
		else{
			if(StringUtils.isBlank(cblCode)){
				vc.setValue("NO_CBL", DSMTConstants.CBL_CD);
			}
			if(StringUtils.isBlank(coCode)){
				vc.setValue("NO_CO", DSMTConstants.CO_CD);
			}
			if(StringUtils.isBlank(mleCode)){
				vc.setValue("NO_MLE", DSMTConstants.MLE_CD);
			}
			if(StringUtils.isBlank(gfCode)){
				vc.setValue("NO_GF", DSMTConstants.GF_CD);
			}
		}
		
		super.handleBeforeCreate(context);
		
	}
	
	
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		
		
		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		Session session = context.getSession();
		UserReference user=session.getUserReference();
		Role role=Role.forSpecificRole("Rosetta_Plan_Owner");
		String Soeid = session.getUserReference().getUserId();
		
		final String cblCode = (String) vc.getValue(DSMTConstants.CBL_CD);
		final String coCode = (String) vc.getValue(DSMTConstants.CO_CD);
		final String mleCode = (String) vc.getValue(DSMTConstants.MLE_CD);
		final String gfCode = (String) vc.getValue(DSMTConstants.GF_CD);
		
		
		List<String> plans = new ArrayList<String>();
		
		Repository repo = context.getAdaptationHome().getRepository();
		Adaptation metadataDataSet;
		try {
			metadataDataSet = EBXUtils.getMetadataDataSet(repo,"Rosetta", "Rosetta");
			final AdaptationTable targetTable = metadataDataSet.getTable(userPlanTablePath);
			
			String predicate = SoeID.format() + "= \"" + Soeid+"\"";
			
			RequestResult reqRes = targetTable.createRequestResult(predicate);
			for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
				String planID =  record.getString(Path.SELF.add(PLAN_ID_PATH));
				LOG.info("Plan  ID ::  "+ planID);
				plans.add(planID);
			}
		} catch (OperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		SchemaNode cblNode = vc.getNode(DSMTConstants.CBL_CD);
		SchemaNode coNode = vc.getNode(DSMTConstants.CO_CD);
		SchemaNode mleNode = vc.getNode(DSMTConstants.MLE_CD);
		SchemaNode gfNode = vc.getNode(DSMTConstants.GF_CD);
			if(cblNode == null){
				LOG.info("cbl node not present");
			}
			if(coNode == null){
				LOG.info("co node not present");
			}
			if(mleNode == null){
				LOG.info("mle node not present");
			}
			if(gfNode == null){
				LOG.info("gf node not present");
			}
			
			if(session.getDirectory().isUserInRole(user, role)){
			
				if(StringUtils.isBlank(cblCode)){
					vc.setValue("NO_CBL", DSMTConstants.CBL_CD);
				}else if(!"NO_CBL".equalsIgnoreCase(cblCode) && (! plans.contains(cblCode)) ) {
					throw OperationException.createError(
							"You are not allowed to created record for Plan " + cblCode);
				}
				if(StringUtils.isBlank(coCode)){
					vc.setValue("NO_CO", DSMTConstants.CO_CD);
				}else if(!"NO_CO".equalsIgnoreCase(coCode)&& (! plans.contains(coCode))){
					throw OperationException.createError(
							"You are not allowed to created record for Plan " + coCode);
				}
				if(StringUtils.isBlank(mleCode)){
					vc.setValue("NO_MLE", DSMTConstants.MLE_CD);
				}else if(!"NO_MLE".equalsIgnoreCase(mleCode) && (! plans.contains(mleCode))){
					throw OperationException.createError(
							"You are not allowed to created record for Plan " + mleCode);
				}
				if(StringUtils.isBlank(gfCode)){
					vc.setValue("NO_GF", DSMTConstants.GF_CD);
				}else if(!"NO_GF".equalsIgnoreCase(gfCode) && (! plans.contains(gfCode))){
					throw OperationException.createError(
							"You are not allowed to created record for Plan " + gfCode);
				}
			}
			else{
				if(StringUtils.isBlank(cblCode)){
					vc.setValue("NO_CBL", DSMTConstants.CBL_CD);
				}
				if(StringUtils.isBlank(coCode)){
					vc.setValue("NO_CO", DSMTConstants.CO_CD);
				}
				if(StringUtils.isBlank(mleCode)){
					vc.setValue("NO_MLE", DSMTConstants.MLE_CD);
				}
				if(StringUtils.isBlank(gfCode)){
					vc.setValue("NO_GF", DSMTConstants.GF_CD);
				}
			}
			

		super.handleBeforeModify(context);
	}
	
/*	final SimpleDateFormat dateFormat = new SimpleDateFormat(DSMTConstants.DEFAULT_DATE_FORMAT);
	
	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context)
			throws OperationException {
			final Adaptation newRec = context.getAdaptationOccurrence();
			final Date endDate = newRec.getDate(Path.parse(endDateField));
			String dateStr = dateFormat.format(endDate);
			
			if (endDate == null || MAX_END_DATE.equals(dateStr)) {
				PurgeUtil purgeUtil = new PurgeUtil(newRec ,context.getAdaptationHome().getRepository() ,tablePath,parentFk,columnId );
			Utils.executeProcedure(purgeUtil, context.getSession(), context.getAdaptationHome().getRepository().lookupHome(
			HomeKey.forBranchName(DSMTConstants.GOC)));
			}
			 
			 super.handleAfterModify(context);
			
		}
	*/

}
