package com.citi.ebx.dsmt.trigger;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.TableRecordPrcodeureUtil;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;

public class ManagerHierarchyTrigger extends TableTrigger {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	@Override
	public void setup(TriggerSetupContext arg0) {
		// TODO Auto-generated method stub
		LOG.info("in setup()");
	}
	
	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		final Adaptation newRec = context.getAdaptationOccurrence();
		final ProcedureContext pContext = context.getProcedureContext();
		final ValueContextForUpdate vc = pContext.getContext(newRec.getAdaptationName());
		String status = vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._C_GDW_EMP_STATUS).toString();
		if(status.equals("A")) {
			Session session=context.getSession();
			Repository repo = context.getAdaptationHome().getRepository();
			AdaptationHome accMatrixDataSpace = repo.lookupHome(
	                HomeKey.forBranchName(AccountibilityMatrixConstants.DATASPACE_Accounting_Matrix));
			Adaptation accMatrixDataSet = EBXUtils.getMetadataDataSet(repo, AccountibilityMatrixConstants.DATASPACE_Accounting_Matrix,AccountibilityMatrixConstants.DATASET_Accounting_Matrix);
	        AdaptationTable mngrTable = accMatrixDataSet.getTable(Path.parse("/root/ACC_MAT/C_DSMT_MANAGER"));
	        ValueContextForUpdate vcTarget = pContext.getContextForNewOccurrence(mngrTable);
			vcTarget = setTargetValueContext(vc,vcTarget);
	        Adaptation accMatRecord =  null;
			manipulateTableProc(session,accMatrixDataSpace,vcTarget,mngrTable,accMatRecord,"create");
		}
	}
	public void handleAfterModify(AfterModifyOccurrenceContext context) throws OperationException {
		AdaptationHome dataSpace = context.getAdaptationHome();
		AdaptationHome accMatrixDataSpace = dataSpace.getRepository().lookupHome(
                HomeKey.forBranchName(AccountibilityMatrixConstants.DATASPACE_Accounting_Matrix));
        Adaptation accMatrixDataSet = accMatrixDataSpace.findAdaptationOrNull(AdaptationName.forName(AccountibilityMatrixConstants.DATASET_Accounting_Matrix));
        AdaptationTable mngrTable = accMatrixDataSet.getTable(Path.parse("/root/ACC_MAT/C_DSMT_MANAGER"));
        
		final Adaptation newRec = context.getAdaptationOccurrence();
		final ProcedureContext pContext = context.getProcedureContext();
		final ValueContextForUpdate vc = pContext.getContext(newRec.getAdaptationName());
		ValueContextForUpdate vcTarget = pContext.getContextForNewOccurrence(mngrTable);
		
		String status = vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._C_GDW_EMP_STATUS).toString();
		String mgrId = newRec.getOccurrencePrimaryKey().format();
		String predicate = AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._MANAGER_ID.format() + "='"+mgrId+"'";
		RequestResult rs = mngrTable.createRequestResult(predicate);
		Adaptation accMatRecord =  null;
		if(rs.getSize() >0) 
			accMatRecord = rs.nextAdaptation();
		Session session=context.getSession();
		if(status.equalsIgnoreCase("A")) {			
			vcTarget = setTargetValueContext(vc,vcTarget);
			if(null != accMatRecord) 
				manipulateTableProc(session,accMatrixDataSpace,vcTarget,mngrTable,accMatRecord,"update");
			else 
				manipulateTableProc(session,accMatrixDataSpace,vcTarget,mngrTable,accMatRecord,"create");
		}
		else{
			if(null != accMatRecord)
				manipulateTableProc(session,accMatrixDataSpace,vcTarget,mngrTable,accMatRecord,"delete");
		}
	}
	public ValueContextForUpdate setTargetValueContext(ValueContextForUpdate vc, ValueContextForUpdate vcTarget) {
		vcTarget.setValue(vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._SETID),AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._SETID);
		vcTarget.setValue(vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._MANAGER_ID),AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._MANAGER_ID);
		vcTarget.setValue(vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._C_GDW_EMP_NAME),AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._C_GDW_EMP_NAME);
		vcTarget.setValue(vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._C_GDW_EMP_STATUS),AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._C_GDW_EMP_STATUS);
		vcTarget.setValue(vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._C_GDW_TERM_DATE),AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._C_GDW_TERM_DATE);
		vcTarget.setValue(vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._C_GDW_EMP_TYPCD),AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._C_GDW_EMP_TYPCD);
		vcTarget.setValue(vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._EFFDT),AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._EFFDT);
		vcTarget.setValue(vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._CREATED_OPERID),AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._CREATED_OPERID);
		vcTarget.setValue(vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._ORIG_DTTM),AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._ORIG_DTTM);
		vcTarget.setValue(vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._LASTUPDOPRID),AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._LASTUPDOPRID);
		vcTarget.setValue(vc.getValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._CHNG_DTTM),AccountibilityMatrixPaths._ACC_MAT_C_DSMT_MANAGER._CHNG_DTTM);
		return vcTarget;
	}
	public void manipulateTableProc(Session session,AdaptationHome accMatrixDataSpace, ValueContextForUpdate vcTarget,AdaptationTable table,Adaptation record,String operation){
		TableRecordPrcodeureUtil service = null;
		ProgrammaticService svc =  null;
		try{
			service = new TableRecordPrcodeureUtil(vcTarget,table,record,operation); 
			svc = ProgrammaticService.createForSession(session, accMatrixDataSpace);
			ProcedureResult procResult  = svc.execute(service);
		}
		catch(Exception e) {
			LOG.error("Exception from ManagerHeirarchyTrigger:::"+e);e.printStackTrace();
		}
	}
}
