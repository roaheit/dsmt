package com.citi.ebx.dsmt.trigger;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.PurgeUtil;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class NewHierarchyTrigger extends HierarchyTrigger{
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	public String tablePath ;
	public String columnId ;
	public String description;
	public String description2;
	public String status;
	public String effDate;
	public String param1;
	public String param2;
	public String param3;
	public String param4;
	public String parentFk;
	private static final String MAX_END_DATE = "9999-12-31";
	private String endDateField = "./ENDDT";
	
	
	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		
		final Adaptation newRec = context.getAdaptationOccurrence();
		//final AdaptationTable aTab = newRec.getContainerTable();
		LOG.debug("inside newhierarchy triger");

		
		
		try{
				PurgeUtil purgeUtil = new PurgeUtil(newRec,context.getAdaptationHome().getRepository() ,tablePath,parentFk,columnId );
				Utils.executeProcedure(purgeUtil, context.getSession(), context.getAdaptationHome().getRepository().lookupHome(
				HomeKey.forBranchName(DSMTConstants.GOC)));
				
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		 
		 super.handleAfterCreate(context);
		
	}
	
	final SimpleDateFormat dateFormat = new SimpleDateFormat(DSMTConstants.DEFAULT_DATE_FORMAT);
	
	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context)
			throws OperationException {
			final Adaptation newRec = context.getAdaptationOccurrence();
			final Date endDate = newRec.getDate(Path.parse(endDateField));
			String dateStr = dateFormat.format(endDate);
			
			if (endDate == null || MAX_END_DATE.equals(dateStr)) {
				PurgeUtil purgeUtil = new PurgeUtil(newRec ,context.getAdaptationHome().getRepository() ,tablePath,parentFk,columnId );
			Utils.executeProcedure(purgeUtil, context.getSession(), context.getAdaptationHome().getRepository().lookupHome(
			HomeKey.forBranchName(DSMTConstants.GOC)));
			}
			 
			 super.handleAfterModify(context);
			
		}
	
	
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
			//System.out.println("call pureg");
		
			//context.getSession().setAttribute(DSMTConstants.
			//	DELETED_RECORD_PARAM, context.getAdaptationOccurrence());	
				/*PurgeUtil purgeUtil = new PurgeUtil(context.getAdaptationOccurrence() ,context.getAdaptationHome().getRepository() ,tablePath,parentFk,columnId );
				Utils.executeProcedure(purgeUtil, context.getSession(), context.getAdaptationHome().getRepository().lookupHome(
				HomeKey.forBranchName(DSMTConstants.ARCHIVE_DATA_SPACE_NAME)));*/
		
	}
	
	/*
	public void handleAfterDelete(AfterDeleteOccurrenceContext context)
			throws OperationException {
		System.out.println("call pureg");
		String tableName = "/root/GLOBAL_STD/PMF_STD/F_CHANNEL/C_DSMT_CHANNEL";
			/*ValueContextForUpdate oldVC= context.getOccurrenceContextForUpdate() ;
			ProcedureContext pContext= context.getProcedureContext();
			ValueContext vc = context.getOccurrenceContext();
			Repository repo=  context.getAdaptationHome().getRepository();
			Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo, "TEMP", "Test");
			final AdaptationTable targetTable = metadataDataSet.getTable(getPath(tableName));
		
		
			pContext.setAllPrivileges(false);
			pContext.doCreateOccurrence(vc, targetTable);
			
			try
	        {
				
				
				 AdaptationHome dataSpace = context.getAdaptationHome();
		            // Don't want to do this if you're currently in the archive data space!
		            if (!DSMTConstants.ARCHIVE_DATA_SPACE_NAME.equals(dataSpace.getKey().getName()))
		            {
		                // Get the deleted record stored in the session
		                Adaptation deletedRecord = (Adaptation) context.getSession().getAttribute(
		                		DSMTConstants.DELETED_RECORD_PARAM);
		                
		                AdaptationHome archiveDataSpace = dataSpace.getRepository().lookupHome(
		                    HomeKey.forBranchName(DSMTConstants.ARCHIVE_DATA_SPACE_NAME));
		                Adaptation archiveDataSet = archiveDataSpace.findAdaptationOrNull(AdaptationName.forName(DSMTConstants.ARCHIVE_DATA_SET_NAME));
		                AdaptationTable archiveTable = archiveDataSet.getTable(getPath(tableName));
		                
		                // Use the same procedure context from this trigger. If all things happen in same
		                // procedure context, then if an error occurs they all get rolled back in same transaction.
		                ProcedureContext pContext = context.getProcedureContext();
		                
		                // Create a context for the new record and copy the existing record into it.
		                ValueContextForUpdate valueContext = pContext.getContextForNewOccurrence(
		                    deletedRecord, archiveTable);
		                
		                // If you want to modify the new record before creating it you can do so here
		                // valueContext.setValue(...)
		                
		                // Create the new record. This returns the new record which you can do something with,
		                // or ignore.
		                pContext.doCreateOccurrence(valueContext, archiveTable);
		            }

	        }
			
	        finally
	        {
	            // Clear the session variable
	            context.getSession().setAttribute(DSMTConstants.DELETED_RECORD_PARAM, null);
	        }


		
				
		
	}
	*/

	
	public void handleAfterDelete(AfterDeleteOccurrenceContext context)
			throws OperationException {
		Repository repo = context.getAdaptationHome().getRepository();
		final AdaptationHome gocDataSpace = repo.lookupHome(
				HomeKey.forBranchName(DSMTConstants.GOC));
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
				DSMTConstants.GOC, DSMTConstants.GOC);
		AdaptationTable targetTable = metadataDataSet
				.getTable(getPath(tablePath));
		
		final String rec = context.getDeletedRecordXPathExpression();
		String[] sToken = rec.split("\\'");
		//String mapId = null;
		//mapId = sToken[3];
		
		ProcedureContext procCtx = context.getProcedureContext();
		procCtx.setAllPrivileges(true);
		
		String effDatePredicate =  " date-equal(" + Path.parse(effDate).format() + ", '"+ sToken[5] + "')";
		
		final String predicate = Path.parse(columnId).format() + "='" + sToken[3] + "'" + " and " + effDatePredicate;
		
		
		LOG.info(" predicate  >>>>>>  "+ predicate);
		
		
		RequestResult resultSet =targetTable.createRequestResult(predicate);
		Adaptation record = null;

		if (resultSet != null&& resultSet.getSize()>0) {
			record = resultSet.nextAdaptation();
					
		// call procedure to udpate record
			final DeleteRecordFromChildTablesProcedure copyProc = new DeleteRecordFromChildTablesProcedure(targetTable, record);
			Utils.executeProcedure(copyProc, context.getSession(), gocDataSpace);
		
		}
		
		//super.handleAfterDelete(context);

	}
	private class DeleteRecordFromChildTablesProcedure implements Procedure {
		private AdaptationTable table;
		private Adaptation record;
		
		public DeleteRecordFromChildTablesProcedure(AdaptationTable table,Adaptation record) {
			this.table = table;
			this.record= record;
		}
		//GOCConstants.DSMT2_RELATIONAL_DATASET
		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			
			
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			pContext.doDelete(record.getAdaptationName(), false);
			
		}
	}
		


private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}

/**
 * @return the tablePath
 */
public String getTablePath() {
	return tablePath;
}



/**
 * @param tablePath the tablePath to set
 */
public void setTablePath(String tablePath) {
	this.tablePath = tablePath;
}



/**
 * @return the columnId
 */
public String getColumnId() {
	return columnId;
}



/**
 * @param columnId the columnId to set
 */
public void setColumnId(String columnId) {
	this.columnId = columnId;
}



/**
 * @return the description
 */
public String getDescription() {
	return description;
}



/**
 * @param description the description to set
 */
public void setDescription(String description) {
	this.description = description;
}



/**
 * @return the description2
 */
public String getDescription2() {
	return description2;
}



/**
 * @param description2 the description2 to set
 */
public void setDescription2(String description2) {
	this.description2 = description2;
}



/**
 * @return the status
 */
public String getStatus() {
	return status;
}



/**
 * @param status the status to set
 */
public void setStatus(String status) {
	this.status = status;
}



/**
 * @return the param1
 */
public String getParam1() {
	return param1;
}



/**
 * @param param1 the param1 to set
 */
public void setParam1(String param1) {
	this.param1 = param1;
}



/**
 * @return the param2
 */
public String getParam2() {
	return param2;
}



/**
 * @param param2 the param2 to set
 */
public void setParam2(String param2) {
	this.param2 = param2;
}



/**
 * @return the param3
 */
public String getParam3() {
	return param3;
}



/**
 * @param param3 the param3 to set
 */
public void setParam3(String param3) {
	this.param3 = param3;
}



/**
 * @return the param4
 */
public String getParam4() {
	return param4;
}



/**
 * @param param4 the param4 to set
 */
public void setParam4(String param4) {
	this.param4 = param4;
}



/**
 * @return the param5
 */
public String getParentFk() {
	return parentFk;
}



/**
 * @param param5 the param5 to set
 */
public void setParentFk(String parentFk) {
	this.parentFk = parentFk;
}



/**
 * @return the effDate
 */
public String getEffDate() {
	return effDate;
}



/**
 * @param effDate the effDate to set
 */
public void setEffDate(String effDate) {
	this.effDate = effDate;
}


}
