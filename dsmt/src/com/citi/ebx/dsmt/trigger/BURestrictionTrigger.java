package com.citi.ebx.dsmt.trigger;

import java.util.Arrays;
import java.util.List;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class BURestrictionTrigger extends TableTrigger {

    protected static final LoggingCategory LOG = LoggingCategory.getKernel();


    List<String> myList2 = Arrays.asList("LEADSPLIT", "SPLIT", "STANDALONE",
                                    "SHADOW");

    // private Path foreignKeyPath = Path.parse("./LASTUPDOPRID");


    @Override
    public void setup(TriggerSetupContext context) {}


    public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		final Adaptation newRec = context.getAdaptationOccurrence();

		final ProcedureContext pContext = context.getProcedureContext();
		final ValueContextForUpdate vc = pContext.getContext(newRec
				.getAdaptationName());
		String attributeID = newRec
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_ATTR_ID);
		String value = newRec
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_ATT_VAL_ID);
		String status = newRec
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._EFF_STATUS);

		if (status.equalsIgnoreCase(DSMTConstants.ACTIVE)
				&& ((attributeID.equalsIgnoreCase("CONDI_TYPE") && myList2
						.contains(value)) || (attributeID
						.equalsIgnoreCase("BU_TYPE") && !value
						.equalsIgnoreCase("AUTO-ELMN")))) {
			vc.setValue(
					"N",
					DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_BU_RES);
		} else {
			vc.setValue(
					"Y",
					DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_BU_RES);
		}

		pContext.setAllPrivileges(true);
		pContext.doModifyContent(newRec, vc);

		pContext.setAllPrivileges(false);

		super.handleAfterCreate(context);
	}


    @SuppressWarnings("deprecation")
    public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
                                    throws OperationException {
                    
    }

    @Override
    public void handleBeforeModify(BeforeModifyOccurrenceContext context)
                                    throws OperationException {
                    
    }

    /*
    * 
     * public void setForeignKeyPath(Path foreignKeyPath) { this.foreignKeyPath
    * = foreignKeyPath; }
    * 
     * public Path getForeignKeyPath() { return foreignKeyPath; }
    * 
     * public void checkIfParentIsALeaf(ValueContextForUpdate vc){
    * 
     * Adaptation currentRecord = vc.getAdaptationInstance(); String parentKey =
    * String.valueOf(currentRecord.get(foreignKeyPath));
    * 
     * System.out.println(parentKey);
    * 
     * 
     * }
    */
    public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
                                    throws OperationException {
    }

    public void handleAfterModify(AfterModifyOccurrenceContext aContext)
			throws OperationException {

		Adaptation newRec = aContext.getAdaptationOccurrence();
		final ProcedureContext pContext = aContext.getProcedureContext();

		final ValueContextForUpdate vc = pContext.getContext(newRec
				.getAdaptationName());

		String attributeID = newRec
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_ATTR_ID);
		String value = newRec
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_ATT_VAL_ID);
		String status = newRec
				.getString(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._EFF_STATUS);

		if (status.equalsIgnoreCase(DSMTConstants.ACTIVE)
				&& ((attributeID.equalsIgnoreCase("CONDI_TYPE") && myList2
						.contains(value)) || (attributeID
						.equalsIgnoreCase("BU_TYPE") && !value
						.equalsIgnoreCase("AUTO-ELMN")))) {
			vc.setValue(
					"N",
					DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_BU_RES);
		} else {
			vc.setValue(
					"Y",
					DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_BU_RES);
		}
		try {
			pContext.setAllPrivileges(true);
			pContext.doModifyContent(newRec, vc);
			pContext.setAllPrivileges(false);

		} catch (OperationException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		super.handleAfterModify(aContext);

	}
}
