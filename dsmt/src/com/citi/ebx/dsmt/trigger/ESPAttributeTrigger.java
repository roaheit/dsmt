package com.citi.ebx.dsmt.trigger;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;

public class ESPAttributeTrigger extends TableTrigger {
	
	public static final Path _ATtrNam = Path.parse("./ATTR_NM");
	@Override
	public void setup(TriggerSetupContext context) {
		// do nothing
	}
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context) throws OperationException {
		if(null != context.getChanges().getChange(_ATtrNam))
			throw OperationException.createError("Modification of Attribute Name is not allowed.");
		super.handleBeforeModify(context);
	}
}
