package com.citi.ebx.dsmt.trigger;

import java.util.Calendar;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class DSMTCatalogTrigger extends AuditColumnsTrigger {
	
	private Path lastUpdateDateFieldPath = DSMTConstants.lastUpdateDateFieldPath;
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		context.setAllPrivileges();

		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();		
		Calendar cal = Calendar.getInstance();
		if(null!=vc.getValue(lastUpdateDateFieldPath)){
		cal.setTime((Date)vc.getValue(lastUpdateDateFieldPath));		
		if (Calendar.getInstance().get(Calendar.DATE)>cal.get(Calendar.DATE)){
			vc.setValue(vc.getValue(DSMTConstants.CATALOG_TABLE_COUNT), DSMTConstants.CATALOG_TABLE_PREV_COUNT);
		}
		}

		super.handleBeforeModify(context);
	}

}
