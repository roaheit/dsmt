package com.citi.ebx.dsmt.trigger;

import java.util.Date;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class LatamTrigger extends TableTrigger {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	private Path lastUpdateUserFieldPath = Path.parse("./LASTUPDOPRID");
	private Path lastUpdateDateFieldPath = Path.parse("./CHNG_DTTM");

	public String getLastUpdateUserFieldPath() {
		return lastUpdateUserFieldPath.format();
	}

	public void setLastUpdateUserFieldPath(String lastUpdateUserFieldPath) {
		this.lastUpdateUserFieldPath = Path.parse(lastUpdateUserFieldPath);
	}

	public String getLastUpdateDateFieldPath() {
		return lastUpdateDateFieldPath.format();
	}

	public void setLastUpdateDateFieldPath(String lastUpdateDateFieldPath) {
		this.lastUpdateDateFieldPath = Path.parse(lastUpdateDateFieldPath);
	}

	
	@Override
	public void setup(TriggerSetupContext context) {
		SchemaNode node = context.getSchemaNode();
		if (!node.isTableNode() && !node.isTableOccurrenceNode())
			context.addError("Table trigger " + this.getClass().getName()
					+ " applied to non table.");
	}



	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		// System.out.println("ENTERED handlebeforeCreate");
		context.setAllPrivileges();

		final ValueContextForUpdate vc = context
				.getOccurrenceContextForUpdate();
		String userID = context.getSession().getUserReference().getUserId();

		/** Capitalize the userID */
		if (null != userID) {
			userID = userID.toUpperCase();
		}

		vc.setValue(userID, lastUpdateUserFieldPath);
		Date now = new Date();
		vc.setValue(now, lastUpdateDateFieldPath);

		super.handleBeforeCreate(context);
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		context.setAllPrivileges();

		final ValueContextForUpdate vc = context
				.getOccurrenceContextForUpdate();

		String userID = context.getSession().getUserReference().getUserId();

		/** Capitalize the userID */
		if (null != userID) {
			userID = userID.toUpperCase();
		}

		vc.setValue(userID, lastUpdateUserFieldPath);
		vc.setValue(new Date(), lastUpdateDateFieldPath);

		super.handleBeforeModify(context);
	}

	
}