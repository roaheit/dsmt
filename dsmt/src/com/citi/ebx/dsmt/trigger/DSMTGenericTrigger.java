package com.citi.ebx.dsmt.trigger;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.citi.ebx.dsmt.path.DSMTPaths;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.adaptation.RequestSortCriteria;
import com.onwbp.adaptation.UnavailableContentError;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class DSMTGenericTrigger extends TableTrigger {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private Path creationUserFieldPath = Path.parse("./CREATED_OPERID");
	private Path creationDateFieldPath = Path.parse("./ORIG_DTTM");
	private Path lastUpdateUserFieldPath = Path.parse("./LASTUPDOPRID");
	private Path lastUpdateDateFieldPath = Path.parse("./CHNG_DTTM");
	private Path effDateFieldPath = Path.parse("./EFFDT");
	private Path endDateFieldPath = Path.parse("./ENDDT");
	private RequestSortCriteria sortByEffDate = new RequestSortCriteria();

	public String getCreationUserFieldPath() {
		return this.creationUserFieldPath.format();
	}
	
	public void setCreationUserFieldPath(String creationUserFieldPath) {
		this.creationUserFieldPath = Path.parse(creationUserFieldPath);
	}
	
	public String getCreationDateFieldPath() {
		return creationDateFieldPath.format();
	}

	public void setCreationDateFieldPath(String creationDateFieldPath) {
		this.creationDateFieldPath = Path.parse(creationDateFieldPath);
	}

	public String getLastUpdateUserFieldPath() {
		return lastUpdateUserFieldPath.format();
	}

	public void setLastUpdateUserFieldPath(String lastUpdateUserFieldPath) {
		this.lastUpdateUserFieldPath = Path.parse(lastUpdateUserFieldPath);
	}

	public String getLastUpdateDateFieldPath() {
		return lastUpdateDateFieldPath.format();
	}

	public void setLastUpdateDateFieldPath(String lastUpdateDateFieldPath) {
		this.lastUpdateDateFieldPath = Path.parse(lastUpdateDateFieldPath);
	}

	public String getEffDateFieldPath() {
		return this.effDateFieldPath.format();
	}
	
	public void setEffDateFieldPath(String effDateFieldPath) {
		this.effDateFieldPath = Path.parse(effDateFieldPath);
		this.sortByEffDate = new RequestSortCriteria();
		this.sortByEffDate.add(this.effDateFieldPath, false);
	}
	
	public String getEndDateFieldPath() {
		return this.endDateFieldPath.format();
	}

	public void setEndDateFieldPath(String endDateFieldPath) {
		this.endDateFieldPath = Path.parse(endDateFieldPath);
	}

	@Override
	public void setup(TriggerSetupContext context) {
		SchemaNode node = context.getSchemaNode();
		if (!node.isTableNode() && !node.isTableOccurrenceNode())
			context.addError("Table trigger " + this.getClass().getName()
					+ " applied to non table.");
		if (node.getNode(creationUserFieldPath) == null)
			context.addError("Creation user field path " + this.creationUserFieldPath
					+ " not found on table.");
		if (node.getNode(effDateFieldPath) == null)
			context.addError("Effective date path " + this.effDateFieldPath
					+ " not found on table.");
		if (node.getNode(endDateFieldPath) == null)
			context.addError("End date path " + this.endDateFieldPath
					+ " not found on table.");

		sortByEffDate = new RequestSortCriteria();
		sortByEffDate.add(this.effDateFieldPath, false);
	}

	// There is no static way to initialize to 12-31-9999, use long value
	private static Date lastEndDate = new Date(253402232400000L);

	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		final Adaptation newRec = context.getAdaptationOccurrence();
		final AdaptationTable aTab = newRec.getContainerTable();

		final ProcedureContext pContext = context.getProcedureContext();
		final ValueContextForUpdate vc = pContext.getContext(newRec
				.getAdaptationName());

		Adaptation previous = null;
		Date newRecEnd = lastEndDate;
		final Date newRecEff = newRec.getDate(effDateFieldPath);

		// Search for record we occur after
		try {
			// String searchPred = "date-less-than("+ effDateFieldPath.format()
			// + ", " + vc.getValue(effDateFieldPath) + ")";
			String searchPred = null;
			for (Path p : aTab.getPrimaryKeySpec()) {
				String path = "." + p.format();
				// Copy vc primary key fields except effective date
				if (path.equals(effDateFieldPath.format()))
					continue;
				// Note this does not handle values with '
				if (searchPred == null)
					searchPred = "(" + path + " = '" + vc.getValue(p) + "')";
				else
					searchPred = searchPred.concat(" and (" + path + " = '"
							+ vc.getValue(p) + "')");
			}
			RequestResult res = aTab.createRequestResult(searchPred,
					sortByEffDate);
			Adaptation rec = res.nextAdaptation();

			// Scan for records that take place after the new record
			while (rec != null) {
				Date recStart = rec.getDate(effDateFieldPath);
				if (recStart.after(newRecEff))
					// Rec starts after newRec, adjust endDate
					newRecEnd = recStart;
				else if (recStart.before(newRecEff)) {
					// Rec starts before newRec
					break;
				}
				rec = res.nextAdaptation();
			}
			previous = rec;

		} catch (Exception e) {
			// Previous not found
			LOG.error(e.getMessage());
			// System.out.println("No others found.");
		}

		pContext.setAllPrivileges(true);
		vc.setValue(newRecEnd, endDateFieldPath);
		pContext.doModifyContent(newRec, vc);

		// Update existing record to end when this record begins
		if (previous != null) {
			final ValueContextForUpdate vcPrev = pContext.getContext(previous
					.getAdaptationName());
			vcPrev.setValue(vc.getValue(effDateFieldPath), endDateFieldPath);
			pContext.doModifyContent(previous, vcPrev);
		}
		pContext.setAllPrivileges(false);

		super.handleAfterCreate(context);
	}

	public void handleNewContext(NewTransientOccurrenceContext context) {

		try {
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = 01;
			cal.set(Calendar.MONTH, month);
			cal.set(Calendar.DAY_OF_MONTH, day);
			cal.set(Calendar.YEAR, year);
			context.getOccurrenceContextForUpdate().setValue(cal.getTime(),
					effDateFieldPath);

			super.handleNewContext(context);

		} catch (UnavailableContentError e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		}

		catch (PathAccessException e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		// System.out.println("ENTERED handlebeforeCreate");
		context.setAllPrivileges();
		
		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		String userID = context.getSession().getUserReference()
				.getUserId();
		
		/** Capitalize the userID */
		if(null != userID){
			userID = userID.toUpperCase();
		}
		
		// handle webservice update scenarios
		if( null!=userID && !userID.equalsIgnoreCase(DSMTConstants.EBX_ACCOUNT_WF_USER)){		
			vc.setValue(userID, creationUserFieldPath);
			vc.setValue(userID, lastUpdateUserFieldPath);
		}
		
		Date now = new Date();
		vc.setValue(now, creationDateFieldPath);
		vc.setValue(now, lastUpdateDateFieldPath);
		
		final Date contextDate = (Date) vc.getValue(effDateFieldPath);
		final int newEff = contextDate.getDate();
		if (newEff != 01) {
			contextDate.setDate(01);
			LOG.info("DATE should always be set to 01");
			vc.setValue(contextDate, effDateFieldPath);
		}
		super.handleBeforeCreate(context);
	}

	
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		context.setAllPrivileges();
		
		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		String userID = context.getSession().getUserReference()
				.getUserId();
		
		/** Capitalize the userID */
		if(null != userID){
			userID = userID.toUpperCase();
		}
		
		// handle webservice update scenarios
		if( null!=userID && !userID.equalsIgnoreCase(DSMTConstants.EBX_ACCOUNT_WF_USER)){		
		
			vc.setValue(userID, lastUpdateUserFieldPath);
		}
		vc.setValue(new Date(), lastUpdateDateFieldPath);

		super.handleBeforeModify(context);
	}

	/**
	 * This is overridden in order to update the readme table when a row is deleted
	 */
	@Override
	public void handleAfterDelete(AfterDeleteOccurrenceContext context)
			throws OperationException {
		final SchemaNode tableNode = context.getTable().getTableNode();
		// Don't need to bother trying to update the readme table when the deletion is for the readme table itself
		if (! DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM.getPathInSchema().equals(tableNode.getPathInSchema())) {
			final Date now = new Date();
			final ProcedureContext pContext = context.getProcedureContext();
			// Find all related readme records
			final List<Adaptation> readmeRecords = getReadmeRecords(context.getTable());
			for (Adaptation readmeRecord: readmeRecords) {
				// Update each related readme record's last deletion date to now
				final ValueContextForUpdate vc = pContext.getContext(readmeRecord.getAdaptationName());
				
				vc.setValue(now, DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM._TrgLastDelDt);
				pContext.doModifyContent(readmeRecord, vc);
			}
		}
		super.handleAfterDelete(context);
	}
	
	// Queries for all records in the readme table that are associated with the given table
	private List<Adaptation> getReadmeRecords(final AdaptationTable table) {
		if (LOG.isDebug()) {
			LOG.debug("DSMTGenericTrigger: getReadmeRecords");
			LOG.debug("DSMTGenericTrigger: table = " + table.getTablePath().format());
		}
		final Adaptation dataSet = table.getContainerAdaptation();
		final AdaptationTable readmeTable = dataSet.getTable(DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM.getPathInSchema());
		final String predicate = DSMTPaths._Root_GLOBAL_STD_C_DSMT_GFDSS_RM._C_GTD_TABLE_NAME.format() + "='"
				+ table.getTablePath().format() + "'";
		final RequestResult reqRes = readmeTable.createRequestResult(predicate);
		final ArrayList<Adaptation> readmeRecords = new ArrayList<Adaptation>();
		try {
			// Add all the records from the result set to a list
			for (Adaptation adaptation; (adaptation = reqRes.nextAdaptation()) != null;) {
				readmeRecords.add(adaptation);
			}
		} finally {
			reqRes.close();
		}
		return readmeRecords;
	}
}
