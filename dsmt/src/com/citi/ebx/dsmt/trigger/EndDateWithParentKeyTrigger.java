package com.citi.ebx.dsmt.trigger;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.adaptation.RequestSortCriteria;
import com.onwbp.adaptation.UnavailableContentError;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class EndDateWithParentKeyTrigger extends TableTrigger {


	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	private String tableXPath;		
	private String sourceColumnName;
	private String setIDColumn;	
	private String codeColumn;		
	private String effDateColumn;
	private String parentForeignKeyColumn;
	
	
	
	private Path effDateFieldPath = Path.parse("./EFFDT");
	private Path endDateFieldPath = Path.parse("./ENDDT");
	private Path statusFieldPath = Path.parse("./EFF_STATUS");

	private RequestSortCriteria sortByEffDate = new RequestSortCriteria();

	
	public String getEffDateFieldPath() {
		return this.effDateFieldPath.format();
	}

	public String getStatusFieldPath() {
		return this.statusFieldPath.format();
	}

	protected void setEffDateFieldPath(String effDateFieldPath) {
		this.effDateFieldPath = Path.parse(effDateFieldPath);
		this.sortByEffDate = new RequestSortCriteria();
		this.sortByEffDate.add(this.effDateFieldPath, false);
	}

	public String getEndDateFieldPath() {
		return this.endDateFieldPath.format();
	}

	public void setEndDateFieldPath(String endDateFieldPath) {
		this.endDateFieldPath = Path.parse(endDateFieldPath);
	}

	@Override
	public void setup(TriggerSetupContext context) {
		SchemaNode node = context.getSchemaNode();
	
		if (!node.isTableNode() && !node.isTableOccurrenceNode())
			context.addError("Table trigger " + this.getClass().getName()
					+ " applied to non table.");
		if (node.getNode(effDateFieldPath) == null)
			context.addError("Effective date path " + this.effDateFieldPath
					+ " not found on table.");
		if (node.getNode(endDateFieldPath) == null)
			context.addError("End date path " + this.endDateFieldPath
					+ " not found on table.");

		sortByEffDate = new RequestSortCriteria();
		sortByEffDate.add(this.effDateFieldPath, false);
	}

	// There is no static way to initialize to 12-31-9999, use long value
	private static Date lastEndDate = new Date(253402232400000L);

	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		final Adaptation newRec = context.getAdaptationOccurrence();
		final AdaptationTable aTab = newRec.getContainerTable();

		final ProcedureContext pContext = context.getProcedureContext();
		final ValueContextForUpdate vc = pContext.getContext(newRec
				.getAdaptationName());

		Adaptation previous = null;
		Date newRecEnd = lastEndDate;
		final Date newRecEff = newRec.getDate(effDateFieldPath);

		// Search for record we occur after
		try {
			// String searchPred = "date-less-than("+ effDateFieldPath.format()
			// + ", " + vc.getValue(effDateFieldPath) + ")";
			String searchPred = null;
			for (Path p : aTab.getPrimaryKeySpec()) {
				String path = "." + p.format();
				// Copy vc primary key fields except effective date
				if (path.equals(effDateFieldPath.format()))
					continue;
				// Note this does not handle values with '
				if (searchPred == null)
					searchPred = "(" + path + " = '" + vc.getValue(p) + "')";
				else
					searchPred = searchPred.concat(" and (" + path + " = '"
							+ vc.getValue(p) + "')");
			}
			RequestResult res = aTab.createRequestResult(searchPred,
					sortByEffDate);
			Adaptation rec = res.nextAdaptation();

			// Scan for records that take place after the new record
			while (rec != null) {
				Date recStart = rec.getDate(effDateFieldPath);
				if (recStart.after(newRecEff))
					// Rec starts after newRec, adjust endDate
					newRecEnd = recStart;
				else if (recStart.before(newRecEff)) {
					// Rec starts before newRec
					break;
				}
				rec = res.nextAdaptation();
			}
			previous = rec;

		} catch (Exception e) {
			// Previous not found
			LOG.error(e.getMessage());
			// System.out.println("No others found.");
		}

		pContext.setAllPrivileges(true);
		vc.setValue(newRecEnd, endDateFieldPath);
		
		Object parentKey = getValue(newRec);
		vc.setValue(parentKey, Path.parse(parentForeignKeyColumn) );
		
		pContext.doModifyContent(newRec, vc);

		
		
		// Update existing record to end when this record begins
		if (previous != null) {
			final ValueContextForUpdate vcPrev = pContext.getContext(previous
					.getAdaptationName());
			vcPrev.setValue(vc.getValue(effDateFieldPath), endDateFieldPath);
			pContext.doModifyContent(previous, vcPrev);
		}
		pContext.setAllPrivileges(false);

		super.handleAfterCreate(context);
	}

	
	
	
	public Object getValue(Adaptation record) {

		try {
				
				Path foreignTablePath = Path.parse(tableXPath); 
				Path sourceColumn = Path.parse(sourceColumnName);					
				Path foreignColumn = Path.parse(codeColumn);					
				
				
				Object predicateValueObject = record.get(sourceColumn);
				String sourceValue = null;
				
					if(predicateValueObject instanceof String){
						sourceValue = record.getString(sourceColumn);
					}
					else if (predicateValueObject instanceof BigDecimal){
						sourceValue = String.valueOf((BigDecimal)record.get(sourceColumn));
					}
				
					// set foreign key to null for root node
					if(null==sourceValue){
						return null;
					}
						
					// final String statusPredicate = status.format() + " = 'A'";
					final String endDatePredicate = "date-equal(" + endDateFieldPath.format() + ",'9999-12-31') or osd:is-null(" + endDateFieldPath.format() + ")";
	
					String predicateString = foreignColumn.format() + "= '" + sourceValue + "' and " + "(" + endDatePredicate + ")" ; 
					
					// + "' and (./ENDDT = '9999-12-31' OR ./ENDDT IS NULL) ";
					LOG.info("predicate String = " + predicateString );
					AdaptationTable table = record.getContainer().getTable(foreignTablePath);
					RequestResult reqRes = table.createRequestResult(predicateString);
				
			Adaptation rec = reqRes.nextAdaptation();
			try {
				
				LOG.info("record " + rec);
				
				Path foreignCodeCol = Path.parse(codeColumn);	
				Path foreignEffDateCol = Path.parse(effDateColumn);
				
				Path foreignSetIDCol  = null;
				if(null != setIDColumn){
					foreignSetIDCol  = Path.parse(setIDColumn);
				}
			
				String foreignCodeColString = rec.getString(Path.SELF.add(foreignCodeCol));
				
				Date effectiveDate = rec.getDate(Path.SELF.add(foreignEffDateCol));
				String foreignEffDateColString = getDateInFormat(effectiveDate, DATE_FORMAT);
				
				String foreignSetIDColString = null;
				if(null != foreignSetIDCol){
					foreignSetIDColString = rec.getString(Path.SELF.add(foreignSetIDCol));
				}
				
				String foreignKey = null;
				
				if(null != foreignSetIDColString){
					foreignKey = foreignKey + foreignSetIDColString + DELIMITER;
				}
				if(null != foreignCodeColString){
					foreignKey = foreignKey + foreignCodeColString + DELIMITER;
				}
				if(null != foreignEffDateColString){
					foreignKey = foreignKey + foreignEffDateColString + DELIMITER;
				}
				
				foreignKey = removeNulls(foreignKey);
				
				if(null == foreignKey){
					LOG.info("foreign key is " + foreignKey + " for " + sourceColumnName + " = " + sourceValue);
				}
				else{
					LOG.debug("foreign key is " + foreignKey + " for " + sourceColumnName + " = " + sourceValue);
				}
				
				return String.valueOf(foreignKey);
			} catch (Exception e) {
				
				LOG.error("Exception = " + e.getMessage()  + String.valueOf(e.getStackTrace()));
				
			}
			finally {
				reqRes.close();
			}
		} 
		catch (Exception e) {
			LOG.error("Exception " +  e.getMessage() );
			return null;	
		}
		
		return null;	
	}

	
	
	public void handleNewContext(NewTransientOccurrenceContext context) {

		try {
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = 01;
			cal.set(Calendar.MONTH, month);
			cal.set(Calendar.DAY_OF_MONTH, day);
			cal.set(Calendar.YEAR, year);
			context.getOccurrenceContextForUpdate().setValue(cal.getTime(),
					effDateFieldPath);

			super.handleNewContext(context);

		} catch (UnavailableContentError e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		}

		catch (PathAccessException e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		}
	}

	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
	
			DSMTUtils.handleBeforeDelete(context);
	
	}


	public String getTableXPath() {
		return tableXPath;
	}

	public void setTableXPath(String tableXPath) {
		this.tableXPath = tableXPath;
	}

	public String getSourceColumnName() {
		return sourceColumnName;
	}

	public void setSourceColumnName(String sourceColumnName) {
		this.sourceColumnName = sourceColumnName;
	}

	public String getSetIDColumn() {
		return setIDColumn;
	}

	public void setSetIDColumn(String setIDColumn) {
		this.setIDColumn = setIDColumn;
	}

	public String getCodeColumn() {
		return codeColumn;
	}

	public void setCodeColumn(String codeColumn) {
		this.codeColumn = codeColumn;
	}

	public String getEffDateColumn() {
		return effDateColumn;
	}

	public void setEffDateColumn(String effDateColumn) {
		this.effDateColumn = effDateColumn;
	}

public void setParentForeignKeyColumn(String parentForeignKeyColumn) {
		this.parentForeignKeyColumn = parentForeignKeyColumn;
	}

	public String getParentForeignKeyColumn() {
		return parentForeignKeyColumn;
	}

private String getDateInFormat(Date date, String format){
		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	private String removeNulls(String value) {
		
		String foreignKey = null;
		if (null != value && value.contains(NULL)){
			foreignKey = value.replaceAll(NULL, "");
		}
		else 
			foreignKey = value;
		
		if(null != foreignKey && foreignKey.endsWith(DELIMITER)){
			foreignKey = foreignKey.substring(0, foreignKey.length() - DELIMITER.length());
		}
		
		return foreignKey;
	}

	
	private static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final String NULL = "null";
	private static final String DELIMITER = "|";
	
}