package com.citi.ebx.dsmt.trigger;

import java.util.ArrayList;
import java.util.List;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.ValueContextForUpdate;
import com.orchestranetworks.workflow.ProcessInstanceKey;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.WorkflowEngine;

public class ManagerTerminateTrigger extends TableTrigger {
	
		
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path empStatus = Path.parse("./C_GDW_EMP_STATUS");
	private Path soeID = Path.parse("./MANAGER_ID");
	public static final Path LVID_LEMTable= AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping.getPathInSchema();
	public static final Path Accounting_MS_Hier_NAM =   AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM.getPathInSchema();
	public static final Path Accounting_MS_Hier_ASIA =  AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_ASIA.getPathInSchema();
	public static final Path Accounting_MS_Hier_EMEA =  AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_EMEA.getPathInSchema();
	public static final Path Accounting_MS_Hier_LATAM = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_LATAM.getPathInSchema();
	
	List<Path> pathsList = new ArrayList<Path>();

	
	@Override
	public void setup(TriggerSetupContext context) {
		SchemaNode node = context.getSchemaNode();
		/*if (!node.isTableNode() && !node.isTableOccurrenceNode())
			context.addError("Table trigger " + this.getClass().getName()
					+ " applied to non table.");
		if (node.getNode(creationUserFieldPath) == null)
			context.addError("Creation user field path "
					+ this.creationUserFieldPath + " not found on table.");
	*/
	}

	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		//
	}

	public void handleNewContext(NewTransientOccurrenceContext context) {

	}

	@SuppressWarnings("deprecation")
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		//
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		LOG.info("ManagerTerminateTrigger...start");	

		final ValueContextForUpdate vc = context
				.getOccurrenceContextForUpdate();
		String status=vc.getValue(empStatus).toString();
		String soeid=vc.getValue(soeID).toString();
		
		if(pathsList.isEmpty()|| pathsList.size() < 1){
		pathsList.add(Accounting_MS_Hier_ASIA);
		pathsList.add(Accounting_MS_Hier_EMEA);
		pathsList.add(Accounting_MS_Hier_LATAM);
		pathsList.add(Accounting_MS_Hier_NAM);
		}
		if(status=="T" || status.equals("T"))
		{
		
		Repository rp=context.getAdaptationHome().getRepository();
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(rp,AccountibilityMatrixConstants.DATASPACE_Accounting_Matrix, AccountibilityMatrixConstants.DATASET_Accounting_Matrix);
		AdaptationTable targetTable;
		
		StringBuffer predicate = new StringBuffer("osd:contains-case-insensitive("+AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID
				.format()+",'" + soeid + "')");
		
		for(Path tablePath: pathsList){
			targetTable = metadataDataSet.getTable(tablePath);
			RequestResult rs =targetTable.createRequestResult(predicate.toString());
			if(!rs.isEmpty()){
				launchWorkFlow(rp,context,soeid.toUpperCase(),tablePath);
			}
			rs.close();
		}	
			LOG.info("ManagerTerminateTrigger...end");
			
			super.handleBeforeModify(context);
		}
	}
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
	
		//DSMTUtils.handleBeforeDelete(context);
	
	}
	
	public void launchWorkFlow(Repository rp,BeforeModifyOccurrenceContext context, String soeid,Path tablePath){
		final Session session = context.getSession();
		String soeID="soeID";
		String tableXPath="tableXPath";
		String profile="profile";
		
		try{
			
			WorkflowEngine wrkflowEngine = WorkflowEngine.getFromRepository(rp, session);
			ProcessLauncher launcher = wrkflowEngine.getProcessLauncher(PublishedProcessKey.forName("TerminateManagerWF"));
			launcher.setInputParameter(soeID, soeid);
			launcher.setInputParameter(tableXPath, tablePath.format());
		
			ProcessInstanceKey processKey  = launcher.launchProcess();
		
		
		}catch(OperationException e){
			e.getStackTrace();
			LOG.info("Exception occurred while launching WF in manager terminate Trigger " +e.getMessage());
			e.printStackTrace();			
			
			
		}
	}
	

}