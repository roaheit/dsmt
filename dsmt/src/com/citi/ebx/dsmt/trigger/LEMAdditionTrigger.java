package com.citi.ebx.dsmt.trigger;

import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class LEMAdditionTrigger extends TableTrigger {
	
		
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	private Path lemSoeidPath =AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM;
	private Path lvidPath=AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID;
	
	@Override
	public void setup(TriggerSetupContext context) {
		
	}

	@SuppressWarnings("deprecation")
	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		final Adaptation newRec = context.getAdaptationOccurrence();
			
	final String lemSoeid=newRec.getString(lemSoeidPath);
	final String lvid=newRec.getString(lvidPath);
	final String subject="Legal Entity Manager Addition";
	final String message="Hello"+","+"\n\n"+"You have been marked as Legal Entity Manager for Legal Entity "+lvid
			+"."+"\nIf any concerns please consult your supervisor"+"\n"
			+"\n\n"+"For any Support, Please contact DSMT Tech Support"+"\n"
			+"Thank You";
	DSMTNotificationService.notifyEmailID(lemSoeid,
			subject,
			message, false);
		super.handleAfterCreate(context);
	}

	

	@SuppressWarnings("deprecation")
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		// System.out.println("ENTERED handlebeforeCreate");
		context.setAllPrivileges();

		final ValueContextForUpdate vc = context
				.getOccurrenceContextForUpdate();
		
		super.handleBeforeCreate(context);
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
	
		super.handleBeforeModify(context);
	}


	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
	
			super.handleBeforeDelete(context);
	}
}