package com.citi.ebx.dsmt.trigger;

import java.util.Calendar;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.adaptation.RequestSortCriteria;
import com.onwbp.adaptation.UnavailableContentError;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class DSMTHierarchyTrigger extends TableTrigger {
	
		
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();

	private Path creationUserFieldPath = DSMTConstants.creationUserFieldPath;
	private Path creationDateFieldPath = DSMTConstants.creationDateFieldPath;
	private Path lastUpdateUserFieldPath = DSMTConstants.lastUpdateUserFieldPath;
	private Path lastUpdateDateFieldPath = DSMTConstants.lastUpdateDateFieldPath;
	private Path effDateFieldPath = DSMTConstants.effDateFieldPath;
	private Path endDateFieldPath = DSMTConstants.endDateFieldPath;
	private Path statusFieldPath = DSMTConstants.statusFieldPath;
	
	
	private Path parentFieldID = Path.parse("./PARENT_ID"); // set default value - should be passed as parameter to override.
	
	// private Path foreignKeyPath = Path.parse("./LASTUPDOPRID");

	private RequestSortCriteria sortByEffDate = new RequestSortCriteria();

	public String getCreationUserFieldPath() {
		return this.creationUserFieldPath.format();
	}

	public void setCreationUserFieldPath(String creationUserFieldPath) {
		this.creationUserFieldPath = Path.parse(creationUserFieldPath);
	}

	public String getCreationDateFieldPath() {
		return creationDateFieldPath.format();
	}

	public void setCreationDateFieldPath(String creationDateFieldPath) {
		this.creationDateFieldPath = Path.parse(creationDateFieldPath);
	}

	public String getLastUpdateUserFieldPath() {
		return lastUpdateUserFieldPath.format();
	}

	public void setLastUpdateUserFieldPath(String lastUpdateUserFieldPath) {
		this.lastUpdateUserFieldPath = Path.parse(lastUpdateUserFieldPath);
	}

	public String getLastUpdateDateFieldPath() {
		return lastUpdateDateFieldPath.format();
	}

	public void setLastUpdateDateFieldPath(String lastUpdateDateFieldPath) {
		this.lastUpdateDateFieldPath = Path.parse(lastUpdateDateFieldPath);
	}

	public String getEffDateFieldPath() {
		return this.effDateFieldPath.format();
	}

	public String getStatusFieldPath() {
		return this.statusFieldPath.format();
	}

	public void setEffDateFieldPath(String effDateFieldPath) {
		this.effDateFieldPath = Path.parse(effDateFieldPath);
		this.sortByEffDate = new RequestSortCriteria();
		this.sortByEffDate.add(this.effDateFieldPath, false);
	}

	public String getEndDateFieldPath() {
		return this.endDateFieldPath.format();
	}

	public void setEndDateFieldPath(String endDateFieldPath) {
		this.endDateFieldPath = Path.parse(endDateFieldPath);
	}

	@Override
	public void setup(TriggerSetupContext context) {
		SchemaNode node = context.getSchemaNode();
		if (!node.isTableNode() && !node.isTableOccurrenceNode())
			context.addError("Table trigger " + this.getClass().getName()
					+ " applied to non table.");
		if (node.getNode(creationUserFieldPath) == null)
			context.addError("Creation user field path "
					+ this.creationUserFieldPath + " not found on table.");
		if (node.getNode(effDateFieldPath) == null)
			context.addError("Effective date path " + this.effDateFieldPath
					+ " not found on table.");
		if (node.getNode(endDateFieldPath) == null)
			context.addError("End date path " + this.endDateFieldPath
					+ " not found on table.");
		if (node.getNode(parentFieldID) == null)
			context.addError("Parent Foreign Key field path " + this.parentFieldID + " not found on table.");

		sortByEffDate = new RequestSortCriteria();
		sortByEffDate.add(this.effDateFieldPath, false);
	}

	// There is no static way to initialize to 12-31-9999, use long value
	private static Date lastEndDate = new Date(253402232400000L);

	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		final Adaptation newRec = context.getAdaptationOccurrence();
		final AdaptationTable aTab = newRec.getContainerTable();

		final ProcedureContext pContext = context.getProcedureContext();
		final ValueContextForUpdate vc = pContext.getContext(newRec
				.getAdaptationName());

		Adaptation previous = null;
		Date newRecEnd = lastEndDate;
		
		final Date newRecEff = newRec.getDate(effDateFieldPath);

		// Search for record we occur after
		try {
			// String searchPred = "date-less-than("+ effDateFieldPath.format()
			// + ", " + vc.getValue(effDateFieldPath) + ")";
			String searchPred = null;
			for (Path p : aTab.getPrimaryKeySpec()) {
				String path = "." + p.format();
				// Copy vc primary key fields except effective date
				if (path.equals(effDateFieldPath.format()))
					continue;
				// Note this does not handle values with '
				if (searchPred == null)
					searchPred = "(" + path + " = '" + vc.getValue(p) + "')";
				else
					searchPred = searchPred.concat(" and (" + path + " = '"
							+ vc.getValue(p) + "')");
			}
			RequestResult res = aTab.createRequestResult(searchPred,
					sortByEffDate);
			Adaptation rec = res.nextAdaptation();

			// Scan for records that take place after the new record
			while (rec != null) {
				Date recStart = rec.getDate(effDateFieldPath);
				if (recStart.after(newRecEff))
					// Rec starts after newRec, adjust endDate
					newRecEnd = recStart;
				else if (recStart.before(newRecEff)) {
					// Rec starts before newRec
					break;
				}
				rec = res.nextAdaptation();
			}
			previous = rec;

		} catch (Exception e) {
			// Previous not found
			LOG.error(e.getMessage());
			// System.out.println("No others found.");
		}

		pContext.setAllPrivileges(true);
		pContext.setTriggerActivation(false);
		vc.setValue(newRecEnd, endDateFieldPath);
		pContext.doModifyContent(newRec, vc);

		// Update existing record to end when this record begins
		if (previous != null) {
			final ValueContextForUpdate vcPrev = pContext.getContext(previous
					.getAdaptationName());
			vcPrev.setValue(vc.getValue(effDateFieldPath), endDateFieldPath);
			pContext.doModifyContent(previous, vcPrev);
			final String oldPK= previous.getOccurrencePrimaryKey().format();
			LOG.info("oldPK Value "+ oldPK);
			
			// move all children under a new parent
			updateChildReordFK(pContext,newRec,previous);
		}
		
		pContext.setTriggerActivation(true);
		pContext.setAllPrivileges(false);

		super.handleAfterCreate(context);
	}

	private void updateChildReordFK(ProcedureContext pContext,
			Adaptation newRecord, Adaptation previousRec) {
		
		final String oldPK = previousRec.getOccurrencePrimaryKey().format();
		final String searchPred = parentFieldID.format() + "='" + oldPK + "'" + " and " + endDatePredicate();
		LOG.info("search predicate being used = " + searchPred);
		final AdaptationTable adTable = previousRec.getContainerTable();
		final RequestResult reqRes = adTable.createRequestResult(searchPred);
		if (reqRes != null) {
			try {
				LOG.debug("reqsize in updateChildReordFK : " + reqRes.getSize());
				pContext.setTriggerActivation(false);
				for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
					LOG.debug("update child in updateChildReordFK : child PK  "	+ record.getOccurrencePrimaryKey().format()	+ " new FK for old record  "	+ newRecord.getOccurrencePrimaryKey().format());
					final ValueContextForUpdate vcPrev = pContext.getContext(record.getAdaptationName());
					vcPrev.setValue(newRecord.getOccurrencePrimaryKey().format(), parentFieldID);
					
					pContext.doModifyContent(record, vcPrev);
					
				}
				pContext.setTriggerActivation(true);
			} catch (OperationException e) {
				LOG.error(" exception occured while moving children to the newly created node" + e.getMessage());
			
			}
		}

	}

	
	private String endDatePredicate(){
		
		String endDateField = endDateFieldPath.format();
		return " ( osd:is-null(" + endDateField + ") or date-equal(" + endDateField + ", '9999-12-31') )";
	}
	
	public void handleNewContext(NewTransientOccurrenceContext context) {

		try {
			Date date = new Date();
			final Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = 01;
			cal.set(Calendar.MONTH, month);
			cal.set(Calendar.DAY_OF_MONTH, day);
			cal.set(Calendar.YEAR, year);
			context.getOccurrenceContextForUpdate().setValue(cal.getTime(),
					effDateFieldPath);

			super.handleNewContext(context);

		} catch (UnavailableContentError e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		}

		catch (PathAccessException e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		// System.out.println("ENTERED handlebeforeCreate");
		context.setAllPrivileges();

		final ValueContextForUpdate vc = context
				.getOccurrenceContextForUpdate();
		String userID = context.getSession().getUserReference().getUserId();

		/** Capitalize the userID */
		if (null != userID) {
			userID = userID.toUpperCase();
		}

		// handle webservice update scenarios
		if( null!=userID && !userID.equalsIgnoreCase(DSMTConstants.EBX_ACCOUNT_WF_USER)){		
			vc.setValue(userID, creationUserFieldPath);
			vc.setValue(userID, lastUpdateUserFieldPath);
		}
		
		Date now = new Date();
		vc.setValue(now, creationDateFieldPath);
		vc.setValue(now, lastUpdateDateFieldPath);

		final Date contextDate = (Date) vc.getValue(effDateFieldPath);
		LOG.debug("contextDate" + contextDate);
		final int newEff = contextDate.getDate();
		LOG.debug("newEff" + newEff);
		if (newEff != 01) {
			contextDate.setDate(01);
			LOG.debug("DATE should always be set to 01");
			vc.setValue(contextDate, effDateFieldPath);
		}
		super.handleBeforeCreate(context);
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		context.setAllPrivileges();

		final ValueContextForUpdate vc = context
				.getOccurrenceContextForUpdate();

		// checkIfParentIsALeaf(vc);

		String userID = context.getSession().getUserReference().getUserId();

		/** Capitalize the userID */
		if (null != userID) {
			userID = userID.toUpperCase();
		}

		// handle webservice update scenarios
		if(null!=userID && !userID.equalsIgnoreCase(DSMTConstants.EBX_ACCOUNT_WF_USER)){			
		vc.setValue(userID, lastUpdateUserFieldPath);
		}
		
		vc.setValue(new Date(), lastUpdateDateFieldPath);

		super.handleBeforeModify(context);
	}

	/*
	 * 
	 * public void setForeignKeyPath(Path foreignKeyPath) { this.foreignKeyPath
	 * = foreignKeyPath; }
	 * 
	 * public Path getForeignKeyPath() { return foreignKeyPath; }
	 * 
	 * public void checkIfParentIsALeaf(ValueContextForUpdate vc){
	 * 
	 * Adaptation currentRecord = vc.getAdaptationInstance(); String parentKey =
	 * String.valueOf(currentRecord.get(foreignKeyPath));
	 * 
	 * System.out.println(parentKey);
	 * 
	 * 
	 * }
	 */
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
	
		DSMTUtils.handleBeforeDelete(context);
	
	}
	

	public void setParentFieldID(Path parentFieldID) {
	
		this.parentFieldID = parentFieldID;
	}
	
	
	public Path getParentFieldID() {
		return parentFieldID;
	}
	
	
	/*
	protected List<Adaptation> getChildRecord(final AdaptationTable adTable,
			final Adaptation previousRec) {
		final List<Adaptation> fkRecords = new ArrayList<Adaptation>();
		final String oldPK = previousRec.getOccurrencePrimaryKey().format();
		LOG.info("oldPK in getChildRecord : " + oldPK);
		final String searchPred = parentFieldID.format() + "=\"" + oldPK + "\"";

		final RequestResult reqRes = adTable.createRequestResult(searchPred);
		if (reqRes != null) {
			for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
				fkRecords.add(record);
			}
		}
		return fkRecords;
	}
	
	public void updateChildReord(final ProcedureContext pContext,
			final Adaptation newRecord, final List<Adaptation> childRecord) {

		final String newRecordPK = newRecord.getOccurrencePrimaryKey().format();
		LOG.info("newRecordPK in updateChildReord: " + newRecordPK);
		for (Adaptation adaptation : childRecord) {
			try {
				LOG.info("update child : child PK  " + adaptation.getOccurrencePrimaryKey().format()+ " new FK for old record  " + newRecordPK);
				final ValueContextForUpdate vcPrev = pContext.getContext(adaptation.getAdaptationName());
				vcPrev.setValue(newRecordPK, parentFieldID);
				pContext.doModifyContent(adaptation, vcPrev);
			} catch (OperationException e) {
				LOG.error(" exception coccured " + e.getMessage());
			}

		}

	}
	
	*/

}