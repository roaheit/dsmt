package com.citi.ebx.dsmt.trigger;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

/**
 * @author :Aarti Bhasin 
 * Created this trigger to split value in a column into chunks of 2000 characters.
 * Input includes the column to be split, and 3 other columns where the value will be split into * 
 * xsd:tags <osd:trigger class="com.citi.ebx.dsmt.trigger.SplitColumnTrigger">
 *         		<column1>./col1</column1> 
 *         		<column2>./col2</column2>
 *        		<column3>./col3</column3> 
 *         		<columnToSplit>./col4</columnToSplit>
 *         </osd:trigger>
 */
public class SplitColumnTrigger extends TableTrigger {

	private String column1;
	private String column2;
	private String column3;
	private String columnToSplit;

	public String getColumn1() {
		return column1;
	}

	public void setColumn1(String column1) {
		this.column1 = column1;
	}

	public String getColumn2() {
		return column2;
	}

	public void setColumn2(String column2) {
		this.column2 = column2;
	}

	public String getColumn3() {
		return column3;
	}

	public void setColumn3(String column3) {
		this.column3 = column3;
	}

	public String getColumnToSplit() {
		return columnToSplit;
	}

	public void setColumnToSplit(String columnToSplit) {
		this.columnToSplit = columnToSplit;
	}

	@Override
	public void setup(TriggerSetupContext ctx) {

	}

	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext ctx) throws OperationException {

		ValueContextForUpdate vcu = ctx.getOccurrenceContextForUpdate();
		splitColumnLength(vcu);
		super.handleBeforeCreate(ctx);
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext ctx) throws OperationException {

		ValueContextForUpdate valueContextForUpdate = ctx.getOccurrenceContextForUpdate();
		splitColumnLength(valueContextForUpdate);

		super.handleBeforeModify(ctx);
	}

	/**
	 * This method splits the value of the splitColumn into chunks of 2000
	 * characters.
	 * 
	 * @param valueContextForUpdate
	 */
	private void splitColumnLength(ValueContextForUpdate valueContextForUpdate) {
		Path colToSplitPath = Path.parse(columnToSplit);
		Path column1Path = Path.parse(column1);
		Path column2Path = Path.parse(column2);
		Path column3Path = Path.parse(column3);

		String colToSplitValue = (String) valueContextForUpdate.getValue(colToSplitPath);
		if (colToSplitValue != null) {
			int colToSplitValueLength = colToSplitValue.length();

			if (colToSplitValueLength > 0) {
				if (colToSplitValueLength > 4000) {
					String col1Val = colToSplitValue.substring(0, 2000);
					String col2Val = colToSplitValue.substring(2000, 4000);
					String col3Val = colToSplitValue.substring(4000, colToSplitValueLength);
					valueContextForUpdate.setValue(col1Val, column1Path);
					valueContextForUpdate.setValue(col2Val, column2Path);
					valueContextForUpdate.setValue(col3Val, column3Path);
					valueContextForUpdate.setValue(colToSplitValue, colToSplitPath);
				} else if (colToSplitValueLength > 2000 && colToSplitValueLength <= 4000) {
					String col1Val = colToSplitValue.substring(0, 2000);
					String col2Val = colToSplitValue.substring(2000, colToSplitValueLength);
					valueContextForUpdate.setValue(col1Val, column1Path);
					valueContextForUpdate.setValue(col2Val, column2Path);
					valueContextForUpdate.setValue(colToSplitValue, colToSplitPath);
				}
				else if (colToSplitValueLength < 2000) {
					String col1Val = colToSplitValue.substring(0, colToSplitValueLength);
					valueContextForUpdate.setValue(col1Val, column1Path);
					valueContextForUpdate.setValue(colToSplitValue, colToSplitPath);
				}
			} 
		} 
	}

}
