package com.citi.ebx.dsmt.trigger;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class LEMMappingTrigger extends AuditColumnsTrigger{
	
	private Path lemSoeidPath =AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM;
	private Path lvidPath=AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID;
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		
		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		final String lvid=String.valueOf(vc.getValue(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID));
		AdaptationTable table= context.getTable();
		final String lemPredicate = AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID
				.format()
				+ "= '"
				+ lvid
				+ "' and "
				+ AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM_TYPE
						.format() + "= '" 
				+ AccountibilityMatrixConstants.LEM_TYPE_LEM  
				+ "' and "
				+ AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._EFF_STATUS
						.format() + "= '" 
				+ AccountibilityMatrixConstants.ACTIVESTATUS + 
				"'";
		
		
		RequestResult reqRes = table.createRequestResult(lemPredicate);
		if(reqRes.getSize()>1){
			
			throw OperationException
			.createError("There should be only one active LEM for LE "+ lvid);
		}
		
		super.handleBeforeModify(context);
		
	}
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		final String lvid=String.valueOf(vc.getValue(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID));
		AdaptationTable table= context.getTable();
		String lem_Type = String.valueOf(vc.getValue(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM_TYPE)); 
		final String lemPredicate = AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID
				.format()
				+ "= '"
				+ lvid
				+ "' and "
				+ AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM_TYPE
						.format() + "= '" 
				+ AccountibilityMatrixConstants.LEM_TYPE_LEM  
				+ "' and "
				+ AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._EFF_STATUS
						.format() + "= '" 
				+ AccountibilityMatrixConstants.ACTIVESTATUS + 
				"'";
		
		
		RequestResult reqRes = table.createRequestResult(lemPredicate);
		if(reqRes.getSize()>=1 && lem_Type.equalsIgnoreCase(AccountibilityMatrixConstants.LEM_TYPE_LEM)){
			
			throw OperationException
			.createError("There should be only one active LEM for LE "+ lvid);
		}
		
		super.handleBeforeCreate(context);
	}
	@SuppressWarnings("deprecation")
	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		final Adaptation newRec = context.getAdaptationOccurrence();
			
	final String lemSoeid=newRec.getString(lemSoeidPath);
	final String lvid=newRec.getString(lvidPath);
	final String subject="Legal Entity Manager Addition";
	final String message="Hello"+","+"\n\n"+"You have been marked as Legal Entity Manager for Legal Entity"+lvid
			+"."+"\nIf any concerns please consult your supervisor"+"\n"
			+"\n\n"+"For any Support, Please contact DSMT Tech Support"+"\n"
			+"Thank You";
	DSMTNotificationService.notifyEmailID(lemSoeid,
			subject,
			message, false);
		super.handleAfterCreate(context);
	}
}
