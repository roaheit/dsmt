package com.citi.ebx.dsmt.trigger;

import java.util.Calendar;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeTransactionCommitContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class FRSStageTrigger extends EndDateTrigger {
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path lastModifiedTimeFieldPath = Path.parse("./LAST_MODIFIED_TIME");
	
	@Override
	public void setup(TriggerSetupContext context) {
	
		SchemaNode node = context.getSchemaNode();
		if (node.getNode(lastModifiedTimeFieldPath) == null)
			context.addError("Field - " + this.lastModifiedTimeFieldPath
					+ " not found on table.");
		
	}
	
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		
		context.setAllPrivileges();
		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		vc.setValue(Calendar.getInstance().getTime(), lastModifiedTimeFieldPath);
	LOG.info("handle before create ");
		super.handleBeforeCreate(context);
	}
	
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		
		
		context.setAllPrivileges();
		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		vc.setValue(Calendar.getInstance().getTime(), lastModifiedTimeFieldPath);
		LOG.info("handle before modify ");
		super.handleBeforeModify(context);
	}
	
	
	@Override
	public void handleBeforeTransactionCommit(
			BeforeTransactionCommitContext arg0) throws OperationException {
		// TODO Auto-generated method stub
		LOG.info("transaction commited" );
		super.handleBeforeTransactionCommit(arg0);
	}
	
	
	/*@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		LOG.info("sart of method : NewDSMTHierarchyTrigger");
		final Adaptation newRec = context.getAdaptationOccurrence();
		//final AdaptationTable aTab = newRec.getContainerTable();
		final ProcedureContext pContext = context.getProcedureContext();
		
		Repository repo=  context.getAdaptationHome().getRepository();
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo, GOCConstants.DSMT2_HIERARCHY_DATA_SPACE, GOCConstants.DSMT2_HIERARCHY_DATA_SPACE);
		
		
		String targetTableName = newRec.getContainerTable().getTablePath().format().replaceAll("_STG", "");
		LOG.info("new table = "+targetTableName);
		
		
		final AdaptationTable targetTable = metadataDataSet.getTable( Path.parse(targetTableName));
		
		
		final ValueContextForUpdate vc =pContext.getContextForNewOccurrence(
				newRec, targetTable);
		
			
		LOG.info("get data ");
		pContext.doCreateOccurrence(vc,  targetTable);
		
		 super.handleAfterCreate(context);
		
	}*/

}
