package com.citi.ebx.dsmt.trigger;

import java.util.Calendar;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_PROD_TYPE_Role_Mapping_Matching;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.onwbp.adaptation.UnavailableContentError;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class RoleMappingIDTrigger extends AuditColumnsTrigger {
	private Path effDateFieldPath = DSMTConstants.effDateFieldPath;

	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context) throws OperationException {

		DSMTUtils.handleBeforeDelete(context);

	}

	public void handleNewContext(NewTransientOccurrenceContext context) {

		try {
			context.getOccurrenceContextForUpdate().setValue("", effDateFieldPath);

			super.handleNewContext(context);

		} catch (UnavailableContentError e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		}

		catch (PathAccessException e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			LOG.debug("Error found while creation of Record : " + e);
			e.printStackTrace();
		}
	}

	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		
	
		Repository rp=context.getAdaptationHome().getRepository();
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(rp,DSMTConstants.DATASPACE_DSMT2Hier,DSMTConstants.DATASET_DSMT2Hier);
		AdaptationTable mapping_table=metadataDataSet.getTable(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_PROD_TYPE_Mapping_Table.getPathInSchema());
		AdaptationTable role_table=metadataDataSet.getTable(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_PROD_TYPE_Role_Details.getPathInSchema());
		
		//final AdaptationTable roleTable;
		final Adaptation newRec = context.getAdaptationOccurrence();
		final String Mapping_Name=newRec.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_PROD_TYPE_Role_Mapping_Matching._Mapping_Name);
		final String Role_name=newRec.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_PROD_TYPE_Role_Mapping_Matching._Role_Name);
		
		
		//final AdaptationTable aTab = newRec.getContainerTable();

		final ProcedureContext pContext = context.getProcedureContext();
		final ValueContextForUpdate vc = pContext.getContext(newRec
				.getAdaptationName());

		// Search for record we occur after
			String searchpred1=null;
			String searchpred2=null;	
			
			searchpred1=new String( DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_PROD_TYPE_Role_Details._Role_Name.format() +" = '" + Role_name+"'");
			searchpred2=new String(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_PROD_TYPE_Mapping_Table._Mapping_Name.format()+" = '"+Mapping_Name);
			RequestResult roleRes = role_table.createRequestResult(searchpred1);
			RequestResult mappingRes = mapping_table.createRequestResult(searchpred2);
			
			Adaptation roleRecord=roleRes.nextAdaptation();
			String roleID=roleRecord.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_PROD_TYPE_Role_Details._Role_ID);
			
			Adaptation mappingRecord=mappingRes.nextAdaptation();
			String mappingID=mappingRecord.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_PROD_TYPE_Mapping_Table._Mapping_ID);

			
		pContext.setAllPrivileges(true);
		//vc.setValue(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_PROD_TYPE_Role_Details._Role_ID,roleID);
		//vc.setValue(newRecEnd, endDateFieldPath);
		pContext.doModifyContent(newRec, vc);

		// Update existing record to end when this record begins
		pContext.setAllPrivileges(false);
		super.handleAfterCreate(context);
	}
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException {

		context.setAllPrivileges();
		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();

		super.handleBeforeCreate(context);
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context) throws OperationException {
		context.setAllPrivileges();

		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		super.handleBeforeModify(context);
	}

}