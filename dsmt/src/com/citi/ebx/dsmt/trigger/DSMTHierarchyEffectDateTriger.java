package com.citi.ebx.dsmt.trigger;

import java.util.Date;
import java.util.List;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class DSMTHierarchyEffectDateTriger extends DSMTHierarchyTrigger {

	private Path effDateFieldPath = DSMTConstants.effDateFieldPath;
	
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		// System.out.println("ENTERED handlebeforeCreate");
		context.setAllPrivileges();

		final ValueContextForUpdate vc = context
				.getOccurrenceContextForUpdate();
		String userID = context.getSession().getUserReference().getUserId();

		/** Capitalize the userID */
		if (null != userID) {
			userID = userID.toUpperCase();
		}

		// handle webservice update scenarios
		

		final Date contextDate = (Date) vc.getValue(effDateFieldPath);
		final String accntCode = (String) vc.getValue(DSMTConstants.MRL_Account_Code) ;
		LOG.debug("contextDate" + contextDate);
		//final int newEff = contextDate.getDate();
		//LOG.debug("newEff" + newEff);
		//if (newEff != 01) {
		
		
		final String predicate = DSMTConstants.MRL_Account_Code.format() + " = \""+ accntCode+"\"";
		
		final List<Adaptation> listOfDuplicate = context.getTable().selectOccurrences(predicate);
		if(listOfDuplicate.size() == 0){
			contextDate.setDate(01);
			contextDate.setMonth(00);
			contextDate.setYear(0);
			LOG.debug("DATE should always be set to 01/01/1900 for new account record");
			vc.setValue(contextDate, effDateFieldPath);
			
		}
		//}
		super.handleBeforeCreate(context);
	}
}


