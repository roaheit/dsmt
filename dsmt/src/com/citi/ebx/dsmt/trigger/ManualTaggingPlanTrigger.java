package com.citi.ebx.dsmt.trigger;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class ManualTaggingPlanTrigger extends HierarchyTrigger {

	
	
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException{
		
	final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();

	
		final List<String> cblCode = (List<String>) vc.getValue(DSMTConstants.CBL_CD);
		final List<String> coCode = (List<String>) vc.getValue(DSMTConstants.CO_CD);
		final List<String> gfCode = (List<String>) vc.getValue(DSMTConstants.GF_CD);
		if(cblCode.size() < 1){
			List<String> dCBL = new ArrayList<String>();
			dCBL.add("NO_CBL");
			vc.setValue(dCBL, DSMTConstants.CBL_CD);
		}
		if(coCode.size()<1){
			List<String> dCO = new ArrayList<String>();
			dCO.add("NO_CO");
			vc.setValue(dCO, DSMTConstants.CO_CD);
		}
		if(gfCode.size()<1){
			List<String> dGF = new ArrayList<String>();
			dGF.add("NO_GF");
			vc.setValue(dGF, DSMTConstants.GF_CD);
		}
		super.handleBeforeCreate(context);
		
	}
	
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		
		final List<String> cblCode = (List<String>) vc.getValue(DSMTConstants.CBL_CD);
		final List<String> coCode = (List<String>) vc.getValue(DSMTConstants.CO_CD);
		final List<String> gfCode = (List<String>) vc.getValue(DSMTConstants.GF_CD);
		if(cblCode.size() < 1){
			List<String> dCBL = new ArrayList<String>();
			dCBL.add("NO_CBL");
			vc.setValue(dCBL, DSMTConstants.CBL_CD);
		}
		if(coCode.size()<1){
			List<String> dCO = new ArrayList<String>();
			dCO.add("NO_CO");
			vc.setValue(dCO, DSMTConstants.CO_CD);
		}
		if(gfCode.size()<1){
			List<String> dGF = new ArrayList<String>();
			dGF.add("NO_GF");
			vc.setValue(dGF, DSMTConstants.GF_CD);
		}
		super.handleBeforeModify(context);
		
	}
	
}
