package com.citi.ebx.dsmt.trigger;

import java.util.ArrayList;
import java.util.StringTokenizer;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class ControllerDetailsPopulationTrigger extends TableTrigger {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	String msIDFK;
	private Path controllerDetailPath = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID;
	private Path msIDPath = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID;
	private String tableToUpdate;

	String controller;
	String msID;
	String LVID;

	public String getTableToUpdate() {
		return tableToUpdate;
	}

	public void setTableToUpdate(String tableToUpdate) {
		this.tableToUpdate = tableToUpdate;
	}

	@Override
	public void setup(TriggerSetupContext context) {

	}

	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		final Adaptation newRec = context.getAdaptationOccurrence();
		String controller = newRec.getString(controllerDetailPath);
		String msid = newRec.getString(msIDPath);
		String managerName="";
		final ProcedureContext pContext = context.getProcedureContext();
		Adaptation currentDataSet = context.getTable().getContainerAdaptation();
		
		final Path tableUpdate = Path.parse(tableToUpdate);
		AdaptationTable targetTable = currentDataSet.getTable(tableUpdate);

		final String separator = "\\|";

		String[] parentID = msid.split(separator);

		PrimaryKey key = targetTable.computePrimaryKey(parentID);
		Adaptation record = targetTable.lookupAdaptationByPrimaryKey(key);

		if (record != null) {
			final ValueContextForUpdate vc = pContext.getContext(record
					.getAdaptationName());
			
			String controllerID = record
					.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID);
			String controllerName = record
							.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_Name);
			if (null == controllerID) {
				controllerID = controller;
			} else {
				controllerID = controllerID + "/" + controller;
			}
			managerName=newRec.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_Name);
			if (null == controllerName) {
				controllerName = managerName;
			} else {
				controllerName +=  "/" + managerName;
			}
			pContext.setAllPrivileges(true);
			
			vc.setValue(
					controllerID,
					AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID);
			vc.setValue(controllerName,
					AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_Name);
			
			pContext.doModifyContent(record, vc);
			pContext.setAllPrivileges(false);
		}
		final ValueContextForUpdate vc1 = pContext.getContext(newRec.getAdaptationName());
		
		pContext.setAllPrivileges(true);
		managerName=namePopulationForCreate(context);
		vc1.setValue(managerName,
				AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_Name);
		
		pContext.doModifyContent(newRec, vc1);
		pContext.setAllPrivileges(false);
		super.handleAfterCreate(context);
		
	}

	public void handleNewContext(NewTransientOccurrenceContext context) {
		super.handleNewContext(context);
	}

	@SuppressWarnings("deprecation")
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		ValueContext vc=context.getOccurrenceContextForUpdate();
		if(vc.getValue(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID)==null)
		{
			OperationException.createError("Controller ID cant be null/empty. Please enter the controller ID");
		}
		
		super.handleBeforeCreate(context);
	}
	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context)
			throws OperationException {
			
			final ProcedureContext pContext = context.getProcedureContext();
			final Adaptation newRec = context.getAdaptationOccurrence();
			Adaptation currentDataSet = context.getTable().getContainerAdaptation();
			String managerName="";
			if(null!= context.getChanges().getChange(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID)){
			
				String originalValue = context.getChanges().getChange(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID).getValueBefore()!=null  ? 
						(String) context.getChanges().getChange(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID).getValueBefore() : "";
				String changedValue = context.getChanges().getChange(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID).getValueAfter()!=null ?
						(String) context.getChanges().getChange(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID).getValueAfter() : "";
				
				if(!originalValue.equals(changedValue)){
				 	

					final Path tableUpdate = Path.parse(tableToUpdate);
					AdaptationTable targetTable = currentDataSet.getTable(tableUpdate);

					final String separator = "\\|";
					String msid = newRec.getString(msIDPath);
					String[] parentID = msid.split(separator);

					PrimaryKey key = targetTable.computePrimaryKey(parentID);
					Adaptation record = targetTable.lookupAdaptationByPrimaryKey(key); 
					if (record != null) {
						final ValueContextForUpdate vc = pContext.getContext(record
								.getAdaptationName());
						String controllerID = record
								.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID);
						if (null == controllerID) {
							controllerID = controller;
						} else {
							controllerID = controllerID.replace(originalValue, changedValue) ;
						}
						pContext.setAllPrivileges(true);
						
						vc.setValue(
								controllerID,
								AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID);
					
						pContext.doModifyContent(record, vc);
						pContext.setAllPrivileges(false);
						
					}
					
					pContext.setAllPrivileges(true);
					final ValueContextForUpdate vc1 = pContext.getContext(newRec.getAdaptationName());
					managerName=namePopulationForModify(context);
					vc1.setValue(managerName,
							AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_Name);
					pContext.doModifyContent(newRec, vc1);
					pContext.setAllPrivileges(false);
					super.handleAfterModify(context);
			 }
			}
		
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		if(null!= context.getChanges().getChange(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID)){
			throw OperationException
			.createError("Users  are not allowed to change value for Managed Segment ID ");
		}
		
		super.handleBeforeModify(context);
	}

	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
		Adaptation deletedRec = context.getAdaptationOccurrence();
		
		
		controller = deletedRec.getString(controllerDetailPath);
		msIDFK = deletedRec.getString(msIDPath);
		final String separator = "\\|";
		String[] msIDArray = msIDFK.split(separator);
		msID = msIDArray[0];
		LVID = msIDArray[1];
		// DSMTUtils.handleBeforeDelete(context);
	}

	@Override
	public void handleAfterDelete(AfterDeleteOccurrenceContext context)
			throws OperationException {
		String deletionrecord=context.getDeletedRecordXPathExpression();
		String modifiedControllers = "";
		Adaptation currentDataSet=context.getTable().getContainerAdaptation();
		final Path tableUpdate = Path.parse(tableToUpdate);
		AdaptationTable targetTable = currentDataSet.getTable(tableUpdate);
		//ArrayList<Adaptation> deletionRecord=new ArrayList<Adaptation>();
		final String predicate = "osd:contains-case-insensitive("
				+ AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID
						.format()
				+ ",'"
				+ controller
				+ "')"
				+ " and "
				+ AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_MAN_SEG_ID
						.format()
				+ "= '"
				+ msID
				+ "' and"
				+ AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_LVID
						.format() + " = '" + LVID + "'";

		RequestResult reqRes = targetTable.createRequestResult(predicate);
		if (reqRes.getSize() > 0) {
			
			Adaptation record = reqRes.nextAdaptation();
			
			String controllers = record
					.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID);
			ArrayList<String> controllersArray = new ArrayList<String>();
			if (controllers.contains(controller)) {
				StringTokenizer tokenString = new StringTokenizer(controllers,
						"/");
				while (tokenString.hasMoreElements()) {
					controllersArray.add(tokenString.nextToken());
				}
				
				controllersArray.remove(controller);
				if(controllersArray.size()>=1){
				for (String ca : controllersArray) {
					modifiedControllers += ca + "/";
				}
					modifiedControllers = modifiedControllers.substring(0,
						modifiedControllers.length() - 1);
				}
			}
			final ProcedureContext pContext = context.getProcedureContext();
			final ValueContextForUpdate vc = pContext.getContext(record
					.getAdaptationName());
			//deletionRecord=deleteChildRecord(msIDFK,controller,targetTable,context);
			pContext.setAllPrivileges(true);
			vc.setValue(
					modifiedControllers,
					AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID);
			pContext.doModifyContent(record, vc);
			/**for(Adaptation recordDel : deletionRecord)
			{
				if(recordDel!=null){
				System.out.println("Record For Deletion"+recordDel);
				pContext.doDelete(recordDel.getAdaptationName(), false);
				}
			}**/
			pContext.setAllPrivileges(false);
		}
		
		super.handleAfterDelete(context);
		
	}
	public ArrayList<Adaptation> deleteChildRecord(String msID, String controllerID,
			AdaptationTable targetTable,AfterDeleteOccurrenceContext context)
			throws OperationException {
		
		ArrayList<Adaptation> recordlist=new ArrayList<Adaptation>();
		final String predicate = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Parent_MS_ID
				.format() + "='" + msID + "'";
		RequestResult rs = targetTable.createRequestResult(predicate);
		
		ProcedureContext pContext = context.getProcedureContext();
		pContext.setAllPrivileges(true);

		if (rs.getSize() > 0) {
			for (Adaptation record; (record = rs.nextAdaptation()) != null;) {
				recordlist.add(deleteRecord(record, context, controllerID));
				deleteChildRecord(record.getOccurrencePrimaryKey().format(),
						controllerID, targetTable,context);
			}
		}
		rs.close();
		return recordlist;
	}
	private Adaptation deleteRecord(final Adaptation record,
			AfterDeleteOccurrenceContext context, String controllerID){
			AdaptationTable currenttable=context.getOccurrenceContext().getAdaptationTable();
			Adaptation recordDeletion = null;
			final String controllerDetailsPredicate = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID
				.format() + "='" + record.getOccurrencePrimaryKey().format()
				+ "' and "
				+ "osd:contains-case-insensitive("
				+ AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID
				.format()
				+ ",'"
				+ controller
				+ "')";
			RequestResult reqRes=currenttable.createRequestResult(controllerDetailsPredicate);
			if(reqRes.getSize()>0)
			{
				recordDeletion=reqRes.nextAdaptation();
			}
			return recordDeletion;
	}
	
	private String namePopulationForCreate(AfterCreateOccurrenceContext context)
	{
		String managerName="";
		final Adaptation newRec = context.getAdaptationOccurrence();
		AdaptationTable managerTable = newRec.getContainerTable().getContainerAdaptation().getTable(Path.parse("./root/ACC_MAT/C_DSMT_MANAGER"));

		String controller = newRec.getString(controllerDetailPath);
		if(context.getSession().getAttribute("SOEID")==null || context.getSession().getAttribute("SOEID").toString()!=controller|| !context.getSession().getAttribute("SOEID").toString().equalsIgnoreCase(controller)){
			
/*			Repository rp=context.getAdaptationHome().getRepository();
			Adaptation metadataDataSet2 = null;
			try {
				metadataDataSet2 = EBXUtils.getMetadataDataSet(rp, DSMTConstants.DATASPACE_DSMT2Hier,DSMTConstants.DATASET_DSMT2Hier);
			} catch (OperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			AdaptationTable managerTable=metadataDataSet2.getTable(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER.getPathInSchema());*/
			final String managerPredicate = "osd:contains-case-insensitive("+ DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._MANAGER_ID.format()+",'" + controller + "')";
			RequestResult rs2=managerTable.createRequestResult(managerPredicate);
			Adaptation managerRecord;
			
			if(rs2.getSize()>0)
			{
				managerRecord=rs2.nextAdaptation();	
				managerName=managerRecord.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._C_GDW_EMP_NAME);
			}
			context.getSession().setAttribute("SOEID", controller);	
			context.getSession().setAttribute("MANAGERNAME", managerName);
			}
			else{
				managerName=context.getSession().getAttribute("MANAGERNAME").toString();
			}
		
		return managerName;
	}
	
	private String namePopulationForModify(AfterModifyOccurrenceContext context)
	{
		String managerName="";
		final Adaptation newRec = context.getAdaptationOccurrence();
		AdaptationTable managerTable = newRec.getContainerTable().getContainerAdaptation().getTable(Path.parse("./root/ACC_MAT/C_DSMT_MANAGER"));
		String controller=newRec.getString(controllerDetailPath);
		if(context.getSession().getAttribute("SOEID")==null || context.getSession().getAttribute("SOEID").toString()!=controller ||  !context.getSession().getAttribute("SOEID").toString().equalsIgnoreCase(controller)){
			
			/*Repository rp=context.getAdaptationHome().getRepository();
			Adaptation metadataDataSet2 = null;
			try {
				metadataDataSet2 = EBXUtils.getMetadataDataSet(rp, DSMTConstants.DATASPACE_DSMT2Hier,DSMTConstants.DATASET_DSMT2Hier);
			} catch (OperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		//	AdaptationTable managerTable=metadataDataSet2.getTable(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER.getPathInSchema());
			final String managerPredicate = "osd:contains-case-insensitive("+ DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._MANAGER_ID.format()+",'" + controller + "')";
			RequestResult rs2=managerTable.createRequestResult(managerPredicate);
			Adaptation managerRecord;
			
			if(rs2.getSize()>0)
			{
				managerRecord=rs2.nextAdaptation();	
				managerName=managerRecord.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._C_GDW_EMP_NAME);
			}
			context.getSession().setAttribute("SOEID", controller);	
			context.getSession().setAttribute("MANAGERNAME", managerName);
			}
			else{
				managerName=context.getSession().getAttribute("MANAGERNAME").toString();
			}
		
		return managerName;
	}
}