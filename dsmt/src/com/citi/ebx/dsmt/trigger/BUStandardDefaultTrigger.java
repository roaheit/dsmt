package com.citi.ebx.dsmt.trigger;

import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class BUStandardDefaultTrigger extends TableTrigger {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	@Override
	public void setup(TriggerSetupContext arg0) {
		// TODO Auto-generated method stub
		LOG.info("in setup()");
	}
	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		LOG.info("in handleAfterCreate()....");
		Adaptation record = context.getAdaptationOccurrence();
		Adaptation currentDataSet = context.getTable().getContainerAdaptation();
		ProcedureContext pContext = context.getProcedureContext();
		updateBUDefaults(record,currentDataSet,pContext);
	}
	public void handleAfterModify(AfterModifyOccurrenceContext context) 
			throws OperationException {
		LOG.info("in handleAfterModify()....");
		Adaptation record = context.getAdaptationOccurrence();
		Adaptation currentDataSet = context.getTable().getContainerAdaptation();
		ProcedureContext pContext = context.getProcedureContext();
		updateBUDefaults(record,currentDataSet,pContext);
	}
	public void updateBUDefaults(Adaptation record,Adaptation currentDataSet,ProcedureContext pContext) throws OperationException {
		RequestResult rs =  null;
		AdaptationTable CntrlerTable = null;
		ValueContextForUpdate valueContext = null;
		String BU_Val = record.get(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_BU_Standard_Defaults._C_DSMT_FRS_BU).toString();
		LOG.info("BU_Val::>>:"+BU_Val);
		String predicate = DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_FRS_BU.format() + "='"	+ BU_Val + "'";
		CntrlerTable = currentDataSet.getTable(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL.getPathInSchema());
		rs = CntrlerTable.createRequestResult(predicate);
		String ATTR_ID = "", DomicileVal = "", lvidVal= "";
		if( rs.getSize() > 0){
			for (Adaptation rec; (rec = rs.nextAdaptation()) != null;) {
				if(rec !=null) {
					ATTR_ID = rec.get(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_ATTR_ID).toString();
					if(ATTR_ID != null && ATTR_ID.equalsIgnoreCase("REGULATORY_DOMICILE"))
						DomicileVal = rec.get(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_ATT_VAL_ID).toString();
					else if(ATTR_ID != null && ATTR_ID.equalsIgnoreCase("LVID"))
						lvidVal = rec.get(DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_C_DSMT_F_BUSVAL._C_DSMT_ATT_VAL_ID).toString();
				}
			}
			LOG.info("DomicileVal:::"+DomicileVal+"<<<:lvidVal:>>>"+lvidVal);
			if(!DomicileVal.equals("") && !lvidVal.equals("")){
				valueContext=pContext.getContext(record.getAdaptationName());
				valueContext.setValue(DomicileVal, DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_BU_Standard_Defaults._FRS_BU_Domicile);
				valueContext.setValue(lvidVal, DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OTH_FRS_INT_STD_BU_Standard_Defaults._C_DSMT_LVID);
								
				pContext.setAllPrivileges(true);
				pContext.setTriggerActivation(false);
				pContext.doModifyContent(record,  valueContext);
				pContext.setTriggerActivation(true);
				pContext.setAllPrivileges(false);
				LOG.info("Updated current record"); 
			}
		}
	}
}
