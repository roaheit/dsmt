package com.citi.ebx.dsmt.trigger;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class ControllerNamePopulationTrigger extends TableTrigger {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	String msIDFK;
	private Path controllerIDDetailPath = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID;
	String controller;
	
	@Override
	public void setup(TriggerSetupContext context) {

	}

	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		final Adaptation newRec = context.getAdaptationOccurrence();
		final AdaptationTable aTab = newRec.getContainerTable();
		String controller = newRec.getString(controllerIDDetailPath);
		String managerName="";
		if(context.getSession().getAttribute("SOEID")==null || context.getSession().getAttribute("SOEID").toString()!=controller){
			
		Repository rp=context.getAdaptationHome().getRepository();
		Adaptation metadataDataSet2=EBXUtils.getMetadataDataSet(rp, DSMTConstants.DATASPACE_DSMT2Hier,DSMTConstants.DATASET_DSMT2Hier);
		AdaptationTable managerTable=metadataDataSet2.getTable(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER.getPathInSchema());
		final String managerPredicate = "osd:contains-case-insensitive("+ DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._MANAGER_ID.format()+",'" + controller + "')";
		RequestResult rs2=managerTable.createRequestResult(managerPredicate);
		Adaptation managerRecord;
		
		if(rs2.getSize()>0)
		{
			managerRecord=rs2.nextAdaptation();	
			managerName=managerRecord.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._C_GDW_EMP_NAME);
		}
		context.getSession().setAttribute("SOEID", controller);	
		context.getSession().setAttribute("MANAGERNAME", managerName);
		
	}
		else{
			managerName=context.getSession().getAttribute("MANAGERNAME").toString();
		}
		final ProcedureContext pContext = context.getProcedureContext();
		final ValueContextForUpdate vc = pContext.getContext(newRec.getAdaptationName());
			pContext.setAllPrivileges(true);
			vc.setValue(managerName,
					AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_Name);
			pContext.doModifyContent(newRec, vc);
			pContext.setAllPrivileges(false);
			super.handleAfterCreate(context);
}
	public void handleNewContext(NewTransientOccurrenceContext context) {
		super.handleNewContext(context);
	}

	@SuppressWarnings("deprecation")
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context)
			throws OperationException {
		super.handleBeforeCreate(context);
	}
	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context)
			throws OperationException {
		final Adaptation newRec = context.getAdaptationOccurrence();
		final AdaptationTable aTab = newRec.getContainerTable();
		String controller = newRec.getString(controllerIDDetailPath);
		String managerName="";
		if(context.getSession().getAttribute("SOEID")==null || context.getSession().getAttribute("SOEID").toString()!=controller){
			
		Repository rp=context.getAdaptationHome().getRepository();
		Adaptation metadataDataSet2=EBXUtils.getMetadataDataSet(rp, DSMTConstants.DATASPACE_DSMT2Hier,DSMTConstants.DATASET_DSMT2Hier);
		AdaptationTable managerTable=metadataDataSet2.getTable(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER.getPathInSchema());
		final String managerPredicate = "osd:contains-case-insensitive("+ DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._MANAGER_ID.format()+",'" + controller + "')";
		RequestResult rs2=managerTable.createRequestResult(managerPredicate);
		Adaptation managerRecord;
		
		if(rs2.getSize()>0)
		{
			managerRecord=rs2.nextAdaptation();	
			managerName=managerRecord.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._C_GDW_EMP_NAME);
		}
		context.getSession().setAttribute("SOEID", controller);	
		context.getSession().setAttribute("MANAGERNAME", managerName);
		}
		else{
			managerName=context.getSession().getAttribute("MANAGERNAME").toString();
		}
		final ProcedureContext pContext = context.getProcedureContext();
		final ValueContextForUpdate vc = pContext.getContext(newRec.getAdaptationName());
			pContext.setAllPrivileges(true);
			vc.setValue(managerName,
					AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_Name);
			pContext.doModifyContent(newRec, vc);
			pContext.setAllPrivileges(false);
						super.handleAfterModify(context);	
	
	}
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context)
			throws OperationException {
		if(null!= context.getChanges().getChange(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID)){
			throw OperationException
			.createError("Users  are not allowed to change value for Managed Segment ID ");
		}
		
		super.handleBeforeModify(context);
	}

	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
		Adaptation deletedRec = context.getAdaptationOccurrence();
		
	}

	@Override
	public void handleAfterDelete(AfterDeleteOccurrenceContext context)
			throws OperationException {
	
		super.handleAfterDelete(context);
		
	}
}