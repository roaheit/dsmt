package com.citi.ebx.dsmt.trigger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.openjpa.lib.log.Log;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.TableDuplication;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class NewDSMTGenericTrigger extends DSMTGenericTrigger {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	public String tablePath;
	public String columnId;
	public String dataSpace;
	public String dataSet;
	private String endDateField = "./ENDDT";
	private String effDate = "./EFFDT";
	final SimpleDateFormat dateFormat = new SimpleDateFormat(DSMTConstants.DEFAULT_DATE_FORMAT);
	private static final String MAX_END_DATE = "9999-12-31";
	//private Path DESCR = Path.parse("./DESCR");
	//private Path DESCRSHORT = Path.parse("./DESCRSHORT");

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context)
			throws OperationException {
		
		
		final ProcedureContext pContext = context.getProcedureContext();
		final Adaptation newRec = context.getAdaptationOccurrence();
		
		final Adaptation dataset=context.getTable().getContainerAdaptation();
		// final AdaptationTable aTab = newRec.getContainerTable();
		LOG.debug("inside Generic Hierarchy triger");
		//if (!isInChildDataSpace(dataset)) {
		String currentDataSet = newRec.getContainerTable().toString();
		String childDataSpace = context.getAdaptationHome().getLabelOrName(Locale.getDefault()).toString();
		LOG.info("checking child dataspace "+childDataSpace);

		if (currentDataSet.contains(dataSet)) {
			Repository repo = context.getAdaptationHome().getRepository();
			Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
					childDataSpace, dataSet);
			final AdaptationTable targetTable = metadataDataSet
					.getTable(getPath(tablePath));
			String keyValue1 = newRec.getString(getPath(columnId));
			final PrimaryKey pk = targetTable
					.computePrimaryKey(new String[] { keyValue1 });
			Adaptation targetRecord = targetTable
					.lookupAdaptationByPrimaryKey(pk);
			
			final ValueContextForUpdate vc = pContext
					.getContextForNewOccurrence(newRec, targetTable);
			vc.setValue(newRec.getString(getPath(columnId)), getPath(columnId));
			if (targetRecord == null) {
				targetRecord = pContext.doCreateOccurrence(vc, targetTable);
			} else {
				targetRecord = pContext.doModifyContent(newRec, vc);
			}
		} else {
			try {
				TableDuplication tableDuplication = new TableDuplication(
						newRec, context.getAdaptationHome().getRepository(),
						tablePath, columnId, dataSpace, dataSet);
				Utils.executeProcedure(
						tableDuplication,
						context.getSession(),
						context.getAdaptationHome().getRepository()
								.lookupHome(HomeKey.forBranchName(childDataSpace)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		super.handleAfterCreate(context);
	}

	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context)
			throws OperationException {

		final Adaptation newRec = context.getAdaptationOccurrence();

		final ProcedureContext pContext = context.getProcedureContext();
		String currentDataSet = newRec.getContainerTable().toString();
		final Adaptation dataset=context.getTable().getContainerAdaptation();
		String childDataSpace = context.getAdaptationHome().getLabelOrName(Locale.getDefault()).toString();
		LOG.info("checking child dataspace "+childDataSpace);
		
		final Date endDate = newRec.getDate(Path.parse(endDateField));
		String dateStr = dateFormat.format(endDate);

		//if (!isInChildDataSpace(dataset)) {
		if (endDate == null || MAX_END_DATE.equals(dateStr)) {
		if (currentDataSet.contains(dataSet)) {
			Repository repo = context.getAdaptationHome().getRepository();
			Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
					childDataSpace, dataSet);
			final AdaptationTable targetTable = metadataDataSet
					.getTable(getPath(tablePath));
			String keyValue1 = newRec.getString(getPath(columnId));
			final PrimaryKey pk = targetTable
					.computePrimaryKey(new String[] { keyValue1 });

			Adaptation targetRecord = targetTable
					.lookupAdaptationByPrimaryKey(pk);

			final ValueContextForUpdate vc;
			if (targetRecord == null) {
				vc = pContext.getContextForNewOccurrence(newRec,targetTable);
				vc.setValue(keyValue1, getPath(columnId));
			} else {
				vc = pContext.getContextForNewOccurrence(newRec,targetTable);
			}
			if (targetRecord == null) {
				targetRecord = pContext.doCreateOccurrence(vc, targetTable);
			} else {
				targetRecord = pContext.doModifyContent(targetRecord, vc);
			}
		} else {
			try {
				TableDuplication tableDuplication = new TableDuplication(
						newRec, context.getAdaptationHome().getRepository(),
						tablePath, columnId, dataSpace, dataSet);
				Utils.executeProcedure(
						tableDuplication,
						context.getSession(),
						context.getAdaptationHome().getRepository()
								.lookupHome(HomeKey.forBranchName(childDataSpace)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		}
		super.handleAfterModify(context);

	}

	public void handleBeforeDelete(BeforeDeleteOccurrenceContext context)
			throws OperationException {
		// Need to implement
	}



	public void handleAfterDelete(AfterDeleteOccurrenceContext context)
			throws OperationException {
		Repository repo = context.getAdaptationHome().getRepository();
		final AdaptationHome gocDataSpace = repo.lookupHome(
				HomeKey.forBranchName(dataSpace));
		//LOG.info(" current dataset value : "+ context.getOccurrenceContext().getAdaptationTable().getContainerAdaptation().toString());
		String currentDataSet = context.getTable().getContainerAdaptation().toString();
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,
				dataSpace, dataSet);
		AdaptationTable targetTable = metadataDataSet
				.getTable(getPath(tablePath));
		
		final String rec = context.getDeletedRecordXPathExpression();
		String[] sToken = rec.split("\\'");
		for(int i=0; i<sToken.length;i++){
			LOG.info("token values" + sToken[i]);
		}
		ProcedureContext procCtx = context.getProcedureContext();
		procCtx.setAllPrivileges(true);
		
		String effDatePredicate =  " date-equal(" + Path.parse(effDate).format() + ", '"+ sToken[3] + "')";
		
		final String predicate = Path.parse(columnId).format() + "='" + sToken[1] + "'" ;
		LOG.info(" predicate  >>>>>>  "+ predicate);
		LOG.info(" currentDataSet  >>>>>>  "+ currentDataSet);
		
		
		RequestResult resultSet =targetTable.createRequestResult(predicate);
		Adaptation record = null;

		if (resultSet != null&& resultSet.getSize()>0) {
			record = resultSet.nextAdaptation();
					
		// call procedure to udpate record
			if (currentDataSet.contains(dataSet)) {
				procCtx.doDelete(record.getAdaptationName(), false);
			} else{
				final DeleteRecordFromChildTablesProcedure copyProc = new DeleteRecordFromChildTablesProcedure(targetTable, record);
				Utils.executeProcedure(copyProc, context.getSession(), gocDataSpace);
			}
			
		
		}
		
		//super.handleAfterDelete(context);

	}
	private class DeleteRecordFromChildTablesProcedure implements Procedure {
		private AdaptationTable table;
		private Adaptation record;
		
		public DeleteRecordFromChildTablesProcedure(AdaptationTable table,Adaptation record) {
			this.table = table;
			this.record= record;
		}
		//GOCConstants.DSMT2_RELATIONAL_DATASET
		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			
			
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			pContext.doDelete(record.getAdaptationName(), false);
			
		}
	}
		

	
	
	
	
	private static boolean isInChildDataSpace(final Adaptation dataSet) {

		LOG.info("isInChildDataSpace called ");
		AdaptationHome dataspace = dataSet.getHome();

		if (dataspace.toString().contains("DSMT2")) {
			LOG.info("Is in master DataSpace");
			return false;
		} else {
			LOG.info("Is in Child DataSpace");
			return true;
		}
	}

	public String getTablePath() {
		return tablePath;
	}

	public void setTablePath(String tablePath) {
		this.tablePath = tablePath;
	}

	public String getColumnId() {
		return columnId;
	}

	public void setColumnId(String columnId) {
		this.columnId = columnId;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	private Path getPath(final String fieldName) {

		return Path.parse(fieldName);
	}
}
