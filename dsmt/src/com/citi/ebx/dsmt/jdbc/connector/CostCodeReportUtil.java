package com.citi.ebx.dsmt.jdbc.connector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;

//@author Pratik pg72656 
// This class generates report on the basis of country mapping , and generates single files for each each country . 

public class CostCodeReportUtil {


	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	
	

	protected String reportType;
	
	
	final String env =  propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);

    protected String sqlID ;
    protected String exportFileDirectoryPath;
    protected String exportFileName;
    protected static Connection dbConnection = null;
    protected static ArrayList<String> reportNames ;
    protected static ArrayList<String> sqlNames  ;
    protected static String sqlFile;
    protected DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    protected Calendar cal = Calendar.getInstance();
	
    protected static final LoggingCategory LOG = LoggingCategory.getKernel();
    
    public void generateCostCodeReports (final Connection conection, String outputFilePath) {    	
    	CostCodeReportUtil reportUtil = new CostCodeReportUtil();
    	try{
    		   
    	 	 dbConnection=conection;
    	 	LOG.info("CostCodeReportUtil-->generateCostCodeReports   sqlfilename-->"+reportType + "getReportType : "+getReportType());
    		 /** get default output file path if value is not passed in as a parameter */
    		 if(null == outputFilePath){
    			 outputFilePath = SQLReportConnectionUtils.bundle.getString(env + DSMTConstants.DOT +"ExportFileDirectory");
    		 }
    		 getSQlReportMappingRelative();
    		 reportUtil.generateCostCodeReportfiles(SQLReportConnectionUtils.bundle.getString(env + DSMTConstants.DOT + "SqlFileDirectory"), outputFilePath );
			}
    	catch(SQLException e)
    	{
    		LOG.error ("Error inside method  CostCodeReportUtil-->generateCostCodeReports  ->" + e + " , message " + e.getMessage()); 
    	}
	}
    
    public void generateCostCodeReportfiles (final String sqlFileDirectory, final String exportFileDirectory) throws SQLException
	{	
		try {
			int reportcount=0;
			LOG.debug("CostCodeReportUtil-->generateCostCodeReportfiles   starts-->");
				exportFileDirectoryPath= exportFileDirectory;
				//This method will generate execute the query and write each row in given file
				for (String sqlfilename : sqlNames) {
					// get the sql query from the given file location
					LOG.info("CostCodeReportUtil-->generateCostCodeReportfiles   sqlfilename-->"+sqlfilename);
					sqlID = getInputQuery(sqlFileDirectory, sqlfilename);
					sqlFile=sqlfilename;
					exportFileName = reportNames.get(reportcount);
					//This method will generate execute the query and write each row in given file as per the report type
					if(sqlfilename.contains("Flex")){
						selectRecordsFromFlexCubeTable(); 
					}else{
						selectRecordsFromTable();
					}
					reportcount++;	
				}
		} catch (SQLException e) {
			LOG.error (" CostCodeReportUtil-->generateCostCodeReportfiles Error inside method  generateCostCodeReportfiles  ->" + e + " , message " + e.getMessage()); 
		} catch(Exception e) {
			LOG.error (" CostCodeReportUtil-->generateCostCodeReportfiles Error inside method  generateCostCodeReportfiles  ->" + e + " , message " + e.getMessage());
		}
		finally{
			if (dbConnection != null) {
				dbConnection.close();
			}
			
		}
		  
		
	}
    
    /**
   	 * Helper method to get the O/P from the file table
   	 * @param  
   	 * @throws SQLException
   	 */   
    
    
    
    private  void selectRecordsFromTable() throws SQLException {
		 
		
		PreparedStatement preparedStatement = null;
		
		File file = null;
		FileOutputStream fos = null;
		
				 		 
		try {
			LOG.info("CostCodeReportUtil-->selectRecordsFromTable  : SQl Connect report started for report name  = " + exportFileName + " for report type "+ getReportType()); 
			// Creating prepare statement.
			preparedStatement = dbConnection.prepareStatement(sqlID);
			// execute select SQL statement
			ResultSet rs = preparedStatement.executeQuery();
			ResultSetMetaData rsMetaData = rs.getMetaData();
			// get the count of column to generate the report.
			int columncount = rsMetaData.getColumnCount();
			// to get record counts in report
			int recordcount=0;
			
			if(! exportFileDirectoryPath.endsWith("/")){
				exportFileDirectoryPath = exportFileDirectoryPath + "/";
			}
			LOG.debug("CostCodeReportScheduledtask :--> getExportFileDirectoryPath "+exportFileDirectoryPath);
			//This logic is used for TGL_TSR file name 
			if(exportFileName.equalsIgnoreCase("TGL_TREAS_SEG.dat")){
			    String TglTsrFileName;
				TglTsrFileName=exportFileName.replace(".dat","_"+dateFormat.format(cal.getTime())+".dat" );
				exportFileName=TglTsrFileName;	
			}
			file = new File(exportFileDirectoryPath+exportFileName);
			
			file.setReadable(true, false);
			file.setWritable(true, true);
			
			fos = new FileOutputStream(file);
			while (rs.next()) {

				StringBuffer reportfile= new StringBuffer();
				recordcount++;
					for(int i=0 ; i<columncount; i++ )
					{
							String data= rs.getString(i+1);
							
							if(DSMTUtils.isStringNotEmptyOrNull(data))
							{
								
								reportfile.append(data);
							}
							else
							{
								reportfile.append(DSMTConstants.BLANK_STRING);
							}
						
						
					}
				
				reportfile.append(DSMTConstants.NEXT_LINE);
				byte[] contentInBytes = reportfile.toString().getBytes();
				fos.write(contentInBytes);
				
			}
			
			LOG.info("CostCodeReportUtil-->selectRecordsFromTable  : SQl Connect report complete and Total record count is " + recordcount  +" in "+ exportFileName);
			
 
		} catch (SQLException e) {
			LOG.error (" CostCodeReportUtil-->selectRecordsFromTable  : Error  while excuting the query for report  >>>>" + exportFileName + ". Exception ->" + e + " , message " + e.getMessage() );
			
 
		}
		 catch (FileNotFoundException e) {
			 
			 LOG.error (" CostCodeReportUtil-->selectRecordsFromTable  : Error  while creating   : " + exportFileName + ". Exception ->" + e + " , message " + e.getMessage() );
	 
			} 
		catch (IOException e) {
			 
			 LOG.error ("  CostCodeReportUtil-->selectRecordsFromTable : Error  while writing   : " + exportFileName + ". Exception ->" + e + " , message " + e.getMessage() );
 
		}
		finally {
 
			if (preparedStatement != null) {
				preparedStatement.close();
			}
 
			try {
				if (fos != null) {
					fos.close();
				}
				
				} catch (IOException e) {
					LOG.error ("CostCodeReportUtil-->selectRecordsFromTable :   Error  while closing " + exportFileName + ". Exception ->" + e + " , message " + e.getMessage() );
				
			}
		
			sqlID = DSMTConstants.BLANK_STRING;
 
		}
		
 
	}  
    

    
    /**
   	 * Helper method to get the O/P from the file table
   	 * @param  
   	 * @throws SQLException
   	 */   
    
	public void selectRecordsFromFlexCubeTable() throws SQLException {

		PreparedStatement preparedStatement = null;
		File fileCsv = null;
		FileOutputStream fosCsv = null;
		int countrySequence=0;
		Map<String, StringBuffer> flexCubeReportMap = new HashMap<String, StringBuffer>();
		//map to store the sid mapped with data for csv file; "," seperated file
		Map<String, StringBuffer> flexCubeMapCsv = new HashMap<String, StringBuffer>();
		//map to store the sid mapped with reord count
		Map<String, Integer> flexCubeCountryCountMap = new HashMap<String, Integer>();

		try {
			LOG.debug(" CostCodeReportUtil-->selectRecordsFromFlexCubeTable SQl Connect report started for sql  = "
					+ sqlFile + " for report type " + getReportType());

			preparedStatement = dbConnection.prepareStatement(sqlID);
			ResultSet rs = preparedStatement.executeQuery();
			ResultSetMetaData rsMetaData = rs.getMetaData();
			int columncount = rsMetaData.getColumnCount();
			int recordcount = 0;
			for (int i = 1; i <= columncount; i++) {
				if(rsMetaData.getColumnName(i).equals("COUNTRY")){
					countrySequence = i;
					}
				
			}
			if (!exportFileDirectoryPath.endsWith("/")) {
				exportFileDirectoryPath = exportFileDirectoryPath + "/";
			}
			String country = "";

			while (rs.next()) {
				StringBuffer reportfileDat = new StringBuffer();
				StringBuffer reportfileCSV = new StringBuffer();

				recordcount++;
				for (int i = 0; i < columncount; i++) {
					String data = rs.getString(i + 1);
					//get the country value of the record
					if (i == countrySequence-1) {
						
						country = data;
						continue ;
					}
					if (DSMTUtils.isStringNotEmptyOrNull(data)) {
						reportfileDat.append(data);
						reportfileCSV.append(data.trim());
					} else {
						reportfileDat.append("");
						reportfileCSV.append("");
					}
					//append the seperator for csv file 
					if (i < columncount - 2) {
						reportfileCSV.append(",");
					}
				}
				
				reportfileDat.append(DSMTConstants.NEXT_LINE);
				reportfileCSV.append(DSMTConstants.NEXT_LINE);
				//Create map for different
				if (flexCubeReportMap.containsKey(country)) {
					flexCubeReportMap.put(
								country,
								flexCubeReportMap.get(country).append(
									reportfileDat.toString()));
						
					flexCubeMapCsv.put(
								country,
								flexCubeMapCsv.get(country).append(
									reportfileCSV.toString()));
					flexCubeCountryCountMap.put(country, flexCubeCountryCountMap.get(country) + 1);
				} else {
					flexCubeReportMap.put(country, generateHeader(reportfileDat));
					
					//flexCubeMapCsv.put(country, generateHeaderCsv(reportfileCSV,rsMetaData));
					flexCubeMapCsv.put(
							country,
								reportfileCSV);
					flexCubeCountryCountMap.put(country, 1);
				}

			}
			for (String fileSID : flexCubeReportMap.keySet()) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
				Date date = new Date();
				   Calendar cal = Calendar.getInstance();
				String FLXC =dateFormat.format(cal.getTime());
				String suffix = null;
				if(sqlFile.contains("T_16")){
					suffix="_OUC_";
				}
				if(sqlFile.contains("T_17")){
					suffix="_TRANS-OUC_";
				} 
				if(sqlFile.contains("T_18")){
					suffix="_UDF-STDCUSAC_";
				}
				if(sqlFile.contains("T_19")){
					suffix="_UDF-STDCIF_";
				}
				if(sqlFile.contains("T_20") || sqlFile.contains("T_25")){
					suffix="_EXP-OUC_";
				}
				if(sqlFile.contains("T_21") || sqlFile.contains("T_24") || sqlFile.contains("T_26")){
					suffix="_BR-RESTRIC_";
				}
				if(sqlFile.contains("T_22") || sqlFile.contains("T_23") ){
					suffix="_APA-CODE_";
				}
				if(sqlFile.contains("T_27")  ){
					suffix="_CLASS-CODE_";
				}
				
				
				
				/*byte[] contentInBytes = flexCubeReportMap
						.get(fileSID)
						.append(generateTrailer(fileSID,
								flexCubeCountryCountMap.get(fileSID).toString())
								.toString()).toString().getBytes();

				fileDat = new File(exportFileDirectoryPath
						+ fileSID + suffix 
						+ FLXC +".dat");
				//System.out.println("****************************"+fileDat);
				LOG.debug(" CostCodeReportUtil-->selectRecordsFromFlexCubeTable : SQl Connect report started for report name  = "
						+ fileDat );*/

				byte[] contentInBytesCsv = flexCubeMapCsv
						.get(fileSID).toString().getBytes();
				
				
				// File fileDir=new File(exportFileDirectoryPath);
				// System.out.println("fileDir-->"+fileDir.mkdir());
				//fileDat.setReadable(true, false);
				//fileDat.setWritable(true, true);
				// fosDat = new FileOutputStream(fileDat);
				//fosDat.write(contentInBytes);
				//fosDat.close();
				
				fileCsv = new File(exportFileDirectoryPath
						+ fileSID + suffix 
						+ FLXC + ".csv");
               
			
				LOG.debug(" CostCodeReportUtil-->selectRecordsFromFlexCubeTable : o?P file name is "
						+ fileCsv);
				fileCsv.setReadable(true, false);
				fileCsv.setWritable(true, true);
				fosCsv = new FileOutputStream(fileCsv);
				fosCsv.write(contentInBytesCsv);
				fosCsv.close();

			}

			
			LOG.info(" CostCodeReportUtil-->selectRecordsFromFlexCubeTable : Total record count in "
					+ exportFileName + " is " + recordcount);
		} catch (SQLException e) {
			LOG.error(" CostCodeReportUtil-->selectRecordsFromFlexCubeTable :  Error  while excuting the query for report  >>>>"
					+ exportFileName
					+ ". Exception ->"
					+ e
					+ " , message "
					+ e.getMessage());

		} catch (FileNotFoundException e) {

			LOG.error(" CostCodeReportUtil-->selectRecordsFromFlexCubeTable :  Error  while creating  >>>>"
					+ exportFileName + ". Exception ->"	+ e	+ " , message "	+ e.getMessage());

		} catch (IOException e) {

			LOG.error("  CostCodeReportUtil-->selectRecordsFromFlexCubeTable :  Error  while writing  >>>>"
					+ exportFileName
					+ ". Exception ->"
					+ e
					+ " , message "
					+ e.getMessage());

		} 
		finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}

		}
	}  
	
		
	/**
   	 * Helper method to generate the header for dat file
   	 * @param  stringbuffer
   	 * @throws 
   	 */  
	
    
    
	public StringBuffer generateHeader(StringBuffer buffer){
		StringBuffer header= new StringBuffer();
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		 final Calendar calendar = Calendar.getInstance();
		 //header.append(DSMTConstants.FMSREPORTHEADER);
		 //header.append(sdf.format(calendar.getTime()));
		 //header.append(StringUtils.rightPad(DSMTConstants.FMSREPORTHEADER+sdf.format(calendar.getTime()), 80, " "));
		 header.append("Header to be decided");
		 header.append(DSMTConstants.NEXT_LINE);
		 header.append(buffer);
		return header;
	}
	/**
   	 * Helper method to generate the header for CSV file
   	 * @param  strngbuffer  , resultsetmetadata to get column names
   	 * @throws 
   	 */  
	
	
	public StringBuffer generateHeaderCsv(StringBuffer buffer,ResultSetMetaData rsMetaData) throws SQLException{
		StringBuffer header= new StringBuffer();
		 int columncount = rsMetaData.getColumnCount();
		 for (int i = 0; i < columncount; i++) {
				String columnName = rsMetaData.getColumnName(i+1);
				header.append(columnName);
				if (i < columncount - 1) {
					header.append(",");
				}
			}
		 
		
		 header.append(DSMTConstants.NEXT_LINE);
		 header.append(buffer);
		return header;
	}
	
	/**
   	 * Helper method to generate the trailer for dat file
   	 * @param  sid  , record count
   	 * @throws 
   	 */  
	
	
	public StringBuffer generateTrailer(String sid,String recordCount){
		StringBuffer trailer= new StringBuffer();
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		 final Calendar calendar = Calendar.getInstance();
		trailer.append(StringUtils.rightPad( StringUtils.rightPad(
				DSMTConstants.FMSREPORTRAILER + sdf.format(calendar.getTime())+DSMTConstants.SPACE
						+ StringUtils.rightPad(recordCount, 9, " "), 47, "0"), 80, " "));
		
		
		return trailer;
		
	}
	 
	/**
   	 * Helper method to generate the trailer for CSV file
   	 * @param  sid  , record count
   	 * @throws 
   	 */  
	
	public StringBuffer generateTrailerCsv(String sid,String recordCount){
		StringBuffer trailer= new StringBuffer();
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		 final Calendar calendar = Calendar.getInstance();
		
		trailer.append(DSMTConstants.FMSREPORTRAILER + sdf.format(calendar.getTime())+DSMTConstants.SPACE+ recordCount);
		
		return trailer;
		
	}
    /**
   	 * Helper method to read Sql from file
   	 * @param  sqlFileDirectory , sqlFile
   	 * @throws 
   	 */       
    
	public String getInputQuery(String sqlFileDirectory,
			String sqlFile) {
		LOG.debug("getInputQuery  " + sqlFileDirectory + "    " + sqlFile);
		StringBuffer inputquery = new StringBuffer();
		BufferedReader br = null;
		String line = null;
		try {
			br = new BufferedReader(new FileReader(sqlFileDirectory + sqlFile));
			while ((line = br.readLine()) != null) {

				inputquery.append(line);
				inputquery.append(DSMTConstants.NEXT_LINE);

			}

		} catch (FileNotFoundException e) {

			LOG.error("Unable to find SQl file  >>>>" + sqlFile
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} catch (IOException e) {

			LOG.error("Unable to read SQl file " + sqlFile + ". Exception ->"
					+ e + " , message " + e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return inputquery.toString();
	}
    

	
	 public String getReportType() {
			return reportType;
		}



		public void setReportType(String reportType) {
			this.reportType = reportType;
		}

		
 
		public void getSQlReportMappingRelative()
		{

					BufferedReader br = null;
					reportNames= new ArrayList<String>();
					sqlNames= new ArrayList<String>();
					int count_of_sql=0;
					try {
					
							String line = DSMTConstants.BLANK_STRING;
							String csvSplitBy = DSMTConstants.EQUALS;
							LOG.debug("getSQlReportMappingRelative--->"+getReportType()+env);
								br = new BufferedReader(new FileReader(SQLReportConnectionUtils.bundle.getString(env + DSMTConstants.DOT + "SqlMAppingFile"+DSMTConstants.DOT+DSMTConstants.COSTCODE)));	
							
							while ((line = br.readLine()) != null) {
									String[] values = line.split(csvSplitBy);
										if(null!=reportType) {
											//LOG.debug("sql type >>" +values[0]);
											if(values[0].contains(reportType)){
												sqlNames.add(count_of_sql, values[0]);
												reportNames.add(count_of_sql, values[1]);
												count_of_sql++;
											}
										}else {
											sqlNames.add(count_of_sql, values[0]);
										     reportNames.add(count_of_sql, values[1]);
										     count_of_sql++;
											}
							}
							
					} catch (FileNotFoundException e) {
						 
						LOG.error ("Unable to find SQl property file  >>>> for report Type "+getReportType() +" : "+ e.getMessage() );
				 
						} 
					catch (IOException e) {
						 
						LOG.error ("Unable to read SQl property file " + e.getMessage());
					}
					finally {
						if (br != null) {
							try {
								br.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
					LOG.debug("NO of report ->> " +count_of_sql+ " Report Names --> "+ sqlNames);
					}
		
		
}
