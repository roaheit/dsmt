package com.citi.ebx.dsmt.jdbc.connector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;

public class FRSAccountsIncrementalReportsUtil {
	
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	final String env  =  propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
	protected String sqlID;
	protected String exportFileDirectoryPath;
	protected String exportFileName;
	protected static Connection dbConnection = null;
	protected static ArrayList<String> reportNames;
	protected static ArrayList<String> sqlNames;
	protected static Map<String, Integer> readmeMap;
	protected String SQLName;
	
	
	
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();



	public void generateReport(String outputFilePath) throws SQLException {

		try {
			// get the sql query from the given file location
			if (null == outputFilePath) {

				outputFilePath = SQLReportConnectionUtils.bundle.getString(env
						+ DSMTConstants.DOT + "ExportFileDirectory");
			}
			String sqlfilename = "FRS_Incremental_Report.sql";
			if(getSQLName()!=null){
				sqlfilename=getSQLName();
			}
			
			LOG.info("getExportFileName >>   " +getExportFileName());
			if(getExportFileName()!=null){
				this.exportFileName=getExportFileName();
			}
			else{
				this.exportFileName = "DSMT_FRS_ACCOUNT_ONLY.csv";
			}
				sqlID = getInputQuery(SQLReportConnectionUtils.bundle.getString(env + DSMTConstants.DOT + "SqlFileDirectory"), sqlfilename);
				LOG.info(" exportFileName >>   " +exportFileName);
				exportFileDirectoryPath = outputFilePath;
				
				// This method will generate execute the query and write each
				// row in given file
					createIncrementalReports();
				
			

		} catch (SQLException e) {
			LOG.error("Error inside method  generateCurrencyRpt  ->" + e
					+ " , message " + e.getMessage());
		} finally {
			if (dbConnection != null) {
				dbConnection.close();
			}

		}

	}
/*
 * read the SQL placed in the server file system *
 * 
 */
     protected static String getInputQuery(String sqlFileDirectory,
			String sqlFile) {
		LOG.info("getInputQuery  " + sqlFileDirectory + "    " + sqlFile);
		StringBuffer inputquery = new StringBuffer();
		BufferedReader br = null;
		String line = null;
		try {
			br = new BufferedReader(new FileReader(sqlFileDirectory + sqlFile));
			while ((line = br.readLine()) != null) {

				inputquery.append(line);
				inputquery.append(DSMTConstants.NEXT_LINE);

			}

		} catch (FileNotFoundException e) {

			LOG.error("Unable to find SQl file  >>>>" + sqlFile
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} catch (IOException e) {

			LOG.error("Unable to read SQl file " + sqlFile + ". Exception ->"
					+ e + " , message " + e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return inputquery.toString();
	}
 /*
  * Create the report after executing the SQL 
  * 
  */
	private void createIncrementalReports() throws SQLException {

		PreparedStatement preparedStatement = null;

		File file = null;
		FileOutputStream fos = null;
		readmeMap= new java.util.HashMap<String, Integer>();
		try {
			LOG.info(" createIncrementalReports >> SQl Connect report started ");
			// Creating prepare statement.
			dbConnection= SQLReportConnectionUtils.getConnection();
			preparedStatement = dbConnection.prepareStatement(sqlID);
			// execute select SQL statement
			ResultSet rs = preparedStatement.executeQuery();
			ResultSetMetaData rsMetaData = rs.getMetaData();
			// get the count of column to generate the report.
			int columncount = rsMetaData.getColumnCount();
			// to get record counts in report
			int recordcount = 0;

			if (!exportFileDirectoryPath.endsWith("/")) {
				exportFileDirectoryPath = exportFileDirectoryPath + "/";
			}
			file = new File(exportFileDirectoryPath + exportFileName);

			file.setReadable(true, false);
			file.setWritable(true, true);

			fos = new FileOutputStream(file);

			StringBuffer header = generateIncrementalHeader(rsMetaData);

			fos.write(header.toString().getBytes());

			while (rs.next()) {

				StringBuffer reportfile = new StringBuffer();
				recordcount++;
				for (int i = 0; i < columncount; i++) {
					String data = rs.getString(i + 1);
					
					if (DSMTUtils.isStringNotEmptyOrNull(data)) {
						reportfile.append(data);
					} else {
						reportfile.append(DSMTConstants.BLANK_STRING);
					}
					if (i < columncount - 1) {
						reportfile.append(",");
					}

				}

				reportfile.append(DSMTConstants.NEXT_LINE);
				byte[] contentInBytes = reportfile.toString().getBytes();
				fos.write(contentInBytes);

			}
			readmeMap.put(exportFileName, recordcount);
			// T|699263

			String trailer = DSMTConstants.TRAILER + DSMTConstants.COMMA
					+ recordcount + DSMTConstants.NEXT_LINE;
			fos.write(trailer.getBytes());
			LOG.info("Total record count in " + exportFileName + " is "
					+ recordcount);

		} catch (SQLException e) {
			LOG.error(" Error  while excuting the query for report  >>>>"
					+ exportFileName + ". Exception ->" + e + " , message "
					+ e.getMessage());

		} catch (FileNotFoundException e) {

			LOG.error(" Error  while creating  >>>>" + exportFileName
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} catch (IOException e) {

			LOG.error("  Error  while writing  >>>>" + exportFileName
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} 
		catch (Exception e) {

			e.printStackTrace();

		}finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}
			dbConnection.close();

			try {
				if (fos != null) {
					fos.close();
				}

			} catch (IOException e) {
				LOG.error("  Error  while closing  >>>>" + exportFileName
						+ ". Exception ->" + e + " , message " + e.getMessage());

			}

			sqlID = DSMTConstants.BLANK_STRING;

		}

	}

	/*
	 * H|col1|clo2|col3
	 */
	private StringBuffer generateIncrementalHeader(ResultSetMetaData rsMetaData)
			throws SQLException {

		StringBuffer header = new StringBuffer();
		int columncount = rsMetaData.getColumnCount();
		for (int i = 0; i < columncount - 1; i++) {
			String columnName = rsMetaData.getColumnName(i + 1);
			header.append(columnName);
			if (i < columncount - 1) {
				header.append(",");
			}
		}

		header.append(DSMTConstants.NEXT_LINE);

		return header;

	}
	/**
	 * @return the exportFileName
	 */
	public String getExportFileName() {
		return exportFileName;
	}
	/**
	 * @param exportFileName the exportFileName to set
	 */
	public void setExportFileName(String exportFileName) {
		this.exportFileName = exportFileName;
	}
	/**
	 * @return the sQLName
	 */
	public String getSQLName() {
		return SQLName;
	}
	/**
	 * @param sQLName the sQLName to set
	 */
	public void setSQLName(String sQLName) {
		SQLName = sQLName;
	}

}
