package com.citi.ebx.dsmt.jdbc.connector;

import java.io.IOException;
import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class CreateDatasource
 */
public class CreateDatasource extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost( request,  response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection conn=null;
	
	try{
			
			Context aContext = new InitialContext();
			DataSource aDataSource = (DataSource)aContext.lookup("jdbc/EBX_REPOSITORY");
			conn = aDataSource.getConnection();
			conn.setReadOnly(true);
			
			SQLConnectReportUtil connectReportUtil = new SQLConnectReportUtil();
			
			connectReportUtil.generateReport(conn, null);
			
		}
		catch (Exception e)
		{
			System.err.println(e);
		}

	}

}
