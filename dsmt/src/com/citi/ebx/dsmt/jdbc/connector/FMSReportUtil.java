package com.citi.ebx.dsmt.jdbc.connector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;

public class FMSReportUtil {


	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	
	

	private String reportType;
	
	
	final String env =  propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);

    protected String sqlID ;
    protected String exportFileDirectoryPath;
    protected String exportFileName;
    protected static Connection dbConnection = null;
    protected DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    protected Calendar cal = Calendar.getInstance();
    protected int countrySequence=0;
	
    
    
	
    protected static final LoggingCategory LOG = LoggingCategory.getKernel();
    
    public void generateReport (final Connection conection, String outputFilePath) {    	
    	FMSReportUtil reportUtil = new FMSReportUtil();
    	try{
    		dbConnection=conection;
    		 /** get default output file path if value is not passed in as a parameter */
    		 if(null == outputFilePath){
    			 outputFilePath = SQLReportConnectionUtils.bundle.getString(env + DSMTConstants.DOT +"ExportFileDirectory");
    		 }
    		 reportUtil.generateFMSReport(SQLReportConnectionUtils.bundle.getString(env + DSMTConstants.DOT + "SqlFileDirectory"), outputFilePath );
		 }
    	catch(SQLException e)
    	{
    		LOG.error ("Error inside method FMSReportUtil-->generateReport  ->" + e + " , message " + e.getMessage()); 
    	}
    	
	}
    
    public void generateFMSReport (final String sqlFileDirectory, final String exportFileDirectory) throws SQLException
	{	
		try {
			
			
				sqlID = getInputQuery(sqlFileDirectory, "FMS_Report.sql");
//				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH mm ss");
//         		Calendar cal = Calendar.getInstance();
//				String FMS ="FMS_" + dateFormat.format(cal.getTime());
				exportFileDirectoryPath= exportFileDirectory;
				LOG.debug("FMSReportUtil-->generateFMSReport exportFileDirectoryPath is "+exportFileDirectoryPath);
				//This method will generate execute the query and write each row in given file
				selectRecordsFromTable(); 	
			
		} catch (SQLException e) {
			LOG.error ("Error inside method  generateCurrencyRpt  ->" + e + " , message " + e.getMessage()); 
		}
		finally{
			if (dbConnection != null) {
				dbConnection.close();
			}
			
		}
		  
		
	}
    
    /**
   	 * Helper method to get the O/P from the file table
   	 * @param  
   	 * @throws SQLException
   	 */   
    
	public void selectRecordsFromTable() throws SQLException {

		PreparedStatement preparedStatement = null;
		File fileDat = null;
		File fileCsv = null;
		FileOutputStream fosDat = null;
		FileOutputStream fosCsv = null;
		//map to store the sid mapped with data for dat file
		Map<String, StringBuffer> fmsReportMap = new HashMap<String, StringBuffer>();
		//map to store the sid mapped with data for csv file; "," seperated file
		Map<String, StringBuffer> fmsReportMapCsv = new HashMap<String, StringBuffer>();
		//map to store the sid mapped with reord count
		Map<String, Integer> fmsSidCountMap = new HashMap<String, Integer>();

		try {
			LOG.info("FMSReportUtil--> selectRecordsFromTable SQl Connect report started " );

			preparedStatement = dbConnection.prepareStatement(sqlID);
			ResultSet rs = preparedStatement.executeQuery();
			ResultSetMetaData rsMetaData = rs.getMetaData();
			int columncount = rsMetaData.getColumnCount();
			int countrySequence=0;
			int recordcount = 0;
			for (int i = 1; i <= columncount; i++) {
				if(rsMetaData.getColumnName(i).equalsIgnoreCase("SID")){
					countrySequence = i;
					}
				
			}
			
			if (!exportFileDirectoryPath.endsWith("/")) {
				exportFileDirectoryPath = exportFileDirectoryPath + "/";
			}
			String sid = "";

			while (rs.next()) {
				StringBuffer reportfileDat = new StringBuffer();
				StringBuffer reportfileCSV = new StringBuffer();

				recordcount++;
				for (int i = 0; i < columncount; i++) {
					String data = rs.getString(i + 1);
					//get the Sid value of the record
					if (i == countrySequence-1) {
						
							sid = data;
						continue ;
					}
					if (DSMTUtils.isStringNotEmptyOrNull(data)) {
						reportfileDat.append(data);
						reportfileCSV.append(data.trim());
					} else {
						reportfileDat.append("");
						reportfileCSV.append("");
					}
					//append the seperator for csv file 
					if (i < columncount - 2) {
						reportfileCSV.append(",");
					}
				}
				
				reportfileDat.append(DSMTConstants.NEXT_LINE);
				reportfileCSV.append(DSMTConstants.NEXT_LINE);
				//Create map for different sid 
				if (fmsReportMap.containsKey(sid)) {
						fmsReportMap.put(
									sid,
									fmsReportMap.get(sid).append(
									reportfileDat.toString()));
						
						fmsReportMapCsv.put(
							sid,
							fmsReportMapCsv.get(sid).append(
									reportfileCSV.toString()));
					fmsSidCountMap.put(sid, fmsSidCountMap.get(sid) + 1);
				} else {
					fmsReportMap.put(sid, generateHeader(reportfileDat));
					
					fmsReportMapCsv.put(sid, generateHeaderCsv(reportfileCSV,rsMetaData));
					fmsSidCountMap.put(sid, 1);
				}
			}
			
			// logic to create Files
			for (String fileSID : fmsReportMap.keySet()) {

				byte[] contentInBytes = fmsReportMap
						.get(fileSID)
						.append(generateTrailer(fileSID,
								fmsSidCountMap.get(fileSID).toString())
								.toString()).toString().getBytes();

				String fileSIDName = fileSID.trim();
				

				
				fileDat = new File(exportFileDirectoryPath
						+ DSMTConstants.FMSREPORTNAME + fileSIDName
						+ DSMTConstants.FMSREPORTDAT.replace(".dat",dateFormat.format(cal.getTime())+".dat" ));
				

				byte[] contentInBytesCsv = fmsReportMapCsv
						.get(fileSID).toString().getBytes();
				/*File fileDir=new File(exportFileDirectoryPath);
				// creating Directory for FMS reports.
				if(!fileDir.exists()){
					if (fileDir.mkdir()) {
						LOG.debug("Directory is created!" + fileDir);
					} else {
						LOG.debug("Failed to create directory!");
					}					
				}*/
				

				fileDat.setReadable(true, false);
				fileDat.setWritable(true, true);
				fosDat = new FileOutputStream(fileDat);
				fosDat.write(contentInBytes);
				fosDat.close();
				
				
				fileCsv = new File(exportFileDirectoryPath
						+ DSMTConstants.FMSREPORTNAME + fileSIDName
						+ DSMTConstants.FMSREPORTCSV.replace(".csv",dateFormat.format(cal.getTime())+".csv" ));
				fileCsv.setReadable(true, false);
				fileCsv.setWritable(true, true);
				fosCsv = new FileOutputStream(fileCsv);
				fosCsv.write(contentInBytesCsv);
				fosCsv.close();

			}

			
			LOG.info(" FMSReportUtil--> selectRecordsFromTable Total record count in " + exportFileName + " is "
					+ recordcount);
		} catch (SQLException e) {
			LOG.error("FMSReportUtil-->  Error  while excuting the query for report  >>>>"
					+ exportFileName + ". Exception ->" + e + " , message "
					+ e.getMessage());

		} catch (FileNotFoundException e) {

			LOG.error(" Error  while creating  >>>>" + exportFileName
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} catch (IOException e) {

			LOG.error("  Error  while writing  >>>>" + exportFileName
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}

		}
	}  
	
	
	
	/**
   	 * Helper method to generate the header for dat file
   	 * @param  stringbuffer
   	 * @throws 
   	 */  
	
    
    
	public StringBuffer generateHeader(StringBuffer buffer){
		StringBuffer header= new StringBuffer();
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		 final Calendar calendar = Calendar.getInstance();
		 //header.append(DSMTConstants.FMSREPORTHEADER);
		 //header.append(sdf.format(calendar.getTime()));
		 header.append(StringUtils.rightPad(DSMTConstants.FMSREPORTHEADER+sdf.format(calendar.getTime()), 80, " "));
		 header.append(DSMTConstants.NEXT_LINE);
		 header.append(buffer);
		return header;
	}
	/**
   	 * Helper method to generate the header for CSV file
   	 * @param  strngbuffer  , resultsetmetadata to get column names
   	 * @throws 
   	 */  
	
	
	public StringBuffer generateHeaderCsv(StringBuffer buffer,ResultSetMetaData rsMetaData) throws SQLException{
		StringBuffer header= new StringBuffer();
		 int columncount = rsMetaData.getColumnCount();
		 for (int i = 0; i < columncount-1; i++) {
				String columnName = rsMetaData.getColumnName(i+1);
				header.append(columnName);
				if (i < columncount - 2) {
					header.append(",");
				}
			}
		 
		
		 header.append(DSMTConstants.NEXT_LINE);
		 header.append(buffer);
		return header;
	}
	
	/**
   	 * Helper method to generate the trailer for dat file
   	 * @param  sid  , record count
   	 * @throws 
   	 */  
	
	
	public StringBuffer generateTrailer(String sid,String recordCount){
		StringBuffer trailer= new StringBuffer();
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		 final Calendar calendar = Calendar.getInstance();
		trailer.append(StringUtils.rightPad( StringUtils.rightPad(
				DSMTConstants.FMSREPORTRAILER + sdf.format(calendar.getTime())+DSMTConstants.SPACE
						+ StringUtils.rightPad(recordCount, 9, " "), 47, "0"), 80, " "));
		
		
		return trailer;
		
	}
	 
	/**
   	 * Helper method to generate the trailer for CSV file
   	 * @param  sid  , record count
   	 * @throws 
   	 */  
	
	public StringBuffer generateTrailerCsv(String sid,String recordCount){
		StringBuffer trailer= new StringBuffer();
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		 final Calendar calendar = Calendar.getInstance();
		
		trailer.append(DSMTConstants.FMSREPORTRAILER + sdf.format(calendar.getTime())+DSMTConstants.SPACE+ recordCount);
		
		return trailer;
		
	}
    /**
   	 * Helper method to read Sql from file
   	 * @param  sqlFileDirectory , sqlFile
   	 * @throws 
   	 */       
    
	public String getInputQuery(String sqlFileDirectory,
			String sqlFile) {
		LOG.debug("getInputQuery  " + sqlFileDirectory + "    " + sqlFile);
		StringBuffer inputquery = new StringBuffer();
		BufferedReader br = null;
		String line = null;
		try {
			br = new BufferedReader(new FileReader(sqlFileDirectory + sqlFile));
			while ((line = br.readLine()) != null) {

				inputquery.append(line);
				inputquery.append(DSMTConstants.NEXT_LINE);

			}

		} catch (FileNotFoundException e) {

			LOG.error("Unable to find SQl file  >>>>" + sqlFile
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} catch (IOException e) {

			LOG.error("Unable to read SQl file " + sqlFile + ". Exception ->"
					+ e + " , message " + e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return inputquery.toString();
	}

	 public String getReportType() {
			return reportType;
		}



		public void setReportType(String reportType) {
			this.reportType = reportType;
		}

		
}
