package com.citi.ebx.dsmt.jdbc.connector;

import java.sql.Connection;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.orchestranetworks.service.LoggingCategory;

public class SQLReportConnectionUtils {

	
	/**
	 * Helper method for generating new report  
	 * @param  sqlFileDirectory , sqlFileName, exportFileDirectory , exportFile
	 * @throws 
	 */
    
	public static final ResourceBundle bundle = ResourceBundle.getBundle("com.citi.ebx.dsmt.jdbc.connector.resources.SQLConnectResource");
	  
    /**
	 * Helper method for generating new report  
	 * @param  sqlFileDirectory , sqlFileName, exportFileDirectory , exportFile
	 * @throws 
	 */
	
    public static Connection getConnection(){
    	final LoggingCategory LOG = LoggingCategory.getKernel();
    	Connection connection = null;
    	final String dataSourceJNDI = bundle.getString("ebx.datasource.jndi");    // jdbc/SQLReports
    
    	try{
			
			Context aContext = new InitialContext();
			DataSource aDataSource = (DataSource)aContext.lookup(dataSourceJNDI);
			connection = aDataSource.getConnection();
		
			
			LOG.debug("Connection created = " + connection); 
	
    	}
		catch (Exception e)
		{
			LOG.error ("Connection not established with data source " + dataSourceJNDI + ". Exception ->" + e + " , message " + e.getMessage());
			
		}
		
		return connection;
    }
    
    
}
