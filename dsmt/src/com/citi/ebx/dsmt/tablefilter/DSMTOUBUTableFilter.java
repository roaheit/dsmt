package com.citi.ebx.dsmt.tablefilter;

import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessageLocalized;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.TableRefFilter;
import com.orchestranetworks.schema.TableRefFilterContext;
import com.orchestranetworks.service.LoggingCategory;

public class DSMTOUBUTableFilter implements TableRefFilter {

	protected final LoggingCategory LOG = LoggingCategory.getKernel();

	private String message = null;

	private String statusField = "./EFF_STATUS";

	@Override
	public boolean accept(Adaptation record, ValueContext vc) {

		boolean isParentRecordActiveOrBothInactive = isParentRecordActiveOrBothInactive(
				record, vc);

		LOG.debug("isParentRecordActiveOrBothInactive = "
				+ isParentRecordActiveOrBothInactive);
		return isParentRecordActiveOrBothInactive;
	}

	private boolean isParentRecordActiveOrBothInactive(Adaptation parentRecord,
			ValueContext context) {

		final String currentRecordStatus = (String) context
				.getValue(getParent(statusField));
		final String parentRecordStatus = parentRecord
				.getString(getPath(statusField));

		if (null == currentRecordStatus || null == parentRecordStatus) { // THIS
																			// SCENARIO
																			// SHOULD
																			// NOT
																			// HAPPEN
																			// BUT
																			// JUST
																			// IN
																			// CASE
			return true;
		}

		final boolean isParentRecordActiveOrBothInactive = DSMTConstants.ACTIVE
				.equalsIgnoreCase(parentRecordStatus)
				|| (DSMTConstants.INACTIVE
						.equalsIgnoreCase(currentRecordStatus) && DSMTConstants.INACTIVE
						.equalsIgnoreCase(parentRecordStatus));
		return isParentRecordActiveOrBothInactive;

	}

	public String getStatusField() {
		return statusField;
	}

	public void setStatusField(String statusField) {
		this.statusField = statusField;
	}

	@Override
	public void setup(TableRefFilterContext context) {

		UserMessageLocalized localized = new UserMessageLocalized();
		localized.setMessage("The Parent record is inactive.");
		localized.setSeverity(Severity.ERROR);
		context.setFilterValidationMessage(localized);

	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext context)
			throws InvalidSchemaException {

		return message;

	}

	private Path getParent(final String fieldName) {

		return Path.parse(DSMTConstants.DOT + fieldName);
	}

	private Path getPath(final String fieldName) {

		return Path.parse(fieldName);
	}

}
