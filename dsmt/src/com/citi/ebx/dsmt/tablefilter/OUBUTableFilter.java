package com.citi.ebx.dsmt.tablefilter;

import java.util.Date;
import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessageLocalized;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.TableRefFilter;
import com.orchestranetworks.schema.TableRefFilterContext;
import com.orchestranetworks.service.LoggingCategory;

public class OUBUTableFilter implements TableRefFilter {

	protected final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private String message = null;
	
	private String endDateField = "./ENDDT";
	private String statusField = "./EFF_STATUS";
	
	
	private static Date lastEndDate = new Date(253402232400000L);
	
	
	@Override
	public boolean accept(Adaptation record, ValueContext vc) {
	
		
		final Date currentRecordEndDate = (Date) vc.getValue(getParent(endDateField));	
		boolean isCurrentRecordEndDated = lastEndDate.after(currentRecordEndDate);
		if(isCurrentRecordEndDated){
			return true;
		}
		
		boolean isParentEffectiveOrBothEndDated = isParentEffectiveOrBothEndDated(record, vc); // logic to be set up  isNodeFlagSetAsParent && isParentRecordActiveOrBothInactive && isChildEndDatedOrBothEffective ; 		
		boolean isParentRecordActiveOrBothInactive = isParentRecordActiveOrBothInactive(record, vc);
		
		LOG.debug("isParentEffectiveOrBothEndDated = " + isParentEffectiveOrBothEndDated + ", isParentRecordActiveOrBothInactive = " + isParentRecordActiveOrBothInactive);
		return isParentEffectiveOrBothEndDated && isParentRecordActiveOrBothInactive  ;
	}
	
	
	private boolean isParentEffectiveOrBothEndDated(Adaptation parentRecord, ValueContext context){
		
		
		
		final Date currentRecordEndDate = (Date) context.getValue(getParent(endDateField));	
		final Date parentRecordEndDate =  parentRecord.getDate(getPath(endDateField));
		
		if(null == currentRecordEndDate || null == parentRecordEndDate){ // THIS SCENARIO SHOULD NOT HAPPEN BUT JUST IN CASE
			return true;
		}
		
		
		
		// CHECK if the parent record end date is 12-31-9999 (NOT END DATED) or both the records are before 12-31-9999 (END DATED)
		final boolean isParentEffectiveOrBothEndDated =  (( null == parentRecordEndDate ) || lastEndDate.equals(parentRecordEndDate)) 
											|| 
										 ( lastEndDate.after(parentRecordEndDate) && lastEndDate.after(currentRecordEndDate));
		
		return isParentEffectiveOrBothEndDated;
		
		
	}
	
	
	private boolean isParentRecordActiveOrBothInactive(Adaptation parentRecord, ValueContext context){
		
		final String currentRecordStatus = (String) context.getValue(getParent(statusField));	
		final String parentRecordStatus =  parentRecord.getString(getPath(statusField));
		
		if(null == currentRecordStatus || null == parentRecordStatus){ // THIS SCENARIO SHOULD NOT HAPPEN BUT JUST IN CASE
			return true;
		}
		
		final boolean isParentRecordActiveOrBothInactive = DSMTConstants.ACTIVE.equalsIgnoreCase(parentRecordStatus) || ( DSMTConstants.INACTIVE.equalsIgnoreCase(currentRecordStatus) && DSMTConstants.INACTIVE.equalsIgnoreCase(parentRecordStatus));
		return isParentRecordActiveOrBothInactive;
		
		
	}
	
	

	public String getEndDateField() {
		return endDateField;
	}







	public void setEndDateField(String endDateField) {
		this.endDateField = endDateField;
	}





	public String getStatusField() {
		return statusField;
	}







	public void setStatusField(String statusField) {
		this.statusField = statusField;
	}







	@Override
	public void setup(TableRefFilterContext context) {
	
	
		UserMessageLocalized localized = new UserMessageLocalized();
		localized.setMessage("The Parent has been end dated with a non-end dated child or The Parent record is inactive.");
		localized.setSeverity(Severity.ERROR);
		context.setFilterValidationMessage(localized);
		

		
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext context)
			throws InvalidSchemaException {
				
		return message;
		
		
	}
	
	

	private Path getParent(final String fieldName) {
		
		return Path.parse(DSMTConstants.DOT + fieldName);
	}


	private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}


	

}
