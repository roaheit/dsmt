package com.citi.ebx.dsmt.tablefilter;

import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessageLocalized;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.TableRefFilter;
import com.orchestranetworks.schema.TableRefFilterContext;
import com.orchestranetworks.service.LoggingCategory;

public class DSMTHierarchyTableFilter implements TableRefFilter{


	protected final LoggingCategory LOG = LoggingCategory.getKernel();
	
	private String message = null;
	
	
	private String nodeFlagField = "./nodeFlag";
	private String statusField = "./EFF_STATUS";
	
	
	
	@Override
	public boolean accept(Adaptation record, ValueContext vc) {
	
		
		boolean isNodeFlagSetAsParent = isNodeFlagSetAsParent(record, vc);
		boolean isParentRecordActiveOrBothInactive = isParentRecordActiveOrBothInactive(record, vc);
		
		LOG.debug(" isNodeFlagSetAsParent = " + isNodeFlagSetAsParent + ", isParentRecordActiveOrBothInactive = " + isParentRecordActiveOrBothInactive);
		return isNodeFlagSetAsParent && isParentRecordActiveOrBothInactive  ;
	}
	
	
	private boolean isNodeFlagSetAsParent(Adaptation parentRecord, ValueContext vc) {
	
		final String parentRecordNodeFlag =  parentRecord.getString(getPath(nodeFlagField));
		
		if(null == parentRecordNodeFlag || DSMTConstants.BLANK_STRING.equalsIgnoreCase(parentRecordNodeFlag)){  // THIS SCENARIO SHOULD NOT HAPPEN BUT JUST IN CASE
			return true;
		}
		
		return DSMTConstants.PARENT.equalsIgnoreCase(parentRecordNodeFlag);
	}




	
	
	private boolean isParentRecordActiveOrBothInactive(Adaptation parentRecord, ValueContext context){
		
		final String currentRecordStatus = (String) context.getValue(getParent(statusField));	
		final String parentRecordStatus =  parentRecord.getString(getPath(statusField));
		
		if(null == currentRecordStatus || null == parentRecordStatus){ // THIS SCENARIO SHOULD NOT HAPPEN BUT JUST IN CASE
			return true;
		}
		
		final boolean isParentRecordActiveOrBothInactive = DSMTConstants.ACTIVE.equalsIgnoreCase(parentRecordStatus) || ( DSMTConstants.INACTIVE.equalsIgnoreCase(currentRecordStatus) && DSMTConstants.INACTIVE.equalsIgnoreCase(parentRecordStatus));
		return isParentRecordActiveOrBothInactive;
		
		
	}
	







	public String getNodeFlagField() {
		return nodeFlagField;
	}







	public void setNodeFlagField(String nodeFlagField) {
		this.nodeFlagField = nodeFlagField;
	}







	public String getStatusField() {
		return statusField;
	}







	public void setStatusField(String statusField) {
		this.statusField = statusField;
	}







	@Override
	public void setup(TableRefFilterContext context) {
	
	
		UserMessageLocalized localized = new UserMessageLocalized();
		localized.setMessage("The Node flag of the parent is not set as P or The Parent record is inactive.");
		localized.setSeverity(Severity.ERROR);
		context.setFilterValidationMessage(localized);
		

		
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext context)
			throws InvalidSchemaException {
				
		return message;
		
		
	}
	
	

	private Path getParent(final String fieldName) {
		
		return Path.parse(DSMTConstants.DOT + fieldName);
	}


	private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}


	



}
