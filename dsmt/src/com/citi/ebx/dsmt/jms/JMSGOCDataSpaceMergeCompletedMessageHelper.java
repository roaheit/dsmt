package com.citi.ebx.dsmt.jms;

import java.util.HashMap;
import java.util.Map;

public class JMSGOCDataSpaceMergeCompletedMessageHelper extends JMSMessageHelper {
	private static final String GOC_DATA_SPACE_MERGE_WORKFLOW_REQUEST = "GOCDataSpaceMergeWorkflowRequest";
	
	private String correlationID;
	private String errorMessage;
	private String errorDetails;
	
	private String replyToQueue;

	public String getCorrelationID() {
		return correlationID;
	}

	public void setCorrelationID(String correlationID) {
		this.correlationID = correlationID;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	@Override
	public String createMessageContent() {
		String statusAndErrors;
		if (errorMessage == null || "".equals(errorMessage)) {
			statusAndErrors = "<status>true</status>  ";
		} else {
			statusAndErrors = "<status>false</status>  "
					+ "<errorMessage>" + errorMessage + "</errorMessage>  "
					+ (errorDetails == null || "".equals(errorDetails)
							? "<errorDetails></errorDetails>  "
							: "<errorDetails>" + errorDetails + "</errorDetails>  ");
		}
		
		String xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>  " +
				"<response xmlns:ns0=\"http://com.citi.dsmt2.workflow\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns0:WorkflowResponse\">  " +
				"<correlationID>" + correlationID + "</correlationID>  " +
				statusAndErrors +
				"</response>";
		log.info("xmlMessage being sent = " + xmlMessage);
		return xmlMessage;
	}
	
	@Override
	protected Map<String, String> createJMSMap() {
		HashMap<String, String> jmsMap = new HashMap<String, String>();
		
		jmsMap.put("JMSReplyTo", queueName); 
		jmsMap.put("JMSCorrelationID", correlationID);
		jmsMap.put("JMSType", GOC_DATA_SPACE_MERGE_WORKFLOW_REQUEST);
		
		return jmsMap;
	}

	public void setReplyToQueue(String replyToQueue) {
		this.replyToQueue = replyToQueue;
	}

	public String getReplyToQueue() {
		return replyToQueue;
	}
}
