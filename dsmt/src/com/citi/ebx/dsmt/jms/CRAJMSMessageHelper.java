/**
 * 
 */
package com.citi.ebx.dsmt.jms;

import java.io.StringWriter;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.workflow.vo.CRARequest;
import com.orchestranetworks.service.LoggingCategory;

/**
 * @author rk00242
 *
 */
public class CRAJMSMessageHelper extends JMSTopicMessageHelper {
	private static final String EMPTY_STRING = DSMTConstants.BLANK_STRING;

	protected LoggingCategory log = LoggingCategory.getWorkflow();
	
	protected String eventType;
	protected String eventTimestamp;
	protected String systemName;
	protected String requestId;
	protected String reportingFrequency;
	protected String subjectArea;
	protected String reportingPeriod;
	protected String topicName;
	
	
	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventTimestamp() {
		return eventTimestamp;
	}

	public void setEventTimestamp(String eventTimestamp) {
		this.eventTimestamp = eventTimestamp;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getReportingFrequency() {
		return reportingFrequency;
	}

	public void setReportingFrequency(String reportingFrequency) {
		this.reportingFrequency = reportingFrequency;
	}

	public String getSubjectArea() {
		return subjectArea;
	}

	public void setSubjectArea(String subjectArea) {
		this.subjectArea = subjectArea;
	}


	public String getReportingPeriod() {
		return reportingPeriod;
	}

	public void setReportingPeriod(String reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}
	
	

	/* (non-Javadoc)
	 * @see com.citi.ebx.dsmt.jms.JMSMessageHelper#createMessageContent()
	 */
	@Override
	public String createMessageContent() {
		// TODO Auto-generated method stub
		String xmlMessage = generateRequest();
		
		log.info("check XML message : "+xmlMessage);
		
		return xmlMessage.toString();
	}

	/* (non-Javadoc)
	 * @see com.citi.ebx.dsmt.jms.JMSMessageHelper#createJMSMap()
	 */
	@Override
	protected Map<String, String> createJMSMap() {
		// TODO Auto-generated method stub
		return null;
	}
	
	protected String generateRequest(){
		String requestXml= EMPTY_STRING;
		
		
		try {
			CRARequest craRequest = new CRARequest();
			craRequest.setEventTimestamp(eventTimestamp);
			craRequest.setEventType(eventType);
			craRequest.setReportingFrequency(reportingFrequency);
			craRequest.setRequestId(requestId);
			craRequest.setSubjectArea(subjectArea);
			craRequest.setSystemName(systemName);
			
			CRARequest.ReportingList reportingList = new CRARequest.ReportingList();
			
			CRARequest.ReportingList.Reporting reporting = new CRARequest.ReportingList.Reporting();
			
			reporting.setPeriod(reportingPeriod);
			
			reportingList.setReporting(reporting);
			
			craRequest.setReportingList(reportingList);
			
			JAXBContext jaxbContext = JAXBContext.newInstance(CRARequest.class);
			Marshaller jaxbmMarshaller = jaxbContext.createMarshaller();
			jaxbmMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			StringWriter sw = new StringWriter();
			jaxbmMarshaller.marshal(craRequest, sw);
			
			StringBuffer buffer= sw.getBuffer();
			requestXml=buffer.toString();
			
			requestXml=requestXml.replace(":ns2", "");
			requestXml=requestXml.replace("ns2:", "");
			requestXml=requestXml.replace("&amp;", "&");
			requestXml=requestXml.replace("&lt;", "<");
			requestXml=requestXml.replace("&gt;", ">");
			
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error(e.getMessage());
		}
		
		
		return requestXml ;
	}

}
