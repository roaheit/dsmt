package com.citi.ebx.dsmt.jms;

import java.util.Hashtable;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;

import com.citi.ebx.dsmt.jms.connector.CitiJMSConnector;
import com.citi.ebx.dsmt.jms.connector.impl.CitiJMSConnectorImpl;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public abstract class JMSMessageHelper {
	protected static final String INITIAL_CONTEXT_FACTORY = "com.ibm.websphere.naming.WsnInitialContextFactory";
	protected static final String REFERRAL = "throw";
	protected static final String CONNECTION_FACTORY = "javax.jms.QueueConnectionFactory";
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	
	protected LoggingCategory log = LoggingCategory.getWorkflow();
	protected String queueName;
	protected Repository repository;
	protected com.orchestranetworks.service.Session session;
	protected InitialDirContext initialDirContext;
	
	public LoggingCategory getLog() {
		return log;
	}

	public void setLog(LoggingCategory log) {
		this.log = log;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
	
	public Repository getRepository() {
		return repository;
	}

	public void setRepository(Repository repository) {
		this.repository = repository;
	}

	public com.orchestranetworks.service.Session getSession() {
		return session;
	}

	public void setSession(com.orchestranetworks.service.Session session) {
		this.session = session;
	}

	public abstract String createMessageContent();
	
	protected abstract Map<String, String> createJMSMap();
	
	public void sendMessage(String msg, String sendQueue) throws OperationException {
		Map<String, String> jmsMap = createJMSMap();
		
		String queueCF = propertyHelper.getProp("queue.connection.factory");
		
		if(null == sendQueue){
			sendQueue = propertyHelper.getProp("send.queue.jndi");
		}
		CitiJMSConnector connector = new CitiJMSConnectorImpl();
		
		try {
			log.info("JMSTableComparisonMessageHelper: {jmsMap = " + jmsMap + "}, { queueCF = " +  queueCF + "}, {sendQueue = " + sendQueue + "}");
			connector.sendMessage(msg, jmsMap, queueCF, sendQueue);
		} catch (NamingException e) {
			log.error("JMSTableComparisonMessageHelper: Naming Exception " + e + ", Exception Message " + e.getMessage());
			throw OperationException.createError(e);
		} catch (JMSException e) {
			log.error("JMSTableComparisonMessageHelper: JMSException Exception " + e + ", Exception Message " + e.getMessage());
			throw OperationException.createError(e);
		}
	}
	
	protected QueueConnectionFactory initConnectionFactory()
			throws OperationException {
		log.info("JMSMessageHelper: initConnectionFactory");
		final Hashtable<String,String> hashtable = new Hashtable<String,String>();
	    hashtable.put("java.naming.factory.initial", INITIAL_CONTEXT_FACTORY);
	    String providerURL = propertyHelper.getProp("ebx.bpm.provider.url");  /** iiop://vm-edab-a780.nam.nsroot.net:8004 */
	    hashtable.put("java.naming.provider.url", providerURL);
	    hashtable.put("java.naming.referral", REFERRAL);
	    log.info("JMSMessageHelper: initialDirContext hashtable = " + hashtable);
	    try {
		    initialDirContext = new InitialDirContext(hashtable);
		    log.info("JMSMessageHelper: Looking up initial dir context with conn factory " + CONNECTION_FACTORY);
			return (QueueConnectionFactory) initialDirContext.lookup(CONNECTION_FACTORY);
	    } catch (NamingException ex) {
	    	log.error("JMSMessageHelper: Exception initializing connection factory", ex);
	    	throw OperationException.createError(ex);
	    }
	}
	
	protected Queue lookupQueue(QueueSession queueSession) throws OperationException {
		log.info("JMSMessageHelper: lookupQueue, queueName=" + queueName);
		try {
			return (Queue) initialDirContext.lookup(queueName);
		} catch (NamingException ex) {
			log.error("JMSMessageHelper: Exception looking up queue", ex);
			throw OperationException.createError(ex);
		}
	}
	
	
	protected String wrapWithCDATA(final String input){
		
		if(null != input && ! "".equalsIgnoreCase(input)){
			return "<![CDATA[" + input + "]]>";
		}
		return "";
	}
}
