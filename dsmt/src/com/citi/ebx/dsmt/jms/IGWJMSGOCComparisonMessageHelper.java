package com.citi.ebx.dsmt.jms;

import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.citi.ebx.dsmt.jaxb.util.GOCResponseVO;
import com.citi.ebx.dsmt.jaxb.util.GenerateGOCRequest;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.goc.util.IGWGOCWorkflowUtils;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.Utils;
import com.citi.ebx.workflow.vo.DSMTWorkflowRequestVO;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValidationReport;
import com.orchestranetworks.service.comparison.CompareFilter;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.ui.UIHttpManagerComponent;

public class IGWJMSGOCComparisonMessageHelper extends JMSComparisonMessageHelper {
	private static final String Y = "Y";

	private static final String EMPTY_STRING = DSMTConstants.BLANK_STRING;
	private String sid;
	private boolean isWarning;
	private boolean isHeadCountGOC;
	private boolean isAttestationRequired = false;
	private String routingRoleIDs;
	private String glTableURI;
	private String reTableURI;
	private String p2pTableURI;
	private String comparisonURI ;
	private String gocTableURI;
	private String maintenanceType;
	private String functionalID;
	private Date calenderDate ;
	private String maintenanceOrder;
	private String bu;
	private String buDesc;
	private String gocUsages;
	private String mnSeg;
	private String mnSegDesc;
	private String actionRequried;
    private String managementOnly;
    private String requestType;
    private boolean isWarningInCreate;
    private String sidCountry;
    private HashSet<String> regionList = new HashSet<String>();
    private HashSet<String> functionalIDList = new HashSet<String>();
	
	
	
	public String getManagementOnly() {
		return managementOnly;
	}

	public void setManagementOnly(String managementOnly) {
		this.managementOnly = managementOnly;
	}

	public String getMaintenanceType() {
		return maintenanceType;
	}

	public void setMaintenanceType(String maintenanceType) {
		this.maintenanceType = maintenanceType;
	}

	public String getFunctionalID() {
		return functionalID;
	}

	public void setFunctionalID(String functionalID) {
		this.functionalID = functionalID;
	}
	
	
	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}

	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	@Override
	public String createMessageContent() {
		log.debug("JMSGOCComparisonMessageHelper: createMessageContent");
		
		comparisonURI = wrapWithCDATA (createURIForComparison()); // "<![CDATA[" + createURIForComparison() + "]]>";
		
		log.info("JMSGOCComparisonMessageHelper: comparisonURI = " + comparisonURI);
		
		determineGOCFields();
		
		 gocTableURI = createTableURI(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC.getPathInSchema().format());
		determinePartnerTableURIs();
		/*
		String xmlMessage =
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
				"<request xmlns:ns0=\"http://com.citi.dsmt2.workflow\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns0:WorkflowGOCRequest\"> " +
				"<root>  " +
				"<userID>" + user + "</userID>  " +
				"<sid>" + sid + "</sid>	 " +
				"<gridRoutingID>" + dataSpace + "</gridRoutingID>  " +
				"<dataSpace>" + dataSpace + "</dataSpace>  " +
				"<dataSet>" + dataSet + "</dataSet>  " +
				"<requestID>" + requestID + "</requestID>  " +
				"<comparisonURI>" + comparisonURI + " </comparisonURI>  " +
				"<gocTableURI>" + gocTableURI + " </gocTableURI>  " +
				"<glTableURI>" + glTableURI + " </glTableURI>  " +
				"<reTableURI>" + reTableURI + " </reTableURI>  " +
				"<p2pTableURI>" + p2pTableURI + " </p2pTableURI>  " +
				"<userComment>" + userComment + "</userComment>	 " +
				"<isWarning>" + isWarning + "</isWarning>  " +
				"<isHeadCountGOC>" + isHeadCountGOC + "</isHeadCountGOC>  " +
				new GenerateGOCRequest().generateResponseXmlfromVO(routingRoleIDs) +
			//	"<routingRoleIDs>" + routingRoleIDs + "</routingRoleIDs>  " +
				"<workflowStatus>" + workflowStatus + "</workflowStatus>  " +
				"</root>  " +
				"</request>";*/
		calenderDate = GOCWorkflowUtils.getCalenderDate(repository, sid,maintenanceType);
		maintenanceOrder=  GOCWorkflowUtils.getMaintenanceOrder(repository, sid, bu);
		String xmlMessage = generateWorkflowRequest(routingRoleIDs);
		 
		
		
		log.info("JMSGOCComparisonMessageHelper: xmlMessage being sent = " + xmlMessage);
		return xmlMessage.toString();
	}
	
	private void determineGOCFields() {
		
		log.info("JMSGOCComparisonMessageHelper.determineGOCFields started at " + Calendar.getInstance().getTime() );
		
		final AdaptationHome dataSpaceRef = repository.lookupHome(HomeKey.forBranchName(dataSpace));
		log.info("JMSGOCComparisonMessageHelper.dataSpaceRef " + dataSpaceRef + " retrieved at " + Calendar.getInstance().getTime() );
		
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
		log.info("JMSGOCComparisonMessageHelper.dataSetRef " + dataSetRef + " retrieved at " + Calendar.getInstance().getTime() );
		// Do a validation so report is up to date
//		@SuppressWarnings("unused")
//		final ValidationReport report = dataSetRef.getValidationReport();
//		log.info("JMSGOCComparisonMessageHelper.ValidationReport " + report + " retrieved at " + Calendar.getInstance().getTime() );
		
		final AdaptationTable gocTable = dataSetRef.getTable(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC.getPathInSchema());
		
		// We want to compare against the initial version of the data in the data space.
		// Get the same table from the initial snapshot's data set
		final AdaptationTable initialTable = GOCWorkflowUtils.getTableInInitialSnapshot(gocTable);
		
		Adaptation firstRecord = null;
		
		// Compare the table with the same table in the initial snapshot's data set
		log.info("JMSGOCComparisonMessageHelper.compareAdaptationTables started at " + Calendar.getInstance().getTime() );
		final DifferenceBetweenTables diff = DifferenceHelper.compareAdaptationTables(initialTable, gocTable, false);
		log.info("JMSGOCComparisonMessageHelper.compareAdaptationTables " + diff + " retrieved at " + Calendar.getInstance().getTime() );
		// All extra occurrences on the right will be all records that exist in data space
		// that are not in initial snapshot, i.e. those added.
		@SuppressWarnings("unchecked")
		List<ExtraOccurrenceOnRight> adds = diff.getExtraOccurrencesOnRight();
		 Adaptation metadataDataSet =null;
		try {
			 metadataDataSet = EBXUtils.getMetadataDataSet(repository,
					DSMTConstants.GOC_WF_DATASPACE, DSMTConstants.GOC_WF_DATASET);
		} catch (OperationException ex) {
			log.error("Error looking up metadata data set", ex);
			
		}
	//	for (int i = 0; ! (isHeadCountGOC && isWarning) && i < adds.size(); i++) {
		for (int i = 0; i < adds.size(); i++) {
			final ExtraOccurrenceOnRight add = adds.get(0);
			final Adaptation record = add.getExtraOccurrence();
			if (firstRecord == null) {
				firstRecord = record;
			}
			functionalIDList.add(record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Functional_ID));
			if (! isHeadCountGOC) {
				isHeadCountGOC = Y.equalsIgnoreCase(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._HEADCOUNT_GOC));
			}
			
			if (! isWarning) {
				// Don't need to get up to date since we already validated everything above
				final ValidationReport recordReport = record.getValidationReport(false, true);
				isWarning = recordReport.hasItemsOfSeverity(Severity.WARNING);
			}
			
			if (! isAttestationRequired) {
				
				isAttestationRequired = belongsToDeactivationOrPurge(record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD));
			}
			
			getRegion(record, metadataDataSet);
			
		}
		
			if(adds.size() > 1){
				this.setRequestType("BULK");
				log.info("Request type is set as " +requestType);
			}
				else{
					this.setRequestType("SINGLE");
					log.info("Request type is set as " +requestType);
				}
		// if headcount goc is false or warning is false or attestationRequired flag is false then check in deltas
		if ( !isHeadCountGOC || ! isWarning || ! isAttestationRequired ) {
			// Do similar to what we did for adds, but for updates
			@SuppressWarnings("unchecked")
			List<DifferenceBetweenOccurrences> deltas = diff.getDeltaOccurrences();
		//	for (int i = 0; ! (isHeadCountGOC && isWarning) && i < deltas.size(); i++) {
			
			for (int i = 0; i < deltas.size(); i++) {
				final DifferenceBetweenOccurrences delta = deltas.get(0);
				final Adaptation record = delta.getOccurrenceOnRight();
				if (firstRecord == null) {
					firstRecord = record;
				}
				functionalIDList.add(record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Functional_ID));
				if (! isHeadCountGOC) {
					isHeadCountGOC = Y.equalsIgnoreCase(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._HEADCOUNT_GOC));
				}
				
				if (! isWarning) {
					// Don't need to get up to date since we already validated everything above
					final ValidationReport recordReport = record.getValidationReport(false, true);
					isWarning = recordReport.hasItemsOfSeverity(Severity.WARNING);
				}
				
				if (! isAttestationRequired) {
					
					isAttestationRequired = belongsToDeactivationOrPurge(record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD));
				}
				
				getRegion(record, metadataDataSet);
			}

			
			if(adds.size() > 1 || deltas.size() > 1 || (deltas.size() + adds.size() > 1) ){
				setRequestType("BULK");
				log.info("Request type is set as " +requestType);
			}
				else{
					setRequestType("SINGLE");
					log.info("Request type is set as " +requestType);
				}
			
		}
		
		
		
		log.info("JMSGOCComparisonMessageHelper.isHeadCountGOC/isWarning/isAttestationRequired checks completed at " + Calendar.getInstance().getTime() );
		
		// They should all have same SID & routing role IDs
		if (firstRecord != null) {
			final Adaptation sidRecord = Utils.getLinkedRecord(firstRecord,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
			if (sidRecord != null) {
				sid = sidRecord.getString(
						DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID);
				routingRoleIDs = firstRecord.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup);

				final Adaptation buRecord = Utils.getLinkedRecord(firstRecord,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU);
				
				bu = buRecord.getString(
						DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_BU_STD_C_DSMT_FRS_BU._C_DSMT_FRS_BU);
				buDesc = buRecord.getString(
						DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_BU_STD_C_DSMT_FRS_BU._DESCR80);
				

				final Adaptation gocUsageRecord = Utils.getLinkedRecord(firstRecord,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_USAGE);
				gocUsages = gocUsageRecord.getString(
						DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_USE._C_DSMT_GOC_USAGE);
				

				final Adaptation msSegRecord = Utils.getLinkedRecord(firstRecord,
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
				mnSeg = msSegRecord.getString(
						DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MAN_SEG_ID);
				mnSegDesc = msSegRecord.getString(
						DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._DESCR80);
				
				actionRequried = getActionRequriedCode(firstRecord.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD));
			}
		}
		if (sid == null) {
			sid = EMPTY_STRING;
		}
		if (routingRoleIDs == null) {
			routingRoleIDs = EMPTY_STRING;
		}
		
		log.info("JMSGOCComparisonMessageHelper.determineGOCFields finished at " + Calendar.getInstance().getTime() );
	}
	
	
	private  void getRegion(Adaptation record, Adaptation metaDataset){
		String sid =  record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
		String bu =  record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU);
		Adaptation sidRecord = IGWGOCWorkflowUtils.getSIDEhnechmentData(sid, bu ,metaDataset);
		
		if(null!=sidRecord){
			regionList.add(sidRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._Region));
		}
		
	}
	
	private static boolean belongsToDeactivationOrPurge(String actionRequired){
		
		if(GOCConstants.ACTION_DEACTIVATE_GOC_LEG_CCS.equalsIgnoreCase(actionRequired) || GOCConstants.ACTION_PURGE_GOC_LEG_CCS.equalsIgnoreCase(actionRequired)){
			return true;
		}else{
			return false;
		}
	}
	
	private void determinePartnerTableURIs() {
		final AdaptationHome metadataDataSpace = repository.lookupHome(
				HomeKey.forBranchName(GOCConstants.METADATA_DATA_SPACE));
		final Adaptation metadataDataSet = metadataDataSpace.findAdaptationOrNull(
				AdaptationName.forName(GOCConstants.METADATA_DATA_SET));
		final AdaptationTable sidEnhTable = metadataDataSet.getTable(
				GOCWFPaths._C_DSMT_SID_ENH.getPathInSchema());
		
		final Adaptation sidEnhRecord = sidEnhTable.lookupAdaptationByPrimaryKey(
				PrimaryKey.parseString(sid));
		if (sidEnhRecord == null) {
			glTableURI = EMPTY_STRING;
			reTableURI = EMPTY_STRING;
			p2pTableURI = EMPTY_STRING;
			sidCountry		= EMPTY_STRING;
		} else {
			final String glTablePath = sidEnhRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._GLTable);
			final String reTablePath = sidEnhRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._RptEngineTable);
			final String p2pTablePath = sidEnhRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._P2PTable);
			sidCountry = sidEnhRecord.getString(GOCWFPaths._C_DSMT_SID_ENH._Country);
			
			glTableURI = createTableURI(glTablePath);
			reTableURI = createTableURI(reTablePath);
			p2pTableURI = createTableURI(p2pTablePath);
		}
	}
	
	private String createTableURI(final String partnerTablePath) {
		if (partnerTablePath == null || EMPTY_STRING.equals(partnerTablePath)) {
			return EMPTY_STRING;
		}
		final HomeKey dataSpaceKey = HomeKey.forBranchName(dataSpace);
		final AdaptationName dataSetName = AdaptationName.forName(dataSet);
		
		
		
		final UIHttpManagerComponent uiHttpManagerComp =UIHttpManagerComponent.createWithBaseURI(BASE_URI);
		uiHttpManagerComp.select(dataSpaceKey, dataSetName, partnerTablePath);
		uiHttpManagerComp.setCompareFilter(CompareFilter.PERSISTED_VALUES_ONLY);
		
		return wrapWithCDATA(uiHttpManagerComp.getURIWithParameters());  // "<![CDATA[" + uiHttpManagerComp.getURIWithParameters() + "]]>";
	}
	
	
	
	
	protected String generateWorkflowRequest(String  routingRoleIDs )
	{	String requestXml= EMPTY_STRING;
	
	log.info("routing role ID = " + routingRoleIDs);
	 GOCResponseVO gocResponseVO =  new GenerateGOCRequest().generateGOCVO(routingRoleIDs);
	try{
		
		//TODO set business due date from Calendar table.
		XMLGregorianCalendar businessDueDate;
		
		if(calenderDate!=null){
			GregorianCalendar gregorianCalendar = new GregorianCalendar();
			gregorianCalendar.setTime(calenderDate);
			businessDueDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
								
							}
							else{
							
								businessDueDate= DatatypeFactory.newInstance().newXMLGregorianCalendar( new GregorianCalendar());
								businessDueDate.setDay(28);
							}
		
		// set the Value for parent Tag
		DSMTWorkflowRequestVO dsmtWorkflowRequest = new DSMTWorkflowRequestVO();
		dsmtWorkflowRequest.setWorkflowType(DSMTConstants.GOC_WORKFLOW_TYPE);
		dsmtWorkflowRequest.setRequestID(requestID);
		dsmtWorkflowRequest.setUserID(user);
		dsmtWorkflowRequest.setDataSpace(dataSpace);
		dsmtWorkflowRequest.setDataSet(dataSet);
		dsmtWorkflowRequest.setComparisonURI(comparisonURI);
		dsmtWorkflowRequest.setUserComment(wrapWithCDATA(userComment));
		
		if(gocTableURI==null)
			{
				dsmtWorkflowRequest.setTableURI(gocTableURI);
			}
			else
			{
				dsmtWorkflowRequest.setTableURI(gocTableURI);
			}
		
		if(tableXPath==null)
			{
				dsmtWorkflowRequest.setEbxRecordPath(EMPTY_STRING);
			}
			else
			{
				dsmtWorkflowRequest.setEbxRecordPath(tableXPath);
			}
		
		if(workflowStatus==null)
			{
			dsmtWorkflowRequest.setWorkFlowStatus(EMPTY_STRING);
			}
			else
			{
				dsmtWorkflowRequest.setWorkFlowStatus(workflowStatus);	
			}
		dsmtWorkflowRequest.setEbxTableName(EMPTY_STRING);
		dsmtWorkflowRequest.setEbxTableID(EMPTY_STRING);
		dsmtWorkflowRequest.setEbxRecordPath(EMPTY_STRING);

		// set the value for Goc Tags
		
		DSMTWorkflowRequestVO.GocRequest gocRequest = new DSMTWorkflowRequestVO.GocRequest();
		gocRequest.setSid(sid);
		managementOnly = IGWGOCWorkflowUtils.isManagmentOnlySID(repository, sid);
		gocRequest.setManagementOnly(managementOnly);
		gocRequest.setBU(bu);
		gocRequest.setBuDesc(wrapWithCDATA(buDesc));
		gocRequest.setGocUsage(gocUsages);
		gocRequest.setMnSeg(mnSeg);
		gocRequest.setMnSegDesc( wrapWithCDATA(mnSegDesc));
		gocRequest.setGridRoutingID(dataSpace);
		gocRequest.setMaintenanceType("regular".equalsIgnoreCase(maintenanceType)?"Regular":"Functional");
		gocRequest.setRequestType(requestType);
		if(null == regionList ){
			gocRequest.setSidCountry(EMPTY_STRING);
		}
		else{
			gocRequest.setSidCountry(regionList.toString());
		}
		
		if(functionalIDList.size()>0){
			functionalID= functionalIDList.toString();
			
		}else{
			functionalID = "Not Available";
			}
		gocRequest.setFunctionalID(wrapWithCDATA(functionalID));
		
		gocRequest.setIsWarning(isWarning);
		gocRequest.setIsHeadCountGOC(isHeadCountGOC);
		gocRequest.setActionRequried(actionRequried);;
		gocRequest.setCountryController(gocResponseVO.getCountryController());
		if(isWarningInCreate){
			if(isWarning && !(mnSeg.startsWith("98"))){
				gocRequest.setContentManagement("DSMT_CONTENT_MANAGMENT");
			}else{
				gocRequest.setContentManagement("");
			}
				
		}
		else{
			gocRequest.setContentManagement("");
		}
		
		if(isHeadCountGOC){
			gocRequest.setCountryBusinessHR(gocResponseVO.getCountryBusinessHR());
			gocRequest.setHRRole1(gocResponseVO.getHrRole1());
			gocRequest.setHRRole2(gocResponseVO.getHrRole2());
			gocRequest.setHRRole3(gocResponseVO.getHrRole3());
			gocRequest.setHRRole4(gocResponseVO.getHrRole4());
		}else{
			gocRequest.setCountryBusinessHR(EMPTY_STRING);
			gocRequest.setHRRole1(EMPTY_STRING);
			gocRequest.setHRRole2(EMPTY_STRING);
			gocRequest.setHRRole3(EMPTY_STRING);
			gocRequest.setHRRole4(EMPTY_STRING);
		}
		/*
		if(isWarning){
			gocRequest.setRegionalDQTeam(gocResponseVO.getRegionalDQTeam());
		}else{
			gocRequest.setRegionalDQTeam(EMPTY_STRING);
		}
		*/
		gocRequest.setRegionalDQTeam(EMPTY_STRING);
		
		gocRequest.setFinanceRole1(gocResponseVO.getFinanceRole1());
		gocRequest.setFinanceRole2(gocResponseVO.getFinanceRole2());
		gocRequest.setFinanceRole3(gocResponseVO.getFinanceRole3());
		gocRequest.setFinanceRole4(gocResponseVO.getFinanceRole4());
		gocRequest.setFinanceRole5(gocResponseVO.getFinanceRole5());
	
		
		gocRequest.setGlMaintenance(gocResponseVO.getGlMaintenance());
		gocRequest.setREMaintenance(gocResponseVO.getReMaintenance());
		gocRequest.setP2PMaintenance(gocResponseVO.getP2PMaintenance());
		gocRequest.setRegionalFinance(gocResponseVO.getRegionalFinance());
		gocRequest.setPlanningUnitApprover(gocResponseVO.getPlanningUnitApprover());
 		gocRequest.setDsmt2Maintenance(DSMTConstants.DSMT_WORKFLOW_MAINTENANCE_ROLE);
		
		// gocRequest.setMaintenanceOrder(gocResponseVO.getMaintenanceOrder());
		//Calendar Due date 
		gocRequest.setBusinessDueDate(businessDueDate);
		gocRequest.setAttestationAttachedForDeactivation(isAttestationRequired);
		gocRequest.setGlTableURI(glTableURI);
		gocRequest.setReportingEngineTableURI(reTableURI);
		gocRequest.setP2PTableURI(p2pTableURI);
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repository,
				DSMTConstants.GOC_WF_DATASPACE, DSMTConstants.GOC_WF_DATASET);
		if("Resubmitted".equalsIgnoreCase(workflowStatus)){
			String predicate  = GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL._WFREQ_ID
					.format() + " = '" + requestID + "'";
			log.info("predicate for request id  ====  "+predicate);
			AdaptationTable IGW_DTL = metadataDataSet.getTable(GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL.getPathInSchema());
			RequestResult dtlRec = IGW_DTL.createRequestResult(predicate);
			if(!dtlRec.isEmpty()){
				log.info("fetched detail record");
				Adaptation detailRec = dtlRec.nextAdaptation();
				String parntID = detailRec.getString((GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL._REQUEST_ID));
				log.info("parent request id ==== "+parntID);
				
				gocRequest.setParentID(parntID+"GPID18");
			}else{
				log.info("no record with this workflow id === "+requestID);
				gocRequest.setParentID(requestID);
			}
			
			
		}else{
			log.info("submitted request with parent ID  == "+parentID);
			gocRequest.setParentID(parentID+"GPID18");
		}
		
		// P2P_DSMT2_GL_RE Update by vikash
		
		gocRequest.setMaintenanceOrder(maintenanceOrder);
	
		dsmtWorkflowRequest.setGocRequest(gocRequest);
		JAXBContext jaxbContext = JAXBContext.newInstance(DSMTWorkflowRequestVO.class);
		
		Marshaller jaxbmMarshaller = jaxbContext.createMarshaller();
		jaxbmMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		StringWriter sw = new StringWriter();
		jaxbmMarshaller.marshal(dsmtWorkflowRequest, sw);
		StringBuffer buffer= sw.getBuffer();
		requestXml=buffer.toString();
		
		requestXml=requestXml.replace("&amp;", "&");
		requestXml=requestXml.replace("&lt;", "<");
		requestXml=requestXml.replace("&gt;", ">");
		
	//	log.info("XML message to be sent to CWM >> " + requestXml);
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
	
	
	return requestXml ;
}

	public String getActionRequriedCode(String  actionRequried){
		String code= "";
		char value= actionRequried.charAt(0);
		switch (value) {
		  case 'A':
			  code ="Create GOC and Any Legacy CC [A]";
			 
				  isWarningInCreate=true;
			    break;
		  case 'B': 
			  code="Deactivate GOC and Any Legacy CCs [B]";
		        break;
		  case 'D': 
			  code="Purge GOC and Any Legacy CCs [D]";
		        break;
				  
		  case 'E': 
			  code="Reactivate GOC and Any Legacy CCs [E]";
		        break;
		  case 'F': 
			  code="Modify GOC Description (only) [F]";
		        break;
		  case 'G':
			  code="Modify GOC Usage (only) [G]";
		        break;
		  case 'H': 
			  code="Modify FRS BU (only) [H]";
		        break;
		  case 'J': 
			  code="Modify FRS OU (only) [J]";
		        break;
		  case 'K': 
			  code="Modify Managed Segment (only) [K]";
		        break;
		  case 'L': 
			  code="Modify Managed Geography (only) [L]";
		        break;
		  case 'M': 
			  code="Modify Function (only) [M]";
		        break;
		  case 'N': 
			  code="Modify GL-Specific Attribute [N]";
		        break;
		  case 'O': 
			  code="Modify P2P-Specific Attribute [O]";
		        break;
		  case 'P': 
			  code="Modify RE-Specific Attribute [P]" ;
		        break;
		  case 'U': 
			  code="Modify Multiple GOC Attributes [U]";
		        break;
		 
		}
		
		return code;
		       
	}
	
	
	
}
