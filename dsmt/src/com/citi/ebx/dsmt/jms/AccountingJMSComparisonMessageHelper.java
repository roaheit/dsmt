package com.citi.ebx.dsmt.jms;

import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.citi.ebx.dsmt.jms.JMSComparisonMessageHelper.ComparisonLevel;
import com.citi.ebx.dsmt.jms.connector.CitiJMSConnector;
import com.citi.ebx.dsmt.jms.connector.impl.CitiJMSConnectorImpl;
import com.citi.ebx.workflow.vo.DSMTWorkflowRequestVO;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.comparison.CompareFilter;
import com.orchestranetworks.ui.UIHttpManagerComponent;

public class AccountingJMSComparisonMessageHelper extends JMSMessageHelper  {
	public enum ComparisonLevel {
		TABLE, DATASET
	}
	
	private static final String REPLY_TO_QUEUE = "reply.to.queue";
	private static final String ACCOUNTING_WORKFLOW_REQUEST = "AccountingMatWFRequest";
	protected static final String BASE_URI = "/ebx/";
	
	private ComparisonLevel comparisonLevel = ComparisonLevel.TABLE;
	
	protected String workflowType;
	
	protected String bpmProcessID;
	protected String requestID;
	protected String dataSpace;
	protected String dataSet;
	protected String tableXPath;
	protected String user;
	protected String userComment;
	protected String tableID;
	protected String tableName;
	protected String workflowStatus;
	protected String terminatedLEM;
	protected String lvid;
	protected String approvalGroup;
	
	public String getWorkflowType() {
		return workflowType;
	}

	public void setWorkflowType(String workflowType) {
		this.workflowType = workflowType;
	}

	public ComparisonLevel getComparisonLevel() {
		return comparisonLevel;
	}

	public void setComparisonLevel(ComparisonLevel comparisonLevel) {
		this.comparisonLevel = comparisonLevel;
	}

	public String getBpmProcessID() {
		return bpmProcessID;
	}

	public void setBpmProcessID(String bpmProcessID) {
		this.bpmProcessID = bpmProcessID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getRequestID() {
		
		return requestID;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public void setTableXPath(String tableXPath) {
		this.tableXPath = tableXPath;
	}

	public String getTableXPath() {
		return tableXPath;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getTableID() {
		return tableID;
	}

	public void setTableID(String tableID) {
		this.tableID = tableID;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getUserComment() {
		return userComment;
	}
	
	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}
	
	/**
	 * @return the parentID
	 */
	

	/*public String createMessageContentOld() {
		log.info("JMSComparisonMessageHelper: createMessageContent");
		
		final String comparisonURI =  "<![CDATA[" + createURIForComparison() + "]]>";
		final String editURI =  "<![CDATA[" + createTableURIForEdit() + "]]>";
	
		log.info("JMSComparisonMessageHelper: comparisonURI = " + comparisonURI);
		log.info("JMSComparisonMessageHelper: editURI = " + editURI);
	
		StringBuilder xmlMessage = new StringBuilder(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
				"<request xmlns:ns0=\"http://com.citi.dsmt2.workflow\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns0:WorkflowRequest\"> " +
				"<requestID>" + requestID + "</requestID>  " +
				"<dataSpace>" + dataSpace + "</dataSpace>  " +
				"<dataSet>" + dataSet + "</dataSet>  " +
				"<comparisonURI>" + comparisonURI + " </comparisonURI>  ");
		if (ComparisonLevel.TABLE.equals(comparisonLevel)) {
			xmlMessage.append(
					"<tableURI>" + editURI + " </tableURI>  ");
		} else {
			xmlMessage.append(
					"<dataSetURI>" + editURI + "</dataSetURI>  ");
		}
		xmlMessage.append(
				"<userID>" + user + "</userID>	" +
				"<userComment>" + userComment + "</userComment>	");
		if (ComparisonLevel.TABLE.equals(comparisonLevel)) {
			xmlMessage.append(
				"<ebxRecordPath>" + tableXPath + "</ebxRecordPath>		" +
				"<ebxTableID>" + tableID + "</ebxTableID>		" +
				"<ebxTableName>" + tableName + "</ebxTableName>		");
		}
		xmlMessage.append(
				"<workflowStatus>" + workflowStatus + "</workflowStatus>		" +
				"</request>");
		
		log.info("JMSComparisonMessageHelper: xmlMessage being sent = " + xmlMessage);
		return xmlMessage.toString();
	}
	*/
	@Override
	public String createMessageContent()
	{	
		
		String requestXml= "";
	
	try{
		
		final String comparisonURI =  "<![CDATA[" + createURIForComparison() + "]]>";
		final String tableURI =  "<![CDATA[" + createTableURIForEdit() + "]]>";
		
		DSMTWorkflowRequestVO dsmtWorkflowRequest = new DSMTWorkflowRequestVO();
		
		dsmtWorkflowRequest.setWorkflowType(workflowType);
		
		dsmtWorkflowRequest.setRequestID(requestID);
		dsmtWorkflowRequest.setUserID(user);
		dsmtWorkflowRequest.setDataSpace(dataSpace);
		dsmtWorkflowRequest.setDataSet(dataSet);
		dsmtWorkflowRequest.setComparisonURI(comparisonURI);
		dsmtWorkflowRequest.setUserComment(wrapWithCDATA(userComment));
		
		dsmtWorkflowRequest.setTableURI(tableURI);
		
	
		dsmtWorkflowRequest.setWorkFlowStatus(workflowStatus);	
		dsmtWorkflowRequest.setEbxTableName(tableName);
		dsmtWorkflowRequest.setEbxTableID(tableID);
		dsmtWorkflowRequest.setEbxRecordPath(tableXPath);
		dsmtWorkflowRequest.setTerminatedLEM(terminatedLEM);
		dsmtWorkflowRequest.setLvid(lvid);
		dsmtWorkflowRequest.setApprovalGroup(approvalGroup);

		
		JAXBContext jaxbContext = JAXBContext.newInstance(DSMTWorkflowRequestVO.class);
		
		Marshaller jaxbmMarshaller = jaxbContext.createMarshaller();
		jaxbmMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
		StringWriter sw = new StringWriter();
		jaxbmMarshaller.marshal(dsmtWorkflowRequest, sw);
		StringBuffer buffer= sw.getBuffer();
		requestXml=buffer.toString();
		
		requestXml=requestXml.replace("&amp;", "&");
		requestXml=requestXml.replace("&lt;", "<");
		requestXml=requestXml.replace("&gt;", ">");
		
		log.info("XML message to be sent to CWM -  " + requestXml);
		}
		catch (Exception e) {
			log.error("Exception during Request XML creation " + e + ". Check System Error log for details.");
			e.printStackTrace();
		}
	
	
	return requestXml ;
}
	
	
	@Override
	protected Map<String, String> createJMSMap() {
		HashMap<String, String> jmsMap = new HashMap<String, String>();
		
		jmsMap.put("JMSReplyTo", propertyHelper.getProp(REPLY_TO_QUEUE)); 
		jmsMap.put("JMSCorrelationID", dataSpace);
		jmsMap.put("JMSType", ACCOUNTING_WORKFLOW_REQUEST);
		
		
		
		return jmsMap;
	}
	
	
	protected String createURIForComparison() {
		log.debug("JMSComparisonMessageHelper: createURIForComparison");
		final HomeKey dataSpaceKey = HomeKey.forBranchName(dataSpace);
		final AdaptationHome dataSpaceRef = repository.lookupHome(dataSpaceKey);
		final AdaptationName dataSetName = AdaptationName.forName(dataSet);
		log.debug("JMSComparisonMessageHelper: dataSpace=" + dataSpaceKey + ", dataSet=" + dataSetName);

		final UIHttpManagerComponent uiHttpManagerComp =
				UIHttpManagerComponent.createWithBaseURI(BASE_URI);
		final HomeKey parentKey = dataSpaceRef.getParent().getKey();
		
		if (ComparisonLevel.TABLE.equals(comparisonLevel)) {
			uiHttpManagerComp.select(parentKey , dataSetName, tableXPath);
			uiHttpManagerComp.compareSelectionWithEntity( dataSpaceKey, dataSetName, tableXPath);
		} else {
			uiHttpManagerComp.selectInstance(parentKey , dataSetName);
			uiHttpManagerComp.compareSelectionWithEntity( dataSpaceKey, dataSetName, null);
		}
		
		uiHttpManagerComp.setCompareFilter(CompareFilter.PERSISTED_VALUES_ONLY);
		
		return uiHttpManagerComp.getURIWithParameters();
	}
	
	protected String createTableURIForEdit(){
		log.debug("JMSComparisonMessageHelper: createURIForEdit");
		final HomeKey dataSpaceKey = HomeKey.forBranchName(dataSpace);
		final AdaptationName dataSetName = AdaptationName.forName(dataSet);
		log.debug("JMSComparisonMessageHelper: dataSpace=" + dataSpaceKey + ", dataSet=" + dataSetName);

		final UIHttpManagerComponent uiHttpManagerComp =
				UIHttpManagerComponent.createWithBaseURI(BASE_URI);
		if (ComparisonLevel.TABLE.equals(comparisonLevel)) {
			uiHttpManagerComp.select(dataSpaceKey, dataSetName, tableXPath);
		} else {
			uiHttpManagerComp.selectInstance(dataSpaceKey, dataSetName);
		}
		return uiHttpManagerComp.getURIWithParameters();
	}

	public String getTerminatedLEM() {
		return terminatedLEM;
	}

	public void setTerminatedLEM(String terminatedLEM) {
		this.terminatedLEM = terminatedLEM;
	}

	public String getLvid() {
		return lvid;
	}

	public void setLvid(String lvid) {
		this.lvid = lvid;
	}

	public String getApprovalGroup() {
		return approvalGroup;
	}

	public void setApprovalGroup(String approvalGroup) {
		this.approvalGroup = approvalGroup;
	}
	
	
}
