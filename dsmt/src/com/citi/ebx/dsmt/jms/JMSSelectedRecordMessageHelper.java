package com.citi.ebx.dsmt.jms;

import java.util.HashMap;
import java.util.Map;

import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.ui.UIHttpManagerComponent;

public class JMSSelectedRecordMessageHelper extends JMSMessageHelper {
	private static final String BASE_URI = "/ebx/";
	
	private String bpmProcessID;
	private String dataSpace;
	private String dataSet;
	private String recordPath;
	private String user;
	
	public String getBpmProcessID() {
		return bpmProcessID;
	}

	public void setBpmProcessID(String bpmProcessID) {
		this.bpmProcessID = bpmProcessID;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getRecordPath() {
		return recordPath;
	}

	public void setRecordPath(String recordPath) {
		this.recordPath = recordPath;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

/*	 Commenting the UCA based invocation code.
 	@Override
	protected String createMessageContent() {
		log.info("JMSSelectedRecordMessageHelper: createMessageContent");
		final String uri =  "<![CDATA[" + createURIForRecord() + "]]>";  // createURIForRecord();
		log.info("JMSSelectedRecordMessageHelper: uri = " + uri);
		
		String bpmAppName = propertyHelper.getProp("message.bpm.appname");	//EBXPOC
		String snapShotName = propertyHelper.getProp("message.bpm.snapshotname"); // "JMSTASNAPSHOT"
		String ucaName = propertyHelper.getProp("message.bpm.ucaname"); // "reqUCA"
		String eventName = propertyHelper.getProp("message.bpm.eventname"); // EBX5Message
	
		return "<eventmsg><event processApp=\"" + bpmAppName 
				+ "\" snapshot=\"" + snapShotName
				+ "\" ucaname=\""+ ucaName
				+ "\">" + eventName + "</event><parameters><parameter><key>ucaInput</key><value><userID>"
				+ user
				+ "</userID><url>"
				+ uri
				+ "</url><requestID>dsmt2Request</requestID><bpmProcessID>"
				+ bpmProcessID
				+ "</bpmProcessID><dataSpace>"
				+ dataSpace
				+ "</dataSpace><ebxRecordPath>"
				+ recordPath
				+ "</ebxRecordPath></value></parameter></parameters></eventmsg>";
	}
*/	
	@Override
	public String createMessageContent() {
		log.info("JMSSelectedRecordMessageHelper: createMessageContent");
		final String uri =  "<![CDATA[" + createURIForRecord() + "]]>";  // createURIForRecord();
		log.info("JMSSelectedRecordMessageHelper: uri = " + uri);
		final String requestID = "123";

		String xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
				"<request xmlns:ns0=\"http://com.citi.dsmt2.workflow\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns0:WorkflowRequest\"> " +
				"<requestID>" + requestID + "</requestID>  " +
				"<dataSpace>" + dataSpace + "</dataSpace>  " +
				"<dataSet>" + dataSet + "</dataSet>  " +
				"<url>" + uri +" </url>  " +
				"<userID>" + user + "</userID>	" +
				"<ebxRecordPath>" + recordPath + "</ebxRecordPath>		" +
				"</request>";
		
		log.debug("JMSSelectedRecordMessageHelper: xmlMessage being sent = " + xmlMessage);
		return xmlMessage;
	}
	
	private String createURIForRecord() {
		log.debug("JMSSelectedRecordMessageHelper: createURIForRecord");
		final HomeKey dataSpaceKey = HomeKey.forBranchName(dataSpace);
		final AdaptationName dataSetName = AdaptationName.forName(dataSet);
		log.debug("JMSSelectedRecordMessageHelper: dataSpace=" + dataSpaceKey + ", dataSet=" + dataSetName);

		final UIHttpManagerComponent uiHttpManagerComp =
				UIHttpManagerComponent.createWithBaseURI(BASE_URI);
		uiHttpManagerComp.select(dataSpaceKey, dataSetName, recordPath);
		return uiHttpManagerComp.getURIWithParameters();
	}

	@Override
	protected Map<String, String> createJMSMap() {
		HashMap<String, String> jmsMap = new HashMap<String, String>();
		
		// TODO: This would have to be implemented. See TableComparison helper.
		
		return jmsMap;
	}
}
