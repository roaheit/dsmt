package com.citi.ebx.dsmt.jms;

import java.util.HashMap;
import java.util.Map;

public class JMSDataSetImportCompletedMessageHelper extends JMSMessageHelper {
	private static final String INVOKE_DATASET_IMPORT_WORKFLOW_REQUEST = "InvokeDataSetImportWorkflowRequest";
//	private static final String REPLY_TO_QUEUE = "reply.to.queue";
	
	private String importConfigID;
	private String correlationID;
	private String errorMessage;
	private String errorDetails;
	
	private String replyToQueue; 
	
	
	public String getImportConfigID() {
		return importConfigID;
	}

	public void setImportConfigID(String importConfigID) {
		this.importConfigID = importConfigID;
	}

	public String getCorrelationID() {
		return correlationID;
	}

	public void setCorrelationID(String correlationID) {
		this.correlationID = correlationID;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	@Override
	public String createMessageContent() {
		//log.info("PutBPMDataSetImportCompletedScriptTask: createMessageContent");
	
		String statusAndErrors;
		if (errorMessage == null || "".equals(errorMessage)) {
			statusAndErrors = "<status>true</status>";
		} else {
			statusAndErrors = "					<status>false</status>" +
					"					<errorMessage>" + errorMessage + "</errorMessage>" +
					(errorDetails == null || "".equals(errorDetails) ?"					<errorDetails></errorDetails>" :	"					<errorDetails>" + errorDetails + "</errorDetails>");
		}
		/*String xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
				"<response xmlns:ns0=\"http://com.citi.dsmt2.workflow\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns0:WorkflowRequest\"> " +
				"<importConfigID>" + importConfigID + "</importConfigID>  " +
				"<correlationID>" + correlationID + "</correlationID>  " +
				statusAndErrors +
				"</response>";*/
		
		String xmlMessage = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:frs=\"http://com.citi.dsmt2.workflow/FRSDA/\">" +
							 "			<soapenv:Header/>" +
							 "			<soapenv:Body>" +
							 " 				<frs:response>" +
							 "					<importConfigID>" + importConfigID + "</importConfigID>  " +
							 "					<correlationID>" + correlationID + "</correlationID>  " +
							 statusAndErrors +
							 "				</frs:response>" +
							 "			</soapenv:Body>" +
							 "</soapenv:Envelope>" ;
		log.info("xmlMessage being sent = " + xmlMessage);
		return xmlMessage;
	}
	
	public static void main(String[] args) {
		
		new JMSDataSetImportCompletedMessageHelper().printMessage();
	}
	
	private void printMessage(){
		
		errorMessage = "errorMessage_!";
		errorDetails = "errorDetails";
		
		log.info(createMessageContent());
	}
	
	@Override
	protected Map<String, String> createJMSMap() {
		HashMap<String, String> jmsMap = new HashMap<String, String>();
		
		jmsMap.put("JMSReplyTo", queueName); 
		jmsMap.put("JMSCorrelationID", correlationID);
		jmsMap.put("JMSType", INVOKE_DATASET_IMPORT_WORKFLOW_REQUEST);
		
		return jmsMap;
	}

	public void setReplyToQueue(String replyToQueue) {
		this.replyToQueue = replyToQueue;
	}

	public String getReplyToQueue() {
		return replyToQueue;
	}
}
