package com.citi.ebx.dsmt.jms.connector;

import java.util.Map;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.naming.NamingException;

/**
 * 
 * @author nc72931
 *  
 */
public interface CitiJMSConnector {

	/**
	 * Sends message with JMS attributes to a specified queue
	 * JMSHeader Property is to be passed as the attribute "key name" in propertiesMap. 
	 * Supported JMSHeaders are {JMSCorrelationID, JMSDestination, JMSMessageID, JMSReplyTo,  JMSType,  JMSPriority}
	 * @param messageContent
	 * @param JMSHeader
	 * @param queueConnectionFactory
	 * @param sendQueue
	 * @throws NamingException
	 * @throws JMSException
	 */
	public void sendMessage(final String messageContent, final Map<String, String> propertiesMap, final String queueCF, final String sendQueue) throws NamingException, JMSException;
		
	/**
	 *  Picks the next available message from a specified queue 
	 *	@param queueConnectionFactory
	 * @param receiveQueue 
	 * @throws NamingException
	 * @throws JMSException
	 * */
	public String receiveMessage(final String queueCF, final String receiveQueue) throws NamingException, JMSException;
	
	/**
	 * Filters a queue/ picks a message based on input selector String 
	 * @param receiveQueue
	 * @param selectorString
	 * @throws NamingException
	 * @throws JMSException
	 */
	public String pickMessage(final String queueCF, final String receiveQueue, final String selectorString) throws NamingException, JMSException;
	
	
	public static final Logger logger = Logger.getLogger("com.citi.jms.connector.CitiJMSConnector");
	
}
