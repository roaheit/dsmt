package com.citi.ebx.dsmt.jms.connector.impl;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.citi.ebx.dsmt.jms.connector.CitiJMSConnector;
import com.citi.ebx.dsmt.util.DSMTConstants;

public class CitiJMSConnectorImpl implements CitiJMSConnector{

	
	private static final int RECEIVE_TIME_OUT = 120000; // 120 seconds
	
	
	/**
	 *  implements the receiveMessage method of the CitiJMSConnector Interface 
	 *	@param queueConnectionFactory
	 * @param receiveQueue 
	 * @throws NamingException
	 * @throws JMSException
	 * */
	public String receiveMessage(final String queueCF, final String receiveQueue) throws NamingException, JMSException{

		logger.log(Level.ALL, "Getting a QueueConnectionFactory");
		final Context messaging = new InitialContext();
		final QueueConnectionFactory queueConnectionFactory = (QueueConnectionFactory)messaging.lookup(queueCF);
		
		logger.log(Level.ALL, "Getting a QueueConnection");
		
		
		
		final QueueConnection queueConnection = getQueueConnection(queueConnectionFactory);
		
		logger.log(Level.ALL, "Starting Queue Connection");
		queueConnection.start();
		
		logger.log(Level.ALL, "Getting Queue Session");
		final QueueSession session = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
		
		logger.log(Level.ALL, "Starting Queue Receiver");
		final Queue queue = (javax.jms.Queue)messaging.lookup(receiveQueue);
		
		final QueueBrowser queueBrowser = session.createBrowser(queue);
		
		String messageText = null;
		Message message = null;
		
		/** Check if there are any messages in the Queue */
		if(queueBrowser.getEnumeration().hasMoreElements()){
		
			final QueueReceiver queueReceiver = session.createReceiver(queue);
			message = queueReceiver.receive(RECEIVE_TIME_OUT);
			messageText = String.valueOf(message);
			logger.log(Level.INFO, "received Message = " + message);
			queueReceiver.close();
			logger.log(Level.ALL, "queueReceiver closed.. ");
		}
		else{
			messageText = "No message in Queue !!" ;
			logger.log(Level.INFO, "No Message in queue " );
		}
		
		queueBrowser.close();
		logger.log(Level.ALL, "queueBrowser closed.. ");
		session.close();
		logger.log(Level.ALL, "queueSession closed.. ");
		queueConnection.close();
		logger.log(Level.ALL, "queueConnection closed.. ");
		
		return  messageText;
	}

	/**
	 * implements the pickMessage method of the CitiJMSConnector Interface 
	 * @param receiveQueue
	 * @param selectorString
	 * @throws NamingException
	 * @throws JMSException
	 */
	public String pickMessage( final String queueCF, final String receiveQueue, final String selectorString) throws NamingException, JMSException{

		logger.log(Level.ALL, "Getting a QueueConnectionFactory");
		final Context messaging = new InitialContext();
		final QueueConnectionFactory queueConnectionFactory = (QueueConnectionFactory)messaging.lookup(queueCF);
		
		logger.log(Level.ALL, "Getting a QueueConnection");
		final QueueConnection queueConnection = getQueueConnection(queueConnectionFactory);
		
		logger.log(Level.ALL, "Starting Queue Connection");
		queueConnection.start();
		
		logger.log(Level.ALL, "Getting Queue Session");
		final QueueSession session = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
	
		logger.log(Level.ALL, "Starting Queue Receiver");
		final Queue queue = (javax.jms.Queue)messaging.lookup(receiveQueue);
		
		final QueueBrowser queueBrowser = session.createBrowser(queue);
		
		String messageText = null;
		Message message = null;
		
		/** Check if there are any messages in the Queue */
		if(queueBrowser.getEnumeration().hasMoreElements()){
		
			logger.log(Level.INFO, "Selector String = " + selectorString);
			final QueueReceiver queueReceiver = session.createReceiver(queue, selectorString);
		
			message = queueReceiver.receive();
			logger.log(Level.INFO, "picked Message = " + message);
			messageText = String.valueOf(message);
			queueReceiver.close();
			logger.log(Level.INFO, "queueReceiver closed.. ");
		}else{
			messageText = "No message in Queue !!" ;
			logger.log(Level.INFO, "No Message in queue " );
		}
		session.close();
		logger.log(Level.ALL, "queueSession closed.. ");
		queueConnection.close();
		logger.log(Level.ALL, "queueConnection closed.. ");
		
		return messageText  ;
	}


	/**
	 *  implements the sendMessage method of the CitiJMSConnector Interface 
	 *	@param receiveQueue
	 * @param  hashMap containing jmsAttributes
	 * @param  queueConnectionFactory
	 * @param  queue
	 * @throws NamingException
	 * @throws JMSException
	 */
	public void sendMessage(final String messageContent, final Map<String, String> propertiesMap, final String queueCF, final String sendQueue)
			throws NamingException, JMSException {
		
		logger.log(Level.ALL, "Getting a QueueConnectionFactory");
		final Context messaging = new InitialContext();
		final QueueConnectionFactory queueConnectionFactory = (QueueConnectionFactory)messaging.lookup(queueCF);
		
		
		final QueueConnection queueConnection = getQueueConnection(queueConnectionFactory);
		
		logger.log(Level.ALL, "Starting Queue Connection");
		queueConnection.start();
		
		logger.log(Level.ALL, "Getting Queue Session");
		final QueueSession session = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
	
		logger.log(Level.ALL, "Starting Queue Sender");
		final Queue queue = (javax.jms.Queue)messaging.lookup(sendQueue);
		
		logger.log(Level.INFO, "Queue = " + queue);
		
		final QueueSender queueSender = session.createSender(queue);
		
		logger.log(Level.INFO, "queueSender = " + queueSender);
		TextMessage textmessage = session.createTextMessage();
		textmessage.setText(messageContent);
		
		textmessage =  new CitiJMSConnectorUtil().setJMSHeaders(textmessage, propertiesMap,  session);
		
		queueSender.setDeliveryMode(textmessage.getJMSDeliveryMode());
		logger.log(Level.INFO, "now sending Message = " + textmessage);
		queueSender.send(textmessage);
		logger.log(Level.FINE,"message sent successfully -" + textmessage);
		
		queueSender.close();
		logger.log(Level.ALL, "queueSender closed.. ");
		session.close();
		logger.log(Level.ALL, "queueSession closed.. ");
		queueConnection.close();
		logger.log(Level.ALL, "queueConnection closed.. ");
		
	}


	private QueueConnection getQueueConnection(final QueueConnectionFactory queueConnectionFactory ) throws JMSException {
		
		QueueConnection queueConnection = null;
		try {
			queueConnection = queueConnectionFactory.createQueueConnection();
		} catch (Exception e) {
			final String env = DSMTConstants.propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT); // "local" ; 
			
			if("prod".equalsIgnoreCase(env) || "uat".equalsIgnoreCase(env) || "sit".equalsIgnoreCase(env)){
				final String userId = DSMTConstants.bundle.getString(env + DSMTConstants.DOT + "jms.queuecf.user");
				final String password = DSMTConstants.bundle.getString(env + DSMTConstants.DOT + "jms.queuecf.pwd");
				
				System.err.println("username = " + userId + "-"+ password );
				queueConnection = queueConnectionFactory.createQueueConnection(userId, password);
				
			}
		}
		
		return queueConnection;
	}


	protected static Set<String> jmsHeaderSet =   populateJMSHeaderSet(); 
	
 	private static Set<String> populateJMSHeaderSet() {
	
		jmsHeaderSet = new TreeSet<String>();
		
		jmsHeaderSet.add("JMSDestination");
		jmsHeaderSet.add("JMSMessageID");
		jmsHeaderSet.add("JMSCorrelationID");
		jmsHeaderSet.add("JMSReplyTo");
		jmsHeaderSet.add("JMSType");
		jmsHeaderSet.add("JMSExpiration");
		jmsHeaderSet.add("JMSPriority");
		
		return jmsHeaderSet;
		
	}
}
