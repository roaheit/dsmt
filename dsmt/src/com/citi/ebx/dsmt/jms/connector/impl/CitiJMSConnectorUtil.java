package com.citi.ebx.dsmt.jms.connector.impl;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.QueueSession;
import javax.jms.TextMessage;

/**
 * Class containing utility methods to be used by CitiJMSConnectorImpl class.
 * @author nc72931
 *
 */
public class CitiJMSConnectorUtil extends CitiJMSConnectorImpl {

	protected TextMessage setJMSHeaders(final TextMessage textmessage,
			final Map<String, String> propertiesMap, final QueueSession session) throws JMSException {
		
		logger.log(Level.INFO,  " jms properties map = " + propertiesMap);
		
		final Set<String> keySet = propertiesMap.keySet();
		final Iterator<String> keyIterator = keySet.iterator();
		
		while (keyIterator.hasNext()) {
			final String key = (String) keyIterator.next();
			final String value = propertiesMap.get(key);
			
			if(jmsHeaderSet.contains(key)){
				addJMSHeader(textmessage, key, value, session);
			}
			else{
				
				textmessage.setStringProperty(key, value);
			}
		}
		
		return textmessage;
	}

	protected void addJMSHeader(final TextMessage textmessage, final String key, final String value, final QueueSession session) throws JMSException {

		if(key.equalsIgnoreCase("JMSCorrelationID")){
			textmessage.setJMSCorrelationID(value);
		}
		
		else if(key.equalsIgnoreCase("JMSDestination")){
			
			final Destination destination = session.createQueue(value);
			textmessage.setJMSDestination(destination);
		}

		else if(key.equalsIgnoreCase("JMSMessageID")){
			
			textmessage.setJMSMessageID(value);
		}
		
		else if(key.equalsIgnoreCase("JMSReplyTo")){
			
			final Destination destination = session.createQueue(value);
			textmessage.setJMSReplyTo(destination);
		}
		
		else if(key.equalsIgnoreCase("JMSMessageID")){
			
			textmessage.setJMSMessageID(value);
		}
		
		else if(key.equalsIgnoreCase("JMSType")){
			
			textmessage.setJMSType(value);
		}
		
		else if(key.equalsIgnoreCase("JMSPriority")){
			
			if(value.equalsIgnoreCase("persistent") || value.equalsIgnoreCase("2") ){
				textmessage.setJMSPriority(DeliveryMode.PERSISTENT);
			}
			else if(value.equalsIgnoreCase("non_persistent") || value.equalsIgnoreCase("1") ){
				textmessage.setJMSPriority(DeliveryMode.NON_PERSISTENT);
			}
			
		}
	}
}
