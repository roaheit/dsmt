package com.citi.ebx.dsmt.jms;

import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.citi.ebx.workflow.vo.DSMTWorkflowRequestVO;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.service.comparison.CompareFilter;
import com.orchestranetworks.ui.UIHttpManagerComponent;

public class JMSComparisonMessageHelper extends JMSMessageHelper {
	public enum ComparisonLevel {
		TABLE, DATASET
	}
	
	private static final String REPLY_TO_QUEUE = "reply.to.queue";
	private static final String DSMT2_WORKFLOW_REQUEST = "DSMT2WorkflowRequest";
	private static final String DSMT2_WORKFLOW_REQUEST_85 = "DSMT2WorkflowRequest85";
	protected static final String BASE_URI = "/ebx/";
	
	private ComparisonLevel comparisonLevel = ComparisonLevel.TABLE;
	
	protected String workflowType;
	
	protected String bpmProcessID;
	protected String requestID;
	protected String dataSpace;
	protected String dataSet;
	protected String tableXPath;
	protected String user;
	protected String userComment;
	protected String tableID;
	protected String tableName;
	protected String workflowStatus;
	protected String parentID;
	
	public String getWorkflowType() {
		return workflowType;
	}

	public void setWorkflowType(String workflowType) {
		this.workflowType = workflowType;
	}

	public ComparisonLevel getComparisonLevel() {
		return comparisonLevel;
	}

	public void setComparisonLevel(ComparisonLevel comparisonLevel) {
		this.comparisonLevel = comparisonLevel;
	}

	public String getBpmProcessID() {
		return bpmProcessID;
	}

	public void setBpmProcessID(String bpmProcessID) {
		this.bpmProcessID = bpmProcessID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getRequestID() {
		
		return requestID;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public void setTableXPath(String tableXPath) {
		this.tableXPath = tableXPath;
	}

	public String getTableXPath() {
		return tableXPath;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getTableID() {
		return tableID;
	}

	public void setTableID(String tableID) {
		this.tableID = tableID;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getUserComment() {
		return userComment;
	}
	
	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}
	
	/**
	 * @return the parentID
	 */
	public String getParentID() {
		return parentID;
	}

	/**
	 * @param parentID the parentID to set
	 */
	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	/*public String createMessageContentOld() {
		log.info("JMSComparisonMessageHelper: createMessageContent");
		
		final String comparisonURI =  "<![CDATA[" + createURIForComparison() + "]]>";
		final String editURI =  "<![CDATA[" + createTableURIForEdit() + "]]>";
	
		log.info("JMSComparisonMessageHelper: comparisonURI = " + comparisonURI);
		log.info("JMSComparisonMessageHelper: editURI = " + editURI);
	
		StringBuilder xmlMessage = new StringBuilder(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
				"<request xmlns:ns0=\"http://com.citi.dsmt2.workflow\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns0:WorkflowRequest\"> " +
				"<requestID>" + requestID + "</requestID>  " +
				"<dataSpace>" + dataSpace + "</dataSpace>  " +
				"<dataSet>" + dataSet + "</dataSet>  " +
				"<comparisonURI>" + comparisonURI + " </comparisonURI>  ");
		if (ComparisonLevel.TABLE.equals(comparisonLevel)) {
			xmlMessage.append(
					"<tableURI>" + editURI + " </tableURI>  ");
		} else {
			xmlMessage.append(
					"<dataSetURI>" + editURI + "</dataSetURI>  ");
		}
		xmlMessage.append(
				"<userID>" + user + "</userID>	" +
				"<userComment>" + userComment + "</userComment>	");
		if (ComparisonLevel.TABLE.equals(comparisonLevel)) {
			xmlMessage.append(
				"<ebxRecordPath>" + tableXPath + "</ebxRecordPath>		" +
				"<ebxTableID>" + tableID + "</ebxTableID>		" +
				"<ebxTableName>" + tableName + "</ebxTableName>		");
		}
		xmlMessage.append(
				"<workflowStatus>" + workflowStatus + "</workflowStatus>		" +
				"</request>");
		
		log.info("JMSComparisonMessageHelper: xmlMessage being sent = " + xmlMessage);
		return xmlMessage.toString();
	}
	*/
	@Override
	public String createMessageContent()
	{	
		
		String requestXml= "";
	
	try{
		
		final String comparisonURI =  "<![CDATA[" + createURIForComparison() + "]]>";
		final String tableURI =  "<![CDATA[" + createTableURIForEdit() + "]]>";
		
		DSMTWorkflowRequestVO dsmtWorkflowRequest = new DSMTWorkflowRequestVO();
		
		dsmtWorkflowRequest.setWorkflowType(workflowType);
		
		dsmtWorkflowRequest.setRequestID(requestID);
		dsmtWorkflowRequest.setUserID(user);
		dsmtWorkflowRequest.setDataSpace(dataSpace);
		dsmtWorkflowRequest.setDataSet(dataSet);
		dsmtWorkflowRequest.setComparisonURI(comparisonURI);
		dsmtWorkflowRequest.setUserComment(wrapWithCDATA(userComment));
		
		dsmtWorkflowRequest.setTableURI(tableURI);
		
		dsmtWorkflowRequest.setEbxRecordPath(tableXPath);
		
		dsmtWorkflowRequest.setWorkFlowStatus(workflowStatus);	
		dsmtWorkflowRequest.setEbxTableName(tableName);
		dsmtWorkflowRequest.setEbxTableID(tableID);
		dsmtWorkflowRequest.setEbxRecordPath(tableXPath);

		
		JAXBContext jaxbContext = JAXBContext.newInstance(DSMTWorkflowRequestVO.class);
		
		Marshaller jaxbmMarshaller = jaxbContext.createMarshaller();
		jaxbmMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
		StringWriter sw = new StringWriter();
		jaxbmMarshaller.marshal(dsmtWorkflowRequest, sw);
		StringBuffer buffer= sw.getBuffer();
		requestXml=buffer.toString();
		
		requestXml=requestXml.replace("&amp;", "&");
		requestXml=requestXml.replace("&lt;", "<");
		requestXml=requestXml.replace("&gt;", ">");
		
		log.info("XML message to be sent to CWM -  " + requestXml);
		}
		catch (Exception e) {
			log.error("Exception during Request XML creation " + e + ". Check System Error log for details.");
			e.printStackTrace();
		}
	
	
	return requestXml ;
}
	
	
	@Override
	protected Map<String, String> createJMSMap() {
		HashMap<String, String> jmsMap = new HashMap<String, String>();
		
		jmsMap.put("JMSReplyTo", propertyHelper.getProp(REPLY_TO_QUEUE)); 
		jmsMap.put("JMSCorrelationID", dataSpace);
		
		if(isCWM85Live(dataSpace)){
			jmsMap.put("JMSType", DSMT2_WORKFLOW_REQUEST_85);
		}else{
			jmsMap.put("JMSType", DSMT2_WORKFLOW_REQUEST);
		}
		
		
		return jmsMap;
	}
	
	/**
	 * this method returns by looking at property "cwm.85.live.date" in ebx.properties file and whether the dataspace date is earlier or later than
	 * the cwm 85 live date.
	 * Helpful for BPM poller to adjust accordingly.
	 * @param dataSpace
	 * @return
	 */
	private boolean isCWM85Live(final String dataSpace) {
		
		String cwm85LiveDateStr =   propertyHelper.getProp("cwm.85.live.date"); // sample - "2015-03-16T00:00:00.000";  // 
		try {
			
			SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS"); 
			Date cwm85LiveDate = sdf.parse(cwm85LiveDateStr);
			Date dataSpaceDate = sdf.parse(dataSpace);
			log.info("{cwm85LiveDate " + cwm85LiveDate +  ", dataSpaceDate = " + dataSpaceDate + "}");
			if(cwm85LiveDate.before(dataSpaceDate)){
					return true;
				}
			else{
				return false;
			}
		} catch (ParseException e) {
			
			e.printStackTrace();
			return false; // any exceptions mean that the CWM 85 code is not live yet.
		}
		
	}


	protected String createURIForComparison() {
		log.debug("JMSComparisonMessageHelper: createURIForComparison");
		final HomeKey dataSpaceKey = HomeKey.forBranchName(dataSpace);
		final AdaptationHome dataSpaceRef = repository.lookupHome(dataSpaceKey);
		final AdaptationName dataSetName = AdaptationName.forName(dataSet);
		log.debug("JMSComparisonMessageHelper: dataSpace=" + dataSpaceKey + ", dataSet=" + dataSetName);

		final UIHttpManagerComponent uiHttpManagerComp =
				UIHttpManagerComponent.createWithBaseURI(BASE_URI);
		final HomeKey parentKey = dataSpaceRef.getParent().getKey();
		
		if (ComparisonLevel.TABLE.equals(comparisonLevel)) {
			uiHttpManagerComp.select(parentKey , dataSetName, tableXPath);
			uiHttpManagerComp.compareSelectionWithEntity( dataSpaceKey, dataSetName, tableXPath);
		} else {
			uiHttpManagerComp.selectInstance(parentKey , dataSetName);
			uiHttpManagerComp.compareSelectionWithEntity( dataSpaceKey, dataSetName, null);
		}
		
		uiHttpManagerComp.setCompareFilter(CompareFilter.PERSISTED_VALUES_ONLY);
		
		return uiHttpManagerComp.getURIWithParameters();
	}
	
	protected String createTableURIForEdit(){
		log.debug("JMSComparisonMessageHelper: createURIForEdit");
		final HomeKey dataSpaceKey = HomeKey.forBranchName(dataSpace);
		final AdaptationName dataSetName = AdaptationName.forName(dataSet);
		log.debug("JMSComparisonMessageHelper: dataSpace=" + dataSpaceKey + ", dataSet=" + dataSetName);

		final UIHttpManagerComponent uiHttpManagerComp =
				UIHttpManagerComponent.createWithBaseURI(BASE_URI);
		if (ComparisonLevel.TABLE.equals(comparisonLevel)) {
			uiHttpManagerComp.select(dataSpaceKey, dataSetName, tableXPath);
		} else {
			uiHttpManagerComp.selectInstance(dataSpaceKey, dataSetName);
		}
		return uiHttpManagerComp.getURIWithParameters();
	}
	
	
}
