package com.citi.ebx.dsmt.lrr;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class ChinaLRRConstraint implements
ConstraintOnTableWithRecordLevelCheck{
	
	private final LoggingCategory LOG = LoggingCategory.getKernel();
	public static final Path CTRY_CD_FK  = Path.parse("./CTRY_CD_FK");
	
	public static final Path LOC_OVERSEAS = Path.parse("./LOC_OVERSEAS");

	@Override
	public void checkTable(ValueContextForValidationOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
		// TODO Auto-generated method stub
		
		final ValueContext vc = context.getRecord();
		
		// remove any of the previous validation error messages
		context.removeRecordFromMessages(vc);
		final String locOverseas = (String) vc
				.getValue(LOC_OVERSEAS);
		final String stdCountryCode = (String) vc
				.getValue(CTRY_CD_FK);
		
		
		if(null!=locOverseas && "O".equals(locOverseas)){
			if(StringUtils.isEmpty(stdCountryCode)){
				context.addMessage(UserMessage.createError(
						"Please enter Standard Country Code Reference"));
			}
		}
		
	}

}
