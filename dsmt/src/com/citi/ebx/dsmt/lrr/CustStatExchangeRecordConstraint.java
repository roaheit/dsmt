package com.citi.ebx.dsmt.lrr;

import java.util.Locale;


import org.apache.commons.lang.StringUtils;

import com.citi.ebx.dsmt.util.DSMTConstants;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;


public class CustStatExchangeRecordConstraint implements ConstraintOnTableWithRecordLevelCheck{

	@Override
	public void checkTable(ValueContextForValidationOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkRecord(ValueContextForValidationOnRecord context) {
		final ValueContext vc = context.getRecord();
		// remove any of the previous validation error messages
		context.removeRecordFromMessages(vc);
		
		final String isListed = (String) context.getRecord()
				.getValue(DSMTConstants.EXCHANGE_LISTED);
		
		final String exchange = (String) context.getRecord()
				.getValue(DSMTConstants.EXCHANGE);
		
		if("Y".equalsIgnoreCase(isListed)){
			if(StringUtils.isNotBlank(exchange)){
				
			}else{
				context.addMessage(UserMessage
						.createError("Please select the Exchange or make Listed in exchange as N"));
			}
		}else if("N".equalsIgnoreCase(isListed)){
			if(StringUtils.isNotBlank(exchange)){
				context.addMessage(UserMessage
						.createError("Please remove the Exchange or make Listed in exchange as Y"));
			}
		}
		
	}

	

}
