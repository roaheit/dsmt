package com.citi.ebx.dsmt.jaxb.util;

import java.util.ArrayList;
import java.util.List;

public class GenerateGOCRequest {
	
	private final String N_A = "N/A";
	private final String EMPTY_STRING = "";
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		GenerateGOCRequest response = new GenerateGOCRequest();
		
	/*	String request="DSMT_AU_CTY_CONTROLLER_BU10500_ICG|DSMT_APAC_REGIONAL_DQ|DSMT_AU_ICG_COUNTRY_HR_APPROVER|DSMT_ICG_CTS_MSEG_APPROVER_ASIA|"+
				 " DSMT_ICG_GLOBAL_PA|N/A|N/A|DSMT_ICG_GLOBAL_HR_APPROVER|N/A|N/A|N/A|DSMT_AU_MAINTENANCE|DSMT_AU_GLMAINTENANCE_FLEX|DSMT_AU_REMAINTENANCE_SMART|"+
				 "DSMT_AU_P2PMAINTENANCE|P2P_DSMT2_GL_RE";
				 */
		String request= "DSMT_BR_CTY_CONTROLLER_BU10578_ALL|DSMT_LATAM_REGIONAL_DQ|DSMT_BR_SAP_COUNTRY_HR_APPROVER||||||||||DSMT_AU_GLMAINTENANCE_FLEX|DSMT_AU_REMAINTENANCE_SMART|DSMT_AU_P2PMAINTENANCE";
		
		request = "DSMT_US_CTY_CONTROLLER_BU10418_ALL||DSMT_US_TREASURY_COUNTRY_HR_APPROVER||||||||||DSMT_US_GL_MAINTENANCE_ECBF|DSMT_US_GL_MAINTENANCE_ECBF_2|";
		
		request = "DSMT_US_CTY_CONTROLLER_BU10581_ALL||||||DSMT_US_CORP_ITEMS_COUNTRY_HR_APPROVER|DSMT_CORPITEMS_GLOBAL_PandA||||||DSMT_US_RE_MAINTENANCE_SMART|DSMT_US_P2P_MAINTENANCE";
		//response.generateResponseXml(request);
		//response.generateResponseXml1(request);
		//System.out.println(response.generateResponseXmlfromVO(request));
		
	}
	
	 /**
	 * Helper method for generating partial xml for GOCResponse from input string having pipe separated value. 
	 * @param  String
	 * @throws 
	 * @return 
	 */
	public String generateResponseXmlfromVO(String inputdata)
	{
		
		String responsexml =EMPTY_STRING;
		
		try {
			
			GOCResponseVO gocResponseVO = generateGOCVO(inputdata);
			
		
									
					 responsexml= "<CountryController>" +gocResponseVO.getCountryController() +"</CountryController> "+
					"<RegionalDQTeam>" +gocResponseVO.getRegionalDQTeam() +"</RegionalDQTeam> "+ 
					"<CountryBusinessHR>" + gocResponseVO.getCountryBusinessHR() +"</CountryBusinessHR> "+
					"<FinanceRole1>" + gocResponseVO.getFinanceRole1() +"</FinanceRole1> "+ 
					"<FinanceRole2>" + gocResponseVO.getFinanceRole2() +"</FinanceRole2> "+
					"<FinanceRole3>" + gocResponseVO.getFinanceRole3() +"</FinanceRole3> "+
					"<FinanceRole4>" + gocResponseVO.getFinanceRole4() +"</FinanceRole4> "+
					"<FinanceRole5>" + gocResponseVO.getFinanceRole4() +"</FinanceRole5> "+
					"<HRRole1>" + gocResponseVO.getHrRole1() +"</HRRole1> "+
					"<HRRole2>" + gocResponseVO.getHrRole2() +"</HRRole2> "+
					"<HRRole3>" + gocResponseVO.getHrRole3() +"</HRRole3> "+
					"<HRRole4>" + gocResponseVO.getHrRole4()+"</HRRole4> "+
					"<Dsmt2Maintenance>" + gocResponseVO.getDsmt2Maintenance() +"</Dsmt2Maintenance>"+ 
					"<GlMaintenance>" + gocResponseVO.getGlMaintenance() +"</GlMaintenance> "+
					"<REMaintenance>" + gocResponseVO.getReMaintenance() +"</REMaintenance> "+
					"<P2PMaintenance>" + gocResponseVO.getP2PMaintenance() +"</P2PMaintenance> "+
					"<MaintenanceOrder>" + gocResponseVO.getMaintenanceOrder() +"</MaintenanceOrder>" ;
			
		
		//jaxbmMarshaller.marshal(request, System.out);
		//(responsexml);
			
			 
			 
		}
		
		catch (Exception e) {
		
		}	
		return responsexml;
		
	}
	
	 /**
		 * Helper method for generating GOCResponseVO from input string having pipe seperated value. 
		 * @param  String
		 * @throws 
		 * @return GOCResponseVO
		 */
	public GOCResponseVO generateGOCVO(String inputdata)
	{
		
		GOCResponseVO request= new GOCResponseVO();
		String csvSplitBy="[|]";
		
			String[] responsevalues = inputdata.split(csvSplitBy,-1);
			
			request.setCountryController(responsevalues[0] );
			request.setRegionalDQTeam(responsevalues[1] );
			request.setCountryBusinessHR(responsevalues[7] );
			List<String> financeRole = new ArrayList<String>();
			List<String> hrRole = new ArrayList<String>();
			
			
			try {
			if (!(responsevalues[2].equals(EMPTY_STRING)|| responsevalues[2].equals(N_A)))
			{
				financeRole.add(responsevalues[2]);	
			}
			if (!(responsevalues[3].equals(EMPTY_STRING)|| responsevalues[3].equals(N_A)))
			{
				financeRole.add(responsevalues[3]);	
			}
			if (!(responsevalues[4].equals(EMPTY_STRING)|| responsevalues[4].equals(N_A)))
			{
				financeRole.add(responsevalues[4]);	
			}
			if (!(responsevalues[5].equals(EMPTY_STRING)|| responsevalues[5].equals(N_A)))
			{
				financeRole.add(responsevalues[5]);	
			}
			if (!(responsevalues[6].equals(EMPTY_STRING)|| responsevalues[6].equals(N_A)))
			{
				financeRole.add(responsevalues[6]);	
			}
			
				
				
				if (!(responsevalues[8].equals(EMPTY_STRING)|| responsevalues[8].equals(N_A)))
				{
					hrRole.add(responsevalues[8]);	
				}
				if (!(responsevalues[9].equals(EMPTY_STRING)|| responsevalues[9].equals(N_A)))
				{
					hrRole.add(responsevalues[9]);	
				}
				if (!(responsevalues[10].equals(EMPTY_STRING)|| responsevalues[10].equals(N_A)))
				{
					hrRole.add(responsevalues[10]);	
				}
				if (!(responsevalues[11].equals(EMPTY_STRING)|| responsevalues[11].equals(N_A)))
				{
					hrRole.add(responsevalues[11]);	
				}
				
				
				
			
			} catch (Exception e) {
				e.printStackTrace();
			}
			for(int hrlength= hrRole.size(); hrlength<4;hrlength++  )
			{
				hrRole.add("");
			}

			for(int financeLength= financeRole.size(); financeLength<5;financeLength++  )
			{
				financeRole.add("");
			}
			request.setFinanceRole1(financeRole.get(0) );
			request.setFinanceRole2(financeRole.get(1) );
			request.setFinanceRole3(financeRole.get(2) );
			request.setFinanceRole4(financeRole.get(3) );
			request.setFinanceRole5(financeRole.get(4) );
			request.setHrRole1(hrRole.get(0) );
			request.setHrRole2(hrRole.get(1) );
			request.setHrRole3(hrRole.get(2) );
			request.setHrRole4(hrRole.get(3) );
			try {
			request.setGlMaintenance(responsevalues[12] );
			request.setReMaintenance(responsevalues[13] );
			request.setP2PMaintenance(responsevalues[14] );
			request.setRegionalFinance(responsevalues[16]);
			request.setPlanningUnitApprover(responsevalues[17]);
			}catch (Exception e) {
				e.printStackTrace();
			}	
			
		
		
		
		return request ;
		
	}
	
	

}
