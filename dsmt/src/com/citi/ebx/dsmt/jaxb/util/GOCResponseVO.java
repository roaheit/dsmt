package com.citi.ebx.dsmt.jaxb.util;

import com.citi.ebx.dsmt.util.DSMTConstants;

public class GOCResponseVO {
	
	 
	    protected String countryController ="";
	   
	    protected String regionalDQTeam ="";
	   
	    protected String countryBusinessHR ="";
	   
	    protected String financeRole1 ="";
	   
	    protected String financeRole2 ="";
	   
	    protected String financeRole3  ="" ;
	   
	    protected String financeRole4 ="";
	    
	    protected String financeRole5 ="";
	   
	    protected String hrRole1 ="";
	   
	    protected String hrRole2 ="";
	   
	    protected String hrRole3 ="";
	   
	    protected String hrRole4 ="";
	   
	    protected String dsmt2Maintenance ="";
	   
	    protected String glMaintenance ="";
	   
	    protected String reMaintenance ="";
	   
	    protected String p2PMaintenance ="";
	   
	    protected String maintenanceOrder =""  ;
	    
	    protected String regionalFinance =""  ;
	    
	    protected String planningUnitApprover =""  ;

		/**
		 * @return the countryController
		 */
		public String getCountryController() {
			return countryController;
		}

		/**
		 * @param countryController the countryController to set
		 */
		public void setCountryController(String countryController) {
			if(countryController.equals(DSMTConstants.NA)){
				this.countryController ="";
			}
			else
			{
			this.countryController = countryController;
			}
		}

		/**
		 * @return the regionalDQTeam
		 */
		public String getRegionalDQTeam() {
			return regionalDQTeam;
		}

		/**
		 * @param regionalDQTeam the regionalDQTeam to set
		 */
		public void setRegionalDQTeam(String regionalDQTeam) {
			if(regionalDQTeam.equals(DSMTConstants.NA)){
				this.regionalDQTeam ="";
			}
			else
			{
				this.regionalDQTeam = regionalDQTeam;
			}
			
		}

		/**
		 * @return the countryBusinessHR
		 */
		public String getCountryBusinessHR() {
			return countryBusinessHR;
		}

		/**
		 * @param countryBusinessHR the countryBusinessHR to set
		 */
		public void setCountryBusinessHR(String countryBusinessHR) {
			if(countryBusinessHR.equals(DSMTConstants.NA)){
				this.countryBusinessHR ="";
			}
			else
			{
				this.countryBusinessHR = countryBusinessHR;
			}
			
		}

		/**
		 * @return the financeRole1
		 */
		public String getFinanceRole1() {
			return financeRole1;
		}

		/**
		 * @param financeRole1 the financeRole1 to set
		 */
		public void setFinanceRole1(String financeRole1) {
			if(financeRole1.equals(DSMTConstants.NA)){
				this.financeRole1 ="";
			}
			else
			{
				this.financeRole1 = financeRole1;
			}
			
		}

		/**
		 * @return the financeRole2
		 */
		public String getFinanceRole2() {
			return financeRole2;
		}

		/**
		 * @param financeRole2 the financeRole2 to set
		 */
		public void setFinanceRole2(String financeRole2) {
			if(financeRole2.equals(DSMTConstants.NA)){
				this.financeRole2 ="";
			}
			else
			{
				this.financeRole2 = financeRole2;
			}
			
		}

		/**
		 * @return the financeRole3
		 */
		public String getFinanceRole3() {
			return financeRole3;
		}

		/**
		 * @param financeRole3 the financeRole3 to set
		 */
		public void setFinanceRole3(String financeRole3) {
			if(financeRole3.equals(DSMTConstants.NA)){
				this.financeRole3 ="";
			}
			else
			{
				this.financeRole3 = financeRole3;
			}
			
		}

		/**
		 * @return the financeRole4
		 */
		public String getFinanceRole4() {
			return financeRole4;
		}

		/**
		 * @param financeRole4 the financeRole4 to set
		 */
		public void setFinanceRole4(String financeRole4) {
			if(financeRole4.equals(DSMTConstants.NA)){
				this.financeRole4 ="";
			}
			else
			{
				this.financeRole4 = financeRole4;
			}
			
		}

		/**
		 * @return the hrRole1
		 */
		public String getHrRole1() {
			return hrRole1;
		}

		/**
		 * @param hrRole1 the hrRole1 to set
		 */
		public void setHrRole1(String hrRole1) {
			if(hrRole1.equals(DSMTConstants.NA)){
				this.hrRole1 ="";
			}
			else
			{
				this.hrRole1 = hrRole1;
			}
			
		}

		/**
		 * @return the hrRole2
		 */
		public String getHrRole2() {
			return hrRole2;
		}

		/**
		 * @param hrRole2 the hrRole2 to set
		 */
		public void setHrRole2(String hrRole2) {
			if(hrRole2.equals(DSMTConstants.NA)){
				this.hrRole2 ="";
			}
			else
			{
				this.hrRole2 = hrRole2;
			}
			
		}

		/**
		 * @return the hrRole3
		 */
		public String getHrRole3() {
			return hrRole3;
		}

		/**
		 * @param hrRole3 the hrRole3 to set
		 */
		public void setHrRole3(String hrRole3) {
			if(hrRole3.equals(DSMTConstants.NA)){
				this.hrRole3 ="";
			}
			else
			{
				this.hrRole3 = hrRole3;
			}
		}

		/**
		 * @return the hrRole4
		 */
		public String getHrRole4() {
			return hrRole4;
		}

		/**
		 * @param hrRole4 the hrRole4 to set
		 */
		public void setHrRole4(String hrRole4) {
			if(hrRole4.equals(DSMTConstants.NA)){
				this.hrRole4 ="";
			}
			else
			{
				this.hrRole4 = hrRole4;
			}
		}

		/**
		 * @return the dsmt2Maintenance
		 */
		public String getDsmt2Maintenance() {
			return dsmt2Maintenance;
		}

		/**
		 * @param dsmt2Maintenance the dsmt2Maintenance to set
		 */
		public void setDsmt2Maintenance(String dsmt2Maintenance) {
			if(dsmt2Maintenance.equals(DSMTConstants.NA)){
				this.dsmt2Maintenance ="";
			}
			else
			{
				this.dsmt2Maintenance = dsmt2Maintenance;
			}
			
		}

		/**
		 * @return the glMaintenance
		 */
		public String getGlMaintenance() {
			return glMaintenance;
		}

		/**
		 * @param glMaintenance the glMaintenance to set
		 */
		public void setGlMaintenance(String glMaintenance) {
			if(glMaintenance.equals(DSMTConstants.NA)){
				this.glMaintenance ="";
			}
			else
			{
				this.glMaintenance = glMaintenance;
			}
		}

		/**
		 * @return the reMaintenance
		 */
		public String getReMaintenance() {
			return reMaintenance;
		}

		/**
		 * @param reMaintenance the reMaintenance to set
		 */
		public void setReMaintenance(String reMaintenance) {
			if(reMaintenance.equals(DSMTConstants.NA)){
				this.reMaintenance ="";
			}
			else
			{
				this.reMaintenance = reMaintenance;
			}
		}

		/**
		 * @return the p2PMaintenance
		 */
		public String getP2PMaintenance() {
			return p2PMaintenance;
		}

		/**
		 * @param p2pMaintenance the p2PMaintenance to set
		 */
		public void setP2PMaintenance(String p2PMaintenance) {
			if(p2PMaintenance.equals(DSMTConstants.NA)){
				this.p2PMaintenance ="";
			}
			else
			{
				this.p2PMaintenance = p2PMaintenance;
			}
			
		}

		/**
		 * @return the maintenanceOrder
		 */
		public String getMaintenanceOrder() {
			return maintenanceOrder;
		}

		/**
		 * @param maintenanceOrder the maintenanceOrder to set
		 */
		public void setMaintenanceOrder(String maintenanceOrder) {
			if(maintenanceOrder.equals(DSMTConstants.NA)){
				this.maintenanceOrder ="";
			}
			else
			{
				this.maintenanceOrder = maintenanceOrder;
			}
			
		}

		public String getFinanceRole5() {
			return financeRole5;
		}

		public void setFinanceRole5(String financeRole5) {
			if(financeRole5.equals(DSMTConstants.NA)){
				this.financeRole5 ="";
			}
			else
			{
				this.financeRole5 = financeRole5;
			}
			
		}

		public String getRegionalFinance() {
			return regionalFinance;
		}

		public void setRegionalFinance(String regionalFinance) {
			if(regionalFinance.equals(DSMTConstants.NA)){
				this.regionalFinance = "";
			}else{
				this.regionalFinance = regionalFinance;
			}
			
		}

		public String getPlanningUnitApprover() {
			return planningUnitApprover;
		}

		public void setPlanningUnitApprover(String planningUnitApprover) {
			if(planningUnitApprover.equals(DSMTConstants.NA)){
				this.planningUnitApprover = "";
			}else{
				this.planningUnitApprover = planningUnitApprover;
			}
		}
		
		

}
