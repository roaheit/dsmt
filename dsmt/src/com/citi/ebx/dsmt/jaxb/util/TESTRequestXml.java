package com.citi.ebx.dsmt.jaxb.util;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.citi.ebx.dsmt.jaxb.util.GOCResponseVO;
import com.citi.ebx.dsmt.jaxb.util.GenerateGOCRequest;
import com.citi.ebx.workflow.vo.DSMTWorkflowRequestVO;

public class TESTRequestXml {
	

	private String sid;
	private boolean isWarning;
	private boolean isHeadCountGOC;
	private String routingRoleIDs;
	private String glTableURI;
	private String reTableURI;
	private String p2pTableURI;
	
	private String comparisonLevel;
	protected String bpmProcessID;
	protected String requestID;
	protected String dataSpace;
	protected String dataSet;
	protected String tableXPath;
	protected String user;
	protected String userComment;
	protected String tableID;
	protected String tableName;
	protected String workflowStatus;

	public static void main(String[] args) {
		
		
		TESTRequestXml requestXml = new TESTRequestXml();
		requestXml.generateWorkflowRequest();
	}
	
	protected String generateWorkflowRequest()
	{	String requestXml= "";
	final String comparisonURI =  "<![CDATA[ ]]>";
	final String gocTableURI ="";
	//String request= "DSMT_BR_CTY_CONTROLLER_BU10578_ALL|DSMT_LATAM_REGIONAL_DQ|DSMT_BR_SAP_COUNTRY_HR_APPROVER||||||||||DSMT_AU_GLMAINTENANCE_FLEX|DSMT_AU_REMAINTENANCE_SMART|DSMT_AU_P2PMAINTENANCE";
	String request ="DSMT_BR_CTY_CONTROLLER_BU10578_ALL|DSMT_LATAM_REGIONAL_DQ|DSMT_BR_GCB_COUNTRY_HR_APPROVER|N/A|N/A|DSMT_ICG_OS&B_MSEG_APPROVER_LATAM|DSMT_ICG_GLOBAL_P&A||DSMT_ICG_GLOBAL_HR_APPROVER|N/A|N/A|N/A|DSMT_AU_GLMAINTENANCE";
	request= "DSMT_BR_CTY_CONTROLLER_BU10578_ALL|DSMT_LATAM_REGIONAL_DQ|DSMT_BR_SAP_COUNTRY_HR_APPROVER||||||||||||";
	 GOCResponseVO gocResponseVO =  new GenerateGOCRequest().generateGOCVO(request);
	try{
		
		XMLGregorianCalendar callender = DatatypeFactory.newInstance().newXMLGregorianCalendar("2013-12-10T11:58:48.74");
		// set the Value for parent Tag
		DSMTWorkflowRequestVO dsmtWorkflowRequest = new DSMTWorkflowRequestVO();
		dsmtWorkflowRequest.setWorkflowType("GOC");
		dsmtWorkflowRequest.setRequestID(requestID);
		dsmtWorkflowRequest.setUserID(user);
		dsmtWorkflowRequest.setDataSpace(dataSpace);
		dsmtWorkflowRequest.setDataSet(dataSet);
		dsmtWorkflowRequest.setComparisonURI(comparisonURI);
		dsmtWorkflowRequest.setUserComment(userComment);
		dsmtWorkflowRequest.setTableURI(gocTableURI);
		dsmtWorkflowRequest.setEbxRecordPath(tableXPath);
		dsmtWorkflowRequest.setWorkFlowStatus(workflowStatus);

		// set the value for Goc Tags
		
		DSMTWorkflowRequestVO.GocRequest gocRequest = new DSMTWorkflowRequestVO.GocRequest();
		gocRequest.setSid(sid);
		gocRequest.setGridRoutingID(dataSpace);
		gocRequest.setIsWarning(isWarning);
		gocRequest.setIsHeadCountGOC(isHeadCountGOC);
		gocRequest.setCountryController(gocResponseVO.getCountryController());
		gocRequest.setRegionalDQTeam(gocResponseVO.getRegionalDQTeam());
		gocRequest.setCountryBusinessHR(gocResponseVO.getCountryBusinessHR());
		gocRequest.setFinanceRole1(gocResponseVO.getFinanceRole1());
		gocRequest.setFinanceRole2(gocResponseVO.getFinanceRole2());
		gocRequest.setFinanceRole3(gocResponseVO.getFinanceRole3());
		gocRequest.setFinanceRole4(gocResponseVO.getFinanceRole4());
		gocRequest.setFinanceRole5(gocResponseVO.getFinanceRole5());
		gocRequest.setHRRole1(gocResponseVO.getHrRole1());
		gocRequest.setHRRole2(gocResponseVO.getHrRole1());
		gocRequest.setHRRole3(gocResponseVO.getHrRole1());
		gocRequest.setHRRole4(gocResponseVO.getHrRole1());
		//gocRequest.setDsmt2Maintenance(gocResponseVO.getDsmt2Maintenance());
		gocRequest.setGlMaintenance(gocResponseVO.getGlMaintenance());
		gocRequest.setREMaintenance(gocResponseVO.getReMaintenance());
		gocRequest.setP2PMaintenance(gocResponseVO.getP2PMaintenance());
		gocRequest.setMaintenanceOrder(gocResponseVO.getMaintenanceOrder());
		gocRequest.setBusinessDueDate(callender);
		gocRequest.setAttestationAttachedForDeactivation(true);
		gocRequest.setGlTableURI(glTableURI);
		gocRequest.setReportingEngineTableURI("![CDATA[/ebx]]");
		gocRequest.setP2PTableURI(p2pTableURI);
		
	
		dsmtWorkflowRequest.setGocRequest(gocRequest);
		JAXBContext jaxbContext = JAXBContext.newInstance(DSMTWorkflowRequestVO.class);
		
		Marshaller jaxbmMarshaller = jaxbContext.createMarshaller();
		jaxbmMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
		StringWriter sw = new StringWriter();
		jaxbmMarshaller.marshal(dsmtWorkflowRequest, sw);
		StringBuffer buffer= sw.getBuffer();
		requestXml=buffer.toString();
		//System.out.println("buffer >> "+requestXml);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	
	
	return requestXml ;
}

}
