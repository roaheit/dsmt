/**
 * 
 */
package com.citi.ebx.constraint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.ConstraintEnumeration;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

/**
 * @author rk00242
 *
 */
public class GOCActionRequiredConstraintEnumeration implements ConstraintEnumeration {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	Map<String,String> act_map = null;
	List<String> req_act_lst =  null;
	
	@Override
	public void checkOccurrence(Object obj, ValueContextForValidation context)
			throws InvalidSchemaException {
	}
	
	@Override
	public void setup(ConstraintContext context) {
		// do nothing
	}
	
	@Override
	public String toUserDocumentation(Locale locale, ValueContext context)
			throws InvalidSchemaException {
		return "Value must be a row in the table defined by the rule.";
	}
	
	@Override
	public String displayOccurrence(Object arg0, ValueContext arg1, Locale arg2)
			throws InvalidSchemaException {
		String sel_val = (String)arg0, desc ="";
		if( null == act_map) {
			req_act_lst = new ArrayList<String>();
			req_act_lst = getAction_lst(arg1);
		}
		if( null != act_map)
			desc = act_map.get(sel_val) +" ["+sel_val+"]";
		return desc;
	}
	@Override
	public List getValues(ValueContext context) throws InvalidSchemaException {
		return getAction_lst(context);		
	}
	public List getAction_lst(ValueContext context)  {
		req_act_lst = new ArrayList<String>();
		Adaptation dataSet = null;
		Adaptation metadataDataSet  = null;
		AdaptationTable table = null;
		RequestResult reqRes = null;
		try {
			dataSet = context.getAdaptationInstance();
			final String maintenanceType = GOCWorkflowUtils.getMaintenanceType(dataSet);
			metadataDataSet = GOCWorkflowUtils.getMetadataDataSet(context.getHome().getRepository());
			Path tablePath = GOCWFPaths._C_DSMT_ACT_ROLE_MAP.getPathInSchema();
			table = metadataDataSet.getTable(tablePath); 
			String predicate = null;
			if(null != maintenanceType){
				if(GOCConstants.MAINTENANCE_TYPE_REGULAR.equals(maintenanceType))
					predicate = GOCWFPaths._C_DSMT_ACT_ROLE_MAP._GOC_WF.format() + 	" = '"+ GOCConstants.CONSTANT_YES + "'";
				else
					predicate = GOCWFPaths._C_DSMT_ACT_ROLE_MAP._FUNCT_WF.format() + 	" = '"+ GOCConstants.CONSTANT_YES + "'";
			}
			act_map = new HashMap<String,String>();
			if (table != null) {
				reqRes = table.createRequestResult(predicate);
				String req_act = "", req_actCod="";
				for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) { 
					req_act = record.getString(Path.parse("./ACTION_REQ"));
					req_actCod	= record.getString(Path.parse("./ACTION_REQ_CODE")).toUpperCase();
					act_map.put(req_actCod, req_act);
					if(null != req_actCod && !req_actCod.equals(""))
						req_act_lst.add(req_actCod);
				}
				req_act_lst.add("Z");
				act_map.put("Z","No Change required");
			}
		} 
		catch(Exception e){
			LOG.info("Exception from GOCConstraintEnumeration::"+e);e.printStackTrace();
		}
		finally {
			if(reqRes != null) reqRes = null;
			if(table != null) table = null;
			if(metadataDataSet != null) metadataDataSet = null;
			if(dataSet != null) dataSet = null;
		}	
		return req_act_lst;
	}
}

