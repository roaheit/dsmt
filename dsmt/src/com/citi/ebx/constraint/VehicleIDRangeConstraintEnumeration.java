package com.citi.ebx.constraint;


public class VehicleIDRangeConstraintEnumeration {

/*
 
implements
		ConstraintEnumeration {
	
	private static final Path RANGE_TABLE_PATH =
			DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_LegalVehicleIDRange.getPathInSchema();
	private static final Path RANGE_START_PATH =
			DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_LegalVehicleIDRange._Start;
	private static final Path RANGE_END_PATH =
			DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_LegalVehicleIDRange._End;
	
	private static final int NUM_DIGITS = 5;
	
	@Override
	public void checkOccurrence(Object value, ValueContextForValidation context)
			throws InvalidSchemaException {
		// do nothing
	}

	@Override
	public void setup(ConstraintContext context) {
		// do nothing
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext context)
			throws InvalidSchemaException {
		return "Values are defined in " + RANGE_TABLE_PATH.format() + " table.";
	}

	@Override
	public String displayOccurrence(Object value, ValueContext context, Locale locale)
			throws InvalidSchemaException {
		return (String) value;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getValues(ValueContext context) throws InvalidSchemaException {
		final AdaptationTable rangeTable =
				context.getAdaptationInstance().getTable(RANGE_TABLE_PATH);
		final ArrayList<String> values = new ArrayList<String>();
		final RequestResult reqRes = rangeTable.createRequestResult(null);
		try {
			for (Adaptation range; (range = reqRes.nextAdaptation()) != null;) {
				final int startInt = range.get_int(RANGE_START_PATH);
				final int endInt = range.get_int(RANGE_END_PATH);
				
				final NumberFormat numberFormat = NumberFormat.getInstance();
				numberFormat.setMinimumIntegerDigits(NUM_DIGITS);
				values.add(numberFormat.format(startInt) + " - " + numberFormat.format(endInt));
			}
		} finally {
			reqRes.close();
		}
		return values;
	}
*/	
}
