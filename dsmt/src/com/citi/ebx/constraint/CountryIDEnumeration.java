package com.citi.ebx.constraint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.ConstraintEnumeration;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class CountryIDEnumeration implements
		ConstraintEnumeration {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path dsmtCountryPath = Path
	.parse("/root/GLOBAL_STD/ENT_STD/C_DSMT_NEW_CNTRY");
	private Path countryCodePath = Path.parse("./COUNTRY");

	@Override
	public void checkOccurrence(Object arg0, ValueContextForValidation vc)
			throws InvalidSchemaException {

	}

	@Override
	public void setup(ConstraintContext arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		return "Value is unique from the City Status table ";
	}

	@Override
	public String displayOccurrence(Object arg0, ValueContext arg1, Locale arg2)
			throws InvalidSchemaException {
		return (String) arg0;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getValues(ValueContext context) throws InvalidSchemaException {

		Adaptation dataSet = context.getAdaptationInstance();
		AdaptationTable table = dataSet.getTable(dsmtCountryPath);
		final ArrayList<String> values = new ArrayList<String>();
		if (table == null) {
			return values;
		}
		RequestResult reqRes = table.createRequestResult(null);
		try {
			// loop through all records
			for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
				String code = record.getString(countryCodePath);
				values.add(code);
				final HashSet domainSet = new HashSet();
				domainSet.addAll(values);
				values.clear();
				values.addAll(domainSet);
				Collections.sort(values);

			}

		} finally {
			reqRes.close();
		}

		return values;
	}

}

