package com.citi.ebx.constraint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.goc.path.DSMT2Paths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.ConstraintEnumeration;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class AccontibilityregionEnumeration implements
ConstraintEnumeration {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path globalCountryTable = DSMT2Paths._GLOBAL_STD_ENT_STD_C_GBL_CNTRY.getPathInSchema();
	final String env = DSMTConstants.propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT); // "local" ; 
	private String  region ;
	private  String DataSetName;
	private  String DataSpaceName;
	
	
	

	@Override
	public void checkOccurrence(Object arg0, ValueContextForValidation vc)
			throws InvalidSchemaException {
		
		Repository repo  = vc.getHome().getRepository();
		
		try {
			
			Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,DataSpaceName, DataSetName);
			AdaptationTable targetTable = metadataDataSet.getTable(globalCountryTable);
			final String pred1 = DSMT2Paths._GLOBAL_STD_ENT_STD_C_GBL_CNTRY._COUNTRY_REGION.format() + "= '" + region + "'";
			

			final String predicate = pred1  ;

			RequestResult reqRes = targetTable.createRequestResult(predicate);
			if (reqRes.isEmpty()) {
				vc.addError("There is no country belonging to region "+region);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		

	}

	@Override
	public void setup(ConstraintContext arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		return "Value is unique from the Account Category table ";
	}

	@Override
	public String displayOccurrence(Object arg0, ValueContext arg1, Locale arg2)
			throws InvalidSchemaException {
		return (String) arg0;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getValues(ValueContext context) throws InvalidSchemaException {

		
		
		Repository repo  = context.getHome().getRepository();
		final ArrayList<String> values = new ArrayList<String>();
		
	try {
		
		
		final String pred1 = DSMT2Paths._GLOBAL_STD_ENT_STD_C_GBL_CNTRY._COUNTRY_REGION.format() + "= '" + region + "'";
		//LOG.info("AccontibilityregionEnumeration.getValues region "+region);

	

		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,DataSpaceName, DataSetName);
		AdaptationTable targetTable = metadataDataSet.getTable(globalCountryTable);
		RequestResult reqRes = targetTable.createRequestResult(pred1);
		
		try {
			// loop through all records
			//LOG.info("AccontibilityregionEnumeration.getValues resultset set size   "+reqRes.getSize());
			for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
				String code = record.getString(DSMT2Paths._GLOBAL_STD_ENT_STD_C_GBL_CNTRY._DESCR);
				// LOG.info("AccontibilityregionEnumeration.getValues country   "+ code);
				values.add(code);
				final HashSet domainSet = new HashSet();
				domainSet.addAll(values);
				values.clear();
				values.addAll(domainSet);
				Collections.sort(values);

			}

		} finally {
			reqRes.close();
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
		

		return values;
	}
	

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getDataSetName() {
		return DataSetName;
	}

	public void setDataSetName(String dataSetName) {
		DataSetName = dataSetName;
	}

	public String getDataSpaceName() {
		return DataSpaceName;
	}

	public void setDataSpaceName(String dataSpaceName) {
		DataSpaceName = dataSpaceName;
	}

}
