package com.citi.ebx.constraint;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.ConstraintEnumeration;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;

public class ArrangementLCStatInfoConstraintEnumeration implements ConstraintEnumeration{

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	private Path arrangementLCStatPath = Path
			.parse("/root/GLOBAL_STD/ENT_STD/F_ARNG_LIFE/C_DSMT_ARR_STA");
	private Path arrangementLCStatCode = Path.parse("./C_DSMT_LIFE_STA_CD");
	private Path status = Path.parse("./EFF_STATUS");
	private Path endDateFieldPath = Path.parse("./ENDDT");

	@Override
	public void checkOccurrence(Object arg0, ValueContextForValidation vc)
			throws InvalidSchemaException {
		Adaptation dataSet = vc.getAdaptationInstance();
		AdaptationTable table = dataSet.getTable(arrangementLCStatPath);

		final String pred1 = arrangementLCStatCode.format() + "= '" + arg0 + "'";
		final String pred2 = status.format() + "= 'A'";
		final String pred3 = "date-equal(" + endDateFieldPath.format()
				+ ",'9999-12-31')";
		final String pred4 = "osd:is-null(" + endDateFieldPath.format() + ")";

		final String predicate = pred1 + " and " + pred2 + " and (" + pred3
				+ " or " + pred4 + ")";

		RequestResult reqRes = table.createRequestResult(predicate);
		if (reqRes.isEmpty()) {
			vc.addError("ArrangementLC Status Code " + (String) arg0 + " is not active.");
		}


	}

	@Override
	public void setup(ConstraintContext arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		return "Value is unique from the Arrangement LifeCycle table ";
	}

	@Override
	public String displayOccurrence(Object arg0, ValueContext arg1, Locale arg2)
			throws InvalidSchemaException {
		// LOG.info("arg0" +arg0);
		return (String) arg0.toString();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getValues(ValueContext context) throws InvalidSchemaException {

		Adaptation dataSet = context.getAdaptationInstance();
		AdaptationTable table = dataSet.getTable(arrangementLCStatPath);
		final ArrayList<BigDecimal> values = new ArrayList<BigDecimal>();
		if (table == null) {
			return values;
		}
		RequestResult reqRes = table.createRequestResult(null);
		try {
			// loop through all records
			for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
				BigDecimal code = (BigDecimal) record.get(arrangementLCStatCode);
				values.add(code);
				final HashSet domainSet = new HashSet();
				domainSet.addAll(values);
				values.clear();
				values.addAll(domainSet);
				Collections.sort(values);
			}

		} finally {
			reqRes.close();
		}

		return values;
	}
}
