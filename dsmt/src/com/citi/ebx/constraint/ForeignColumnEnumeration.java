package com.citi.ebx.constraint;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.RepositoryAccessException;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.ConstraintEnumeration;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.service.LoggingCategory;

public class ForeignColumnEnumeration implements
		ConstraintEnumeration {
	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	
	
	
	/**
	 * 
	 */
	private String sourceTableXPath;  // 	"/root/GLOBAL_STD/ENT_STD/F_BAL_TYPE/C_DSMT_ACC_CTGR"
	private String sourceColumnName;  //	"./C_DSMT_ACC_CATGORY"
	
	private Path sourcePath = null;
	private Path sourceColumn = null;
	
//	private Path accounCategoryPath = Path.parse("/root/GLOBAL_STD/ENT_STD/F_BAL_TYPE/C_DSMT_ACC_CTGR");
//	private Path accountCategoryCode = Path.parse("./C_DSMT_ACC_CATGORY");
	private Path status = Path.parse("./EFF_STATUS");
	private Path endDateFieldPath = Path.parse("./ENDDT");

	@Override
	public void checkOccurrence(Object arg0, ValueContextForValidation vc)
			throws InvalidSchemaException {
		
		try {
			
			sourcePath = Path.parse(sourceTableXPath);
			sourceColumn = Path.parse(sourceColumnName);
			
			Adaptation dataSet = vc.getAdaptationInstance();
			AdaptationTable table = dataSet.getTable(sourcePath);

			final String pred1 = sourceColumn.format() + "= '" + arg0 + "'";
			final String pred2 = status.format() + "= 'A'";
			final String pred3 = "date-equal(" + endDateFieldPath.format()
					+ ",'9999-12-31')";
			final String pred4 = "osd:is-null(" + endDateFieldPath.format() + ")";

			final String predicate = pred1 + " and " + pred2 + " and (" + pred3
					+ " or " + pred4 + ")";

			RequestResult reqRes = table.createRequestResult(predicate);
			if (reqRes.isEmpty()) {
				vc.addWarning((String) arg0 + " is not active.");
			}
		} catch (PathAccessException e) {
			LOG.error("Path Access Exception " + e.getMessage());
		} catch (RepositoryAccessException e) {
			// TODO Auto-generated catch block
			LOG.error("Repository Access Exception " + e.getMessage());
		}

	}

	@Override
	public void setup(ConstraintContext arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1)
			throws InvalidSchemaException {
		return "Value is unique from the source table " + sourceTableXPath + ", source column = " + sourceColumnName;
	}

	@Override
	public String displayOccurrence(Object arg0, ValueContext arg1, Locale arg2)
			throws InvalidSchemaException {
		
		return (String) arg0.toString();
		
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getValues(ValueContext context) throws InvalidSchemaException {

		ArrayList<String> values = null;
		
		
		try {
			
			sourcePath = Path.parse(sourceTableXPath);
			sourceColumn = Path.parse(sourceColumnName);
			
			
			LOG.info("sourcePath" + sourcePath + ", sourceColumn " + sourceColumn);
			Adaptation dataSet = context.getAdaptationInstance();
			AdaptationTable table = dataSet.getTable(sourcePath);
			values = new ArrayList<String>();
			if (table == null) {
				return values;
			}
			RequestResult reqRes = table.createRequestResult(null);
			try {
				// loop through all records
				for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
					
					String code = null;
					Object codeObj = record.get(sourceColumn);
					
					if(codeObj instanceof BigDecimal ){
						code = String.valueOf((BigDecimal)record.get(sourceColumn));
					}else if(codeObj instanceof String ){
						code = record.getString(sourceColumn);
					}
					values.add(code);
					final HashSet domainSet = new HashSet();
					domainSet.addAll(values);
					values.clear();
					values.addAll(domainSet);
					Collections.sort(values);
				}
				
			} finally {
				reqRes.close();
			}
		} catch (Exception e) {
			LOG.error("Exception trying to fetch value from sourceTableXPath : " + sourceTableXPath + ", Column Name : " + sourceColumnName);
			e.printStackTrace();
		}

		return values;
	}

	public String getSourceTableXPath() {
		return sourceTableXPath;
	}

	public void setSourceTableXPath(String sourceTableXPath) {
		this.sourceTableXPath = sourceTableXPath;
	}

	public String getSourceColumnName() {
		return sourceColumnName;
	}

	public void setSourceColumnName(String sourceColumnName) {
		this.sourceColumnName = sourceColumnName;
	}
}
