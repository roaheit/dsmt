package com.citi.ebx.workflow;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValidationReportItem;
import com.orchestranetworks.service.ValidationReportItemIterator;
import com.orchestranetworks.service.ValidationReportItemSubjectForAdaptation;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class MailNotificationScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	private String attachment_name;
	private String dataSpace;
	private String dataSet;
	private String tableName;
	private String report_filename;
	private String importConfigID;

	
		
	public String getImportConfigID() {
		return importConfigID;
	}
	public void setImportConfigID(String importConfigID) {
		this.importConfigID = importConfigID;
	}
	public String getReport_filename() {
		return report_filename;
	}
	public void setReport_filename(String report_filename) {
		this.report_filename = report_filename;
	}
	String errored_record_list="";
	int line_count=0;
	private String allocatedUser;
	
	public String getDataSpace() {
		return dataSpace;
	}
	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}
	public String getDataSet() {
		return dataSet;
	}
	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getAttachment_name() {
		return attachment_name;
	}
	public void setAttachment_name(String attachment_name) {
		this.attachment_name = attachment_name;

	}
	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		BufferedReader reader;
		if(importConfigID.contains("CGM")){
			allocatedUser= DSMTConstants.propertyHelper.getProp(DSMTConstants.CGM_PROJECT_NOTIFICATION);
		}else {
			allocatedUser= DSMTConstants.propertyHelper.getProp(DSMTConstants.IMS_PROJECT_NOTIFICATION);
		}
		
		try {
			reader = new BufferedReader(new FileReader(attachment_name));
			while (reader.readLine() != null) line_count++;
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String current_date=dateFormat.format(date);
	if(line_count>1)
	{
		Repository repo=context.getRepository();
		Adaptation metadataDataSet;
		metadataDataSet = EBXUtils.getMetadataDataSet(repo,
				dataSpace, dataSet);
		Path tablePath=Path.parse(tableName);
		
		final AdaptationTable targetTable = metadataDataSet
				.getTable(tablePath);
		ValidationReportItemIterator record=targetTable.getValidationReport(false, false).getItemsOfSeverity(Severity.ERROR);
		ValidationReportItem errored_item;
		int record_count=0;
		StringBuffer errorMsg= new StringBuffer();
		while(record.hasNext())
		{
			record_count++;
			errored_item=record.nextItem();
			ValidationReportItemSubjectForAdaptation errorRecordSubject = errored_item.getSubjectForAdaptation() ;
			//LOG.info(errored_item.getSubjectForAdaptation().getAdaptation().getOccurrencePrimaryKey().format());
			//errored_record_list+=errored_item.getSubjectForAdaptation().getAdaptation().getOccurrencePrimaryKey().toString()+ "  "+errored_item.getMessage().formatMessage(context.getSession().getLocale()).toString()+"\n";
			errorMsg.append(errored_item.getSubjectForAdaptation().getAdaptation().getOccurrencePrimaryKey().format());
			errorMsg.append(",");
			errorMsg.append(errored_item.getSubjectForAdaptation().getPathInAdaptation().format());
			errorMsg.append(",");
			errorMsg.append( errorRecordSubject.getAdaptation().getString(Path.parse("./DESCR")));
			errorMsg.append(",");
			errorMsg.append( errorRecordSubject.getAdaptation().getString(Path.parse("./C_DSMT_DESCR_3000")));
			errorMsg.append(",   ");
			errorMsg.append(errored_item.getMessage().formatMessage(context.getSession().getLocale()).toString());
			errorMsg.append(DSMTConstants.NEXT_LINE);
			
		}
		LOG.info("Error Record Count::"+Integer.toString(record_count));
		String message = "Date:"+current_date+"\n\nThe workflow import process has validation error(s)."+"\n";
		message+="Total Number of errored records are : "+Integer.toString(record_count)+"\n\n";
		message+="Following errors were found in the file and the File containing errored records are found in file"+ report_filename +"\n";
		message+="\n\n"+errorMsg.toString();
		String Subject="Error Record List from Import Process";
		DSMTNotificationService.notifyEmailID(allocatedUser, Subject, message, false);
	}else{
		String Successmessage = "Date:"+current_date+"\n\nThe workflow import process is completed successfully"+"\n";
		String Subject="Project Data loaded successfully";
		DSMTNotificationService.notifyEmailID(allocatedUser, Subject, Successmessage, false);
	}
		
	}
}