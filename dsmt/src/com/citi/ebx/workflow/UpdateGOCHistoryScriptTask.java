/**
 * 
 */
package com.citi.ebx.workflow;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

/**
 * @author rk00242
 *
 */
public class UpdateGOCHistoryScriptTask  extends ScriptTaskBean  {
	
	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	protected String sql ;
	
	private String sqlFilePath;
	
	public String getSqlFilePath() {
		return sqlFilePath;
	}

	public void setSqlFilePath(String sqlFilePath) {
		this.sqlFilePath = sqlFilePath;
	}

	
	
	
	@Override
	public void executeScript(ScriptTaskBeanContext arg0)
			throws OperationException {
		final Connection dbConnection = SQLReportConnectionUtils.getConnection();
		PreparedStatement preparedStatement = null;
	
		
		// TODO Auto-generated method stub
		sql = getInputQuery(sqlFilePath);
		
		LOG.debug("executing query " +sql);
		
		try {
			
			preparedStatement = dbConnection.prepareStatement(sql);
			
			ResultSet rs = preparedStatement.executeQuery();
			
			LOG.info("Successfully executed query");
			
			rs.close();
			preparedStatement.close();
		}catch (SQLException e) {
			LOG.error(" Error  while excuting the query >>>>"
					+ ". Exception ->" + e + " , message "
					+ e.getMessage());
		}
		finally{
			
			if(null != dbConnection){
				try{
					dbConnection.close();
					
				}
				catch (SQLException SQ){
					LOG.error("Report generation terminated with Exception -> " + SQ + ", message = " + SQ.getMessage());
				}
			}
		}
		
		
		
	}
	
    /**
   	 * Helper method to read Sql from file
   	 * @param  sqlFileDirectory , sqlFile
   	 * @throws 
   	 */       
    
	public String getInputQuery(String sqlFile) {
		LOG.debug("getInputQuery  " + sqlFile);
		StringBuffer inputquery = new StringBuffer();
		BufferedReader br = null;
		String line = null;
		try {
			br = new BufferedReader(new FileReader(sqlFile));
			while ((line = br.readLine()) != null) {

				inputquery.append(line);
				inputquery.append(DSMTConstants.NEXT_LINE);

			}

		} catch (FileNotFoundException e) {

			LOG.error("Unable to find SQl file  >>>>" + sqlFile
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} catch (IOException e) {

			LOG.error("Unable to read SQl file " + sqlFile + ". Exception ->"
					+ e + " , message " + e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return inputquery.toString();
	}

}
