/**
 * 
 */
package com.citi.ebx.workflow;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.citi.ebx.util.UpdateManagerProcedure;
import com.citi.ebx.util.Utils;
import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;

/**
 * @author rk00242
 * 
 */
public class AcctMatrxDeleteHierScriptTask extends ScriptTaskBean {

	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	protected String sql;

	private String sqlFilePath;
	private String dataSpace;

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getSqlFilePath() {
		return sqlFilePath;
	}

	public void setSqlFilePath(String sqlFilePath) {
		this.sqlFilePath = sqlFilePath;
	}

	public static final Path NAMTable = Path
			.parse("/root/ACC_MAT/ACC_REG/ACC_MS_Hier_NAM");
	public static final Path LATAMTable = Path
			.parse("/root/ACC_MAT/ACC_REG/ACC_MS_Hier_LATAM");
	public static final Path EMEATable = Path
			.parse("/root/ACC_MAT/ACC_REG/ACC_MS_Hier_EMEA");
	public static final Path ASIATable = Path
			.parse("/root/ACC_MAT/ACC_REG/ACC_MS_Hier_ASIA");
	public static final Path NAMControllerTable = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM
			.getPathInSchema();
	public static final Path EMEAControllerTable = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_EMEA
			.getPathInSchema();
	public static final Path LATAMControllerTable = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_LATAM
			.getPathInSchema();
	public static final Path ASIAControllerTable = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_ASIA
			.getPathInSchema();

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		final Connection dbConnection = SQLReportConnectionUtils
				.getConnection();
		PreparedStatement preparedStatement = null;
		ArrayList<String> lem = new ArrayList<String>();
		Repository rep = context.getRepository();
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(rep,
				dataSpace,
				AccountibilityMatrixConstants.DATASET_Accounting_Matrix);
		AdaptationTable NamTable = metadataDataSet.getTable(NAMTable);
		AdaptationTable latamTable = metadataDataSet.getTable(LATAMTable);
		AdaptationTable emeaTable = metadataDataSet.getTable(EMEATable);
		AdaptationTable asiaTable = metadataDataSet.getTable(ASIATable);
		AdaptationTable NamControllerTable = metadataDataSet
				.getTable(NAMControllerTable);
		AdaptationTable latamControllerTable = metadataDataSet
				.getTable(LATAMControllerTable);
		AdaptationTable emeaControllerTable = metadataDataSet
				.getTable(EMEAControllerTable);
		AdaptationTable asiaControllerTable = metadataDataSet
				.getTable(ASIAControllerTable);
		AdaptationTable lvidLEMTargetTable = metadataDataSet
				.getTable(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping
						.getPathInSchema());
		final String predicateLVLEM = AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._Acc_Matrix_flag
				.format() + "= '" + "Y" + "'";
		RequestResult result = lvidLEMTargetTable
				.createRequestResult(predicateLVLEM);
		if (result.getSize() > 0) {
			for (Adaptation Recrd; (Recrd = result.nextAdaptation()) != null;) {
				lem.add(Recrd
						.getString(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM));
			}
		}
		ArrayList<String> msIDList = new ArrayList<String>();
		ArrayList<AdaptationTable> TableList = new ArrayList<AdaptationTable>();
		ArrayList<AdaptationTable> ControllerTableList = new ArrayList<AdaptationTable>();
		TableList.add(NamTable);
		TableList.add(asiaTable);
		TableList.add(latamTable);
		TableList.add(emeaTable);
		ControllerTableList.add(NamControllerTable);
		ControllerTableList.add(asiaControllerTable);
		ControllerTableList.add(latamControllerTable);
		ControllerTableList.add(emeaControllerTable);
		// TODO Auto-generated method stub
		sql = getInputQuery(sqlFilePath);

		try {

			preparedStatement = dbConnection.prepareStatement(sql);
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				for (AdaptationTable targetTable : TableList) {
					if(! msIDList.contains(rs.getString(1))){
					msIDList.add(rs.getString(1));
					}
					UpdateManagerProcedure updateManagerProc = new UpdateManagerProcedure(
							targetTable, rs.getString(1), true);
					Utils.executeProcedure(
							updateManagerProc,
							context.getSession(),
							context.getRepository().lookupHome(
									HomeKey.forBranchName(dataSpace)));
				}
			}
			rs.close();
			preparedStatement.close();
			for (String ms : msIDList) {
				for (AdaptationTable targetTable : ControllerTableList) {
					UpdateManagerProcedure updateManagerProc = new UpdateManagerProcedure(
							targetTable, ms, false);
					Utils.executeProcedure(
							updateManagerProc,
							context.getSession(),
							context.getRepository().lookupHome(
									HomeKey.forBranchName(dataSpace)));
				}
			}

			emailNotification(msIDList, lem);
		} catch (SQLException e) {
			/*
			 * LOG.error(" Error  while excuting the query >>>>" +
			 * ". Exception ->" + e + " , message " + e.getMessage());
			 */
			e.printStackTrace();
		} finally {

			if (null != dbConnection) {
				try {
					dbConnection.close();

				} catch (SQLException SQ) {
					LOG.error("Report generation terminated with Exception -> "
							+ SQ + ", message = " + SQ.getMessage());
				}
			}
		}

	}

	/**
	 * Helper method to read Sql from file
	 * 
	 * @param sqlFileDirectory
	 *            , sqlFile
	 * @throws
	 */

	public String getInputQuery(String sqlFile) {
		StringBuffer inputquery = new StringBuffer();
		BufferedReader br = null;
		String line = null;
		try {
			br = new BufferedReader(new FileReader(sqlFile));
			while ((line = br.readLine()) != null) {

				inputquery.append(line);
				inputquery.append(DSMTConstants.NEXT_LINE);

			}

		} catch (FileNotFoundException e) {

			LOG.error("Unable to find SQl file  >>>>" + sqlFile
					+ ". Exception ->" + e + " , message " + e.getMessage());

		} catch (IOException e) {

			LOG.error("Unable to read SQl file " + sqlFile + ". Exception ->"
					+ e + " , message " + e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return inputquery.toString();
	}

	public void emailNotification(ArrayList<String> msIDList,
			ArrayList<String> owner) {

		String msidvalues = "";
		for (String msid : msIDList) {
			msidvalues=msidvalues+msid;
			msidvalues+="\n";
		}
		String message = "Hello \n MSID" + msidvalues + " has been deleted"
				+ " from Accountibility Matrix"
				+ "\nAccordingly Managed Segment hierarchy has been updated";
		String Subject = "LEM Notification : Managed Segment deleted from Accountibility Matrix";
		String receipents = "";

		for (String receipent : owner) {

			receipents += receipent + ";";
		}
		DSMTNotificationService.notifyEmailID(receipents, Subject, message,
				false);
	}

}
