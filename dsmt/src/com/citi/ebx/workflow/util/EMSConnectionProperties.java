package com.citi.ebx.workflow.util;

import com.orchestranetworks.ps.util.DSMTPropertyHelper;

public class EMSConnectionProperties {

	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	
	public EMSConnectionProperties() {
	
		this.providerURL = propertyHelper.getProp("provider.url");
		this.initialContext = propertyHelper.getProp("initial.context");
		this.connectionFactory = propertyHelper.getProp("connection.factory");
		this.queueName = propertyHelper.getProp("ebx_bpm.queue.name");
		

	}
	
	private String providerURL;
	private String initialContext;
	private String connectionFactory;
	private String queueName;
	
	
	public String getProviderURL() {
		
		return providerURL;
	}
	public void setProviderURL(String providerURL) {
		this.providerURL = providerURL;
	}
	public String getInitialContext() {
		
		return initialContext;
	}
	public void setInitialContext(String initialContext) {
		this.initialContext = initialContext;
	}
	public String getConnectionFactory() {
		
		return connectionFactory;
	}
	public void setConnectionFactory(String connectionFactory) {
		this.connectionFactory = connectionFactory;
	}
	public String getQueueName() {
		return queueName;
	}
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	@Override
	public String toString() {
	
		return "{ providerURL = " + this.providerURL + ", connectionFactory = " + this.connectionFactory
				+ ", queueName =" + this.queueName + "}";
	}
}
