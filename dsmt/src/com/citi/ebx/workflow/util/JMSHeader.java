package com.citi.ebx.workflow.util;

public class JMSHeader {

	private String JMSDestination;
	private String JMSDeliveryMode;
	private String JMSMessageID;
	private String JMSTimestamp;
	private String JMSCorrelationID;
	private String JMSReplyTo;
	private String JMSRedelivered;
	private String JMSType;
	private String JMSExpiration;
	private String JMSPriority;
	
	public String getJMSDestination() {
		return JMSDestination;
	}
	public void setJMSDestination(String jMSDestination) {
		JMSDestination = jMSDestination;
	}
	public String getJMSDeliveryMode() {
		return JMSDeliveryMode;
	}
	public void setJMSDeliveryMode(String jMSDeliveryMode) {
		JMSDeliveryMode = jMSDeliveryMode;
	}
	public String getJMSMessageID() {
		return JMSMessageID;
	}
	public void setJMSMessageID(String jMSMessageID) {
		JMSMessageID = jMSMessageID;
	}
	public String getJMSTimestamp() {
		return JMSTimestamp;
	}
	public void setJMSTimestamp(String jMSTimestamp) {
		JMSTimestamp = jMSTimestamp;
	}
	public String getJMSCorrelationID() {
		return JMSCorrelationID;
	}
	public void setJMSCorrelationID(String jMSCorrelationID) {
		JMSCorrelationID = jMSCorrelationID;
	}
	public String getJMSReplyTo() {
		return JMSReplyTo;
	}
	public void setJMSReplyTo(String jMSReplyTo) {
		JMSReplyTo = jMSReplyTo;
	}
	public String getJMSRedelivered() {
		return JMSRedelivered;
	}
	public void setJMSRedelivered(String jMSRedelivered) {
		JMSRedelivered = jMSRedelivered;
	}
	public String getJMSType() {
		return JMSType;
	}
	public void setJMSType(String jMSType) {
		JMSType = jMSType;
	}
	public String getJMSExpiration() {
		return JMSExpiration;
	}
	public void setJMSExpiration(String jMSExpiration) {
		JMSExpiration = jMSExpiration;
	}
	public String getJMSPriority() {
		return JMSPriority;
	}
	public void setJMSPriority(String jMSPriority) {
		JMSPriority = jMSPriority;
	}
	
}
