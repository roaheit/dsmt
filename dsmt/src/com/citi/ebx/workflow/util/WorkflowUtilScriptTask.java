package com.citi.ebx.workflow.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.DataContextReadOnly;
import com.orchestranetworks.workflow.ProcessInstance;
import com.orchestranetworks.workflow.ProcessInstanceKey;
import com.orchestranetworks.workflow.PublishedProcess;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;
import com.orchestranetworks.workflow.WorkflowEngine;

public class WorkflowUtilScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	/** Publication Name of the workflow. */
	protected String workflowPublicationName ;
	
	
	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		LOG.info("WorkflowUtilScriptTask executeScript started");
		
		// get handler for workflow engine
		WorkflowEngine wfEngine = WorkflowEngine.getFromRepository(context.getRepository(), context.getSession());
		
		/*to find the Published process for the given workflowPublicationName*/
		if (null !=  workflowPublicationName) {
		
			PublishedProcess publishedProcess = wfEngine.getPublishedProcess(PublishedProcessKey.forName(workflowPublicationName));
			LOG.info("publishedProcess = " + publishedProcess);
			
			/*to find the published process key for the given published process*/
			PublishedProcessKey publishedProcessKey = publishedProcess.getPublishedProcessKey();
			LOG.info("publishedProcessKey = " + publishedProcessKey);
			
			/*to find the list of ProcessInstanceKeys for the published process key*/
			@SuppressWarnings("unchecked")
			List<ProcessInstanceKey> processInstanceKeyList = wfEngine.getProcessInstanceKeys(publishedProcessKey);
			LOG.info("processInstanceKeyList = " + processInstanceKeyList);
			
			processWorkflowInstance(wfEngine, processInstanceKeyList);
		}
		else{
			
			@SuppressWarnings("unchecked")
			List<PublishedProcessKey> publishedProcessKeys = (ArrayList<PublishedProcessKey>)wfEngine.getPublishedKeys(true);
			LOG.info("publishedProcessKeys = " + publishedProcessKeys);
			for (PublishedProcessKey publishedProcessKey : publishedProcessKeys) {
				LOG.info("publishedProcessKey = " + publishedProcessKey);
				@SuppressWarnings("unchecked")
				List<ProcessInstanceKey> processInstanceKeyList = wfEngine.getProcessInstanceKeys(publishedProcessKey);
				LOG.info("processInstanceKeyList = " + processInstanceKeyList);
				processWorkflowInstance(wfEngine, processInstanceKeyList);
			}
		}
		
		
	}

	private String GetStringToPrintWorkflowData(ProcessInstance processInstance) {
		
		return "[{Label =  " + processInstance.getLabel().formatMessage(Locale.US) + "}, {startDate = " + processInstance.getCreationDate() 
		+ "}, {creator = " + processInstance.getCreator()
		+ "}, {isInError = " + processInstance.isInError()
		+ "}, {description = " + processInstance.getDescription()
		+ "}, {processInstanceID = " + processInstance.getProcessInstanceKey().getId()
		+"}]";
	}
	
	
	private void processWorkflowInstance(WorkflowEngine wfEngine, List<ProcessInstanceKey> processInstanceKeyList){
		
		
		if(processInstanceKeyList != null && !processInstanceKeyList.isEmpty()){
			
			for(ProcessInstanceKey instanceKey : processInstanceKeyList){
				/*to find each instance of the process from the process instance keys*/
				/*this is where you get the information about a specific instance of the workflow*/
				ProcessInstance processInstance = wfEngine.getProcessInstance(instanceKey);
				
				//	LOG.info("[WorkflowMaintenance] Label of the workflow instance is: "+processInstance.getLabel().formatMessage(Locale.US));
				LOG.info(GetStringToPrintWorkflowData(processInstance));
				
				if (processInstance.isInError() && "1".equalsIgnoreCase(processInstance.getLabel().formatMessage(Locale.US))) {
					
					wfEngine.terminateProcessInstance(instanceKey);
					DataContextReadOnly dataContextReadOnly = processInstance.getDataContext();
					@SuppressWarnings("unchecked")
					Iterator<String> variableIterator = (Iterator<String>)dataContextReadOnly.getVariableNames();
					
					while (variableIterator.hasNext()) {
						String variableName = (String) variableIterator.next();
						String variableValue = dataContextReadOnly.getVariableString(variableName);
						LOG.info("[variableName = " + variableName + ", variableValue = " + variableValue + "]");
						
					}				
				}
				
					
				
			}
		}
	}

	public String getWorkflowPublicationName() {
		return workflowPublicationName;
	}

	public void setWorkflowPublicationName(String workflowPublicationName) {
		this.workflowPublicationName = workflowPublicationName;
	}

}
