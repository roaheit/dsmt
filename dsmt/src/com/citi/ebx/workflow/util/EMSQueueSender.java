package com.citi.ebx.workflow.util;

import java.util.Hashtable;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;

import com.orchestranetworks.service.LoggingCategory;

public class EMSQueueSender {

	private static final String JAVA_NAMING_REFERRAL = "java.naming.referral";
	private static final String JAVA_NAMING_PROVIDER_URL = "java.naming.provider.url";
	private static final String JAVA_NAMING_FACTORY_INITIAL = "java.naming.factory.initial";
	protected static final LoggingCategory wfLogger = LoggingCategory.getWorkflow();
	
	public void sendMessage(String messageContent, EMSConnectionProperties connectionProperties){
		
		Hashtable<String, String> hashtable = new Hashtable<String, String>();
	    hashtable.put(JAVA_NAMING_FACTORY_INITIAL, connectionProperties.getInitialContext());
	    hashtable.put(JAVA_NAMING_PROVIDER_URL, connectionProperties.getProviderURL());
	    hashtable.put(JAVA_NAMING_REFERRAL, "throw");

	    try {
			InitialDirContext initialdircontext = new InitialDirContext(hashtable);

			//wfLogger.info("Looking up...");
			QueueConnectionFactory queueConnFactory = null;
			queueConnFactory = (QueueConnectionFactory)initialdircontext.lookup(connectionProperties.getConnectionFactory());

			//wfLogger.info("Creating Queue Connection..." );
			QueueConnection queueconnection = ((QueueConnectionFactory)queueConnFactory).createQueueConnection();

			//wfLogger.info("Starting Connection...");
			queueconnection.start();

			//wfLogger.info("Creating Session...");
			QueueSession queuesession = queueconnection.createQueueSession(false, 1);

			
			wfLogger.info("connectionProperties = " + connectionProperties);
			javax.jms.Queue queue = (javax.jms.Queue)initialdircontext.lookup(connectionProperties.getQueueName());

			QueueSender queuesender = queuesession.createSender(queue);
			
			queuesender.setDeliveryMode(DeliveryMode.PERSISTENT);
			
			TextMessage textmessage = queuesession.createTextMessage();
			textmessage.setText(messageContent);
			wfLogger.info("Sending message -> " + textmessage);
			queuesender.send(textmessage);

			//wfLogger.info("Sent");
			queuesender.close();
			//wfLogger.info("Closing queue connection...");

			queuesession.close();
			//wfLogger.info("Closing queue session...");

			queuesession = null;
			//wfLogger.info("Closing connection...");
			queueconnection.close();
			//wfLogger.info("Connection closed successfully...");
			queueconnection = null;
		} catch (NamingException e) {
			
			e.printStackTrace();
		} catch (JMSException e) {
	
			e.printStackTrace();
		}
	}
	
	
	
}
