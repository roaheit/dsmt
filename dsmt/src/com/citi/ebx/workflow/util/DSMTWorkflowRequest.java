package com.citi.ebx.workflow.util;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.citi.ebx.workflow.vo.DSMTWorkflowRequestVO;

public class DSMTWorkflowRequest {
	
	public static void main(String[] args) {
		
		DSMTWorkflowRequest gocRespose = new DSMTWorkflowRequest();
		gocRespose.generateWorkflowRequest();
		
	}
	
	protected String generateWorkflowRequest()
	{
		String requestXml= "";
		
		
		try{
			
			XMLGregorianCalendar callender = DatatypeFactory.newInstance().newXMLGregorianCalendar("2013-12-10T11:58:48.74");
			// set the Value for parent Tag
			DSMTWorkflowRequestVO dsmtWorkflowRequest = new DSMTWorkflowRequestVO();
			dsmtWorkflowRequest.setWorkflowType("GOC");
			dsmtWorkflowRequest.setRequestID("155529EUC79");
			dsmtWorkflowRequest.setUserID("rk72899");
			dsmtWorkflowRequest.setDataSpace("GOC_1");
			dsmtWorkflowRequest.setDataSet("GOC");
			dsmtWorkflowRequest.setComparisonURI("![CDATA[/ebx/?configuration=ebx-manager&version=2013-08-07T14%3A03%3A18.971&instance=NEW_DSMT2&xpath=%2Froot%2FGLOBAL_STD%2FGFTDS_STD%2FC_DSMT_ACTCLASS&service=%40compare&compare.branch=2013-08-07T14%3A03%3A18.971&compare.instance=NEW_DSMT2&compare.xpath=%2Froot%2FGLOBAL_STD%2FGFTDS_STD%2FC_DSMT_ACTCLASS&ebx-interaction-enableReject=false ]]");
			dsmtWorkflowRequest.setUserComment("Sample Comment");
			dsmtWorkflowRequest.setTableURI("![CDATA[/ebx/?configuration=ebx-manager&branch=2013-07-29T16%3A35%3A47.821&instance=NEW_DSMT2&xpath=%2Froot%2FGLOBAL_STD%2FGFTDS_STD%2FC_DSMT_CITINAIC&ebx-interaction-enableReject=false]]");
			dsmtWorkflowRequest.setEbxRecordPath("/root/GLOBAL/GOC");

			// set the value for Goc Tags
			
			DSMTWorkflowRequestVO.GocRequest gocRequest = new DSMTWorkflowRequestVO.GocRequest();
			gocRequest.setSid("0000");
			gocRequest.setGridRoutingID("2013-12-10T11:58:48.749");
			gocRequest.setIsWarning(true);
			gocRequest.setIsHeadCountGOC(true);
			gocRequest.setCountryController("DSMT_AU_CTY_CONTROLLER_BU10500_ICG");
			gocRequest.setRegionalDQTeam("");
			gocRequest.setCountryBusinessHR("");
			gocRequest.setFinanceRole1("DSMT_ICG_CTS_MSEG_APPROVER");
			gocRequest.setFinanceRole2("DSMT_ICG_CTS_MSEG_APPROVER_2");
			gocRequest.setFinanceRole3("");
			gocRequest.setFinanceRole4("");
			gocRequest.setFinanceRole5("");
			gocRequest.setHRRole1("");
			gocRequest.setHRRole2("");
			gocRequest.setHRRole3("");
			gocRequest.setHRRole4("");
			gocRequest.setGlMaintenance("DSMT_AU_GLMAINTENANCE_FLEX");
			gocRequest.setREMaintenance("DSMT_AU_REMAINTENANCE_SMART");
			gocRequest.setP2PMaintenance("DSMT_AU_P2PMAINTENANCE");
			gocRequest.setMaintenanceOrder("P2P_DSMT2_GL_RE");
			gocRequest.setBusinessDueDate(callender);
			gocRequest.setAttestationAttachedForDeactivation(true);
			gocRequest.setGlTableURI("![CDATA[/ebx]]");
			gocRequest.setReportingEngineTableURI("![CDATA[/ebx]]");
			gocRequest.setP2PTableURI("![CDATA[/ebx]]");
			
		
			dsmtWorkflowRequest.setGocRequest(gocRequest);
			JAXBContext jaxbContext = JAXBContext.newInstance(DSMTWorkflowRequestVO.class);
			
			Marshaller jaxbmMarshaller = jaxbContext.createMarshaller();
			jaxbmMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			StringWriter sw = new StringWriter();
			jaxbmMarshaller.marshal(dsmtWorkflowRequest, sw);
			StringBuffer buffer= sw.getBuffer();
			requestXml=buffer.toString();
			//System.out.println("buffer >> "+requestXml);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		
		
		return requestXml ;
	}
}
