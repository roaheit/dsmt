package com.citi.ebx.workflow;

import com.citi.ebx.dsmt.jms.AccountingJMSComparisonMessageHelper;
import com.citi.ebx.dsmt.jms.AccountingJMSComparisonMessageHelper.ComparisonLevel;
import com.citi.ebx.dsmt.jms.JMSMessageHelper;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

public class PutAccountingBPMScriptTask extends JMSMessageScriptTask  {
	private String bpmProcessID;
	

	private String requestID;
	private String dataSpace;
	private String dataSet;
	private String tableXPath;
	private String user;
	private String userComment;
	private String tableID;
	private String tableName;
	private String workflowStatus;
	private String terminatedLEM;
	private String lvid;
	private String approvalGroup;
	
	
	

	@Override
	protected JMSMessageHelper createJMSMessageHelper(
			Repository repository, Session session)
			throws OperationException {
		final AccountingJMSComparisonMessageHelper helper = new AccountingJMSComparisonMessageHelper();
		helper.setRepository(repository);
		helper.setSession(session);
		helper.setLog(LoggingCategory.getWorkflow());
		helper.setComparisonLevel(ComparisonLevel.TABLE);
		helper.setWorkflowType(DSMTConstants.ACCOUNTING_MTX_WORKFLOW_TYPE);
		helper.setQueueName(queueName);
		helper.setBpmProcessID(bpmProcessID);
		helper.setRequestID(requestID);
		helper.setDataSpace(dataSpace);
		helper.setDataSet(dataSet);
		helper.setTableXPath(tableXPath);
		helper.setUser(user);
		helper.setUserComment(userComment);
		helper.setTableID(tableID);
		helper.setTableName(tableName);
		helper.setTerminatedLEM(user);
		helper.setLvid(lvid);
		helper.setApprovalGroup(approvalGroup);
		
		helper.setWorkflowStatus(workflowStatus);
		return helper;
	}
	
	public String getBpmProcessID() {
		return bpmProcessID;
	}

	public void setBpmProcessID(String bpmProcessID) {
		this.bpmProcessID = bpmProcessID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getRequestID() {
		return requestID;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public void setTableXPath(String tableXPath) {
		this.tableXPath = tableXPath;
	}

	public String getTableXPath() {
		return tableXPath;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getTableID() {
		return tableID;
	}

	public void setTableID(String tableID) {
		this.tableID = tableID;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}

	public String getTerminatedLEM() {
		return terminatedLEM;
	}

	public void setTerminatedLEM(String terminatedLEM) {
		this.terminatedLEM = terminatedLEM;
	}

	public String getLvid() {
		return lvid;
	}

	public void setLvid(String lvid) {
		this.lvid = lvid;
	}

	public String getApprovalGroup() {
		return approvalGroup;
	}

	public void setApprovalGroup(String approvalGroup) {
		this.approvalGroup = approvalGroup;
	}

	
	

	
}
