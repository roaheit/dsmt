package com.citi.ebx.workflow;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.citi.ebx.dsmt.jms.JMSComparisonMessageHelper.ComparisonLevel;
import com.citi.ebx.dsmt.jms.JMSGOCComparisonMessageHelper;
import com.citi.ebx.goc.CopyIntoSiblingDataSpaces;
import com.citi.ebx.goc.path.GOCPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class CreateSiblingDataSpacesAndBPMWorkflowsScriptTask extends
		ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	private static final Path DECIDER_FIELD = GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup;
	
	private static final Set<Path> TABLE_PATHS_TO_COMPARE = new HashSet<Path>();
	static {
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_ASIA.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_JAPAN.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_WEUROPE.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_CEECA.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_LATAM.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_RE_C_PTNR_SMART_NAM.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_DBS.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_Flexcube.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_FMS.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_TGL.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_CMN01.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_CCC01.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_DAU.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_CRCBS.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_NAM_CMI.getPathInSchema());
		/*TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_PS_LATAM.getPathInSchema());*/
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_P2P_C_PTNR_P2P_AESM.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_P2P_C_PTNR_P2P_NAM.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_ALFA_PMBB.getPathInSchema());
		TABLE_PATHS_TO_COMPARE.add(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_F_PTNR_GL_C_PTNR_ALFA_PMBNB.getPathInSchema());
	}
	
	protected String queueName;
	private String bpmProcessID;
	private String requestID;
	private String dataSpace;
	private String dataSet;
	private String user;
	private String userComment;
	private String workflowStatus;
	private String dataSpaceToCopyPermissionsFrom;
	private String sid;

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getBpmProcessID() {
		return bpmProcessID;
	}

	public void setBpmProcessID(String bpmProcessID) {
		this.bpmProcessID = bpmProcessID;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public String getDataSpaceToCopyPermissionsFrom() {
		return dataSpaceToCopyPermissionsFrom;
	}

	public void setDataSpaceToCopyPermissionsFrom(
			String dataSpaceToCopyPermissionsFrom) {
		this.dataSpaceToCopyPermissionsFrom = dataSpaceToCopyPermissionsFrom;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		final Repository repo = context.getRepository();
		final Session session = context.getSession();
		final AdaptationHome dataSpaceRef = repo.lookupHome(HomeKey.forBranchName(dataSpace));
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
		
		final HashSet<AdaptationTable> tables = new HashSet<AdaptationTable>();
		for (Path tablePath: TABLE_PATHS_TO_COMPARE) {
			tables.add(dataSetRef.getTable(tablePath));
		}
		
		final AdaptationHome dataSpaceRefToCopyPermissionsFrom =
				(dataSpaceToCopyPermissionsFrom == null) ?
				null : repo.lookupHome(HomeKey.forBranchName(dataSpaceToCopyPermissionsFrom));
		final Map<String,AdaptationHome> createdDataSpaceMap;
		
		// TODO: wrap this call below into a try/catch block for OperationException
		// and do something with it rather than have script task fail silently.
		// (i.e. send error msg via JMS, email user, etc)
		
		// Create the sibling data spaces and copy the data into them
		createdDataSpaceMap = createSiblingDataSpaces(tables, dataSpaceRefToCopyPermissionsFrom, session);
		
		// Send a JMS message for each data space
		sendJMSMessages(repo, session, createdDataSpaceMap);
	}
	
	private static Map<String,AdaptationHome> createSiblingDataSpaces(final Set<AdaptationTable> tables,
			final AdaptationHome dataSpaceRefToCopyPermissionsFrom, final Session session)
			throws OperationException {
		final CopyIntoSiblingDataSpaces copyIntoSiblings = new CopyIntoSiblingDataSpaces();
		copyIntoSiblings.setDataSpaceNamePrefix("goc_");
		// This is the field that determines which group it goes into.
		// If more than one table, each must have the same field.
		copyIntoSiblings.setDeciderField(DECIDER_FIELD);
		copyIntoSiblings.setTables(tables);
		
		LOG.info("data space permissions will be copied from " + dataSpaceRefToCopyPermissionsFrom);
		
		copyIntoSiblings.setDataSpaceToCopyPermissionsFrom(dataSpaceRefToCopyPermissionsFrom);
		LOG.debug("CreateSiblingDataSpacesAndBPMWorkflowsScriptTask: executing CopyIntoSiblingDataSpaces.");
		return copyIntoSiblings.execute(session);
	}
	
	private void sendJMSMessages(final Repository repository, final Session session,
			final Map<String,AdaptationHome> createdDataSpaceMap)
			throws OperationException {
		final JMSGOCComparisonMessageHelper helper = new JMSGOCComparisonMessageHelper();
		helper.setRepository(repository);
		helper.setSession(session);
		helper.setLog(LoggingCategory.getWorkflow());
		helper.setComparisonLevel(ComparisonLevel.DATASET);
		
		helper.setQueueName(queueName);
		helper.setRequestID(requestID);
		helper.setDataSet(dataSet);
		helper.setUser(user);
		helper.setUserComment(userComment);
		helper.setWorkflowStatus(workflowStatus);
		
		for (String approverGroup: createdDataSpaceMap.keySet()) {
			final AdaptationHome createdDataSpace = createdDataSpaceMap.get(approverGroup);
			helper.setDataSpace(createdDataSpace.getKey().getName());
			
			// TODO: set isWarning, isHeadCountGOD
			
			String msg = helper.createMessageContent();
			LOG.debug("CreateSiblingDataSpacesAndBPMWorkflowsScriptTask: Sending message = " + msg);
			helper.sendMessage(msg, queueName);
		}
	}
}
