package com.citi.ebx.workflow;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.DSMT2HierRelPaths;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.adaptation.RequestSortCriteria;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class MergeGOCDataSpaceScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private String dataSpace;
	private String dataSet;
	private boolean writeErrorToContext = true;
	private String errorMessage;
	private String errorDetails;
	
	private String requestID;
	private String gridRoutingID;
	private String approverList;
	
	private Adaptation dataSetRef;
	private List<String> gocList = new ArrayList<String>();
	
	private Date effectiveDate;
	
	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public boolean isWriteErrorToContext() {
		return writeErrorToContext;
	}

	public void setWriteErrorToContext(boolean writeErrorToContext) {
		this.writeErrorToContext = writeErrorToContext;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getGridRoutingID() {
		return gridRoutingID;
	}

	public void setGridRoutingID(String gridRoutingID) {
		this.gridRoutingID = gridRoutingID;
	}

	public String getApproverList() {
		return approverList;
	}

	public void setApproverList(String approverList) {
		this.approverList = approverList;
	}

	private static RequestSortCriteria sortByEffDate = new RequestSortCriteria();
	
	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		sortByEffDate = new RequestSortCriteria();
		sortByEffDate.add(DSMTConstants.effDateFieldPath, false);
		
		LOG.info("MergeGOCDataSpaceScriptTask executeScript started for dataSpace" + dataSpace);
		LOG.info("Received approval response for dataSpace " + dataSpace + ",  workflow Request ID - " +requestID + ", approversSOEID " + approverList);
		
		try {
			final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
					HomeKey.forBranchName(dataSpace));
			if (dataSpaceRef == null) {
				throw OperationException.createError("Data space " + dataSpace + " can't be found.");
			}
			dataSetRef = dataSpaceRef.findAdaptationOrNull(
					AdaptationName.forName(dataSet));
			if (dataSetRef == null) {
				throw OperationException.createError("Data set " + dataSet + " can't be found in data space " + dataSpace + ".");
			}
			
			final AdaptationTable gocTable = dataSetRef.getTable(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC.getPathInSchema());
			
			// We want to compare against the initial version of the data in the data space.
			// Get the same table from the initial snapshot's data set
			final AdaptationTable initialTable = GOCWorkflowUtils.getTableInInitialSnapshot(gocTable);
			
			LOG.info("MergeGOCDataSpaceScriptTask.compareAdaptationTables started at " + Calendar.getInstance().getTime());
			// Compare the table with the same table in the initial snapshot's data set
			final DifferenceBetweenTables diff = DifferenceHelper.compareAdaptationTables(initialTable, gocTable, false);
			LOG.info("MergeGOCDataSpaceScriptTask.compareAdaptationTables finished at " + Calendar.getInstance().getTime());
			
			// All extra occurrences on the right will be all records that exist in data space
			// that are not in initial snapshot, i.e. those added.
			@SuppressWarnings("unchecked")
			final List<ExtraOccurrenceOnRight> adds = diff.getExtraOccurrencesOnRight();
			@SuppressWarnings("unchecked")
			final List<DifferenceBetweenOccurrences> deltas = diff.getDeltaOccurrences();
			
			final UpdateRecordsProcedure updateProc = new UpdateRecordsProcedure(adds, deltas);
			Utils.executeProcedure(updateProc, context.getSession(), dataSpaceRef);
			
			final CopyRecordsToHistoricalTableProcedure copyProc = new CopyRecordsToHistoricalTableProcedure(adds, deltas);
			final AdaptationHome dsmt2HierDataSpace = context.getRepository().lookupHome(
					HomeKey.forBranchName(GOCConstants.DSMT2_RELATIONAL_DATASET));
			if (dsmt2HierDataSpace == null) {
				throw OperationException.createError("Data space " + GOCConstants.DSMT2_RELATIONAL_DATASET + " can't be found.");
			}
			
			Utils.executeProcedure(copyProc, context.getSession(), dsmt2HierDataSpace);


		} catch (OperationException ex) {
			LOG.error("PrepareGOCDataSpaceForMergeScriptTask: Error while preparing data space for merge.", ex);
			if (writeErrorToContext) {
				errorMessage = ex.getMessage();
				errorDetails = ExceptionUtils.getStackTrace(ex);
			} else {
				throw ex;
			}
		}
		
		notifyWorkflowCompletion();
		
		LOG.info("MergeGOCDataSpaceScriptTask executeScript finished");
	}
	
	/**
	 * Notify Workflow Completion to Workflow maintenance teams.
	 */
	private void notifyWorkflowCompletion() {
	
		
		String notificationDL = DSMTConstants.propertyHelper.getProp(DSMTConstants.WORKFLOW_ADMIN_DISTRIBUTION_LIST);
		String subject = null;
		String message = null;
		if(DSMTConstants.DIRECTUPLOAD.equalsIgnoreCase(requestID)){
			subject = "DSMT2 GOC maintenance completed";
			message = "GOC Direct maintenance completed successfully for {dataSpace " + dataSpace  + ", approvers = " + approverList + ", gocList = " + gocList + "]";;
		}else{
			
			subject = "DSMT2 GOC Workflow completed - Request ID : " +  requestID;
			message = "GOC workflow Successfully completed. [requestID = " + requestID + ", dataSpace = " + dataSpace + ", approvers = " + approverList + ", gocList = " + gocList + "]";
		}
			DSMTNotificationService.notifyEmailID(notificationDL, subject, message , false);
	}

	private class UpdateRecordsProcedure implements Procedure {
		private List<ExtraOccurrenceOnRight> adds;
		private List<DifferenceBetweenOccurrences> deltas;
		
		public UpdateRecordsProcedure(final List<ExtraOccurrenceOnRight> adds,
				final List<DifferenceBetweenOccurrences> deltas) {
			this.adds = adds;
			this.deltas = deltas;
		}
		
		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			
			LOG.info("MergeGOCDataSpaceScriptTask - new rows creation in GOC Workflow child started at " + Calendar.getInstance().getTime() );
			
			for (ExtraOccurrenceOnRight add: adds) {
				final Adaptation record = add.getExtraOccurrence();
				updateRecord(record, pContext);
			}
			LOG.info("MergeGOCDataSpaceScriptTask - Updation in GOC Workflow child started at " + Calendar.getInstance().getTime() );
			for (DifferenceBetweenOccurrences delta: deltas) {
				final Adaptation record = delta.getOccurrenceOnRight();
				updateRecord(record, pContext);
			}
			
			GOCWorkflowUtils.setWorkflowParameters(new HashMap<String, String>(), dataSetRef, pContext);
			
			pContext.setTriggerActivation(true);
			pContext.setAllPrivileges(false);
		}
		
		private void updateRecord(final Adaptation record, final ProcedureContext pContext)
				throws OperationException {
			final ValueContextForUpdate vc = pContext.getContext(record.getAdaptationName());
			
			if(null == effectiveDate){
				effectiveDate =  GOCWorkflowUtils.calculateGOCEffectiveDate(dataSetRef,pContext);
			}
			
			
			String actionReqCode = String.valueOf(vc.getValue(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD));
			Date now = Calendar.getInstance().getTime();
			vc.setValue(actionReqCode , GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LAST_ACTION_REQUIRED);
			LOG.debug("LAST_ACTION_REQUIRED-->"+vc.getValue(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LAST_ACTION_REQUIRED).toString());
			
			
			final Adaptation sidRecord = Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
			
			if (sidRecord != null && GOCConstants.ACTION_CREATE_GOC_LEG_CCS.equalsIgnoreCase(actionReqCode)) {
				
				// set goc as sid + local cost code for all new creates.
				String goc =  sidRecord.getString(DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID) 
							+  vc.getValue(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD);
			
				vc.setValue(goc, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC);
				LOG.debug("GOC Updated to " + goc);
				if(! DSMTConstants.DIRECTUPLOAD.equalsIgnoreCase(requestID)){
					vc.setValue(now, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ORIG_DTTM);
					vc.setValue(now, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CHNG_DTTM);
					if(null!=approverList){
					vc.setValue(approverList.substring(approverList.lastIndexOf(",")+1, approverList.lastIndexOf(")")), GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CREATED_OPERID);
					vc.setValue(approverList.substring(approverList.lastIndexOf(",")+1, approverList.lastIndexOf(")")), GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LASTUPDOPRID);
					}
					}else{
						vc.setValue(now, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ORIG_DTTM);
						vc.setValue(vc.getValue(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LASTUPDOPRID), GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CREATED_OPERID);
						vc.setValue(now, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CHNG_DTTM);
						vc.setValue(vc.getValue(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LASTUPDOPRID), GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LASTUPDOPRID);
					}
				
			}else{
				if(! DSMTConstants.DIRECTUPLOAD.equalsIgnoreCase(requestID)){
				vc.setValue(now, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CHNG_DTTM);
					if(null!=approverList){
					vc.setValue(approverList.substring(approverList.lastIndexOf(",")+1, approverList.lastIndexOf(")")), GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LASTUPDOPRID);
					}
				}else{
					vc.setValue(now, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CHNG_DTTM);
					vc.setValue(vc.getValue(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LASTUPDOPRID), GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LASTUPDOPRID);
				}
				
			}
			

			if(! DSMTConstants.DIRECTUPLOAD.equalsIgnoreCase(requestID)){
				vc.setValue(effectiveDate, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFFDT);
				LOG.info("setting effective date " + DSMTUtils.formatDate(effectiveDate, DSMTConstants.DEFAULT_DATE_FORMAT) + " ,resetting Action required code changes for GOC " + vc.getValue(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC));
				
			}


			
			vc.setValue(now, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._MERGE_DATE);// REQUIRED TO track when GOC was merged after workflow approval
		
			
			vc.setValue(GOCConstants.ACTION_NO_CHANGE, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD);
			pContext.doModifyContent(record, vc);
		}
	}
	
	
	private class CopyRecordsToHistoricalTableProcedure implements Procedure {
		private List<ExtraOccurrenceOnRight> adds;
		private List<DifferenceBetweenOccurrences> deltas;
		
		public CopyRecordsToHistoricalTableProcedure(final List<ExtraOccurrenceOnRight> adds,
				final List<DifferenceBetweenOccurrences> deltas) {
			this.adds = adds;
			this.deltas = deltas;
		}
		//GOCConstants.DSMT2_RELATIONAL_DATASET
		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			final AdaptationHome targetDataSpace = pContext.getAdaptationHome();
			final Adaptation targetDataSet = targetDataSpace.findAdaptationOrNull(
					AdaptationName.forName(GOCConstants.DSMT2_RELATIONAL_DATASET));
			final AdaptationTable targetTable = targetDataSet.getTable(
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY.getPathInSchema());
			
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			
			LOG.info("MergeGOCDataSpaceScriptTask - new rows creation in History started at " + Calendar.getInstance().getTime() );
			
			for (ExtraOccurrenceOnRight add: adds) {
				final Adaptation record = add.getExtraOccurrence();
				copyRecord(record, pContext, targetTable);
			}
			LOG.info("MergeGOCDataSpaceScriptTask -  modification to existing rows in History started at " + Calendar.getInstance().getTime() );
			for (DifferenceBetweenOccurrences delta: deltas) {
				final Adaptation record = delta.getOccurrenceOnRight();
				copyRecord(record, pContext, targetTable);
			}
			
			LOG.info("MergeGOCDataSpaceScriptTask -  modification to existing rows in History finished at " + Calendar.getInstance().getTime() );
			pContext.setTriggerActivation(true);
			pContext.setAllPrivileges(false);
		}
		
		
		
		
		private void copyRecord(final Adaptation record, final ProcedureContext pContext,
				final AdaptationTable targetTable) throws OperationException {
			final Object setId = record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._SETID);
			final Object goc = record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC);
			final Object effDate = record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFFDT);
			final PrimaryKey pk = targetTable.computePrimaryKey(new Object[] {setId, goc, effDate});
			
			long timeStartToLookupRecord = Calendar.getInstance().getTimeInMillis();
		
			Adaptation targetRecord = targetTable.lookupAdaptationByPrimaryKey(pk);	
									// getHistoryRecord(setId, goc, effDate, targetTable); // 
			
			long timeEndtoLookupRecord = Calendar.getInstance().getTimeInMillis();
			
			long timeTaken = timeEndtoLookupRecord - timeStartToLookupRecord; 
			if(timeTaken > 10000){ // log only when lookupAdaptationByPrimaryKey takes more than 10 seconds.
				
				LOG.info("MergeGOCDataSpaceScriptTask.copyRecord.lookupAdaptationByPrimaryKey " + (timeTaken/1000.00) + " seconds for " + pk + " in dataSpace " + dataSpace);
			}
			
			final ValueContextForUpdate vc;
			if (targetRecord == null) {
				vc = pContext.getContextForNewOccurrence(targetTable);
				vc.setValue(setId, DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._SETID);
				vc.setValue(goc, DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_GOC_ID);
				vc.setValue(effDate, DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._EFFDT);
			} else {
				vc = pContext.getContext(targetRecord.getAdaptationName());
			}
			
			/**
			 * start - add GOCs in the GOCList to be sent in an email after process completion
			 */
			gocList.add(String.valueOf(goc));
			/**
			 * end
			 */
			
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFF_STATUS),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._EFF_STATUS);
			
		
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._MANAGER_ID),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._MANAGER_ID);
		
			
			
			vc.setValue(Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID).getString(getPath("/HISTORY_FK")),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_SID_FK);
			
			vc.setValue(Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU).getString(getPath("/HISTORY_FK")),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_FRS_BU_FK);
			
			vc.setValue(Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_OU).getString(getPath("/HISTORY_FK")),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_FRS_OU_FK);
			
			vc.setValue(Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID).getString(getPath("/HISTORY_FK")),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_MAN_GEO_FK);
			
			vc.setValue(Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID).getString(getPath("/HISTORY_FK")),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_MAN_SEG_FK);
			
			vc.setValue(Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FUNCTION).getString(getPath("/HISTORY_FK")),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_FUNCTION_FK);
			
			vc.setValue(Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LVID).getString(getPath("/HISTORY_FK")),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_LVID_FK);
			
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._HEADCOUNT_GOC),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._HEADCOUNT_GOC);
			
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCRSHORT),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._DESCRSHORT);
			
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCR),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._DESCR);
			
			vc.setValue(Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_USAGE).getString(getPath("/HISTORY_FK")),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_GOC_USAGE_FK);
			
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_LOC_COST_CD);
			
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_COMMENT),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_DSMT_GOC_COMMENT);

			
			
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CREATED_OPERID),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._CREATED_OPERID);
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ORIG_DTTM),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._ORIG_DTTM);
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LASTUPDOPRID),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._LASTUPDOPRID);
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CHNG_DTTM),
					DSMT2HierRelPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._CHNG_DTTM);
			
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Functional_ID),
					Path.parse("./Functional_ID"));
			
			
			vc.setValue(Calendar.getInstance().getTime(), DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._MERGE_DATE);
			
			
			vc.setValue(requestID, DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._WF_REQUEST_ID);
			vc.setValue(gridRoutingID, DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._DATASPACE_ID);
			vc.setValue(approverList, DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._ApproverSOEID);
			
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Requested_Date),
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Requested_Date);
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Requested_by),
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Requested_by);
			
			if (targetRecord == null) {
				 targetRecord = pContext.doCreateOccurrence(vc,  targetTable);
			} else {
				targetRecord = pContext.doModifyContent(targetRecord,  vc);
			}
		}
	}
	
	protected Adaptation getHistoryRecord(final Object setId, final Object goc, final Object effDate, final AdaptationTable targetTable){
		
		Adaptation targetRecord = null;
		final String predicate = " ( " + DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._SETID.format()
		+ "=\"" + String.valueOf(setId) + "\" and "
		+ DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_GOC_ID.format()
		+ "=\"" + String.valueOf(goc)+ "\" and date-equal( "
		+ DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._EFFDT.format() 
		+ ",'" + DSMTUtils.formatDate((Date)effDate, DSMTConstants.DEFAULT_DATE_FORMAT ) + "') )";
		
		LOG.debug("predicate = " + predicate);
		RequestResult reqRes = targetTable.createRequestResult(predicate);
		if (null != reqRes) {
			
			targetRecord = reqRes.nextAdaptation();
			reqRes.close();
			
		}
	
		return targetRecord;
	}
	

	
	protected Adaptation getHistoryRecordByGOC( final Object goc, final AdaptationTable targetTable){
		
		
		Adaptation targetRecord = null;
		final String predicate = " ( " +  DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_HISTORY._C_GOC_ID.format()
		+ "=\"" + String.valueOf(goc)+ " )";
		
		LOG.debug("predicate = " + predicate);
		RequestResult reqRes = targetTable.createRequestResult(predicate, sortByEffDate);
		
		if (null != reqRes) {
			
			targetRecord = reqRes.nextAdaptation();
			reqRes.close();
			
		}
	
		return targetRecord;
	}
private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}


		
}
