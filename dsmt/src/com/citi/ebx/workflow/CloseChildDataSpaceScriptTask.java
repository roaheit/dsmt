package com.citi.ebx.workflow;

import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class CloseChildDataSpaceScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private String dataSpace;
	private String dataSet;
	private boolean writeErrorToContext = true;
	private String errorMessage;
	private String errorDetails;
	
	
	
	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		LOG.info("CloseChildDataSpaceScriptTask executeScript started for " + dataSpace);
		
		
		try {
			
			final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
					HomeKey.forBranchName(dataSpace));
			if (dataSpaceRef == null) {
				throw OperationException.createError("Data space " + dataSpace + " can't be found.");
			}
			dataSetRef = dataSpaceRef.findAdaptationOrNull(
					AdaptationName.forName(dataSet));
			if (dataSetRef == null) {
				throw OperationException.createError("Data set " + dataSet + " can't be found in data space " + dataSpace + ".");
			}
			
			LOG.info("dataSpaceRef " + dataSpaceRef);
			LOG.info("Child Data Space List " + dataSpaceRef.findAllChildren(null));
			
			List<AdaptationHome> childDataSpaceList = dataSpaceRef.getBranchChildren();
			LOG.info("Child Data Space List " + childDataSpaceList);
			
			
		} catch (OperationException ex) {
			LOG.error("PrepareGOCDataSpaceForMergeScriptTask: Error while preparing data space for merge.", ex);
			if (writeErrorToContext) {
				errorMessage = ex.getMessage();
				errorDetails = ExceptionUtils.getStackTrace(ex);
			} else {
				throw ex;
			}
		}
		
		LOG.info("CloseChildDataSpaceScriptTask ended for dataSpace " + dataSpace);
		
	}
	
	
	
private Adaptation dataSetRef;
	
	
	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public boolean isWriteErrorToContext() {
		return writeErrorToContext;
	}

	public void setWriteErrorToContext(boolean writeErrorToContext) {
		this.writeErrorToContext = writeErrorToContext;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

}
