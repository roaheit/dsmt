package com.citi.ebx.workflow;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestSortCriteria;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class MergeTableChangesScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private String dataSpace;
	private String dataSet;
	
	
	private String requestID;
	private String approvalID;
	private String tableID;
	
	private Adaptation dataSetRef;
	
	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	
	/**
	 * @return the approvalID
	 */
	public String getApprovalID() {
		return approvalID;
	}

	/**
	 * @param approvalID the approvalID to set
	 */
	public void setApprovalID(String approvalID) {
		this.approvalID = approvalID;
	}

	/**
	 * @return the tableID
	 */
	public String getTableID() {
		return tableID;
	}

	/**
	 * @param tableID the tableID to set
	 */
	public void setTableID(String tableID) {
		this.tableID = tableID;
	}


	/**
	 * @return the requestID
	 */
	public String getRequestID() {
		return requestID;
	}

	/**
	 * @param requestID the requestID to set
	 */
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}


	private static RequestSortCriteria sortByEffDate = new RequestSortCriteria();
	
	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		sortByEffDate = new RequestSortCriteria();
		sortByEffDate.add(DSMTConstants.effDateFieldPath, false);
		
		LOG.info("MergeTableChangesScriptTask executeScript started for dataSpace" + dataSpace);
		LOG.info("Received approval response for dataSpace " + dataSpace + ",  workflow Request ID - " +requestID + ", approversSOEID " + approvalID);
		
		try {
			final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
					HomeKey.forBranchName(dataSpace));
			if (dataSpaceRef == null) {
				throw OperationException.createError("Data space " + dataSpace + " can't be found.");
			}
			dataSetRef = dataSpaceRef.findAdaptationOrNull(
					AdaptationName.forName(dataSet));
			if (dataSetRef == null) {
				throw OperationException.createError("Data set " + dataSet + " can't be found in data space " + dataSpace + ".");
			}
			
			final AdaptationTable tableName = dataSetRef.getTable(Path.parse(tableID));
			
			// We want to compare against the initial version of the data in the data space.
			// Get the same table from the initial snapshot's data set
			final AdaptationTable initialTable = GOCWorkflowUtils.getTableInInitialSnapshot(tableName);
			
			LOG.debug("MergeTableChangesScriptTask.compareAdaptationTables started at " + Calendar.getInstance().getTime());
			// Compare the table with the same table in the initial snapshot's data set
			final DifferenceBetweenTables diff = DifferenceHelper.compareAdaptationTables(initialTable, tableName, false);
			LOG.debug("MergeTableChangesScriptTask.compareAdaptationTables finished at " + Calendar.getInstance().getTime());
			
			// All extra occurrences on the right will be all records that exist in data space
			// that are not in initial snapshot, i.e. those added.
			@SuppressWarnings("unchecked")
			final List<ExtraOccurrenceOnRight> adds = diff.getExtraOccurrencesOnRight();
			@SuppressWarnings("unchecked")
			final List<DifferenceBetweenOccurrences> deltas = diff.getDeltaOccurrences();
			
			final UpdateRecordsProcedure updateProc = new UpdateRecordsProcedure(adds, deltas);
			Utils.executeProcedure(updateProc, context.getSession(), dataSpaceRef);
			
			
			} catch (OperationException ex) {
			LOG.error("MergeTableChangesScriptTask: Error while preparing data space for merge.", ex);
		
		}
		String notificationDLName = DSMTConstants.propertyHelper.getProp(DSMTConstants.WORKFLOW_ADMIN_DISTRIBUTION_LIST);
		if(dataSet.contains(DSMTConstants.DATASPACE_LA)){
			notificationDLName =DSMTConstants.propertyHelper.getProp(DSMTConstants.LATAM_WORKFLOW_ADMIN_DISTRIBUTION_LIST);
			}else if(dataSet.contains(DSMTConstants.DATASET_ROSETTA)
					|| dataSet.contains(DSMTConstants.DATASET_R3MANUAL)){
				notificationDLName =DSMTConstants.propertyHelper.getProp(DSMTConstants.ROSETTA_WORKFLOW_ADMIN_DISTRIBUTION_LIST);
				}else if(dataSet.contains(DSMTConstants.GOC_WORKFLOW_TYPE )||
						dataSet.contains(GOCConstants.DSMT2_HIERARCHY_DATA_SET)||
						dataSet.contains(DSMTConstants.DATASET_NEW_DSMT2)){
					notificationDLName =DSMTConstants.propertyHelper.getProp(DSMTConstants.WORKFLOW_ADMIN_DISTRIBUTION_LIST);
				}else{
					notificationDLName =DSMTConstants.propertyHelper.getProp(DSMTConstants.DEFAULT_WORKFLOW_ADMIN_DISTRIBUTION_LIST);
				}
		
		notifyWorkflowCompletion(notificationDLName);
		
		LOG.info("MergeTableChangesScriptTask executeScript finished");
	}
	
	/**
	 * Notify Workflow Completion to Workflow maintenance teams.
	 */
	private void notifyWorkflowCompletion( String notificationDLName) {
	
		
		String notificationDL = notificationDLName;
		String subject = null;
		String message = null;
		
			
			subject = "DSMT2  Workflow completed - Request ID : " +  requestID;
			message = "Workflow Successfully completed. [requestID = " + requestID + ", dataSpace = " + dataSpace + ", approvers = " + approvalID + "]";
			DSMTNotificationService.notifyEmailID(notificationDL, subject, message , false);
	}

	private class UpdateRecordsProcedure implements Procedure {
		private List<ExtraOccurrenceOnRight> adds;
		private List<DifferenceBetweenOccurrences> deltas;
		
		public UpdateRecordsProcedure(final List<ExtraOccurrenceOnRight> adds,
				final List<DifferenceBetweenOccurrences> deltas) {
			this.adds = adds;
			this.deltas = deltas;
		}
		
		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			
			LOG.info("MergeTableChangesScriptTask - merege process started at " + Calendar.getInstance().getTime() );
			
			for (ExtraOccurrenceOnRight add: adds) {
				final Adaptation record = add.getExtraOccurrence();
				updateRecord(record, pContext);
			}
			LOG.debug("MergeTableChangesScriptTask - Updation in Child data sapce started at " + Calendar.getInstance().getTime() );
			for (DifferenceBetweenOccurrences delta: deltas) {
				final Adaptation record = delta.getOccurrenceOnRight();
				updateRecord(record, pContext);
			}
			
			pContext.setTriggerActivation(true);
			pContext.setAllPrivileges(false);
		}
		
		private void updateRecord(final Adaptation record, final ProcedureContext pContext)
				throws OperationException {
			final ValueContextForUpdate vc = pContext.getContext(record.getAdaptationName());
			Date now = Calendar.getInstance().getTime();
			try{
			vc.setValue(now, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._MERGE_DATE);// REQUIRED TO track when record was merged after workflow approval
			vc.setValue(approvalID, Path.parse("./APPROVED_BY"));
			}catch(Exception ex){
				LOG.error("MergeTableChangesScriptTask - "+GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._MERGE_DATE.format() + "Column not found in table "+
						tableID);
			}
			
			pContext.doModifyContent(record, vc);
		
	}
	
	
	
	

	}		
}
