package com.citi.ebx.workflow;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.DSMT2HierarchyPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

@Deprecated
public class PrepareGOCDataSpaceForMergeScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private String dataSpace;
	private String dataSet;
	private boolean writeErrorToContext = true;
	private String errorMessage;
	private String errorDetails;
	
	private String requestID;
	private String gridRoutingID;
	private String approverList;
	
	private Adaptation dataSetRef;
	
	
	private Date effectiveDate;
	
	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public boolean isWriteErrorToContext() {
		return writeErrorToContext;
	}

	public void setWriteErrorToContext(boolean writeErrorToContext) {
		this.writeErrorToContext = writeErrorToContext;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getGridRoutingID() {
		return gridRoutingID;
	}

	public void setGridRoutingID(String gridRoutingID) {
		this.gridRoutingID = gridRoutingID;
	}

	public String getApproverList() {
		return approverList;
	}

	public void setApproverList(String approverList) {
		this.approverList = approverList;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		LOG.info("PrepareGOCDataSpaceForMerge executeScript started");
		
		LOG.info("Received approval response for dataSpace " + dataSpace + ",  workflow Request ID - " +requestID + ", approversSOEID " + approverList);
		
		try {
			final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
					HomeKey.forBranchName(dataSpace));
			if (dataSpaceRef == null) {
				throw OperationException.createError("Data space " + dataSpace + " can't be found.");
			}
			dataSetRef = dataSpaceRef.findAdaptationOrNull(
					AdaptationName.forName(dataSet));
			if (dataSetRef == null) {
				throw OperationException.createError("Data set " + dataSet + " can't be found in data space " + dataSpace + ".");
			}
			
			final AdaptationTable gocTable = dataSetRef.getTable(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC.getPathInSchema());
			
			// We want to compare against the initial version of the data in the data space.
			// Get the same table from the initial snapshot's data set
			final AdaptationTable initialTable = GOCWorkflowUtils.getTableInInitialSnapshot(gocTable);
			
			// Compare the table with the same table in the initial snapshot's data set
			final DifferenceBetweenTables diff = DifferenceHelper.compareAdaptationTables(initialTable, gocTable, false);
			
			// All extra occurrences on the right will be all records that exist in data space
			// that are not in initial snapshot, i.e. those added.
			@SuppressWarnings("unchecked")
			final List<ExtraOccurrenceOnRight> adds = diff.getExtraOccurrencesOnRight();
			@SuppressWarnings("unchecked")
			final List<DifferenceBetweenOccurrences> deltas = diff.getDeltaOccurrences();
			
			final UpdateRecordsProcedure updateProc = new UpdateRecordsProcedure(adds, deltas);
			Utils.executeProcedure(updateProc, context.getSession(), dataSpaceRef);
			
			final CopyRecordsToHistoricalTableProcedure copyProc = new CopyRecordsToHistoricalTableProcedure(adds, deltas);
			final AdaptationHome dsmt2HierDataSpace = context.getRepository().lookupHome(
					HomeKey.forBranchName(GOCConstants.DSMT2_HIERARCHY_DATA_SPACE));
			if (dsmt2HierDataSpace == null) {
				throw OperationException.createError("Data space " + GOCConstants.DSMT2_HIERARCHY_DATA_SPACE + " can't be found.");
			}
			Utils.executeProcedure(copyProc, context.getSession(), dsmt2HierDataSpace);

		} catch (OperationException ex) {
			LOG.error("PrepareGOCDataSpaceForMergeScriptTask: Error while preparing data space for merge.", ex);
			if (writeErrorToContext) {
				errorMessage = ex.getMessage();
				errorDetails = String.valueOf(ex); //TODO
			} else {
				throw ex;
			}
		}
		
		LOG.info("PrepareGOCDataSpaceForMerge executeScript finished");
	}
	
	private class UpdateRecordsProcedure implements Procedure {
		private List<ExtraOccurrenceOnRight> adds;
		private List<DifferenceBetweenOccurrences> deltas;
		
		public UpdateRecordsProcedure(final List<ExtraOccurrenceOnRight> adds,
				final List<DifferenceBetweenOccurrences> deltas) {
			this.adds = adds;
			this.deltas = deltas;
		}
		
		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			
			for (ExtraOccurrenceOnRight add: adds) {
				final Adaptation record = add.getExtraOccurrence();
				updateRecord(record, pContext);
			}
			for (DifferenceBetweenOccurrences delta: deltas) {
				final Adaptation record = delta.getOccurrenceOnRight();
				updateRecord(record, pContext);
			}
			
			GOCWorkflowUtils.setWorkflowParameters(new HashMap<String, String>(), dataSetRef, pContext);
			
			pContext.setTriggerActivation(true);
			pContext.setAllPrivileges(false);
		}
		
		private void updateRecord(final Adaptation record, final ProcedureContext pContext)
				throws OperationException {
			final ValueContextForUpdate vc = pContext.getContext(record.getAdaptationName());
			
			/*final String actionRequired = (String) vc.getValue(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD);
			if(actionRequired.equalsIgnoreCase(GOCConstants.ACTION_PURGE_GOC_LEG_CCS)){
				vc.setValue(GOCWorkflowUtils.calculateGOCEffectiveDate(record),	GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ENDDT);
				LOG.info("Purging GOC " + (String.valueOf(vc.getValue(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC))));
			}*/
			
			/*vc.setValue(GOCWorkflowUtils.calculateGOCEffectiveDate(record),
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFFDT);*/
			
			if(null == effectiveDate){
				effectiveDate =  GOCWorkflowUtils.calculateGOCEffectiveDate(dataSetRef,pContext);
			}
			
			Date now = Calendar.getInstance().getTime();
			vc.setValue(now, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._MERGE_DATE);// REQUIRED TO track when GOC was merged after workflow approval
			vc.setValue(effectiveDate, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFFDT);
			vc.setValue(vc.getValue(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD), GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LAST_ACTION_REQUIRED);	
			vc.setValue(GOCConstants.ACTION_NO_CHANGE, GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD);
			
			pContext.doModifyContent(record, vc);
		}
	}
	
	private class CopyRecordsToHistoricalTableProcedure implements Procedure {
		private List<ExtraOccurrenceOnRight> adds;
		private List<DifferenceBetweenOccurrences> deltas;
		
		public CopyRecordsToHistoricalTableProcedure(final List<ExtraOccurrenceOnRight> adds,
				final List<DifferenceBetweenOccurrences> deltas) {
			this.adds = adds;
			this.deltas = deltas;
		}
		
		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			final AdaptationHome targetDataSpace = pContext.getAdaptationHome();
			final Adaptation targetDataSet = targetDataSpace.findAdaptationOrNull(
					AdaptationName.forName(GOCConstants.DSMT2_HIERARCHY_DATA_SET));
			final AdaptationTable targetTable = targetDataSet.getTable(
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC.getPathInSchema());
			
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			
			for (ExtraOccurrenceOnRight add: adds) {
				final Adaptation record = add.getExtraOccurrence();
				copyRecord(record, pContext, targetTable);
			}
			for (DifferenceBetweenOccurrences delta: deltas) {
				final Adaptation record = delta.getOccurrenceOnRight();
				copyRecord(record, pContext, targetTable);
			}
			
			pContext.setTriggerActivation(true);
			pContext.setAllPrivileges(false);
		}
		
		private void copyRecord(final Adaptation record, final ProcedureContext pContext,
				final AdaptationTable targetTable) throws OperationException {
			final Object setId = record.get(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._SETID);
			final Object goc = record.get(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC);
			final Object effDate = record.get(
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFFDT);
			final PrimaryKey pk = targetTable.computePrimaryKey(new Object[] {setId, goc, effDate});
			Adaptation targetRecord = targetTable.lookupAdaptationByPrimaryKey(pk);
			
			final ValueContextForUpdate vc;
			if (targetRecord == null) {
				vc = pContext.getContextForNewOccurrence(targetTable);
				vc.setValue(setId, DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._SETID);
				vc.setValue(goc, DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._C_GOC_ID);
				vc.setValue(effDate, DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._EFFDT);
			} else {
				vc = pContext.getContext(targetRecord.getAdaptationName());
			}
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFF_STATUS),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._EFF_STATUS);
			
			final Adaptation mgrRecord = Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._MANAGER_ID);
			if (mgrRecord != null) {
				vc.setValue(mgrRecord.get(
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_MANAGER._MANAGER_ID),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._MANAGER_ID);
			}
			
			final Adaptation sidRecord = Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID);
			if (sidRecord != null) {
				vc.setValue(sidRecord.get(
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_SID._C_DSMT_SID),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._C_DSMT_SID);
			}
			
			final Adaptation buRecord = Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU);
			if (buRecord != null) {
				vc.setValue(buRecord.get(
					DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_BU_STD_C_DSMT_FRS_BU._C_DSMT_FRS_BU),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._C_DSMT_FRS_BU);
			}
			
			final Adaptation ouRecord = Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_OU);
			if (ouRecord != null) {
				vc.setValue(ouRecord.get(
					DSMT2HierarchyPaths._GLOBAL_STD_OFD_STD_F_OU_STD_C_DSMT_FRS_OU._C_DSMT_FRS_OU),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._C_DSMT_FRS_OU);
			}
			
			final Adaptation mgRecord = Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID);
			if (mgRecord != null) {
				vc.setValue(mgRecord.get(
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_GEO_STD_C_DSMT_MAN_GEO_NEW._C_DSMT_MAN_GEO_ID),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._C_DSMT_MAN_GEO_ID);
			}
			
			final Adaptation msRecord = Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID);
			if (msRecord != null) {
				vc.setValue(msRecord.get(
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_MANAGED_SEG_STD_C_DSMT_MAN_SEG_NEW._C_DSMT_MAN_SEG_ID),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._C_DSMT_MAN_SEG_ID);
			}
			
			// TODO: Function?
			final Adaptation functionRecord = Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FUNCTION);
			if (functionRecord != null) {
				vc.setValue(functionRecord.get(
					DSMT2HierarchyPaths._GLOBAL_STD_PMF_STD_F_FUNCTION_STD_Function_New2._C_DSMT_FUNCTION),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._C_DSMT_FUNCTION);
			}
			
			
			final Adaptation lvidRecord = Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LVID);
			if (lvidRecord != null) {
				vc.setValue(lvidRecord.get(
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_LEGAL_VEH_STD_C_DSMT_LVID_TBL._C_DSMT_LVID),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._C_DSMT_LVID);
			}
			
			// TODO: Headcount?
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._HEADCOUNT_GOC),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._HEADCOUNT_GOC);
			
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCRSHORT),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._DESCRSHORT);
			
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCR),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._DESCR);
			
			// TODO: Usage?
			final Adaptation usageRecord = Utils.getLinkedRecord(record,
					GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_USAGE);
			if (usageRecord != null) {
				vc.setValue(usageRecord.get(
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC_USE._C_DSMT_GOC_USAGE),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._C_DSMT_GOC_USAGE);
			}
			
			// TODO: Local Cost Code?
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._C_DSMT_LOC_COST_CD);
			
			// TODO: Comment?
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_COMMENT),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._C_DSMT_GOC_COMMENT);
			
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CREATED_OPERID),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._CREATED_OPERID);
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ORIG_DTTM),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._ORIG_DTTM);
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LASTUPDOPRID),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._LASTUPDOPRID);
			vc.setValue(record.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CHNG_DTTM),
					DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._CHNG_DTTM);
			
			// TODO: Merge Date (use current time stamp for this field)
			vc.setValue(Calendar.getInstance().getTime(), DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._MERGE_DATE);
			
			
			vc.setValue(requestID, DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._WF_REQUEST_ID);
			vc.setValue(gridRoutingID, DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._DATASPACE_ID);
			vc.setValue(approverList, DSMT2HierarchyPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_GOC._ApproverSOEID);
			
			if (targetRecord == null) {
				targetRecord = pContext.doCreateOccurrence(vc,  targetTable);
			} else {
				targetRecord = pContext.doModifyContent(targetRecord,  vc);
			}
		}
	}
}
