package com.citi.ebx.workflow;


import java.util.Iterator;
import java.util.List;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.MonitoringTableBean;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.util.InsertDSMTWFMonitoringdata;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.comparison.DifferenceBetweenInstances;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class InsertDSMTWFReportingData extends ScriptTaskBean{

private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private String dataSpace;
	private String dataSet;
	private String requestID;
	private String tableID;
	private String allocatedUser;
	private String operation;
	private String userComment;
	private String tableUpdated;
	int recordCount =0;
	
	public String getDataSpace() {
		return dataSpace;
	}


	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}


	public String getDataSet() {
		return dataSet;
	}


	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}


	public String getRequestID() {
		return requestID;
	}


	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}


	public String getTableID() {
		return tableID;
	}


	public void setTableID(String tableID) {
		this.tableID = tableID;
	}


	public String getAllocatedUser() {
		return allocatedUser;
	}


	public void setAllocatedUser(String allocatedUser) {
		this.allocatedUser = allocatedUser;
	}


	public String getOperation() {
		return operation;
	}


	public void setOperation(String operation) {
		this.operation = operation;
	}


	public String getUserComment() {
		return userComment;
	}


	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}


	public int getRecordCount() {
		return recordCount;
	}


	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}


	public String getTableUpdated() {
		return tableUpdated;
	}


	public void setTableUpdated(String tableUpdated) {
		this.tableUpdated = tableUpdated;
	}


	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		LOG.info("InsertGOCReportingScriptTask executeScript started for dataSpace " + dataSpace);
		LOG.debug("InsertGOCReportingScriptTask executeScript operation " + operation);
		
		if(DSMTConstants.INSERT.equals(operation)){
			insertMonitoringdata(context);
		}/*else if(DSMTConstants.UPDATE.equals(operation)){
			updateMonitoringdata(context);
		}*/
		else if(DSMTConstants.REJECT.equals(operation) || DSMTConstants.SUBMIT.equals(operation)){
			updateMonitoringdata(context);
		}
		
		LOG.debug("InsertGOCReportingScriptTask executeScript finished");
		
	}
	
private void insertMonitoringdata(ScriptTaskBeanContext context) {
		
		LOG.info("InsertGOCReportingScriptTask insertMonitoringdata started for dataSpace");
			
		try {
			MonitoringTableBean monitoringTableBean = new MonitoringTableBean();
			monitoringTableBean.setChildDataSpaceID(dataSpace);
			monitoringTableBean.setDataSet(dataSet);
			monitoringTableBean.setOperation(operation);
			LOG.debug("requestID >> "+requestID);
			monitoringTableBean.setRequestID(requestID);
			monitoringTableBean.setRequestType(GOCConstants.DSMT_WORKFLOW_PUBLICATION);
			if(allocatedUser!=null){
			monitoringTableBean.setRequestBy(allocatedUser.replace("U", ""));
			}
			monitoringTableBean.setRequestStatus(GOCConstants.CREATED);
			monitoringTableBean.setRequestorComment(userComment);
			monitoringTableBean.setTableUpdated(tableUpdated);
			InsertDSMTWFMonitoringdata insertDSMTWFMonitoringdata = new InsertDSMTWFMonitoringdata(monitoringTableBean ,context.getRepository(), tableID, tableUpdated);
				Utils.executeProcedure(insertDSMTWFMonitoringdata, context.getSession(), context.getRepository().lookupHome(HomeKey.forBranchName("DSMT_WF")));
			
			} catch (Exception ex) {
			LOG.error("InsertGOCReportingScriptTask: Error while preparing data space for merge.", ex);
		
		}
		
	}

private void updateMonitoringdata(ScriptTaskBeanContext context) {
	
	LOG.info("InsertGOCReportingScriptTask updateMonitoringdata started for dataSpace for requestID " + requestID);
			
	try {
		MonitoringTableBean monitoringTableBean = new MonitoringTableBean();
		monitoringTableBean.setOperation(operation);
		monitoringTableBean.setChildDataSpaceID(dataSpace);
		LOG.debug("requestID >> "+requestID);
		
		monitoringTableBean.setRequestID(requestID);
		if(DSMTConstants.SUBMIT.equals(operation)){
			int deltaSize = 0;
			monitoringTableBean.setRequestStatus(GOCConstants.SUBMITTED);
			final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(HomeKey.forBranchName(dataSpace));
			final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
			final AdaptationHome initialSnapshot = dataSpaceRef.getParent();
			final Adaptation initialDataSet = initialSnapshot.findAdaptationOrNull(dataSetRef.getAdaptationName());
			final DifferenceBetweenInstances dataSetDiffs = DifferenceHelper.compareInstances(initialDataSet, dataSetRef, false);
			final List<DifferenceBetweenTables> tableDiffsList = dataSetDiffs.getDeltaTables();
			
			final Iterator<DifferenceBetweenTables> tableDiffsIter = tableDiffsList.iterator();
			
			while (tableDiffsIter.hasNext()) {
				final DifferenceBetweenTables tableDiffs = tableDiffsIter.next();

				final List<ExtraOccurrenceOnRight> adds = tableDiffs.getExtraOccurrencesOnRight();
				final List<DifferenceBetweenOccurrences> deltas = tableDiffs.getDeltaOccurrences();
				int totalAdditions = adds.size();
				int totalUpdates =  deltas.size();
				deltaSize = totalAdditions + totalUpdates;
				
			}
			
			//deltaSize = dataSetDiffs.getDeltaTablesSize();
			monitoringTableBean.setRecordCount(String.valueOf(deltaSize));
			
		}else if (DSMTConstants.REJECT.equals(operation)){
			monitoringTableBean.setRequestStatus(GOCConstants.REJECTED);	
		}
		monitoringTableBean.setRequestorComment(userComment);
		InsertDSMTWFMonitoringdata insertDSMTWFMonitoringdata = new InsertDSMTWFMonitoringdata(monitoringTableBean ,context.getRepository(), tableID, tableUpdated);
		Utils.executeProcedure(insertDSMTWFMonitoringdata, context.getSession(), context.getRepository().lookupHome(HomeKey.forBranchName("DSMT_WF")));
		
		} catch (Exception ex) {
		LOG.error("InsertGOCReportingScriptTask: Error while preparing data space for merge.", ex);
	
	}
	

	
}
}
