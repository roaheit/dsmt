package com.citi.ebx.workflow;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.citi.ebx.dsmt.jdbc.connector.SQLReportConnectionUtils;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class GOCMisallignmentUpdateScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {

		try {

			Connection con = SQLReportConnectionUtils.getConnection();
			CallableStatement cStmt = con
					.prepareCall("{call GOC_MISALLIGNMENT_UPDATE ()}");
			cStmt.execute();

			con.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOG.info("Exception Occured while updating GOC misallignment : "
					+ e.getMessage());
			e.printStackTrace();
		}
	}

}
