package com.citi.ebx.workflow;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.IGWMonitoringTableBean;
import com.citi.ebx.dsmt.util.MonitoringTableBean;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.IGWInsertMonitoringdata;
import com.citi.ebx.util.InsertMonitoringdata;
import com.citi.ebx.util.Utils;
import com.citi.ebx.workflow.vo.DsmtGocVO;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class IGWInsertGOCReportingScriptTask extends ScriptTaskBean{
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private String listOfCDAndWFCreated;
	private String dataSet;
	private String tableID;
	private String allocatedUser;
	private String operation;
	private String userComment;
	private String dataSpace;	
	private String requestID;
	private String newGOCCount;
	private String totalMod;
	private String updatedGOCCount;
	private String newNonGOCCount;
	private String updatedNonGOCCount;
	private String maintenanceType;
	private String functionalID;

	
	
	
	
	
	public String getFunctionalID() {
		return functionalID;
	}

	public void setFunctionalID(String functionalID) {
		this.functionalID = functionalID;
	}
	

	public String getNewGOCCount() {
		return newGOCCount;
	}

	public void setNewGOCCount(String newGOCCount) {
		this.newGOCCount = newGOCCount;
	}

	public String getTotalMod() {
		return totalMod;
	}

	public void setTotalMod(String totalMod) {
		this.totalMod = totalMod;
	}

	public String getUpdatedGOCCount() {
		return updatedGOCCount;
	}

	public void setUpdatedGOCCount(String updatedGOCCount) {
		this.updatedGOCCount = updatedGOCCount;
	}

	public String getNewNonGOCCount() {
		return newNonGOCCount;
	}

	public void setNewNonGOCCount(String newNonGOCCount) {
		this.newNonGOCCount = newNonGOCCount;
	}

	public String getUpdatedNonGOCCount() {
		return updatedNonGOCCount;
	}

	public void setUpdatedNonGOCCount(String updatedNonGOCCount) {
		this.updatedNonGOCCount = updatedNonGOCCount;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	int gocCount =0;
	

	public String getListOfCDAndWFCreated() {
		return listOfCDAndWFCreated;
	}

	public void setListOfCDAndWFCreated(String listOfCDAndWFCreated) {
		this.listOfCDAndWFCreated = listOfCDAndWFCreated;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}
	
	/**
	 * @return the tableID
	 */
	public String getTableID() {
		return tableID;
	}

	/**
	 * @param tableID the tableID to set
	 */
	public void setTableID(String tableID) {
		this.tableID = tableID;
	}




	/**
	 * @return the allocatedUser
	 */
	public String getAllocatedUser() {
		return allocatedUser;
	}

	/**
	 * @param allocatedUser the allocatedUser to set
	 */
	public void setAllocatedUser(String allocatedUser) {
		this.allocatedUser = allocatedUser;
	}
	
	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * @param operation the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	/**
	 * @return the userComment
	 */
	public String getUserComment() {
		return userComment;
	}

	/**
	 * @param userComment the userComment to set
	 */
	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}
	


	public String getMaintenanceType() {
		return maintenanceType;
	}

	public void setMaintenanceType(String maintenanceType) {
		this.maintenanceType = maintenanceType;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		
		
		LOG.info("IGWInsertGOCReportingScriptTask executeScript started ");
		
		if(null!=listOfCDAndWFCreated){

			LOG.info("listOfCDAndWFCreated == "+ listOfCDAndWFCreated);
			
			StringTokenizer tokenizer= new StringTokenizer(listOfCDAndWFCreated, ",");
			Map<String, String> cdAndWFMap= new HashMap<String, String>();
			while (tokenizer.hasMoreTokens()){
				String cdAndWF= tokenizer.nextToken();
				cdAndWFMap.put(cdAndWF.substring(0,cdAndWF.indexOf("|")), cdAndWF.substring(cdAndWF.indexOf("|")+1,cdAndWF.length()));
				
			}
			for(String dataSpace: cdAndWFMap.keySet()){
				if(DSMTConstants.INSERT.equals(operation)){
					insertMonitoringdata(context,dataSpace,cdAndWFMap.get(dataSpace)  );
				}else if(DSMTConstants.UPDATE.equals(operation)){
					updateMonitoringdata(context, dataSpace,cdAndWFMap.get(dataSpace) );
				}
				else if(DSMTConstants.REJECT.equals(operation)){
					updateMonitoringdata(context, dataSpace,cdAndWFMap.get(dataSpace) );
				}
				
				
			}
			
			
		}
		else{
			if(DSMTConstants.INSERT.equals(operation)){
				insertMonitoringdata(context,dataSpace,requestID);
			}else if(DSMTConstants.UPDATE.equals(operation)){
				updateMonitoringdata(context,dataSpace,requestID);
			}
			else if(DSMTConstants.REJECT.equals(operation)){
				updateMonitoringdata(context,dataSpace,requestID);
			}
			
		}
		
		
		LOG.info("IGWInsertGOCReportingScriptTask executeScript finished");
	}

	private void insertMonitoringdata(ScriptTaskBeanContext context, String dataSpace,String requestID) {
		
		LOG.info("InsertGOCReportingScriptTask insertMonitoringdata started for dataSpace"+dataSpace +" and request ID : "+requestID);
			
		try {
			IGWMonitoringTableBean monitoringTableBean = new IGWMonitoringTableBean();
			monitoringTableBean.setChildDataSpaceID(dataSpace);
			monitoringTableBean.setOperation(operation);
			
			if(DSMTConstants.DIRECTUPLOAD.equalsIgnoreCase(requestID)){
				monitoringTableBean.setRequestID(String.valueOf(Calendar.getInstance().getTime().getTime()));
				monitoringTableBean.setRequestType(DSMTConstants.DIRECTUPLOAD);
				}else{
					monitoringTableBean.setRequestID(requestID);
					monitoringTableBean.setRequestType(GOCConstants.WORKFLOW_PUBLICATION);
					
				}
			
			if(allocatedUser!=null){
			monitoringTableBean.setRequestBy(allocatedUser.replace("U", ""));
			}
			monitoringTableBean.setRequestStatus(GOCConstants.CREATED);
			monitoringTableBean.setMaintenanceType(maintenanceType);
			monitoringTableBean.setRequestorComment(userComment);
			//monitoringTableBean.setFunctionalID(functionalID);
			if(!tableID.contains("GOC_WF")){
				monitoringTableBean.setGocRecordCount(newGOCCount);
				monitoringTableBean.setUpdatedGOCCount(updatedGOCCount);
				monitoringTableBean.setNewNonGOCCount(newNonGOCCount);
				monitoringTableBean.setUpdatedNonGOCCount(updatedNonGOCCount);
				monitoringTableBean.setTotalMod(totalMod);
				//monitoringTableBean.setFunctionalID(functionalID);
				monitoringTableBean.setMaintenanceType(maintenanceType);	
			}
			
			 IGWInsertMonitoringdata insertMonitoringdata = new IGWInsertMonitoringdata(monitoringTableBean ,context.getRepository(), tableID);
				Utils.executeProcedure(insertMonitoringdata, context.getSession(), context.getRepository().lookupHome(
				HomeKey.forBranchName(GOCConstants.METADATA_DATA_SPACE)));
			
			} catch (Exception ex) {
			LOG.error("IGWInsertGOCReportingScriptTask: Error while preparing data space for merge.", ex);
		
		}
		
	}
	
	private void updateMonitoringdata(ScriptTaskBeanContext context, String dataSpace,String requestID) {
		
		LOG.info("InsertGOCReportingScriptTask updateMonitoringdata started for dataSpace : "+dataSpace+ " and for requestID :" + requestID);
		ArrayList<DsmtGocVO> dsmtGocVOs = new ArrayList<DsmtGocVO>();
			if(!DSMTConstants.DIRECTUPLOAD.equalsIgnoreCase(requestID)){
				if(DSMTConstants.UPDATE.equals(operation))
				dsmtGocVOs = determineGOCFields(context, dataSpace);
			}
			
		try {
			MonitoringTableBean monitoringTableBean = new MonitoringTableBean();
			monitoringTableBean.setOperation(operation);
			monitoringTableBean.setChildDataSpaceID(dataSpace);
			
			
			monitoringTableBean.setRequestID(requestID);
			if(DSMTConstants.UPDATE.equals(operation)){
			monitoringTableBean.setRequestStatus(GOCConstants.SUBMITTED);
			}
			else{
				monitoringTableBean.setRequestStatus(GOCConstants.REJECTED);
			}
			monitoringTableBean.setGocRecordCount(String.valueOf(gocCount));
			monitoringTableBean.setDsmtGocVOs(dsmtGocVOs);
			monitoringTableBean.setRequestorComment(userComment);
			monitoringTableBean.setMaintenanceType(maintenanceType);
			//monitoringTableBean.setFunctionID(functionalID);
			
			 InsertMonitoringdata insertMonitoringdata = new InsertMonitoringdata(monitoringTableBean ,context.getRepository(), tableID);
				Utils.executeProcedure(insertMonitoringdata, context.getSession(), context.getRepository().lookupHome(
				HomeKey.forBranchName(GOCConstants.METADATA_DATA_SPACE)));
			
			} catch (Exception ex) {
			LOG.error("IGWInsertGOCReportingScriptTask: Error while preparing data space for merge.", ex);
		
		}
		
	
		
	}
	
	protected  ArrayList<DsmtGocVO>  determineGOCFields(ScriptTaskBeanContext context, String dataSpace) {
		
		LOG.info("IGWInsertMonitoringdata :  determineGOCFields Starts ");
		
		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(HomeKey.forBranchName(dataSpace));
		
		
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
		final AdaptationTable gocTable = dataSetRef.getTable(
				GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC.getPathInSchema());
		
		final AdaptationTable initialTable = GOCWorkflowUtils.getTableInInitialSnapshot(gocTable);
		
		
		ArrayList<DsmtGocVO> gocVOs = new ArrayList<DsmtGocVO>();
		final DifferenceBetweenTables diff = DifferenceHelper.compareAdaptationTables(initialTable, gocTable, false);
		@SuppressWarnings("unchecked")
		List<ExtraOccurrenceOnRight> adds = diff.getExtraOccurrencesOnRight();
			for (int i = 0; i < adds.size(); i++) {
				final ExtraOccurrenceOnRight add = adds.get(i);
				final Adaptation record = add.getExtraOccurrence();
				DsmtGocVO dsmtGocVO = new DsmtGocVO();
				dsmtGocVO.setGOC(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC));
				dsmtGocVO.setGOCPK(String.valueOf(record.get(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK)));
				dsmtGocVO.setMANAGER_ID(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._MANAGER_ID));
				dsmtGocVO.setSETID(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID));
				dsmtGocVO.setC_DSMT_MAN_GEO_ID(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID));
				dsmtGocVO.setC_DSMT_MAN_SEG_ID(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID));
				dsmtGocVO.setC_DSMT_FRS_BU(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU));
				dsmtGocVO.setC_DSMT_FRS_OU(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_OU));
				dsmtGocVO.setC_DSMT_FUNCTION(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FUNCTION));
				dsmtGocVO.setC_DSMT_LVID(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LVID));
				dsmtGocVO.setDesc( record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCR));
				dsmtGocVO.setDescShort( record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCRSHORT));
				dsmtGocVO.setGocUsages( record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_USAGE));
				dsmtGocVO.setC_DSMT_LOC_COST_CD(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD));				
				dsmtGocVO.setACTION_REQD(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD));
				dsmtGocVO.setApproverGroup(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup));
				dsmtGocVO.setHeadCount_GOC(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._HEADCOUNT_GOC));
				dsmtGocVO.setComment(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_COMMENT));
				dsmtGocVO.setFunctionalID(record.getString(
						GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Functional_ID));
				gocVOs.add(dsmtGocVO);
			}
		
			@SuppressWarnings("unchecked")
			List<DifferenceBetweenOccurrences> deltas = diff.getDeltaOccurrences();
			for (int i = 0; i < deltas.size(); i++) {
					final DifferenceBetweenOccurrences delta = deltas.get(i);
					final Adaptation record = delta.getOccurrenceOnRight();
					
					DsmtGocVO dsmtGocVO = new DsmtGocVO();
					dsmtGocVO.setGOC(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC));
					dsmtGocVO.setGOCPK(String.valueOf(record.get(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK)));
					dsmtGocVO.setMANAGER_ID(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._MANAGER_ID));
					dsmtGocVO.setSETID(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID));
					dsmtGocVO.setC_DSMT_MAN_GEO_ID(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID));
					dsmtGocVO.setC_DSMT_MAN_SEG_ID(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID));
					dsmtGocVO.setC_DSMT_FRS_BU(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU));
					dsmtGocVO.setC_DSMT_FRS_OU(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_OU));
					dsmtGocVO.setC_DSMT_FUNCTION(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FUNCTION));
					dsmtGocVO.setC_DSMT_LVID(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LVID));
					dsmtGocVO.setDesc( record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCR));
					dsmtGocVO.setDescShort( record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCRSHORT));
					dsmtGocVO.setGocUsages( record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_USAGE));
					dsmtGocVO.setC_DSMT_LOC_COST_CD(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD));
					dsmtGocVO.setACTION_REQD(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD));
					dsmtGocVO.setApproverGroup(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup));
					dsmtGocVO.setHeadCount_GOC(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._HEADCOUNT_GOC));
					dsmtGocVO.setComment(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_COMMENT));
					dsmtGocVO.setFunctionalID(record.getString(
							GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Functional_ID));
					gocVOs.add(dsmtGocVO);
					
				}
		
		gocCount =	deltas.size() +adds.size() ;
		LOG.info("IGWInsertMonitoringdata :  determineGOCFields Ends ");
			return gocVOs;
	}

	
	

}
