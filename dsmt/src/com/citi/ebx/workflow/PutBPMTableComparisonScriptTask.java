package com.citi.ebx.workflow;

import com.citi.ebx.dsmt.jms.JMSComparisonMessageHelper;
import com.citi.ebx.dsmt.jms.JMSComparisonMessageHelper.ComparisonLevel;
import com.citi.ebx.dsmt.jms.JMSMessageHelper;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

public class PutBPMTableComparisonScriptTask extends JMSMessageScriptTask {
	private String bpmProcessID;
	

	private String requestID;
	private String dataSpace;
	private String dataSet;
	private String tableXPath;
	private String user;
	private String userComment;
	private String tableID;
	private String tableName;
	private String workflowStatus;
	
	
	

	@Override
	protected JMSMessageHelper createJMSMessageHelper(
			Repository repository, Session session)
			throws OperationException {
		final JMSComparisonMessageHelper helper = new JMSComparisonMessageHelper();
		helper.setRepository(repository);
		helper.setSession(session);
		helper.setLog(LoggingCategory.getWorkflow());
		helper.setComparisonLevel(ComparisonLevel.TABLE);
		helper.setWorkflowType(DSMTConstants.DSMT2_WORKFLOW_TYPE);
		helper.setQueueName(queueName);
		helper.setBpmProcessID(bpmProcessID);
		helper.setRequestID(requestID);
		helper.setDataSpace(dataSpace);
		helper.setDataSet(dataSet);
		helper.setTableXPath(tableXPath);
		helper.setUser(user);
		helper.setUserComment(userComment);
		helper.setTableID(tableID);
		helper.setTableName(tableName);
		helper.setWorkflowStatus(workflowStatus);
		return helper;
	}
	
	public String getBpmProcessID() {
		return bpmProcessID;
	}

	public void setBpmProcessID(String bpmProcessID) {
		this.bpmProcessID = bpmProcessID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getRequestID() {
		return requestID;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public void setTableXPath(String tableXPath) {
		this.tableXPath = tableXPath;
	}

	public String getTableXPath() {
		return tableXPath;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getTableID() {
		return tableID;
	}

	public void setTableID(String tableID) {
		this.tableID = tableID;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}

	
	

	
}
