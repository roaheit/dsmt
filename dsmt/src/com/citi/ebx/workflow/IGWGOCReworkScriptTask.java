package com.citi.ebx.workflow;

import java.util.HashMap;
import java.util.Map;

import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.util.IGWGOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class IGWGOCReworkScriptTask extends ScriptTaskBean {

	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();

	private String dataSpace;
	private String dataSet;
	private String workflowStatus;

	/**
	 * @return the workflowStatus
	 */
	public String getWorkflowStatus() {
		return workflowStatus;
	}

	/**
	 * @param workflowStatus
	 *            the workflowStatus to set
	 */
	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
				HomeKey.forBranchName(dataSpace));
		if (dataSpaceRef == null) {
			throw OperationException.createError("Data space " + dataSpace
					+ " can't be found.");
		}
		final Adaptation dataSetRef = dataSpaceRef
				.findAdaptationOrNull(AdaptationName.forName(dataSet));
		if (dataSetRef == null) {
			throw OperationException.createError("Data set " + dataSet
					+ " can't be found in data space " + dataSpace + ".");
		}

		final Procedure setWFStatusProc = new Procedure() {
			@Override
			public void execute(ProcedureContext pContext) throws Exception {

				Map<Path, String> workflowParameterMap = new HashMap<Path, String>();

				workflowParameterMap.put(GOCPaths._WorkflowStatus,
						"Resubmitted");

				IGWGOCWorkflowUtils.setWorkflowParameters(workflowParameterMap,
						dataSetRef, pContext);

			}
		};

		Utils.executeProcedure(setWFStatusProc, context.getSession(), dataSpaceRef);
	}

}
