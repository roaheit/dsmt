package com.citi.ebx.workflow;



import java.io.IOException;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.exporter.DataSetExportConfig;
import com.orchestranetworks.ps.exporter.DataSetExportConfigFactory;
import com.orchestranetworks.ps.exporter.DataSetExporter;
import com.orchestranetworks.ps.exporter.DefaultTableExporterFactory;
import com.orchestranetworks.ps.exporter.TableExporterFactory;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class DataSetExportScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	

	protected String exportConfigID;
	private String dataSpace;
	private String dataSet;

	private String errorMessage;
	private String errorDetails;
	


	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
	
		LOG.info("Exporter code started with exportConfigID = " + exportConfigID + ", dataSpace = " + dataSpace + ", dataSet = " + dataSet );
		
		
		final Repository repo = context.getRepository();
		final Session session = context.getSession();
		
		
		try {
			final AdaptationHome dataSpaceRef = repo.lookupHome(HomeKey.forBranchName(dataSpace));
			if (dataSpaceRef == null) {
				throw OperationException.createError("Data space " + dataSpace + " can't be found.");
			}
			final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
			if (dataSetRef == null) {
				throw OperationException.createError("Data set " + dataSet + " can't be found in data space " + dataSpace + ".");
			}
			
			final TableExporterFactory tableExporterFactory = createTableExporterFactory();
			final DataSetExportConfig config = createConfig(repo);
			final DataSetExporter exporter = createExporter(tableExporterFactory, config);
			try {
				exporter.exportDataSet(session, dataSetRef);
			} catch (final IOException ex) {
				throw OperationException.createError(ex);
			}
			
			LOG.info("Export file created successfully at location " + config.getFolder() );
		} catch (OperationException ex) {
			ex.printStackTrace();
		}
	}
	

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getExportConfigID() {
		return exportConfigID;
	}

	public void setExportConfigID(String exportConfigID) {
		this.exportConfigID = exportConfigID;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}
	
	protected DataSetExporter createExporter(TableExporterFactory tableExporterFactory, DataSetExportConfig config) {
		LOG.info("DataSetExportServlet: createExporter");
		return new DataSetExporter(tableExporterFactory, config);
	}
	
	protected TableExporterFactory createTableExporterFactory() {
		LOG.info("DataSetExportServlet: createTableExporterFactory");
		return new DefaultTableExporterFactory();
	}
	
	protected DataSetExportConfig createConfig(Repository repo)
			throws OperationException {
		LOG.info("DataSetExportServlet: createConfig");
		// Look up the data space and data set for the export config records
		final AdaptationHome exportConfigDataSpaceRef = repo.lookupHome(
				HomeKey.forBranchName(DEFAULT_EXPORT_CONFIG_DATA_SPACE));
		final Adaptation exportConfigDataSetRef = exportConfigDataSpaceRef.findAdaptationOrNull(
				AdaptationName.forName(DEFAULT_EXPORT_CONFIG_DATA_SET));
		final DataSetExportConfigFactory configFactory = new DataSetExportConfigFactory();
		
		LOG.info("exportConfigDataSetRef = "+  exportConfigDataSetRef);
		return configFactory.createFromEBXRecord(
				exportConfigDataSetRef, exportConfigID);
		}

	
	private static final String DEFAULT_EXPORT_CONFIG_DATA_SPACE = "DataSetExport";
	private static final String DEFAULT_EXPORT_CONFIG_DATA_SET = "DataSetExport";
}
