package com.citi.ebx.workflow;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.openjpa.lib.log.Log;

import com.citi.ebx.dsmt.jms.IGWJMSGOCComparisonMessageHelper;
import com.citi.ebx.dsmt.jms.JMSComparisonMessageHelper.ComparisonLevel;
import com.citi.ebx.dsmt.jms.JMSMessageHelper;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class IGWGOCComparisonScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();

	private String listOfCDAndWFCreated;
	private String dataSet;
	private String user;
	private String userComment;
	private String maintenanceType;
	private String functionalID;
	private String managementOnly;
	private String dataSpace;
	private String requestID;
	
	
	

	public String getManagementOnly() {
		return managementOnly;
	}

	public void setManagementOnly(String managementOnly) {
		this.managementOnly = managementOnly;
	}

	private String workflowStatus;
	protected String queueName;

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getListOfCDAndWFCreated() {
		return listOfCDAndWFCreated;
	}

	public void setListOfCDAndWFCreated(String listOfCDAndWFCreated) {
		this.listOfCDAndWFCreated = listOfCDAndWFCreated;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}
	
	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	protected JMSMessageHelper createJMSMessageHelper(Repository repository, Session session, String dataSpace, String requestID, String parentID) throws OperationException {
		final IGWJMSGOCComparisonMessageHelper helper = new IGWJMSGOCComparisonMessageHelper();
		LOG.info("createJMSMessageHelper: entered");
		LOG.info("createJMSMessageHelper: dataspace:"+dataSpace);
		LOG.info("createJMSMessageHelper: requestID:"+requestID);
		helper.setRepository(repository);
		helper.setSession(session);
		helper.setLog(LoggingCategory.getWorkflow());
		helper.setComparisonLevel(ComparisonLevel.DATASET);

		helper.setQueueName(queueName);
		helper.setRequestID(requestID);
		helper.setDataSpace(dataSpace);
		helper.setDataSet(dataSet);
		helper.setUser(user);
		helper.setUserComment(userComment);
		helper.setMaintenanceType(maintenanceType);
		helper.setFunctionalID(functionalID);
		helper.setWorkflowStatus(workflowStatus);
		helper.setManagementOnly(managementOnly);
		helper.setParentID(parentID);

		return helper;
	}

	public void executeScript(ScriptTaskBeanContext context) throws OperationException {
		LOG.info("IGWGOCComparisonScriptTask: entered");
		final Repository repository = context.getRepository();
		final Session session = context.getSession();
		if("Resubmitted".equalsIgnoreCase(workflowStatus) || "Terminated".equalsIgnoreCase(workflowStatus) ){
			final JMSMessageHelper helper = createJMSMessageHelper(repository, session,dataSpace,requestID, null );
			final String msg = helper.createMessageContent();
			LOG.debug("IGWGOCComparisonScriptTask: Sending message = " + msg);

			final String environment = new DSMTPropertyHelper().getProp(DSMTConstants.DSMT_ENVIRONMENT);

			if (!"local".equalsIgnoreCase(environment)) {

				helper.sendMessage(msg, queueName);
			}
		}
		else{
			StringTokenizer tokenizer= new StringTokenizer(listOfCDAndWFCreated, ",");
			Map<String, String> cdAndWFMap= new HashMap<String, String>();
			while (tokenizer.hasMoreTokens()){
				String cdAndWF= tokenizer.nextToken();
				cdAndWFMap.put(cdAndWF.substring(0,cdAndWF.indexOf("|")), cdAndWF.substring(cdAndWF.indexOf("|")+1,cdAndWF.length()));
				
			}
			for(String dataSpace: cdAndWFMap.keySet()){
				LOG.info("parent request id == "+requestID);
				final JMSMessageHelper helper = createJMSMessageHelper(repository, session,dataSpace,cdAndWFMap.get(dataSpace), requestID );
				final String msg = helper.createMessageContent();
				LOG.debug("IGWGOCComparisonScriptTask: Sending message = " + msg);
	
				final String environment = new DSMTPropertyHelper().getProp(DSMTConstants.DSMT_ENVIRONMENT);
	
				if (!"local".equalsIgnoreCase(environment)) {
	
					helper.sendMessage(msg, queueName);
				}
	
			}
		}
	}

	public void setMaintenanceType(String maintenanceType) {
		this.maintenanceType = maintenanceType;
	}

	public String getMaintenanceType() {
		return maintenanceType;
	}

	public void setFunctionalID(String functionalID) {
		this.functionalID = functionalID;
	}

	public String getFunctionalID() {
		return functionalID;
	}
}
