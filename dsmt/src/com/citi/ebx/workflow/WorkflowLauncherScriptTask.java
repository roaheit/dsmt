package com.citi.ebx.workflow;

import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ProcessInstanceKey;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;
import com.orchestranetworks.workflow.WorkflowEngine;

/*To launch a workflow from another workflow. 
 Input parameter workflowName which needs to be launched
 */

public class WorkflowLauncherScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();

	private String workFlowName; // eg "ReactivateGOC"

	public String getWorkFlowName() {
		return workFlowName;
	}

	public void setWorkFlowName(String workFlowName) {
		this.workFlowName = workFlowName;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context) throws OperationException {

		final Repository repo = context.getRepository();
		final Session session = context.getSession();

		WorkflowEngine wrkflowEngine = WorkflowEngine.getFromRepository(repo, session);
		LOG.info("Workflow - " + workFlowName + " is now being started.. ");
		ProcessLauncher launcher = wrkflowEngine.getProcessLauncher(PublishedProcessKey.forName(workFlowName));
		ProcessInstanceKey processKey  = launcher.launchProcess();
		LOG.info("Workflow - " + workFlowName + " has been started successfully.. Process ID " +  processKey);

	}

}
