package com.citi.ebx.workflow;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class UpdateAccMatrixFlagScriptTask extends ScriptTaskBean {

	
	final private Path lemMappingTable= AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping.getPathInSchema();
	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	private String lvID;
	
	
	@Override
	public void executeScript(ScriptTaskBeanContext ctx)
			throws OperationException {
		// TODO Auto-generated method stub
		LOG.info("UpdateAccMatrixFlagScriptTask :............................Start");
		LOG.info("setting flag value to Yes for LVID: "+lvID);

		
		Repository repo = ctx.getRepository();
		
		final AdaptationHome dataSpace = ctx.getRepository().lookupHome(
				HomeKey.forBranchName(AccountibilityMatrixConstants.DATASPACE_Accounting_Matrix));
		
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,AccountibilityMatrixConstants.DATASPACE_Accounting_Matrix, AccountibilityMatrixConstants.DATASET_Accounting_Matrix);
		AdaptationTable targetTable = metadataDataSet.getTable(lemMappingTable);
		
		final String predicate = AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID.format()
				 + "= '" + lvID + "'";
		
		RequestResult reqRes = targetTable.createRequestResult(predicate);
		try{
		
		if(reqRes.getSize() >0 ){
			for (Adaptation record; (record = reqRes.nextAdaptation()) != null;) {
			final UpdateRecordsProcedure updateProc = new UpdateRecordsProcedure(record);
			Utils.executeProcedure(updateProc, ctx.getSession(), dataSpace);
			}	
		}
		}catch(OperationException e){
			
			LOG.info("UpdateAccMatrixFlagScriptTask : exception in updating flag value "+e.getMessage());
				
		}
		
		LOG.info("UpdateAccMatrixFlagScriptTask :............................end");
	}
	
	private class UpdateRecordsProcedure implements Procedure {
		private Adaptation record;
		
		public UpdateRecordsProcedure(Adaptation record){
			this.record=record;
		}

		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			// TODO Auto-generated method stub
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			
			updateRecord(record, pContext);
			
			pContext.setTriggerActivation(true);
			pContext.setAllPrivileges(false);
			
		}
		
		private void updateRecord(final Adaptation record, final ProcedureContext pContext)
				throws OperationException {
			final ValueContextForUpdate vc = pContext.getContext(record.getAdaptationName());

			vc.setValue("Y", AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._Acc_Matrix_flag);
			
			pContext.doModifyContent(record, vc);
		}
		
	}


	public String getLvID() {
		return lvID;
	}


	public void setLvID(String lvID) {
		this.lvID = lvID;
	}
	

}
