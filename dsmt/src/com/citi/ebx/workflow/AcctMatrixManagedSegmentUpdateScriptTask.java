package com.citi.ebx.workflow;

import java.util.ArrayList;
import java.util.List;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;

public class AcctMatrixManagedSegmentUpdateScriptTask extends ScriptTaskBean {

	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private String dataSpace;
	private String dataSet;
	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getParentDataSpace() {
		return parentDataSpace;
	}

	public void setParentDataSpace(String parentDataSpace) {
		this.parentDataSpace = parentDataSpace;
	}

	private String parentDataSpace;
	
	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		Repository rep=context.getRepository();
		ArrayList<String> lem = new ArrayList<String>();
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(rep,
				AccountibilityMatrixConstants.DATASPACE_Accounting_Matrix,
				AccountibilityMatrixConstants.DATASET_Accounting_Matrix);
		AdaptationTable lvidLEMTargetTable = metadataDataSet
				.getTable(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping
						.getPathInSchema());
		final String predicateLVLEM = AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._Acc_Matrix_flag
				.format() + "= '" + "Y" + "'";
		RequestResult result = lvidLEMTargetTable
				.createRequestResult(predicateLVLEM);
		if (result.getSize() > 0) {
			for (Adaptation Recrd; (Recrd = result.nextAdaptation()) != null;) {
				lem.add(Recrd
						.getString(AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._LEM));
			}
		}
		
		final AdaptationHome dataSpaceRef = rep.lookupHome(HomeKey.forBranchName(dataSpace));
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
		
		final AdaptationTable NAMTable=dataSetRef.getTable(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM.getPathInSchema());
		final AdaptationTable initialNAMTable =GOCWorkflowUtils.getTableInInitialSnapshot(NAMTable);
		final DifferenceBetweenTables diff = DifferenceHelper.compareAdaptationTables(initialNAMTable, NAMTable, false);
		@SuppressWarnings("unchecked")
		List<ExtraOccurrenceOnRight> additionalRecords = diff.getExtraOccurrencesOnRight();
		@SuppressWarnings("unchecked")
		List<DifferenceBetweenOccurrences> updates = diff.getDeltaOccurrences();
		ExtraOccurrenceOnRight addition;
		DifferenceBetweenOccurrences delta;
		ArrayList<Adaptation> changedRecord=new ArrayList<Adaptation>();
		for(int i=0;i<updates.size();i++)
		{
			delta = updates.get(i);
			changedRecord.add(delta.getOccurrenceOnRight());	
		}
		for(int i=0;i<additionalRecords.size();i++)
		{
			addition = additionalRecords.get(i);
			if(! changedRecord.contains(addition)){
			changedRecord.add(addition.getExtraOccurrence());
			}
		}
		
		ArrayList<String> msid=new ArrayList<String>();
		for(Adaptation record : changedRecord )
		{
			if( ! msid.contains(record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_MAN_SEG_ID)))
			{
			msid.add(record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_MAN_SEG_ID));
			}
		}
		emailNotification(msid, lem);
	}
	public void emailNotification(ArrayList<String> msIDList,
			ArrayList<String> owner) {

		String msidvalues = "";
		for (String msid : msIDList) {
			msidvalues += msid;
			msidvalues+="\n";
		}
		String message = "Hello \n MSID" + msidvalues + " has been updated/Modified"
				+ " from Accountibility Matrix"
				+ "\nAccordingly Managed Segment hierarchy has been updated";
		String Subject = "LEM Notification : Managed Segment deleted from Accountibility Matrix";
		String receipents = "";

		for (String receipent : owner) {

			receipents += receipent + ";";
		}
		DSMTNotificationService.notifyEmailID(receipents, Subject, message,
				false);
	}

}

