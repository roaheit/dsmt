package com.citi.ebx.workflow;

import java.util.Date;
import java.util.List;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class DSMT2DataSpaceForMergeScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private String dataSpace;
	private String dataSet;
	private boolean writeErrorToContext = true;
	private String errorMessage;
	private String errorDetails;
	private String profile;
	private String requestID;
	
	private Adaptation dataSetRef;
	private String tablePath;
	private String tableName;
	private String _ACTION_REQ = "./ACTION_REQ";
	private Path effDateFieldPath = DSMTConstants.effDateFieldPath;
	private Path endDateFieldPath = Path.parse("./ENDDT");
	
	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * @return the tablePath
	 */
	public String getTablePath() {
		return tablePath;
	}

	/**
	 * @param tablePath the tablePath to set
	 */
	public void setTablePath(String tablePath) {
		this.tablePath = tablePath;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public boolean isWriteErrorToContext() {
		return writeErrorToContext;
	}

	public void setWriteErrorToContext(boolean writeErrorToContext) {
		this.writeErrorToContext = writeErrorToContext;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		LOG.info("DSMT2DataSpaceForMergeScriptTask executeScript started");
		
		LOG.info("Received approval response for dataSpace " + dataSpace + ",  workflow Request ID - " +requestID );

		try {
			final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
					HomeKey.forBranchName(dataSpace));
			if (dataSpaceRef == null) {
				throw OperationException.createError("Data space " + dataSpace + " can't be found.");
			}
			dataSetRef = dataSpaceRef.findAdaptationOrNull(
					AdaptationName.forName(dataSet));
			if (dataSetRef == null) {
				throw OperationException.createError("Data set " + dataSet + " can't be found in data space " + dataSpace + ".");
			}

			
			final AdaptationTable currentTable = dataSetRef.getTable(Path.parse(tablePath));
			
			// We want to compare against the initial version of the data in the data space.
			// Get the same table from the initial snapshot's data set
			final AdaptationTable initialTable = GOCWorkflowUtils.getTableInInitialSnapshot(currentTable);
			
			// Compare the table with the same table in the initial snapshot's data set
			final DifferenceBetweenTables diff = DifferenceHelper.compareAdaptationTables(initialTable, currentTable, false);
			
			// All extra occurrences on the right will be all records that exist in data space
			// that are not in initial snapshot, i.e. those added.
			@SuppressWarnings("unchecked")
			final List<ExtraOccurrenceOnRight> adds = diff.getExtraOccurrencesOnRight();
			@SuppressWarnings("unchecked")
			final List<DifferenceBetweenOccurrences> deltas = diff.getDeltaOccurrences();
			
			final UpdateRecordsProcedure updateProc = new UpdateRecordsProcedure(adds, deltas,initialTable,currentTable);
			Utils.executeProcedure(updateProc, context.getSession(), dataSpaceRef);

		} catch (OperationException ex) {
			LOG.error("DSMT2DataSpaceForMergeScriptTask: Error while preparing data space for merge.", ex);
			if (writeErrorToContext) {
				errorMessage = ex.getMessage();
				errorDetails = String.valueOf(ex); //TODO
			} else {
				throw ex;
			}
		}
		
		LOG.info("DSMT2DataSpaceForMergeScriptTask executeScript finished");
	}
	
	private class UpdateRecordsProcedure implements Procedure {
		private List<ExtraOccurrenceOnRight> adds;
		private List<DifferenceBetweenOccurrences> deltas;
		private AdaptationTable initialTable;
		private AdaptationTable currentTable;
		
		public UpdateRecordsProcedure(final List<ExtraOccurrenceOnRight> adds,
				final List<DifferenceBetweenOccurrences> deltas,AdaptationTable initialTable ,AdaptationTable currentTable) {
			this.adds = adds;
			this.deltas = deltas;
			this.initialTable = initialTable;
			this.currentTable = currentTable;
			
		}
		
		@Override
		public void execute(ProcedureContext pContext) throws Exception {
			pContext.setAllPrivileges(true);
			pContext.setTriggerActivation(false);
			
			for (ExtraOccurrenceOnRight add: adds) {
				final Adaptation record = add.getExtraOccurrence();
				updateRecord(record, pContext);
			}
			for (DifferenceBetweenOccurrences delta: deltas) {
				final Adaptation record = delta.getOccurrenceOnRight();

				copyRecord(record,initialTable,pContext,currentTable);
				
			}
						
			pContext.setTriggerActivation(true);
			pContext.setAllPrivileges(false);
		}
		
		private void updateRecord(final Adaptation record, final ProcedureContext pContext)
				throws OperationException {
			final ValueContextForUpdate vc = pContext.getContext(record.getAdaptationName());

			vc.setValue(DSMTConstants.ACTION_NO_CHANGE, Path.parse(_ACTION_REQ));
			
			pContext.doModifyContent(record, vc);
		}

		private void copyRecord(Adaptation rec, AdaptationTable initialtTable,ProcedureContext pContext,AdaptationTable currentTable) throws OperationException{
			
			try{
				
				Date contextDate = new Date();
				contextDate.setDate(01);
				LOG.info("DSMT2DataSpaceForMergeScriptTask copyRecord started ");
				
				pContext.setTriggerActivation(true);
				ValueContextForUpdate valueContextInsert = pContext.getContextForNewOccurrence(rec,currentTable);
				valueContextInsert.setValue(contextDate , effDateFieldPath);
				valueContextInsert.setValue(DSMTConstants.ACTION_NO_CHANGE, Path.parse(_ACTION_REQ));
				pContext.doCreateOccurrence(valueContextInsert,  currentTable);
				
				
				PrimaryKey key = rec.getOccurrencePrimaryKey();
				// in child dataSpace
				Adaptation oldRec =initialtTable.lookupAdaptationByPrimaryKey(key);// get the record from parentdataspace
				ValueContextForUpdate valueContextUpdate = pContext.getContextForNewOccurrence(oldRec,currentTable); // record that we have to update
				// update the value
				valueContextUpdate.setValue(rec.get(Path.parse("/EFFDT")) , effDateFieldPath);
				valueContextUpdate.setValue(DSMTConstants.ACTION_NO_CHANGE, Path.parse(_ACTION_REQ));
				valueContextUpdate.setValue(new Date() , endDateFieldPath);
				
				pContext.doModifyContent(rec,  valueContextUpdate);
								 
			}
			catch( Exception e){
				e.getStackTrace();
			}
		}
		
	}
	
}
