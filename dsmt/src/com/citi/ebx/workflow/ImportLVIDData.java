/**
 * 
 */
package com.citi.ebx.workflow;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ProcessInstanceKey;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.WorkflowEngine;

/**
 * @author rk00242
 *
 */
public class ImportLVIDData {
	
	final private static Path lemMappingTable= AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping.getPathInSchema();
	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	

	/* (non-Javadoc)
	 * @see com.orchestranetworks.workflow.ScriptTaskBean#executeScript(com.orchestranetworks.workflow.ScriptTaskBeanContext)
	 */

	public static void importData( Repository repo,Session session,String lvid)
			throws OperationException {
		LOG.info("ImportLVIDData: ..................start");


		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(repo,AccountibilityMatrixConstants.DATASPACE_Accounting_Matrix, AccountibilityMatrixConstants.DATASET_Accounting_Matrix);
		AdaptationTable targetTable = metadataDataSet.getTable(lemMappingTable);
		boolean importData = false;
		
		final String predicate = AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._C_DSMT_LVID.format()
				 + "= '" + lvid + "'";
		final String predicate1 = AccountibilityMatrixPaths._ACC_MAT_LVID_LEM_Mapping._Acc_Matrix_flag.format()
				 + "= '" + "Y" + "'";
		
	
		RequestResult reqRes = targetTable.createRequestResult(predicate+"and"+predicate1);
		if(reqRes.getSize() >0 ){
		
				importData= true;
		}

		if(!importData){
			launchWorkFlow(repo,session,lvid);
			
		}
		
		LOG.info("ImportLVIDData: ..................end");
		

	}
	
	
	public static void launchWorkFlow(Repository rp,Session session,String lvid){
		String lvID="lvID";
		
		try{
			
			WorkflowEngine wrkflowEngine = WorkflowEngine.getFromRepository(rp, session);
			ProcessLauncher launcher = wrkflowEngine.getProcessLauncher(PublishedProcessKey.forName("ACCT_MATRIX_DATALOAD"));
			launcher.setInputParameter(lvID, lvid);
		
			ProcessInstanceKey processKey  = launcher.launchProcess();
			LOG.info("workflow launched for Accounting Matrix");
		
		}catch(OperationException e){
			e.getStackTrace();
			LOG.info("Exception occurred while launching WF in ImportLVIDData " +e.getMessage());
			e.printStackTrace();			
			
			
		}
	}

}
