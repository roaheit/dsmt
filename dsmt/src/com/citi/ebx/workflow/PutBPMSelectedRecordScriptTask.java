package com.citi.ebx.workflow;

import com.citi.ebx.dsmt.jms.JMSMessageHelper;
import com.citi.ebx.dsmt.jms.JMSSelectedRecordMessageHelper;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

public abstract class PutBPMSelectedRecordScriptTask extends JMSMessageScriptTask {
	private String bpmProcessID;
	private String dataSpace;
	private String dataSet;
	private String recordPath;
	private String user;
	
	public String getBpmProcessID() {
		return bpmProcessID;
	}

	public void setBpmProcessID(String bpmProcessID) {
		this.bpmProcessID = bpmProcessID;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getRecordPath() {
		return recordPath;
	}

	public void setRecordPath(String recordPath) {
		this.recordPath = recordPath;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@Override
	protected JMSMessageHelper createJMSMessageHelper(
			Repository repository, Session session)
			throws OperationException {
		JMSSelectedRecordMessageHelper helper = new JMSSelectedRecordMessageHelper();
		helper.setRepository(repository);
		helper.setSession(session);
		helper.setLog(LoggingCategory.getWorkflow());
		
		helper.setQueueName(queueName);
		helper.setBpmProcessID(bpmProcessID);
		helper.setDataSpace(dataSpace);
		helper.setDataSet(dataSet);
		helper.setRecordPath(recordPath);
		helper.setUser(user);
		return helper;
	}
}
