package com.citi.ebx.workflow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.citi.ebx.dsmt.util.ApprovalMonitoringTableBean;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.util.InsertApprovalWFMonitoringdata;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.comparison.DifferenceBetweenInstances;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class InsertApprovalWFReportingData extends ScriptTaskBean {

	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();

	private String dataSpace;
	private String dataSet;
	private int requestID;
	private String tableID;
	private String allocatedUser;
	private String operation;
	private String userComment;
	private String tableUpdated;
	private int recordCount = 0;
	private String actionRequried;
	int addition;
	int updation;
	int reactivation;
	int deactivation;
	private int ApproverLevel;
	private static String approvers;
	private static int levels;
	private static String status;
	private static String allocatedApprover;
	private String tablePath;
	private String _ACTION_REQ = "./ACTION_REQ";


	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		LOG.info("InsertGOCReportingScriptTask executeScript started for dataSpace "
				+ dataSpace);
		LOG.debug("InsertGOCReportingScriptTask executeScript operation "
				+ operation);

		if (DSMTConstants.INSERT.equals(operation)) {
			insertMonitoringdata(context);
		} else {
			updateMonitoringdata(context);
		}

		LOG.debug("InsertGOCReportingScriptTask executeScript finished");

	}

	private void insertMonitoringdata(ScriptTaskBeanContext context) {

		LOG.info("InsertGOCReportingScriptTask insertMonitoringdata started for dataSpace");

		try {
			ApprovalMonitoringTableBean reportingTableBean = new ApprovalMonitoringTableBean();
			reportingTableBean.setChildDataSpaceID(dataSpace);
			reportingTableBean.setDataSet(dataSet);
			reportingTableBean.setOperation(operation);
			LOG.debug("requestID >> " + requestID);
			reportingTableBean.setRequestID(requestID);
			reportingTableBean
					.setRequestType(GOCConstants.DSMT_WORKFLOW_PUBLICATION);
			if (allocatedUser != null) {
				reportingTableBean.setRequestBy(allocatedUser.replace("U", ""));
			}
			reportingTableBean.setRequestStatus(GOCConstants.CREATED);
			reportingTableBean.setRequestorComment(userComment);
			reportingTableBean.setTableUpdated(tableUpdated);
			reportingTableBean.setTablePath(tablePath);

			InsertApprovalWFMonitoringdata insertApprovalWFMonitoringdata = new InsertApprovalWFMonitoringdata(
					reportingTableBean, context.getRepository(), tableID,
					tableUpdated);

			Utils.executeProcedure(
					insertApprovalWFMonitoringdata,
					context.getSession(),
					context.getRepository().lookupHome(
							HomeKey.forBranchName("DSMT_WF")));

		} catch (Exception ex) {
			LOG.error(
					"InsertGOCReportingScriptTask: Error while preparing data space for merge.",
					ex);

		}

	}

	private void updateMonitoringdata(ScriptTaskBeanContext context) {

		LOG.info("InsertGOCReportingScriptTask updateMonitoringdata started for dataSpace for requestID "
				+ requestID);

		try {
			ApprovalMonitoringTableBean reportingTableBean = new ApprovalMonitoringTableBean();
			
			reportingTableBean.setRequestStatus(operation);
			reportingTableBean.setAllocatedApprover(allocatedApprover);
			reportingTableBean.setApproverLevel(levels);
			reportingTableBean.setApprover(approvers);

			if (GOCConstants.SUBMITTED.equals(operation)) {

				LOG.info("Inside submit");

				final AdaptationHome dataSpaceRef = context.getRepository()
						.lookupHome(HomeKey.forBranchName(dataSpace));
				final Adaptation dataSetRef = dataSpaceRef
						.findAdaptationOrNull(AdaptationName.forName(dataSet));
				final AdaptationHome initialSnapshot = dataSpaceRef.getParent();
				final Adaptation initialDataSet = initialSnapshot
						.findAdaptationOrNull(dataSetRef.getAdaptationName());
				final DifferenceBetweenInstances dataSetDiffs = DifferenceHelper
						.compareInstances(initialDataSet, dataSetRef, false);
				int deltaSize = dataSetDiffs.getDeltaTablesSize();

				LOG.info("delta size : " + deltaSize);

				final AdaptationTable cntryTable = dataSetRef.getTable(Path
						.parse(tablePath));

				final AdaptationTable initialTable = getTableInInitialSnapshot(cntryTable);

				final DifferenceBetweenTables diff = DifferenceHelper
						.compareAdaptationTables(initialTable, cntryTable,
								false);
				LOG.info(" total diff " + diff.getExtraOccurrencesOnRightSize());
				Map<String, Integer> map = new HashMap<String, Integer>();
				@SuppressWarnings("unchecked")
				List<DifferenceBetweenOccurrences> delta = diff
						.getDeltaOccurrences();
				Integer recCount =0;
				LOG.info("printing  delta " + delta);
				LOG.info("printing  delta size  " + delta.size());
				for (DifferenceBetweenOccurrences add : delta) {
					final Adaptation record = add.getOccurrenceOnRight();
					Adaptation firstRecord = null;
					if (firstRecord == null) {
						firstRecord = record;
					}
					
					recCount = recCount+1;
					String code = record.getString(Path
							.parse(_ACTION_REQ));
					Integer count = map.get(code);
					map.put(code, (count == null) ? 1 : count + 1);

				}

				@SuppressWarnings("unchecked")
				List<ExtraOccurrenceOnRight> adds = diff
						.getExtraOccurrencesOnRight();
				LOG.info("delta size" + adds);

				for (ExtraOccurrenceOnRight add : adds) {
					final Adaptation records = add.getExtraOccurrence();
					Adaptation firstRecords = null;
					if (firstRecords == null) {
						firstRecords = records;
					}
					recCount = recCount+1;
					
					String code = firstRecords.getString(Path
							.parse(_ACTION_REQ));
					Integer count = map.get(code);
					map.put(code, (count == null) ? 1 : count + 1);

				}
				
				calculateCount(map);
				
				reportingTableBean.setOperation(operation);
				reportingTableBean.setTableUpdated(tableUpdated);
				reportingTableBean.setTablePath(tablePath);
				reportingTableBean.setRecordCount(String.valueOf(recCount));
				reportingTableBean.setAddition(addition);
				reportingTableBean.setUpdation(updation);
				reportingTableBean.setReactivation(reactivation);
				reportingTableBean.setDeactivation(deactivation);
				if (allocatedUser != null) {
					reportingTableBean.setRequestBy(allocatedUser.replace("U",
							""));
				}
			}
/*
			else if (DSMTConstants.ASSIGN.equals(operation)) {
				LOG.info("Inside Assign");
			//	reportingTableBean.setApproverLevel(levels);
			//	reportingTableBean.setApprover(approvers);
			//	reportingTableBean.setAllocatedApprover(allocatedApprover);
			}
*/
			else if (DSMTConstants.REJECT.equals(operation)) {
				LOG.info("Inside Rejected");
				reportingTableBean.setRequestStatus(GOCConstants.REJECTED);
				reportingTableBean.setTableUpdated(tableUpdated);
			//	reportingTableBean.setAllocatedApprover(allocatedApprover);
			//	reportingTableBean.setApproverLevel(levels);

			}
/*
			else if (DSMTConstants.COMPLETED.equals(operation)) {
				LOG.info("Inside completed");
			//	reportingTableBean.setAllocatedApprover(allocatedApprover);
			//	reportingTableBean.setApproverLevel(levels);
			}*/

			else if (DSMTConstants.PENDING.equals(operation)) {
				LOG.info("Inside pending");
				reportingTableBean.setRequestStatus(status);
				reportingTableBean.setApproverLevel(levels);
				/*reportingTableBean.setApprover(approvers);
				reportingTableBean.setAllocatedApprover(allocatedApprover);*/
			}

			else if (DSMTConstants.REQ_REJECT.equals(operation)) {
				LOG.info("Inside Requestor Reject");
				reportingTableBean.setApproverLevel(0);
			}

			
			reportingTableBean.setRequestID(requestID);
			reportingTableBean.setRequestorComment(userComment);

			InsertApprovalWFMonitoringdata insertApprovalWFMonitoringdata = new InsertApprovalWFMonitoringdata(
					reportingTableBean, context.getRepository(), tableID,
					tableUpdated);
			Utils.executeProcedure(
					insertApprovalWFMonitoringdata,
					context.getSession(),
					context.getRepository().lookupHome(
							HomeKey.forBranchName("DSMT_WF")));

		} catch (Exception ex) {
			LOG.error(
					"InsertApprovalWFReportingData: Error while preparing data space for merge.",
					ex);
		}
	}

	public static AdaptationTable getTableInInitialSnapshot(
			final AdaptationTable table) {
		final Adaptation dataSet = table.getContainerAdaptation();
		final AdaptationHome dataSpace = dataSet.getHome();
		final AdaptationHome initialSnapshot = dataSpace.getParent();
		final Adaptation initialDataSet = initialSnapshot
				.findAdaptationOrNull(dataSet.getAdaptationName());
		return initialDataSet.getTable(table.getTablePath());
	}

	public void updateApprover(String appr, int count) {
		LOG.info("update approver count  " + count);
		LOG.info("checkinf approver in reporting " + appr);
		approvers = appr;
		levels = count;
		approvers = approvers.replaceAll(",$", "");

	}
	//update status as Pending
	public void updatePendingStatus(String reqStatus) {
		status = reqStatus;
	}

	public void updateAllocatedUser(String user) {
		LOG.info("checking allocated user param" + user);
		allocatedApprover = user;
	}
	
	private void calculateCount(Map<String, Integer> map){
		if (map.get(DSMTConstants.AR_ADDITION) == null) {
			addition = 0;
		} else {
			addition = map.get(DSMTConstants.AR_ADDITION);
		}
		
		if (map.get(DSMTConstants.AR_DEACTIVATION) == null) {
			deactivation = 0;
		} else
			deactivation = map.get(DSMTConstants.AR_DEACTIVATION);
		if (map.get(DSMTConstants.AR_REACTIVATION) == null) {
			reactivation = 0;
		} else
			reactivation = map.get(DSMTConstants.AR_REACTIVATION);
		if (map.get(DSMTConstants.AR_UPDATION) == null) {
			updation = 0;
		} else
			updation = map.get(DSMTConstants.AR_UPDATION);

	}
	

	public String getTablePath() {
		return tablePath;
	}

	public void setTablePath(String tablePath) {
		this.tablePath = tablePath;
	}

	/**
	 * @return the levels
	 */
	public int getLevels() {
		return levels;
	}

	/**
	 * @param levels
	 *            the levels to set
	 */
	public void setLevels(int levels) {
		this.levels = levels;
	}

	public int getApproverLevel() {
		return ApproverLevel;
	}

	public void setApproverLevel(int approverLevel) {
		ApproverLevel = approverLevel;
	}

	public String getApprovers() {
		return approvers;
	}

	public void setApprovers(String approvers) {
		this.approvers = approvers;
	}

	public int getAddition() {
		return addition;
	}

	public void setAddition(int addition) {
		this.addition = addition;
	}

	public int getUpdation() {
		return updation;
	}

	public void setUpdation(int updation) {
		this.updation = updation;
	}

	public int getReactivation() {
		return reactivation;
	}

	public void setReactivation(int reactivation) {
		this.reactivation = reactivation;
	}

	public int getDeactivation() {
		return deactivation;
	}

	public void setDeactivation(int deactivation) {
		this.deactivation = deactivation;
	}

	public String getActionRequried() {
		return actionRequried;
	}

	public void setActionRequried(String actionRequried) {
		this.actionRequried = actionRequried;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public int getRequestID() {
		return requestID;
	}

	public void setRequestID(int requestID) {
		this.requestID = requestID;
	}

	public String getTableID() {
		return tableID;
	}

	public void setTableID(String tableID) {
		this.tableID = tableID;
	}

	public String getAllocatedUser() {
		return allocatedUser;
	}

	public void setAllocatedUser(String allocatedUser) {
		this.allocatedUser = allocatedUser;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public int getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}

	public String getTableUpdated() {
		return tableUpdated;
	}

	public void setTableUpdated(String tableUpdated) {
		this.tableUpdated = tableUpdated;
	}

	
}
