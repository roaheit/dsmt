package com.citi.ebx.workflow;

import java.util.HashMap;
import java.util.Map;

import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.util.GOCWorkflowUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class SetSIDScriptTask extends ScriptTaskBean {

	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	
	private String dataSpace;
	private String dataSet;
	private String sid;
	private String maintenanceType;
	private String functionalID;

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getMaintenanceType() {
		return maintenanceType;
	}

	public void setMaintenanceType(String maintenanceType) {
		this.maintenanceType = maintenanceType;
	}

	public String getFunctionalID() {
		return functionalID;
	}

	public void setFunctionalID(String functionalID) {
		this.functionalID = functionalID;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
				HomeKey.forBranchName(dataSpace));
		if (dataSpaceRef == null) {
			throw OperationException.createError("Data space " + dataSpace + " can't be found.");
		}
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(
				AdaptationName.forName(dataSet));
		if (dataSetRef == null) {
			throw OperationException.createError("Data set " + dataSet + " can't be found in data space " + dataSpace + ".");
		}
		
		LOG.info("SID entered is " + sid + ", MaintenanceType is " + maintenanceType + " and functionalID is " + functionalID);
		
		final Procedure setSIDProc = new Procedure() {
			@Override
			public void execute(ProcedureContext pContext) throws Exception {
				
				Map<String, String> workflowParameterMap = new HashMap<String, String>();
				workflowParameterMap.put(GOCConstants.WF_PARAM_SID, sid);
				workflowParameterMap.put(GOCConstants.WF_PARAM_MAINTENANCE_TYPE, maintenanceType);
				workflowParameterMap.put(GOCConstants.WF_PARAM_FUNCTION_ID, functionalID);
				
				GOCWorkflowUtils.setWorkflowParameters(workflowParameterMap, dataSetRef, pContext);
				
			}
		};
		
		Utils.executeProcedure(setSIDProc, context.getSession(), dataSpaceRef);
	}
}
