package com.citi.ebx.workflow;


import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Profile;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class WorkflowMailNotification extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private String parentDataSpace;
	private String dataSpace;
	private String requestID;
	private String profile;
	
	public String getParentDataSpace() {
		return parentDataSpace;
	}
	public void setParentDataSpace(String parentDataSpace) {
		this.parentDataSpace = parentDataSpace;
	}
	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getDataSpace() {
		return dataSpace;
	}
	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}
	
	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		final Profile profileRef = Profile.parse(profile);
		UserReference userRef = (UserReference) profileRef;
		String requestor=userRef.getUserId();
		String Subject = "Workflow Request Approved";
		String message="Hello, \n\n Changes made to DataSpace "+dataSpace+"has been merged to Parent DataSpace "+parentDataSpace+
				" as part of Workflow Request ID " + requestID+"\n Thank You";
		DSMTNotificationService.notifyEmailID(requestor, Subject, message, true);
	}
}