package com.citi.ebx.workflow;

import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.naming.NamingException;

import com.citi.ebx.dsmt.jms.connector.CitiJMSConnector;
import com.citi.ebx.dsmt.jms.connector.impl.CitiJMSConnectorImpl;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class FinishGOCWorkflowAfterCorrectionScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	
	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		LOG.info("FinishGOCWorkflowAfterCorrectionScriptTask executeScript started");
		
		final String userID = context.getSession().getUserReference().getUserId();
		
		sendMessage(getXMLMessage(userID));
		
		LOG.info("FinishGOCWorkflowAfterCorrectionScriptTask executeScript finished");
	}
	
	
	public void sendMessage(String msg) throws OperationException {
		
		final Map<String, String> jmsMap = createJMSMap(dataSpace);
		
		final String queueCF = propertyHelper.getProp("queue.connection.factory");
		
		final String sendQueue = propertyHelper.getProp("receive.queue.jndi");
		
		CitiJMSConnector connector = new CitiJMSConnectorImpl();
		
		try {
			log.info("FinishGOCWorkflowAfterCorrectionScriptTask: {jmsMap = " + jmsMap + "}, { queueCF = " +  queueCF + "}, {sendQueue = " + sendQueue + "}");
			connector.sendMessage(msg, jmsMap, queueCF, sendQueue);
		} catch (NamingException e) {
			log.error("FinishGOCWorkflowAfterCorrectionScriptTask: Naming Exception " + e + ", Exception Message " + e.getMessage());
			throw OperationException.createError(e);
		} catch (JMSException e) {
			log.error("FinishGOCWorkflowAfterCorrectionScriptTask: JMSException Exception " + e + ", Exception Message " + e.getMessage());
			throw OperationException.createError(e);
		}
	}
	
	
	protected Map<String, String> createJMSMap(final String dataSpace) {
		
		final HashMap<String, String> jmsMap = new HashMap<String, String>();
		
		jmsMap.put("JMSCorrelationID", dataSpace);
		
		return jmsMap;
	}
	
	
	private String getXMLMessage(final String userID){
		
		return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:ebx-schemas:dataservices_1.0\" xmlns:sec=\"http://schemas.xmlsoap.org/ws/2002/04/secext\">    <soapenv:Header>       <urn:session/>       <sec:Security>          <UsernameToken>             <Username>wfadmin</Username>             <Password>wfadmin123</Password>          </UsernameToken>       </sec:Security>    </soapenv:Header>    <soapenv:Body>       <urn:workflowProcessInstanceStart>          <publishedProcessKey>MergeGOCDataSpace</publishedProcessKey>          <documentation>             <locale>Locale</locale>             <label>Updated Manually by DS Operations : SOEID "  + userID +  " </label>          </documentation>          <parameters>                          <parameter>                <name>dataSpace</name>                <value>"
				+ dataSpace
				+ "</value>             </parameter>             <parameter>                <name>dataSet</name>                <value>"
				+ dataSet
				+ "</value>             </parameter>                         <parameter>                <name>requestID</name>                <value>"
				+ requestID
				+ "</value>             </parameter>  			<parameter>                <name>gridRoutingID</name>                <value>"
				+ gridRoutingID
				+ "</value>             </parameter>               <parameter>                <name>approverList</name>                <value>"
				+ approverList
				+ "</value>             </parameter>                        </parameters>       </urn:workflowProcessInstanceStart>    </soapenv:Body> </soapenv:Envelope>";
	}
	
	
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();
	protected LoggingCategory log = LoggingCategory.getWorkflow();


	private String dataSpace;
	private String dataSet;
	private boolean writeErrorToContext = true;
	private String errorMessage;
	private String errorDetails;
	
	private String requestID;
	private String gridRoutingID;
	private String approverList;
	
	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public boolean isWriteErrorToContext() {
		return writeErrorToContext;
	}

	public void setWriteErrorToContext(boolean writeErrorToContext) {
		this.writeErrorToContext = writeErrorToContext;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getGridRoutingID() {
		return gridRoutingID;
	}

	public void setGridRoutingID(String gridRoutingID) {
		this.gridRoutingID = gridRoutingID;
	}

	public String getApproverList() {
		return approverList;
	}

	public void setApproverList(String approverList) {
		this.approverList = approverList;
	}

}
