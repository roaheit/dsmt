package com.citi.ebx.workflow.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.util.EBXUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ServiceContext;

public class OpenBPMCoachService extends HttpServlet {
	private static final String UNDERSCORE_ = "_";
	private static final long serialVersionUID = 1L;
	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	
	private BufferedWriter out;
	protected Repository repository;
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		out = new BufferedWriter(response.getWriter());
		final ServiceContext sContext = ServiceContext.getServiceContext(request);
		final AdaptationHome dataSpace = sContext.getCurrentHome();
		Path tablepath =  sContext.getCurrentPathInAdaptation();
		
		String tableName = sContext.getCurrentAdaptation().getLabelOrName(Locale.US);
	//	String requestID = dataSpace.getKey().getName();
		
		/** Gets trackingInfo as "RequestChanges_100200PID18" where 100200PID18 is a sample request ID returned by CWM. */
		 
		final String trackingInfo = sContext.getSession().getTrackingInfo();
		final String[] trackingInfos = trackingInfo.split(UNDERSCORE_);
		
		/** Split the tracking Info and parse the request ID from the 2nd element */
		String workflowID =  trackingInfos[1];// new DSMTWorkflowUtil().generateWorkflowID(requestID);
		/*if(tableName.equals("GOC") && tablepath.format().contains(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD.format())){
			workflowID = 	workflowID+"GPID18";
		}*/
		if (tableName.equals("GOC") && tablepath.format().contentEquals("/")) {
			if (workflowID.contains("PID18")) {
				try {
					repository = dataSpace.getRepository();
					Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(
							repository, DSMTConstants.GOC_WF_DATASPACE,
							DSMTConstants.GOC_WF_DATASET);

					String predicate = GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL._WFREQ_ID
							.format() + " = '" + workflowID + "'";
					LOG.info("predicate for request id for doc upload  ====  " + predicate);
					AdaptationTable IGW_DTL = metadataDataSet.getTable(GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL.getPathInSchema());
					RequestResult dtlRec = IGW_DTL.createRequestResult(predicate);
					if (!dtlRec.isEmpty()) {
						LOG.info("fetched detail record for doc uplaod");
						Adaptation detailRec = dtlRec.nextAdaptation();
						String parentID = detailRec.getString((GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL._REQUEST_ID));
						LOG.info("parent request id for doc upload ==== " + parentID);

						workflowID = parentID + "GPID18";

					}

					else {

					}
				} catch (OperationException e) {
					LOG.error("error occured while fetching record from IGW reporting table");					
					e.printStackTrace();
				}
			} else {
				workflowID = workflowID + "GPID18";
			}

		}
		
		
		LOG.info("workflow ID in OpenBPMCoachService =" + workflowID);
		
		final String url = createURL(dataSpace, workflowID);
		
		writeln("<script>");
		
		writeln("var popup = window.open('" + url + "', '_blank', 'width=800,height=600');");
		writeln("popup.focus();");
		writeln("window.location.href='" + sContext.getURLForEndingService() + "';");
		writeln("</script>");
		out.flush();
	}
	
	private void writeln(final String line) throws IOException {
		out.write(line);
		out.newLine();
	}
	
	// You probably would want more complicated logic around this. Maybe pull base url from a properties file?
	// Or pull service name, etc from params into the workflow which then can get passed into this service...
	// I'm ignoring that for now.
	private static String createURL(final AdaptationHome dataSpace, final String workflowID) {
	
		// This assumes you're using the data space name for the workflow ID
		// final String bpmWorkflowId = dataSpace.getKey().getName();
		
		String url = DSMTConstants.propertyHelper.getProp("bpm.upload.coach.https.url") + workflowID;
		
		LOG.info("BPM upload coach service HTTPS URL = " + url);
		
		return url;
		
		// 
	}
	
	
	
	
	
}
