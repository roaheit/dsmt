package com.citi.ebx.workflow;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.dsmt.util.DSMTUtils;
import com.citi.ebx.dsmt.util.IGWBPMWSUtil;
import com.citi.ebx.dsmt.util.MonitoringTableBean;
import com.citi.ebx.goc.GOCConstants;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.goc.path.GOCWFPaths;
import com.citi.ebx.goc.util.IGWGOCWorkflowUtils;
import com.citi.ebx.util.IGWSaveUtil;
import com.citi.ebx.util.InsertMonitoringdata;
import com.citi.ebx.util.Utils;
import com.citi.ebx.workflow.vo.IGWSplitBean;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeCreationSpec;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Profile;
import com.orchestranetworks.service.comparison.DifferenceBetweenInstances;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class IGWSplitGOCRequestScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	public static final String FILE_SEPERATOR = ",";
	private String dataSpace;
	private String dataSet;
	private String requestID;
	private String splitFileLocation;
	private boolean writeFiles;
	private String listOfCDAndWFCreated;
	private String profile;
	private String parentDataSpace;
	private String allocatedUser;
	private String tableID;
	private String newGOCCount;
	private String totalMod;
	private String updatedGOCCount;
	private String newNonGOCCount;
	private String updatedNonGOCCount;
	
	

	public String getNewGOCCount() {
		return newGOCCount;
	}

	public void setNewGOCCount(String newGOCCount) {
		this.newGOCCount = newGOCCount;
	}

	public String getTotalMod() {
		return totalMod;
	}

	public void setTotalMod(String totalMod) {
		this.totalMod = totalMod;
	}

	public String getUpdatedGOCCount() {
		return updatedGOCCount;
	}

	public void setUpdatedGOCCount(String updatedGOCCount) {
		this.updatedGOCCount = updatedGOCCount;
	}

	public String getNewNonGOCCount() {
		return newNonGOCCount;
	}

	public void setNewNonGOCCount(String newNonGOCCount) {
		this.newNonGOCCount = newNonGOCCount;
	}

	

	public String getUpdatedNonGOCCount() {
		return updatedNonGOCCount;
	}

	public void setUpdatedNonGOCCount(String updatedNonGOCCount) {
		this.updatedNonGOCCount = updatedNonGOCCount;
	}

	public String getTableID() {
		return tableID;
	}

	public void setTableID(String tableID) {
		this.tableID = tableID;
	}

	public String getAllocatedUser() {
		return allocatedUser;
	}

	public void setAllocatedUser(String allocatedUser) {
		this.allocatedUser = allocatedUser;
	}

	public String getParentDataSpace() {
		return parentDataSpace;
	}

	public void setParentDataSpace(String parentDataSpace) {
		this.parentDataSpace = parentDataSpace;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getListOfCDAndWFCreated() {
		return listOfCDAndWFCreated;
	}

	public void setListOfCDAndWFCreated(String listOfCDAndWFCreated) {
		this.listOfCDAndWFCreated = listOfCDAndWFCreated;
	}

	public boolean isWriteFiles() {
		return writeFiles;
	}

	public void setWriteFiles(boolean writeFiles) {
		this.writeFiles = writeFiles;
	}

	public String getSplitFileLocation() {
		return splitFileLocation;
	}

	public void setSplitFileLocation(String splitFileLocation) {
		this.splitFileLocation = splitFileLocation;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context) throws OperationException {
		LOG.info("IGWSplitGOCRequestScriptTask: Params:-DataSpace :" + dataSpace + " DataSet :" + dataSet + " Profile :"
				+ profile + " requestID :" + requestID + " splitFileLocation :" + splitFileLocation + " writeFiles:"
				+ writeFiles + " parentDataSpace:" + parentDataSpace + " allocatedUser:" + allocatedUser + " tableID:"
				+ tableID);
		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(HomeKey.forBranchName(dataSpace));
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
		final AdaptationHome initialSnapshot = dataSpaceRef.getParent();
		final Adaptation initialDataSet = initialSnapshot.findAdaptationOrNull(dataSetRef.getAdaptationName());
		final DifferenceBetweenInstances dataSetDiffs = DifferenceHelper.compareInstances(initialDataSet, dataSetRef, false);
		Map<String, ArrayList<IGWSplitBean>> routingIdMap = splitGOCRequestBasedOnRoutingIds(dataSetDiffs);
		segregateFiles(routingIdMap, context);
		LOG.info("IGWSplitGOCRequestScriptTask:executeScript ends");

	}

	private void segregateFiles(Map<String, ArrayList<IGWSplitBean>> routingIdMap, ScriptTaskBeanContext context) {
		LOG.info("IGWSplitGOCRequestScriptTask:segregateFiles started");

		Map<Path, Object> schemaPaths = prepareGOCPathMap();
		String createdDataSpacesNames = "";
		int i = 0;
		boolean isSingleRecord = false;
		if (routingIdMap.size() == 1) {
			isSingleRecord = true;
		}
		for (String gridRoutingId : routingIdMap.keySet()) {
			LOG.info("IGWSplitGOCRequestScriptTask:Started Processing for Grid Routing Id:" + gridRoutingId);
			List<String> recordsToWrite = new ArrayList<String>();

			try {

				final String environment = new DSMTPropertyHelper().getProp(DSMTConstants.DSMT_ENVIRONMENT);
				String goc_workflow_requestId = "1234" + i;
				
					LOG.info("IGWSplitGOCRequestScriptTask:About to call CWM for WF Id  for grid routing id:" + gridRoutingId);
					try {
						goc_workflow_requestId = new IGWBPMWSUtil().getRequestIDFromBPM(profile.replace("U", ""));
					} catch (Exception e) {
						
						final String message = "The GOC workflow request " + requestID + "could not comunicate with CWM";
						DSMTNotificationService.notifyEmailID(allocatedUser, DSMTUtils.getSubject(allocatedUser, requestID) , message, true);
						LOG.info("errorFound in fetching CWM requestID for : "+ requestID );
						throw new OperationException("Coudnt get GOC workflow ID", e) {

							/**
							 * 
							 */
							private static final long serialVersionUID = -5129151992631597747L;

						};
					}
					LOG.info("IGWSplitGOCRequestScriptTask:Received WF Id from CWM: " + goc_workflow_requestId
							+ " for grid routing id:" + gridRoutingId);
				

				String createdDataSpace = null;
				if (isSingleRecord)
					createdDataSpace = dataSpace;
				else
					createdDataSpace = createChildDataSpace(context);

				if ("".equals(createdDataSpacesNames)) {
					createdDataSpacesNames = createdDataSpace + "|" + goc_workflow_requestId;

				} else {
					createdDataSpacesNames = createdDataSpacesNames + "," + createdDataSpace + "|" + goc_workflow_requestId;
				}

				LOG.info("IGWSplitGOCRequestScriptTask:GOC_WF_RequestID :" + goc_workflow_requestId + ", childDataSpace:"
						+ createdDataSpace + " for routing ID" + gridRoutingId);
				String fileName = splitFileLocation + "/" + requestID + "_" + i + ".csv";
				recordsToWrite.add(fileName + FILE_SEPERATOR + goc_workflow_requestId + FILE_SEPERATOR + createdDataSpace);

				// Create input map for saving in audit tables
				Map<Path, Object> inputMap = new HashMap<Path, Object>();
				inputMap.put(GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL._REQUEST_ID, requestID);
				inputMap.put(GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL._WFREQ_ID, goc_workflow_requestId);
				inputMap.put(GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL._CHILD_DS, createdDataSpace);
				inputMap.put(GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL._FILENAME, fileName);

				// auditWFIdAndCreatedDS
				auditWFIdAndCreatedDS(context, inputMap);
				// createORUpdateRecordsInCDCreated
				if (!isSingleRecord)
					createORUpdateRecordsInCDCreated(routingIdMap.get(gridRoutingId), createdDataSpace, context, schemaPaths,
							recordsToWrite);
				insertMonitoringdata(context, createdDataSpace, goc_workflow_requestId);

				if (writeFiles) {
					LOG.info("IGWSplitGOCRequestScriptTask:About to write file:" + fileName);
					File file = new File(fileName);
					FileUtils.writeLines(file, recordsToWrite);
					LOG.info("IGWSplitGOCRequestScriptTask:File Writing done for fileName" + fileName);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i++;
		}
		listOfCDAndWFCreated = createdDataSpacesNames;
		LOG.info("listOfChildDSpaceCreated :" + listOfCDAndWFCreated);
		LOG.info("IGWSplitGOCRequestScriptTask:segregateFiles ends");
	}

	private void auditWFIdAndCreatedDS(ScriptTaskBeanContext context, Map<Path, Object> inputMap) {
		LOG.info("IGWSplitGOCRequestScriptTask:auditWFIdAndCreatedDS starts");

		IGWSaveUtil saveUtil = new IGWSaveUtil(context.getRepository(), DSMTConstants.GOC_WF_DATASPACE,
				DSMTConstants.GOC_WF_DATASET, GOCWFPaths._C_DSMT_INT_GOC_REPORT_DTL.getPathInSchema(), inputMap, null, false, null, null);
		try {
			Utils.executeProcedure(saveUtil, context.getSession(),
					context.getRepository().lookupHome(HomeKey.forBranchName(DSMTConstants.GOC_WF_DATASPACE)));
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOG.info("IGWSplitGOCRequestScriptTask:auditWFIdAndCreatedDS ends");

	}

	private void createORUpdateRecordsInCDCreated(ArrayList<IGWSplitBean> splitBeanList, String createdDataSpace,
			ScriptTaskBeanContext context, Map<Path, Object> schemaPaths, List<String> recordsToWrite)
			throws OperationException {
		LOG.info("IGWSplitGOCRequestScriptTask:createORUpdateRecordsInCDCreated starts");
		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(HomeKey.forBranchName(createdDataSpace));
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
		for (IGWSplitBean bean : splitBeanList) {
			Adaptation record = bean.getRecord();
			Map<Path, Object> copySchemaPaths = new LinkedHashMap<Path, Object>();
			if (bean.getTablePath().format().contains("C_DSMT_DFN_GOC")) {
				copySchemaPaths.putAll(schemaPaths);
				recordsToWrite.add(getRecord(record, copySchemaPaths));
				removeReadOnlyAttributes(copySchemaPaths);
			}

			if (bean.isNew()) {
				LOG.info("IGWSplitGOCRequestScriptTask:About to save a new record in DataSpace:" + createdDataSpace);
				IGWSaveUtil saveUtil = new IGWSaveUtil(context.getRepository(), createdDataSpace, DSMTConstants.GOC,
						bean.getTablePath(), new HashMap<Path, Object>(), record, false, null, null);
				Utils.executeProcedure(saveUtil, context.getSession(),
						context.getRepository().lookupHome(HomeKey.forBranchName(createdDataSpace)));
				//final Map<String, String> workflowParameterMap = new HashMap<String, String>();
				
				
				
				final Map<Path, String> workflowParameterMap = new HashMap<Path, String>();
				/*vc.setValue(workflowParameterMap.get(GOCConstants.WF_PARAM_SID), GOCPaths._CurrentSID);
				vc.setValue(workflowParameterMap.get(GOCConstants.WF_PARAM_MAINTENANCE_TYPE), GOCPaths._MaintenanceType);
				vc.setValue(workflowParameterMap.get(GOCConstants.WF_PARAM_FUNCTION_ID), GOCPaths._FunctionalID);*/
				workflowParameterMap.put(GOCPaths._CurrentSID, IGWGOCWorkflowUtils.getCurrentSID(record.getContainer()));
				workflowParameterMap.put(GOCPaths._MaintenanceType, IGWGOCWorkflowUtils.getMaintenanceType(record.getContainer()));
				//workflowParameterMap.put(GOCPaths._FunctionalID,  IGWGOCWorkflowUtils.getFunctionalID(record.getContainer()));
				workflowParameterMap.put(GOCPaths._ManagementOnly, IGWGOCWorkflowUtils.getManagementOnly(record.getContainer()));
				
				
				final Procedure setSIDProc = new Procedure() {
					@Override
					public void execute(ProcedureContext pContext) throws Exception {

						IGWGOCWorkflowUtils.setWorkflowParameters(workflowParameterMap, dataSetRef, pContext);

					}
				};

				Utils.executeProcedure(setSIDProc, context.getSession(), dataSpaceRef);
				LOG.info("IGWSplitGOCRequestScriptTask:Save of new records done in DataSpace: " + createdDataSpace);
			} else {
				LOG.info("IGWSplitGOCRequestScriptTask:About to save an updated record in DataSpace:" + createdDataSpace);
				/*String predicate = "(" + GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK.format() + " = '"
						+ copySchemaPaths.get(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK) + "')";
				LOG.info("IGWSplitGOCRequestScriptTask:predicate:" + predicate);*/

				IGWSaveUtil igwSaveUtil = new IGWSaveUtil(context.getRepository(), createdDataSpace, DSMTConstants.GOC,
						bean.getTablePath(), new HashMap<Path, Object>(), record, true, null, null);

				Utils.executeProcedure(igwSaveUtil, context.getSession(),
						context.getRepository().lookupHome(HomeKey.forBranchName(createdDataSpace)));
				
				
				final Map<Path, String> workflowParameterMap = new HashMap<Path, String>();
				/*vc.setValue(workflowParameterMap.get(GOCConstants.WF_PARAM_SID), GOCPaths._CurrentSID);
				vc.setValue(workflowParameterMap.get(GOCConstants.WF_PARAM_MAINTENANCE_TYPE), GOCPaths._MaintenanceType);
				vc.setValue(workflowParameterMap.get(GOCConstants.WF_PARAM_FUNCTION_ID), GOCPaths._FunctionalID);*/
				workflowParameterMap.put(GOCPaths._CurrentSID, IGWGOCWorkflowUtils.getCurrentSID(record.getContainer()));
				workflowParameterMap.put(GOCPaths._MaintenanceType, IGWGOCWorkflowUtils.getMaintenanceType(record.getContainer()));
				//workflowParameterMap.put(GOCPaths._FunctionalID,  IGWGOCWorkflowUtils.getFunctionalID(record.getContainer()));
				workflowParameterMap.put(GOCPaths._ManagementOnly, IGWGOCWorkflowUtils.getManagementOnly(record.getContainer()));
				
				final Procedure setSIDProc = new Procedure() {
					@Override
					public void execute(ProcedureContext pContext) throws Exception {

						IGWGOCWorkflowUtils.setWorkflowParameters(workflowParameterMap, dataSetRef, pContext);

					}
				};

				Utils.executeProcedure(setSIDProc, context.getSession(), dataSpaceRef);

				LOG.info("IGWSplitGOCRequestScriptTask: Save of updated records done in DataSpace:" + createdDataSpace);
			}

		}// Insert in goc reporting table.

		LOG.info("IGWSplitGOCRequestScriptTask:createORUpdateRecordsInCDCreated ends");
	}

	private void removeReadOnlyAttributes(Map<Path, Object> copySchemaPaths) {
		copySchemaPaths.remove(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup);
		copySchemaPaths.remove(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._IsCurrentSID);
		copySchemaPaths.remove(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._PRT_EFFDT);
		copySchemaPaths.remove(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._PRT_COMMENT_L);

	}

	private String getRecord(Adaptation adaptation, Map<Path, Object> schemaPaths) {
		LOG.info("IGWSplitGOCRequestScriptTask:getRecord starts");
		StringBuffer buffer = new StringBuffer();
		int i = 1;
		for (Path path : schemaPaths.keySet()) {

			schemaPaths.put(path, adaptation.get(path));
			if (null != adaptation.get(path))
				buffer.append(adaptation.get(path).toString());
			if (i != schemaPaths.size()) {
				buffer.append(FILE_SEPERATOR);
			}
			/*
			 * if (null == schemaPaths.get(path)) {
			 * buffer.append(adaptation.getString(path)); } else if
			 * ("Date".equalsIgnoreCase(schemaPaths.get(path))) { Object value =
			 * adaptation.get(path); buffer.append(String.valueOf(value)); }
			 * else if ("Boolean".equalsIgnoreCase(schemaPaths.get(path))) {
			 * buffer.append(adaptation.get_boolean(path)); } else if
			 * ("Integer".equalsIgnoreCase(schemaPaths.get(path))) {
			 * buffer.append(adaptation.get_int(path));
			 * 
			 * }
			 */
			i++;
		}
		LOG.info("IGWSplitGOCRequestScriptTask:getRecord :" + buffer.toString());
		LOG.info("IGWSplitGOCRequestScriptTask:getRecord ends");

		return buffer.toString();
	}

	private Map<Path, Object> prepareGOCPathMap() {
		LOG.info("IGWSplitGOCRequestScriptTask:prepareGOCPathMap started");

		Map<Path, Object> schemaPaths = new LinkedHashMap<Path, Object>();
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._SETID, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ACTION_REQD, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFFDT, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._EFF_STATUS, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._MANAGER_ID, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_SID, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_GEO_ID, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_MAN_SEG_ID, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_BU, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FRS_OU, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_FUNCTION, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LVID, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._HEADCOUNT_GOC, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCRSHORT, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._DESCR, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_USAGE, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_LOC_COST_CD, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._C_DSMT_GOC_COMMENT, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CREATED_OPERID, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ORIG_DTTM, null);// need
																								// to
																								// check
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LASTUPDOPRID, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._CHNG_DTTM, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Requested_by, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._Requested_Date, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ENDDT, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._IsCurrentSID, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._PRT_EFFDT, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._PRT_COMMENT_L, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._LAST_ACTION_REQUIRED, null);
		schemaPaths.put(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._MERGE_DATE, null);
		LOG.info("IGWSplitGOCRequestScriptTask:prepareGOCPathMap ends");
		return schemaPaths;

	}

	public Map<String, ArrayList<IGWSplitBean>> splitGOCRequestBasedOnRoutingIds(DifferenceBetweenInstances dataSetDiffs) {
		LOG.info("IGWSplitGOCRequestScriptTask:splitGOCRequestBasedOnRoutingIds started");
		int totalNewGOCAdditions = 0;
		int totalNewNonGOCAdditions = 0;
		int totalGOCUpdates = 0;
		int totalNonGOCUpdates = 0;
		int totalModified = 0;
		Map<String, ArrayList<IGWSplitBean>> routingIdMap = new HashMap<String, ArrayList<IGWSplitBean>>();
		List<IGWSplitBean> gocList = new ArrayList<IGWSplitBean>();
		List<IGWSplitBean> nongocList = new ArrayList<IGWSplitBean>();

		final List<DifferenceBetweenTables> tableDiffsList = dataSetDiffs.getDeltaTables();

		if (null != tableDiffsList) {
			totalModified = tableDiffsList.size();
			LOG.info("Total Modifications in requestID :" + requestID + " are :" + totalModified);
			totalMod=String.valueOf(totalModified);
		}
		final Iterator<DifferenceBetweenTables> tableDiffsIter = tableDiffsList.iterator();
		while (tableDiffsIter.hasNext()) {
			final DifferenceBetweenTables tableDiffs = tableDiffsIter.next();

			final List<ExtraOccurrenceOnRight> adds = tableDiffs.getExtraOccurrencesOnRight();
			final List<DifferenceBetweenOccurrences> deltas = tableDiffs.getDeltaOccurrences();
			Path table = tableDiffs.getPathOnRight();
			if (table.format().contains("C_DSMT_DFN_GOC")) {
				totalNewGOCAdditions = adds.size();
				totalGOCUpdates = deltas.size();

				LOG.info("totalNewGOCAdditions :  " + totalNewGOCAdditions);
				LOG.info("totalGOCUpdates :  " + totalGOCUpdates);
				newGOCCount=String.valueOf(totalNewGOCAdditions);
				updatedGOCCount= String.valueOf(totalGOCUpdates);
				// First iterating for additions
				final Iterator<ExtraOccurrenceOnRight> addsIter = adds.iterator();
				while (addsIter.hasNext()) {

					final ExtraOccurrenceOnRight add = addsIter.next();
					final Adaptation record = add.getExtraOccurrence();
					String routingId = record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup);
					Integer goc_PK = record.get_int(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK);
					IGWSplitBean newGOCBean = new IGWSplitBean(true, routingId, goc_PK, record);
					gocList.add(newGOCBean);

					/*
					 * if (null != routingId &&
					 * routingIdMap.containsKey(routingId.trim())) {
					 * LOG.info("IGWSplitGOCRequestScriptTask:Found new routingId :"
					 * + routingId); Map<String, ArrayList<Adaptation>>
					 * newRecordsPerRoutingID = routingIdMap.get(routingId);
					 * ArrayList<Adaptation> recordsPerRoutingID =
					 * newRecordsPerRoutingID.get("New");
					 * recordsPerRoutingID.add(record); } else { if (null !=
					 * routingId) {
					 * LOG.info("IGWSplitGOCRequestScriptTask:Added new routingId :"
					 * + routingId); Map<String, ArrayList<Adaptation>>
					 * newRecordsPerRoutingID = new HashMap<String,
					 * ArrayList<Adaptation>>();
					 * 
					 * ArrayList<Adaptation> recordsPerRoutingID = new
					 * ArrayList<Adaptation>(); recordsPerRoutingID.add(record);
					 * newRecordsPerRoutingID.put("New", recordsPerRoutingID);
					 * routingIdMap.put(routingId, newRecordsPerRoutingID); } }
					 */

				}
				// Now iterating for updates
				final Iterator<DifferenceBetweenOccurrences> deltasIter = deltas.iterator();
				while (deltasIter.hasNext()) {
					final DifferenceBetweenOccurrences delta = deltasIter.next();
					final Adaptation record = delta.getOccurrenceOnRight();
					String routingId = record.getString(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._ApproverGroup);
					Integer goc_PK = record.get_int(GOCPaths._GLOBAL_STD_ENT_STD_F_GOC_STD_C_DSMT_DFN_GOC._GOC_PK);

					IGWSplitBean updatedGOCBean = new IGWSplitBean(false, routingId, goc_PK, record);
					gocList.add(updatedGOCBean);
				}

			} else {
				totalNewNonGOCAdditions = totalNewNonGOCAdditions + adds.size();
				totalNonGOCUpdates = totalNonGOCUpdates + deltas.size();
				LOG.info("IGWSplitGOCRequestScriptTask: totalNewNonGOCAdditions : " + totalNewNonGOCAdditions);
				LOG.info("IGWSplitGOCRequestScriptTask:totalNonGOCUpdates :" + totalNonGOCUpdates);
				newNonGOCCount=String.valueOf(totalNewNonGOCAdditions);
				updatedNonGOCCount= String.valueOf(totalNonGOCUpdates);
				// First iterating for additions
				final Iterator<ExtraOccurrenceOnRight> addsIter = adds.iterator();
				while (addsIter.hasNext()) {
					final ExtraOccurrenceOnRight add = addsIter.next();
					final Adaptation record = add.getExtraOccurrence();
					String goc_PK = record.getString(Path.parse("./C_GOC_FK"));
					IGWSplitBean newNonGOCBean = new IGWSplitBean(true, null, Integer.parseInt(goc_PK), record);
					nongocList.add(newNonGOCBean);

				}
				final Iterator<DifferenceBetweenOccurrences> deltasIter = deltas.iterator();
				while (deltasIter.hasNext()) {
					final DifferenceBetweenOccurrences delta = deltasIter.next();
					final Adaptation record = delta.getOccurrenceOnRight();

					String goc_PK = record.getString(Path.parse("./C_GOC_FK"));

					IGWSplitBean updatedNonGOCBean = new IGWSplitBean(false, null, Integer.parseInt(goc_PK), record);
					nongocList.add(updatedNonGOCBean);

				}
			}

		}
		Collections.sort(gocList, IGWSplitBean.getGRIDComparator());
		for (IGWSplitBean bean : gocList) {
			String routingId = bean.getRoutingID();
			if (null != routingId && routingIdMap.containsKey(routingId.trim())) {

				ArrayList<IGWSplitBean> listSplitBean = routingIdMap.get(routingId);
				listSplitBean.add(bean);

			} else {
				if (null != routingId) {
					ArrayList<IGWSplitBean> listSplitBean = new ArrayList<IGWSplitBean>();
					listSplitBean.add(bean);
					routingIdMap.put(routingId, listSplitBean);
				}
			}
		}
		Collections.sort(gocList, IGWSplitBean.getGOCComparator());
		for (IGWSplitBean bean : nongocList) {
			Integer goc_pk = bean.getGOC_PK();
			Iterator<IGWSplitBean> itr= gocList.iterator();
			while (itr.hasNext()) {
				IGWSplitBean gocBean = itr.next();
				if (gocBean.getGOC_PK().equals(goc_pk)) {
					ArrayList<IGWSplitBean> listSplitBean = routingIdMap.get(gocBean.getRoutingID());
					listSplitBean.add(bean);
				}
			}
		}

		LOG.info("IGWSplitGOCRequestScriptTask:Total number of unique routing ids found:" + routingIdMap.size());
		LOG.info("IGWSplitGOCRequestScriptTask:splitGOCRequestBasedOnRoutingIds ends");
		return routingIdMap;

	}

	public String createChildDataSpace(ScriptTaskBeanContext context) throws OperationException {
		LOG.info("InsertGOCReportingScriptTask:createChildDataSpace starts");
		final AdaptationHome parentAdaptationHome = Repository.getDefault().lookupHome(HomeKey.forBranchName(parentDataSpace));

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss");
		Date today = Calendar.getInstance().getTime();
		String childDataSpaceName = df.format(today);
		final HomeCreationSpec spec = new HomeCreationSpec();
		spec.setKey(HomeKey.forBranchName(childDataSpaceName));
		spec.setParent(parentAdaptationHome);
		spec.setOwner(Profile.parse(profile));
		spec.setHomeToCopyPermissionsFrom(Repository.getDefault().lookupHome(HomeKey.forBranchName("DSMTPermissionsTemplate")));
		String createdDataSpace = new String();
		try {
			final AdaptationHome childDataSpace = context.getRepository().createHome(spec, context.getSession());
			createdDataSpace = childDataSpace.getKey().getName();
			LOG.debug("InsertGOCReportingScriptTask: Successfully created Child DataSpace: " + createdDataSpace);
		} catch (final OperationException e) {
			throw e;
		}
		LOG.info("InsertGOCReportingScriptTask:createChildDataSpace ends");
		return createdDataSpace;
	}

	private void insertMonitoringdata(ScriptTaskBeanContext context, String dataSpace, String requestID) {

		LOG.info("InsertGOCReportingScriptTask: insertMonitoringdata started for dataSpace:" + dataSpace + " and requestID: "
				+ requestID);

		try {
			MonitoringTableBean monitoringTableBean = new MonitoringTableBean();
			monitoringTableBean.setChildDataSpaceID(dataSpace);
			monitoringTableBean.setOperation(DSMTConstants.INSERT);
			LOG.debug("requestID >> " + requestID);
			if (DSMTConstants.DIRECTUPLOAD.equalsIgnoreCase(requestID)) {
				monitoringTableBean.setRequestID(String.valueOf(Calendar.getInstance().getTime().getTime()));
				monitoringTableBean.setRequestType(DSMTConstants.DIRECTUPLOAD);
			} else {
				monitoringTableBean.setRequestID(requestID);
				monitoringTableBean.setRequestType(GOCConstants.WORKFLOW_PUBLICATION);

			}

			if (profile != null) {
				monitoringTableBean.setRequestBy(profile.replace("U", ""));
			}
			monitoringTableBean.setRequestStatus(GOCConstants.CREATED);
			LOG.debug("Bean "+monitoringTableBean);
			// monitoringTableBean.setRequestorComment(userComment);

			InsertMonitoringdata insertMonitoringdata = new InsertMonitoringdata(monitoringTableBean, context.getRepository(),
					tableID);
			Utils.executeProcedure(insertMonitoringdata, context.getSession(),
					context.getRepository().lookupHome(HomeKey.forBranchName(GOCConstants.METADATA_DATA_SPACE)));

		} catch (Exception ex) {
			LOG.error("InsertGOCReportingScriptTask: Error while preparing data space for merge.", ex);

		}
		LOG.info("InsertGOCReportingScriptTask: insertMonitoringdata ends");

	}

}
