package com.citi.ebx.workflow.vo;


public class DsmtGocVO {
	
	public   String sid ;
	public   String ACTION_REQD  ;
	public   String GOC ;
	public   String GOCPK ;
	public   String MANAGER_ID  ;
	public   String C_DSMT_MAN_GEO_ID  ;
	public   String C_DSMT_MAN_SEG_ID ;
	public   String C_DSMT_FRS_BU  ;
	public   String C_DSMT_FRS_OU  ;
	public   String C_DSMT_FUNCTION  ;
	public   String C_DSMT_LVID  ;
	public   String desc  ;
	public   String descShort  ;
	public   String gocUsages  ;
	public   String ApproverGroup  ;
	public   String C_DSMT_LOC_COST_CD  ;
	public   String headCount_GOC  ;
	public   String comment  ;
	public   String functionalID  ;
	/**
	 * @return the sETID
	 */
	public String getSETID() {
		return sid;
	}
	/**
	 * @param sETID the sETID to set
	 */
	public void setSETID(String sETID) {
		sid = sETID;
	}
	/**
	 * @return the aCTION_REQD
	 */
	public String getACTION_REQD() {
		return ACTION_REQD;
	}
	/**
	 * @param aCTION_REQD the aCTION_REQD to set
	 */
	public void setACTION_REQD(String aCTION_REQD) {
		ACTION_REQD = aCTION_REQD;
	}
	/**
	 * @return the gOC
	 */
	public String getGOC() {
		return GOC;
	}
	/**
	 * @param gOC the gOC to set
	 */
	public void setGOC(String gOC) {
		GOC = gOC;
	}
	/**
	 * @return the mANAGER_ID
	 */
	public String getMANAGER_ID() {
		return MANAGER_ID;
	}
	/**
	 * @param mANAGER_ID the mANAGER_ID to set
	 */
	public void setMANAGER_ID(String mANAGER_ID) {
		MANAGER_ID = mANAGER_ID;
	}
	/**
	 * @return the c_DSMT_MAN_GEO_ID
	 */
	public String getC_DSMT_MAN_GEO_ID() {
		return C_DSMT_MAN_GEO_ID;
	}
	/**
	 * @param c_DSMT_MAN_GEO_ID the c_DSMT_MAN_GEO_ID to set
	 */
	public void setC_DSMT_MAN_GEO_ID(String c_DSMT_MAN_GEO_ID) {
		C_DSMT_MAN_GEO_ID = c_DSMT_MAN_GEO_ID;
	}
	/**
	 * @return the c_DSMT_MAN_SEG_ID
	 */
	public String getC_DSMT_MAN_SEG_ID() {
		return C_DSMT_MAN_SEG_ID;
	}
	/**
	 * @param c_DSMT_MAN_SEG_ID the c_DSMT_MAN_SEG_ID to set
	 */
	public void setC_DSMT_MAN_SEG_ID(String c_DSMT_MAN_SEG_ID) {
		C_DSMT_MAN_SEG_ID = c_DSMT_MAN_SEG_ID;
	}
	/**
	 * @return the c_DSMT_FRS_BU
	 */
	public String getC_DSMT_FRS_BU() {
		return C_DSMT_FRS_BU;
	}
	/**
	 * @param c_DSMT_FRS_BU the c_DSMT_FRS_BU to set
	 */
	public void setC_DSMT_FRS_BU(String c_DSMT_FRS_BU) {
		C_DSMT_FRS_BU = c_DSMT_FRS_BU;
	}
	/**
	 * @return the approverGroup
	 */
	public String getApproverGroup() {
		return ApproverGroup;
	}
	/**
	 * @param approverGroup the approverGroup to set
	 */
	public void setApproverGroup(String approverGroup) {
		ApproverGroup = approverGroup;
	}
	/**
	 * @return the c_DSMT_LOC_COST_CD
	 */
	public String getC_DSMT_LOC_COST_CD() {
		return C_DSMT_LOC_COST_CD;
	}
	/**
	 * @param c_DSMT_LOC_COST_CD the c_DSMT_LOC_COST_CD to set
	 */
	public void setC_DSMT_LOC_COST_CD(String c_DSMT_LOC_COST_CD) {
		C_DSMT_LOC_COST_CD = c_DSMT_LOC_COST_CD;
	}
	/**
	 * @return the gOCPK
	 */
	public String getGOCPK() {
		return GOCPK;
	}
	/**
	 * @param gOCPK the gOCPK to set
	 */
	public void setGOCPK(String gOCPK) {
		GOCPK = gOCPK;
	}
	/**
	 * @return the c_DSMT_FRS_OU
	 */
	public String getC_DSMT_FRS_OU() {
		return C_DSMT_FRS_OU;
	}
	/**
	 * @param c_DSMT_FRS_OU the c_DSMT_FRS_OU to set
	 */
	public void setC_DSMT_FRS_OU(String c_DSMT_FRS_OU) {
		C_DSMT_FRS_OU = c_DSMT_FRS_OU;
	}
	/**
	 * @return the c_DSMT_FUNCTION
	 */
	public String getC_DSMT_FUNCTION() {
		return C_DSMT_FUNCTION;
	}
	/**
	 * @param c_DSMT_FUNCTION the c_DSMT_FUNCTION to set
	 */
	public void setC_DSMT_FUNCTION(String c_DSMT_FUNCTION) {
		C_DSMT_FUNCTION = c_DSMT_FUNCTION;
	}
	/**
	 * @return the c_DSMT_LVID
	 */
	public String getC_DSMT_LVID() {
		return C_DSMT_LVID;
	}
	/**
	 * @param c_DSMT_LVID the c_DSMT_LVID to set
	 */
	public void setC_DSMT_LVID(String c_DSMT_LVID) {
		C_DSMT_LVID = c_DSMT_LVID;
	}
	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}
	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	/**
	 * @return the descShort
	 */
	public String getDescShort() {
		return descShort;
	}
	/**
	 * @param descShort the descShort to set
	 */
	public void setDescShort(String descShort) {
		this.descShort = descShort;
	}
	/**
	 * @return the gocUsages
	 */
	public String getGocUsages() {
		return gocUsages;
	}
	/**
	 * @param gocUsages the gocUsages to set
	 */
	public void setGocUsages(String gocUsages) {
		this.gocUsages = gocUsages;
	}
	public String getHeadCount_GOC() {
		return headCount_GOC;
	}
	public void setHeadCount_GOC(String headCount_GOC) {
		this.headCount_GOC = headCount_GOC;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getFunctionalID() {
		return functionalID;
	}
	public void setFunctionalID(String functionalID) {
		this.functionalID = functionalID;
	}
	

}
