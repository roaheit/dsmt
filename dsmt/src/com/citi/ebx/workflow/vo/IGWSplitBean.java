package com.citi.ebx.workflow.vo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;

public class IGWSplitBean  {
	private boolean isNew;
	private String routingID;
	private Integer GOC_PK;
	private Adaptation record;
	private Path tablePath;
	public IGWSplitBean(boolean isNew, String routingID, Integer gOC_PK, Adaptation record) {
		super();
		this.isNew = isNew;
		this.routingID = routingID;
		GOC_PK = gOC_PK;
		this.record = record;
		this.tablePath=record.getContainerTable().getTablePath();
	}
	public boolean isNew() {
		return isNew;
	}
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	public String getRoutingID() {
		return routingID;
	}
	public void setRoutingID(String routingID) {
		this.routingID = routingID;
	}
	public Integer getGOC_PK() {
		return GOC_PK;
	}
	public void setGOC_PK(Integer gOC_PK) {
		GOC_PK = gOC_PK;
	}
	public Adaptation getRecord() {
		return record;
	}
	public void setRecord(Adaptation record) {
		this.record = record;
	}
	public Path getTablePath() {
		return tablePath;
	}
	public void setTablePath(Path tablePath) {
		this.tablePath = tablePath;
	}
	
	public  static Comparator<IGWSplitBean> getGOCComparator() {
	        return new Comparator<IGWSplitBean>() {

				@Override
				public int compare(IGWSplitBean o1, IGWSplitBean o2) {
					 return Integer.valueOf(o1.getGOC_PK()).compareTo(o2.getGOC_PK());
				}
	            
	        };
	    }

	   public  static Comparator<IGWSplitBean> getGRIDComparator() {
	        return new Comparator<IGWSplitBean>() {

				@Override
				public int compare(IGWSplitBean o1, IGWSplitBean o2) {
					return o1.getRoutingID().compareTo(o2.getRoutingID());
				}
	           
	        };
	    }
	    public static void main(String[]args){
	    	List<IGWSplitBean> ar= new ArrayList<IGWSplitBean>();
	    	Collections.sort(ar, IGWSplitBean.getGOCComparator());
	    }
}
	 

	 
	