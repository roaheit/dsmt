/**
 * 
 */
package com.citi.ebx.workflow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;

import com.citi.ebx.dsmt.util.AccountibilityMatrixConstants;
import com.citi.ebx.dsmt.util.BPMReqIDUtil;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.UpdateManagerProcedure;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

/**
 * @author rk00242
 * 
 */
public class TerminateManagerScriptTask extends ScriptTaskBean {

	protected static final LoggingCategory LOG = LoggingCategory.getKernel();
	protected static final DSMTPropertyHelper propertyHelper = new DSMTPropertyHelper();


	private String soeID;
	private String dataSpace;
	private String tableXPath;
	private String approvalGroup;
	private String lvid;
	private String user;
	private String requestID;
	private String tableName;
	private String tableID;
	private Path tabletoUpdate;

	Set<String> approver = new TreeSet<String>();
	Set<String> lvidSet = new TreeSet<String>();
	
	public static final Path CONTROLLER_DETAILS_PATH_NAM = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM.getPathInSchema();
	public static final Path CONTROLLER_DETAILS_PATH_LATAM = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_LATAM.getPathInSchema();
	public static final Path CONTROLLER_DETAILS_PATH_ASIA = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_ASIA.getPathInSchema();
	public static final Path CONTROLLER_DETAILS_PATH_EMEA = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_EMEA.getPathInSchema();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.orchestranetworks.workflow.ScriptTaskBean#executeScript(com.
	 * orchestranetworks.workflow.ScriptTaskBeanContext)
	 */
	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		LOG.info("TerminateManagerScriptTask.....start");

		Repository rp = context.getRepository();
		Adaptation metadataDataSet = EBXUtils.getMetadataDataSet(rp, dataSpace,
				AccountibilityMatrixConstants.DATASET_Accounting_Matrix);

		updateControllerID(metadataDataSet, context);

		LOG.info("TerminateManagerScriptTask.....end");
	}

	public void updateControllerID(Adaptation metadataDataSet,
			ScriptTaskBeanContext context) {
		AdaptationTable targetTable;
		Adaptation record = null;
		String reqSOEID = "WFAdmin";
		String prevController = null;

		try {
			String environment = getEnvironment();
			if(null == environment || "local".equalsIgnoreCase(environment)){
				requestID = "101240PID18" ;// Use some test value and Do not create new request ID for local testing
			}else {
				
				requestID = new BPMReqIDUtil().getRequestIDFromBPM(reqSOEID); // Get Request ID from CWM
			}
		//	requestID = "101660PID18";
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			LOG.info("exception occurred while generating request ID from BPM"
					+ ex.getMessage());
		}

		StringBuffer predicate1 = new StringBuffer(
				"osd:contains-case-insensitive("
						+ AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID
								.format() + ",'" + soeID + "')");

		LOG.info("checking tablePath : " + tableXPath);

		String[] tableSplit = tableXPath.split("/");
		tableID = tableSplit[tableSplit.length - 1];

		targetTable = metadataDataSet.getTable(Path.parse(tableXPath));
		tableName = targetTable.getTableOccurrenceRootNode().getLabel(
				context.getSession());
		
		if(tableName.contains("NAM")){
			tabletoUpdate = CONTROLLER_DETAILS_PATH_NAM;
		}else if(tableName.contains("LATAM")){
			tabletoUpdate = CONTROLLER_DETAILS_PATH_LATAM;
		}else if(tableName.contains("ASIA")){
			tabletoUpdate = CONTROLLER_DETAILS_PATH_ASIA;
		}else if(tableName.contains("EMEA")){
			tabletoUpdate = CONTROLLER_DETAILS_PATH_EMEA;
		}

		RequestResult rs1 = targetTable.createRequestResult(predicate1
				.toString());
		for (Adaptation recordtoUpdate; (recordtoUpdate = rs1.nextAdaptation()) != null;) {

			if (null != recordtoUpdate) {
				try {

					String controllerID = recordtoUpdate
							.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID);
					
					String currentlvid = recordtoUpdate.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_LVID);
					lvidSet.add(currentlvid);
					
				
					controllerID = controllerID.toUpperCase();
					
					//check if there are multiple controllerID and remove the SOEID from it(if exists)
					if (controllerID.contains(AccountibilityMatrixConstants.SEPARATOR)) {
						prevController = getControllerID(controllerID, soeID);
					}
					
					//get Parent record if prev controller is null 
					if (prevController == null || prevController.isEmpty()
							|| controllerID.equalsIgnoreCase(soeID)
							|| prevController.equalsIgnoreCase(soeID)) {
						record = getParentRecord(recordtoUpdate, targetTable);

						if (null != record) {
							prevController = record
									.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID);
							LOG.info("prevController : " + prevController);

							if (null != prevController) {
								while (prevController.contains(soeID)) {
									prevController = getControllerID(
											prevController, soeID);
									if (prevController == null
											|| prevController.isEmpty()) {
										record = getParentRecord(record,
												targetTable);
										prevController = prevController
												.concat(record
														.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Controller_ID));
									}
								}
								prevController = getControllerID(
										prevController, soeID);
							} else
								prevController = "";

						}
					}
					String magSegID = recordtoUpdate.getOccurrencePrimaryKey().format();
					
					
					AdaptationTable targetTable1 = metadataDataSet.getTable(tabletoUpdate);
					if (prevController.contains(AccountibilityMatrixConstants.SEPARATOR)) {
						String [] ctrlList=prevController.split(AccountibilityMatrixConstants.SEPARATOR);
						for(String ctrlID: ctrlList){
							insertControllerID(ctrlID, recordtoUpdate, targetTable1, magSegID, context);
							approver.add(ctrlID);
						}
						
					} else {
						approver.add(prevController.toUpperCase());
						insertControllerID(prevController, recordtoUpdate, targetTable1, magSegID, context);
					}
					
					if(approver.contains(soeID)){
						approver.remove(soeID);
					}
							
					//delete terminated Controller from controller detail table
					
					final String predicate = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID.format()
							+"= '" +soeID + "' and "+AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID.format()+
							"='" +recordtoUpdate.getOccurrencePrimaryKey().format()+"'";

					RequestResult res = targetTable1.createRequestResult(predicate);

					if(res.getSize()>0 || !res.isEmpty()){
					Adaptation rec = res.getAdaptation(0);

					// call procedure to udpate record
					UpdateManagerProcedure updateManagerPro = new UpdateManagerProcedure(
							 rec,true);

					
					Utils.executeProcedure(
							updateManagerPro,
							context.getSession(),
							context.getRepository().lookupHome(
									HomeKey.forBranchName(dataSpace)));
					}
					res.close();
					
					approvalGroup = StringUtils.join(approver, "|");
					lvid = StringUtils.join(lvidSet, "|");

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	public Adaptation getParentRecord(Adaptation recordUpdate,
			AdaptationTable targetTable) {

		final String separator = "\\|";
		String parentMSID = recordUpdate
				.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Parent_MS_ID);

		String[] parentID = parentMSID.split(separator);

		PrimaryKey key = targetTable.computePrimaryKey(parentID);
		Adaptation rec = targetTable.lookupAdaptationByPrimaryKey(key);

		return rec;
	}

	public String getControllerID(String ControllerID, String soeID) {

		List<String> myList = new ArrayList<String>(Arrays.asList(ControllerID
				.split(AccountibilityMatrixConstants.SEPARATOR)));

		myList.remove(soeID);

		return StringUtils.join(myList, AccountibilityMatrixConstants.SEPARATOR);
	}
	
	public void insertControllerID(String ctrlID,Adaptation recordtoUpdate,AdaptationTable targetTable1,String magSegID,ScriptTaskBeanContext context) throws OperationException{
		final String pred = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID.format()
				+"= '" +ctrlID + "' and "+AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID.format()+
				"='" +recordtoUpdate.getOccurrencePrimaryKey().format()+"'";
		
		
		RequestResult rs = targetTable1.createRequestResult(pred);
		
		if(rs.nextAdaptation()==null){
			UpdateManagerProcedure updateManagerProc = new UpdateManagerProcedure(
					targetTable1, ctrlID,magSegID,recordtoUpdate,true);

			Utils.executeProcedure(
					updateManagerProc,
					context.getSession(),
					context.getRepository().lookupHome(
							HomeKey.forBranchName(dataSpace)));
			
		}
	}

	public String getSoeID() {
		return soeID;
	}

	public void setSoeID(String soeID) {
		this.soeID = soeID;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getTableXPath() {
		return tableXPath;
	}

	public void setTableXPath(String tableXPath) {
		this.tableXPath = tableXPath;
	}

	public String getApprovalGroup() {
		return approvalGroup;
	}

	public void setApprovalGroup(String approvalGroup) {
		this.approvalGroup = approvalGroup;
	}

	public String getLvid() {
		return lvid;
	}

	public void setLvid(String lvid) {
		this.lvid = lvid;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableID() {
		return tableID;
	}

	public void setTableID(String tableID) {
		this.tableID = tableID;
	}
	
	private String getEnvironment() {
		String environment = null;
		
		try {
			environment = propertyHelper.getProp(DSMTConstants.DSMT_ENVIRONMENT);
		} catch (Exception e) {
			// Digest the exception and continue.
		}
		return environment;
	}

}
