package com.citi.ebx.workflow;

import com.citi.ebx.dsmt.jms.JMSMessageHelper;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.DSMTPropertyHelper;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public abstract class JMSMessageScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	protected String queueName;
	
	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	protected abstract JMSMessageHelper createJMSMessageHelper(
			Repository repository, Session session)
			throws OperationException;
	
	public void executeScript(ScriptTaskBeanContext context) throws OperationException {
		
		final Repository repository = context.getRepository();
		final Session session = context.getSession();
		final JMSMessageHelper helper = createJMSMessageHelper(repository, session);
		final String msg = helper.createMessageContent();
		LOG.debug("JMSMessageScriptTask: Sending message = " + msg);
	
		
		final String environment = new DSMTPropertyHelper().getProp(DSMTConstants.DSMT_ENVIRONMENT);
		
		if(! "local".equalsIgnoreCase(environment)){
		
			helper.sendMessage(msg, queueName);
		}
		
	}
}
