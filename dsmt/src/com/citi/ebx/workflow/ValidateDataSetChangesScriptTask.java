package com.citi.ebx.workflow;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValidationReport;
import com.orchestranetworks.service.comparison.DifferenceBetweenInstances;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class ValidateDataSetChangesScriptTask extends ScriptTaskBean {

	protected static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	
	private String dataSpace;
	private String dataSet;
	

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}


	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		
		LOG.info("start of executeScript in ValidateDataSetChangesScriptTask");
		
		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
				HomeKey.forBranchName(dataSpace));
		if (dataSpaceRef == null) {
			throw OperationException.createError("Data space " + dataSpace + " can't be found.");
		}
		final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(
				AdaptationName.forName(dataSet));
		if (dataSetRef == null) {
			throw OperationException.createError("Data set " + dataSet + " can't be found in data space " + dataSpace + ".");
		}
		
		final AdaptationHome initialSnapshot = dataSpaceRef.getParent();
		final Adaptation initialDataSet = initialSnapshot.findAdaptationOrNull(dataSetRef.getAdaptationName());
		
		boolean errorFound = false;
		LOG.info("ValidateDataSetChangesScriptTask.compareAdaptationTables started at " + Calendar.getInstance().getTime() );
		final DifferenceBetweenInstances dataSetDiffs = DifferenceHelper.compareInstances(initialDataSet, dataSetRef, false);
		LOG.info("ValidateDataSetChangesScriptTask.compareAdaptationTables finished at " + Calendar.getInstance().getTime() );
		
		@SuppressWarnings("unchecked")
		final List<DifferenceBetweenTables> tableDiffsList = dataSetDiffs.getDeltaTables();
		final Iterator<DifferenceBetweenTables> tableDiffsIter = tableDiffsList.iterator();
		while (! errorFound && tableDiffsIter.hasNext()) {
			final DifferenceBetweenTables tableDiffs = tableDiffsIter.next();
			
			@SuppressWarnings("unchecked")
			final List<ExtraOccurrenceOnRight> adds = tableDiffs.getExtraOccurrencesOnRight();
			
		
			final Iterator<ExtraOccurrenceOnRight> addsIter = adds.iterator();
			while (! errorFound && addsIter.hasNext()) {
				final ExtraOccurrenceOnRight add = addsIter.next();
				final Adaptation record = add.getExtraOccurrence();
				final ValidationReport recordReport = record.getValidationReport(false, true);
				errorFound = recordReport.hasItemsOfSeverity(Severity.ERROR);
				
				if(errorFound){
					LOG.info("adds retrieved in ChangeSetContainsErrorsCondition " + adds);
					LOG.info("errorFound in adds" + errorFound + ", dataSpace = " + dataSpace);
				}
			}
			
			@SuppressWarnings("unchecked")
			final List<DifferenceBetweenOccurrences> deltas = tableDiffs.getDeltaOccurrences();
			
			
			final Iterator<DifferenceBetweenOccurrences> deltasIter = deltas.iterator();
			while (! errorFound && deltasIter.hasNext()) {
				final DifferenceBetweenOccurrences delta = deltasIter.next();
				final Adaptation record = delta.getOccurrenceOnRight();
				final ValidationReport recordReport = record.getValidationReport(false, true);
				errorFound = recordReport.hasItemsOfSeverity(Severity.ERROR);
				if(errorFound){
					LOG.info("deltas retrieved in ChangeSetContainsErrorsCondition " + deltas.toString());
					LOG.info("errorFound in deltas" + errorFound + ", dataSpace = " + dataSpace);
				}
			}
		}
	
		
		LOG.info("end of executeScript in ValidateDataSetChangesScriptTask");
	}
}
