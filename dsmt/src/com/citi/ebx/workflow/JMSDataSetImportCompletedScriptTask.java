package com.citi.ebx.workflow;

import com.citi.ebx.dsmt.jms.JMSDataSetImportCompletedMessageHelper;
import com.citi.ebx.dsmt.jms.JMSMessageHelper;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

public class JMSDataSetImportCompletedScriptTask extends JMSMessageScriptTask {
	private String importConfigID;
	private String correlationID;
	private String errorMessage;
	private String errorDetails;
	
	public String getImportConfigID() {
		return importConfigID;
	}

	public void setImportConfigID(String importConfigID) {
		this.importConfigID = importConfigID;
	}

	public String getCorrelationID() {
		return correlationID;
	}

	public void setCorrelationID(String correlationID) {
		this.correlationID = correlationID;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	@Override
	protected JMSMessageHelper createJMSMessageHelper(
			Repository repository, Session session)
			throws OperationException {
		JMSDataSetImportCompletedMessageHelper helper = new JMSDataSetImportCompletedMessageHelper();
		helper.setRepository(repository);
		helper.setSession(session);
		helper.setLog(LoggingCategory.getWorkflow());
		
		helper.setQueueName(queueName);
		helper.setImportConfigID(importConfigID);
		helper.setCorrelationID(correlationID);
		helper.setErrorMessage(errorMessage);
		helper.setErrorDetails(errorDetails);
		
		return helper;
	}

	
}
