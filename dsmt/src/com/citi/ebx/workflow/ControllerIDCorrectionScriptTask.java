package com.citi.ebx.workflow;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.orchestranetworks.schema.Path;
import com.citi.ebx.dsmt.acctMatrix.ControllerDetailsUpdationProcedure;
import com.citi.ebx.dsmt.acctMatrix.ControllerIDUpdationProcedure;
import com.citi.ebx.dsmt.util.DSMTConstants;
import com.citi.ebx.dsmt.util.DSMTNotificationService;
import com.citi.ebx.goc.path.AccountibilityMatrixPaths;
import com.citi.ebx.goc.path.GOCPaths;
import com.citi.ebx.util.EBXUtils;
import com.citi.ebx.util.Utils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValidationReportItem;
import com.orchestranetworks.service.ValidationReportItemIterator;
import com.orchestranetworks.service.ValidationReportItemSubjectForAdaptation;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class ControllerIDCorrectionScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	private String dataSpace="Accounting_Matrix";
	private String dataSet="Accounting_Matrix";
	public String getMsID() {
		return msID;
	}
	public void setMsID(String msID) {
		this.msID = msID;
	}
	public String getLvid() {
		return lvid;
	}
	public void setLvid(String lvid) {
		this.lvid = lvid;
	}
	
	private String msID;
	private String lvid;
	private String HierTablePath;
	private String ControllerDetailsTablePath;
	private Adaptation dataSetRef;
	
	public String getHierTablePath() {
		return HierTablePath;
	}
	public void setHierTablePath(String hierTablePath) {
		HierTablePath = hierTablePath;
	}
	public String getControllerDetailsTablePath() {
		return ControllerDetailsTablePath;
	}
	public void setControllerDetailsTablePath(String controllerDetailsTablePath) {
		ControllerDetailsTablePath = controllerDetailsTablePath;
	}

	
	public String getDataSpace() {
		return dataSpace;
	}
	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}
	public String getDataSet() {
		return dataSet;
	}
	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		Session session=context.getSession();
		
		final AdaptationHome dataSpaceRef = context.getRepository().lookupHome(
				HomeKey.forBranchName(dataSpace));
		if (dataSpaceRef == null) {
			throw OperationException.createError("Data space " + dataSpace + " can't be found.");
		}
		dataSetRef = dataSpaceRef.findAdaptationOrNull(
				AdaptationName.forName(dataSet));
		if (dataSetRef == null) {
			throw OperationException.createError("Data set " + dataSet + " can't be found in data space " + dataSpace + ".");
		}
		
		final AdaptationTable msHierTable = dataSetRef.getTable(getPath(HierTablePath));
		final AdaptationTable ControllerDetailsTable = dataSetRef.getTable(getPath(ControllerDetailsTablePath));
		/*final String predicate=AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID.format()
				+"= '"+msID+"|"+lvid+"'";
		RequestResult rs = ControllerDetailsTable.createRequestResult(predicate);
		for (Adaptation record; (record = rs.nextAdaptation()) != null;) 
		{
			System.out.println(""+record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID));
		}*/
		updateChild(msID, lvid, context, msHierTable, ControllerDetailsTable, session);
		getChildRecord(msID, lvid, msHierTable, ControllerDetailsTable, session, context);
	}
	public void updateChild(String msid,String lvid,ScriptTaskBeanContext context,AdaptationTable managedSegmentHierTable,AdaptationTable controllersDetailTable, Session session)
	{
		String controllers="";
		final String predicate=AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._C_DSMT_MAN_SEG_ID.format()
				+"= '"+msID+"|"+lvid+"'";
		RequestResult rs = controllersDetailTable.createRequestResult(predicate);
		for (Adaptation record; (record = rs.nextAdaptation()) != null;) 
		{
			controllers+=record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_Controller_Details_NAM._Controller_ID);
			controllers+="/";
		}
		LOG.info("Update Child Function");
		ControllerIDUpdationProcedure proc = new ControllerIDUpdationProcedure(msID, controllers, managedSegmentHierTable,lvid);
		try {
			Utils.executeProcedure(proc, session, context.getRepository().lookupHome(
					HomeKey.forBranchName(dataSpace)));
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void getChildRecord(String msID, String lvid,AdaptationTable managedSegmentHierTable, AdaptationTable controllersDetailTable, Session session,ScriptTaskBeanContext context)
			throws OperationException {

		LOG.info("Inside Get Child"+msID);
		final String predicate = AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._Parent_MS_ID.format()+ "='" + msID +"|"+lvid+ "'";
		LOG.info("Predicate"+predicate);
		RequestResult rs = managedSegmentHierTable.createRequestResult(predicate);	
		
		if (rs.getSize() > 0) {
			System.out.println("Get Child Found Parent MSID 1st"+msID);
			for (Adaptation record; (record = rs.nextAdaptation()) != null;) 
			{
				LOG.info("Get Child Found Parent MSID 2nd "+record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_MAN_SEG_ID));
				updateChild(msID, lvid, context, managedSegmentHierTable, controllersDetailTable, session);
				getChildRecord(record.getString(AccountibilityMatrixPaths._ACC_MAT_ACC_REG_ACC_MS_Hier_NAM._C_DSMT_MAN_SEG_ID),lvid,managedSegmentHierTable,controllersDetailTable, session,context);
			}
		}
		rs.close();
	}
private Path getPath(final String fieldName) {
		
		return Path.parse(fieldName);
	}

}