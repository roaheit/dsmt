package com.citi.ebx.workflow;

import com.citi.ebx.dsmt.jms.JMSComparisonMessageHelper.ComparisonLevel;
import com.citi.ebx.dsmt.jms.JMSGOCComparisonMessageHelper;
import com.citi.ebx.dsmt.jms.JMSMessageHelper;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;

public class GOCComparisonScriptTask extends JMSMessageScriptTask {
	
	private String requestID;
	private String dataSpace;
	private String dataSet;
	private String user;
	private String userComment;
	private String maintenanceType;
	private String functionalID;
	
	private String workflowStatus;

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	@Override
	protected JMSMessageHelper createJMSMessageHelper(
			Repository repository, Session session)
			throws OperationException {
		final JMSGOCComparisonMessageHelper helper = new JMSGOCComparisonMessageHelper();
		helper.setRepository(repository);
		helper.setSession(session);
		helper.setLog(LoggingCategory.getWorkflow());
		helper.setComparisonLevel(ComparisonLevel.DATASET);
		
		helper.setQueueName(queueName);
		helper.setRequestID(requestID);
		helper.setDataSpace(dataSpace);
		helper.setDataSet(dataSet);
		helper.setUser(user);
		helper.setUserComment(userComment);
		helper.setMaintenanceType(maintenanceType);
		helper.setFunctionalID(functionalID);
		helper.setWorkflowStatus(workflowStatus);
		
		
		return helper;
	}

	public void setMaintenanceType(String maintenanceType) {
		this.maintenanceType = maintenanceType;
	}

	public String getMaintenanceType() {
		return maintenanceType;
	}

	public void setFunctionalID(String functionalID) {
		this.functionalID = functionalID;
	}

	public String getFunctionalID() {
		return functionalID;
	}
}
