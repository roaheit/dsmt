package com.citi.ebx.workflow;



import org.apache.commons.lang.exception.ExceptionUtils;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.importer.BasicDataSetImporterFactory;
import com.orchestranetworks.ps.importer.DataSetImporter;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class DataSetImportScriptTask extends ScriptTaskBean {
	private static final LoggingCategory LOG = LoggingCategory.getWorkflow();
	
	private String dataSpace;
	private String dataSet;
	private String importConfigDataSpace;
	private String importConfigDataSet;
	private String importConfigID;
	private boolean writeErrorToContext = true;
	private String errorMessage;
	private String errorDetails;
	private boolean disableTriggers = true;
	private boolean runValidation = true;
	
	public String getDataSpace() {
		return dataSpace;
	}

	public void setDataSpace(String dataSpace) {
		this.dataSpace = dataSpace;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	public String getImportConfigDataSpace() {
		return importConfigDataSpace;
	}

	public void setImportConfigDataSpace(String importConfigDataSpace) {
		this.importConfigDataSpace = importConfigDataSpace;
	}

	public String getImportConfigDataSet() {
		return importConfigDataSet;
	}

	public void setImportConfigDataSet(String importConfigDataSet) {
		this.importConfigDataSet = importConfigDataSet;
	}

	public String getImportConfigID() {
		return importConfigID;
	}

	public void setImportConfigID(String importConfigID) {
		this.importConfigID = importConfigID;
	}

	public boolean isWriteErrorToContext() {
		return writeErrorToContext;
	}

	public void setWriteErrorToContext(boolean writeErrorToContext) {
		this.writeErrorToContext = writeErrorToContext;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	public boolean isDisableTriggers() {
		return disableTriggers;
	}

	public void setDisableTriggers(boolean disableTriggers) {
		this.disableTriggers = disableTriggers;
	}
	
	public boolean isRunValidation() {
		return runValidation;
	}

	public void setRunValidation(boolean runValidation) {
		this.runValidation = runValidation;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context)
			throws OperationException {
		final Repository repo = context.getRepository();
		final Session session = context.getSession();
		
		LOG.info("Now running import for import Config id " + importConfigID );
		
		try {
			final AdaptationHome dataSpaceRef = repo.lookupHome(HomeKey.forBranchName(dataSpace));
			if (dataSpaceRef == null) {
				throw OperationException.createError("Data space " + dataSpace + " can't be found.");
			}
			final Adaptation dataSetRef = dataSpaceRef.findAdaptationOrNull(AdaptationName.forName(dataSet));
			if (dataSetRef == null) {
				throw OperationException.createError("Data set " + dataSet + " can't be found in data space " + dataSpace + ".");
			}
			
			final AdaptationHome importConfigDataSpaceRef = repo.lookupHome(
					HomeKey.forBranchName(importConfigDataSpace));
			if (importConfigDataSpaceRef == null) {
				throw OperationException.createError("Data space " + importConfigDataSpace + " can't be found.");
			}
			final Adaptation importConfigDataSetRef = importConfigDataSpaceRef.findAdaptationOrNull(
					AdaptationName.forName(importConfigDataSet));
			if (importConfigDataSetRef == null) {
				throw OperationException.createError("Data set " + importConfigDataSet + " can't be found in data space " + importConfigDataSpace + ".");
			}
			
			final BasicDataSetImporterFactory factory = new BasicDataSetImporterFactory();
			
			final DataSetImporter importer = factory.createImporter(importConfigDataSetRef, importConfigID);
			
			LOG.info("Triggers disabled = " + disableTriggers);
			
			importer.setDisableTriggers(disableTriggers);
			importer.setRunValidation(runValidation);
			importer.setEnableAllPrivileges(true);
			importer.setImportConfigName(importConfigID);
			importer.executeImport(session, dataSpaceRef, dataSetRef);
			
			LOG.info("Import Succeeded for import Config id " + importConfigID );
		} catch (OperationException ex) {
			LOG.error("DataSetImportScriptTask: Error while importing.", ex);
			if (writeErrorToContext) {
				errorMessage = ex.getMessage();
				errorDetails =  ExceptionUtils.getStackTrace(ex);
			} else {
				throw ex;
			}
		}
	}
}
