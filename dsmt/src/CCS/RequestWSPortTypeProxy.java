package CCS;

public class RequestWSPortTypeProxy implements CCS.RequestWSPortType {
  private boolean _useJNDI = true;
  private boolean _useJNDIOnly = false;
  private String _endpoint = null;
  private CCS.RequestWSPortType __requestWSPortType = null;
  
  public RequestWSPortTypeProxy() {
    _initRequestWSPortTypeProxy();
  }
  
  private void _initRequestWSPortTypeProxy() {
  
    if (_useJNDI || _useJNDIOnly) {
      try {
        javax.naming.InitialContext ctx = new javax.naming.InitialContext();
        __requestWSPortType = ((CCS.RequestWS)ctx.lookup("java:comp/env/service/RequestWS")).getRequestWSSoap();
      }
      catch (javax.naming.NamingException namingException) {
        if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
          System.out.println("JNDI lookup failure: javax.naming.NamingException: " + namingException.getMessage());
          namingException.printStackTrace(System.out);
        }
      }
      catch (javax.xml.rpc.ServiceException serviceException) {
        if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
          System.out.println("Unable to obtain port: javax.xml.rpc.ServiceException: " + serviceException.getMessage());
          serviceException.printStackTrace(System.out);
        }
      }
    }
    if (__requestWSPortType == null && !_useJNDIOnly) {
      try {
        __requestWSPortType = (new CCS.RequestWSLocator()).getRequestWSSoap();
        
      }
      catch (javax.xml.rpc.ServiceException serviceException) {
        if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
          System.out.println("Unable to obtain port: javax.xml.rpc.ServiceException: " + serviceException.getMessage());
          serviceException.printStackTrace(System.out);
        }
      }
    }
    if (__requestWSPortType != null) {
      if (_endpoint != null)
        ((javax.xml.rpc.Stub)__requestWSPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
      else
        _endpoint = (String)((javax.xml.rpc.Stub)__requestWSPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
    }
    
  }
  
  
  public void useJNDI(boolean useJNDI) {
    _useJNDI = useJNDI;
    __requestWSPortType = null;
    
  }
  
  public void useJNDIOnly(boolean useJNDIOnly) {
    _useJNDIOnly = useJNDIOnly;
    __requestWSPortType = null;
    
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (__requestWSPortType == null)
      _initRequestWSPortTypeProxy();
    if (__requestWSPortType != null)
      ((javax.xml.rpc.Stub)__requestWSPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public java.lang.String create(int projectId, java.lang.String requestorSOEID) throws java.rmi.RemoteException{
    if (__requestWSPortType == null)
      _initRequestWSPortTypeProxy();
    return __requestWSPortType.create(projectId, requestorSOEID);
  }
  
  
  public CCS.RequestWSPortType getRequestWSPortType() {
    if (__requestWSPortType == null)
      _initRequestWSPortTypeProxy();
    return __requestWSPortType;
  }
  
}