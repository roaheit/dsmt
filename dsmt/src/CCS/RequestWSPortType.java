/**
 * RequestWSPortType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190823.02 v62608112801
 */

package CCS;

public interface RequestWSPortType extends java.rmi.Remote {
    public java.lang.String create(int projectId, java.lang.String requestorSOEID) throws java.rmi.RemoteException;
}
