/**
 * RequestWSInformation.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190823.02 v62608112801
 */

package CCS;

public class RequestWSInformation implements com.ibm.ws.webservices.multiprotocol.ServiceInformation {

    @SuppressWarnings("rawtypes")
	private static java.util.Map operationDescriptions;
    @SuppressWarnings("rawtypes")
	private static java.util.Map typeMappings;

    static {
         initOperationDescriptions();
         initTypeMappings();
    }

    @SuppressWarnings("unchecked")
	private static void initOperationDescriptions() { 
        operationDescriptions = new java.util.HashMap();

        java.util.Map inner0 = new java.util.HashMap();

        java.util.List list0 = new java.util.ArrayList();
        inner0.put("create", list0);

        com.ibm.ws.webservices.engine.description.OperationDesc create0Op = _create0Op();
        list0.add(create0Op);

        operationDescriptions.put("RequestWSSoap",inner0);
        operationDescriptions = java.util.Collections.unmodifiableMap(operationDescriptions);
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _create0Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc create0Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "projectId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false, false, true),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "requestorSOEID"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, true),
          };
        if (_params0[0] instanceof com.ibm.ws.webservices.engine.configurable.Configurable) {
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_params0[0]).setOption("inputPosition","0");
        }
        if (_params0[1] instanceof com.ibm.ws.webservices.engine.configurable.Configurable) {
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_params0[1]).setOption("inputPosition","1");
        }
        create0Op = new com.ibm.ws.webservices.engine.description.OperationDesc("create", _params0, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "requestXML"));
        create0Op.setReturnType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        create0Op.setElementQName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "create"));
        if (create0Op instanceof com.ibm.ws.webservices.engine.configurable.Configurable) {
         ((com.ibm.ws.webservices.engine.configurable.Configurable)create0Op).setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "RequestWSPortType"));
         ((com.ibm.ws.webservices.engine.configurable.Configurable)create0Op).setOption("inputName","createRequest");
         ((com.ibm.ws.webservices.engine.configurable.Configurable)create0Op).setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "createResponse"));
         ((com.ibm.ws.webservices.engine.configurable.Configurable)create0Op).setOption("ResponseNamespace","http://CCS/RequestWS.tws");
         ((com.ibm.ws.webservices.engine.configurable.Configurable)create0Op).setOption("targetNamespace","http://CCS/RequestWS.tws");
         ((com.ibm.ws.webservices.engine.configurable.Configurable)create0Op).setOption("outputName","createResponse");
         ((com.ibm.ws.webservices.engine.configurable.Configurable)create0Op).setOption("ResponseLocalPart","createResponse");
         ((com.ibm.ws.webservices.engine.configurable.Configurable)create0Op).setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "createRequest"));
        }
        if (create0Op.getReturnParamDesc() instanceof com.ibm.ws.webservices.engine.configurable.Configurable) {
         ((com.ibm.ws.webservices.engine.configurable.Configurable)create0Op.getReturnParamDesc()).setOption("outputPosition","0");
        }
        com.ibm.ws.webservices.engine.description.FaultDesc _fault0 = null;
        return create0Op;

    }


    private static void initTypeMappings() {
        typeMappings = new java.util.HashMap();
        typeMappings = java.util.Collections.unmodifiableMap(typeMappings);
    }

    public java.util.Map getTypeMappings() {
        return typeMappings;
    }

    public Class getJavaType(javax.xml.namespace.QName xmlName) {
        return (Class) typeMappings.get(xmlName);
    }

    public java.util.Map getOperationDescriptions(String portName) {
        return (java.util.Map) operationDescriptions.get(portName);
    }

    public java.util.List getOperationDescriptions(String portName, String operationName) {
        java.util.Map map = (java.util.Map) operationDescriptions.get(portName);
        if (map != null) {
            return (java.util.List) map.get(operationName);
        }
        return null;
    }

}
