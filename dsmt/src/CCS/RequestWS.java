/**
 * RequestWS.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190823.02 v62608112801
 */

package CCS;

public interface RequestWS extends javax.xml.rpc.Service {
    public java.lang.String getRequestWSSoapAddress();
    public CCS.RequestWSPortType getRequestWSSoap() throws javax.xml.rpc.ServiceException;
    public CCS.RequestWSPortType getRequestWSSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;

}
