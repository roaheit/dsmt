/**
 * RequestWSSoapSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190823.02 v62608112801
 */

package CCS;

public class RequestWSSoapSoapBindingStub extends com.ibm.ws.webservices.engine.client.Stub implements CCS.RequestWSPortType {
    public RequestWSSoapSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws com.ibm.ws.webservices.engine.WebServicesFault {
        if (service == null) {
            super.service = new com.ibm.ws.webservices.engine.client.Service();
        }
        else {
            super.service = service;
        }
        super.engine = ((com.ibm.ws.webservices.engine.client.Service) super.service).getEngine();
        initTypeMapping();
        super.cachedEndpoint = endpointURL;
        super.connection = ((com.ibm.ws.webservices.engine.client.Service) super.service).getConnection(endpointURL);
        super.messageContexts = new com.ibm.ws.webservices.engine.MessageContext[1];
    }

    private void initTypeMapping() {
        javax.xml.rpc.encoding.TypeMapping tm = super.getTypeMapping(com.ibm.ws.webservices.engine.Constants.URI_LITERAL_ENC);
        java.lang.Class javaType = null;
        javax.xml.namespace.QName xmlType = null;
        com.ibm.ws.webservices.engine.encoding.SerializerFactory sf = null;
        com.ibm.ws.webservices.engine.encoding.DeserializerFactory df = null;
    }

    private com.ibm.ws.webservices.engine.description.OperationDesc _createOperation0 = null;
    private com.ibm.ws.webservices.engine.description.OperationDesc _getcreateOperation0() {
        if (_createOperation0 == null) {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "projectId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false, false, true),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "requestorSOEID"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, true),
          };
        if (_params0[0] instanceof com.ibm.ws.webservices.engine.configurable.Configurable) {
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_params0[0]).setOption("inputPosition","0");
        }
        if (_params0[1] instanceof com.ibm.ws.webservices.engine.configurable.Configurable) {
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_params0[1]).setOption("inputPosition","1");
        }
        _createOperation0 = new com.ibm.ws.webservices.engine.description.OperationDesc("create", _params0, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "requestXML"));
        _createOperation0.setReturnType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        _createOperation0.setElementQName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "create"));
        _createOperation0.setSoapAction("http://CCS/RequestWS.tws/create");
        if (_createOperation0 instanceof com.ibm.ws.webservices.engine.configurable.Configurable) {
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_createOperation0).setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "RequestWSPortType"));
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_createOperation0).setOption("inputName","createRequest");
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_createOperation0).setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "createResponse"));
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_createOperation0).setOption("ResponseNamespace","http://CCS/RequestWS.tws");
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_createOperation0).setOption("targetNamespace","http://CCS/RequestWS.tws");
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_createOperation0).setOption("outputName","createResponse");
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_createOperation0).setOption("ResponseLocalPart","createResponse");
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_createOperation0).setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://CCS/RequestWS.tws", "createRequest"));
        }
        if (_createOperation0.getReturnParamDesc() instanceof com.ibm.ws.webservices.engine.configurable.Configurable) {
         ((com.ibm.ws.webservices.engine.configurable.Configurable)_createOperation0.getReturnParamDesc()).setOption("outputPosition","0");
        }
        com.ibm.ws.webservices.engine.description.FaultDesc _fault0 = null;
        }
        return _createOperation0;
    }

    private int _createIndex0 = 0;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getcreateInvoke0(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_createIndex0];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(_getcreateOperation0());
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("http://CCS/RequestWS.tws/create");
            mc.setOperationStyle("wrapped");
            mc.setOperationUse("literal");
            mc.setEncodingStyle(com.ibm.ws.webservices.engine.Constants.URI_LITERAL_ENC);
            mc.setProperty(com.ibm.ws.webservices.engine.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
            mc.setProperty(com.ibm.ws.webservices.engine.WebServicesEngine.PROP_DOMULTIREFS, Boolean.FALSE);
            super.primeMessageContext(mc);
            super.messageContexts[_createIndex0] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String create(int projectId, java.lang.String requestorSOEID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getcreateInvoke0(new java.lang.Object[] {new java.lang.Integer(projectId), requestorSOEID}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            throw wsf;
        } 
        try {
            return (java.lang.String) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String.class);
        }
    }

}
