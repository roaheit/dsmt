/**
 * RequestWSLocator.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190823.02 v62608112801
 */

package CCS;

public class RequestWSLocator extends com.ibm.ws.webservices.multiprotocol.AgnosticService implements com.ibm.ws.webservices.multiprotocol.GeneratedService, CCS.RequestWS {

    public RequestWSLocator() {
        super(com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
           "http://CCS/RequestWS.tws",
           "RequestWS"));

        context.setLocatorName("CCS.RequestWSLocator");
    }

    public RequestWSLocator(com.ibm.ws.webservices.multiprotocol.ServiceContext ctx) {
        super(ctx);
        context.setLocatorName("CCS.RequestWSLocator");
    }

    // Use to get a proxy class for requestWSSoap

     // SOAP Port
    private final java.lang.String requestWSSoap_address = "http://ccgfibpm10d.nam.nsroot.net:30120/teamworks/webservices/CCS/RequestWS.tws";

    public java.lang.String getRequestWSSoapAddress() {
        return requestWSSoap_address;
    }

    private java.lang.String requestWSSoapPortName = "RequestWSSoap";

    // The WSDD port name defaults to the port name.
    private java.lang.String requestWSSoapWSDDPortName = "RequestWSSoap";

    public java.lang.String getRequestWSSoapWSDDPortName() {
        return requestWSSoapWSDDPortName;
    }

    public void setRequestWSSoapWSDDPortName(java.lang.String name) {
        requestWSSoapWSDDPortName = name;
    }

    public CCS.RequestWSPortType getRequestWSSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(requestWSSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            return null; // unlikely as URL was validated in WSDL2Java
        }
        return getRequestWSSoap(endpoint);
    }

    public CCS.RequestWSPortType getRequestWSSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        CCS.RequestWSPortType _stub =
            (CCS.RequestWSPortType) getStub(
                requestWSSoapPortName,
                (String) getPort2NamespaceMap().get(requestWSSoapPortName),
                CCS.RequestWSPortType.class,
                "CCS.RequestWSSoapSoapBindingStub",
                portAddress.toString());
        if (_stub instanceof com.ibm.ws.webservices.engine.client.Stub) {
            ((com.ibm.ws.webservices.engine.client.Stub) _stub).setPortName(requestWSSoapWSDDPortName);
        }
        return _stub;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (CCS.RequestWSPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                return getRequestWSSoap();
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("WSWS3273E: Error: There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        String inputPortName = portName.getLocalPart();
        if ("RequestWSSoap".equals(inputPortName)) {
            return getRequestWSSoap();
        }
        else  {
            throw new javax.xml.rpc.ServiceException();
        }
    }

    public void setPortNamePrefix(java.lang.String prefix) {
        requestWSSoapWSDDPortName = prefix + "/" + requestWSSoapPortName;
    }

    public javax.xml.namespace.QName getServiceName() {
        return super.getServiceName();
    }

    private java.util.Map port2NamespaceMap = null;

    protected java.util.Map getPort2NamespaceMap() {
        if (port2NamespaceMap == null) {
            port2NamespaceMap = new java.util.HashMap();
            port2NamespaceMap.put(
               "RequestWSSoap",
               "http://schemas.xmlsoap.org/wsdl/soap/");
        }
        return port2NamespaceMap;
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            String serviceNamespace = getServiceName().getNamespaceURI();
            for (java.util.Iterator i = getPort2NamespaceMap().keySet().iterator(); i.hasNext(); ) {
                ports.add(
                    com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                        serviceNamespace,
                        (String) i.next()));
            }
        }
        return ports.iterator();
    }

    public javax.xml.rpc.Call[] getCalls(javax.xml.namespace.QName portName) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName should not be null.");
        }
        if  (portName.getLocalPart().equals("RequestWSSoap")) {
            return new javax.xml.rpc.Call[] {
                createCall(portName, "create", "createRequest"),
            };
        }
        else {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName should not be null.");
        }
    }
}
