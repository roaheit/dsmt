SELECT A.CURRENCY_CD, n'~', A.DESCR120, n'~', D.DESCR120, n'~', C.DECIMAL_POSITIONS, n'~', A.COUNTRY_2CHAR, n'~', n' '
  FROM EBX_C_DSMT_CURR_REL A	-- A.C_DSMT_NEW_CNTRY = n' '
  , EBX_C_DSMT_CURR C		-- max effdt
  , EBX_C_DSMT_CURR D	-- max effdt
	WHERE A.CURRENCY_CD = C.CURRENCY_CD
     AND C.CURRENCY_CD = A.CURRENCY_CD
     AND C.CURRENCY_CD = D.CURRENCY_CD
       AND A.COUNTRY = n' '
    AND (A.enddt is null or A.enddt ='31-Dec-9999')
    AND (C.enddt is null or C.enddt ='31-Dec-9999')
	AND (D.enddt is null or D.enddt ='31-Dec-9999')
UNION
SELECT  B.CURRENCY_CD, n'~', B.DESCR120, n'~', G.DESCR120, n'~', E.DECIMAL_POSITIONS, n'~', B.COUNTRY_2CHAR, n'~', H.DESCR120
  FROM EBX_C_DSMT_CURR_REL B		-- EBX_C_DSMT_CURR_REL 	B
  , EBX_C_DSMT_CURR E			-- EBX_C_DSMT_CURR 		E/G	MAX EFFDT
  , EBX_C_DSMT_NEW_CNTRY F		--	EBX_C_DSMT_CNTRY2	F/H	MAX EFFDT
  , EBX_C_DSMT_CURR G
  , EBX_C_DSMT_NEW_CNTRY H
  WHERE B.CURRENCY_CD = E.CURRENCY_CD
     AND E.CURRENCY_CD = B.CURRENCY_CD
     AND F.COUNTRY_2CHAR = B.COUNTRY_2CHAR
     AND B.EFF_STATUS = n'A'								-- Active currency codes
     AND E.EFF_STATUS = n'A'
      AND G.EFF_STATUS = n'A'
     AND E.CURRENCY_CD = G.CURRENCY_CD
     AND F.COUNTRY = H.COUNTRY
    AND NOT EXISTS (SELECT I.COUNTRY							-- Country code on Currency Rel max-effdt row is not blank
  FROM EBX_C_DSMT_CURR_REL I
  WHERE I.EFFDT =
        (SELECT MAX(I_ED.EFFDT) FROM EBX_C_DSMT_CURR_REL I_ED
        WHERE I.CURRENCY_CD = I_ED.CURRENCY_CD
          AND I.COUNTRY = I_ED.COUNTRY
          AND I_ED.EFFDT <= SYSDATE)
     AND I.CURRENCY_CD = B.CURRENCY_CD
     AND I.COUNTRY = n' ')
     AND B.CURRENCY_CD <> n'EUR'								-- Non-European Currency codes
     AND (E.enddt is null or E.enddt ='31-Dec-9999')
     AND (G.enddt is null or G.enddt ='31-Dec-9999')
	 AND (B.enddt is null or B.enddt ='31-Dec-9999')
     AND H.EFFDT =
        (SELECT MAX(N_ED.EFFDT) FROM EBX_C_DSMT_NEW_CNTRY N_ED
        WHERE H.COUNTRY = N_ED.COUNTRY
          AND N_ED.EFFDT <= SYSDATE)
     UNION
SELECT J.CURRENCY_CD, n'~', J.DESCR120, n'~', M.DESCR120, n'~', K.DECIMAL_POSITIONS, n'~', J.COUNTRY_2CHAR, n'~', N.DESCR120
  FROM EBX_C_DSMT_CURR_REL J				--EBX_C_DSMT_CURR_REL 	J
  , EBX_C_DSMT_CURR K					--EBX_C_DSMT_CURR 		K/M	- MAX EFFDT
  , EBX_C_DSMT_NEW_CNTRY L					--- EBX_C_DSMT_CNTRY2 	L/N	- MAX EFFDT
  , EBX_C_DSMT_CURR M
  , EBX_C_DSMT_NEW_CNTRY N
  WHERE J.CURRENCY_CD = K.CURRENCY_CD
     AND K.CURRENCY_CD = J.CURRENCY_CD
     AND L.COUNTRY_2CHAR = J.COUNTRY_2CHAR
     AND J.EFF_STATUS = n'A'								-- Currency code is Active
     AND K.EFF_STATUS = n'A'
     AND K.CURRENCY_CD = M.CURRENCY_CD
     AND L.COUNTRY = N.COUNTRY
	  AND (M.enddt is null or M.enddt ='31-Dec-9999')
	  AND (J.enddt is null or J.enddt ='31-Dec-9999')
     AND N.EFFDT =
        (SELECT MAX(N_ED.EFFDT) FROM EBX_C_DSMT_NEW_CNTRY N_ED
        WHERE N.COUNTRY = N_ED.COUNTRY
          AND N_ED.EFFDT <= SYSDATE)
     AND J.CURRENCY_CD = n'EUR'								-- European Currency
      and m.enddt is null