<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="com.orchestranetworks.service.ServiceContext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">
function enableFunction()
		{ 
			
			var dropdownvalue=document.getElementById("maintenanceType").value;
				if(dropdownvalue=='functional')
					{
					//		alert("enable");
					 		if(document.getElementById("isfunctionalEndDate").value== "false")
								{				
									document.getElementById("submitButton").disabled =true;
									alert("Functional cycle is currently not open. Only regular requests are permitted.");
								}
								else{
										document.getElementById("functiondiv").style.visibility ="visible" ;
					 					document.getElementById("functiondiv").style.display="block";
								}
					}
					else{
						document.getElementById("functionalID").value="";
					 	document.getElementById("functiondiv").style.visibility  ="hidden";
					 	document.getElementById("functiondiv").style.display="none";
					    document.getElementById("submitButton").disabled =false;
					    if(document.getElementById("isValidSID").value.indexOf("false")== 0)
						  		{
						  		 document.getElementById("submitButton").disabled =true;
						  		 }
						  		else{
						  		document.getElementById("submitButton").disabled =false;						  		
						  				 	 }
					 }
					 
					
		}
		
function enableFunctional()
{

	

}



	
function validatevalue()
{
 document.getElementById("submitButton").disabled =true;
var teststyl = document.getElementById("functiondiv").style.visibility;
var functionValue = document.getElementById("functionalID").value;
var sidValue= document.getElementById("sid").value;
var actionvalue= document.getElementById("actionid").value;
var validationCheck= document.getElementById("validationCheck").checked;
	//alert("validatioCheck >>"+ validatioCheck);
        if(""==sidValue||sidValue==null )
        {
        alert("Please Enter a value for SID");
        document.getElementById("sid").focus();
        document.getElementById("submitButton").disabled =false;
		return false;
        }
        else if (!validationCheck)
		{
		alert("Please select the Check box to proceed.");
		document.getElementById("validationCheck").focus();
		document.getElementById("submitButton").disabled =false;
		return false;
		}
		else if(teststyl=="visible" && (""==functionValue||functionValue==null ) )
		{
		alert("Please Enter a Functional ID.");
		document.getElementById("functionalID").focus();
		 document.getElementById("submitButton").disabled =false;
		return false;
		
		}
		
		else
		{
		
		//alert("inside submit");
		document.getElementById("GOCform").action=actionvalue;
		document.getElementById("GOCform").submit();
		}

}	

function getXMLHttpRequest() {  
 var xmlHttpReq = false;   
 // to create XMLHttpRequest object in non-Microsoft browsers   
 if (window.XMLHttpRequest) {     
	 xmlHttpReq = new XMLHttpRequest();   
	 } else if (window.ActiveXObject) {  
	    try {      
	     // to create XMLHttpRequest object in later versions       
	     // of Internet Explorer      
	      xmlHttpReq = new ActiveXObject("Msxml2.XMLHTTP");     
	      } catch (exp1) {       
	      try {        
	       // to create XMLHttpRequest object in older versions         
	       // of Internet Explorer        
	        xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");      
	         } catch (exp2) {        
	          xmlHttpReq = false;      
	           }
	         }  
	     }   
	 return xmlHttpReq;
 }
 
 function checkSID()
{

	var xmlHttpRequest = getXMLHttpRequest();   
	xmlHttpRequest.onreadystatechange = getReadySID(xmlHttpRequest);  
	var sidValue= document.getElementById("sid").value;
	var actionvalue= document.getElementById("calenderActionId").value;
	actionvalue=actionvalue+"&sid="+sidValue+"&ajaxOperation=getSID";
	//alert("actionvalue"+actionvalue);
	xmlHttpRequest.open("POST", actionvalue, true);  
	xmlHttpRequest.setRequestHeader("Content-Type", "application/text");  
	xmlHttpRequest.send(null); 
}

function getReadySID(xmlHttpRequest) {    
 // an anonymous function returned  
  // it listens to the XMLHttpRequest instance   
  	return function() {     
  						if (xmlHttpRequest.readyState == 4) {      
  							 if (xmlHttpRequest.status == 200) {  
  							 	 var sidValue=  xmlHttpRequest.responseText; 
  							 
		  								 	var responseIndex= sidValue.indexOf("<");
		  							 		var isValidSID =sidValue.substring(0,responseIndex);
		  							 		//	alert("isValidSID >> " +isValidSID);
		  									document.getElementById("isValidSID").value =isValidSID;
		  									 if(isValidSID.indexOf("true")== 0)
						  							 	 {
						  							 	
						  							 	 document.getElementById("submitButton").disabled =false;
						  							 	 document.getElementById("isfunctionalEndDate").value ="true";
						  							 	 }
						  							 	 else if(isValidSID.indexOf("false")== 0)
						  							 	 {
						  							 	  alert("Entered SID is either inactive or not present in SID Enhancement table");
						  							 	   document.getElementById("isfunctionalEndDate").value ="true";
						  							 	 document.getElementById("submitButton").disabled =true;
						  							 	 }
						  							 	  else if(isValidSID.indexOf("error")== 0)
						  							 	 {
						  							 	 
						  							 	  document.getElementById("isfunctionalEndDate").value ="false";
						  							 	  enableFunction();
						  							 	 }
						  							 	
		  							 		
		  							      }  else {        
		  							      		 alert("HTTP error " + xmlHttpRequest.status + ": " + xmlHttpRequest.statusText);       
		  							      		 }    
		  							 }   
  			};
  			
  	 } 

function loadClenderDate()
{

	var xmlHttpRequest = getXMLHttpRequest();   
	xmlHttpRequest.onreadystatechange = getReadyStateHandler(xmlHttpRequest);  
	var sidValue= document.getElementById("sid").value;
	var maintanceType= document.getElementById("maintenanceType").value; 
	var actionvalue= document.getElementById("calenderActionId").value;
	actionvalue=actionvalue+"&sid="+sidValue+"&maintenanceType="+maintanceType+"&ajaxOperation=loadCalender";
	//alert("actionvalue"+actionvalue);
	xmlHttpRequest.open("POST", actionvalue, true);  
	xmlHttpRequest.setRequestHeader("Content-Type", "application/text");  
	xmlHttpRequest.send(null); 
}


//get data for Calender date
function getReadyStateHandler(xmlHttpRequest) {    
 // an anonymous function returned  
  // it listens to the XMLHttpRequest instance   
  	return function() {     
  						if (xmlHttpRequest.readyState == 4) {      
  							 if (xmlHttpRequest.status == 200) {  
  							 	 var calenderdat=  xmlHttpRequest.responseText; 
  							 	
		  								 	var dateDiffIndex= calenderdat.indexOf("<");
		  							 		var dateDiff =calenderdat.substring(0,dateDiffIndex);
		  							
		  							 		//	 document.getElementById("calendeId").value =dateDiff;
		  							 		var col=document.getElementById("calenderData");
									
		  							 
				  							 	 if(dateDiff.indexOf("error")!= -1)
						  							 	 {
						  							 	 col.style.color="red";
						  							 	col.innerHTML = "Selected Maintenance Type is not defined in the Calendar Set up table under GOC_WF data space";  
						  							 	 }
						  							 	 else
						  							 	 {
						  							 	  col.style.color="green";
						  							 	  col.innerHTML = "Selected Maintenance cycle ends on date : " + dateDiff + " EST";
						  							 	 }
		  							      }  else {        
		  							      		 alert("HTTP error " + xmlHttpRequest.status + ": " + xmlHttpRequest.statusText);       
		  							      		 }    
		  							 }   
  			};
  			
  	 } 



</script>
<%
    // This is a scriptlet.  Notice that the "date"
    final ServiceContext sContext = ServiceContext.getServiceContext(request);
    String action= sContext.getURLForIncludingResource("/GOCRequestChanges");
    String ajaxAction= sContext.getURLForIncludingResource("/GOCWorkflowCalenderservlet");
   // System.out.println("action URL = " + action);
%>
<title>GOC Workflow Request</title>
</head>
<body>
 <form id="GOCform" action=""      method="post" >
		   <table>
		     <tbody>
			       <tr >
				         <td class="ebx_Label" >
				           <label >SID :</label>
				         </td>
				         <td class="ebx_Input">
				           <input type="text" id="sid" name="sid" value="" onblur="checkSID()"/>
				         </td>
			       </tr>
			        <tr >
				         <td>
				           <label >Maintenance Type :</label>
				         </td>
				         <td class="ebx_attribute" >
				         <select id="maintenanceType"  style="width:135px"  name="maintenanceType" onchange="enableFunction()">
							 <option value="regular">Regular</option>
							 <option value="functional">Functional</option>
							</select>	
				         </td>
			       </tr>
			        <tr id="functiondiv" style="visibility:hidden;display:none;"  >
					         <td >
					           <label >Functional ID :</label>
					         </td>
					         <td class="ebx_Input">
					         	 <input type="text" id="functionalID" name="functionalID" value="" />
					         </td>
			       </tr>
		      
				</tbody>
		   </table>
		   <table>
		   <tr>
		   <td>
		   <label class="ebx_Label"><input id="validationCheck" type="checkbox"  onclick="loadClenderDate()">I agree that I will attach necessary attestation for any GOC Deactivation and/or Purge request.</label>
		   </td>
		   </tr>
		   <tr>
		     <td id="calenderData"> </td>
		     </tr>
		     
		   <tr><td>
		   <button type="button" id="submitButton" name ="submitButton" class="ebx_Button" onclick="validatevalue()">Launch GOC Workflow Request</button>
		    <button type="button" class="ebx_Button" onclick="loadClenderDate()">Refresh Calendar Days Remaining</button>
		   </td>
		  
		     
		    </table>
		     <input type="hidden" id="calenderActionId" name="calenderActionId" value="<%= ajaxAction %>"/>
		     <input type="hidden" id="actionid" name="actionid" value="<%= action %>"/>
		     <input type="hidden" id="isValidSID" name="isValidSID" value="true"/>
		      <input type="hidden" id="isfunctionalEndDate" name="isfunctionalEndDate" value="true"/>
		  </form>
</body>
</html>