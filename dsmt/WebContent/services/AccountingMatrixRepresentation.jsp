<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.orchestranetworks.ps.util.AcctMatHierRepresentation,javax.servlet.http.HttpSession" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script>
var searchTyp ="";
var callAccMatService = function(obj) {
	var lvid = obj.value;
	if(lvid != "" && lvid.length>1) {
		searchTyp = obj.id;
		var ajaxURL = document.getElementById("Hid_ajax_URL").value+"&srchType="+obj.id+"&"+obj.id+"="+obj.value;
		ajaxURL = ajaxURL.replace("AccMatEscpopupService/","AccMatEscpopupService");
		ajaxURL = ajaxURL.replace("%40","@");
		var xhttp = new XMLHttpRequest();
	  	xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	document.getElementById("resultDIV").innerHTML = "";
		    	var ajaxResponse = this.responseText;
		     	document.getElementById("resultDIV").innerHTML = ajaxResponse.substring(ajaxResponse.indexOf("<AccMatrixAjaxResp>")+19,ajaxResponse.indexOf("</AccMatrixAjaxResp>"));
		     	document.getElementById("resultDIV").style.display = "block";
		    }
	  	};
		xhttp.open("POST", ajaxURL, true);
		xhttp.send();
	}
	else 
		closePopUP();
};
var closePopUP = function(){
	document.getElementById("resultDIV").style.display = "none";
};
var selectRW = function(val){
	document.getElementById(searchTyp).value = val;
	closePopUP();
};
function changeVal(obj){
	document.getElementById("LVID").value = obj.value.toUpperCase();
}
</script>
<style>
tr:hover.trEven{
	background-color:#c8cfd6;cursor: pointer; cursor: hand;
}
tr:hover.trOdd{
	background-color:#c8cfd6;cursor: pointer; cursor: hand;
}
.msDivstyl{
	border: 0px solid red; border-image: none; width: 330px; height: 305px; margin-top: -76px; margin-left: 180px;background-color:#FFFFFF;position:absolute;z-index:10;
}
.lvDivstyl{
	border: 0px solid red; border-image: none; width: 330px; height: 305px; margin-top: -40px; margin-left: 180px;background-color:#FFFFFF;position:absolute;z-index:10;
}
.mgDivstyl{
	border: 0px solid red; border-image: none; width: 330px; height: 305px; margin-top: -2px; margin-left: 180px;background-color:#FFFFFF;position:absolute;z-index:10;
}
.innerDispStyle {
	overflow-y:scroll;background-color:#FFFFFF;
	border: 1px solid grey;height:300px;border-bottom-left-radius:2px;border-bottom-right-radius:2px;
}
.tabHDStyle {
	background-color:#A8C0EF;border-top: 1px solid grey;border-right: 1px solid grey;border-left: 1px solid grey;color:#FFFFFF;padding-left:5px;border-top-left-radius:2px;border-top-right-radius:2px;
}
.butnstyl{
	background-color:#bacdf1;color: #FFFFFF;font-weight:bold;border:1px solid #65738F;padding 0 8px;border-radius:4px;
}
.regbutnstyl{
	background-color:#e6ebf6;border:1px solid #C0C0C0;padding 0 8px;border-radius:4px;
}
.divstyl{
	margin-left:87px;margin-top:20px;padding:5px;height:20px;
}

.trEven{
	background-color:#F8F8F8;padding:2px;
}	
.trOdd{
	background-color:#FFFFFF;padding:2px;
}
.errStyl{
	margin: 8px 0;color: #FF0000;
}
.resStyl{
	margin: 8px 0;color: #008000;
}
.alrtDVStyl{
	margin: 25px 0 0 32px;color: #FF0000;position:absolute;
}
</style>

<%
try {
	final AcctMatHierRepresentation form = new AcctMatHierRepresentation();
	form.setServlet("/AcctMatRepresentationServlet");
	form.service(request, response);
} catch (final Exception ex) {
	%><p style="margin:10px;color:red">(!) <%=ex.getMessage()%></p><%
}
%>