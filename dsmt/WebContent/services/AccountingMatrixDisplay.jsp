<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.orchestranetworks.ps.util.AcctMatHierDisplay" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
try {
	final AcctMatHierDisplay form = new AcctMatHierDisplay();
	form.setServlet("/AcctMatDisplayServlet");
	//form.setDefaultImportConfigDataSpace(...);
	//form.setDefaultImportConfigDataSet(...);
	form.service(request, response);
} catch (final Exception ex) {
	%><p style="margin:10px;color:red">(!) <%=ex.getMessage()%></p><%
}
%>