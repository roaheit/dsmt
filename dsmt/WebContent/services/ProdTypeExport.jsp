<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.orchestranetworks.ps.exporter.servlet.DataSetExportForm" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
try {
	final DataSetExportForm form = new DataSetExportForm();
	form.setServlet("/ProdTypeExport");
	form.service(request, response);
} catch (final Exception ex) {
	%><p style="margin:10px;color:red">(!) <%=ex.getMessage()%></p><%
}
%>