<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="com.orchestranetworks.service.ServiceContext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">
function enableFunction()
		{ 
			
			var dropdownvalue=document.getElementById("maintenanceType").value;
				if(dropdownvalue=='functional')
					{
					 		if(document.getElementById("isfunctionalEndDate").value== "false")
								{				
									document.getElementById("submitButton").disabled =true;
									alert("Functional cycle is currently not open. Only regular requests are permitted.");
								}
								
					}
					else{
						document.getElementById("functiondiv").style.visibility  ="hidden";
					 	document.getElementById("functiondiv").style.display="none";
					    document.getElementById("submitButton").disabled =false;
				
					 }
					 
					
		}
		
function enableFunctional()
{

	

}



	
function validatevalue()
{
 document.getElementById("submitButton").disabled =true;
var teststyl = document.getElementById("functiondiv").style.visibility;
//var functionValue = document.getElementById("functionalID").value;
var actionvalue= document.getElementById("actionid").value;
var validationCheck= document.getElementById("validationCheck").checked;
var dropdownvalue=document.getElementById("maintenanceType").value;
        if (!validationCheck)
		{
		alert("Please select the Check box to proceed.");
		document.getElementById("validationCheck").focus();
		document.getElementById("submitButton").disabled =false;
		return false;
		}
		
		else
		{
		document.getElementById("GOCform").action=actionvalue;
		document.getElementById("GOCform").submit();
		}

}	

function getXMLHttpRequest() {  
 var xmlHttpReq = false;   
 // to create XMLHttpRequest object in non-Microsoft browsers   
 if (window.XMLHttpRequest) {     
	 xmlHttpReq = new XMLHttpRequest();   
	 } else if (window.ActiveXObject) {  
	    try {      
	     // to create XMLHttpRequest object in later versions       
	     // of Internet Explorer      
	      xmlHttpReq = new ActiveXObject("Msxml2.XMLHTTP");     
	      } catch (exp1) {       
	      try {        
	       // to create XMLHttpRequest object in older versions         
	       // of Internet Explorer        
	        xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");      
	         } catch (exp2) {        
	          xmlHttpReq = false;      
	           }
	         }  
	     }   
	 return xmlHttpReq;
 }
 
 function checkSID()
{

	var xmlHttpRequest = getXMLHttpRequest();   
	xmlHttpRequest.onreadystatechange = getReadySID(xmlHttpRequest);  
	var sidValue= document.getElementById("sid").value;
	var actionvalue= document.getElementById("calenderActionId").value;
	actionvalue=actionvalue+"&sid="+sidValue+"&ajaxOperation=getSID";
	xmlHttpRequest.open("POST", actionvalue, true);  
	xmlHttpRequest.setRequestHeader("Content-Type", "application/text");  
	xmlHttpRequest.send(null); 
}


function checkFunctionID(){
	var xmlHttpRequest = getXMLHttpRequest();   
	xmlHttpRequest.onreadystatechange = getFunctionID(xmlHttpRequest);  
	//var functionValue= document.getElementById("functionalID").value;
	var actionvalue= document.getElementById("calenderActionId").value;
	actionvalue=actionvalue+"&ajaxOperation=validateFunctionID";
	xmlHttpRequest.open("POST", actionvalue, true);  
	xmlHttpRequest.setRequestHeader("Content-Type", "application/text");  
	xmlHttpRequest.send(null); 

}


function getReadySID(xmlHttpRequest) {    
 // an anonymous function returned  
  // it listens to the XMLHttpRequest instance   
  	return function() {     
  						if (xmlHttpRequest.readyState == 4) {      
  							 if (xmlHttpRequest.status == 200) {  
  							 	 var sidValue=  xmlHttpRequest.responseText; 
  							 
		  								 	var responseIndex= sidValue.indexOf(";");
		  								 	var angleIndex=sidValue.indexOf("<");
		  								 	var managementOnly= sidValue.substring(responseIndex+1,angleIndex);
		  							 		var isValidSID =sidValue.substring(0,responseIndex);
		  							 		//	alert("isValidSID >> " +isValidSID);
		  									document.getElementById("isValidSID").value =isValidSID;
		  									if(null!=managementOnly){
		  								 	managementOnly=managementOnly.trim();
		  								 	}
		  									document.getElementById("managementOnly").value =managementOnly;
		  									 if(isValidSID.indexOf("true")== 0)
						  							 	 {
						  							 	
						  							 	 document.getElementById("submitButton").disabled =false;
						  							 	 document.getElementById("isfunctionalEndDate").value ="true";
						  							 	 }
						  							 	 else if(isValidSID.indexOf("false")== 0)
						  							 	 {
						  							 	  alert("Entered SID is either inactive or not present in SID Enhancement/SID BU table");
						  							 	   document.getElementById("isfunctionalEndDate").value ="true";
						  							 	 document.getElementById("submitButton").disabled =true;
						  							 	 }
						  							 	  else if(isValidSID.indexOf("error")== 0)
						  							 	 {
						  							 	 
						  							 	  document.getElementById("isfunctionalEndDate").value ="false";
						  							 	  //enableFunction();
						  							 	 }
						  							 	
		  							 		
		  							      }  else {        
		  							      		 alert("HTTP error " + xmlHttpRequest.status + ": " + xmlHttpRequest.statusText);       
		  							      		 }    
		  							 }   
  			};
  			
  	 } 



function getFunctionID(xmlHttpRequest) {    
 // an anonymous function returned  
  // it listens to the XMLHttpRequest instance   
  	return function() {     
  						if (xmlHttpRequest.readyState == 4) {      
  							 if (xmlHttpRequest.status == 200) {  
  							 	 var functionidValue=  xmlHttpRequest.responseText; 
  							 
		  								 	var responseIndex= functionidValue.indexOf(";");
		  							 		var isValidFunctionID =functionidValue.substring(0,responseIndex);
		  									if(isValidFunctionID.indexOf("true")== 0)
						  							 	 {
						  							 	
						  							 	 document.getElementById("submitButton").disabled =false;
						  							 	 
						  							 	 }
						  							 	 else if(isValidFunctionID.indexOf("false")== 0)
						  							 	 {
						  							 	  alert("Entered Functional ID is not present in Function ID table");
						  							 	   document.getElementById("isfunctionalEndDate").value ="true";
						  							 	 document.getElementById("submitButton").disabled =true;
						  							 	 }
						  							 	  else if(isValidFunctionID.indexOf("error")== 0)
						  							 	 {
						  							 	 
						  							 	  document.getElementById("isfunctionalEndDate").value ="false";
						  							 	  enableFunction();
						  							 	 }
						  							 	
		  							 		
		  							      }  else {        
		  							      		 alert("HTTP error " + xmlHttpRequest.status + ": " + xmlHttpRequest.statusText);       
		  							      		 }    
		  							 }   
  			};
  			
  	 }




function loadClenderDate()
{

	var xmlHttpRequest = getXMLHttpRequest();   
	xmlHttpRequest.onreadystatechange = getReadyStateHandler(xmlHttpRequest);  
	var sidValue= null;
	var maintanceType= document.getElementById("maintenanceType").value; 
	var actionvalue= document.getElementById("calenderActionId").value;
	actionvalue=actionvalue+"&sid="+sidValue+"&maintenanceType="+maintanceType+"&ajaxOperation=loadCalender";
	xmlHttpRequest.open("POST", actionvalue, true);  
	xmlHttpRequest.setRequestHeader("Content-Type", "application/text");  
	xmlHttpRequest.send(null); 
}


//get data for Calender date
function getReadyStateHandler(xmlHttpRequest) {    
 // an anonymous function returned  
  // it listens to the XMLHttpRequest instance   
  	return function() {     
  						if (xmlHttpRequest.readyState == 4) {      
  							 if (xmlHttpRequest.status == 200) {  
  							 	 var calenderdat=  xmlHttpRequest.responseText; 
  							 	
		  								 	var dateDiffIndex= calenderdat.indexOf(";");
		  								 	
		  								 	var angleIndex=calenderdat.indexOf("<");
		  								 	var managementOnly= calenderdat.substring(dateDiffIndex+1,angleIndex);
		  								 	
		  								 	if(null!=managementOnly){
		  								 	managementOnly=managementOnly.trim();
		  								 	}
		  								 		  							 		
		  									document.getElementById("managementOnly").value =managementOnly;
		  							 		var dateDiff =calenderdat.substring(0,dateDiffIndex);
		  							 		var col=document.getElementById("calenderData");
				  							 	 if(dateDiff.indexOf("error")!= -1)
						  							 	 {
						  							 	 col.style.color="red";
						  							 	col.innerHTML = "Selected Maintenance Type is not defined in the Calendar Set up table under GOC_WF data space";  
						  							 	 }
						  							 	 else
						  							 	 {
						  							 	  col.style.color="green";
						  							 	  col.innerHTML = "Selected Maintenance cycle ends on date : " + dateDiff + " EST";
						  							 	 }
		  							      }  else {        
		  							      		 alert("HTTP error " + xmlHttpRequest.status + ": " + xmlHttpRequest.statusText);       
		  							      		 }    
		  							 }   
  			};
  			
  	 } 


	
</script>
<%
    // This is a scriptlet.  Notice that the "date"
    final ServiceContext sContext = ServiceContext.getServiceContext(request);
    String action= sContext.getURLForIncludingResource("/IGWGOCRequestChanges");
    String ajaxAction= sContext.getURLForIncludingResource("/IGWGOCWorkflowCalenderservlet");
   // System.out.println("action URL = " + action);
%>
<title>GOC Workflow Request</title>
</head>
<body>
 <form id="GOCform" action=""      method="post" >
		   <table>
		     <tbody>
		     		<tr >
				         <td>
				           <label >Maintenance Type :</label>
				         </td>
				         <td class="ebx_attribute" >
				         <select id="maintenanceType"  style="width:135px"  name="maintenanceType" onchange="checkFunctionID();">
							 <option value="regular">Regular</option>
							 <option value="functional">Functional</option>
							</select>	
				         </td>
			       </tr>
			     <!--      commented for DSMT0446 allow multiple SID for regular maintenance type
			       <tr id="siddiv" style="visibility:"  >
				         <td class="ebx_Label" >
				           <label >SID :</label>
				         </td>
				         <td class="ebx_Input">
				           <input type="text" id="sid" name="sid" value="" onblur="checkSID()"/>
				         </td>
			       </tr>
			         -->
			        <tr id="functiondiv" style="visibility:hidden;display:none;"  >
					         <td >
					           <label >Functional ID :</label>
					         </td>
					         <td class="ebx_Input">
					         	 <input type="text" id="functionalID123" name="functionalID123" value="" onblur=""/>
					         </td>
			       </tr>
		      
				</tbody>
		   </table>
		   <table>
		   <tr>
		   <td>
		   <label class="ebx_Label"><input id="validationCheck" type="checkbox"  onclick="loadClenderDate()">I agree that I will attach necessary attestation for any GOC Deactivation and/or Purge request.</label>
		   </td>
		   </tr>
		   <tr>
		     <td id="calenderData"> </td>
		     </tr>
		     
		   <tr><td>
		   <button type="button" id="submitButton" name ="submitButton" class="ebx_Button" onclick="validatevalue()">Launch GOC Workflow Request</button>
		    <button type="button" class="ebx_Button" onclick="loadClenderDate()">Refresh Calendar Days Remaining</button>
		   </td>
		  
		     
		    </table>
		     <input type="hidden" id="calenderActionId" name="calenderActionId" value="<%= ajaxAction %>"/>
		     <input type="hidden" id="actionid" name="actionid" value="<%= action %>"/>
		     <input type="hidden" id="isValidSID" name="isValidSID" value="true"/>
		      <input type="hidden" id="isfunctionalEndDate" name="isfunctionalEndDate" value="true"/>
		      <input type="hidden" id="managementOnly" name="managementOnly" value="false"/>
		      <input type="hidden" id="isValidFunctionID" name="isValidFunctionID" value="true"/>
		     
		  </form>
</body>
</html>