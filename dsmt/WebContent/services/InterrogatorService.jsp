<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.citi.ebx.dsmt.acctMatrix.AcctMatrixUpdateControllerDetails" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
try {
	final AcctMatrixUpdateControllerDetails form = new AcctMatrixUpdateControllerDetails();
	form.setServlet("/ControllerIDWorkflowServlet");
	//form.setDefaultImportConfigDataSpace(...);
	//form.setDefaultImportConfigDataSet(...);
	form.service(request, response);
} catch (final Exception ex) {
	%><p style="margin:10px;color:red">(!) <%=ex.getMessage()%></p><%
}
%>