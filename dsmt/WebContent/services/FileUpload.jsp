<%@page import="com.citi.ebx.workflow.service.OpenBPMCoachService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
try {
//	final FileUploadService servlet = new FileUploadService();

	final OpenBPMCoachService servlet = new OpenBPMCoachService();
	servlet.service(request, response);
} catch (final Exception ex) {
	%><p style="margin:10px;color:red">(!) <%=ex.getMessage()%></p><%
}
%>