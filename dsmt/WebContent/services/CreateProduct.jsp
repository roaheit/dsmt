<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<%@ page import="com.citi.ebx.service.LaunchCreateProductWorkflowServlet"%>

<%
try {
	(new LaunchCreateProductWorkflowServlet()).service(request, response);
} catch (Exception ex) {
	%><p style="color:red">Exception occurred: <%=ex.getMessage()%></p><%
}
%>