<%@page import="com.citi.ebx.goc.task.GenerateGOCSyncFileTask"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.orchestranetworks.ps.exporter.servlet.DataSetExportForm" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	try {

	System.out.println(new GenerateGOCSyncFileTask().generateSQLReport( "2014-05-02 00:00:00", "2014-05-02 23:59:59", "/citi/goc"));
	final DataSetExportForm form = new DataSetExportForm();
	form.setServlet("/BasicExport");
	form.service(request, response);
} catch (final Exception ex) {
%><p style="margin:10px;color:red">(!) <%=ex.getMessage()%></p><%
}
%>