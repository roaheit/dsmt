var div;
var popupDivBGImgURL;

function popupDetails(URL, BGImgURL)
{
	popupDivBGImgURL = BGImgURL;
	var iFrameWidth = getBodyWidth() - 300;
	var iFrameHeight = getBodyHeight() - 100;
	if (iFrameHeight <= 300)
		iFrameHeight = bodyHeight;
	if (iFrameWidth <= 300)
		iFrameWidth = bodyWidth;

	div = new popupDiv("mergerDiv","0px","0px", URL, iFrameWidth + "px", iFrameHeight + "px");
	div.setWidth("100%");
	div.setHeight("100%");
	div.setHasBG();
	div.setParentNode(document.body);
	div.createAndDisplayDiv();
}

function closeDiv(){
	div.hideAndDestroyDiv();
}

function getBodyWidth()
{
	var bodyWidth = 800;
	try {
		if (window.innerWidth) bodyWidth = window.innerWidth;
		else if (document.body.offsetWidth) bodyWidth = document.body.offsetWidth;
		else if (document.body.scrollWidth) bodyWidth = document.body.scrollWidth;
	} catch(e) {}
	return bodyWidth;
}

function getBodyHeight()
{
	var bodyHeight = 600;
	try {
		if (window.innerHeight) bodyHeight = window.innerHeight;
		else if (document.body.offsetHeight) bodyHeight = document.body.offsetHeight;
		else if (document.body.scrollHeight) bodyHeight = document.body.scrollHeight;
	} catch(e) {}
	return bodyHeight;
}

function popupDiv(divId, divTop, divLeft, iFrameURL, iFrameWidth, iFrameHeight)
{
	this.id = divId;
	this.top = divTop;
	this.left = divLeft;
	this.width = "0px";
	this.height = "0px";
	this.zIndex=99;
	this.hasBG = false;
	this.innerIFrameURL = iFrameURL;
	this.innerIFrameWidth = iFrameWidth;
	this.innerIFrameHeight = iFrameHeight;
	
	this.theDiv;
	this.theIFRame;
	
	this.createAndDisplayDiv = function()
	{
		this.theDiv=document.createElement("DIV");
		this.theDiv.id=this.id;
		this.theDiv.style.position = "absolute";
		this.theDiv.style.left = this.left;
		this.theDiv.style.top = this.top;
		this.theDiv.style.width = this.width;
		this.theDiv.style.height = this.height;
		this.theDiv.style.zIndex = this.zIndex;
		this.theDiv.style.display = "";
		if (this.hasBG && popupDivBGImgURL)
		{
			this.theDiv.style.backgroundColor = "rgba(0,0,0,0.25)";
			/*this.theDiv.style.opacity = "0.25";*/
		}
		this.theDiv.style.padding = "0px";
		this.theDiv.style.margin = "0px";
		this.theDiv.style.border = "0px";
		
		var tmpInnerHTML = "<table border=0 cellpadding=0 cellspacing=0 width=\"100%\" height=\"100%\"><tr><td align=center valign=middle><iframe id='" + this.getInnerFrameId() + "' src='" + this.innerIFrameURL + "' width='" + this.innerIFrameWidth + "' height='" + this.innerIFrameHeight + "' align='middle' scrolling='no' style='border: 1px solid gray; z-index: " + this.zIndex + ";' scrolling='auto' frameborder='0'></iframe></td></tr></table>";
		
		this.theDiv.innerHTML = tmpInnerHTML;

		this.divParentNode.appendChild(this.theDiv);
		this.theIFRame = document.getElementById(this.getInnerFrameId());
	}
	
	this.resize = function (newDivWidth, newDivHeight, newIFrameWidth, newIFrameHeight)
	{
		this.theDiv.style.width = newDivWidth;
		this.theDiv.style.height = newDivHeight;
		this.theIFRame.width=newIFrameWidth;
		this.theIFRame.height=newIFrameHeight;
	}
	
	this.getInnerFrameId = function()
	{
		return this.id + "_frame";
	}
	
	this.setParentNode = function (newParentNode)
	{
		this.divParentNode = newParentNode;
	}
	
	this.hideAndDestroyDiv=function()
	{
		this.theDiv.style.display="none";
		this.divParentNode.removeChild(this.theDiv);
	}
	
	this.setHasBG = function()
	{
		this.hasBG = true;
	}
	
	this.setZIndex = function(newZIndex)
	{
		this.zIndex = newZIndex;
	}
	
	this.setWidth = function(newWidth)
	{
		this.width=newWidth;
	}
	
	this.setHeight = function(newHeight)
	{
		this.height=newHeight;
	}
}